//-*-c++-*------------------------------------------------------------
// NAME: Average links travel times
// AUTH: Qi Yang
// FILE: MSA-LinkTime.h
// DATE: Sat Jun 15 01:26:51 1996
//--------------------------------------------------------------------

#ifndef MSA_LINK_TIME_HEADER
#define MSA_LINK_TIME_HEADER

#include <map>
#include <vector>
#include <iostream.h>
#include <Templates/STL_Configure.h>

class Reader;
class PS_LinkMap;
class PS_Link;

class PS_Link
{
      friend PS_LinkMap;

   public:

      PS_Link() : type_(0), length_(0.0) { }
      ~PS_Link() { }

      void read(Reader &is);
      void combine(PS_Link &, float alpha);
	  void calculate(int mode);
      void print(ostream& os = cout);

   private:

      int type_;
      float length_;
      vector<float ALLOCATOR> avg_;
      vector<float ALLOCATOR> min_;
      vector<float ALLOCATOR> max_;
};

class PS_LinkMap
{
      friend PS_Link;

   public:

      PS_LinkMap() { }
      ~PS_LinkMap() { }

      void read(Reader &is);
      void combine(Reader &is, float alpha);
	  void calculate(int mode);
      void print(ostream& os = cout);

   private:

      int start_;
      int nCols_;
      int length_;
      map<int, PS_Link, less<int> ALLOCATOR> links_;
};

#endif
