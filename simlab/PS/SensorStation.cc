//-*-c++-*------------------------------------------------------------
// NAME: Aggregating sensor data
// NOTE: 
// AUTH: Qi Yang
// FILE: SensorStation.C
// DATE: Tue May 21 15:50:56 1996
//--------------------------------------------------------------------

#include <stdlib.h>
#include <fstream.h>
#include <iomanip.h>

#include <Tools/Reader.h>
#include <Tools/ToolKit.h>

#include "Setup.h"
#include "SensorStation.h"

// Function Name: addIn()
// Parameters: station, counts, occupancy and  speed
// Returns: 1 if count is no zero and 0 otherwise

int
SensorStation::addIn(int id, int c, double o, double s)
{
   if (!station_) station_ = id;
   else if (id != station_) return 0;
   else if (c <= 0) return 0;
   count_ += c;			// sum
   occupancy_ += o;		// average
   speed_ += c * s;		// weighted average
   num_ += 1;			// number of sensor with data
   return (1);
}


// Function Name: calc()
// Parameters: None
// Returns: None

void 
SensorStation::calc()
{
   if (count_ > 0) {
      speed_ /= count_;
      occupancy_ /= num_;		// average
   } else {
      reset();
   }
}


void 
SensorStation::reset()
{
   station_ = 0;
   count_ = 0;
   occupancy_ = 0.0;
   speed_ = 0.0;
   num_ = 0;
}


int
LoadDataForOnePeriod(Reader& is, int nStations,
					 SensorStation* s)
{
   int station, count;
   double occ, speed;
   int i = 0, num = 0;
   if (!is.findToken(OPEN_TOKEN)) exit(1);
   for (is.eatwhite();
		is.peek() != CLOSE_TOKEN;
		is.eatwhite())	{
      is >> station
		 >> count >> occ >> speed;
	  if (i < nStations &&
		  (s + i)->addIn(station, count, occ, speed)) {
		 num ++;
	  }
	  i ++;
   }
   if (!is.findToken(CLOSE_TOKEN)) exit(1);
   if (i != nStations) {
	  cerr << "Error: " << nStations << " Stations specified, "
		   << i << " found." << endl;
   }
   return (num);
}


void
CalcVarsForOnePeriod(int n, SensorStation* s)
{
   for (int i = 0; i < n; i ++) {
      (s + i)->calc();
   }
}


void
ResetVarsForOnePeriod(int n, SensorStation* s)
{
   for (int i = 0; i < n; i ++) {
      (s + i)->reset();
   }
}


void
PrintCount(ofstream& os, int n, SensorStation* s, int* selected)
{
   for (int i = 0; i < n; i ++) {
      if (selected[i]) {
		 os << setw(7) << (s + i)->count() << endc;
      }
   }
   os << endl;
}

void
PrintOccupancy(ofstream& os, int n, SensorStation* s, int* selected)
{
   for (int i = 0; i < n; i ++) {
      if (selected[i]) {
		 os << setw(7) << (s + i)->occupancy() << endc;
      }
   }
   os << endl;
}

void
PrintSpeed(ofstream& os, int n, SensorStation* s, int* selected)
{
   for (int i = 0; i < n; i ++) {
      if (selected[i]) {
		 os << setw(7) << (s + i)->speed() << endc;
      }
   }
   os << endl;
}
