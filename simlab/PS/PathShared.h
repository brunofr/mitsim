//-*-c++-*------------------------------------------------------------
// AUTH: Qi Yang
// FILE: PathShared.h
// DATE: Tue Sep 17 21:24:09 1996
//--------------------------------------------------------------------

#ifndef PATHSHARED_HEADER
#define PATHSHARED_HEADER

#include <Templates/STL_Configure.h>
#include <vector>
#include <map>
#include <string>
#include <iostream.h>

class Reader;
class Path;

typedef map<string, Path, less<string> ALLOCATOR> PathTableType;

class Path
{
   public:

      Path() : code_(0), ori_(0), des_(0), cost_(0.0) { }
      ~Path() { }

      static void renumber(PathTableType&);
      static void load(char*, PathTableType&);

      void read(Reader &is);
      void print(ostream& os = cout) const;

      int ori() { return ori_; }
      int des() { return des_; }

      int code() { return code_; }
      int nLinks() { return path_.size(); }
      int link(int i) { return path_[i]; }
      float cost() { return cost_; }
      void cost(float c) { cost_ = c; }

      int isSameAs(Path &p);

   protected:

      int code_;
      int ori_;
      int des_;
      vector<int ALLOCATOR> path_;

      float cost_;
};

// Print a path table

ostream& operator << (ostream& os, const PathTableType& t);

#endif
