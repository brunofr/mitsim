//-*-c++-*------------------------------------------------------------
// NAME: Create link path table based on the OD path table
// AUTH: Qi Yang
// FILE: PathPointer.h
// DATE: Wed Jun  5 13:09:50 1996
//--------------------------------------------------------------------

#ifndef PATHPOINTER_HEADER
#define PATHPOINTER_HEADER

#include <vector>
#include <map>
#include <iostream.h>
#include <Templates/STL_Configure.h>

class Reader;

class LinkPath
{
   public:
      
      LinkPath() : pos_(-1), path_(-1) { }
      LinkPath(short int p, short int i) {
	path_ = p;
	pos_ = i;
      }
      ~LinkPath() { }

      void print(ostream& os);

   private:

      short int pos_;		// index in a OD Path
      short int path_;		// index in OD Path Table 
};

class LinkPathSet
{
   public:

      LinkPathSet() { }
      ~LinkPathSet() { }
      
      void add(short int path, short int pos);
      void print(ostream& os = cout);

   private:

      vector<LinkPath ALLOCATOR> paths_;	// set of ODPaths
};

typedef map<int, LinkPathSet, less<int> ALLOCATOR> LinkPathTable;

class ODPath
{
   public:

      ODPath() : code_(0), ori_(0), des_(0), cost_(0.0) { }
      ~ODPath() { }

      void read(Reader& is, int code);
      void print(ostream& os);
      void setLinkPointers(LinkPathTable&);

   protected:

      int code_;
      short int ori_;
      short int des_;
      vector<short int ALLOCATOR> path_;
      float cost_;
};

typedef map<int, ODPath, less<int> ALLOCATOR> ODPathTable;

ODPathTable* LoadODPathTable(char *filename);
void PrintODPathTable(ODPathTable *, ostream& os = cout);
LinkPathTable * CreateLinkPathTable(ODPathTable *);
void PrintLinkPathTable(LinkPathTable *, ostream& os = cout);

#endif
