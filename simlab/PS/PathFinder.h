//-*-c++-*------------------------------------------------------------
// NAME: Attach path IDs to the OD Table
// AUTH: Qi Yang
// FILE: PathFinder.h
// DATE: Wed Jun  5 13:09:50 1996
//--------------------------------------------------------------------

#ifndef PATHFINDER_HEADER
#define PATHFINDER_HEADER

#include <vector>
#include <map>
#include <iostream.h>
#include <Templates/STL_Configure.h>

class ODPath
{
   public:

      ODPath() : ori_(0), des_(0), cost_(0.0) { }
      ~ODPath() { }

      void read(Reader& is);
      void print(ostream& os);
      short int ori() { return ori_; }
      short int des() { return des_; }

   protected:

      short int ori_;
      short int des_;
      vector<short int ALLOCATOR> path_;
      float cost_;
};

typedef map<int, ODPath, less<int> ALLOCATOR> ODPathTable;
typedef vector<int ALLOCATOR> PathSet;

ODPathTable* LoadODPathTable(char *filename);
void PrintODPathTable(ODPathTable *, ostream& os = cout);


PathSet FindPath(ODPathTable *odp, short int ori, short int des);

void ConvertODTable(char *filename, ODPathTable *,
		    ostream& os = cout);

#endif
