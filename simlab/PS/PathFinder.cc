//-*-c++-*------------------------------------------------------------
// NAME: Attach path IDs to the OD Table
// AUTH: Qi Yang
// FILE: PathTableGenerator.C
// DATE: Wed Jun  5 13:08:10 1996
//--------------------------------------------------------------------

#include <stdio.h>
#include <Tools/Reader.h>
#include "PathShared.h"
#include <stdlib.h>
#include <vector>
#include <string>
#include <iostream.h>
#include <fstream.h>

class Pair;

typedef vector<int ALLOCATOR> PathSet;
typedef map<string, Pair, less<string> ALLOCATOR> PairSet;

class Pair {
   public:
      Pair() : ori_(0), des_(0) { }
      Pair(int o, int d) : ori_(o), des_(d) { }
      ~Pair() { }
      void print(ostream &os) const {
		// we create 3 vehicles for each pair
		os << "  5 * { " << ori_ << " " << des_ << " }";
      }
   private:
      int ori_, des_;
};

// Print out a path

template <class T>
ostream& operator << (ostream& os, const T& p)
{
   p.second.print(os);
   return os;
}

ostream& operator << (ostream& os, const PairSet& t)
{
   os << "/*" << endl
      << " * This files list OD pairs that have no path assigned." << endl
	  << " * You may use this file as vehicle trip table to create" << endl
	  << " * paths for the listed OD pairs using the simulator." << endl
      << " */" << endl << endl;

   os << 0 << "\t" << "# time" << endl;
   os << "{\t# " << t.size() << " OD pairs" << endl;

   copy(t.begin(), t.end(),
        ostream_iterator <PairSet::value_type>(os, "\n"));

   os << "}" << endl << endl;
   os << "<END>" << endl;
   
   return os;
}

PathSet* FindPath(PathTableType& table, int ori, int des)
{
   PathSet *ps = new vector<int ALLOCATOR>;
   PathTableType::iterator i;
   for (i = table.begin(); i != table.end(); i ++) {
      Path &p = (*i).second;
      if (p.ori() == ori && p.des() == des) {
		 ps->push_back(p.code());
      }
   }
   return ps;
}


PairSet* ConvertODTable(char *filename, PathTableType& table,
						ostream& os = cout)
{
   typedef PairSet::value_type value_type;
   PairSet* ps = new PairSet;
   char key[24];
   Reader is(filename);
   int ori, des, type;
   float rate, scale, var, dis;
   double time;

   os << "/*" << endl
	  << " * OD Trip Table File" << endl
	  << " */" << endl << endl;

   while (is.good() && is.peek() != '<') {
	  time = is.gettime();
      is >> type >> scale;
      if (!is.findToken('{')) exit(1);
      os << endl << time << " " << type
		 << " " << scale << endl
		 << "{" << endl;
      while (is.peek() != '}') {
		 if (!is.findToken('{')) exit(1);
		 is >> ori >> des >> rate;
		 
		 os << "  { "
			<< ori << " "
			<< des << " "
			<< rate << " ";

		 if (is.peek() != '}') { // variance
			is >> var;
			os << var << " ";
		 }
		 if (is.peek() != '}') { // distribution factor
			is >> dis;
			os << dis << " ";
		 }
		 if (is.peek() != '}') { // old paths
			while(is.get() != '}') ;
		 }
		 if (!is.findToken('}')) exit(1);

		 if (rate > 1.0E-5) {	// no need to find path for zero demand
		   PathSet* p = FindPath(table, ori, des);
		   if (p->size() > 0) {
			 os << "{ ";
			 for (register int i = 0; i < p->size(); i ++) {
			   os << (*p)[i] << " ";
			 }
			 os << "} ";
		   } else {
			 sprintf(key, "%5.5d%5.5d", ori, des);
			 ps->insert(value_type(string(key), Pair(ori, des)));
		   }
		   delete p;
		 }

		 os << "}\n";
      }
      if (!is.findToken('}')) exit(1);
      os << "}" << endl;
   }
   os << endl << "<END>" << endl;
   return ps;
}


int main (int argc, char *argv[])
{
   if (argc != 3) {
      cerr << "Usage: " << argv[0] << " PathTable ODTable" << endl;
      return 1;
   }

   PathTableType table;

   Path::load(argv[1], table);
   PairSet *ps = ConvertODTable(argv[2], table);

   if (ps->size() > 1) {
      const char *log = "nopath.out";
      ofstream os(log);
      os << *ps;
      os.close();
      cout << endl
		   << "% " << ps->size()
		   << " pairs have no path assigned (see "
		   << log << ")." << endl; 
   }

   cout << endl
		<< "% Generated from:" << endl
		<< "%   " << argv[1] << endl
		<< "%   " << argv[2] << endl
		<< "% Using the program " << argv[0] << " by Qi Yang" << endl;

   return 0;
}
