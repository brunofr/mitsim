//-*-c++-*------------------------------------------------------------
// AUTH: Qi Yang
// FILE: PathShared.C
// DATE: Tue Sep 17 21:22:31 1996
//--------------------------------------------------------------------

#include <stdio.h>
#include "PathShared.h"
#include <Tools/Reader.h>
#include <stdlib.h>

void Path::read(Reader &is)
{
   int link;

   if (!is.findToken('{')) exit(1);

   is >> code_ >> ori_ >> des_;

   if (!is.findToken('{')) exit(1);

   while (is.good() && is.peek() != '}') {
      is >> link;
      path_.push_back(link);
   }

   if (!is.findToken('}')) exit(1);

   is >> cost_;

   if (!is.findToken('}')) exit(1);
}


void Path::print(ostream& os) const
{
   os << "  {"
      << " " << code_
      << " " << ori_
      << " " << des_
      << " {";
   int n = path_.size();
   for (register int i = 0; i < n; i ++) {
      os << " " << path_[i];
   }
   os << " } " << cost_ << " }";
}


int Path::isSameAs(Path &p)
{
   if (path_.size() != p.path_.size()) return 0;
   int n = path_.size();
   for (register int i = 0; i < n; i ++) {
      if (path_[i] != p.path_[i]) return 0;
   }
   return 1;
}


void Path::load(char *filename, PathTableType &table)
{
   typedef PathTableType::value_type value_type;
   char key[24];
   char title[128];
   Path *path;
   Reader is(filename);
   is.getline(title, 128);

   if (!is.findToken('{')) exit(1);
   while (is.good() && is.peek() != '}') {
      path = new Path;
      path->read(is);
      sprintf(key, "%5.5d", path->code());
      table.insert(value_type(string(key), *path));
      delete path;
   }
   if (!is.findToken('}')) exit(1);
   
   is.close();
}

void Path::renumber(PathTableType& table)
{
   int id = 0;
   PathTableType::iterator i;
   for (i = table.begin(); i != table.end(); i ++) {
      id ++;
      (*i).second.code_ = id;
   }
}


// Print out a path

template <class T>
ostream& operator << (ostream& os, const T& p)
{
   p.second.print(os);
   return os;
}


// Print out a path table

ostream& operator << (ostream& os, const PathTableType& t)
{
   os << "/*" << endl
      << " * Path Table File" << endl
      << " */" << endl << endl
      << "[Path Table] : " << t.size() << endl
      << "{" << endl;

   copy(t.begin(), t.end(),
	ostream_iterator <PathTableType::value_type>(os, "\n"));

   os << "}" << endl;
   
   return os;
}
