//-*-c++-*------------------------------------------------------------
// NAME: The program compare the simulated sensor data with field data
// NOTE: 
// AUTH: Qi Yang
// FILE: Compare.C
// DATE: Tue May 21 12:34:35 1996
//--------------------------------------------------------------------

#include <fstream.h>
#include <strstream.h>
#include <new.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <Tools/Reader.h>
#include <Tools/ToolKit.h>

#include "Setup.h"
#include "SensorStation.h"

// Function Name: readField()
// Parameters: Field data file
// Returns: Array of field data

double*
readField(char *fname)
{
   Reader is(fname);
   int num = theSetup->nPeriods * theSetup->nStations;
   double *field = new double[num];
   int i, j;
   for (i = 0; i < theSetup->nPeriods; i ++) {
      if (!(theSetup->prdSelected[i])) continue;
      for (j = 0; j < theSetup->nStations; j ++) {
		 if (!(theSetup->stnSelected[j])) continue;
		 is >> *(field + i * theSetup->nStations + j);
      }
   }
   return field;
}


// Function Name: print()
// Parameters: Filename and data to be printed
// Returns: None

void
print(char *flag, char chos, double *f)
{
   char fname[32];
   sprintf(fname, "%s%c.txt", flag, chos);
   ofstream os(fname);
   double *mean = new double[theSetup->nStations];
   int i, j;
   for (j = 0; j < theSetup->nStations; j ++) mean[j] = 0.0;

   double msn, average;
   printf("\n%4s ", "T/S");
   for (j = 0; j < theSetup->nStations; j ++) {
      if (theSetup->stnSelected[j]) printf("%7d", j+1);
   }
   printf("\n%4s ", "Code");
   for (j = 0; j < theSetup->nStations; j ++) {
      if (theSetup->stnSelected[j]) {
		 printf("%7d", theSetup->station[j].station());
	  }
   }
   printf("%7s\n", "AVG");

   for (i = 0; i < theSetup->nPeriods; i ++) {
      if (!(theSetup->prdSelected[i])) continue;
      printf("%4d ", i+1);
      average = 0.0;
      for (j = 0; j < theSetup->nStations; j ++) {
		 if (!(theSetup->stnSelected[j])) continue;
		 msn = f[i * theSetup->nStations + j];
		 printf("%7.1lf", msn);
		 os << msn << endc;
		 average += msn;
		 mean[j] += msn;
      }
      printf("%7.1lf\n", average/theSetup->nSelectedStations);
      os << endl;
   }
   printf("%4s ", "AVG");
   average = 0.0;
   for (j = 0; j < theSetup->nStations; j ++) {
      if (!(theSetup->stnSelected[j])) continue;
      msn = mean[j];
      average += msn;
      printf("%7.1lf", msn/theSetup->nSelectedPeriods);
   }
   printf("%7.1lf\n", average/(theSetup->nSelectedPeriods *
							   theSetup->nSelectedStations));
   os.close();
   delete [] mean;
}


// Function Name: statistics()
// Parameters: Field data and simulated data
// Returns: None

void
statistics(double *f, double *s)
{
   int m = theSetup->nPeriods;
   int n = theSetup->nStations;
   double *diff = new double[m * n];
   double *rmse = new double[m];
   double *cmse = new double[n];
   double *r = new double[m];
   double *c = new double[n];
   int num = m * n;
   int i, j, k, cts;

   // take the difference between simulated and field obs
    
   for (i = 0; i < num; i ++) diff[i] = s[i] - f[i];

   double mse;
   for (i = 0; i < m; i ++) {
      if (!(theSetup->prdSelected[i])) continue;
      rmse[i] = 0.0;
      r[i] = 0.0;
      cts = 0;
      for (j = 0; j < n; j ++) {
		 if (!(theSetup->stnSelected[j]) ||
			 !(theSetup->valStation[j])) continue;
		 k = i * n + j;
		 mse = diff[k];
		 rmse[i] += mse * mse;
		 r[i] += f[k];
		 cts ++;
      }
      if (cts > 0 && r[i] > 1.0E-5) {
		 rmse[i] = sqrt(rmse[i]/cts);
		 r[i] /= cts;
		 r[i] = rmse[i] / r[i] * 100.0;
      } else {
		 rmse[i] = r[i] = 0.0;
      }
   }
   for (j = 0; j < n; j ++) {
      if (!(theSetup->stnSelected[j])) continue;
      cmse[j] = 0.0;
      c[j] = 0.0;
      cts = 0;
      for (i = 0; i < m; i ++) {
		 if (!(theSetup->prdSelected[i]) ||
			 !(theSetup->valPeriod[i])) continue;
		 k = i * n + j;
		 mse = diff[k];
		 cmse[j] += mse * mse;
		 c[j] += f[k]; 
		 cts ++;
      }
      if (cts > 0 && c[j] > 1.0E-5) {
		 cmse[j] = sqrt(cmse[j]/cts);
		 c[j] /= cts;
		 c[j] = cmse[j] / c[j] * 100.0;
      } else {
		 cmse[j] = c[j] = 0.0;
      }
   }

   // calculate the means

   double f_mean = 0.0;
   double s_mean = 0.0;
   double rmspe = 0.0;			// rms percent error
   int rmspe_used = 0;			// number samples in rms percent error
   int rmspe_skipped = 0;
   double f_sq = 0.0;
   double s_sq = 0.0;
   double mean_err = 0.0;
   double mean_p_err = 0.0;
   mse = 0.0;
   cts = 0;
   for (i = 0; i < m; i ++) {

      if (!(theSetup->prdSelected[i]) ||
		  !(theSetup->valPeriod[i])) continue;

      for (j = 0; j < n; j ++) {

		 if (!(theSetup->stnSelected[j]) ||
			 !(theSetup->valStation[j])) continue;

		 k = i * n + j;
		 f_mean += f[k];
		 s_mean += s[k];
		 mse += diff[k] * diff[k];
		 f_sq += f[k] * f[k];
		 s_sq += s[k] * s[k];
		 mean_err += diff[k];

		 cts ++;

		 if (fabs(f[k]) > 1.0E-2) {
			double ratio = diff[k] / f[k];
			mean_p_err += ratio;
			rmspe += ratio * ratio;
			rmspe_used ++;
		 } else {
			rmspe_skipped ++;
		 }
	  }
   }
   if (cts < 0) {
	  printf("\nNo data points selected\n");
	  goto done;
   }

   f_mean = f_mean / cts;
   s_mean = s_mean / cts;
   f_sq = f_sq / cts;
   s_sq = s_sq / cts;
   mean_err = mean_err / cts;

   double avg_sq_error = mse / cts;
   double rms = sqrt(avg_sq_error);

   if (rmspe_used > 0) {
	  rmspe = sqrt(rmspe / rmspe_used);
	  mean_p_err = mean_p_err / rmspe_used;
   }

   double s_std = 0.0;
   double f_std = 0.0;
   double fs_std = 0.0;
   for (i = 0; i < m; i ++) {
      if (!(theSetup->prdSelected[i]) ||
		  !(theSetup->valPeriod[i])) continue;
      for (j = 0; j < n; j ++) {
		 if (!(theSetup->stnSelected[j]) ||
			 !(theSetup->valStation[j])) continue;

		 k = i * n + j;
		 f_std += (f[k] - f_mean) * (f[k] - f_mean);
		 s_std += (s[k] - s_mean) * (s[k] - s_mean);
		 fs_std += (f[k] - f_mean) * (s[k] - s_mean);
      }
   }
   f_std = sqrt(f_std / cts);
   s_std = sqrt(s_std / cts);
   fs_std = fs_std / cts;

   double rho = fs_std / (f_std * s_std);
   double u = rms / (sqrt(f_sq) + sqrt(s_sq)); 
   double um = (s_mean - f_mean) * (s_mean - f_mean) / avg_sq_error;
   double us = (s_std - f_std) * (s_std - f_std) / avg_sq_error;
   double uc = 2.0 * (1.0 - rho) * s_std * f_std / avg_sq_error;

   printf("\n%4s  ", "T/S");
   for (j = 0; j < n; j ++) {
      if (!(theSetup->stnSelected[j])) continue;
      printf("%6d", j+1);
      if (!(theSetup->valStation[j])) printf("~");
      else printf(" ");
   }
   printf("\n%4s ", "Code");
   for (j = 0; j < theSetup->nStations; j ++) {
      if (theSetup->stnSelected[j]) {
		 printf("%7d", theSetup->station[j].station());
	  }
   }
   printf("%7s\n", "MSE");

   for (i = 0; i < m; i ++) {
      if (!(theSetup->prdSelected[i])) continue;
      printf("%4d", i+1);
      if (!(theSetup->valPeriod[i])) printf("~");
      else printf(" ");
      for (j = 0; j < n; j ++) {
		 if (!(theSetup->stnSelected[j])) continue;
		 printf("%7.1lf", diff[i * n + j]);
      }
      printf("%7.1lf\n", rmse[i]);
   }
   printf("%4s ", "MSE");
   for (j = 0; j < n; j ++) {
      if (!(theSetup->stnSelected[j])) continue;
      printf("%7.1lf", cmse[j]);
   }
   printf("%7.1lf\n", rms);

   const int w = 30;
   printf("\n%*s = %.2lf\n", w, "rms error", rms);
   printf("%*s = %.2lf\n", w, "mean error", mean_err);

   if (rmspe_used > 0) {
	  printf("%*s = %.2lf %%\n", w, "mean percent error", mean_p_err * 100.0);
	  printf("%*s = %.2lf %%\n", w, "rms percent error", rmspe * 100.0);
	  if (rmspe_skipped) {
		 printf("(%d points skipped because actual data equal to 0).\n",
				rmspe_skipped);
	  }
   }

   printf("%*s = %.2lf\n", w, "Correlation", rho);
   printf("%*s = %.4lf\n", w, "Theil's inequality coefficient", u);
   printf("%*s :\n", w, "Proportions of inequality");
   printf("%*s = %.4lf\n", w, "  Um", um);
   printf("%*s = %.4lf\n", w, "  Us", us);
   printf("%*s = %.4lf\n", w, "  Uc", uc);
   printf("%*s = %d\n", w, "Number of data points", cts);

  done:

   delete [] c;
   delete [] r;
   delete [] cmse;
   delete [] rmse;
   delete [] diff;
}


// Function Name: readSimulated()
// Parameters: Input file name, data flag, pointer to the output array
// Returns: Number of valid records processed

int
readSimulated(char *fname, char chos, double *run)
{
   Reader is(fname);
   double time_stamp;
   int recording = 0;
   int cts = 0;
   double selected_time = theSetup->startTime + theSetup->stepSize;
   int i, k = 0;
   int num = theSetup->nPeriods * theSetup->nStations;

   for (i = 0; i < num; i ++) run[i] = 0.0;

   while (!(is.eof())) {
      is >> time_stamp;
      if (!recording && fabs(time_stamp - selected_time) < 1.E-5) {
		 recording = 1;
      } else if (recording && time_stamp > theSetup->endTime) {
		 break;
      }
      ResetVarsForOnePeriod(theSetup->nStations,
							theSetup->station);
      num = LoadDataForOnePeriod(is, theSetup->nStations,
								 theSetup->station);
      if (recording && theSetup->prdSelected[k]) {
		 cts += num;
		 CalcVarsForOnePeriod(theSetup->nStations,
							  theSetup->station);
		 double *start = run + k * theSetup->nStations;
		 switch (chos) {
			case 'c':
			theSetup->getCount(start);
			break;
			case 'o':
			theSetup->getOccupancy(start);
			break;
			case 's':
			theSetup->getSpeed(start);
			break;
		 }
      }
      if (recording) k ++;
      is.eatwhite();
   }
   is.close();
   return (cts);
}


int
main(int argc, char *argv[])
{
   if (argc < 5) {
      cerr << "This program aggregate simulated sensor data and ";
      cerr << "compares them with field data." << endl;
      cerr << "Usage: " << argv[0];
      cerr << " ConfigFile [c|o|s] [Field|None] Run1 Run2 ..." << endl;
      return(1);
   }

   theSetup = new Setup(argv[1]);

   int nRows = theSetup->nPeriods;
   int nCols = theSetup->nStations;

   ostrstream title;
   char chos = *argv[2];
   switch (chos) {
      case 'c':
      {
		 title << "Count";
		 break;
      }
      case 'o':
      {
		 title << "Occupancy";
		 break;
      }
      case 's':
      {
		 title << "Speed";
		 break;
      }
      default:
      {
		 cerr << "Error:: <" << chos
			  << "> is invalid option!" << endl;
		 return 1;
      }
   }
	
   title << null;

   int i, j, num = nRows * nCols;
   double *simulated = new double[num];
   double *std = new double[num];
   double *run = new double[num];

   for (i = 0; i < num; i ++) simulated[i] = 0.0;
   for (i = 0; i < num; i ++) std[i] = 0.0;

   // load field data
    
   double *field = NULL;
   int args;

   if (ToolKit::comp(argv[3], "none", 1) != 0) {
      if (!(field = readField(argv[3]))) {
		 return (1);
      }
      args = 4;
   } else {
      args = 3;
   }

   // load each file
    
   int nfiles = 0;
   for  (j = args; j < argc; j ++) {
      if (!readSimulated(argv[j], chos, run)) {
		 return (1);
      }
      for (i = 0; i < num; i ++) {
		 simulated[i] += run[i];
		 std[i] += run[i] * run[i];
      }
      nfiles ++;
   }

   // take the average over runs
    
   if (nfiles > 1) {
      for (i = 0; i < num; i ++) {

		 // this is the mean
	  
		 simulated[i] /= nfiles;
		 double mu = simulated[i];

		 // standard error
		 double x = std[i] / nfiles - mu * mu;
		 if (x <= 0.0) std[i] = 0.0;
		 else std[i] = sqrt(x);
      }
   }

   // print results
    
   if (field) {
      printf("\n\nField %s (%s)\n", title.str(), argv[3]);
      print("f", chos, field);
   }

   printf("\n\nSimulated %s [%d Run(s):", title.str(), nfiles);
   for  (j = 4; j < argc; j ++) {
      printf(" %s", argv[j]);
   }

   printf("]\n");
   print("m", chos, simulated);

   if (nfiles > 1) {
      printf("\n\nStandard Deviation [%d Run(s)]\n", nfiles);
      print("e", chos, std);
   }

   if (field) {
      printf("\n\nDeviation between Simulated and Field\n");
      statistics(field, simulated);
   }
   return (0);
}

