//-*-c++-*------------------------------------------------------------
// NAME: Average simulated sensor data for multiple runs and compare
//       the result with field data
// NOTE: 
// AUTH: Qi Yang
// FILE: Setup.h
// DATE: Tue May 21 13:03:58 1996
//--------------------------------------------------------------------

#ifndef SETUP_HEADER
#define SETUP_HEADER

class SensorStation;
class Reader;

class Setup
{
   public:

      Setup(char *);
      ~Setup() {}

      void defaultList(int, int*, int);
      int addAllToList(Reader&, int, int*);
      int drapFromList(Reader&, int, int*);
      int addToList(Reader&, int, int*);

      void getCount(double *);
      void getOccupancy(double *);
      void getSpeed(double *);

      int nStations;		// maximum num of sensor stations 
      double startTime;	// start time in seconds
      double endTime;		// end time in seconds
      double stepSize;		// length of each interval
      int nPeriods;		// number of time periods
      int* stnSelected;	// true if the station is selected
      int* prdSelected;	// true if the time period is selected

      SensorStation *station;
      int nSelectedStations;
      int nSelectedPeriods;

      // For validation statistics

      int* valStation;		// true if the station is selected
      int* valPeriod;		// true if the period is selected
      int nValStations;
      int nValPeriods;
};

extern Setup * theSetup;

#endif
