//-*-c++-*------------------------------------------------------------
// NAME: Delete duplicated and "ramp" paths
// AUTH: Qi Yang
// FILE: PathDelete.C
// DATE: Wed Jun  5 13:08:10 1996
//--------------------------------------------------------------------

#include <stdio.h>
#include "PathShared.h"
#include "LinkShared.h"
#include <Tools/Reader.h>
#include <stdlib.h>
#include <fstream.h>

void LoadPathTable(char *filename, PathTableType &table)
{
   typedef PathTableType::value_type value_type;
   char key[24];
   char title[128];
   Path *path;
   Reader is(filename);
   is.getline(title, 128);

   if (!is.findToken('{')) exit(1);
   while (is.good() && is.peek() != '}') {
      path = new Path;
      path->read(is);
      sprintf(key, "%5.5d%5.5d%5.5d",
	      path->ori(), path->des(), path->code());
      table.insert(value_type(string(key), *path));
      delete path;
   }
   if (!is.findToken('}')) exit(1);
   
   is.close();
}


// Find the path i in the range given by (s,e). If path i does not
// exist in this range, it returns e.

PathTableType::iterator
FindPath(PathTableType::iterator s,
	 PathTableType::iterator e,
	 Path &i)
{
   PathTableType::iterator k = s;
   while (k != e) {
      if (i.isSameAs((*k).second)) break;
      k ++;
   }
   return k;
}


PathTableType*
RemoveDuplicatedPaths(PathTableType &table)
{
   PathTableType* erased = new PathTableType;
   PathTableType::iterator i = table.begin();
   PathTableType::iterator k;
   PathTableType::iterator p;

   i ++;
   while(i != table.end()) {
      k = i;
      i ++;
      p = FindPath(table.begin(), k, (*k).second);
      if (p != k) {
	 erased->insert(PathTableType::value_type(
	    (*k).first,
	    (*k).second));
	 table.erase(k);		// duplicated
      }
   }
   return erased;
}


// Return 1 if it is a freeway-ramps-freeway path or 0 otherwise

int IsRampPath(LinkTableType& linktable, Path &p)
{
   int n = p.nLinks();
   int id, type;
   int phase = 0;
   for (register int i = 0; i < n; i ++) {
      id = p.link(i);
      type = linktable[id].linkType();
      if (phase == 2) {		// phase 2
	 if (type == LINK_TYPE_FREEWAY) {
	    return 1;
	 }
      } else if (phase == 1) {	// phase 1
	 if (type != LINK_TYPE_FREEWAY) {
	    phase = 2;		// switch to 2nd phase
	 }
      } else if (phase == 0) {	// phase 0
	 if (type == LINK_TYPE_FREEWAY) {
	    phase = 1;		// switch to 1st phase
	 }
      }
   }
   return 0;
}

PathTableType*
RemoveRampPaths(LinkTableType& linktable, PathTableType& pathtable)
{
   PathTableType* erased = new PathTableType;
   PathTableType::iterator i = pathtable.begin();;
   PathTableType::iterator k;
   while(i != pathtable.end()) {
      k = i;
      i ++;
      if (IsRampPath(linktable, (*k).second)) {
	 erased->insert(PathTableType::value_type(
	    (*k).first,
	    (*k).second));
	 pathtable.erase(k);
      }
   }
   return erased;
}


int main (int argc, char *argv[])
{
   if (argc < 2) {
      cerr << "Usage: " << argv[0]
	   << " PathTable [LinkTimeTable]" << endl;
      return 1;
   }

   PathTableType pathtable;
   LoadPathTable(argv[1], pathtable);

   PathTableType *duppath = RemoveDuplicatedPaths(pathtable);
   PathTableType *ramppath;

   if (argc > 2) {
	  LinkTableType linktable;
	  Link::read(argv[2], linktable);
	  ramppath = RemoveRampPaths(linktable, pathtable);
   } else {
	  ramppath = NULL;
   }

   cout << pathtable;
   
   if (duppath && duppath->size()) {
     const char *duppath_log = "dup.out";
     ofstream os(duppath_log);
     if (!os.good()) {
       cerr << "% Error:: can not open " << duppath_log << endl;
       return 1;
     }
     cout << endl << "% " << duppath->size()
	  << " duplicated path removed (see "
	  << duppath_log << ")." << endl;
     os << *duppath;
     os.close();
   }
   if (ramppath && ramppath->size()) {
     const char *ramppath_log = "ramp.out";
     ofstream os(ramppath_log);
     if (!os.good()) {
       cerr << "% Error:: can not open " << ramppath_log << endl;
       return 1;
     }
     cout << endl << "% " << ramppath->size()
	  << " ramp path removed (see " << ramppath_log << ")." << endl;
     os << *ramppath;
     os.close();
   }

   cout << endl	<< "% This file is generated from:" << endl
	<< "%   " << argv[1] << endl
	<< "%   " << argv[2] << endl
	<< "% Using the program " << argv[0]
	<< " developed by Qi Yang" << endl;

   return 0;
}
