//-*-c++-*------------------------------------------------------------
// AUTH: Qi Yang
// FILE: LinkShared.C
// DATE: Tue Sep 17 21:22:31 1996
//--------------------------------------------------------------------

#include "LinkShared.h"
#include <Tools/Reader.h>
#include <stdlib.h>

int Link::startPeriod_ = 0;
int Link::nPeriods_ = 0;
int Link::periodLength_ = 0;

void Link::read(char *filename, LinkTableType& table)
{
   int id;
   Reader is(filename);
   is >> startPeriod_ >> nPeriods_ >> periodLength_;
   if (!is.findToken('{')) exit(1);
   while(is.good() && is.peek() != '}') {
      is >> id;
      table[id].read(is, id);
   }
   if (!is.findToken('}')) exit(1);
}

void Link::read(Reader &is, int id)
{
   code_ = id;
   is >> type_ >> length_;
   cost_.resize(nPeriods_);
   for (register int i = 0; i < nPeriods_; i ++) {
      is >> cost_[i];
   }
}


void Link::print(ostream& os) const
{
   os << "  " << code_
      << " " << type_
      << " " << length_;
   int n = cost_.size();
   for (register int i = 0; i < n; i ++) {
      os << " " << cost_[i];
   }
}


float Link::cost(float t)
{
   float dt = t / periodLength_ + 0.5;
   int i = (int)dt;
   if (i < 1) {
      return cost_[0];
   } else if (i >= nPeriods_) {
      return cost_[nPeriods_ - 1];
   } else {
      dt = t - (i - 0.5) * periodLength_;
      float z = cost_[i-1];
      return z + (cost_[i] - z) / periodLength_ * dt;
   }
}


// Print out a path

template <class T>
ostream& operator << (ostream& os, const T& p)
{
   p.second.print(os);
   return os;
}


// Print out a path table

ostream& operator << (ostream& os, const LinkTableType& t)
{
   copy(t.begin(), t.end(),
	ostream_iterator <LinkTableType::value_type>(os, "\n"));
   return os;
}
