//-*-c++-*------------------------------------------------------------
// AUTH: Qi Yang
// FILE: LinkShared.h
// DATE: Tue Sep 17 21:24:09 1996
//--------------------------------------------------------------------

#ifndef LINKSHARED_HEADER
#define LINKSHARED_HEADER

#include <Templates/STL_Configure.h>
#include <vector>
#include <map>
#include <string>
#include <iostream.h>

class Reader;
class Link;

typedef map<int, Link, less<int> ALLOCATOR> LinkTableType;

enum {
   LINK_TYPE_FREEWAY = 1,
   LINK_TYPE_RAMP    = 2,
   LINK_TYPE_URBAN   = 3
};

const int LINK_TYPE_MASK = 7;
const int IN_TUNNEL_MASK = 8;

class Link
{
   public:

      Link() : code_(0), type_(0), length_(0.0) { }
      ~Link() { }

      static void read(char *, LinkTableType& table);
      void read(Reader &is, int id);
      void print(ostream& os = cout) const;

      int type() { return type_; }
      inline int linkType() { return type_ & LINK_TYPE_MASK; }
      inline int isInTunnel() { return type_ & IN_TUNNEL_MASK; }
      int length() { return length_; }
      int code() { return code_; }
      float cost(float t);

   protected:

      static int startPeriod_;
      static int nPeriods_;
      static int periodLength_;

      int code_;
      int type_;
      float length_;
      vector<float ALLOCATOR> cost_;
};

// Print out a path table

ostream& operator << (ostream& os, const LinkTableType& t);

#endif
