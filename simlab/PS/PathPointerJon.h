//-*-c++-*------------------------------------------------------------
// NAME: Create link path table based on the OD path table
// AUTH: Qi Yang
// FILE: PathPointerJon.h
// DATE: Wed Jun  5 13:09:50 1996
//--------------------------------------------------------------------

#ifndef PATHPOINTERJON_HEADER
#define PATHPOINTERJON_HEADER

#include <vector>
#include <map>
#include <iostream.h>
#include <Templates/STL_Configure.h>

class Reader;

class LinkPath
{
   public:
      
      LinkPath() : link_(-1), path_(-1) { }
      LinkPath(short int l, short int p) {
	link_ = l;
	path_ = p;
      }
      ~LinkPath() { }

      void print(ostream& os);

   private:

      short int link_;		// next link
      short int path_;		// index in path vector of next link
};

class LinkPathSet
{
   public:

      LinkPathSet() { }
      ~LinkPathSet() { }
      
      void add(short int link, short int path);
      void print(ostream& os = cout);
      vector<LinkPath ALLOCATOR>& paths() { return paths_; }

   private:

      vector<LinkPath ALLOCATOR> paths_;	// set of ODPaths
};

typedef map<int, LinkPathSet, less<int> ALLOCATOR> LinkPathTable;

class ODPath
{
   public:

      ODPath() : code_(0), ori_(0), des_(0), cost_(0.0) { }
      ~ODPath() { }

      void read(Reader& is, int code);
      void print(ostream& os);
      void setLinkPointers(LinkPathTable&);

   protected:

      int code_;
      short int ori_;
      short int des_;
      vector<short int ALLOCATOR> path_;
      float cost_;
};

typedef map<int, ODPath, less<int> ALLOCATOR> ODPathTable;

ODPathTable* LoadODPathTable(char *filename);
void PrintODPathTable(ODPathTable *, ostream& os = cout);
LinkPathTable * CreateLinkPathTable(ODPathTable *);
void PrintLinkPathTable(LinkPathTable *, ostream& os = cout);

#endif
