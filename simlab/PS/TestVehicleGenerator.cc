//-*-c++-*------------------------------------------------------------
// NAME: TestVehicleGenerator.C
// This file generates test vehicles for each od pair.  These test
// vehicles can be loaded into the simulator to generate path tables
// needed by production runs of (MITSIM and MesoTS).
//--------------------------------------------------------------------

#include <iostream.h>
#include <stdlib.h>
#include <stdio.h>

#include <Tools/Reader.h>
#include <Templates/STL_Configure.h>

void main(int argc, char *argv[])
{
   if (argc < 2) {
      cout << "Usage: " << argv[0]
	   << " ODTable [NumVehiclesPerPair=2]"
	   << endl;
      return;
   }

   int num = 2;
   if (argc > 2) num = atoi(argv[2]);

   Reader is(argv[1]);
   double time, scale;
   int type;
   int ori, des;
   double rate;
   int total = 0;

   cout << "/*" << endl
	<< " * Testing vehicles" << endl
	<< " */" << endl;

   while (is.good() && is.peek() != '<') {
      is >> time >> type >> scale;
      if (!is.findToken('{')) exit(1);
      cout << endl << time << endl
	   << "{" << endl;
      while (is.peek() != '}') {
	 if (!is.findToken('{')) exit(1);
	 is >> ori >> des >> rate;
	 if (!is.findToken('}')) exit(1);
	 if (rate > 1.0E-4) {
	    for (int i = 0; i < num; i ++) {
	       cout << "  { " << ori << " " << des << " }" << endl;
	    }
	    total += num;
	 }
      }
      if (!is.findToken('}')) exit(1);
      cout << "}" << endl;
   }

   is.close();
  
   cout << endl << "<END>" << endl;

   cout << endl
	<< "% " << total << " vehicles generated from "
	<< argv[1] << endl
	<< "% using " << argv[0]
	<< " developed by Qi Yang." << endl;
}
