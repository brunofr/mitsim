//-*-c++-*------------------------------------------------------------
// NAME: Average links travel times
// AUTH: Qi Yang
// FILE: MSA_LinkTime.C
// DATE: Sat Jun 15 01:34:23 1996
//--------------------------------------------------------------------

#include <stdlib.h>
#include <strstream.h>

#include <IO/Exception.h>
#include <Templates/BasicMath.h>
#include <Tools/Reader.h>
#include <Tools/ToolKit.h>

#include "LinkTime.h"

const int MODE_MAX	  = 2;
const int MODE_MIN	  = 3;
const int MODE_MEAN	  = 4;
const int MODE_WEIGHT = 5;
const int MODE_MSA	  = 6;

PS_LinkMap * theLinkMap = new PS_LinkMap;

void PS_Link::read(Reader& is)
{
   is >> type_ >> length_;

   avg_.resize(theLinkMap->nCols_);
   min_.resize(theLinkMap->nCols_);
   max_.resize(theLinkMap->nCols_);

   for (register int i = 0; i < theLinkMap->nCols_; i ++) {
      is >> avg_[i];
	  min_[i] = max_[i] = avg_[i];
   }
}


void PS_Link::combine(PS_Link &p, float alpha)
{
   if (p.type_ == 0) return;
   register int i;
   if (type_ == 0) {
      type_ = p.type_;
      length_ = p.length_;
      avg_.resize(theLinkMap->nCols_);
	  min_.resize(theLinkMap->nCols_);
	  max_.resize(theLinkMap->nCols_);
      for (i = 0; i < theLinkMap->nCols_; i ++) {
         avg_[i] = p.avg_[i];
		 min_[i] = p.min_[i];
		 max_[i] = p.max_[i];
      }
   } else {
	  float beta;
	  if (alpha >= 0 && alpha <= 1.0) {
		 beta = 1.0 - alpha;
	  } else {
		 alpha = beta = 0.5;	// not used
	  }
	  for (i = 0; i < theLinkMap->nCols_; i ++) {
		 avg_[i] = beta * avg_[i] + alpha * p.avg_[i];
		 max_[i] = Max(max_[i], p.max_[i]);
		 min_[i] = Min(min_[i], p.min_[i]);
	  }
   }
}


void PS_Link::calculate(int mode)
{
   int i;
   if (mode == MODE_MAX) {
	  for (i = 0; i < theLinkMap->nCols_; i ++) {
		 avg_[i] = max_[i];
	  }
   } else if (mode == MODE_MIN) {
	  for (i = 0; i < theLinkMap->nCols_; i ++) {
		 avg_[i] = min_[i];
	  }
   } else if (mode == MODE_MEAN) {
	  for (i = 0; i < theLinkMap->nCols_; i ++) {
		 avg_[i] = 0.5 * (min_[i] + max_[i]);
	  }
   }
}


void PS_Link::print(ostream& os)
{
   float x;
   os << " " << type_ << " " << length_;
   for (register int i = 0; i < avg_.size(); i ++) {
      x = avg_[i];
      x = (int) (x * 10.0 + 0.5) / 10.0;
      os << " " << x;
   }
   os << endl;
}


void PS_LinkMap::read(Reader& is)
{
   int code;

   is >> start_ >> nCols_ >> length_;

   if (!is.findToken('{')) theException->exit();
   for (is.eatwhite(); is.peek() != '}'; is.eatwhite()) {
      is >> code;
      links_[code].read(is);
   }
   if (!is.findToken('}')) theException->exit();
}


void PS_LinkMap::combine(Reader &is, float alpha)
{
   int code, start, ncols, length;

   is >> start >> ncols >> length;

   if (start_ != start || nCols_ != ncols || length_ != length) {
      theException->exit();
   }

   if (!is.findToken('{')) theException->exit();
   for (is.eatwhite(); is.peek() != '}'; is.eatwhite()) {
      PS_Link p;
      is >> code;
      p.read(is);
      links_[code].combine(p, alpha);
   }
   if (!is.findToken('}')) theException->exit();
}


void PS_LinkMap::calculate(int mode)
{
   map<int, PS_Link, less<int> ALLOCATOR>::iterator i;
   for (i = links_.begin(); i != links_.end(); i ++) {
      (*i).second.calculate(mode);
   }
}

void PS_LinkMap::print(ostream& os)
{
   os << "% LINK TRAVEL TIMES" << endl
      << start_ << "\t% Start period" << endl
      << nCols_ << "\t% Number of periods" << endl
      << length_ << "\t% Seconds per period" << endl;

   os << "{" << endl;

   map<int, PS_Link, less<int> ALLOCATOR>::iterator i;
   for (i = links_.begin(); i != links_.end(); i ++) {
      os << "  " << (*i).first;
      (*i).second.print(os);
   }

   os << "}" << endl;
}

int usage(int mode = 0)
{
   if (mode) {
	  cout << "Usage:" << endl;
   } else {
	  cout << "This program is to be symbolically linked and" << endl
		   << "used for the following perposes:" << endl;
   }
   if (!mode || mode == MODE_MAX) {
	  cout << "  lt-max f1 f2 ...        > maximum" << endl;
   }
   if (!mode || mode == MODE_MIN) {
	  cout << "  lt-min f1 f2 ...        > minimum" << endl;
   }
   if (!mode || mode == MODE_MEAN) {
	  cout << "  lt-mean f1 f2 ...       > mean of maximum and minimum" << endl;
   }
   if (!mode || mode == MODE_WEIGHT) {
	  cout << "  lt-weight rho f1 f2 ... > weighted average" << endl;
   }
   if (!mode || mode == MODE_MSA) {
	  cout << "  lt-msa i f1 f2 ...      > successive average" << endl;
   }
   if (!mode) {
	  cout << "to generate the link travel times" << endl;
   }
   return mode;
}

int main(int argc, char *argv[])
{
   // Find what we want

   float alpha;
   int i, n;
   int mode = 0;
   const char *prog = argv[0];

   if (strstr(argv[0], "lt-max")) {
	  alpha = mode = MODE_MAX;
	  if (argc <= 2) return usage(mode);
	  i = 1;

   } else if (strstr(prog, "lt-min")) {
	  alpha = mode = MODE_MIN;
	  if (argc <= 2) return usage(mode);
	  i = 1;

   } else if (strstr(prog, "lt-mean")) {
	  alpha = mode = MODE_MEAN;
	  if (argc <= 2) return usage(mode);
	  i = 1;

   } else if (strstr(prog, "lt-weight")) {
	  mode = MODE_WEIGHT;
	  if (argc <= 3) return usage(mode);
	  alpha = atof(argv[1]);
	  i = 2;

   } else if (strstr(prog, "lt-msa")) {
	  mode = MODE_MSA;
	  if (argc <= 3) return usage(mode);
	  n = atoi(argv[1]);
	  alpha = 1.0 / (1.0 + n);
	  i = 2;

   } else {
	  return usage(0);
   }

   int k = i;

   Reader is(argv[i]);
   theLinkMap->read(is);
   is.close();

   while ((++i) < argc) {
	  is.open(argv[i]);
	  theLinkMap->combine(is, alpha);
	  is.close();
	  if (mode == MODE_MSA) {
		 alpha = 1.0 / (1.0 + n);
		 n ++;
	  }
   }

   theLinkMap->calculate(mode);
   theLinkMap->print();

   cout << endl << "% Generated from:" << endl;
   while (k < argc) {
	  cout << "%   " << argv[k] << endl;
	  k ++;
   }
   if (mode == MODE_MSA) {
	  cout << "% Start iteration " << argv[1] << endl;
   } else if (mode == MODE_WEIGHT) {
	  cout << "% Weight = " << argv[1] << endl;
   }
   cout << "% Using program " << prog << endl
		<< "% by Qi Yang" << endl
		<< "% on " << ToolKit::timeTag() << endl;

   return 0;
}
