//-*-c++-*------------------------------------------------------------
// NAME: Create link path table based on the OD path table
// AUTH: Qi Yang
// FILE: PathPointer.C
// DATE: Wed Jun  5 13:08:10 1996
//--------------------------------------------------------------------

#include <Tools/Reader.h>
#include <IO/Exception.h>

#include "PathPointer.h"

#include <string.h>
//#include <algo>

#include <stdlib.h>
#include <fstream.h>
#include <iomanip.h>

void LinkPath::print(ostream& os)
{
   os << " {" << path_ << " " << pos_ << "}";
}


void LinkPathSet::add(short int p, short int i)
{
   paths_.push_back(LinkPath(p, i));
}

void LinkPathSet::print(ostream& os)
{
   int n = paths_.size();
   os << " {";
   for (register int i = 0; i < n; i ++) {
      paths_[i].print(os);
   }
   os << " }" << endl;
}

void ODPath::read(Reader& is, int code)
{
   short int link;
   code_ = code;
   is >> ori_ >> des_;
   if (!is.findToken('{')) theException->exit();
   for (is.eatwhite(); is.peek() != '}'; is.eatwhite()) {
      is >> link;
      path_.push_back(link);
   }
   if (!is.findToken('}')) theException->exit();
   is >> cost_;
}


void ODPath::print(ostream& os)
{
   int n = path_.size();
   os << "  " << code_ << " "
      << ori_ << " "
      << des_ << " {";
   for (register int i = 0; i < n; i ++) {
      os << " " << path_[i];
   }
   os << " } " << cost_ << endl;
}


void ODPath::setLinkPointers(LinkPathTable& lpt)
{
   short int n = path_.size();
   short int k;
   for (register int i = 0; i < n; i ++) {
      k = path_[i];
      lpt[k].add(code_, i);
   }
}


ODPathTable* LoadODPathTable(char *filename)
{
   int code;
   Reader is(filename);
   ODPathTable *table = new ODPathTable;
   if (!is.findToken('{')) theException->exit();
   for (is.eatwhite(); is.peek() != '}'; is.eatwhite()) {
      is >> code;
      (*table)[code].read(is, code);
   }
   if (!is.findToken('}')) theException->exit();
   is.close();
   return table;
}

LinkPathTable * CreateLinkPathTable(ODPathTable *odp)
{
   LinkPathTable *linkp = new LinkPathTable;
   map<int, ODPath, less<int> ALLOCATOR>::iterator i;
   for (i = odp->begin(); i != odp->end(); i ++) {
      (*i).second.setLinkPointers(*linkp);
   }
   return linkp;
}

void PrintODPathTable(ODPathTable *odp, ostream& os)
{
   map<int, ODPath, less<int> ALLOCATOR>::iterator i;
   os << "{" << endl;
   for (i = odp->begin(); i != odp->end(); i ++) {
      (*i).second.print(os);
   }
   os << "}" << endl;
}


void PrintLinkPathTable(LinkPathTable *linkp, ostream& os)
{
   map<int, LinkPathSet, less<int> ALLOCATOR>::iterator i;
   os << "{" << endl;
   for (i = linkp->begin(); i != linkp->end(); i ++) {
      os << "  " << (*i).first;
      (*i).second.print(os);
   }
   os << "}" << endl;
}


int main (int argc, char *argv[])
{
   if (argc != 2) {
      cerr << "Usage: " << argv[0] << " ODPathTable" << endl;
      return 1;
   }

   ODPathTable *odp = LoadODPathTable(argv[1]);

   cout << "% Generated from " << argv[1] << endl
	<< "% Using the program " << argv[0] << " by Qi Yang" << endl;

   PrintODPathTable(odp);
   LinkPathTable *lpt = CreateLinkPathTable(odp);
   PrintLinkPathTable(lpt);

   return 0;
}
