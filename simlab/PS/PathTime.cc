//-*-c++-*------------------------------------------------------------
// NAME: Calculate path time for a given departure time
// AUTH: Qi Yang
// FILE: PathTime.C
// DATE: Wed Jun  5 13:08:10 1996
//--------------------------------------------------------------------

#include "PathShared.h"
#include "LinkShared.h"
#include <Tools/Reader.h>
#include <stdlib.h>
#include <fstream.h>

void CalcPathTravelTime(LinkTableType& linktable,
			PathTableType& pathtable,
			float depart)
{
   int id, i, n;
   PathTableType::iterator p;
   float t;
   for (p = pathtable.begin(); p != pathtable.end(); p ++) {
      Path &path = (*p).second;
      t = depart;
      for (i = 0, n = path.nLinks(); i < n; i ++) {
	 id = path.link(i);
	 t += linktable[id].cost(t);
      }
      t -= depart;
      t = (int)(10.0 * t + 0.5);
      path.cost(0.1 * t);
   }
}

int main (int argc, char *argv[])
{
   if (argc < 3 || argc > 4) {
      cerr << "Usage: " << argv[0]
	   << " LinkTimeTable PathTable"
	   << " [DepartureTime=0]" << endl;
      return 1;
   }

   LinkTableType linktable;
   Link::read(argv[1], linktable);

   PathTableType pathtable;
   Path::load(argv[2], pathtable);

   float depart;
   if (argc == 4) depart = atoi(argv[3]);
   else depart = 0.0;

   CalcPathTravelTime(linktable, pathtable, depart);

   cout << "% Departure Time = " << depart << endl << endl;

   cout << pathtable;
   
   cout << endl
	<< "% This file is generated from:" << endl
	<< "%   " << argv[1] << endl
	<< "%   " << argv[2] << endl
	<< "% Using the program " << argv[0]
	<< " developed by Qi Yang" << endl;

   return 0;
}
