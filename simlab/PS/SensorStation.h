//-*-c++-*------------------------------------------------------------
// NAME: The program compare the simulated sensor data with field data
// NOTE: 
// AUTH: Qi Yang
// FILE: SensorStation.h
// DATE: Tue May 21 15:48:24 1996
//--------------------------------------------------------------------

#ifndef SENSORSTATION_HEADER
#define SENSORSTATION_HEADER

#include <fstream.h>

class Reader;

class SensorStation
{
   public:

      SensorStation()
		: station_(0), count_(0), occupancy_(0.0),
		  speed_(0.0), num_(0) {
	  }
      ~SensorStation() {}

	  inline int	station() const { return station_; }
      inline int    count() const { return count_; }
      inline double occupancy() const { return occupancy_; }
      inline double speed() const { return speed_;  }
      inline int    num() const { return num_; }

      int addIn(int id, int c, double o, double s);
      void calc();
      void reset();
	
   private:

	  int station_;
      int count_;
      double occupancy_;
      double speed_;
      int num_;			// number of sensors w/ data
};

int LoadDataForOnePeriod(Reader& is, int n, SensorStation* s);
void CalcVarsForOnePeriod(int n, SensorStation* s);
void ResetVarsForOnePeriod(int n, SensorStation* s);

void PrintCount(ofstream& os, int n, SensorStation* s, int *l);
void PrintOccupancy(ofstream& os, int n, SensorStation* s, int *l);
void PrintSpeed(ofstream& os, int n, SensorStation* s, int *l);

#endif
