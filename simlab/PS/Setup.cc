//-*-c++-*------------------------------------------------------------
// NAME: Average simulated sensor data for multiple runs and compare
//       the result with field data
// NOTE: 
// AUTH: Qi Yang
// FILE: Setup.C
// DATE: Tue May 21 13:10:37 1996
//--------------------------------------------------------------------

#include <new.h>
#include <stdlib.h>

#include <Tools/Reader.h>
#include <Tools/ToolKit.h>

#include "Setup.h"
#include "SensorStation.h"

Setup *theSetup = NULL;

Setup::Setup(char *fname)
   : nStations(0),		// maximum num of sensor stations 
     startTime(0),		// start time in seconds
     endTime(0),		// end time in seconds
     stepSize(0),		// length of each interval
     nPeriods(0),		// number of time periods
     stnSelected(0),		// true if the station is selected
     prdSelected(0),		// true if the time period is selected
     station(0),
     nSelectedStations(0),
     nSelectedPeriods(0),

     valStation(0),		// true if the station is selected
     valPeriod(0),		// true if the period is selected
     nValStations(0),
     nValPeriods(0)
{
   Reader is(fname);		// open the setup file
   int flag;

   cout << "Loading setup file ..." << endl;

   is >> nStations;
   is >> startTime;
   is >> endTime;
   is >> stepSize;

   double num = (endTime - startTime) / stepSize;
   int inum = (int)num;
   int extra_one = (num - inum > 0.1) ? (1) : (0);
   nPeriods = inum + extra_one;

   cout << indent << "Number of stations = " << nStations << endl;
   cout << indent << "Start time = " << startTime << endl;
   cout << indent << "End time = " << endTime << endl;
   cout << indent << "Step size = " << stepSize << endl;
   cout << indent << "Number of time periods = " << nPeriods << endl;

   station = new SensorStation[nStations];
   stnSelected = new int[nStations];
   prdSelected = new int[nPeriods];

   valStation = new int[nStations];
   valPeriod = new int[nPeriods];

   // Specifies detector station

   is >> flag;
   if (flag < 0) {
      nSelectedStations = drapFromList(is, nStations, stnSelected);
   } else if (flag > 0) {
      nSelectedStations = addToList(is, nStations, stnSelected);
   } else {
      nSelectedStations = addAllToList(is, nStations, stnSelected);
   }

   // Specifies time periods

   is >> flag;
   if (flag < 0) {
      nSelectedPeriods = drapFromList(is, nPeriods, prdSelected);
   } else  if (flag > 0) {
      nSelectedPeriods = addToList(is, nPeriods, prdSelected);
   } else {
      nSelectedPeriods = addAllToList(is, nPeriods, prdSelected);
   }

   // Specifies detector station for validation statistics

   is >> flag;
   if (flag < 0) {
      nValStations = drapFromList(is, nStations, valStation);
   } else if (flag > 0) {
      nValStations = addToList(is, nStations, valStation);
   } else {
      nValStations = addAllToList(is, nStations, valStation);
   }

   // Specifies time periods for validation statistics

   is >> flag;
   if (flag < 0) {
      nValPeriods = drapFromList(is, nPeriods, valPeriod);
   } else  if (flag > 0) {
      nValPeriods = addToList(is, nPeriods, valPeriod);
   } else {
      nValPeriods = addAllToList(is, nPeriods, valPeriod);
   }

   is.close();
}


void
Setup::defaultList(int n, int* element, int defa)
{
   for (int i = 0; i < n; i ++) {
      element[i] = defa;
   }
}


int
Setup::addAllToList(Reader& is, int n, int* element)
{
   defaultList(n, element, 1);
   if (!is.findToken(OPEN_TOKEN)) exit(1);
   if (!is.findToken(CLOSE_TOKEN)) exit(1);
   return (n);
}


int
Setup::drapFromList(Reader& is, int n, int* element)
{
   int i;
   defaultList(n, element, 1);
   if (!is.findToken(OPEN_TOKEN)) exit(1);
   for (is.eatwhite();
		is.peek() != CLOSE_TOKEN;
		is.eatwhite()) {
      is >> i;				// element to drap
      if (i <= 0 || i > n) {
		 cerr << "Error:: Invalid ID <" << i << "> ";
		 is.reference();
		 exit(1);
      }
      element[i-1] = 0;
   }
   if (!is.findToken(CLOSE_TOKEN)) exit(1);

   int num = 0;
   for (i = 0; i < n; i ++) {
      if (element[i]) num ++;
   }
   return (num);
}


int
Setup::addToList(Reader& is, int n, int* element)
{
   int i;
   defaultList(n, element, 0);
   if (!is.findToken(OPEN_TOKEN)) exit(1);
   for (is.eatwhite();
		is.peek() != CLOSE_TOKEN;
		is.eatwhite()) {
      is >> i;				// element to drap
      if (i <= 0 || i > n) {
		 cerr << "Error:: Invalid ID <" << i << "> ";
		 is.reference();
		 exit(1);
      }
      element[i-1] = 1;
   }
   if (!is.findToken(CLOSE_TOKEN)) exit(1);
	
   int num = 0;
   for (i = 0; i < n; i ++) {
      if (element[i]) num ++;
   }
   return (num);
}


void
Setup::getCount(double *f)
{
   for (int i = 0; i < nStations; i ++) {
      f[i] = station[i].count();
   }
}

void
Setup::getOccupancy(double *f)
{
   for (int i = 0; i < nStations; i ++) {
      f[i] = station[i].occupancy();
   }
}

void
Setup::getSpeed(double *f)
{
   for (int i = 0; i < nStations; i ++) {
      f[i] = station[i].speed();
   }
}
