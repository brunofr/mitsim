//-*-c++-*------------------------------------------------------------
// NAME: Sort path tables by od and re-number all the paths.
// AUTH: Qi Yang
// FILE: PathSorter.C
// DATE: Wed Jun  5 13:08:10 1996
//--------------------------------------------------------------------

#include <stdio.h>
#include "PathShared.h"
#include <Tools/Reader.h>
#include <stdlib.h>

void LoadPathTable(int id, char *filename, PathTableType &table)
{
  typedef PathTableType::value_type value_type;
   char key[24];
   char title[128];
   Path *path;
   Reader is(filename);
   is.getline(title, 128);

   if (!is.findToken('{')) exit(1);
   while (is.good() && is.peek() != '}') {
     path = new Path;
     path->read(is);
     sprintf(key, "%5.5d%5.5d%5.5d.%x",
	     path->ori(), path->des(), path->code(), id);
     table.insert(value_type(string(key), *path));
     delete path;
   }
   if (!is.findToken('}')) exit(1);
   
   is.close();
}


int main (int argc, char *argv[])
{
   if (argc < 2) {
      cerr << "Usage: " << argv[0]
	   << " PathTable1 PathTable2 ..." << endl;
      return 1;
   }

   int i;
   PathTableType table;

   for (i = 1; i < argc; i ++) {
     LoadPathTable(i, argv[i], table);
   }
   
   Path::renumber(table);
   cout << table;
   
   cout << endl	<< "% Converted from: " << endl;
   for (i = 1; i < argc; i ++) {
     cout << "%   " << argv[i] << endl;
   }
   cout << "% Using the program " << argv[0]
	<< " by Qi Yang" << endl;

   return 0;
}
