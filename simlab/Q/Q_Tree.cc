//-*-c++-*------------------------------------------------------------
// Q_Tree.C
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class implementation for the class Q_Tree
//--------------------------------------------------------------------

#include <vector>
#include <stdio.h>

#include "Q_Tree.h"

long Q_Tree::theTime = 0;
vector<Pointer<Q_Tree> ALLOCATOR> Q_Tree::trees;
int Q_Tree::selected = 0;


// Append a branch to the tree

void
Q_Tree::append(Q_Branch *b)
{
   branches_.push_back(*b);
}


// Print a tree of queues

void
Q_Tree::print(ostream &os)
{
   os << convertTime(time_) << " " << head_ << " {" << endl;
   for (int i = 0; i < branches_.size(); i ++) {
	  branches_[i].print(os);
   }
   os << "} " << length_ << endl;
}

// Convert string time to double

double 
Q_Tree::convertTime(const char* t)
{
   int hh, mm;
   double ss;
   if (sscanf(t, "%d:%d:%lf", &hh, &mm, &ss) != 3) {
      cerr << "Error:: invalid time <" << t << ">" << endl;
      exit(1);
   }
   return (hh * 3600.0 + mm * 60.0 + ss);
}

const char*
Q_Tree::convertTime(long ss)
{
   static char buffer[12];
   int hh, mm;
   hh = ss / 3600;
   ss %= 3600;
   mm = ss / 60;
   ss %= 60;
   sprintf(buffer, "%2.2d:%2.2d:%2.2ld", hh, mm, ss);
   return buffer;
}


int
Q_Tree::insert(Q_Tree *tree)
{
   if (tree->head_ != selected) return 0;
   trees.push_back(Pointer<Q_Tree>(tree));
   return 1;
}

void
Q_Tree::printTrees(ostream &os)
{
   for (int i = 0; i < trees.size(); i ++) {
	  trees[i]->print(os);
   }
}
