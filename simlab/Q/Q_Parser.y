/*-*-c++-*------------------------------------------------------------
 * Q_Parser.y -- Queue parser generator
 * By Qi Yang
 *--------------------------------------------------------------------
 */

%name Q_BisonParser
%define ERROR_BODY =0
%define LEX_BODY =0

%define MEMBERS   Q_Scanner qs; Q_Tree* qt; int counts;

%define CONSTRUCTOR_PARAM const char *f

%define CONSTRUCTOR_INIT : qs(f), qt(NULL), counts(0)

   %union {
		 int    itype;
		 float  ftype;
		 char  *stype;
   }

%token <stype> Q_OB
%token <stype> Q_CB
%token <itype> Q_INT
%token <ftype> Q_REAL
%token <stype> Q_TIME

%header{
#include <fstream.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Q_Tree.h"
#include "Q_Branch.h"

#undef yyFlexLexer
#define yyFlexLexer Q_FlexLexer
#include <FlexLexer.h>

   class Q_Scanner : public Q_FlexLexer
   {
	  private:

		 char *name_;			// file name

	  public:

		 Q_Scanner() : name_(NULL), Q_FlexLexer() { }
		 Q_Scanner(const char *fname)
			: name_(strdup(fname)),
			  Q_FlexLexer() {
				 ifstream *is = new ifstream(fname);
				 if (is->good()) {
					cout << "Parsing Queues <"
						 << name_ << "> ..." << endl;
					switch_streams(is, 0);
				 } else {
					cerr << "Error:: cannot open input file <"
						 << name_ << ">." << endl;
					exit(1);
				 }
		 }
		 ~Q_Scanner() {
			free(name_);
			delete yyin;
		 }

		 char* name() const { return name_; }

		 char* value() { return (char*)YYText(); }
   };
%}

//--------------------------------------------------------------------
// Symbols used road network database grammer rules.
//--------------------------------------------------------------------

%type <stype> ob cb
%type <itype> inum

%type <stype> everything
%type <stype> multi_tables table
%type <stype> multi_trees tree
%type <stype> multi_branches branch
%type <itype> head

%start everything

%%

//--------------------------------------------------------------------
// Beginniing of Q table grammer rules.
//--------------------------------------------------------------------

everything: multi_tables
{
  cout << "# " << counts
	   << " tables parsed from <"
	   << qs.name() << ">." << endl;
}
;

multi_tables: table
| multi_tables table
;

table: time multi_trees
{
  counts ++;
}
;

multi_trees: tree
| multi_trees tree
;

tree: head ob multi_branches cb inum
{
   // $5=length

   qt->length($5);
   if (!Q_Tree::insert(qt)) {
	 delete qt;
   }
}
;

head: inum
{
   qt = new Q_Tree($1);
}
;

multi_branches: branch
| multi_branches branch
;

branch: inum inum inum
{
   // $1=from $2=to $3=length
   Q_Branch(qt, $1, $2, $3);
}
;

//--------------------------------------------------------------------
// End of Queue grammer rules.
//--------------------------------------------------------------------


//--------------------------------------------------------------------
// Beginning of basic grammer rules.
//--------------------------------------------------------------------

ob: Q_OB
;

cb: Q_CB
;

inum: Q_INT
{
   char *ptr;
   $$ = (int)strtol(qs.value(), &ptr, 0);
}
;

time: Q_TIME
{
   double t = qt->convertTime(qs.value());
   Q_Tree::setTheTime(t);
}
;

//--------------------------------------------------------------------
// End of basic grammer rules
//--------------------------------------------------------------------


%%

//--------------------------------------------------------------------
// Following pieces of cqe will be verbosely copied into the parser.
//--------------------------------------------------------------------

%header{
   class Q_Parser : public Q_BisonParser
   {
      public:
		 Q_Parser(const char *fname)
			: Q_BisonParser(fname) { }
		 ~Q_Parser() { }

		 char* name() const { return qs.name(); }

		 void yyerror(char *msg) {
			cerr << "\nError:: " << msg
				 << " (" << name() << ":"
				 << qs.lineno() << ")." << endl;
			exit(1);
		 }

		 int yylex() { return qs.yylex(); }

	     void parse() {
			yyparse();
		 }
   };
%}
