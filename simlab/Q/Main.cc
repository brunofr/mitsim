//-*-c++-*------------------------------------------------------------
// Main.C
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This program parse and average the queue output from mitsim for
// multiple replications of simulation.
//
//--------------------------------------------------------------------

#include <iostream.h>
#include <vector>
#include <stdlib.h>
#include "Q_Parser.h"
#include "Q_Tree.h"

void main(int argc, char *argv[])
{
  if (argc < 3) {
	cout << "Usage: " << argv[0]
		 << " id list_of_queue_files" << endl;
  } else {
	Q_Tree::selected = atoi(argv[1]);
  }

  for (int i = 2; i < argc; i ++) {
	Q_Parser parser(argv[i]);
	parser.parse();
  }

  Q_Tree::printTrees(cout);
}
