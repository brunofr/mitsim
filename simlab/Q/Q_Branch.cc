//-*-c++-*------------------------------------------------------------
// FILE: Q_Branch.C
// AUTH: Qi Yang
// DATE: Thu Oct 19 12:21:52 1995
//--------------------------------------------------------------------

#include "Q_Branch.h"
#include "Q_Tree.h"

// Called by the parser to insert a branch to the tree

Q_Branch::Q_Branch(Q_Tree *tree, int from, int to, int length)
  : tree_(tree),
	from_(from),
	to_(to),
	length_(length)
{
  tree->append(this);
}


// Print a branch of the tree

void
Q_Branch::print(ostream &os)
{
  os << "\t"
	 << from_ << " "
	 << to_ << " "
	 << length_ << endl;
}
