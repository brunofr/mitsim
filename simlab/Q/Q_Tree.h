//-*-c++-*------------------------------------------------------------
// Q_Tree.h
// Qi Yang
//--------------------------------------------------------------------

#ifndef Q_TREE_HEADER
#define Q_TREE_HEADER

#include <iostream.h>
#include <Templates/STL_Configure.h>
#include <Templates/Pointer.h>
#include "Q_Branch.h"

#include <vector>

class Q_Tree
{
	  friend Q_Branch;

   protected:

      long time_;
      int head_;
	  int length_;
      vector<Q_Branch ALLOCATOR> branches_;

   public:

      Q_Tree(int h) : head_(h), length_(0) {
		 time_ = theTime;
	  }
      ~Q_Tree() { }

	  void append(Q_Branch *b);
	  void length(int n) { length_ = n; }
	  void print(ostream &os = cout);

	  static void setTheTime(double t) {
		 theTime = (long) (t + 0.5);
	  }
	  static double convertTime(const char *t);
	  static const char* convertTime(long t);
	  static int insert(Q_Tree *);
	  static void printTrees(ostream &os = cout);

   public:

	  static long theTime;
	  static vector<Pointer<Q_Tree> ALLOCATOR> trees;
	  static int selected;
};

#endif
