//-*-c++-*------------------------------------------------------------
// Q_Branch.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class definition for the class Q_Branch
//--------------------------------------------------------------------

#ifndef Q_BRANCH_HEADER
#define Q_BRANCH_HEADER

#include <iostream.h>

class Q_Tree;

class Q_Branch
{
	  friend Q_Tree;

   protected:

	  Q_Tree *tree_;
	  int from_;
	  int to_;
	  int length_;

   public:

      Q_Branch() : tree_(NULL), from_(0), to_(0), length_(0) { }
	  Q_Branch(Q_Tree *t, int from, int to, int length);
      ~Q_Branch() { }
      void print(ostream &os = cout);
};

#endif
