/*-*-c++-*------------------------------------------------------------
 *  Q_Scanner.lex -- Queue lexical analyzer generator
 *--------------------------------------------------------------------
 */

%{
#include "Q_Parser.h"
#define YY_BREAK
%}

%option yyclass="Q_FlexLexer"
%option noyywrap
%option yylineno

C	(#.*)|(%.*)|("//".*)
WS	[\t ,;\n]+

D	[0-9]
H   [0-9A-Fa-f]
E	[eE][-+]?{D}+
N	{D}+
X   {H}+
F1	{D}+\.{D}*({E})?
F2	{D}*\.{D}+({E})?
F3	{D}+({E})?

HEXTAG  ("0x")|("0X")|("x")|("X")

I	[-+]?({N}|({HEXTAG}{X}))
R	[-+]?({F1}|{F2}|{F3})

HH	(([01]?[0-9])|([2][0-3]))
MM	([0-5]?[0-9])
SS	([0-5]?[0-9])
TIME	{HH}[:]{MM}[:]{SS}(.{D}+)?

OB	{WS}?[{]{WS}?
CB	{WS}?[}]{WS}?

%%

"/*"	{	/* skip comments */
  int c;
  while ((c = yyinput()) != 0) {
    if (c == '*') {
      if ((c = yyinput()) == '/') break;
      else unput(c);
    }
  }
  break;
}

{WS}	{ break; }
{C}		{ break; }

{OB}	{ return Q_Parser::Q_OB; }
{CB}	{ return Q_Parser::Q_CB; }
		            
{I}		{ return Q_Parser::Q_INT; }
{R}		{ return Q_Parser::Q_REAL; }
{TIME}  { return Q_Parser::Q_TIME; }

%%
