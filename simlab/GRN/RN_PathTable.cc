//-*-c++-*------------------------------------------------------------
// AUTH: Qi Yang
// FILE: RN_PathTable.C
// DATE: Thu Sep 26 14:21:01 1996
//--------------------------------------------------------------------

#include <algorithm>

#include <Tools/ToolKit.h>
#include "RN_Link.h"
#include "RN_PathTable.h"
#include "RN_Path.h"
#include "OD_Cell.h"
#include "RN_LinkTimes.h"
using namespace std;

// thePathTable will be initialized in the ParsePathTables() func in
// TS_Setup.C or MesoTS_Setup.C

RN_PathTable * thePathTable = NULL;

char * RN_PathTable::name_ = NULL;

RN_PathTable::~RN_PathTable()
{
   if (name_) {
	  delete [] name_;
	  name_ = NULL;
   }
}

// This may be overloaded by derived class
 
RN_Path*
RN_PathTable::newPath()
{
   return new RN_Path;
}


int
RN_PathTable::addPath(int code, int ori, int des)
{
   RN_Path *p = newPath();
   paths_.push_back(p);
   return p->init(code, ori, des);
}

RN_Path* RN_PathTable::findPath(int c)
{
   vector<RN_Path*>::iterator i;
   i = find_if(paths_.begin(), paths_.end(), CodeEqual(c));
   if (i != paths_.end()) return *i;
   else return NULL;
}

void
RN_PathTable::print(ostream &os)
{
   register int i;
   os << "// Path table generated from file <" << name_ << "> on "
      << TimeTag() << " by "
      << UserName() << endl << endl;
   os << nPaths() << indent << "// Number of paths" << endl;
   os << "{" << endl;
   for (i = 0; i < nPaths(); i ++) {
      path(i)->print(os);
   }
   os << "}" << endl;
}


void
RN_PathTable::setPathPointers()
{
   for (register int i = 0; i < nPaths(); i ++) {
      path(i)->setPathPointers();
   }
}
