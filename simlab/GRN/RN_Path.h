//-*-c++-*------------------------------------------------------------
// FILE: RN_Path.h
// DATE: Thu Oct 19 17:29:19 1995
//--------------------------------------------------------------------

#ifndef RN_PATH_HEADER
#define RN_PATH_HEADER

#include <iosfwd>
#include <vector>

#include <Templates/Object.h>

class RoadNetwork;
class RN_Link;
class RN_Node;
class RN_PathParser;
class RN_PathTable;
class RN_LinkTimes;

class RN_Path : public CodedObject
{
  friend class RN_PathParser;
  friend class RN_PathTable;
  friend class RoadNetwork;
  
protected:
  
  static int sorted_;	// 1=sorted 0=arbitrary order
  
  int index_;		// index in the array;
  std::vector<RN_Link*> links_;	// list of links of the path
  RN_Node* oriNode_;	// origin node
  RN_Node* desNode_;	// destination node
  
  // Total travel time from origin to destination. This value is
  // read from path table and not changed during the simulation.
  
  float pathTravelTime_;
  
public:
  
  RN_Path() : CodedObject() { }
  ~RN_Path() { }
  
  static inline int sorted() { return sorted_; }
  
  RN_Node * oriNode() { return oriNode_; }
  RN_Node * desNode() { return desNode_; }
  
  int oriCode();
  int desCode();
  int pathCode();

      int nLinks() { return links_.size(); }
      RN_Link* link(int i) {
		 if (i < 0) return NULL;
		 else if (i >= nLinks()) return NULL;
		 else return links_[i];
	  }
      RN_Link* nextLink(int i);
      RN_Link* firstLink() {
		if (nLinks() > 0) return links_[0];
		else return NULL;
	  }
      RN_Link* lastLink() {
		if (nLinks()) return links_[nLinks() - 1];
		else return NULL;
	  }

      int index() { return index_; }
 
	  float pathTravelTime() { return pathTravelTime_; }
	  void setPathTravelTime(float s) {  pathTravelTime_ = s; }

	  // Travel time from the kth link to the destination if entering
	  // at the calling time (currentTiem of the SimulationClock)

      float travelTime(RN_LinkTimes *info, int kth);
	  float travelTime(RN_LinkTimes *info, int kth, float t);
	  void setPathPointers();

      // These two are called by RN_PathParser

      virtual int init(int code, int ori, int des);
      virtual int addLink(int c);

      virtual void print(std::ostream &os);
};

#endif
