//-*-c++-*------------------------------------------------------------
// FILE: RN_DrawableSegment.C
// DATE: Sat Nov 11 18:15:42 1995
//--------------------------------------------------------------------

#include <Tools/Math.h>

#include "RN_Link.h"
#include "RN_Node.h"
#include "RN_DrawableSegment.h"
#include "RN_DrawableLane.h"

// These functions returns a point at position r (0=downstream
// 1=upstream), at the center or on left or right curb

WcsPoint
RN_DrawableSegment::leftPoint(float r)
{
   return intermediatePoint(r);
}


WcsPoint
RN_DrawableSegment::centerPoint(float r)
{
   WcsPoint p = intermediatePoint(r);
   double w = 0.5 * width(r);
   double a = tangentAngle(r) - HALF_PI;
   return p.bearing(w, a);
}


WcsPoint
RN_DrawableSegment::rightPoint(float r)
{
   WcsPoint p = intermediatePoint(r);
   double w = width(r);
   double a = tangentAngle(r) - HALF_PI;
   return p.bearing(w, a);;
}


// ---------------------------------------------------------------------
// Calculate the geometric data. The left curb of a segment is defined
// as the base line of the segment. Information of the base line is
// read from the network database.  This function calculates the
// widths at upstream and downstream end of segment and the lanes it
// contains.  It alse calculates the offset of the right lane mark
// from the base for each contained lane.  All these calculation takes
// into account the lane connection information and the intersection
// type.
// ---------------------------------------------------------------------

void
RN_DrawableSegment::calcWidthAndOffset()
{
   int uskip = !upstream() ||
      (link()->upNode()->geometryType() != NODE_TYPE_CENTRIOD);
   int dskip = !downstream() &&
      (link()->dnNode()->geometryType() != NODE_TYPE_CENTRIOD);

   RN_DrawableLane* plane = (RN_DrawableLane *)leftLane();

   short int uplanes, upoffset = 0;
   short int dnlanes, dnoffset = 0;

   while (plane) {		// Loop over each right right lane
      if (uskip) {
		 uplanes = 1;
      } else {
		 uplanes = Max(1, plane->nUpLanes());
      }
      if (dskip) {
		 dnlanes = 1;
      } else {
		 dnlanes = Max(1, plane->nDnLanes());
      }
      upoffset += uplanes;
      dnoffset += dnlanes;

      plane->upOffset_ = upoffset;
      plane->dnOffset_ = dnoffset;
      plane->upWidth_ = uplanes * LANE_WIDTH;
      plane->dnWidth_ = dnlanes * LANE_WIDTH;

      plane = (RN_DrawableLane *)plane->right();
   }

   upWidth_ = upoffset * LANE_WIDTH;
   dnWidth_ = dnoffset * LANE_WIDTH;
}


// Calculate the curve for each lane based on the lane connection
// information.

void
RN_DrawableSegment::calcLaneCurves()
{
   int uped, dned;
   RN_Arc *up = createUpRightArc(uped);
   RN_Arc *dn = createDnRightArc(dned);
   int ed = (uped ? 2 : 0) + (dned ? 1 : 0);
   RN_Curve curve(up, 0.0,
				  this, -upWidth_, -dnWidth_,
				  dn, 0.0,
				  ed);
   RN_DrawableLane *plane = (RN_DrawableLane *)rightLane();
   while (plane) {
      plane->RN_Curve::intrapolateInitializer(
											  (RN_Arc *)this, &curve,
											  plane->upOffset_ * LANE_WIDTH / upWidth_,
											  plane->dnOffset_ * LANE_WIDTH / dnWidth_);
      plane = (RN_DrawableLane *)plane->left();
   }
   delete up;
   delete dn;
}


// Find the right most curb arc of the segment at the upstrean end
//

RN_Arc*
RN_DrawableSegment::createUpRightArc(int& editable)
{
   RN_Link * pl;
   RN_Segment * ps;
   float offset;
   editable = 1;

   if (ps = upstream()) {	// in same link

      offset = ((RN_DrawableSegment *)ps)->dnWidth();
      return new RN_Arc((RN_Arc *)ps, -offset);

   } else if (link()->upNode()->geometryType() == NODE_TYPE_EXTERNAL) {
      
      return NULL;

   } else if (pl = link()->upRightNeighbor()) { // in different link
      if (pl->dnNode() == link()->upNode()) {
		 // same direction (shift yellow line to the right side to create
		 // a white line)
		 ps = pl->endSegment(); 
		 offset = ((RN_DrawableSegment *)ps)->dnWidth();
		 return new RN_Arc((RN_Arc *)ps, -offset);
      } else {			
		 // opposite direction (use yellow line)
		 ps = pl->startSegment();
		 editable = 0;
		 return new RN_Arc(ps->endPnt(), ps->startPnt(),
						   -ps->bulge());
      }
   }
   return NULL;		// must be network boundary
}


// Find the right most curb arc of the segment at the downstrean end

RN_Arc*
RN_DrawableSegment::createDnRightArc(int& editable)
{
   RN_Link * pl;
   RN_Segment * ps;
   float offset;
   editable = 1;

   if (ps = downstream()) {	// in same link

      offset = ((RN_DrawableSegment *)ps)->upWidth();
      return new RN_Arc((RN_Arc *)ps, -offset);

   } else if (link()->dnNode()->geometryType() == NODE_TYPE_EXTERNAL) {
      
      return NULL;

   } else if (pl = link()->dnRightNeighbor()) { // in different link

      if (pl->upNode() == link()->dnNode()) { // same direction
		 ps = pl->startSegment(); 
		 offset = ((RN_DrawableSegment *)ps)->upWidth();
		 return new RN_Arc((RN_Arc *)ps, -offset);
      } else {			// opposite direction
		 ps = pl->endSegment();
		 editable = 0;
		 return new RN_Arc(ps->endPnt(), ps->startPnt(),
						   -ps->bulge());
      }
   }
   return NULL;		// must be network boundary
}
