//-*-c++-*------------------------------------------------------------
// FILE: Parameter.C
// DATE: Thu Oct 19 12:04:41 1995
//--------------------------------------------------------------------

#include <iostream>

#include "Parameter.h"
#include "RN_CtrlStation.h"

#include <Tools/Math.h>
#include <Tools/ToolKit.h>
#include <Tools/GenericVariable.h>
#include <Tools/VariableParser.h>
#include <IO/MessageTags.h>
#include <IO/Exception.h>
using namespace std;

char * Parameter::name_ = NULL;

float Parameter::lengthFactor_	= 0.3048;	// feet
float Parameter::speedFactor_	= 0.4470;	// feet/sec
float Parameter::densityFactor_= 0.6214;	// vehicles/mile
float Parameter::flowFactor_	= 1.0000;	// vehicles/hr
float Parameter::timeFactor_	= 60.000;	// minutes
float Parameter::odFactor_     = 1.0000;     	// vehicles/hr

char * Parameter::densityLabel_ = "Density (vpm)";
char * Parameter::speedLabel_ = "Speed (mph)";
char * Parameter::flowLabel_ = "Flow (vph)";
char * Parameter::occupancyLabel_ = "Occupancy (%)";

const int NUM_RESOLUTIONS = 4;
int Parameter::resolution_[] = { 3, 4, 8, 12 };
const float DEFAULT_VISIBILITY = 80; // meters
const float DEFAULT_BUSLC_VISIBILITY = 300; // meters
const float DEFAULT_SQUEEZE_FACTOR = 1.0;

Parameter * theBaseParameter = NULL;

Parameter::Parameter()
  : GenericSwitcher(),
	pathAlpha_(0.75),
	commonalityFactor_(0.0),
	freewayBias_(0.8),
	validPathFactor_(1.5),
        rationalLinkFactor_(0.0),
	visibilityScaler_(1.0),
        visibility_(DEFAULT_VISIBILITY),
        busToStopVisibility_(DEFAULT_BUSLC_VISIBILITY),
        busStopSqueezeFactor_(DEFAULT_SQUEEZE_FACTOR)
{
  theBaseParameter = this;
}

int Parameter::error(const char *name)
{
  cerr << "Error: Obsolete or missing value(s) in parameter ["
	   << name << "]." << endl;
  return -1;
}

void Parameter::checkVisibility()
{
  float v = RN_CtrlStation::maxVisibility();
  if (v > 1.0) {
	visibility_ = v;
  } else {
	visibility_ = DEFAULT_VISIBILITY;
  }
}

// Check if two token are equal.

int
Parameter::isEqual(const char *s1, const char *s2)
{
  return IsEqual(s1, s2);
}

// Return 0 if parsed the variable.

int
Parameter::parseVariable(GenericVariable &gv)
{
  int notfound = 0; 
  char * token = compact(gv.name());
  if (isEqual(token, "NativeLengthToMeter")) {
	lengthFactor_ = gv.element();
  } else if (isEqual(token, "NativeSpeedToMetersPerSecond")) {
	speedFactor_ = gv.element();
  } else if (isEqual(token, "NativeDensityToVehiclesPerKilometer")) {
	densityFactor_ = gv.element();
  } else if (isEqual(token, "NativeFlowToVehiclesPerHour")) {
	flowFactor_ = gv.element();
  } else if (isEqual(token, "NativeTravelTimeToMinute")) {
	timeFactor_ = gv.element();  
  } else if (isEqual(token, "NativeDemandToVehiclesPerHour")) {
	odFactor_ = gv.element();	

  } else if (isEqual(token, "DensityLabel")) {
	densityLabel_ = Copy(gv.string());
  } else if (isEqual(token, "SpeedLabel")) {
	speedLabel_ = Copy(gv.string());
  } else if (isEqual(token, "FlowLabel")) {
	flowLabel_ = Copy(gv.string());
  } else if (isEqual(token, "OccupancyLabel")) {
	occupancyLabel_ = Copy(gv.string());

  } else if (isEqual(token, "Resolution")) {
	return loadResolution(gv);

  } else if (isEqual(token, "PathAlpha")) {
	pathAlpha_ = gv.element();

  } else if (isEqual(token, "CommonalityFactor")) {
	commonalityFactor_ = gv.element();

  } else if (isEqual(token, "RouteChoice")) {
	return loadRouteChoiceParas(gv);

  } else if (isEqual(token, "VisibilityScaler")) {
	visibilityScaler_ = gv.element();

  } else if (isEqual(token, "FreewayBias")) {
	freewayBias_ = gv.element();
    // YW 4/5/2006: to see what value is parsed
    cout << "YW 4/5/2006: freewayBias_ = " << freewayBias_ << '\n';
    
  } else if (isEqual(token, "DiversionPenalty")) {
	return loadDiversionPenalty(gv);
  } else if (isEqual(token, "ValidPathFactor")) {
	validPathFactor_ = gv.element();
  } else if (isEqual(token, "RationalLinkFactor")) {
        rationalLinkFactor_ = gv.element();
  } else if (isEqual(token, "BusLCVisibility")) {
        busToStopVisibility_ = gv.element();
	busToStopVisibility_ *= lengthFactor_ ;
  } else if (isEqual(token, "BusStopSqueezeFactor")) {
        busStopSqueezeFactor_ = gv.element();
  } else if (isEqual(token, "NoMsgWaitingTime")) {
	NO_MSG_WAITING_TIME = gv.element();

  } else {
	notfound = 1;
  }
  return notfound;
}


// Penalty for diversion

int
Parameter::loadDiversionPenalty(GenericVariable &gv)
{
  if (gv.nElements() != 2) return error(gv.name());

  diversionPenalty_ = new float [2];

  diversionPenalty_[0] = gv.element(0);
  diversionPenalty_[1] = gv.element(1);

  diversionPenalty_[0] *= 60.0;	// covert to seconds
  diversionPenalty_[1] *= 60.0;	// covert to seconds

  return 0;
}


// These defines the resolution thresholds used for viewing various
// graphical objects

int
Parameter::loadResolution(GenericVariable &gv)
{
  int i, n = gv.nElements();
  if (n < NUM_RESOLUTIONS) return error(gv.name());
  for (i = 0; i < n; i ++) {
	resolution_[i] = gv.element(i);
  }
  return 0;
}


/*
 * Read parameters in the route choice model
 */

int
Parameter::loadRouteChoiceParas(GenericVariable &gv)
{
  const int nparams = 2;
  int i, n = gv.nElements() / nparams;
  if (n != 2) return error(gv.name());
  for (i = 0; i < n; i ++) {
	int k = i * nparams;
	routingParams_[i] = new float[nparams];
	routingParams_[i][0] = gv.element(k + 0);
	routingParams_[i][1] = gv.element(k + 1);
  }
  return 0;
}
