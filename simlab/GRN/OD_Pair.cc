//-*-c++-*------------------------------------------------------------
// NAME: Traffic simulator
// NOTE: 
// AUTH: Qi Yang
// FILE: OD_Pair.C
// DATE: Mon Aug 12 13:46:33 1996
//--------------------------------------------------------------------


#include "OD_Pair.h"
#include "RN_Node.h"
using namespace std;

OdPairSetType theOdPairs;

OD_Pair::OD_Pair(RN_Node *o, RN_Node *d)
   : oriNode_(o),
     desNode_(d)
{
}

OD_Pair::OD_Pair(OD_Pair &od)
   : oriNode_(od.oriNode_),
     desNode_(od.desNode_)
{
}

OD_Pair::OD_Pair(const OD_Pair &od)
   : oriNode_(od.oriNode_),
     desNode_(od.desNode_)
{
}

OD_Pair&
OD_Pair::operator=(const OD_Pair &od)
{
   oriNode_ = od.oriNode_;
   desNode_ = od.desNode_;
   return *this;
}

bool
OD_Pair::operator==(const OD_Pair &od)
{
   return (oriNode_ == od.oriNode_ && desNode_ == od.desNode_);
}

bool
OD_Pair::operator<(const OD_Pair &od)
{
   if (oriNode_ < od.oriNode_) return true;
   else if (oriNode_ > od.oriNode_) return false;
   else if (desNode_ < od.desNode_) return true;
   else return false;
}


int
OD_Pair::ori()
{
   return oriNode_->code();
}


int
OD_Pair::des()
{
   return desNode_->code();
}
