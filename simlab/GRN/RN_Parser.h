#ifndef YY_RN_Parser_h_included
#define YY_RN_Parser_h_included

#line 1 "/usr/lib/bison.h"
/* before anything */
#ifdef c_plusplus
#ifndef __cplusplus
#define __cplusplus
#endif
#endif
#ifdef __cplusplus
#ifndef YY_USE_CLASS
#define YY_USE_CLASS
#endif
#else
#endif
#include <stdio.h>

/* #line 14 "/usr/lib/bison.h" */
#line 21 "RN_Parser.h"
#define YY_RN_Parser_ERROR_BODY 	{ _scanner.check(-1, "Parser error") ; }
#define YY_RN_Parser_LEX_BODY 	  { return _scanner.yylex() ; }
#define YY_RN_Parser_CONSTRUCTOR_PARAM  RoadNetwork* pContainer_
#define YY_RN_Parser_CONSTRUCTOR_INIT   : _pContainer(pContainer_)
#define YY_RN_Parser_MEMBERS                                                \
    RoadNetwork* _pContainer ;                                \
    RN_IS* ISptr;                                              \
    CManuver* mnvptr;                                          \
    private:                                                  \
    RN_Scanner _scanner ;                                     \
    public:                                                   \
    RN_Scanner& scanner() { return _scanner ; }               \
    int lineno() { return _scanner.lineno() ; }               \
    void check(int err_no, const char* szMsg_ = 0) {          \
      _scanner.check(err_no, szMsg_) ;                        \
    }                                                         \
    const char* text() { return _scanner.text() ; }           \
    const char* value(const char* delis_ = 0) {               \
      return _scanner.value(delis_) ;                         \
    }                                                         \
    const char* filename() { return _scanner.filename() ; }   \
    void parse(const char* szFilename_) {                     \
      if (szFilename_ && _scanner.open(szFilename_)) {        \
        yyparse() ;                                           \
      } else if (_scanner.good()) {                           \
        yyparse() ;                                           \
      }                                                       \
    }                                                         \


#line 41 "RN_Parser.y"
typedef union {
  long        type_i ;
  double      type_f ;
  const char* type_s ;
} yy_RN_Parser_stype;
#define YY_RN_Parser_STYPE yy_RN_Parser_stype
#line 74 "RN_Parser.y"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>

using namespace std;

#include <IO/Exception.h>
#include <Tools/ToolKit.h>  
#include <UTL/Misc.h>  
#include <GRN/RoadNetwork.h>
#include <GRN/RN_Node.h>
#include <GRN/RN_Link.h>
#include <GRN/RN_Segment.h>
#include <GRN/RN_Lane.h>
#include <GRN/RN_TollBooth.h>
#include <GRN/RN_BusStop.h>
#include <GRN/RN_SdFn.h>
#include <GRN/RN_Label.h>
#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>
#include <GRN/RN_IS.h>

#undef yyFlexLexer
#define yyFlexLexer RNSFlexLexer
#include <FlexLexer.h>

class RN_Scanner : public RNSFlexLexer
{
 public:

  RN_Scanner(istream* yyin_ = 0, ostream* yyout_ = 0)
      : RNSFlexLexer(yyin_, yyout_), _szFilename(NULL), _pis(yyin_) { }

  virtual ~RN_Scanner() {  close() ; }

  int yylex() { return RNSFlexLexer::yylex() ; }

  void check(int err_no, const char* szMsg_ = NULL) ;
  const char* text() ;
  const char* value(const char* delis_ = 0) ;
  const char* filename() { return _szFilename ; }
  bool open(const char* szFilename_) ;
  bool close() ;
  bool good() ;

 protected:

  // Redefine the virtual function in yyFlexLexer

  void LexerError(const char* szMsg_) { check(-1, szMsg_) ; }

 private:

  const char* _szFilename ;
  istream* _pis ;
} ;


#line 14 "/usr/lib/bison.h"
 /* %{ and %header{ and %union, during decl */
#ifndef YY_RN_Parser_COMPATIBILITY
#ifndef YY_USE_CLASS
#define  YY_RN_Parser_COMPATIBILITY 1
#else
#define  YY_RN_Parser_COMPATIBILITY 0
#endif
#endif

#if YY_RN_Parser_COMPATIBILITY != 0
/* backward compatibility */
#ifdef YYLTYPE
#ifndef YY_RN_Parser_LTYPE
#define YY_RN_Parser_LTYPE YYLTYPE
/* WARNING obsolete !!! user defined YYLTYPE not reported into generated header */
/* use %define LTYPE */
#endif
#endif
#ifdef YYSTYPE
#ifndef YY_RN_Parser_STYPE 
#define YY_RN_Parser_STYPE YYSTYPE
/* WARNING obsolete !!! user defined YYSTYPE not reported into generated header */
/* use %define STYPE */
#endif
#endif
#ifdef YYDEBUG
#ifndef YY_RN_Parser_DEBUG
#define  YY_RN_Parser_DEBUG YYDEBUG
/* WARNING obsolete !!! user defined YYDEBUG not reported into generated header */
/* use %define DEBUG */
#endif
#endif
#ifdef YY_RN_Parser_STYPE
#ifndef yystype
#define yystype YY_RN_Parser_STYPE
#endif
#endif
/* use goto to be compatible */
#ifndef YY_RN_Parser_USE_GOTO
#define YY_RN_Parser_USE_GOTO 1
#endif
#endif

/* use no goto to be clean in C++ */
#ifndef YY_RN_Parser_USE_GOTO
#define YY_RN_Parser_USE_GOTO 0
#endif

#ifndef YY_RN_Parser_PURE

/* #line 63 "/usr/lib/bison.h" */
#line 172 "RN_Parser.h"

#line 63 "/usr/lib/bison.h"
/* YY_RN_Parser_PURE */
#endif

/* #line 65 "/usr/lib/bison.h" */
#line 179 "RN_Parser.h"

#line 65 "/usr/lib/bison.h"
/* prefix */
#ifndef YY_RN_Parser_DEBUG

/* #line 67 "/usr/lib/bison.h" */
#line 186 "RN_Parser.h"

#line 67 "/usr/lib/bison.h"
/* YY_RN_Parser_DEBUG */
#endif
#ifndef YY_RN_Parser_LSP_NEEDED

/* #line 70 "/usr/lib/bison.h" */
#line 194 "RN_Parser.h"

#line 70 "/usr/lib/bison.h"
 /* YY_RN_Parser_LSP_NEEDED*/
#endif
/* DEFAULT LTYPE*/
#ifdef YY_RN_Parser_LSP_NEEDED
#ifndef YY_RN_Parser_LTYPE
typedef
  struct yyltype
    {
      int timestamp;
      int first_line;
      int first_column;
      int last_line;
      int last_column;
      char *text;
   }
  yyltype;

#define YY_RN_Parser_LTYPE yyltype
#endif
#endif
/* DEFAULT STYPE*/
#ifndef YY_RN_Parser_STYPE
#define YY_RN_Parser_STYPE int
#endif
/* DEFAULT MISCELANEOUS */
#ifndef YY_RN_Parser_PARSE
#define YY_RN_Parser_PARSE yyparse
#endif
#ifndef YY_RN_Parser_LEX
#define YY_RN_Parser_LEX yylex
#endif
#ifndef YY_RN_Parser_LVAL
#define YY_RN_Parser_LVAL yylval
#endif
#ifndef YY_RN_Parser_LLOC
#define YY_RN_Parser_LLOC yylloc
#endif
#ifndef YY_RN_Parser_CHAR
#define YY_RN_Parser_CHAR yychar
#endif
#ifndef YY_RN_Parser_NERRS
#define YY_RN_Parser_NERRS yynerrs
#endif
#ifndef YY_RN_Parser_DEBUG_FLAG
#define YY_RN_Parser_DEBUG_FLAG yydebug
#endif
#ifndef YY_RN_Parser_ERROR
#define YY_RN_Parser_ERROR yyerror
#endif

#ifndef YY_RN_Parser_PARSE_PARAM
#ifndef __STDC__
#ifndef __cplusplus
#ifndef YY_USE_CLASS
#define YY_RN_Parser_PARSE_PARAM
#ifndef YY_RN_Parser_PARSE_PARAM_DEF
#define YY_RN_Parser_PARSE_PARAM_DEF
#endif
#endif
#endif
#endif
#ifndef YY_RN_Parser_PARSE_PARAM
#define YY_RN_Parser_PARSE_PARAM void
#endif
#endif

/* TOKEN C */
#ifndef YY_USE_CLASS

#ifndef YY_RN_Parser_PURE
extern YY_RN_Parser_STYPE YY_RN_Parser_LVAL;
#endif


/* #line 143 "/usr/lib/bison.h" */
#line 272 "RN_Parser.h"
#define	RN_COL	258
#define	RN_OCB	259
#define	RN_CCB	260
#define	RN_INT	261
#define	RN_REAL	262
#define	RN_TIME	263
#define	RN_PAIR	264
#define	RN_STRING	265
#define	RN_END	266
#define	RN_HIPHEN	267
#define	RN_TITLE	268
#define	RN_DRIVINGDIRECTION	269
#define	RN_LABELS	270
#define	RN_NODES	271
#define	RN_LINKS	272
#define	RN_CONNECTORS	273
#define	RN_PROHIBITORS	274
#define	RN_SENSORS	275
#define	RN_SIGNALS	276
#define	RN_TOLLS	277
#define	RN_STOPS	278
#define	RN_SDFNS	279
#define	RN_INTERSECTS	280
#define	RN_ISNODE	281


#line 143 "/usr/lib/bison.h"
 /* #defines token */
/* after #define tokens, before const tokens S5*/
#else
#ifndef YY_RN_Parser_CLASS
#define YY_RN_Parser_CLASS RN_Parser
#endif

#ifndef YY_RN_Parser_INHERIT
#define YY_RN_Parser_INHERIT
#endif
#ifndef YY_RN_Parser_MEMBERS
#define YY_RN_Parser_MEMBERS 
#endif
#ifndef YY_RN_Parser_LEX_BODY
#define YY_RN_Parser_LEX_BODY  
#endif
#ifndef YY_RN_Parser_ERROR_BODY
#define YY_RN_Parser_ERROR_BODY  
#endif
#ifndef YY_RN_Parser_CONSTRUCTOR_PARAM
#define YY_RN_Parser_CONSTRUCTOR_PARAM
#endif
/* choose between enum and const */
#ifndef YY_RN_Parser_USE_CONST_TOKEN
#define YY_RN_Parser_USE_CONST_TOKEN 0
/* yes enum is more compatible with flex,  */
/* so by default we use it */ 
#endif
#if YY_RN_Parser_USE_CONST_TOKEN != 0
#ifndef YY_RN_Parser_ENUM_TOKEN
#define YY_RN_Parser_ENUM_TOKEN yy_RN_Parser_enum_token
#endif
#endif

class YY_RN_Parser_CLASS YY_RN_Parser_INHERIT
{
public: 
#if YY_RN_Parser_USE_CONST_TOKEN != 0
/* static const int token ... */

/* #line 182 "/usr/lib/bison.h" */
#line 341 "RN_Parser.h"
static const int RN_COL;
static const int RN_OCB;
static const int RN_CCB;
static const int RN_INT;
static const int RN_REAL;
static const int RN_TIME;
static const int RN_PAIR;
static const int RN_STRING;
static const int RN_END;
static const int RN_HIPHEN;
static const int RN_TITLE;
static const int RN_DRIVINGDIRECTION;
static const int RN_LABELS;
static const int RN_NODES;
static const int RN_LINKS;
static const int RN_CONNECTORS;
static const int RN_PROHIBITORS;
static const int RN_SENSORS;
static const int RN_SIGNALS;
static const int RN_TOLLS;
static const int RN_STOPS;
static const int RN_SDFNS;
static const int RN_INTERSECTS;
static const int RN_ISNODE;


#line 182 "/usr/lib/bison.h"
 /* decl const */
#else
enum YY_RN_Parser_ENUM_TOKEN { YY_RN_Parser_NULL_TOKEN=0

/* #line 185 "/usr/lib/bison.h" */
#line 374 "RN_Parser.h"
	,RN_COL=258
	,RN_OCB=259
	,RN_CCB=260
	,RN_INT=261
	,RN_REAL=262
	,RN_TIME=263
	,RN_PAIR=264
	,RN_STRING=265
	,RN_END=266
	,RN_HIPHEN=267
	,RN_TITLE=268
	,RN_DRIVINGDIRECTION=269
	,RN_LABELS=270
	,RN_NODES=271
	,RN_LINKS=272
	,RN_CONNECTORS=273
	,RN_PROHIBITORS=274
	,RN_SENSORS=275
	,RN_SIGNALS=276
	,RN_TOLLS=277
	,RN_STOPS=278
	,RN_SDFNS=279
	,RN_INTERSECTS=280
	,RN_ISNODE=281


#line 185 "/usr/lib/bison.h"
 /* enum token */
     }; /* end of enum declaration */
#endif
public:
 int YY_RN_Parser_PARSE(YY_RN_Parser_PARSE_PARAM);
 virtual void YY_RN_Parser_ERROR(char *msg) YY_RN_Parser_ERROR_BODY;
#ifdef YY_RN_Parser_PURE
#ifdef YY_RN_Parser_LSP_NEEDED
 virtual int  YY_RN_Parser_LEX(YY_RN_Parser_STYPE *YY_RN_Parser_LVAL,YY_RN_Parser_LTYPE *YY_RN_Parser_LLOC) YY_RN_Parser_LEX_BODY;
#else
 virtual int  YY_RN_Parser_LEX(YY_RN_Parser_STYPE *YY_RN_Parser_LVAL) YY_RN_Parser_LEX_BODY;
#endif
#else
 virtual int YY_RN_Parser_LEX() YY_RN_Parser_LEX_BODY;
 YY_RN_Parser_STYPE YY_RN_Parser_LVAL;
#ifdef YY_RN_Parser_LSP_NEEDED
 YY_RN_Parser_LTYPE YY_RN_Parser_LLOC;
#endif
 int YY_RN_Parser_NERRS;
 int YY_RN_Parser_CHAR;
#endif
#if YY_RN_Parser_DEBUG != 0
public:
 int YY_RN_Parser_DEBUG_FLAG;	/*  nonzero means print parse trace	*/
#endif
public:
 YY_RN_Parser_CLASS(YY_RN_Parser_CONSTRUCTOR_PARAM);
public:
 YY_RN_Parser_MEMBERS 
};
/* other declare folow */
#endif


#if YY_RN_Parser_COMPATIBILITY != 0
/* backward compatibility */
#ifndef YYSTYPE
#define YYSTYPE YY_RN_Parser_STYPE
#endif

#ifndef YYLTYPE
#define YYLTYPE YY_RN_Parser_LTYPE
#endif
#ifndef YYDEBUG
#ifdef YY_RN_Parser_DEBUG 
#define YYDEBUG YY_RN_Parser_DEBUG
#endif
#endif

#endif
/* END */

/* #line 236 "/usr/lib/bison.h" */
#line 455 "RN_Parser.h"

#line 912 "RN_Parser.y"

  // Empty
#endif
