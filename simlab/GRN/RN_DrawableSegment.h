//-*-c++-*------------------------------------------------------------
// FILE: RN_DrawableSegment.h
// DATE: Sat Nov 11 18:15:16 1995
//--------------------------------------------------------------------

#ifndef RN_SHAPEDSEGMENT_HEADER
#define RN_SHAPEDSEGMENT_HEADER

#include "RN_Segment.h"
#include "WcsPoint.h"

// This class is derived from RN_Segment and provides information
// needed for drawing the segment.  If you need graphics support,
// inherent your Segment from RN_DrawableSegment; otherwise use
// RN_Segment would be more efficient.


class RN_DrawableSegment : public RN_Segment
{
   protected:

      float upWidth_;		// upstream width
      float dnWidth_;		// downstream width

   public:

      RN_DrawableSegment() : RN_Segment() { }
      virtual ~RN_DrawableSegment() { }


      inline float upWidth() { return upWidth_;}
      inline float dnWidth() { return dnWidth_;}

      // Segment width at intermediate point. r=1 is upWidth and r=0
      // is the dnWidth.

      inline float width(float r = 0.5) {
         return (r * upWidth_ + (1.0 - r) * dnWidth_);
      }

      // These functions returns a point at position r (0=downstream
      // 1=upstream).

      WcsPoint leftPoint(float r = 0.5);
      WcsPoint centerPoint(float r = 0.5);
      WcsPoint rightPoint(float r = 0.5);

      void calcWidthAndOffset();
      void calcLaneCurves();

      // Create the right most curb arc of the segment at the upstream
      // end. The caller is responsible of release the arc when it is
      // no longer useful

      RN_Arc* createUpRightArc(int &editable);
      RN_Arc* createDnRightArc(int &editable);
};


#endif
