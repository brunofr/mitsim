//-*-c++-*------------------------------------------------------------
// NAME: A parser for bus assignment table
// AUTH: Qi Yang & Daniel Morgan
// FILE: BusAssignmentTable.h
// DATE: Fri Nov 30 16:55:50 2001
//--------------------------------------------------------------------

#ifndef BUSASSIGNMENTTABLE_HEADER
#define BUSASSIGNMENTTABLE_HEADER

#include <iostream>
#include <vector>

#include "RN_Vehicle.h"
#include "BusAssignment.h"
#include "RN_BusSchedule.h"

const int NUM_ASSIGNMENTS = 100;	// Default initial num of assignments

class BusAssignmentParser;
class BusAssignmentBisonParser;
class RN_Vehicle;

class BusAssignmentTable
{
      friend class BusAssignmentBisonParser;
      friend class BusAssignmentParser;
      friend class RN_Vehicle;

   protected:

      static char * name_;		// file name
      int BRTOnly_;                     // 0 = no assignment file, just bus rapid transit in od.dat, 
                                        // 1 = bus assignment file exists
      double startTime_;		// dep time smaller than this is skipped
      double nextTime_;			// time to read od pairs
      std::vector <BusAssignment*> assignments_;  // array of pointers to bus assignments

      // Dan: the following are for bus sensors
      int minLoadThresh_;       // minimum bus load, below which priority will NOT be activated
      float maxAllowSchedDev_;  // minimum deviation from schedule, below which priority will NOT be activated
      float maxAllowHW_;        // minimum headway, above which priority will be activated

   public:

      BusAssignmentTable();

      virtual ~BusAssignmentTable() {}
	
      void open(double start_time = -86400.0);
      double read();

      static inline char* name() { return name_; }
      static inline char** nameptr() { return &name_; }

      int BRTOnly() { return BRTOnly_; }
      void setBRTOnly() { BRTOnly_ = 0; }      

      inline double startTime() { return startTime_; }
	  inline double nextTime() { return nextTime_; }

	  inline int skip() {
		return (nextTime_ < startTime_);
	  }

      int nAssignments() { return assignments_.size(); }

      BusAssignment* assignment(int i) {
		         if (i < 0) return NULL;
		         else if (i >= nAssignments()) return NULL;
		         else return assignments_[i];
	  }
      BusAssignment* nextAssignment(int i);
      BusAssignment* firstAssignment() {
		         if (nAssignments() > 0) return assignments_[0];
		         else return NULL;
	  }
      BusAssignment* finalAssignment() {
		         if (nAssignments()) return assignments_[nAssignments() - 1];
	               	 else return NULL;
	  }

      virtual int initBus(double start);

      // These may has to be overloaded by derived class

      virtual RN_Vehicle* newVehicle() = 0;

      virtual BusAssignment* newBusAssignment();

      virtual int addBusAssignment(int code, int type, int run);
      virtual int addBRTAssignment(int code, int type, int run, double hw);
      virtual int addBusAssignment(int code, int type, int run, int load);

      inline BusAssignment* lastAssignment() { return assignments_.back(); }

      BusAssignment* findBusAssignment(int c);

      virtual void print(std::ostream &os = std::cout);

   private:

      BusAssignmentParser* parser_; // the parser
      int nBusesParsed_;	// buses parsed in this round
};

extern BusAssignmentTable * theBusAssignmentTable;

#endif
