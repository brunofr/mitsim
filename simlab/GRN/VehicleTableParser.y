//
// VehicleTableParser.y
// by Qi Yang
//

%name   VehicleTableParser
%define ERROR_BODY	DEF_ERROR_BODY
%define LEX_BODY	DEF_LEX_BODY

%define CONSTRUCTOR_PARAM  VehicleTable* pContainer_
%define CONSTRUCTOR_INIT   : _pContainer(pContainer_)   \
                           ,_current(NULL)              \
                           ,_vid(0)                     \
                           ,_type(0)                    \

%define MEMBERS  DEF_MEMBERS                            \
   VehicleTable* _pContainer ;                          \
   RN_Vehicle* _current ;                               \
   int _vid ;                                           \
   int _type ;                                          \

%union {
  long        type_i ;
  double      type_f ;
  const char* type_s ;
}

%token <type_s> LEX_EQUAL
%token <type_s> LEX_COMMA
%token <type_s> LEX_COLON
%token <type_s> LEX_SEMICOLON
%token <type_s> LEX_OAB
%token <type_s> LEX_CAB
%token <type_s> LEX_OBB
%token <type_s> LEX_CBB
%token <type_s> LEX_OCB
%token <type_s> LEX_CCB
%token <type_i> LEX_INT
%token <type_f> LEX_REAL
%token <type_s> LEX_TIME
%token <type_s> LEX_PAIR
%token <type_s> LEX_TAG
%token <type_s> LEX_STRING
%token <type_s> LEX_DUP
%token <type_s> LEX_END

%header{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <IO/Exception.h>
#include <Tools/ToolKit.h>  
#include <Tools/Scanner.h>  
#include <Tools/SimulationClock.h>
#include <Tools/Math.h>

#include <GRN/VehicleTable.h>
#include <GRN/RN_Vehicle.h>
#include <GRN/RN_Path.h>
#include <GRN/RN_Node.h>
%}

//--------------------------------------------------------------------
// Symbols used vehicle table grammer rules.
//--------------------------------------------------------------------

%type <type_f> fnum float_num time string_time
%type <type_s> everything multi_vehicle_tables vehicle_list
%type <type_s> vehicle_table vehicle_table_data
%type <type_s> vid vehicle_without_id one_vehicle
%type <type_s> multi_vehicles vehicles several_vehicles the_end
%type <type_i> inum cnt vehicle_data_2 vehicle_data_3 vehicle_data_4

%start everything

%%

//--------------------------------------------------------------------
// Beginniing of VehicleTable table grammer rules.
//--------------------------------------------------------------------

everything: multi_vehicle_tables the_end
| vehicle_list the_end
| vehicle_list multi_vehicle_tables the_end
;

the_end: LEX_END
{
  _pContainer->init(DBL_INF);
  if (ToolKit::verbose()) {
	cout << "Finished parsing <" << filename() << ">." << endl;
  }
  YYACCEPT;			// return and never back
}
;

multi_vehicle_tables: vehicle_table
| multi_vehicle_tables vehicle_table
;

vehicle_table: vehicle_table_data vehicle_list
;

vehicle_list: LEX_OCB multi_vehicles LEX_CCB
;

vehicle_table_data: time
{
  // $1=DepartuteTime
  int err_no = _pContainer->init($1); 
  check(err_no);
  if (_pContainer->nextTime() > theSimulationClock->currentTime()) {
	YYACCEPT;					// return and will come back later
  }
}
;

multi_vehicles: vehicles
| multi_vehicles vehicles
;

vehicles: several_vehicles | one_vehicle
{
  if (ToolKit::debug()) {
	cout << endl;
  }
  _current = NULL;
}
;

several_vehicles: cnt LEX_DUP vehicle_without_id
{
  if (_current) {
	for (int i = 1; i < $1; i ++) {
	  int p;
	  if (_current->path()) {
		p = _current->path()->code();
	  } else {
		p = 0;
	  }
	  RN_Vehicle *ptr = _pContainer->newVehicle();
	  int err_no = ptr->init(_vid,
							_current->oriNode()->code(),
							_current->desNode()->code(),
							_type, p);
	  if (err_no != 0) delete ptr;
	}
  }
}
;

one_vehicle: vehicle_without_id
| vid vehicle_without_id
;

cnt: inum
{
  if (ToolKit::debug()) {
	cout << $1 << endc << "*" << endc;
  }
  _vid = 0;						// set id
  $$ = $1;
}
;

vid: inum
{
  if (ToolKit::debug()) {
	cout << $1 << endc;
  }
  _vid = $1;					// set id
}
;

vehicle_without_id: LEX_OCB vehicle_data_2 LEX_CCB
| LEX_OCB vehicle_data_3 LEX_CCB
| LEX_OCB vehicle_data_4 LEX_CCB
{
  _vid = 0 ;					// clear id
  check($2) ;
}
;

vehicle_data_4: inum inum inum inum
{
  if (ToolKit::debug()) {
	cout << '{'
		 << $1 << endc << $2 << endc
		 << $3 << endc << $4 << '}';
  }

  if (_pContainer->skip()) {
	_current = NULL;
  } else {
	// $1=Ori $2=Des $3=Type $4=Path 
	_current = _pContainer->newVehicle();
	_type = $3;
	$$ = _current->init(_vid, $1, $2, $3, $4);
	if ($$ != 0) {
	  delete _current;
	  _current = NULL;
	}
  }
}
;

vehicle_data_3: inum inum inum
{
  if (ToolKit::debug()) {
	cout << '{' << $1 << endc << $2 << endc << $3 << '}';
  }

  if (_pContainer->skip()) {
	_current = NULL;
  } else {
	// $1=Ori $2=Des $3=Type 
	_current = _pContainer->newVehicle();
	_type = $3;
	$$ = _current->init(_vid, $1, $2, $3);
	if ($$ != 0) {
	  delete _current;
	  _current = NULL;
	}
  }
}
;

vehicle_data_2: inum inum
{
  if (ToolKit::debug()) {
	cout << '{' << $1 << endc << $2 << '}';
  }

  if (_pContainer->skip()) {
	_current = NULL;
  } else {
	// $1=Ori $2=Des
	_current = _pContainer->newVehicle();
	_type = 0;
	$$ = _current->init(_vid, $1, $2, _type);
	if ($$ != 0) {
	  delete _current;
	  _current = NULL;
	}
  }
}
;

//--------------------------------------------------------------------
// Beginning of basic grammer rules.
//--------------------------------------------------------------------

inum: LEX_INT
{
  char *ptr;
  $$ = (int)strtol(value(), &ptr, 0);
}
;

fnum: float_num
| inum
{
  $$ = $1;
}
;

float_num: LEX_REAL
{
  $$ = atof(value());
}
;

time: string_time
| fnum
{
  $$ = $1;
}
;

string_time: LEX_TIME
{
  $$ = theSimulationClock->convertTime(value());
}
;

%%

// Following pieces of code will be verbosely copied into the parser.

%header{
  // empty
%}
