//-*-c++-*------------------------------------------------------------
// NAME: Route choice
// NOTE: Stores time invariant routing information
// AUTH: Qi Yang
// FILE: RN_Route.C
// DATE: Tue Sep 24 09:27:15 1996
//--------------------------------------------------------------------

#include <iostream>
#include <new>
#include <sys/types.h>
#include <sys/stat.h>

#include <Tools/Math.h>
#include <IO/Exception.h>
#include <Tools/SimulationClock.h>
#include <Tools/SimulationEngine.h>
#include <Tools/ToolKit.h>

#include "RoadNetwork.h"

#include "RN_Node.h"
#include "RN_Link.h"
#include "RN_Route.h"
#include "RN_LinkGraph.h"
#include "RN_PathTable.h"
#include "BusAssignmentTable.h"
#include "RN_LinkTimes.h"
using namespace std;

// By default it is called once with time invariant costs

int theSpFlag = 0x0;

RN_Route* theGuidedRoute	= NULL;
RN_Route* theUnGuidedRoute	= NULL;

RN_LinkGraph* RN_Route::linkGraph_ = NULL;

Graph<float, RN_LinkTimes> *
RN_Route::graph()
{
    return linkGraph_->graph();
}


RN_Route::RN_Route(const char *filename)
    : RN_LinkTimes(filename),
      state_(0)
{
}

// Call this function once, but do not call it in the ctor.

void
RN_Route::initialize(char tag)
{
    static char timefile[] = "spt_ .tmp";
    const char *wf = NULL;
    timefile[4] = tag;

    matrixSize_ = nLinks() * nDestNodes();
    size_t size = infoPeriods() * matrixSize_;

    int num_time_tag;
    time_t time_tags[2];
    time_t *the_time_tags;
    struct stat fileinfo;
    const char *fname;
   
    // Find the time that network was modified

    fname = ToolKit::optionalInputFile(theNetwork->name());
    if (::stat(fname, &fileinfo) != 0) goto error;
    time_tags[0] = fileinfo.st_mtime;

    // Find the time that the link time table was modified

    if (filename_) {
        fname = filename_;
        if (::stat(fname, &fileinfo) != 0) goto error;
        time_tags[1] = fileinfo.st_mtime;
    } else {
        time_tags[1] = 0;
    }

    // Chech if the existing tables should be used, this saves
    // computational time in the path based simulation where shortest
    // path table is only used for these vehicles that do not have
    // paths assigned.

    if (isSpFlag(INFO_FLAG_USE_EXISTING_TABLES)) {
        num_time_tag = 0;
        the_time_tags = NULL;
    } else {
        num_time_tag = 2;
        the_time_tags = time_tags;
    }

    wf = ToolKit::wfile(timefile);
    if (routeTimes_.open(wf, size, num_time_tag, the_time_tags) != 0 ) {
   
        state_ = 0;				//  new table

        // Create the space for storing the shortest path infomation

        long int kb;

        kb = size * sizeof(float) / 1024;
        if (kb) {
            const char *s;
            s = Str("Creating travel time table %s (%ld KB).", wf, kb);
            theNetwork->updateProgress(s);
        }
        routeTimes_.create(wf, FLT_INF, size, 2, time_tags);

        SimulationClock *clock = theSimulationClock;
        findShortestPathTrees(clock->startTime(), clock->stopTime());

    } else {

        state_ = 1;				// existing table
        const char *s;
        s = Str("Linked to routing table %s.", wf);
        theNetwork->updateProgress(s);
    }

    return;

 error:

    cerr << "Error:: stat() failed obtaining information on <"
         << fname << ">." << endl;

    theException->exit(1);
}


RN_Route::RN_Route(const RN_Route& sp)
    : RN_LinkTimes(sp)
{
    *this = sp ;
}

RN_Route&
RN_Route::operator=(const RN_Route& sp) 
{
    RN_LinkTimes::operator=(sp) ;
    matrixSize_ = sp.matrixSize_;
    if (sp.state_ == 0) {		// sp is a new table
        state_ = 0;
    } else {						// source is an existing table

        theNetwork->updateProgress("Checking routing tables ...");
        char *timefile = StrCopy("%s.1", sp.routeTimes_.name());
        int obsolute = routeTimes_.open(timefile,
                                        sp.routeTimes_.nItems(),
                                        sp.routeTimes_.nTags(),
                                        sp.routeTimes_.timeTags()) != 0;

        if (obsolute) {
            state_ = 0;
        } else {
            state_ = 1;
        }
        StrFree(timefile);

    }

    if (state_ == 0) {
        long int kb;
        kb = sp.routeTimes_.size() / 1024;
        if (kb) {
            const char *s;
            s = Str("Copying a %ld KB travel time table.", kb);
            theNetwork->updateProgress(s);
        }
        routeTimes_ = sp.routeTimes_;
    }
    return *this ;
}


// Read time dependent link travel times and recalculate the shortest
// path if necessary.  If the path table is specified, it also updates
// the travel time from each link to the destination of individual
// paths.

void
RN_Route::updatePathTable(char * fname, float alpha,
						  double start, double stop)
{
    // Update info network
  cout << "Ok, about to test fname"<<endl;
  
    if (fname) {					// from a file
      cout << "!@$ about to call read" << endl;
      read(fname, alpha);
    } else if (isSpFlag(INFO_FLAG_UPDATE)) {
        // from prevailing condition
      cout << "!@$ about to call link travel times" << endl; 
      updateLinkTravelTimes(alpha);
    } else {
        // info have not changed
        return;
    }

    // Recalculate the shortest path trees.

    if (isSpFlag(INFO_FLAG_UPDATE_TREES)) {
        findShortestPathTrees(start, stop);
    }
}


// Return travel time on the shorted path from upstream end of link
// "olink" to destination node "dnode".

float
RN_Route::upRouteTime(int olink, int dnode, double)
// note: dnode is a destination node index, not a node index.
{
    return routeTimes_.read(olink * nDestNodes() + dnode);
}

float
RN_Route::upRouteTime(RN_Link* olink, RN_Node* dnode, double timesec)
{
    int d = dnode->destIndex();
    if( d >= 0 && d < nDestNodes()) {
        return upRouteTime(olink->index(), d, timesec);
    } else {
        cerr << "Node " << dnode->code()
             << " is not defined as a destination." << endl;
        return FLT_INF;
    }
}


// Return travel time on the shorted path from dnstream end of link
// "olink" to node "dnode".

float
RN_Route::dnRouteTime(RN_Link* olink, RN_Node* dnode, double timesec)
{
    int d = dnode->destIndex();
    if( d >= 0 && d < nDestNodes()) {
        return dnRouteTime(olink->index(), d, timesec);
    }else {
        cerr << "Node " << dnode->code()
             << " is not defined as a destination." << endl;
        return FLT_INF;
    }
}

float
RN_Route::dnRouteTime(int olink, int dnode, double)
// note: dnode is a destination node index, not a node index.
{
    float total = upRouteTime(olink, dnode);
    float dt = avgLinkTime(olink);
    return total - dt;
}

// ------------------------------------------------------------------
// Compute shortest paths from each link to every node for each time
// period
// ------------------------------------------------------------------

void
RN_Route::findShortestPathTrees(double, double)
{
    register int i, n = nLinks();

    if (!linkGraph_) {
        linkGraph_ = new RN_LinkGraph(theNetwork, this);
    } else {
        linkGraph_->updateLinkCosts(this);
    }

    const char *s;
    s = Str("Building %d shortest path trees ...", n);
    theNetwork->updateProgress(s);
   
    float done = 0;
    float step = (n > 1) ? (1.0 / (n - 1)) : 1.0;
   
    for (i = 0; i < n; i ++) {
        linkGraph_->calcShortestPathTree(i);
        recordShortestPathTree(i);
        done = i * step * 100.0;
        theNetwork->updateProgress(done);
    }
}


// -------------------------------------------------------------------
// Record travel time on shortest path from "root" to every nodes
// into the DiskData "routeTime_".  When more than one link arrives at
// a node, the incoming link with the shortes travel time from
// upstream node of "root" is selected.
// -------------------------------------------------------------------

void
RN_Route::recordShortestPathTree(int root, int p)
{
    register int i, n = nNodes();
    unsigned long pos = p * matrixSize() + root * nDestNodes();
    float cost;

    // Travel times from root to every destination node

    for (i = 0; i < n; i ++) {
        cost = graph()->node(i)->cost();
        if( theNetwork->node(i)->destIndex() != -1 ) {
            routeTimes_.write(pos + theNetwork->node(i)->destIndex(), cost);
        }
    }

}


// ------------------------------------------------------------------
// Print shortest travel time from each link to every destination
// node.
// ------------------------------------------------------------------

void
RN_Route::printShortestRouteTimes(ostream &os, int p)
{
    if (!theSimulationEngine->isRectangleFormat()) {
        os << endl << "% TRAVEL TIME ON SHORTEST PATH START AT PERIOD "
           << p << endl << endl;
        os << "% Link / Node-1 Node-2 ... Node-N (in seconds)" << endl;
    }
    int i, nlinks = nLinks();
    int j, nnodes = nNodes();
    for (i = 0; i < nlinks; i ++) {
        os << theNetwork->link(i)->code();
        for (j = 0; j < nnodes; j ++) {
            if( theNetwork->node(j)->destIndex() < 0 ) continue ;
            float t = upRouteTime(i, j, p);
            os.width(6);
            if (t < 86400.0) {
                os << Round(t);
            } else {
                os << "nil";
            }
        }
        os << endl;
    }
}
