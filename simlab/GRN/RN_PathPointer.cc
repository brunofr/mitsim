//-*-c++-*------------------------------------------------------------
// NAME: Pointer to path table
// AUTH: Qi Yang
// FILE: RN_PathPointer.C
// DATE: Tue Jun 11 00:46:35 1996
//--------------------------------------------------------------------


#include "RN_PathPointer.h"

#include "RN_Path.h"
#include "RN_PathTable.h"
#include "BusAssignmentTable.h"
#include "RN_Link.h"
#include "RN_Node.h"
#include "OD_Cell.h"

bool RN_PathPointer::IsUsedBy(OD_Cell* od)
{
	if (!od) return false ;
	for (int i = 0; i < od->nPaths(); i ++) {
		if (path_ == od->path(i)) return true ;
	}
	return false ;
}


RN_Node*
RN_PathPointer::oriNode()
{
   return path_->oriNode();
}


RN_Node* 
RN_PathPointer::desNode()
{
   return path_->desNode();
}


RN_Link* 
RN_PathPointer::link()
{
   return path_->link(position_);
}


float 
RN_PathPointer::cost(RN_LinkTimes *info)
{
   return path_->travelTime(info, position_);
}


RN_Link*
RN_PathPointer::prev()
{
   short int i = position_ - 1;
   if (i >= 0) return path_->link(i);
   else return NULL;
}

RN_Link*
RN_PathPointer::next()
{
   short int i = position_ + 1;
   if (i < path_->nLinks()) return path_->link(i);
   else return NULL;
}
