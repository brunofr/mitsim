//-*-c++-*------------------------------------------------------------
// NAME: Time variant link travel times
// AUTH: Qi Yang
// FILE: RN_LinkTimes.h
// DATE: Sat Jun 15 15:04:39 1996
//--------------------------------------------------------------------

#ifndef RN_LINKTIMES_HEADER
#define RN_LINKTIMES_HEADER

#include <iostream>
#include <cstring>

class RN_Link;
class Reader;
class GenericVariable;

class RN_LinkTimes
{
   public:

      RN_LinkTimes(const char *filename);
      void initTravelTimes();
      RN_LinkTimes(const RN_LinkTimes &);
      virtual ~RN_LinkTimes();

      RN_LinkTimes& operator=(const RN_LinkTimes& rnt);

      inline void filename(const char *f) {
		 if (f != filename_) {
			if (filename_) delete [] filename_;
			filename_ = strdup(f);
		 }
      }

      inline const char *filename() {
		 return filename_;
      }

      int nLinks();
      int nNodes();
      int nDestNodes();

      inline int  infoStartPeriod() { return infoStartPeriod_; }
      inline long int infoStartTime() { return infoStartTime_; }
      inline long int infoStopTime() { return infoStopTime_; }
      inline int infoPeriods() { return infoPeriods_; }
      inline int infoPeriodLength() { return infoPeriodLength_; }
      inline int infoTotalLength() { return infoTotalLength_; }

      int whichPeriod(double t);
      double whatTime(int p);

      float linkTime(RN_Link *i, double timesec);
      float linkTime(int i, double timesec);
      float avgLinkTime(RN_Link *i); // average
      float avgLinkTime(int i) { return avgLinkTime_[i]; }

      // This function is called by Graph::labelCorrecting(...)

      inline float cost(int i, double timesec) {
		 return linkTime(i, timesec);
      }
      inline float cost(int i) { return avgLinkTime_[i]; }

      void calcLinkTravelTimes(); // should called only once
      void updateLinkTravelTimes(float alpha);

      void read(const char *, float alpha = 1.0);

      void printLinkTimes(std::ostream &os = std::cout);

      void preTripGuidance(int flag) { preTripGuidance_ = flag; }
      int preTripGuidance() { return preTripGuidance_; }

   protected:

      char *filename_;	// the latest file name
      
      int infoStartPeriod_;	// the start interval
      long int infoStartTime_;	// start time of link time table
      long int infoStopTime_;	// stop time of link time table
      int infoPeriodLength_;	// length of each period (second)
      int infoTotalLength_;	// total length
      int infoPeriods_;		// number of time periods

      // Travel time are all measured in seconds.

      float *linkTimes_;	// 2D travel times on each link
      float *avgLinkTime_;	// 1D average travel time

      // 0 = used only when the vehicle passes a info node (e.g. vms)
      // 1 = also used as pretrip.
      // default = 1

      int preTripGuidance_;
};

#endif
