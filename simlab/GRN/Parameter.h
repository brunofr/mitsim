//-*-c++-*------------------------------------------------------------
// FILE: Parameter.h
// DATE: Thu Oct 19 11:54:32 1995
//--------------------------------------------------------------------

#ifndef PARAMETER_HEADER
#define PARAMETER_HEADER

#include <Tools/GenericSwitcher.h>

class Parameter : public GenericSwitcher
{
protected:

  static char *name_;	// file name

  // Constants for transfering between I/O and internal
  // units. Internal units are in metric system

  static float lengthFactor_;	// length and coordinates (1=meter) 
  static float speedFactor_;	// speed (1=km/hr) 
  static float densityFactor_;	// density (1=vehicles/km) 
  static float flowFactor_;	// flow & capacity (1=vehicles/hr) 
  static float timeFactor_;	// travel time (1=minutes) 
  static float odFactor_;

  static char * densityLabel_;
  static char * speedLabel_;
  static char * flowLabel_;
  static char * occupancyLabel_;

  float visibilityScaler_;
  float visibility_;

  static int resolution_[];	// for view accuracy

  float pathAlpha_;		// parameter for updating travel time

  // For route choice model

  float *routingParams_[2];	// in logic route choice model
  float commonalityFactor_;	// in path choice model
  float *diversionPenalty_; // cost added in util func
  float validPathFactor_;	// compared to shorted path
  float rationalLinkFactor_;    // reduces irrational link choices
  float freewayBias_;	// travel time
  float busToStopVisibility_; // distance from bus stop at which bus begins to change lanes
  float busStopSqueezeFactor_; // reduction in speed in lane next to bus at a stop

  // Check if the two tokens are the same

  int isEqual(const char *s1, const char *s2);

public:

  Parameter();
  virtual ~Parameter() { }
  
  static inline char * name() { return name_; }
  static inline char ** nameptr() { return &name_; }

  static int error(const char *);

  // Unit transfer

  static float lengthFactor() { return lengthFactor_; }
  static float speedFactor() { return speedFactor_; }
  static float densityFactor() { return densityFactor_; }
  static float flowFactor() { return flowFactor_; }
  static float timeFactor() { return timeFactor_; }

  static char * densityLabel() { return densityLabel_; }
  static char * speedLabel() { return speedLabel_; }
  static char * flowLabel() { return flowLabel_; }
  static char * occupancyLabel() { return occupancyLabel_; }

  static int resolution(int i) { return resolution_[i]; }
  static int loadResolution(GenericVariable &);

  float pathAlpha() { return pathAlpha_; }

  // Route choice

  inline float guidedRate() {
	return routingParams_[1][0];
  }
  inline float routingBeta(int type) {
	return routingParams_[type][1];
  }
  inline float commonalityFactor() {
	return commonalityFactor_;
  }
  inline float diversionPenalty() {
	return  diversionPenalty_[0];
  }
  inline float rationalLinkFactor() {
	return  rationalLinkFactor_;
  }
  inline float busToStopVisibility() {
	return  busToStopVisibility_;
  }
  inline float busStopSqueezeFactor() {
	return  busStopSqueezeFactor_;
  }
  inline float pathDiversionPenalty() {
	return  diversionPenalty_[1];
  }
      
  inline float validPathFactor() { return validPathFactor_; }
  inline float freewayBias() { return freewayBias_; }

  // TS_Parameters for responding traffic ocntrols

  inline float visibilityScaler() {	// virtual
	return visibilityScaler_;
  }
  float visibility() { return visibility_; }
  void checkVisibility();

  virtual int parseVariable(GenericVariable &gv);

private:

  int loadRouteChoiceParas(GenericVariable &);
  int loadDiversionPenalty(GenericVariable &);
};

extern Parameter * theBaseParameter;

#endif
