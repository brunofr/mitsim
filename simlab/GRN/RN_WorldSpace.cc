//-*-c++-*------------------------------------------------------------
// NAME: Massachusetts Institue of Technology
// AUTH: Peter J. Welch and Qi Yang
// FILE: RN_WorldSpace.h
// DATE: Thu Mar 23 12:01:24 1995
//--------------------------------------------------------------------

#include "RN_WorldSpace.h"
#include "RoadNetwork.h"
#include "Parameter.h"

#include <Tools/Math.h>

#include <cstdlib>
using namespace std;

RN_WorldSpace * theWorldSpace = NULL;

// We initialize the north east point to - LARGE_NUMBER and the south
// west point to + LARGE_NUMBER to ensure that the first network point
// compared in recordExtremePoints will replace the initial values.

RN_WorldSpace::RN_WorldSpace()
   : sw_pnt_(DBL_INF, DBL_INF),
     ne_pnt_(-DBL_INF, -DBL_INF)
{
  theWorldSpace = this;
}

void 
RN_WorldSpace::recordExtremePoints(const WcsPoint & point) 
{
   sw_pnt_ = WcsPoint ( Min(sw_pnt_.x(), point.x()),
			Min(sw_pnt_.y(), point.y()) );

   ne_pnt_ = WcsPoint ( Max(ne_pnt_.x(), point.x()),
			Max(ne_pnt_.y(), point.y()) );
}


void 
RN_WorldSpace::createWorldSpace() 
{
   width_  = (ne_pnt_.x() - sw_pnt_.x()) * Parameter::lengthFactor();
   height_ = (ne_pnt_.y() - sw_pnt_.y()) * Parameter::lengthFactor();

   ll_pnt_ = worldSpacePoint(sw_pnt_);
   ur_pnt_ = worldSpacePoint(ne_pnt_);

   if (width_ < POINT_EPSILON && height_ < POINT_EPSILON) {
      cerr << "Error: World space is empty. Check geometric data!"
	   << endl;
      exit(1);
   } else if (width_ < POINT_EPSILON) {	// N-S linear network
      width_ = 0.1 * height_;
   } else if (height_ < POINT_EPSILON) { // W-E linear network
      height_ = 0.1 * width_;
   }

   center_.set(0.5 * width_, 0.5 * height_);
   angle_ = 0.0;
}


WcsPoint
RN_WorldSpace::worldSpacePoint(double x, double y)
{
   return WcsPoint((x - sw_pnt_.x()) * Parameter::lengthFactor(),
		   (y - sw_pnt_.y()) * Parameter::lengthFactor());
}


WcsPoint
RN_WorldSpace::worldSpacePoint(const WcsPoint& p)
{
   return worldSpacePoint(p.x(), p.y());
}


WcsPoint
RN_WorldSpace::databasePoint(const WcsPoint& p)
{
   return WcsPoint(p.x() / Parameter::lengthFactor() + sw_pnt_.x(),
				   p.y() / Parameter::lengthFactor() + sw_pnt_.y());
}

WcsPoint
RN_WorldSpace::databasePoint(double x, double y)
{
   return WcsPoint(x / Parameter::lengthFactor() + sw_pnt_.x(),
				   y / Parameter::lengthFactor() + sw_pnt_.y());
}
