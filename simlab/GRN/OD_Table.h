//-*-c++-*------------------------------------------------------------
// ODTable.h -- Origin to Destination Trip Table
//--------------------------------------------------------------------

#ifndef ODTABLE_HEADER
#define ODTABLE_HEADER


#include <Templates/Listp.h>

class RN_Vehicle;

class OD_Table
{
  friend class OD_BisonParser;
  friend class OD_Parser;
  friend class OD_Cell;
  friend class ArgsParser;

public:

  typedef Listp<OD_Cell*> od_list_type;
  typedef od_list_type::link_type od_list_link_type;

protected:

  double nextTime_;			// time to read od pairs
  int type_;				// vehicle type
  double scale_;			// scaling factor
  od_list_type cells_;		// list of OD cells

public:

  static char *name_;	// file name

  OD_Table();

  virtual ~OD_Table() { clean(); }
	
  void open(const char *fn = 0);
  double read();

  static inline char* name() { return name_; }
  static inline char** nameptr() { return &name_; }

  inline double scale() { return scale_; }
  inline double nextTime() { return nextTime_; }
  inline void nextTime(double t) { nextTime_ = t; }
  inline int nCells() { return cells_.nNodes(); }
  inline int type() { return type_; }

  virtual int init(double start, int type, double s);
  int init(double start) {
	return init(start, type_, scale_);
  }
  virtual void insert(OD_Cell* cell);

  // These may need to be overloaded by derived class

  virtual OD_Cell* newOD_Cell() = 0;
  virtual void emitVehicles(void);

  virtual void clean();

private:

  OD_Parser* parser_;	// the OD table parser
  int nCellsParsed_;	// od cells parsed
};

extern OD_Table * theODTable;

#endif
