/*
 * NAME: Lexical analyzer generator for vehicle table parser
 * AUTH: Qi Yang
 * FILE: VehicleTableScanner.lex
 * DATE: Tue Mar  5 20:42:14 1996
 */

%{
#include "VehicleTableParser.h"
#define YY_BREAK
%}

%option yyclass="VehicleTableFlexLexer"
%option noyywrap
%option yylineno

C	(#.*)|(%.*)|("//".*)
WS	[\t ,;\n]+

D	[0-9]
H       [0-9A-Fa-f]
E	[eE][-+]?{D}+
N	{D}+
X       {H}+
F1	{D}+\.{D}*({E})?
F2	{D}*\.{D}+({E})?
F3	{D}+({E})?


HEXTAG  ("0x")|("0X")|("x")|("X")

I	[-+]?({N}|({HEXTAG}{X}))
R	[-+]?({F1}|{F2}|{F3})

HH	(([01]?[0-9])|([2][0-3]))
MM	([0-5]?[0-9])
SS	([0-5]?[0-9])
TIME	{HH}[:]{MM}[:]{SS}

OB	{WS}?[{]{WS}?
CB	{WS}?[}]{WS}?
DUP ("*")|("x")
END ([<]"END"[>])

%%

"/*"	{	/* skip comments */
  int c;
  while ((c = yyinput()) != 0) {
    if (c == '*') {
      if ((c = yyinput()) == '/') break;
      else unput(c);
    }
  }
  break;
}

{WS}	{ break; }
{C}	{ break; }

{OB}	{ return VehicleTableParser::VehicleTableOB; }
{CB}	{ return VehicleTableParser::VehicleTableCB; }
{DUP} { return VehicleTableParser::VehicleTableDUP; }

{I}	{ return VehicleTableParser::VehicleTableINT; }
{R}	{ return VehicleTableParser::VehicleTableREAL; }
{TIME}  { return VehicleTableParser::VehicleTableTIME; }
{END}   { return VehicleTableParser::VehicleTableEND; }

%%
