//-*-c++-*------------------------------------------------------------
// FILE: RN_LinkGraph.C
// AUTH: Qi Yang
// DATE: Wed Oct 18 14:29:29 1995
//--------------------------------------------------------------------

#include <iostream>
#include <new>

#include <Tools/Math.h>

#include "RoadNetwork.h"
#include "Parameter.h"

#include "RN_Node.h"
#include "RN_Link.h"
#include "RN_LinkTimes.h"
#include "RN_LinkGraph.h"
using namespace std;

RN_LinkGraph::RN_LinkGraph(RoadNetwork *rn, RN_LinkTimes *info)
  : network_(rn)
{
   graph_ = new Graph<float, RN_LinkTimes>(
      rn->nNodes(), rn->nLinks(), FLT_INF,
      theBaseParameter->diversionPenalty());
   
   register int i, n = rn->nLinks();

   for (i = 0; i < n; i ++) {
      RN_Link * pl = rn->link(i);
      int grade;
      if (pl->linkType() == LINK_TYPE_FREEWAY) {
	 grade = 0;
      } else {
	 grade = 1;
      }
      graph_->addLink(i,
		      pl->upNode()->index(),
		      pl->dnNode()->index(),
		      info->cost(i), grade,
		      pl->dnLegal(), pl->dnIndex());
   }
}


void
RN_LinkGraph::updateLinkCosts(RN_LinkTimes *info)
{
   register int i, n = network_->nLinks();
   for (i = 0; i < n; i ++) {
      RN_Link * pl = network_->link(i);
      graph_->linkCost(i, info->cost(i));
   }
}
