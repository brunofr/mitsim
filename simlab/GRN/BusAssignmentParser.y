//
// BusAssignmentParser.y
// by Qi Yang & Daniel Morgan
//

%name   BusAssignmentParser
%define ERROR_BODY	DEF_ERROR_BODY
%define LEX_BODY	DEF_LEX_BODY

%define CONSTRUCTOR_PARAM  BusAssignmentTable* pContainer_
%define CONSTRUCTOR_INIT   : _pContainer(pContainer_)   \
                           ,_current(NULL)              \
                           ,_bid(0)                     \
                           ,_type(0)                    \

%define MEMBERS  DEF_MEMBERS                            \
   BusAssignmentTable* _pContainer ;                    \
   RN_Vehicle* _current ;                               \
   int _bid ;                                           \
   int _type ;                                          \

%union {
  long        type_i ;
  double      type_f ;
  const char* type_s ;
}

%token <type_s> LEX_EQUAL
%token <type_s> LEX_COMMA
%token <type_s> LEX_COLON
%token <type_s> LEX_SEMICOLON
%token <type_s> LEX_OAB
%token <type_s> LEX_CAB
%token <type_s> LEX_OBB
%token <type_s> LEX_CBB
%token <type_s> LEX_OCB
%token <type_s> LEX_CCB
%token <type_i> LEX_INT
%token <type_f> LEX_REAL
%token <type_s> LEX_TIME
%token <type_s> LEX_PAIR
%token <type_s> LEX_TAG
%token <type_s> LEX_STRING
%token <type_s> LEX_DUP
%token <type_s> LEX_END

%header{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <IO/Exception.h>
#include <Tools/ToolKit.h>  
#include <Tools/Scanner.h>  
#include <Tools/SimulationClock.h>
#include <Tools/Math.h>

#include <GRN/BusAssignmentTable.h>
#include <GRN/RN_Vehicle.h>
#include <GRN/RN_Path.h>
#include <GRN/RN_Node.h>
%}

//--------------------------------------------------------------------
// Symbols used vehicle table grammer rules.
//--------------------------------------------------------------------

%type <type_f> fnum float_num time string_time
%type <type_s> everything multi_assignment_tables assignment_list
%type <type_s> assignment_table assignment_table_data
%type <type_s> bid assignment_without_id one_assignment
%type <type_s> multi_assignments assignments several_assignments the_end
%type <type_i> inum cnt assignment_data_3 assignment_data_4

%start everything

%%

//--------------------------------------------------------------------
// Beginniing of BusAssignmentTable table grammer rules.
//--------------------------------------------------------------------

everything: multi_assignment_tables the_end
| assignment_list the_end
| assignment_list multi_assignment_tables the_end
;

the_end: LEX_END
{
  _pContainer->initBus(DBL_INF);
  if (ToolKit::verbose()) {
	cout << "Finished parsing <" << filename() << ">." << endl;
  }
  YYACCEPT;			// return and never back
}
;

multi_assignment_tables: assignment_table
| multi_assignment_tables assignment_table
;

assignment_table: assignment_table_data assignment_list
;

assignment_list: LEX_OCB multi_assignments LEX_CCB
;

assignment_table_data: time
{
  // $1=DepartuteTime
  int err_no = _pContainer->initBus($1); 
  check(err_no);
  if (_pContainer->nextTime() > theSimulationClock->currentTime()) {
	YYACCEPT;					// return and will come back later
  }
}
;

multi_assignments: assignments
| multi_assignments assignments
;

assignments: several_assignments | one_assignment
{
  if (ToolKit::debug()) {
	cout << endl;
  }
  _current = NULL;
}
;

several_assignments: cnt LEX_DUP assignment_without_id
{
  if (_current) {
	for (int i = 1; i < $1; i ++) {
	  int p;
	  if (_current->path()) {
		p = _current->path()->code();
	  } else {
		p = 0;
	  }
	  RN_Vehicle *ptr = _pContainer->newVehicle();
	  int err_no = ptr->initBus(_bid
				       ,_pContainer->lastAssignment()->origin()
				       ,_pContainer->lastAssignment()->destination()
				       , p);
	  if (err_no != 0) delete ptr;
	}
  }
}
;

one_assignment: assignment_without_id
| bid assignment_without_id
;

cnt: inum
{
  if (ToolKit::debug()) {
	cout << $1 << endc << "*" << endc;
  }
  _bid = 0;						// set id
  $$ = $1;
}
;

bid: inum
{
  if (ToolKit::debug()) {
	cout << $1 << endc;
  }
  _bid = $1;					// set id
}
;

assignment_without_id: LEX_OCB assignment_data_3 LEX_CCB
| LEX_OCB assignment_data_4 LEX_CCB
{
  _bid = 0 ;					// clear id
  check($2) ;
}
;

assignment_data_3: inum inum inum
{
  //$1 = busID, $2 = busType, $3 = runID
  if (ToolKit::debug()) {
	cout << '{' << $1 << endc << $2 << endc << $3 << '}';
  }

  if (_pContainer->skip()) {
	_current = NULL;
  } else {
	// $1=Ori $2=Des
	_current = _pContainer->newVehicle();
        int err_no = _pContainer->addBusAssignment($1, $2, $3);
	_type = 0;
	$$ = _current->initBus( $1, _pContainer->lastAssignment()->origin()      
                                  , _pContainer->lastAssignment()->destination() 
			          , $3 );
	if ($$ != 0) {  
	  delete _current;
	  _current = NULL;
	}
  }
}
;

assignment_data_4: inum inum inum inum
{
  //$1 = busID, $2 = busType, $3 = runID, $4 = busLoadAtStart
  if (ToolKit::debug()) {
	cout << '{' << $1 << endc << $2 << endc << $3 << $4 << '}';
  }

  if (_pContainer->skip()) {
	_current = NULL;
  } else {
	// $1=Ori $2=Des
	_current = _pContainer->newVehicle();
        int err_no = _pContainer->addBusAssignment($1, $2, $3, $4);
	_type = 0;
	$$ = _current->initBus($1, _pContainer->lastAssignment()->origin()      
                                  , _pContainer->lastAssignment()->destination() 
			          , $3);
	if ($$ != 0) {  
	  delete _current;
	  _current = NULL;
	}
  }
}
;

//--------------------------------------------------------------------
// Beginning of basic grammer rules.
//--------------------------------------------------------------------

inum: LEX_INT
{
  char *ptr;
  $$ = (int)strtol(value(), &ptr, 0);
}
;

fnum: float_num
| inum
{
  $$ = $1;
}
;

float_num: LEX_REAL
{
  $$ = atof(value());
}
;

time: string_time
| fnum
{
  $$ = $1;
}
;

string_time: LEX_TIME
{
  $$ = theSimulationClock->convertTime(value());
}
;

%%

// Following pieces of code will be verbosely copied into the parser.

%header{
  // empty
%}
