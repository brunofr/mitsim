//-*-c++-*------------------------------------------------------------
// NAME: A parser for bus stop parameters 
// AUTH: Qi Yang & Daniel Morgan
// FILE: RN_BusStopPrmTable.C
// DATE: Fri Nov 30 16:59:15 2001
//--------------------------------------------------------------------

#include <algorithm>

#include <Tools/SimulationClock.h>
#include <Tools/ToolKit.h>
#include <IO/Exception.h>

#include "RN_BusStopPrmParser.h"
#include "RN_BusStopPrmTable.h"
#include "RoadNetwork.h"
using namespace std;

// theBusStopPrmTable will be initialized in the ParseBusStopPrms() func in
// TS_Setup.C or MesoTS_Setup.C

RN_BusStopPrmTable * theBusStopPrmTable = NULL;

char * RN_BusStopPrmTable::name_ = NULL;

RN_BusStopPrmTable::RN_BusStopPrmTable()
   : parser_(NULL), nextTime_(-DBL_INF)
{
   theBusStopPrmTable = this;
}

// Open bus stop parameter table file and create a parameter parser

void RN_BusStopPrmTable::open(double start_time)
{
   const char *fn = ToolKit::optionalInputFile(name_);
   if (!fn) theException->exit(1);
   Copy(&name_, fn) ;
   startTime_ = start_time;

   // Parser will be created in read()

   if (parser_) delete parser_ ;
   parser_ = NULL ;
}

// Read bus stop parameter tables for next time period

double RN_BusStopPrmTable::read()
{
  const char *fn ;
  if (parser_) {				// continue parsing of current file
	fn = NULL ;
  } else {						// open the  new file
	parser_ = new RN_BusStopPrmParser(this);
	fn = name_;
  }
  while (nextTime_ <= theSimulationClock->currentTime()) {
	parser_->parse(fn);
  }
  return nextTime_;
}

void
RN_BusStopPrmTable::assignDwellBounds(int code, int lower, int upper)
{
   RN_BusStop *bs = theNetwork->findBusStop(code);
   bs->minDwell_ = lower;
   bs->maxDwell_ = upper;
}

int
RN_BusStopPrmTable::assignRateParams (int code, int routecode, float arr, float alight)
{
   RN_BusStop *bs = theNetwork->findBusStop(code);
   return (bs->addRateParameters(routecode, arr, alight));
}

int
RN_BusStopPrmTable::assignArrivalParams (int code, int routecode, float st, float a,
                                           int b, float c, float peak, float alight)
{
   RN_BusStop *bs = theNetwork->findBusStop(code);
   return (bs->addArrivalParameters(routecode, st, a, b, c, peak, alight));
}

void
RN_BusStopPrmTable::print(ostream &os)
{
   register int i;
   os << "// Bus stop parameter table generated from file <" << name_ << "> on "
      << TimeTag() << " by "
      << UserName();
   os << "\n}" << endl;
}

// Called by parser to setup the parameter tables and update time

int 
RN_BusStopPrmTable::initBusStopPrms(double s)
{
   if (nextTime_ > -86400.0 && ToolKit::debug()) {
      cout << " Bus stop parameters parsed at "
	   << theSimulationClock->convertTime(nextTime_)
	   << "." << endl;
   }

   nextTime_ = s;
   return 0;
}
