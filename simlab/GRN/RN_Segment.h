//-*-c++-*------------------------------------------------------------
// FILE: RN_Segment.h
// AUTH: Qi Yang
// DATE: Wed Oct 18 17:38:54 1995
//--------------------------------------------------------------------

#ifndef RN_SEGMENT_HEADER
#define RN_SEGMENT_HEADER

#include <iostream>
#include <GRN/Constants.h>

#include <Templates/Object.h>
#include <Templates/Listp.h>

#include "RN_Arc.h"

class RoadNetwork;
class RN_BisonParser;
class RN_Node;
class RN_Link;
class RN_Lane;
class RN_CtrlStation;
class RN_SurvStation;
class RN_WorldSpace;

const unsigned short int SEGMENT_TYPE_HEAD		= 0x0001;
const unsigned short int SEGMENT_TYPE_TAIL		= 0x0002;
const unsigned short int SEGMENT_TYPE_THE_BEGINNING	= 0x0010;
const unsigned short int SEGMENT_TYPE_THE_END		= 0x0020;

class RN_Segment : public RN_Arc, public CodedObject
{
    friend class RN_BisonParser;
    friend class RoadNetwork;
    friend class RN_Link;
    friend class RN_Lane;
   
public:

    typedef Listp<RN_SurvStation*> surv_list_type;
    typedef Listp<RN_CtrlStation*> ctrl_list_type;
    typedef surv_list_type::link_type surv_list_link_type;
    typedef ctrl_list_type::link_type ctrl_list_link_type;

protected:

    // The following variables have been added for the MOE collection
    // times in secs

#ifdef EXTERNAL_MOE
    static TimeInterval samplingInterval_;
    static double samplingStepSize_;
    static double reportStepSize_;
#endif
      
    static int sorted_;		// 1=sorted 0=arbitrary order

    int index_;				// index in array

    RN_Link* link_;			// pointer to link
    float grade_;				// grade of the segment
    int nLanes_;				// number of lanes
    int leftLane_;			// index to the left lane
    unsigned int speedLimit_;	// default speed limit
    float freeSpeed_;			// free flow speed
    float distance_;			// distance from dn node

    ctrl_list_link_type ctrlList_; // first (upstream) control station
    surv_list_link_type survList_; // first (upstream) sensor station

    unsigned int state_;
    unsigned short int localType_; // head, tail, etc
    short int sdIndex_;		// index to the performance function

public:
  
    RN_Segment()
        : distance_(0.0), nLanes_(0), link_(NULL),
          ctrlList_(NULL), survList_(NULL),
          localType_(0), state_(0), sdIndex_(0) {
    }
    virtual ~RN_Segment() { }

    static inline int sorted() { return sorted_; }

#ifdef EXTERNAL_MOE
    static TimeInterval samplingInterval() {return samplingInterval_;}
    static double samplingStepSize() {return samplingStepSize_;}
    static double reportStepSize() {return reportStepSize_;}
#endif

    inline unsigned int state(unsigned int mask = 0xFFFF) {
        return (state_ & mask);
    }
    inline void setState(unsigned int s) {
        state_ |= s;
    }
    inline void unsetState(unsigned int s) {
        state_ &= ~s;
    }

    inline unsigned short int isHead() {
        return (localType_ & SEGMENT_TYPE_HEAD);
    }
    inline unsigned short int isTail() {
        return (localType_ & SEGMENT_TYPE_TAIL);
    }
    inline unsigned short int isTheEnd() {
        return (localType_ & SEGMENT_TYPE_THE_END);
    }
    inline unsigned short int isTheBeginning() {
        return (localType_ & SEGMENT_TYPE_THE_BEGINNING);
    }

#ifdef EXTERNAL_MOE
    inline int isMOESelected () { 
        return (state_ & STATE_MOE_SELECTED);
    }
#endif

    inline int index() { return index_; }
    int localIndex();
    inline RN_Link* link() { return link_; }

    // Segment iterators in the entire network

    RN_Segment* prev();
    RN_Segment* next();

    // Segment iterators in a link

    RN_Segment* upstream();
    RN_Segment* downstream();

    // Lane iterators in a segment

    inline int leftLaneIndex() { return leftLane_; }
    RN_Lane* leftLane();
    RN_Lane* rightLane();
    RN_Lane* lane(int i);
    RN_Lane* centralLane();
    virtual WcsPoint centerPoint(float fraction = .5) {
        return intermediatePoint(fraction) ;
    }

    inline ctrl_list_link_type ctrlList() { return ctrlList_; }
    inline surv_list_link_type survList() { return survList_; }

    virtual int init(int id, unsigned int speedlimit,
                     float speed, float grade);
    virtual int initArc(double x1, double y1, double b,
                        double x2, double y2);
    virtual void print(std::ostream &os = std::cout);
    virtual void printLanes(std::ostream &os = std::cout);

    // Segment attributes

    inline int nLanes() { return nLanes_; }
    inline float distance() { return distance_; }
    inline void distance(float x) { distance_ = x; }
    inline unsigned int speedLimit() {
        return speedLimit_;
    }
    inline void freeSpeed(float f) { freeSpeed_ = f; }
    inline float freeSpeed() { return freeSpeed_; }

    inline void grade(float g) { grade_ = g; }
    inline float grade() { return grade_; }
    int type();

    virtual float calcCurrentTravelTime();
    void sdIndex(short int i) { sdIndex_ = i; }
    short int sdIndex() { return sdIndex_; }

    virtual void printSdIndex(std::ostream &) { }

    // Make end point of each pair of connected segments snapped at
    // the same point

    void snapCoordinates();

    virtual void calcArcInfo(RN_WorldSpace &world_space);
    virtual void calcStaticInfo(void);

    virtual float calcDensity(void) { return density(); }
    virtual float calcSpeed(void) { return speed(); }
    virtual int calcFlow(void) { return flow(); }

    virtual float density(void) { return 0; }
    virtual float speed(void) { return freeSpeed_; }
    virtual int flow(void) { return 0; }

    void markConnectedUpLanes();
    void markConnectedDnLanes();
    void unmarkConnectedUpLanes();
    void unmarkConnectedDnLanes();

private:

    // Export geometry data in MapInfo format
  
    void ExportMapInfo(std::ostream& mif, std::ostream& mid) ;
    void Export(std::ostream& os) ;
};

#endif
