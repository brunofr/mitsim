//-*-c++-*------------------------------------------------------------
// NAME: A parser for bus schedule 
// AUTH: Qi Yang & Daniel Morgan
// FILE: BusScheduleTable.h
// DATE: Fri Nov 30 16:55:50 2001
//--------------------------------------------------------------------

#ifndef RN_BUSSCHEDULETABLE_HEADER
#define RN_BUSSCHEDULETABLE_HEADER

#include <iostream>
#include <vector>

#include "RN_BusSchedule.h"

const int NUM_BUSSCHEDULES = 5;	// Default initial num of bus routes

class RN_BusScheduleParser;
class ArgsParser;

class RN_BusScheduleTable
{
      friend class RN_BusScheduleParser;
      friend class ArgsParser;

   protected:

      std::vector<RN_BusSchedule*> busschedules_;	// array of pointers to bus routes

   public:

      static char * name_;	// file name
      static char * name() { return name_; }
      static char ** nameptr() { return &name_; }

      RN_BusScheduleTable() { }

      virtual ~RN_BusScheduleTable();

      // This may be overloaded by derived class

      virtual RN_BusSchedule* newBusSchedule();

      virtual int addBusSchedule(int code);
      inline RN_BusSchedule* lastBusSchedule() { return busschedules_.back(); }

      inline int nBusSchedules() { return busschedules_.size(); }
      inline RN_BusSchedule* busschedule(int i) { return busschedules_[i]; }
      RN_BusSchedule* findBusSchedule(int c);

      virtual void print(std::ostream &os = std::cout);

      // void setBusSchedulePointers();
};

extern RN_BusScheduleTable * theBusScheduleTable;

#endif
