//-*-c++-*------------------------------------------------------------
// NAME: Simulation Widges
// AUTH: Qi Yang
// FILE: RN_Curve.C
// DATE: Thu Nov  2 22:21:03 1995
//--------------------------------------------------------------------

#include <Tools/Math.h>

#include "RN_Curve.h"
#include "RN_Arc.h"
using namespace std;

// Return the center of the curve

WcsPoint&
RN_Curve::center()
{
  return arc_->center();
}


// Returns the start point of the curve

WcsPoint
RN_Curve::startPnt()
{
  // See RN_Curve.h for explaination of data

  if (arc_) {
	return center().bearing(data_[1], data_[0]);
  } else {
	return WcsPoint(data_[0], data_[1]);
  }
}


// returns the end point of the curve

WcsPoint
RN_Curve::endPnt()
{
  // See RN_Curve.h for explaination of data

  if (arc_) {
	return center().bearing(data_[3], data_[2]);
  } else {
	return WcsPoint(data_[2], data_[3]);
  }
}


// Find the point on the linear spiral curve (frac: 0=EndPoint
// 1=StartPoint)

WcsPoint
RN_Curve::intermediatePoint(double frac)
{
  if (arc_) {			// curve
	double alpha = data_[0] + data_[4] * (1.0 - frac);
	double r = frac * (data_[1] - data_[3]) + data_[3];
	return arc_->center().bearing(r, alpha);
  } else {			// straight line
	double x = frac * (data_[0] - data_[2]) + data_[2];
	double y = frac * (data_[1] - data_[3]) + data_[3];
	WcsPoint pnt(x, y);
	return (pnt);
  }
}


// Set the curve to be a spiral curve with offset up and dn from the
// input arc.  Offset is positive if the curve is on the left side of
// the arc and negative if the curve is on the right side of the
// curve.

void
RN_Curve::shiftInitializer(RN_Arc *a, double up, double dn)
{
  if (a->isArc()) { // curve
	if (a->bulge() < 0) {
	  up = -up;
	  dn = -dn;
	}
	data_[0] = a->startAngle();
	data_[1] = a->radius() + up;
	data_[2] = a->endAngle();
	data_[3] = a->radius() + dn;
	data_[4] = a->arcAngle();
	arc_ = a;
  } else {			// straight line
	data_[4] = a->startAngle();
	double alpha = data_[4] - HALF_PI;
	data_[0] = a->startPnt().x() + up * cos(alpha);
	data_[1] = a->startPnt().y() + up * sin(alpha);
	data_[2] = a->endPnt().x() + dn * cos(alpha);
	data_[3] = a->endPnt().y() + dn * sin(alpha);
	arc_ = NULL;
  }
}


// Initialize a spiral curve with offsets up and dn from the the input
// arcs.  The start and end points of the curve are moved to the
// intersections (uparc/arc and arc/dnarc).  Offsets are positive if
// the curve is on the left side of the arc, and negative is the curve
// is on the right side of the arc.

void
RN_Curve::intersectionInitializer(RN_Arc *uparc, double upoffset,
								  RN_Arc *thearc, double offset1, double offset2,
								  RN_Arc *dnarc, double dnoffset,
								  int editable)
{
  WcsPoint uppnt, dnpnt;
  double beta;

  if (uparc) {
	uppnt = shiftedIntersection(uparc, upoffset,
								thearc, offset1,
								editable);
  } else {
	beta = thearc->startTangentAngle();
	if (offset1 >= 0) {
	  beta += HALF_PI;
	  upoffset = offset1;
	} else {
	  beta -= HALF_PI;
	  upoffset = -offset1;
	}
	uppnt = thearc->startPnt().bearing(upoffset, beta);
  }

  if (dnarc) {
	dnpnt =  shiftedIntersection(thearc, offset2,
								 dnarc, dnoffset,
								 editable);
  } else {
	beta = thearc->endTangentAngle();
	if (offset2 >= 0) {
	  beta += HALF_PI;
	  dnoffset = offset2;
	} else {
	  beta -= HALF_PI;
	  dnoffset = -offset2;
	}
	dnpnt = thearc->endPnt().bearing(dnoffset, beta);
  }

  endPointsInitializer(thearc, uppnt, dnpnt);
}

void
RN_Curve::endPointsInitializer(
							   RN_Arc *thearc, WcsPoint& uppnt, WcsPoint& dnpnt)
{
  if (thearc->isArc()) { // curve
	data_[0] = thearc->center().angle(uppnt);
	data_[1] = thearc->center().distance(uppnt);
	data_[2] = thearc->center().angle(dnpnt);
	data_[3] = thearc->center().distance(dnpnt);

	// These is a hard way to calculate the angle between uparc
	// and dnarc. There should be an easier way.

	double r, d;
	if (data_[1] < data_[3]) {
	  r = data_[1];
	  d = uppnt.distance(thearc->center().bearing(r, data_[2]));
	} else {
	  r = data_[3];
	  d = dnpnt.distance(thearc->center().bearing(r, data_[0]));
	}
	d *= 0.5;
	double b = (r - sqrt(r * r - d * d)) / d; 
	double a = atan(b);
	data_[4] = 4.0 * ((thearc->bulge() > 0.0) ? (-a) : (a));

	arc_ = thearc;
  } else {
	data_[0] = uppnt.x();
	data_[1] = uppnt.y();
	data_[2] = dnpnt.x();
	data_[3] = dnpnt.y();
	data_[4] = uppnt.angle(dnpnt);
	arc_ = NULL;
  }
}


// Intrapolates the curve based on a left arc and a right curve.  The
// argumenet r1 and r2 are defined as: 0.0 = left and 1.0 = right.


void RN_Curve::intrapolateInitializer(
									  RN_Arc *left, RN_Curve *right,
									  double r1, double r2)
{
  WcsPoint uppnt(right->startPnt(), left->startPnt(), r1);
  WcsPoint dnpnt(right->endPnt(), left->endPnt(), r2);
  endPointsInitializer(left, uppnt, dnpnt);
}


// Returns the intersection between two shifted arcs.

WcsPoint
RN_Curve::shiftedIntersection(RN_Arc *arc1, double d1,
							  RN_Arc *arc2, double d2,
							  int editable)
{
  const double PARALLEL_ANGLE = ONE_PI / 16.0;
  double alpha1 = arc1->endTangentAngle();
  double alpha2 = arc2->startTangentAngle();

  WcsPoint p1, p2;		// end point of the two arcs
  double x, y;			// intersection

  // Shift the end point of the 1st arc

  if (d1 > POINT_EPSILON) {
	p1 = arc1->endPnt().bearing(d1, alpha1 + HALF_PI);
  } else if (d1 < POINT_EPSILON) {
	p1 = arc1->endPnt().bearing(-d1, alpha1 - HALF_PI);
  } else {
	p1 = arc1->endPnt();
  }

  // Shift the start point of the 2nd arc

  if (d2 > POINT_EPSILON) {
	p2 = arc2->startPnt().bearing(d2, alpha2 + HALF_PI);
  } else if (d2 < POINT_EPSILON) {
	p2 = arc2->startPnt().bearing(-d2, alpha2 - HALF_PI);
  } else {
	p2 = arc2->startPnt();
  }

  // Make sure neither arc is in the direction of HALF_PI
  // (otherwise tan(alpha) is infinity)

  int vertical1 = AproxEqual(alpha1, HALF_PI) ||
	AproxEqual(alpha1, 3.0 * HALF_PI);
  int vertical2 = AproxEqual(alpha2, HALF_PI) ||
	AproxEqual(alpha2, 3.0 * HALF_PI);

  if (vertical1 && vertical2) {

	// both arc1 and arc2 are parallel and verticle
      
	if (editable == 2) {
	  x = p2.x();
	  y = p2.y();
	} else if (editable == 1) {
	  x = p1.x();
	  y = p1.y();
	} else {
	  x = 0.5 * (p1.x() + p2.x());
	  y = 0.5 * (p1.y() + p2.y());
	}
  } else if (vertical1) {

	// 1st arc is vertical
      
	x = p1.x();
	y = p2.y() + tan(alpha2) * (x - p2.x());

  } else if (vertical2) {

	// 2nd arc is vertical

	x = p2.x();
	y = p1.y() + tan(alpha1) * (x - p1.x());

  } else {

	double tan1 = tan(alpha1);
	double tan2 = tan(alpha2);

	if (fabs(tan1 - tan2) < tan(PARALLEL_ANGLE)) {
      
	  // arc1 and arc2 are parallel
      
	  if (editable == 2) {
	    x = p2.x();
	    y = p2.y();
	  } else if (editable == 1) {
	    x = p1.x();
	    y = p1.y();
	  } else {
	    x = 0.5 * (p1.x() + p2.x());
	    y = 0.5 * (p1.y() + p2.y());
	  }
	} else {
	 
	  // arc1 and arc2 intersect
	 
	  double b1 = p1.y() - tan1 * p1.x();
	  double b2 = p2.y() - tan2 * p2.x();
	 
	  x =  (b2 - b1) / (tan1 - tan2);

	  y = tan1 * x + b1;

	  // Check if the new point should be rejected

	  double cx = 0.5 * (p1.x() + p2.x());
	  double cy = 0.5 * (p1.y() + p2.y());
	  double dx = x - cx;
	  double dy = y - cy;

	  // distance between the new point and the center of the end points

	  double dis = dx * dx + dy * dy;
	  if (dis > 36.0) {
	    x = cx;
	    y = cy;
	  }
	}
  }

  return WcsPoint(x, y);
}


double
RN_Curve::startTangentAngle()
{
  if (arc_) {
	const double alpha = TWO_PI - HALF_PI;
	if (arc_->bulge() > BULGE_EPSILON) {
	  return ((data_[0] > HALF_PI) ?
			  (data_[0] - HALF_PI) :
			  (data_[0] + alpha));
	} else {
	  return ((data_[0] < alpha) ?
			  (data_[0] + HALF_PI) :
			  (data_[0] - alpha));
	}
  } else {
	return data_[4];
  }
}


double
RN_Curve::endTangentAngle()
{
  if (arc_) {
	const double alpha = TWO_PI - HALF_PI;
	if (arc_->bulge() > BULGE_EPSILON) {
	  return ((data_[2] > HALF_PI) ?
			  (data_[2] - HALF_PI) :
			  (data_[2] + alpha));
	} else {
	  return ((data_[2] < alpha) ?
			  (data_[2] + HALF_PI) :
			  (data_[2] - alpha));
	}
  } else {
	return data_[4];
  }
}


double
RN_Curve::tangentAngle(double r)
{
  if (arc_) {
	double alpha;
	if (arc_->bulge() > BULGE_EPSILON) {
	  alpha = data_[0] + data_[4] * (1.0 - r) - HALF_PI;
	} else {
	  alpha = data_[0] + data_[4] * (1.0 - r) + HALF_PI;
	}
	if (alpha < 0.0) alpha += TWO_PI;
	else if (alpha > TWO_PI) alpha -= TWO_PI;
	return alpha;
  } else {
	return data_[4];
  }
}


// Return the angle from center to position r (0 <= r <= 1).

double
RN_Curve::angleFromCenter(double r)
{
  double alpha;
  if (arc_) {
	alpha = data_[0] + data_[4] * (1.0 - r);
  } else {
	alpha = data_[4] + HALF_PI;
  }
  if (alpha < 0.0) alpha += TWO_PI;
  else if (alpha > TWO_PI) alpha -= TWO_PI;
  return alpha;
}


double
RN_Curve::radius(double r)
{
  if (arc_) {
	return (r * (data_[1] - data_[3]) + data_[3]);
  } else {
	return DBL_INF;
  }
}


// Find the containing rectangle

float RN_Curve::min_x()
{
  double x = Min(startPnt().x(), endPnt().x());

  if (!arc_) return x;

  x -= center().x();
  double alpha1, alpha2;

  // Let the arc be counter clockwise

  if (arcAngle() > 0) {
	alpha1 = startAngle();
	alpha2 = endAngle();
  } else {
	alpha1 = endAngle();
	alpha2 = startAngle();
  }

  int pass = alpha1 < ONE_PI && ONE_PI < alpha2;
  if (alpha1 < alpha2 && pass || alpha1 > alpha2 && !pass) {
	x = Min(-radius(), x);
  }

  return (x += center().x());
}

float RN_Curve::min_y()
{
  const double beta = 3.0 * HALF_PI;
  double y = Min(startPnt().y(), endPnt().y());

  if (!arc_) return y;

  y -= center().y();
  double alpha1, alpha2;

  // Let the arc be counter clockwise

  if (arcAngle() > 0) {
	alpha1 = startAngle();
	alpha2 = endAngle();
  } else {
	alpha1 = endAngle();
	alpha2 = startAngle();
  }

  int pass = alpha1 < beta && beta < alpha2;
  if (alpha1 < alpha2 && pass || alpha1 > alpha2 && !pass) {
	y = Min(-radius(), y);
  }

  return (y += center().y());
}


float RN_Curve::max_x()
{
  double x = Max(startPnt().x(), endPnt().x());

  if (!arc_) return x;

  x -= center().x();
  double alpha1, alpha2;

  // Let the arc be counter clockwise

  if (arcAngle() > 0) {
	alpha1 = startAngle();
	alpha2 = endAngle();
  } else {
	alpha1 = endAngle();
	alpha2 = startAngle();
  }

  if (alpha1 > alpha2) {
	x = Max(radius(), x);
  }

  return (x += center().x());
}


float RN_Curve::max_y()
{
  double y = Max(startPnt().y(), endPnt().y());

  if (!arc_) return y;

  y -= center().y();
  double alpha1, alpha2;

  // Let the arc be counter clockwise

  if (arcAngle() > 0) {
	alpha1 = startAngle();
	alpha2 = endAngle();
  } else {
	alpha1 = endAngle();
	alpha2 = startAngle();
  }

  int pass = alpha1 < HALF_PI && HALF_PI < alpha2;
  if (alpha1 < alpha2 && pass || alpha1 > alpha2 && !pass) {
	y = Max(radius(), y);
  }

  return (y += center().y());
}
