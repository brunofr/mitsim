//-*-c++-*------------------------------------------------------------
// NAME: A parser for bus stop parameters 
// AUTH: Qi Yang & Daniel Morgan
// FILE: RN_BusCommPrmTable.C
// DATE: Fri Nov 30 16:59:15 2001
//--------------------------------------------------------------------

#include <algorithm>

#include <Tools/SimulationClock.h>
#include <Tools/ToolKit.h>
#include <IO/Exception.h>

#include "RN_BusCommPrmParser.h"
#include "RN_BusCommPrmTable.h"
#include "RN_BusRouteTable.h"
#include "BusAssignmentTable.h"
#include "RoadNetwork.h"
using namespace std;

// theBusCommPrmTable will be initialized in the ParseBusCommPrms() func in
// TS_Setup.C

RN_BusCommPrmTable * theBusCommPrmTable = NULL;

char * RN_BusCommPrmTable::name_ = NULL;

RN_BusCommPrmTable::RN_BusCommPrmTable()
  : parser_(NULL), nextTime_(-DBL_INF), nConditions_(0)
{
   theBusCommPrmTable = this;
}

// Open bus communications parameter table file and create a parameter parser

void RN_BusCommPrmTable::open(double start_time)
{
   const char *fn = ToolKit::optionalInputFile(name_);
   if (!fn) theException->exit(1);
   Copy(&name_, fn) ;
   startTime_ = start_time;

   // Parser will be created in read()

   if (parser_) delete parser_ ;
   parser_ = NULL ;
}

// Read bus sensing parameter tables for next time period

double RN_BusCommPrmTable::read()
{
  const char *fn ;
  if (parser_) {				// continue parsing of current file
	fn = NULL ;
  } else {						// open the  new file
	parser_ = new RN_BusCommPrmParser(this);
	fn = name_;
  }
  while (nextTime_ <= theSimulationClock->currentTime()) {
	parser_->parse(fn);
  }
  return nextTime_;
}

int
RN_BusCommPrmTable::nConditions()
{
  return nConditions_;
}

int
RN_BusCommPrmTable::addCondition(int rid, int code)  // unconditional
{
  vector<int>::iterator i;
  i = find_if(routeID_.begin(), routeID_.end(), CodeEqual(rid));

  if (i != routeID_.end()) {  // condition already exists for bus route, overwrite it ...
    for (int j = 0; j < nConditions(); j ++) {
      if (routeID_[j] == rid) {
        conditionCode_[j] = code;
        minLoadThresh_[j] = 0;         // dummy value, no conditions
	maxAllowSchedDev_[j] = 9999.;  // dummy value, no conditions
	maxAllowHW_[j] = 9999.;        // dummy value, no conditions
      }
    }
  }
  else {  // condition does not yet exist for bus route, add it ...
    nConditions_++;
    routeID_.push_back(rid); 
    conditionCode_.push_back(code);
    minLoadThresh_.push_back(0);         // dummy value, no conditions
    maxAllowSchedDev_.push_back(9999.);  // dummy value, no conditions
    maxAllowHW_.push_back(9999.);        // dummy value, no conditions
  }
  return 0;
}

int
RN_BusCommPrmTable::addCondition(int rid, int code, int x)
{
  vector<int>::iterator i;
  i = find_if(routeID_.begin(), routeID_.end(), CodeEqual(rid));

  if (i != routeID_.end()) {  // condition already exists for bus route, overwrite it ...
    for (int j = 0; j < nConditions(); j ++) {
      if (routeID_[j] == rid) {
        conditionCode_[j] = code;
        minLoadThresh_[j] = x;
	maxAllowSchedDev_[j] = 9999.; // dummy value, only load condition applies
	maxAllowHW_[j] = 9999.;        // dummy value, only load condition applies
      }
    }
  }
  else {  // condition does not yet exist for bus route, add it ...
    nConditions_++;
    routeID_.push_back(rid); 
    conditionCode_.push_back(code);
    minLoadThresh_.push_back(x);
    maxAllowSchedDev_.push_back(9999.);  // dummy value, only load condition applies
    maxAllowHW_.push_back(9999.);        // dummy value, only load condition applies
  }
  return 0;
}

int
RN_BusCommPrmTable::addCondition(int rid, int code, double y)
{
  vector<int>::iterator i;
  i = find_if(routeID_.begin(), routeID_.end(), CodeEqual(rid));

  if (i != routeID_.end()) {  // condition already exists for bus route, overwrite it ...
    for (int j = 0; j < nConditions(); j ++) {
      if (routeID_[j] == rid) {
        conditionCode_[j] = code;
        minLoadThresh_[j] = 0;           // dummy value, only sched OR headway condition applies
        if (code == 2) {
          maxAllowSchedDev_[j] = y;  
          maxAllowHW_[j] = 9999.;        // dummy value, only schedule condition applies
        } else {
          maxAllowSchedDev_[j] = 9999.;  // dummy value, only headway condition applies
          maxAllowHW_[j] = y;
	  if (theBusRouteTable->findBusRoute(rid)->designHeadway() < 1) {
            cerr << "Error:: Route <" << rid << "> does not have a design headway, "
		 << "but a headway priority condition was specified.";
            return -1;
          }
	}        
      }
    }
  }
  else {  // condition does not yet exist for bus route, add it ...  
    nConditions_++;
    routeID_.push_back(rid); 
    conditionCode_.push_back(code);
    minLoadThresh_.push_back(0);           // dummy value, only sched OR headway condition applies
    if (code == 2) {
      maxAllowSchedDev_.push_back(y);  
      maxAllowHW_.push_back(9999.);        // dummy value, only schedule condition applies
    } else {
      maxAllowSchedDev_.push_back(9999.);  // dummy value, only headway condition applies
      maxAllowHW_.push_back(y);      
      if (theBusRouteTable->findBusRoute(rid)->designHeadway() < 1) {
        cerr << "Error:: Route <" << rid << "> does not have a design headway, "
	     << "but a headway priority condition was specified.";
        return -1;
      }  
    }
  }
  return 0;
}

int
RN_BusCommPrmTable::addCondition(int rid, int code, int x, double y)
{
  vector<int>::iterator i;
  i = find_if(routeID_.begin(), routeID_.end(), CodeEqual(rid));

  if (i != routeID_.end()) {  // condition already exists for bus route, overwrite it ...
    for (int j = 0; j < nConditions(); j ++) {
      if (routeID_[j] == rid) {
        conditionCode_[j] = code;
        minLoadThresh_[j] = x;
        if (code == 4) {
          maxAllowSchedDev_[j] = y;  
          maxAllowHW_[j] = 9999.;        // dummy value, load and schedule conditions apply
        } else {
          maxAllowSchedDev_[j] = 9999.;  // dummy value, load and headway conditions apply
          maxAllowHW_[j] = y;
	  if (theBusRouteTable->findBusRoute(rid)->designHeadway() < 1) {
            cerr << "Error:: Route <" << rid << "> does not have a design headway, "
		 << "but a headway priority condition was specified.";
            return -1;
          }
	}        
      }
    }
  }
  else {  // condition does not yet exist for bus route, add it ...  
    nConditions_++;
    routeID_.push_back(rid); 
    conditionCode_.push_back(code);
    minLoadThresh_.push_back(x);
    if (code == 4) {
      maxAllowSchedDev_.push_back(y);  
      maxAllowHW_.push_back(9999.);        // dummy value, load and schedule conditions apply
    } else {
      maxAllowSchedDev_.push_back(9999.);  // dummy value, load and headway conditions apply
      maxAllowHW_.push_back(y);  
      if (theBusRouteTable->findBusRoute(rid)->designHeadway() < 1) {
        cerr << "Error:: Route <" << rid << "> does not have a design headway, "
	     << "but a headway priority condition was specified.";
        return -1;
      }        
    }
  }
  return 0;
}

void
RN_BusCommPrmTable::print(ostream &os)
{
    //register int i;
   os << "// Bus sensing parameter table generated from file <" << name_ << "> on "
      << TimeTag() << " by "
      << UserName();
   os << "\n}" << endl;
}

// Called by parser to setup the parameter tables and update time

int 
RN_BusCommPrmTable::initBusCommPrms(double s)
{
   if (nextTime_ > -86400.0 && ToolKit::debug()) {
      cout << " Bus stop parameters parsed at "
	   << theSimulationClock->convertTime(nextTime_)
	   << "." << endl;
   }

   nextTime_ = s;
   return 0;
}

bool
RN_BusCommPrmTable::conditionSatisfied(int bc)
{
  BusAssignment *ba = theBusAssignmentTable->findBusAssignment(bc);
  int rid = ba->route()->code();

  vector<int>::iterator i;
  i = find_if(routeID_.begin(), routeID_.end(), CodeEqual(rid));

  if (i != routeID_.end()) {  // a condition exists for buses on this route
    for (int j = 0; j < nConditions(); j ++) {
      if (routeID_[j] == rid) {
	int cond = conditionCode_[j];
	switch(cond) {
        case 0:			// unconditional priority for this route
              {
		return true;
	      }
        case 1:			// load is only condition
              {
		if (ba->load() > minLoadThresh_[j]) return true; // load is above min threshold
		else return false;
	      }
        case 2:			// schedule is only condition
              {
		if (ba->schedDeviation() < maxAllowSchedDev_[j]) return true; // behind schedule
		else return false;
	      }
        case 3:			// headway is only condition
              {
		if (ba->prevailingHeadway() > maxAllowHW_[j]) return true; // headway has become too large
		    else return false;
	      }
        case 4:			// conditions are load and schedule
              {
		if (ba->load() > minLoadThresh_[j] &&
                     ba->schedDeviation() < maxAllowSchedDev_[j]) 
                     return true; // load is above min threshold and bus is sufficiently behind schedule
		else return false;
	      }
        case 5:			// conditions are load and headway
              {
		if (ba->load() > minLoadThresh_[j] &&
                     ba->prevailingHeadway() > maxAllowHW_[j]) 
                     return true; // load is above min threshold and headwy has become too long
		else return false;
	      }
	}
      }
    }
  }
  else return false;  // no condition exists for buses on this route
}
