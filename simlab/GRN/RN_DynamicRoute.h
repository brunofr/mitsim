//-*-c++-*------------------------------------------------------------
// NAME: Route choice
// NOTE: Stores time variant routing information
// AUTH: Qi Yang
// FILE: RN_DynamicShortestPath.h
// DATE: Tue Sep 24 09:25:26 1996
//--------------------------------------------------------------------

#ifndef RN_DYNAMIC_ROUTE_HEADER
#define RN_DYNAMIC_ROUTE_HEADER

#include "RN_Route.h"

class GenericVariable;

class RN_DynamicRoute : public RN_Route
{
   public:

      RN_DynamicRoute(const char *);
      RN_DynamicRoute(const RN_DynamicRoute &sp);
      ~RN_DynamicRoute() { }

      int pathPeriods() { return infoPeriods_; }

      // Travel time from upstream end of olink to dnode

      float upRouteTime(int olink, int dnode, double timesec = 0.0);

      // Travel time from dnstream end of olink to dnode

      float dnRouteTime(int olink, int dnode, double timesec = 0.0);

	  static char* file(int i) { return files_[i]; }
	  static char** fileptr(int i) { return files_ + i; }
	  static int availability() { return availability_; }
	  static void availability(int i) { availability_ = i; }

	  static int setFileNamesOfTravelTimeTables(GenericVariable &gv);
	  static void parseTravelTimeTables(char tag);

   protected:

      void findShortestPathTrees(double start, double end);

   private:

	  static char* files_[2];
	  static int availability_;
};

#endif
