//-*-c++-*------------------------------------------------------------
// FILE: RN_CtrlStation.C
// DATE: Sat Oct 21 16:51:05 1995
// Signal stations are stored in a sorted list in each link according
// to their distance from the end of the link.  The sorting is in
// descending order (Upstream=LongerDistance=Top)
//--------------------------------------------------------------------

#include <Tools/Math.h>

#include <Tools/ToolKit.h>

#include "RN_CtrlStation.h"
#include "RoadNetwork.h"
#include "RN_Link.h"
#include "RN_Segment.h"
#include "RN_Lane.h"
#include "RN_Signal.h"
#include "Parameter.h"
#include "Constants.h"
using namespace std;

float RN_CtrlStation::maxVisibility_ = 0.0;

// This function overloads the virtual function defined in
// CodedObject(now sorting objects in descending order)

int RN_CtrlStation::cmp(CodedObject*other) const
{
   RN_CtrlStation *ctrl = (RN_CtrlStation*)other;
   if (distance_ < ctrl->distance_) return 1;
   else if (distance_ > ctrl->distance_) return -1;
   else return 0;
}

// Returns the link contains the ctrleillance station

RN_Link*
RN_CtrlStation::link()
{
   return segment_->link();
}


// Returns pointer to the signal in ith lane. It may be NULL if there
// is no signal in the ith lane.

RN_Signal*
RN_CtrlStation::signal(int i)
{
   if (isLinkWide())
	  return signals_[0];
   else
	  return signals_[i];
}

void RN_CtrlStation::signal(int i, RN_Signal *s)
{
   if (isLinkWide()) i = 0;
   signals_[i] = s;
}

short int
RN_CtrlStation::nLanes()
{
   return segment_->nLanes();
}


// This function is called by RN_Parser to set information of a
// ctrleillance station from network database.  It returns zero if no
// error, -1 if fatal error or a positive number if warning error.

int
RN_CtrlStation::init(int ty, float vis, int seg, float pos)
{
   if (ToolKit::debug()) {
      cout << indent << "<"
		   << ty << endc << vis << endc
		   << seg << endc << pos
		   << ">" << endl;
   }

   int err = initWithoutInsert(ty, vis, seg, pos);
   if (err < 0) return err;

   addIntoNetwork();

   return err;
}

int RN_CtrlStation::initWithoutInsert(int ty, float vis, int seg, float pos)
{
   switch (ty) {
	  case CTRL_PS:
	  case CTRL_TS:
	  case CTRL_VSLS:
	  case CTRL_VMS:
		 {
			type_ = (ty | CTRL_LINKWIDE);
			break;
		 }
	  default:
		 {
			type_ = ty;
			break;
		 }
   }

   if (!(segment_ = theNetwork->findSegment(seg))) {
      cerr << "Error:: Unknown segment <" << seg << ">. ";
      return -1;
   }

   vis *= theBaseParameter->lengthFactor();
   vis *= theBaseParameter->visibilityScaler();
   visibility(vis);

   position_ = 1.0 - pos;	// position in % from segment end
  
   distance_ = segment_->distance() + position_ * segment_->length();
   
   if (isLinkWide()) {
      signals_.reserve(1);
	  signals_[0] = NULL;
   } else {
	  int n = segment_->nLanes();
      signals_.reserve(n);
	  while (n > 0) {
		 n --;
		 signals_[n] = NULL;
	  }
   }
  
   return 0;
}

void RN_CtrlStation::visibility(float d)
{
   visibility_ = d;
   if (visibility_ > maxVisibility_) {
	  maxVisibility_ = visibility_;
   }
}

void RN_CtrlStation::addIntoNetwork()
{
   static int serial_no = 0;
   link()->ctrlStationList().insert(this);
   theNetwork->lastCtrlStation(this);
   code_ = ++serial_no;
}


void
RN_CtrlStation::print(ostream &os)
{
   os << indent << "{";
   if (type_ != CTRL_TOLLBOOTH) {
	  os << type_ << endc;
   }
   os << (visibility_ / Parameter::lengthFactor()) << endc
      << segment_->code() << endc 
      << (1.0 - position()) << endl;
   
   if (isLinkWide()) {
      signals_[0]->print(os);
   } else {
      int num = segment_->nLanes();
      for (register int i = 0; i < num; i ++) {
		 if (signals_[i]) {
			signals_[i]->print(os);
		 } else {
			os << indent << indent << "% none" << endl;
		 }
      }
   }
   os << indent << "}" << endl;
}


