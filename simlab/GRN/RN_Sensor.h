//-*-c++-*------------------------------------------------------------
// FILE: RN_Sensor.h -- Contains class defination for a abstract
//       sensor class and several commonly used sensors
// AUTH: Qi Yang
// DATE: Fri Oct 20 22:47:31 1995
//--------------------------------------------------------------------

#ifndef RN_SENSOR_HEADER
#define RN_SENSOR_HEADER

#include <iostream>

#include <Templates/Object.h>
#include <IO/IOService.h>

#include "Constants.h"
#include "WcsPoint.h"

class RN_BisonParser;
class RoadNetwork;
class RN_Link;
class RN_Segment;
class RN_Lane;
class RN_SurvStation;
class RN_Vehicle;
class Reader;

// NOTE! The following forward declarations are required because of
// the multiple inheritance structure problem defined at the end of
// RN_Sensor

class DRN_DrawingArea;
class WcsPoint;
class TS_Vehicle;

// Abstract Sensor

class RN_Sensor : public CodedObject
{
  friend class RN_BisonParser;
  friend class RoadNetwork;

protected:

  static int sorted_;	        // 1 = sorted by code

  int index_;		        // index in array 
  RN_Lane* lane_;		// pointer to lane
  RN_SurvStation* station_;	// station 
  float prob_;		        // probability to function properly
  unsigned int state_;	        // occupied/working etc
  int intervalType_;	        //IEM(Apr26) 0 for default, >=1 for additional sensor
			        //           types, as defined in master.mitsim

protected:

  int	count_;		// counts
  float	occupancy_;	// occupied time
  float	speed_;		// sum of speeds

public:

  RN_Sensor()  //IEM(Apr26) added intervalType_ to be defaulted to 0
	: CodedObject(), lane_(NULL), station_(NULL), intervalType_(0),
	  index_(-1), state_(0), count_(0), occupancy_(0.0), speed_(0.0) { }
  virtual ~RN_Sensor() { }

  static inline int sorted() { return sorted_; }

  unsigned int clearState(unsigned int flag);
  unsigned int setState(unsigned int flag = 0);

  void addState(unsigned int flag); // used in incident detection

  inline int state() { return state_; }

  // Iterator on the global array
  RN_Sensor* prev();
  RN_Sensor* next();

  inline int index() { return index_; }
  inline RN_SurvStation* station() { return station_;}

  inline int intervalType() {return intervalType_;} //IEM(Apr26)

  RN_Lane* lane() { return lane_; }
  RN_Segment* segment();
  RN_Link* link();
  virtual float position();

  int isLinkWide();
  int type();
  unsigned int tasks(unsigned int flag = 0x0FFFFFFF);
  unsigned int atask();
  unsigned int itask();
  unsigned int stask();
  virtual float distance();
  virtual float zoneLength();
  virtual float width();

  virtual int init(int code, float p, int lane = -1, int interval = 0);
						//IEM(Apr26) added interval
						//IEM(Jun15) set it to 0
  virtual void print(std::ostream &os = std::cout);

  virtual int   count() { return count_; }
  virtual int   flow() { return count_; }
  virtual float occupancy() { return occupancy_; }
  virtual float speed() { return speed_; }
  virtual float measurement() { return occupancy_; }

  // used for traffic sensors in incident detection

  virtual int getRegion(double n) { return -1; }
  virtual int sensorIncState() { return 0; }
  virtual int incClrPersistCount() { return 0; }
  virtual int incDcrPersistCount() { return 0; }

  virtual float occPrev(int i) { return -1; }
  virtual float spdPrev(int i) { return -1; }
  virtual void setOccPrev(int i, float f){}
  virtual void setSpdPrev(int i, float f){}

  // WARNING!!
  // ----------------
  // The following functions were added because Peter and Yang Qi
  // are damn sick of this thing and they are becoming lazy!
  // These functions are nonsensical in the context of RN_Sensor.
  // They are here to avoid casting to derived classes which is
  // not possible based on the multiple inheritance / virtual
  // inheritance we have used for sensors.  See Section 10.6c of
  // The Annotated C++ Reference Manual (pg 227) by Ellis and
  // Stroustrup for more info.
  // -----------------
  // WARNING!!

  // -- DRN_Sensor Functions --
      
  virtual int draw(DRN_DrawingArea *) { return 0; }
  virtual void calcGeometricData() { }
  virtual WcsPoint& center() { return theWcsDummyPoint; }

  // -- TS_Sensor Functions --

  virtual void calcActivatingData(TS_Vehicle*, float, float, float) {}
  virtual void calcPassingData(TS_Vehicle*, float, float, float ) {}
  virtual void calcSnapShotData() {}
  virtual void resetSensorReadings() {
	count_		= 0;
	occupancy_	= 0.0;
	speed_		= 0.0;
	clearState(SENSOR_ACTIVATED);
  }
  virtual void convertSensorReadings() { }

  // -- TC_Sensor functions --

  virtual void send(IOService &) { }
  virtual void receive(IOService &) { }
  virtual void send_i(IOService &, RN_Vehicle *) { }
  virtual void receive_i(IOService &) { }
  virtual void send_s(IOService &, RN_Vehicle *) { }
  virtual void receive_s(IOService &) { }

  virtual void write(std::ostream &os) { }
  virtual void write_i(std::ostream &os, RN_Vehicle *) { }
  virtual void write_s(std::ostream &os, RN_Vehicle *) { }

  // -- TMS_Sensor functions --
      
  virtual void loadDbFlow(Reader &) { }
  virtual void loadDbSpeed(Reader &) { }
};

#endif
