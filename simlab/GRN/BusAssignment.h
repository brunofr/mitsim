//-*-c++-*------------------------------------------------------------
// FILE: BusAssignment.h
// DATE: Thu Oct 19 17:29:19 1995
//--------------------------------------------------------------------

#ifndef BUSASSIGNMENT_HEADER
#define BUSASSIGNMENT_HEADER

#include <iosfwd>
//#include <vector>

#include <Templates/Object.h>

#include "RN_BusRun.h"
#include "RN_BusSchedule.h"

class RoadNetwork;
class RN_Link;
class RN_Node;
class BusAssignmentParser;
class BusAssignmentTable;
class RN_BusRun;
class RN_LinkTimes;
class RN_BusStop;
class ArgsParser;

class BusAssignment : public CodedObject
{
      friend class BusAssignmentParser;
      friend class BusAssignmentTable;
      friend class RoadNetwork;

  // Define "arrival" as a a scheduled arrival time at any stop

   protected:

      static int sorted_;            // 1=sorted 0=arbitrary order
      int index_;	  	     // index in the array;
      int tripTracker_;              // used to keep track of current trip
      int arrivalTracker_;           // used to keep track of next scheduled arrival time
      int busID_;                    // user-assigned bus ID
      int busType_;                  // type of bus
      int load_;                     // number of passengers on bus
      int leftOrRight_;              // direction of approaching bus stop
      RN_BusSchedule* currentTrip_;  // the bus' current trip
      RN_BusRun* run_;               // run assigned to bus
      double schedDeviation_;        // sched arrival time - current time at last stop arrival
      double headway_;               // time headway between previous bus and this one at last stop arrival
      double nextSchedArrival_;      // time the bus is scheduled to arrive at next downstream stop
      int priorityRequest_;          // id of last sensor at which the bus requested priority
      int priorityGranted_;          // id of last sensor at which the bus requested priority

   public:
	
      BusAssignment() : CodedObject() { }
      ~BusAssignment() { }

      static inline int sorted() { return sorted_; }

      RN_BusSchedule* currentTrip();
      RN_BusRoute* route();
      void updateCurrentTrip();
      void updateNextArrival();

      int runcode() { return run_->code(); }
      int origin() { return run_->oricode(); }
      int destination() { return run_->descode(); }
      int load() { return load_; }
      int busType() { return busType_; }
      double nextSchedArrival() { return nextSchedArrival_; }

      // Dan: the following are recorded and overwritten at each bus stop
      void updateSchedDeviation(int sd);
      void updateHeadway(int hw) { headway_ = hw; }
      void updateBusLoad(int l);
      void updatePriorityRequest(int c);
      void updatePriorityGranted(int p);

      double schedDeviation() { return schedDeviation_; }
      double prevailingHeadway() { return headway_; }
      int priorityRequest() { return priorityRequest_; }
      int priorityGranted() { return priorityGranted_; }
      double designHeadway();

      void setLeftOrRight(int lor) { leftOrRight_ = lor; }
      int leftOrRight() { return leftOrRight_; }

      // These two are called by BusAssignmentParser

      virtual int init(int code, int type, int run);
      virtual int initBRT(int code, int type, int run, double hw);
      virtual int init(int code, int type, int run, int load);

      virtual void print(std::ostream &os);

};

#endif
