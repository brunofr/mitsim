//-*-c++-*------------------------------------------------------------
// NAME: A parser for bus run table 
// AUTH: Qi Yang & Daniel Morgan
// FILE: RN_BusRunTable.C
// DATE: Fri Nov 30 16:59:15 2001
//--------------------------------------------------------------------

#include <algorithm>

#include <Tools/SimulationClock.h>
#include <Tools/ToolKit.h>
#include <IO/Exception.h>

#include "RN_BusRunParser.h"
#include "RN_BusRunTable.h"
#include "RoadNetwork.h"
#include "RN_Vehicle.h"
#include "RN_Link.h"
#include "RN_Path.h"
#include "OD_Cell.h"
#include "RN_LinkTimes.h"
using namespace std;

// theBusRunTable will be initialized in the ParseBusRuns() func in
// TS_Setup.C or MesoTS_Setup.C

RN_BusRunTable * theBusRunTable = NULL;

char * RN_BusRunTable::name_ = NULL;

RN_BusRunTable::~RN_BusRunTable()
{
   if (name_) {
	  delete [] name_;
	  name_ = NULL;
   }
}

RN_Path*
RN_BusRunTable::newPath()
{
   return new RN_Path;
}

RN_BusRun*
RN_BusRunTable::newBusRun()
{
   return new RN_BusRun;
}

int
RN_BusRunTable::addBusRun(int code)
{
   RN_BusRun *r = newBusRun();
   runs_.push_back(r);
   return r->init(code);
}

int
RN_BusRunTable::addPath(int code, int ori, int des)
{
   RN_Path *p = newPath();
   paths_.push_back(p);
   return p->init(code, ori, des);
}

RN_Path* 
RN_BusRunTable::findPath(int c)
{
   vector<RN_Path*>::iterator i;
   i = find_if(paths_.begin(), paths_.end(), CodeEqual(c));
   if (i != paths_.end()) return *i;
   else return NULL;
}

RN_BusRun* 
RN_BusRunTable::findBusRun(int c)
{
   vector<RN_BusRun*>::iterator i;
   i = find_if(runs_.begin(), runs_.end(), CodeEqual(c));
   if (i != runs_.end()) return *i;
   else return NULL;
}

void
RN_BusRunTable::print(ostream &os)
{
   register int i;
   os << "// Bus bus run table generated from file <" << name_ << "> on "
      << TimeTag() << " by "
      << UserName() << endl << endl;
   os << nBusRuns() << indent << "// Number of bus runs" << endl;
   os << "{" << endl;
   for (i = 0; i < nBusRuns(); i ++) {
      run(i)->print(os);
   }
   os << "}" << endl;
}

void
RN_BusRunTable::setBusRunPointers()
{
   for (register int i = 0; i < nBusRuns(); i ++) {
         run(i)->setBusRunPointers();
   }
}




 
