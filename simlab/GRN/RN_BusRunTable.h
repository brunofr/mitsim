//-*-c++-*------------------------------------------------------------
// NAME: A parser for bus run table
// AUTH: Qi Yang
// FILE: RN_BusRunTable.h
// DATE: Fri Nov 30 16:55:50 2001
//--------------------------------------------------------------------

#ifndef RN_BUSRUNTABLE_HEADER
#define RN_BUSRUNTABLE_HEADER

#include <iostream>
#include <vector>

#include "RN_BusRun.h"
#include "RN_BusSchedule.h"
#include "RN_Path.h"

const int NUM_BUSRUNS = 10;	// Default initial num of runs

class RN_BusRunParser;
class ArgsParser;
class RN_Vehicle;

class RN_BusRunTable
{
      friend class ArgsParser;
      friend class RN_BusRunParser;
      friend class RN_Vehicle;

   protected:

      std::vector <RN_BusRun*> runs_; // array of pointers to bus runs
      std::vector <RN_Path*> paths_;  // array of pointers to bus paths

   public:

      static char * name_;	// file name
      static char * name() { return name_; }
      static char ** nameptr() { return &name_; }

      RN_BusRunTable() {}

      virtual ~RN_BusRunTable();

      int nPaths() { return paths_.size(); }  

      inline RN_Path* lastPath() { return paths_.back(); }

      RN_Path* path(int i) {
		         if (i < 0) return NULL;
		         else if (i >= nPaths()) return NULL;
		         else return paths_[i];
	  }

      virtual int addPath(int code, int orinode, int desnode);

      virtual RN_Path* newPath();
      RN_Path* findPath(int c);

      int nBusRuns() { return runs_.size(); }  

      inline RN_BusRun* lastBusRun() { return runs_.back(); }

      RN_BusRun* run(int i) {
		         if (i < 0) return NULL;
		         else if (i >= nBusRuns()) return NULL;
		         else return runs_[i];
	  }

      virtual int addBusRun(int code);

      virtual RN_BusRun* newBusRun();
      RN_BusRun* findBusRun(int c);

      virtual void print(std::ostream &os = std::cout);

      void setBusRunPointers();

};

extern RN_BusRunTable * theBusRunTable;

#endif
