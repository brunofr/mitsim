//-*-c++-*------------------------------------------------------------
// FILE: RN_BusSchedule.C
// DATE: Thu Oct 19 17:28:39 1995
//--------------------------------------------------------------------

#include <algorithm>

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>

#include "RoadNetwork.h"
#include "RN_Label.h"
#include "RN_Link.h"
#include "RN_Node.h"
#include "RN_BusSchedule.h"
#include "RN_BusScheduleTable.h"
#include "RN_BusRouteTable.h"
#include "RN_BusStop.h"
#include "RN_LinkTimes.h"
using namespace std;

int RN_BusSchedule::sorted_ = 1;	// element is sorted

double
RN_BusSchedule::nextArrival(int i)
{
   if (i < nArrivals() - 1) return arrivals_[i + 1];
   else return -1;
}

// Called by RN_BusScheduleParser to initialize a bus schedule

int
RN_BusSchedule::init(int c)
{
   static int idx = 0;
   static int last = -1;
   code_ = c;
   if (sorted_ && c <= last) sorted_ = 0;
   else last = c;

   index_ = idx ++;

   return 0;
}

// Called by RN_BusScheduleParser to add a scheduled arrival time to a bus schedule

int
RN_BusSchedule::addArrival(double arrivaltime)
{
   double at = arrivaltime;
   double prevarrival = lastArrival();
   arrivals_.push_back(at);
   return 0;
}


void
RN_BusSchedule::print(ostream &os)
{
   register int i;
   os << indent << "{";
   os << code_ << endc;
   os << nArrivals() << "{";
   for (i = 0; i < nArrivals(); i ++) {
      os << arrivals_[i] << endc;
   }
   os << "}" << endc << endl;
   os << nArrivals() << "{";
   os << "}" << endl;
}

RN_BusRoute*
RN_BusSchedule::tripRoute()
{
  return theBusRouteTable->findBusRoute(routeID_);
}

int
RN_BusSchedule::addBusStop(int stopcode)
{
   RN_BusStop *bsptr = theNetwork->findBusStop(stopcode);
   if (!bsptr) {
      cerr << "Error:: Unkonwn bus stop code <" << stopcode << ">. ";
      return -1;
   }
   RN_BusStop *prevbusstop = lastBusStop();
   stops_.push_back(bsptr);
   return 0;
}

RN_BusStop* 
RN_BusSchedule::findBusStop(int c)
{
   vector<RN_BusStop*>::iterator i;
   i = find_if(stops_.begin(), stops_.end(), CodeEqual(c));
   if (i != stops_.end()) return *i;
   else if (nBusStops() < 1) {
     // Dan: if no stops are defined for the individual trip, then defaults to list of stops on route
     return tripRoute()->findTimepoint(c);  // returns NULL if stop not in route definition
   }
   else return NULL;
}

