//
// RN_BusCommPrmParser.y
// by Qi Yang & Daniel Morgan
//

%name   RN_BusCommPrmParser
%define ERROR_BODY	DEF_ERROR_BODY
%define LEX_BODY	DEF_LEX_BODY

%define CONSTRUCTOR_PARAM  RN_BusCommPrmTable* pContainer_
%define CONSTRUCTOR_INIT   : _pContainer(pContainer_)   \
                           ,_condcode(0)                \
                           ,_cond1(0)                   \
                           ,_cond2(0)                \

%define MEMBERS  DEF_MEMBERS                            \
   RN_BusCommPrmTable* _pContainer ;                    \
   int _condcode ;                                      \
   int _cond1 ;                                      \
   double _cond2 ;                                      \

%union {
  long        type_i ;
  double      type_f ;
  const char* type_s ;
}

%token <type_s> LEX_EQUAL
%token <type_s> LEX_COMMA
%token <type_s> LEX_COLON
%token <type_s> LEX_SEMICOLON
%token <type_s> LEX_OAB
%token <type_s> LEX_CAB
%token <type_s> LEX_OBB
%token <type_s> LEX_CBB
%token <type_s> LEX_OCB
%token <type_s> LEX_CCB
%token <type_i> LEX_INT
%token <type_f> LEX_REAL
%token <type_s> LEX_TIME
%token <type_s> LEX_PAIR
%token <type_s> LEX_TAG
%token <type_s> LEX_STRING
%token <type_s> LEX_DUP
%token <type_s> LEX_END

%header{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <IO/Exception.h>
#include <Tools/ToolKit.h>  
#include <Tools/Scanner.h>  
#include <Tools/SimulationClock.h>
#include <Tools/Math.h>

#include <GRN/RN_BusCommPrmTable.h>
%}

//--------------------------------------------------------------------
// Symbols used vehicle table grammer rules.
//--------------------------------------------------------------------

%type <type_f> fnum float_num time string_time
%type <type_s> everything multi_param_tables param_list ccode
%type <type_s> param_table param_table_data route_id multi_route_ids
%type <type_s> cond_without_id one_cond cond_data double_cond
%type <type_s> multi_conds the_end single_cond 
%type <type_s> load sched_hway multi_conds_without_id
%type <type_i> inum

%start everything

%%

//--------------------------------------------------------------------
// Beginning of RN_BusCommPrmTable table grammer rules.
//--------------------------------------------------------------------

everything: multi_param_tables the_end
;

the_end: LEX_END
{
  _pContainer->initBusCommPrms(DBL_INF);
  if (ToolKit::verbose()) {
	cout << "Finished parsing <" << filename() << ">." << endl;
  }
  YYACCEPT;			// return and never back
}
;

multi_param_tables: param_table
| multi_param_tables param_table
;

param_table: param_table_data param_list
;

param_list: LEX_OCB multi_conds LEX_CCB
;

param_table_data: time
{
  // $1=StartTime
  int err_no = _pContainer->initBusCommPrms($1); 
  check(err_no);
  if (_pContainer->nextTime() > theSimulationClock->currentTime()) {
	YYACCEPT;					// return and will come back later
  }
}
;

multi_conds: one_cond
| multi_conds one_cond
;

one_cond: ccode multi_conds_without_id
;

ccode: inum
{
  if (ToolKit::debug()) {
	cout << $1 << endc;
  }
  _condcode = $1;
}
;

multi_conds_without_id: cond_without_id
| multi_conds_without_id cond_without_id
;

cond_without_id: LEX_OCB cond_data LEX_CCB LEX_OCB multi_route_ids LEX_CCB
| LEX_OCB LEX_CCB LEX_OCB multi_route_ids LEX_CCB
;

cond_data: single_cond
| double_cond
;

single_cond: load
| sched_hway
;

load: inum
{
  //$1 = load threshold (if integer) or a headway or schedule condition (if real)
  if (ToolKit::debug()) {
	cout << '{' << $1 << endc << '}';
  }
  _cond1 = $1;
}
;

sched_hway: fnum
{
  //$1 = load threshold (if integer) or a headway or schedule condition (if real)
  if (ToolKit::debug()) {
	cout << '{' << $1 << endc << '}';
  }
  _cond2 = $1;
}
;

double_cond: inum fnum
{
  //$1 = load threshold, $2 = headway or schedule condition
  if (ToolKit::debug()) {
	cout << '{' << $1 << endc << $2 << '}';
  }
  _cond1 = $1;
  _cond2 = $2;
}
;

multi_route_ids: route_id
| multi_route_ids route_id 
;

route_id: inum
{
  //$1 = route id to which condition applies

  int err_no = 0;

  if (_condcode == 0) err_no = _pContainer->addCondition($1, _condcode);
  else if (_condcode == 1) err_no = _pContainer->addCondition($1, _condcode, _cond1);
  else if (_condcode == 2 || _condcode == 3) err_no = _pContainer->addCondition($1, _condcode, _cond2);
  else err_no = _pContainer->addCondition($1, _condcode, _cond1, _cond2);

  check(err_no);
}

//--------------------------------------------------------------------
// Beginning of basic grammer rules.
//--------------------------------------------------------------------

inum: LEX_INT
{
  char *ptr;
  $$ = (int)strtol(value(), &ptr, 0);
}
;

fnum: float_num
| inum
{
  $$ = $1;
}
;

float_num: LEX_REAL
{
  $$ = atof(value());
}
;

time: string_time
| fnum
{
  $$ = $1;
}
;

string_time: LEX_TIME
{
  $$ = theSimulationClock->convertTime(value());
}
;

%%

// Following pieces of code will be verbosely copied into the parser.

%header{
  // empty
%}
