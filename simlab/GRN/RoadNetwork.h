//-*-c++-*------------------------------------------------------------
// RoadNetwork.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef ROADNETWORK_HEADER
#define ROADNETWORK_HEADER

#include <iostream>
#include <vector>
#include <UTL/State3D.h>

#include "RN_WorldSpace.h"
using namespace std;

class RN_Label;
class RN_Node;
class RN_Link;
class RN_Segment;
class RN_Lane;
class RN_Sensor;
class RN_Signal;
class RN_SurvStation;
class RN_CtrlStation;
class RN_TollBooth;
class RN_BusStop; //margaret
class RN_SdFn;
class Reader;


// tomer

class RN_IS;

// **


// These default initial number of objects

const int NUM_LABELS	= 20; 
const int NUM_NODES	= 100;
const int NUM_LINKS	= 200;
const int NUM_SEGMENTS	= 400;
const int NUM_LANES	= 1600;
const int NUM_SENSORS	= 500;
const int NUM_SIGNALS	= 500;
const int NUM_SDFUNCTIONS = 5;

// tomer 

const int NUM_IS	= 200;

// **


class RN_Parser;
class ArgsParser;

class RoadNetwork
{
  friend class RN_Parser;
  friend class ArgsParser;
  friend class RN_Label;
  friend class RN_Node;
  friend class RN_Link;
  friend class RN_Segment;
  friend class RN_Lane;
  friend class RN_Sensor;
  friend class RN_Signal;
  friend class RN_TollBooth;
  friend class RN_BusStop; //margaret
  


  //  friend TS_Vehicle;

  // tomer

  friend class RN_IS;
      
  // **
 
protected:

  char *description_;		// description of the network

  //int nNodes_, nLinks_, nSegments_, nLanes_, nLabels_;
  //int nSensors_, nSignals_;

  int nDestNodes_;
  int nConnectors_, nProhibitors_;
  int nStdSignals_;
  int nTollBooths_;
  int nBusStops_; //margaret
  int nSelectedSegs_;

  vector< RN_Node *> nodes_;
  vector< RN_Link *> links_;
  vector< RN_Segment *> segments_;
  vector< RN_Lane *> lanes_;
  vector< RN_Label *> labels_;

  vector< RN_Sensor *> sensors_;
  vector< RN_Signal *> signals_;
  vector< RN_BusStop *> busstops_;

  vector < vector <RN_Sensor *> > sensorsOfType_; //IEM(May2) each vector of sensors is for one interval type
  virtual void sortSensors() {}; //IEM(May2) Fills sensorsOfType.  Implemented in TS_Network :P

  vector< RN_SdFn *> sdFns_;

  // tomer

vector < RN_IS *> IS_;

  // **


  RN_WorldSpace worldSpace_;

  RN_SurvStation *lastSurvStation_;
  RN_CtrlStation *lastCtrlStation_;

  // Some basic statistics of the network

  double totalLinkLength_;
  double totalLaneLength_;

public:

  int drivingDirection_; // tomer - should be protected but...

  static char *name_;	// network file name
  static char *name() { return name_; }
  static char **nameptr() { return &name_; }

#ifdef EXTERNAL_MOE
  static char *moeSpecFile_; //moe.dat file name
  static char *moeSpecFile() {return moeSpecFile_;}
  static char **moeSpecFileptr() {return &moeSpecFile_;}
#endif 

  RoadNetwork();

  virtual ~RoadNetwork();

  inline RN_WorldSpace& worldSpace() {
	return worldSpace_;
  }

  // These create actual objects and should be overloaded in the
  // derived class if your network objects differ from the default
  // networkk objects

  virtual RN_Label* newLabel();
  virtual RN_Node* newNode();
  virtual RN_Link* newLink();
  virtual RN_Segment* newSegment();
  virtual RN_Lane* newLane(); 
  virtual RN_Sensor* newSensor();
  virtual RN_Signal* newSignal();
  virtual RN_SurvStation* newSurvStation();
  virtual RN_CtrlStation* newCtrlStation();
  virtual RN_TollBooth* newTollBooth();
  virtual RN_BusStop* newBusStopChars(); //margaret

  // tomer

  virtual RN_IS* newIS();

  // **

  void setDrivingDirection(int);

  /*
 {drivingDirection_ =  k;

  cout << "rama: road network.h: k = " << k << " member = " << drivingDirection_ << endl;}
  */

  //  inline void setDrivingDirection(int n) {drivingDirection_ =  n;}

  // These equal to the numbers objects allocated in arrays

  inline int nMaxLabels() { return labels_.capacity(); }
  inline int nMaxNodes() { return nodes_.capacity(); }
  inline int nMaxLinks() { return links_.capacity(); }
  inline int nMaxSegments() { return segments_.capacity(); }
  inline int nMaxLanes() { return lanes_.capacity(); }
  inline int nMaxSensors() { return sensors_.capacity(); }
  inline int nMaxSignals() { return signals_.capacity(); }
  inline int nMaxSdFns() { return sdFns_.capacity(); }


  // tomer - where is the capacity() function?

  inline int nMaxIS() { return IS_.capacity(); }

  // **



  // These are the numbers parsed from network databases

  inline int nLabels() { return labels_.size(); }
  inline int nNodes() { return nodes_.size(); }
  inline int nLinks() { return links_.size(); }
  inline int nSegments() { return segments_.size(); }
  inline int nLanes() { return lanes_.size(); }
  inline int nSensors() { return sensors_.size(); }
  inline int nSensors(int intervalType) { if (nSensors() == 0) {return 0;} else {return sensorsOfType_[intervalType].size();} }
     //IEM(May2) # of sensors of a given interval type
     //IEM(Jul25) If there are no sensors at all, returns 0 no matter what (that way, no segfaults)
  inline int nSignals() { return signals_.size(); }
  inline int nSdFns() { return sdFns_.size(); }

  // tomer

  inline int nIS() { return IS_.size(); }

  // **

  inline int nStdSignals() { return nStdSignals_; }
  inline int nTollBooths() { return nTollBooths_; }
  inline int nBusStops() { return nBusStops_; } //margaret
  inline int nDestNodes() { return nDestNodes_; }

  // This is called when adding incidents
      
  vector < RN_Signal *> & signals() { return signals_; }

  // These takes an index and return pointer to a object

  RN_Label* label(int i) { return labels_[i]; }
  RN_Node* node(int i) { return nodes_[i]; }
  RN_Link* link(int i) { return links_[i]; }
  RN_Segment* segment(int i) { return segments_[i]; }
  RN_Lane* lane(int i) { return lanes_[i]; }
  RN_Sensor* sensor(int i) { return sensors_[i]; }
  RN_Sensor* sensor(int intervalType, int i) { return sensorsOfType_[intervalType][i]; } //IEM(May2)
  RN_Signal* signal(int i) { return signals_[i]; }
  RN_SdFn* sdFn(int i) { return sdFns_[i]; }


  // tomer

  RN_IS* IS(int i) { return IS_[i]; }


  // **


  RN_Label* findLabel(int c);
  RN_Node* findNode(int c);
  RN_Link* findLink(int c);
  RN_Segment* findSegment(int c);
  RN_Lane* findLane(int c);
  RN_Sensor* findSensor(int c);
  RN_Signal* findSignal(int c);
  RN_BusStop* findBusStop(int c);

  // tomer

  RN_IS* findIS(int c);

  // **


  // These return the last object in each class. Used mostly by RN_Parser.

  RN_Label* lastLabel() { return labels_.back(); }
  RN_Node* lastNode() { return nodes_.back(); }
  RN_Link* lastLink() { return links_.back(); }
  RN_Segment* lastSegment() { return segments_.back(); }
  RN_Lane* lastLane() { return lanes_.back(); }
  RN_Sensor* lastSensor() { return sensors_.back(); }
  RN_Signal* lastSignal() { return signals_.back(); }
	  
  // tomer - again where is the back(0 function defined?

  RN_IS* lastIS() { return IS_.back(); }

  // **


  RN_SurvStation* lastSurvStation() { return lastSurvStation_; }
  RN_CtrlStation* lastCtrlStation() { return lastCtrlStation_; }
  void lastCtrlStation(RN_CtrlStation * ctrl) {
	lastCtrlStation_ = ctrl;
  }
  void lastSurvStation(RN_SurvStation * surv) {
	lastSurvStation_ = surv;
  }

  virtual void addNode(RN_Node *i) { nodes_.push_back(i); }
  virtual void addLink(RN_Link *i) { links_.push_back(i); }
  virtual void addSegment(RN_Segment *i) { segments_.push_back(i); }
  virtual void addLane(RN_Lane *i) { lanes_.push_back(i); }
  virtual void addLabel(RN_Label *i) { labels_.push_back(i); }
  virtual void addSensor(RN_Sensor *i) { sensors_.push_back(i); }
  virtual void addSignal(RN_Signal *i) { signals_.push_back(i); }
  virtual void addBusStop(RN_BusStop *i) { busstops_.push_back(i); }

  // tomer - again where is push_back()?

  virtual void addIS(RN_IS *i) { IS_.push_back(i); }


  // **


  virtual int addLaneConnector(int up, int dn);
  virtual int addTurnProhibitor(int up, int dn);

  virtual int isNeighbor(RN_Link *s1, RN_Link *s2);
  virtual int isNeighbor(RN_Segment *s1, RN_Segment *s2);

  // Before we use the parsed network, this function must called to
  // calculate some static information, sort objects, etc.

  virtual void calcStaticInfo();
  int calcnSelectedSegs();

  inline int drivingDirection() {return drivingDirection_;}
 
  // Control and surveillance devices are stored in each link. But
  // each segment also has a pointer to the first device. The
  // following two function prepares the device pointers in segments.

  void assignSurvListInSegments();
  void assignCtrlListInSegments();

  // Calculate the commonality factors for route choice model

  void calcPathCommonalityFactors();
  void printPathCommonalityFactors(ostream &os = cout);

  // Save the network database.  The result can be reloaded by the
  // network parser.

  virtual int save(const char *name = NULL);

  // This prints the network objects for debugging.

  virtual void printBasicInfo(ostream &os = cout);

  virtual void print(ostream &os = cout);
  virtual void printLabels(ostream &os = cout);
  virtual void printNodes(ostream &os = cout);
  virtual void printLinks(ostream &os = cout);
  virtual void printConnectors(ostream &os = cout);
  virtual void printProhibitors(ostream &os = cout);
  virtual void printSensors(ostream &os = cout);
  virtual void printSignals(ostream &os = cout);
  virtual void printTollPlazas(ostream &os = cout);
  virtual void printBusStops(ostream &os = cout); //margaret
  virtual void printSdFns(ostream &os = cout);

  // tomer - I commented this out now because I don't use it and it gives problems

  //  virtual void printIS(ostream &os = cout);

  // **


  virtual int outputLinkTravelTimes(const char *filename);
  virtual int outputReloadableLinkTravelTimes(const char *filename);

  virtual int outputLinkFlowPlusTravelTimes(
											const char *filename, int col, int ncols);

  virtual void updateProgress(const char * msg = NULL);
  virtual void updateProgress(float pct);

  void initializeLinkStatistics();
  void resetLinkStatistics(int col, int ncols);

#ifdef EXTERNAL_MOE
  virtual int parseMOESpecification(Reader &is);
  int parseSegStats(Reader &is);
#endif

  void save_3d_state(int roll, int tm, const char *prefix = 0);
  void append_3d_state(const char *prefix1, const char *prefix2 = 0);

public:

  // Export data to transcad
  
  int Export(int type) ;

private:

  bool ExportSegmentAsMapInfo(const char* filename) ;
  bool ExportSegmentAsDccText(const char* filename) ;
};

extern RoadNetwork* theNetwork;

#endif
