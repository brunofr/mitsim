//-*-c++-*------------------------------------------------------------
// RN_Lane.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef RN_LANE_HEADER
#define RN_LANE_HEADER

#include <iostream>
#include <vector>

#include <Templates/Object.h>
#include <Templates/Listp.h>
#include "Constants.h"

// Lane change regulation masks

const int LANE_CHANGE_RIGHT             = 0x00000001;
const int LANE_CHANGE_LEFT              = 0x00000002;
const int LANE_CHANGE                   = 0x00000003;

const int LANE_CHANGE_RIGHT_REQUIRED    = 0x00000004;
const int LANE_CHANGE_LEFT_REQUIRED     = 0x00000008;
const int LANE_CHANGE_REQUIRED          = 0x0000000C;

// Lane type masks.

const int LANE_TYPE_SHOULDER		= 0x00000003; // 2 bits sum
const int LANE_TYPE_RIGHT_MOST		= 0x00000001;
const int LANE_TYPE_LEFT_MOST		= 0x00000002;
      
const int LANE_TYPE_RAMP		= 0x000000F0; // mask

const int LANE_TYPE_UP_RAMP		= 0x00000030; // 2 bits sum
const int LANE_TYPE_UP_ONRAMP		= 0x00000010;
const int LANE_TYPE_UP_OFFRAMP		= 0x00000020;
      
const int LANE_TYPE_DN_RAMP		= 0x000000C0; // 2 bits sum
const int LANE_TYPE_DN_ONRAMP		= 0x00000040;
const int LANE_TYPE_DN_OFFRAMP		= 0x00000080;

const int LANE_TYPE_BOUNDARY		= 0x00000200; // last lane
const int LANE_TYPE_DROPPED		= 0x00000100; // no down lane

const double LANE_WIDTH = 3.66;	// in meters (12 feet)

class RoadNetwork;
class RN_Link;
class RN_Segment;
class RN_BisonParser;
class RN_CtrlStation;
class RN_SurvStation;

class RN_Lane : public CodedObject
{
  friend class RN_BisonParser;
  friend class RoadNetwork;

public:

  typedef Listp<RN_SurvStation*> surv_list_type;
  typedef Listp<RN_CtrlStation*> ctrl_list_type;
  typedef surv_list_type::link_type surv_list_link_type;
  typedef ctrl_list_type::link_type ctrl_list_link_type;

protected:

  static int sorted_;	// 1=sorted 0=arbitrary order

  int index_;		// index in array
  RN_Segment* segment_;	// segment that contain the lane 
  unsigned int rules_;	// lane use and change rules
  std::vector<RN_Lane*> upLanes_; // pointers to upstream lanes 
  std::vector<RN_Lane*> dnLanes_; // pointers to downstream lanes 

  unsigned int laneType_;	// lane type
  unsigned int state_;
  unsigned int cmarker_;	// connection marker

public:

  RN_Lane();
  virtual ~RN_Lane() { }

  static inline int sorted() { return sorted_; }

  inline unsigned int state(unsigned int mask = 0xFFFF) {
	return (state_ & mask);
  }
  inline void setState(unsigned int s) {
	state_ |= s;
  }
  inline void unsetState(unsigned int s) {
	state_ &= ~s;
  }

  inline RN_Segment* segment() { return segment_; }

  RN_Link* link();
  int index() { return index_; }
  int localIndex();

  int linkType();

  void setLaneType();

  inline unsigned int laneType(const unsigned int flag = 0xFFFFFFFF) {
	return (laneType_ & flag);
  }
  inline unsigned int isDropped() {
	return (laneType_ & LANE_TYPE_DROPPED);
  }
  inline unsigned int isBoundary() {
	return (laneType_ & LANE_TYPE_BOUNDARY);
  }

  inline unsigned int isEtcLane() {
	return rules_ & VEHICLE_ETC;
  }
  inline unsigned int isHovLane() {
	return rules_ & VEHICLE_HOV;
  }
  inline unsigned int isBusLane() {
	return rules_ & VEHICLE_COMMERCIAL;
  }

  double length();

  RN_Lane* right();
  RN_Lane* left();
  RN_Lane* prev();
  RN_Lane* next();

  inline int nUpLanes() { return upLanes_.size(); }
  inline int nDnLanes() { return dnLanes_.size(); }

  inline RN_Lane* upLane(int i) {
	return upLanes_[i];
  }
  inline RN_Lane* dnLane(int i) {
	return dnLanes_[i];
  }

  // Find if a lane is one the upstream lanes
  RN_Lane* findInUpLane(int c);

  // Find if a lane is one the downstream lanes
  RN_Lane* findInDnLane(int c);

  bool isInDnLanes(RN_Lane *);
  bool isInUpLanes(RN_Lane *);

  inline unsigned int rules() { return rules_; }
  inline void rulesExclude(unsigned int exclude) {
	rules_ &= ~exclude;
  }

  virtual int init(int code, int rules);

  virtual void print(std::ostream &os = std::cout);
  virtual void printDnConnectors(std::ostream &os = std::cout);

  virtual void calcStaticInfo();

  virtual double freeSpeed();
  virtual double grade();

  virtual surv_list_link_type survList();
  virtual ctrl_list_link_type ctrlList();

  inline int hasMarker() { return state_ & STATE_MARKED; }

  // Check if the lane is connected to a lane upstream/downstream.
  // markConnectedUpLanes() or markConnecteDnLanes() must be
  // called (usually for each lane in a segment) before the use of
  // the function isConnected(signature_bit), where signatures is
  // the bit masks (left lane is 1, middle lane is 2, right lane
  // is 4, and so on) of the lanes that do the marking.

  inline bool isConnected(unsigned int signatures) {
	return (cmarker_ & signatures) ? true : false;
  }

  void markConnectedUpLanes(unsigned int signatures, float depth = 0.0);
  void markConnectedDnLanes(unsigned int signatures, float depth = 0.0);

  void unmarkConnectedUpLanes(unsigned int signatures, float depth = 0.0);
  void unmarkConnectedDnLanes(unsigned int signatures, float depth = 0.0);

  static void unMarkAllLanes();
};

#endif
