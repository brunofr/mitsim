//-*-c++-*------------------------------------------------------------
// FILE: RN_DrawableLane.C
// DATE: Sat Nov 11 16:08:58 1995
//--------------------------------------------------------------------

#include "RN_DrawableLane.h"
#include "RN_CtrlStation.h"
#include "RN_SurvStation.h"
#include "WcsPoint.h"

#include <Tools/Math.h>

// Following three functions return points at the position r
// (0=downstream end 1= upstream end).

WcsPoint
RN_DrawableLane::leftPoint(float r)
{
   return intermediatePoint(r).bearing(width(r),
				       tangentAngle(r) + HALF_PI);
}


WcsPoint
RN_DrawableLane::centerPoint(float r)
{
   return intermediatePoint(r).bearing(0.5 * width(r),
				       tangentAngle(r) + HALF_PI);
}


WcsPoint
RN_DrawableLane::rightPoint(float r)
{
   return RN_Curve::intermediatePoint(r);
}
