//-*-c++-*------------------------------------------------------------
// NAME: A drawable curver class
// AUTH: Qi Yang
// FILE: DRN_Curve.h
// DATE: Thu Nov  2 18:17:31 1995
//--------------------------------------------------------------------

#ifndef RN_CURVE_HEADER
#define RN_CURVE_HEADER

//#include <iostream>
#include <cstring>
#include "WcsPoint.h"

class RN_Arc;

// RN_Curve is a base class for drawble lanes and drawable segment.

class RN_Curve
{
protected:
      
  RN_Arc	*arc_;		// base arc or NULL if it is a line
  double	data_[5];	// data of this curve, defined as:
  //        0       1       2       3       4
  // NULL	startX	startY	endX	endY	angle 
  // Arc	startA	startR	endA	endR	arcAngle

public:
      
  RN_Curve() : arc_(NULL) {
	memset(data_, 0, 5 * sizeof(double));
  }
      
  virtual ~RN_Curve() { }
      
  RN_Curve(RN_Arc *a, double up, double dn) : arc_(NULL) {
	memset(data_, 0, 5 * sizeof(double));
	shiftInitializer(a, up, dn);
  }
      
  RN_Curve(RN_Arc *uparc, double upoffset,
		   RN_Arc *arc, double offset1, double offset2,
		   RN_Arc *dnarc, double dnoffset,
		   int editable)  : arc_(NULL) {
	memset(data_, 0, 5 * sizeof(double));
	intersectionInitializer(uparc, upoffset,
							arc, offset1, offset2, 
							dnarc, dnoffset,
							editable);
  }

  // Initialize a spiral curve with offsets up and dn from the the
  // input arcs.  The start and end points of the curve are moved
  // to the intersections (uparc/arc and arc/dnarc).

  void intersectionInitializer(
							   RN_Arc *uparc, double upoffset,
							   RN_Arc *arc, double offset1, double offset2,
							   RN_Arc *dnarc, double dnoffset,
							   int editable);
      
  void endPointsInitializer(
							RN_Arc *left, WcsPoint& uppnt, WcsPoint& dnpnt);

  // Initialize a spiral curve with offsets up and dn from the the
  // input arc.
      
  void shiftInitializer(RN_Arc *a, double up, double dn);

  void intrapolateInitializer(
							  RN_Arc *left, RN_Curve *right,
							  double r1, double r2);
      
  inline int isCurve() { return (arc_ ? 1 : 0); }
  inline RN_Arc* arc() { return arc_; }

  // These methods make sense only if the curve is a real curve.
  // Make sure you are not using these functions for lines

  inline double startAngle() { return data_[0]; }
  double angleFromCenter(double pos = 0.5);
  inline double endAngle() { return data_[2]; }

  inline double startRadius() { return data_[1]; }
  double radius(double pos = 0.5);
  inline double endRadius() { return data_[3]; }

  inline double arcAngle() { return data_[4]; }

  WcsPoint& center();
      
  // These methods make sense only if the curve is a line

  inline double startX() { return data_[0]; }
  inline double startY() { return data_[1]; }
  inline double endX() { return data_[2]; }
  inline double endY() { return data_[3]; }
  inline double angle() { return data_[4]; }
      
  // These methods apply to both curve and line
      
  WcsPoint startPnt();
  WcsPoint endPnt();
      
  // Find the point on the curve (frac: 0=EndPoint 1=StartPoint)
      
  WcsPoint intermediatePoint(double frac);
  inline WcsPoint point(double frac) {
	return intermediatePoint(frac);
  }

  // Find angle of the curve at various point

  double startTangentAngle(); // grade CCW 
  double endTangentAngle();   // grade CCW 
  double tangentAngle(double);

  WcsPoint shiftedIntersection(RN_Arc *arc1, double d1,
							   RN_Arc *arc2, double d2,
							   int editable);

  // Find the containing rectangle

  virtual float min_x();
  virtual float min_y();
  virtual float max_x();
  virtual float max_y();
};

#endif
