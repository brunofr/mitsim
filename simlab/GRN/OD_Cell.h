//-*-c++-*------------------------------------------------------------
// OD_Cell.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//--------------------------------------------------------------------

#ifndef OD_CELL_HEADER
#define OD_CELL_HEADER

#include <iostream>
#include <vector>
#include "RN_Path.h"
#include "OD_Pair.h"

class RoadNetwork;
class RN_Node;
class RN_Vehicle;
class OD_Table;
class RN_PathTable;
class BusAssignmentTable;
class OD_BisonParser;

const double RATE_EPSILON = 1.0E-4;

class OD_Cell
{
	friend class OD_Table;
	friend class OD_BisonParser;
	friend class RoadNetwork;
	friend class RN_PathTable;
      friend class BusAssignmentTable;

protected:

	OD_Pair* od_;				// od pair

	double headway_;			// average headway (second)
	double nextTime_;			// next departure time
	int type_;				// vehicle type
	float randomness_;			// 0=uniform 1=random

	int busTypeBRT_;			// Dan: bus type for bus rapid transit assignment
	int runIDBRT_;			// Dan: assigned bus run id for bus rapid transit

	std::vector<RN_Path *> paths_;	// an array of pointers to paths
	float *splits_;				// probabilities to choose each path
	
	static int nSplits_ ;		// num of splits parsed so far for current cell

public:

	OD_Cell() : nextTime_(0.0), splits_(0) { }
	~OD_Cell() { }
  
	int cmp(OD_Cell* cell);	// for sorting
	int cmp(int c);	// for sorting
	bool eq(OD_Cell* cell);	// for identification
	bool operator ==(OD_Cell* cell) {
		return eq(cell);
	}

	RN_Node* oriNode() { return od_->oriNode(); }
	RN_Node* desNode() { return od_->desNode(); }
	double rate() { return 3600.0 / headway_; }
	double nextTime() { return nextTime_; }
	int type() { return type_; }

	int busTypeBRT() { return busTypeBRT_; }
	int runIDBRT() { return runIDBRT_; }

	int nPaths() {
		return paths_.size();
	}
	RN_Path* path(int i) {
		if (i < 0) return NULL;
		else if (i >= paths_.size()) return NULL;
		else return paths_[i];
	}
	int addPath(int path_id) ;

	int setSplit(float split) ;
	static int nSplits() { return nSplits_ ; }
	float split(int i) { return splits_[i] ; }
	float* splits() { return splits_ ; }

	int init(int ori, int des, double d, double var, float r);

	int initBRT(int ori, int des, double d, double var, float r, int bt, int rid);

	virtual void print(std::ostream &os = std::cout);

	double randomizedHeadway();

	virtual RN_Vehicle* newVehicle() = 0;

	virtual void emitVehicles(void);

	virtual RN_Vehicle* emitVehicle(void);

	RN_Path* chooseRoute(RN_Vehicle *);

	// Used when parsing the od table

	static OD_Cell * workingCell() { return workingCell_; }

private:

	static OD_Cell *workingCell_;
};

#endif
