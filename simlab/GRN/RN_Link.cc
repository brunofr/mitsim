//-*-c++-*------------------------------------------------------------
// FILE: RN_Link.C
// AUTH: Qi Yang
// DATE: Wed Oct 18 14:29:29 1995
//--------------------------------------------------------------------


#include <map>
#include <set>

#include <Tools/Math.h>

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>
#include <Tools/SimulationEngine.h>

#include "RoadNetwork.h"
#include "Constants.h"
#include "Parameter.h"

#include "RN_Node.h"
#include "RN_Link.h"
#include "RN_Segment.h"
#include "RN_Lane.h"
#include "RN_Label.h"
#include "RN_SurvStation.h"
#include "RN_CtrlStation.h"
#include "RN_PathPointer.h"
#include "RN_Path.h"
#include "RN_Vehicle.h"
#include "RN_DynamicRoute.h"
using namespace std;

// When an angle between two link is less that 1/128 PI, we do not
// care their intersection anymore because they are almost in parallel

const double U_ANGLE = ONE_PI / 4096.0;
const double V_ANGLE = TWO_PI - U_ANGLE;

// The angle a link enters and leaves an intersection is defined by
// the angle of the line connecting end point and a point with a given
// distance (ANGLE_DISTANCE) along the arc.  However, this point
// should also be wiithin a given relative distance (ANGLE_POSITION)
// of the length of the last/first segment.

const double ANGLE_DISTANCE = 10.0; // meters from end point
const double ANGLE_POSITION = 0.25;

int RN_Link::sorted_ = 1;

RN_Link::RN_Link()
  : CodedObject(),
	state_(0),
	nSamplesTravelTimeEnteringAt_(NULL),
	sumOfTravelTimeEnteringAt_(NULL)
{
}

RN_Link::~RN_Link()
{
  delete [] nSamplesTravelTimeEnteringAt_;
  delete [] sumOfTravelTimeEnteringAt_;
}


// Returns the number of upstream links

int
RN_Link::nUpLinks()
{
  return (upNode_->nUpLinks());
}


// Returns the number of downstream links

int
RN_Link::nDnLinks()
{
  return (dnNode_->nDnLinks());
}


// Returns the ith upstream link

RN_Link*
RN_Link::upLink(short int i)
{
  return (upNode_->upLink(i));
}


// Return the ith downsteam link

RN_Link*
RN_Link::dnLink(short int i)
{
  return (dnNode_->dnLink(i));
}

RN_Segment*
RN_Link::segment(int i)
{
  return theNetwork->segment(startSegment_ + i);
}

RN_Segment* 
RN_Link::startSegment()
{
  return theNetwork->segment(startSegment_);
}

RN_Segment*
RN_Link::endSegment()
{
  return theNetwork->segment(startSegment_ + nSegments_ - 1);
}


// Returns the name of the link

char*
RN_Link::name()
{
  if (label_) return (label_->name());
  else return NULL;
}

RN_Link::ctrl_list_link_type RN_Link::ctrlList() {
  return ctrlStationList_.head();
}

RN_Link::surv_list_link_type RN_Link::survList() {
  return survStationList_.head();
}

RN_Link::ctrl_list_link_type RN_Link::lastCtrl() {
  return ctrlStationList_.tail();
}

RN_Link::surv_list_link_type RN_Link::lastSurv() {
  return survStationList_.tail();
}

double
RN_Link::inboundAngle()
{
  RN_Segment *ps = endSegment();
  if (ps->isArc()) {
	double pos = Min(ANGLE_DISTANCE/ps->length(), ANGLE_POSITION);
	return ps->intermediatePoint(pos).angle(ps->endPnt());
	// Earlier we use ps->endTangentAngle(pos) but it causes problems
	// with some intersection.
  } else {
	return ps->endAngle();
  }
}

double
RN_Link::outboundAngle()
{
  RN_Segment *ps = startSegment();
  if (ps->isArc()) {
	double pos = Min(ANGLE_DISTANCE/ps->length(), ANGLE_POSITION);
	return ps->startPnt().angle(ps->intermediatePoint(1.0 - pos));
	// Earlier we use startSegment()->startTangentAngle()) but it
	// causes problems with some intersection.
  } else {
	return ps->startAngle();
  }
}


void
RN_Link::calcIndicesAtNodes()
{
  dnIndex_ = upNode_->whichDnLink(this);
  upIndex_ = dnNode_->whichUpLink(this);
}


// This function should be called after the links at each node has
// been sorted.

void
RN_Link::calcStaticInfo()
{
  //   const double v_angle = ONE_PI * 0.125;
  //   const double u_angle = TWO_PI - v_angle;

  double alpha = inboundAngle() + ONE_PI;
  double beta, min_beta;
  register short int i;
  short int narcs = nDnLinks();

   // TOPOLOGY

   // We want angle in range [0, 2PI)

  if (alpha >= TWO_PI) alpha -= TWO_PI;

  min_beta = DBL_INF;
  rightDnIndex_ = 0xFF;

  for (i = 0; i < narcs; i ++) {

	beta = dnLink(i)->outboundAngle() - alpha;

	// Skip the U turn

	//      if (beta < 0.0 && -beta < v_angle) continue;
	//      else if (beta > 0.0 && beta > u_angle) continue;

	//      if (beta < 0.0) beta += TWO_PI; // angle is in range (0,2PI)
	if (beta <= 0.0) beta += TWO_PI;

	// Choose the smallest angle, which is the "right down link"

	if (beta < min_beta) {
	  min_beta = beta;
	  rightDnIndex_ = i;
	}
  }

  // LENGTH, FREE FLOW TRAVEL TIME AND SPEED

  RN_Segment *ps = endSegment();
  length_ = 0.0;
  travelTime_ = 0.0;
  while (ps) {
	ps->distance(length_);
	length_ += ps->length();
	travelTime_ += ps->length() / ps->freeSpeed();
	ps = ps->upstream();
  }
  freeSpeed_ = length_ / travelTime_;

   // CONNECTIVITY

   // Connectivity to dnLinks

  dnLegal_ = 0;	// set all bits to 0

  RN_Lane * plane = endSegment()->rightLane();
  while(plane) {

	// Check each downstream lane connected to plane

	int n = plane->nDnLanes();
	for (i = 0; i < n; i ++) {
	  includeTurn(plane->dnLane(i)->link());
	}
	plane = plane->left();
  }

  // LANE USE RULES

  laneUseRules_ = VEHICLE_LANE_USE;
  plane = startSegment()->rightLane();
  while (laneUseRules_ && plane)	{
	laneUseRules_ &= (plane->rules() & VEHICLE_LANE_USE);
	plane = plane->left();
  }
}


// Returns signalIndex for the downstream link "plink":
//   0 = right most link
//   ...
//   nDnLinks() = left most link
// CAUTION: "link" must be a dn link of this link.

int
RN_Link::signalIndex(RN_Link* link)
{
  register int i;
  if (rightDnIndex_ != 0xFF &&	// defined
	  link != NULL) {
	short int n = nDnLinks();
	i = (link->dnIndex() - rightDnIndex_ + n) % n;
  } else {
	i = 0;
  }
  return i;
}


// Mark the turning movement toward a link as prohibited

void
RN_Link::excludeTurn(RN_Link* link)
{
  dnLegal_ &= ~(1 << link->dnIndex());
}


// Mark the turning movement toward a link as connected

void
RN_Link::includeTurn(RN_Link* link)
{
  dnLegal_ |= (1 << link->dnIndex());
}


/*
 *------------------------------------------------------------------
 * Check if vehicle is permited in this link.
 *------------------------------------------------------------------
 */

int
RN_Link::isCorrect(RN_Vehicle *pv)
{
  if (!laneUseRules_ ||
	  laneUseRules_ & (pv->types() & VEHICLE_LANE_USE)) {
	return 1;
  } else {
	return 0;
  }
}


// Print not connected downstream links

void
RN_Link::printNotConnectedDnLinks(ostream &os)
{
  RN_Link *dnl;
  int cnt = 0;
  for (int i = 0; i < nDnLinks(); i ++) {
	dnl = dnLink(i);
	if (isMovementAllowed(dnl)) continue;
	if (!cnt) os << indent;
	else os << endc;
	os << OPEN_TOKEN
	   << code_ << endc << dnl->code_
	   << CLOSE_TOKEN;
	cnt ++;
  }
  if (cnt) os << endl;
}


int RN_Link::countNotConnectedDnLinks()
{
  RN_Link *dnl;
  int cnt = 0;
  for (int i = 0; i < nDnLinks(); i ++) {
	dnl = dnLink(i);
	if (!isMovementAllowed(dnl)) {
	  cnt ++;
	}
  }
  return cnt;
}


// Return non-zero if the turn from this link to link 'other' is
// allowed
 
int 
RN_Link::isMovementAllowed(RN_Link *other)
{
  return (dnLegal_ & (1 << other->dnIndex()));
}


float
RN_Link::calcCurrentTravelTime()
{
  float tt = 0.0;
  for (int i = 0; i < nSegments(); i ++)	{
	tt += segment(i)->calcCurrentTravelTime();
  }
  return tt;
}


// This function is called by RN_Parser. It resturns -1 if a fatal
// error occurs, 1 if warning errors, and 0 if no error.

int
RN_Link::init(int c, int t, int up, int dn, int l)
{
  static int last = -1;

  if (ToolKit::debug()) {
	cout << indent << "<"
		 << c << endc << t << endc
		 << up << endc << dn << endc
		 << l << ">" << endl;
  }

  code_ = c;
  if (sorted_ && code_ <= last) sorted_ = 0;
  else last = code_;

  type_ = t;

  // Find the nodes coded as "up" and "dn"

  upNode_ = theNetwork->findNode(up);
  dnNode_ = theNetwork->findNode(dn);
	
  if (!upNode_) {
	cerr << "Error:: Unknown UpNode <" << up << ">. ";
	return (-1);
  } else if (!dnNode_) {
	cerr << "Error:: Unknown DnNode <" << dn << ">. ";
	return (-1);
  }

  upNode_->addDnLink(this);
  dnNode_->addUpLink(this);

  // Find pointer to the street name

  if (l > 0 && (label_ = theNetwork->findLabel(l)) == NULL) {
	cerr << "Warning:: Unknown label <" << l << ">. ";
	return -1;
  } else {
	label_ = NULL;
  }

  index_ = theNetwork->nLinks();
  nSegments_ = 0;
  startSegment_ = theNetwork->nSegments();

  theNetwork->addLink(this);

  return 0;
}


// Find the first link (either a upLink or a dnLink) on the right side
// of this link at the downstream node of this link.  Searches
// outbound link first.

RN_Link*
RN_Link::dnRightNeighbor()
{
  RN_Link * neighbor = NULL;
  RN_Link * link;
  register short int i, n;
  double alpha, beta;
  double min_beta = DBL_INF;

  alpha = inboundAngle();
  alpha += ( (alpha < ONE_PI) ? (ONE_PI) : (-ONE_PI) );

  // For outbound links at the downstream node

  for (i = 0, n = dnNode_->nDnLinks(); i < n; i ++) {
	link = dnNode_->dnLink(i);
	beta = link->outboundAngle() - alpha;
	if (beta < 0.0) beta += TWO_PI;
	if (beta < U_ANGLE || beta > V_ANGLE) continue;
	if (beta < min_beta) {
	  min_beta = beta;
	  neighbor = link;
	}
  }

  // For inbound links at the downstream node

  for (i = 0, n = dnNode_->nUpLinks(); i < n; i ++) {
	link = dnNode_->upLink(i);
	if (link == this) continue;
	beta = link->inboundAngle();
	beta += ( (beta < ONE_PI) ? (ONE_PI) : (-ONE_PI) );
	beta = beta - alpha;
	if (beta < 0) beta += TWO_PI;
	if (beta < U_ANGLE || beta > V_ANGLE) continue;
	if (beta + 1.E-5 < min_beta) {

	  // This needs to be absolutely smaller and considering the
	  // float number approximate error.

	  min_beta = beta;
	  neighbor = link;
	}
  }

  return neighbor;
}


// Find the first link (either a upLink or a dnLink) on the right side
// of this link at the upstream node of this link.  Search inbound
// link first.

RN_Link*
RN_Link::upRightNeighbor()
{
  RN_Link * neighbor = NULL;
  RN_Link * link;
  register short int i, n;
  double alpha, beta;
  double min_beta = DBL_INF;

  alpha = outboundAngle();

  // For inbound links at the upstream node

  for (i = 0, n = upNode_->nUpLinks(); i < n; i ++) {
	link = upNode_->upLink(i);
	beta = link->inboundAngle();
	beta += ( (beta < ONE_PI) ? (ONE_PI) : (-ONE_PI) );
	beta = alpha - beta;
	if (beta < 0) beta += TWO_PI;
	if (beta < U_ANGLE || beta > V_ANGLE) continue;
	if (beta < min_beta) {
	  min_beta = beta;
	  neighbor = link;
	}
  }

  // For outbound links at the upstream node

  for (i = 0, n = upNode_->nDnLinks(); i < n; i ++) {
	link = upNode_->dnLink(i);
	if (link == this) continue;
	beta = alpha - link->outboundAngle();
	if (beta < 0.0) beta += TWO_PI;
	if (beta < U_ANGLE || beta > V_ANGLE) continue;
	if (beta + 1.E-6 < min_beta) {

	  // This needs to be absolutely smaller considering float
	  // number approximate error.

	  min_beta = beta;
	  neighbor = link;
	}
  }

  return neighbor;
}


void RN_Link::assignCtrlListInSegments()
{
  // Find the first control device or event in a segment by looking
  // the distance of the devices in that segment.

  RN_Segment * ps;
  CtrlList ctrl = ctrlList();
  while (ctrl) {
	ps = (*ctrl)->segment();
	if (ps->ctrlList_ == NULL) {
	  ps->ctrlList_ = ctrl;
	} else if ((*ctrl)->distance() > 
			   (*(ps->ctrlList_))->distance()) {
	  ps->ctrlList_ = ctrl;
	}
	ctrl = ctrl->next();
  }

  // If no devices in a segment, the pointer should point to the
  // first device in the downstream segment of the link.

  ps = endSegment();
  ctrl = ps->ctrlList_;
  while (ps = ps->upstream()) {
	if (ps->ctrlList_ == NULL) {
	  ps->ctrlList_ = ctrl;
	} else {
	  ctrl = ps->ctrlList_;
	}
  }
}


void RN_Link::assignSurvListInSegments()
{
  // Find the first surveillance device in a segment by looking the
  // distance of the devives in that segment.

  RN_Segment * ps;
  SurvList surv = survList();
  while (surv) {
	ps = (*surv)->segment();
	if (ps->survList() == NULL) {
	  ps->survList_ = surv;
	} else if ((*surv)->distance() >
			   (*(ps->survList_))->distance()) {
	  ps->survList_ = surv;
	}
	surv = surv->next();
  }

  // If no devices in a segment, the pointer should point to the
  // first device in the downstream segment of the link

  ps = endSegment();
  surv = ps->survList_;
  while (ps = ps->upstream()) {
	if (ps->survList_ == NULL) {
	  ps->survList_ = surv;
	} else {
	  surv = ps->survList_;
	}
  }
}


/*
 *------------------------------------------------------------------
 * Find the first control device located before "pos"
 *------------------------------------------------------------------
 */

RN_Link::ctrl_list_link_type
RN_Link::prevCtrl(double pos)
{
  ctrl_list_link_type ctrl = lastCtrl();
  while (ctrl && (*ctrl)->distance() < pos) {
	ctrl = ctrl->prev();
  }
  return (ctrl);
}


/*
 *------------------------------------------------------------------
 * Find the first control device located after "pos"
 *------------------------------------------------------------------
 */

RN_Link::ctrl_list_link_type
RN_Link::nextCtrl(double pos)
{
  ctrl_list_link_type ctrl = ctrlList();
  while (ctrl && (*ctrl)->distance() > pos) {
	ctrl = ctrl->next();
  }
  return (ctrl);
}


/*
 *------------------------------------------------------------------
 * Find the first sensor device located before "pos"
 *------------------------------------------------------------------
 */

RN_Link::surv_list_link_type
RN_Link::prevSurv(double pos)
{
  surv_list_link_type surv = lastSurv();
  while (surv && (*surv)->distance() < pos) {
	surv = surv->prev();
  }
  return (surv);
}


/*
 *------------------------------------------------------------------
 * Find the first sensor device located after "pos"
 *------------------------------------------------------------------
 */

RN_Link::surv_list_link_type
RN_Link::nextSurv(double pos)
{
  surv_list_link_type surv = survList();
  while (surv && (*surv)->distance() > pos) {
	surv = surv->next();
  }
  return (surv);
}


void
RN_Link::unmarkLanes()
{
  RN_Segment* ps = startSegment();
  while (ps) {
	RN_Lane * pl = ps->rightLane();
	while (pl) {
	  pl->unsetState(STATE_MARKED);
	  pl = pl->left();
	}
	ps = ps->downstream();
  }
}

void
RN_Link::unmarkLanesInUpLinks()
{
  int narcs = nUpLinks();
  for (int i = 0; i < narcs; i ++) {
	upLink(i)->unmarkLanes();
  }
}

void
RN_Link::unmarkLanesInDnLinks()
{
  int narcs = nDnLinks();
  for (int i = 0; i < narcs; i ++) {
	dnLink(i)->unmarkLanes();
  }
}


void
RN_Link::print(ostream &os)
{
  os << indent << "{";
  os << code_ << endc << type() << endc
	 << upNode()->code() << endc << dnNode()->code() << endc;
  if (label_) {
	os << label_->code() << endc << "# " << label_->name() << endl;
  } else {
	os << '0' << endl;
  }
  for (register int i = 0; i < nSegments_; i ++) {
	segment(i)->print(os);
  }
  os << indent << "}" << endl;
}


void RN_Link::printSensors(ostream &os)
{
  surv_list_type::iterator i = survStationList_.begin();
  while (i != survStationList_.end()) {
	(*i)->print(os);
	i++;
  }
}

void RN_Link::printSignals(ostream &os)
{
  ctrl_list_type::iterator i = ctrlStationList_.begin();
  while (i != ctrlStationList_.end()) {
	if ((*i)->type() & CTRL_SIGNALS) {
	  (*i)->print(os);
	}
	i++;
  }
}

void RN_Link::printTollPlazas(ostream &os)
{
  ctrl_list_type::iterator i = ctrlStationList_.begin();
  while (i != ctrlStationList_.end()) {
	if ((*i)->type() == CTRL_TOLLBOOTH) {
	  (*i)->print(os);
	}
	i++;
  }
}
void RN_Link::printBusStops(ostream &os) //margaret
{
  ctrl_list_type::iterator i = ctrlStationList_.begin();
  while (i != ctrlStationList_.end()) {
	if ((*i)->type() == CTRL_BUSSTOP) {
	  (*i)->print(os);
	}
	i++;
  }
}

void
RN_Link::addPathPointer(RN_Path *path, short int i)
{
  pathPointers_.push_back(new RN_PathPointer(path, i));
}


void RN_Link::calcPathCommonalityFactors()
{
  register int i, j;
  typedef map<int, int, less<int> > lid_type;
  typedef set<int, less<int> > nid_type;

  // Destination nodes completed so far 

  nid_type nids;
  nid_type::iterator ni;

  RN_PathPointer *pp;
  RN_Path *p;
  RN_Link *pl;
  RN_Node *pn;

  for (int di = 0; di < nPathPointers(); di ++) {
	pp = pathPointer(di);
	pn = pp->desNode();
	ni = nids.find(pn->code());
	if (ni != nids.end()) continue; // already processed
	  
	nids.insert(pn->code());	// register the node being processed

	// Number of paths that connected to the same destination and
	// share a given link

	lid_type lids;
	lid_type::iterator li;
	//	cout << " I am in the Outer Loop \n" ;

	// Count the number of paths that share each link

	for (i = 0; i < nPathPointers(); i ++) {

	  //          cout << " number of Paths " << nPathPointers() << "\n";
	  pp = pathPointer(i);
	  if (pp->desNode() != pn) continue;	// path not connected to pn

	  p = pp->path();
	  // for (j = pp->position() + 1; j < p->nLinks(); j ++) {
          for (j = pp->position(); j < p->nLinks(); j ++) {
	    //	    cout << "Position " << pp->position() << " j " << j << " Number of Links " << p->nLinks() << "\n";
		pl = p->link(j);
		li = lids.find(pl->code());
		if (li == lids.end()) {	// first time
		  lids[pl->code()] = 1;
		} else {				// count the use of the link pl
		  lids[pl->code()] += 1;
		}
		//		cout << " lids for " << pl->code() << "is " << lids[pl->code()] << "\n";
	  }
	}

	// Calculate commonality factors for paths connected to node pn

	for (i = 0; i < nPathPointers(); i ++) {
	  pp = pathPointer(i);
	  if (pp->desNode() != pn) continue;	// path not connected to pn

	  p = pp->path();

	  float cf = 0.0;
	  float total_length = 0.0;
	  // for (j = pp->position() + 1; j < p->nLinks(); j ++) {
          for (j = pp->position(); j < p->nLinks(); j ++) {
		pl = p->link(j);
		total_length += pl->length();
		cf += lids[pl->code()] * pl->length(); 
		//                cout << " Inside the first cf loop " << cf << "\n";
	  }
	  if (cf > FLT_MIN) {
		cf = log(cf / total_length);
	  }
	  pp->cf(cf);
	  //	  cout << " cf " << cf << "\n";
	}
		  
  }
}

void RN_Link::printPathCommonalityFactors(ostream &os)
{
  RN_PathPointer *pp;   
  os << code() << ":";
  for (register int i = 0; i < nPathPointers(); i ++) {
	pp = pathPointer(i);
	os << endc << pp->path()->code() << "=" << pp->cf();
  }
  os << endl;
}


// Number of time periods to be recorded

int
RN_Link::infoPeriods()
{
  return theGuidedRoute->infoPeriods();
}

int
RN_Link::infoPeriodLength()
{
  return theGuidedRoute->infoPeriodLength();
}

// This format is for MOE and other post processing.

void
RN_Link::printTravelTimes(ostream& os)
{
  double len = length() / theBaseParameter->lengthFactor();
  os << code_ << endc
	 << type() << endc
	 << Fix(len, 0.1);

  if (!theSimulationEngine->isRectangleFormat()) {
	os << endc << OPEN_TOKEN << endl;
  }
  int num = infoPeriods();
  for (int i = 0; i < num; i ++) {
	int n = nSamplesTravelTimeEnteringAt_[i];
	double t = averageTravelTimeEnteringAt(i);
	if (!theSimulationEngine->isRectangleFormat()) {
	  os << indent << OPEN_TOKEN;
	}
	os << endc << n << endc << Fix(t, 0.1);
	if (!theSimulationEngine->isRectangleFormat()) {
	  os  << CLOSE_TOKEN << endl;
	}
  }
  if (!theSimulationEngine->isRectangleFormat()) {
	os << CLOSE_TOKEN;
  }
  os << endl;
}


// Write the data in the format required by RN_LinkTimes::read().

void
RN_Link::printReloadableTravelTimes(ostream& os)
{
  double len = length() / theBaseParameter->lengthFactor();
  os << indent << code_
	 << endc << type()
	 << endc << Fix(len, 0.1);
  int num = infoPeriods();
  for (int i = 0; i < num; i ++) {
	double t = averageTravelTimeEnteringAt(i);
	os << endc << Fix(t, 0.1);
  }
  os << endl;
}


void
RN_Link::printFlowPlusTravelTimes(ostream& os, int col, int ncols)
{
  register int i;

  os << indent << code_;

  for (i = col, ncols += col; i < ncols; i ++) {
	int n = nSamplesTravelTimeEnteringAt_[i];
	double t = averageTravelTimeEnteringAt(i);
	os << endc << n << endc << Fix(t, 0.1);
  }

  os << endl;
}


// These functions are used to record the travel time vehicles spend
// in each link, aggregated by the time they enter the link.

// Link Travel Times

void
RN_Link::initializeStatistics()
{
  int n = infoPeriods();
  nSamplesTravelTimeEnteringAt_ = new int[n];
  sumOfTravelTimeEnteringAt_ = new float[n];
  resetStatistics(0, n);
}


void
RN_Link::resetStatistics(int col, int ncols)
{
  int num = col + ncols;
  num = Min(infoPeriods(), num);
  for (register int i = col; i < num; i ++) {
	nSamplesTravelTimeEnteringAt_[i] = 0;
	sumOfTravelTimeEnteringAt_[i] = 0.0;
  }
}


// This function is called when a vehicle leaves a link

void
RN_Link::recordTravelTime(RN_Vehicle * pv)
{
  // Tavel time spent in current segment

  float tt = pv->timeInLink();

   // These are for calculating average travel time for the vehicle
   // who ENTER this segment during the reporting time interval.
	
   // Calculate the ID of the entry time period.

  int i = theGuidedRoute->whichPeriod(pv->timeEntersLink());

  // Time spent in this link

  sumOfTravelTimeEnteringAt_[i] += tt;
  nSamplesTravelTimeEnteringAt_[i] ++;  
}


// This function is called just before the simulation is terminated.
// It accounts for the vehicle which are still in the link.

void
RN_Link::recordExpectedTravelTime(RN_Vehicle * pv)
{
  // Tavel time spent in current link

  float pos;

  if (pv->segment()) {
	pos = pv->distanceFromDownNode();
  } else {
	pos = length_;
  }

  // Calculate the ID of the entry time period.

  int i = theGuidedRoute->whichPeriod(pv->timeEntersLink());

  float ht = theGuidedRoute->linkTime(this, pv->timeEntersLink());
  float tt = pv->timeInLink();
  // Next portion commented out by Joseph Scariza 11/6/01
  //if (pos > 0.75 * length_) {	// upstream end
  //	tt += averageTravelTimeEnteringAt(i) * pos / length_;
  //  } else {						// traveled long enough
  //	tt += tt * pos / (length_ - pos);
  
  //Section added Joseph Scariza 11/6/01
  if (pos < 0.25 * length_)
    {tt += tt* pos / (length_ - pos);
  }

  // Accumulate vehicle's time in this link

  sumOfTravelTimeEnteringAt_[i] += tt;
  nSamplesTravelTimeEnteringAt_[i] ++; 
}


float
RN_Link::averageTravelTimeEnteringAt(double enter)
{
  int i = theGuidedRoute->whichPeriod(enter);
  return averageTravelTimeEnteringAt(i);
}


float
RN_Link::averageTravelTimeEnteringAt(int i)
{
  int num = nSamplesTravelTimeEnteringAt_[i];
  if (num > 0) {
	return sumOfTravelTimeEnteringAt_[i] / num;
  } else {						// no sample
	int p = i - 1, n = i + 1 ;
	int m = infoPeriods() ;

	// Find the first no empty previous time intervals
	while (p >= 0 && nSamplesTravelTimeEnteringAt_[p] == 0) p -- ;

	// Find the first no empty next time intervals
	while (n < m  && nSamplesTravelTimeEnteringAt_[n] == 0) n ++ ;

	float tt = 0 ;
	num = 0 ;
	if (p >= 0) {				// use a previous interval
	  tt += sumOfTravelTimeEnteringAt_[p] ;
	  num += nSamplesTravelTimeEnteringAt_[p] ;
	}
	if (n < m) {				// use a next interval
	  tt += sumOfTravelTimeEnteringAt_[n] ;
	  num += nSamplesTravelTimeEnteringAt_[n] ;
	}

	if (num) {
	  return tt / num ;
	} else {					// use free flow travel time
	  return length_ / freeSpeed_ ;
	}
  }
}


// Generalized travel time used in RN_LinkTimes and shortest path
// calculation.  It takes into account the freeway biases.

float
RN_Link::generalizeTravelTime(float x)
{
    // YW 4/6/2006: change != to ==, because the manual says "the travel
    // times on freeway links are divided by this factor"!
    //
    // if (linkType() != LINK_TYPE_FREEWAY)
//--- changed to ---
    if (linkType() == LINK_TYPE_FREEWAY)
//--- end of change ---
    {
        x /= theBaseParameter->freewayBias();
    }
    return x;
}
