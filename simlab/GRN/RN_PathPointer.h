//-*-c++-*------------------------------------------------------------
// NAME: Pointer to path table
// AUTH: Qi Yang
// FILE: RN_PathPointer.h
// DATE: Tue Jun 11 00:37:04 1996
//--------------------------------------------------------------------

#ifndef RN_PATHPOINTER_HEADER
#define RN_PATHPOINTER_HEADER

//#include <iostream>
//#include <vector>
#include <cstdio>

class RN_Path;
class RN_Link;
class RN_Node;
class RN_LinkTimes;
class OD_Cell ;

class RN_PathPointer
{
   public:

	  bool IsUsedBy(OD_Cell*) ;

      RN_PathPointer() : path_(NULL), position_(-1), cf_(0.0) {
      }
      RN_PathPointer(RN_Path *p, short int i) :
		 path_(p), position_(i), cf_(0.0) {
      }
      ~RN_PathPointer() { }

      RN_Path* path() { return path_; }
      short int position() { return position_; }

      RN_Link* prev();
      RN_Link* next();
      RN_Node* oriNode();
      RN_Node* desNode();
      RN_Link* link();

      float cost(RN_LinkTimes *info); // travel time based on given info

	  float cf() { return cf_; }
	  void cf(float v) { cf_ = v; }

   private:

      RN_Path *path_;
      short int position_;
	  float cf_;				// commonality factor
};

#endif
