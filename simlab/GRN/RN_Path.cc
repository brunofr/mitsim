//-*-c++-*------------------------------------------------------------
// FILE: RN_Path.C
// DATE: Thu Oct 19 17:28:39 1995
//--------------------------------------------------------------------


#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>

#include "RoadNetwork.h"
#include "RN_Label.h"
#include "RN_Link.h"
#include "RN_Node.h"
#include "RN_Path.h"
#include "RN_PathTable.h"
#include "RN_LinkTimes.h"
#include <iostream>
using namespace std;

int RN_Path::sorted_ = 1;	// element is sorted

RN_Link*
RN_Path::nextLink(int i)
{
   if (i < nLinks() - 1) return links_[i + 1];
   else return NULL;
}

int RN_Path::oriCode()
{
   return oriNode_->code();
}

int RN_Path::desCode()
{
   return desNode_->code();
}

int RN_Path::pathCode()
{
  return code_;
}

// Called by RN_PathParser to initialize a path

int
RN_Path::init(int c, int ori, int des)
{
   static int idx = 0;
   static int last = -1;
   code_ = c;
   if (sorted_ && c <= last) sorted_ = 0;
   else last = c;

   if (!(oriNode_ = theNetwork->findNode(ori))) {
      cerr << "Error:: Unknown origin node <" << ori << ">. ";
      return (-1);
   } else if (!(desNode_ = theNetwork->findNode(des))) {
      cerr << "Error:: Unknown destination node <" << des << ">. ";
      return (-1);
   }
   index_ = idx ++;

   return 0;
}


// Called by RN_PathParser to add a link to a path

int
RN_Path::addLink(int linkcode)
{
   RN_Link *linkptr = theNetwork->findLink(linkcode);
   if (!linkptr) {
      cerr << "Error:: Unkonwn link code <" << linkcode << ">. ";
      return -1;
   }
   RN_Link *prevlink = lastLink();
   if (prevlink && prevlink->dnNode() != linkptr->upNode()) {
      cerr << "Error:: Link <" << linkcode << "> is not connected to "
		   << "link <" << prevlink->code() << ">. ";
      return -1;
   }
   links_.push_back(linkptr);
   return 0;
}


void
RN_Path::print(ostream &os)
{
   register int i;
   os << indent << "{";
   os << code_ << endc
      << oriNode_->code() << endc
      << desNode_->code() << endc;
   os << nLinks() << "{";
   for (i = 0; i < nLinks(); i ++) {
      os << links_[i]->code() << endc;
   }
   os << "}" << endc;
   os << "} # " << firstLink()->upNode()->name();
   os << "->" << lastLink()->dnNode()->name() << endl;
}


// Calculate travel times from kth link on the path to the destination
// based on the given information, assuming enter the kth link at the
// current time.

float
RN_Path::travelTime(RN_LinkTimes *info, int kth)
{
   float t = theSimulationClock->currentTime();
   float x = 0.0;
   float y = 0.0;
   register int i = kth, n = nLinks();
   while (i < n) {
	 int k = links_[i]->index(); // index of the link
	 x += info->linkTime(k, t);     // add the travel time on link k
	 y = info->linkTime(k, t);
	 //  cout << " Manish link cost " << x << " time = " << t << endl;
	  // t += x;               // entry time for next link
	  t += y;
	 i ++;
   }
   return x;
}

// Calculate travel times from kth link on the path to the destination
// based on the given information, assuming enter the kth link at time
// t.

float
RN_Path::travelTime(RN_LinkTimes *info, int kth, float t)
{
   float x = 0.0;
   register int i = kth, n = nLinks();
   while (i < n) {
	 int k = links_[i]->index(); // index of the link
	 x += info->linkTime(k, t); // add the travel time on link k
	 t += x;					// entry time for next link
	 i ++;
   }
   return x;
}

// Register the pointers to path table for the links used in OD paths

void
RN_Path::setPathPointers()
{
   for (register int i = 0; i < nLinks(); i ++) {
      link(i)->addPathPointer(this, i);
   }
}
