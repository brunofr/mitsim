#ifndef YY_RN_BusRunParser_h_included
#define YY_RN_BusRunParser_h_included

#line 1 "/usr/lib/bison.h"
/* before anything */
#ifdef c_plusplus
#ifndef __cplusplus
#define __cplusplus
#endif
#endif
#ifdef __cplusplus
#ifndef YY_USE_CLASS
#define YY_USE_CLASS
#endif
#else
#endif
#include <stdio.h>

/* #line 14 "/usr/lib/bison.h" */
#line 21 "RN_BusRunParser.h"
#define YY_RN_BusRunParser_ERROR_BODY 	DEF_ERROR_BODY
#define YY_RN_BusRunParser_LEX_BODY 	  DEF_LEX_BODY
#define YY_RN_BusRunParser_CONSTRUCTOR_PARAM   RN_BusRunTable* pContainer_
#define YY_RN_BusRunParser_CONSTRUCTOR_INIT    : _pContainer(pContainer_)  \
                           ,_current(NULL)             \
                           ,_ori(0)                    \
                           ,_des(0)                    \
                           ,_pathid(0)                 \

#define YY_RN_BusRunParser_MEMBERS             DEF_MEMBERS \
    RN_BusRunTable* _pContainer ;      \
    RN_Path* _current ;                \
    int _ori ;                         \
    int _des ;                         \
    int _pathid ;                      \


#line 28 "RN_BusRunParser.y"
typedef union {
  long        type_i ;
  double      type_f ;
  const char* type_s ;
} yy_RN_BusRunParser_stype;
#define YY_RN_BusRunParser_STYPE yy_RN_BusRunParser_stype
#line 53 "RN_BusRunParser.y"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <IO/Exception.h>

#include <Tools/Scanner.h>
#include <Tools/ToolKit.h>  

#include <GRN/RN_BusRunTable.h>
#include <GRN/RN_BusSchedule.h>
#include <GRN/RN_BusRoute.h>
#include <GRN/RN_Link.h>

#line 14 "/usr/lib/bison.h"
 /* %{ and %header{ and %union, during decl */
#ifndef YY_RN_BusRunParser_COMPATIBILITY
#ifndef YY_USE_CLASS
#define  YY_RN_BusRunParser_COMPATIBILITY 1
#else
#define  YY_RN_BusRunParser_COMPATIBILITY 0
#endif
#endif

#if YY_RN_BusRunParser_COMPATIBILITY != 0
/* backward compatibility */
#ifdef YYLTYPE
#ifndef YY_RN_BusRunParser_LTYPE
#define YY_RN_BusRunParser_LTYPE YYLTYPE
/* WARNING obsolete !!! user defined YYLTYPE not reported into generated header */
/* use %define LTYPE */
#endif
#endif
#ifdef YYSTYPE
#ifndef YY_RN_BusRunParser_STYPE 
#define YY_RN_BusRunParser_STYPE YYSTYPE
/* WARNING obsolete !!! user defined YYSTYPE not reported into generated header */
/* use %define STYPE */
#endif
#endif
#ifdef YYDEBUG
#ifndef YY_RN_BusRunParser_DEBUG
#define  YY_RN_BusRunParser_DEBUG YYDEBUG
/* WARNING obsolete !!! user defined YYDEBUG not reported into generated header */
/* use %define DEBUG */
#endif
#endif
#ifdef YY_RN_BusRunParser_STYPE
#ifndef yystype
#define yystype YY_RN_BusRunParser_STYPE
#endif
#endif
/* use goto to be compatible */
#ifndef YY_RN_BusRunParser_USE_GOTO
#define YY_RN_BusRunParser_USE_GOTO 1
#endif
#endif

/* use no goto to be clean in C++ */
#ifndef YY_RN_BusRunParser_USE_GOTO
#define YY_RN_BusRunParser_USE_GOTO 0
#endif

#ifndef YY_RN_BusRunParser_PURE

/* #line 63 "/usr/lib/bison.h" */
#line 114 "RN_BusRunParser.h"

#line 63 "/usr/lib/bison.h"
/* YY_RN_BusRunParser_PURE */
#endif

/* #line 65 "/usr/lib/bison.h" */
#line 121 "RN_BusRunParser.h"

#line 65 "/usr/lib/bison.h"
/* prefix */
#ifndef YY_RN_BusRunParser_DEBUG

/* #line 67 "/usr/lib/bison.h" */
#line 128 "RN_BusRunParser.h"

#line 67 "/usr/lib/bison.h"
/* YY_RN_BusRunParser_DEBUG */
#endif
#ifndef YY_RN_BusRunParser_LSP_NEEDED

/* #line 70 "/usr/lib/bison.h" */
#line 136 "RN_BusRunParser.h"

#line 70 "/usr/lib/bison.h"
 /* YY_RN_BusRunParser_LSP_NEEDED*/
#endif
/* DEFAULT LTYPE*/
#ifdef YY_RN_BusRunParser_LSP_NEEDED
#ifndef YY_RN_BusRunParser_LTYPE
typedef
  struct yyltype
    {
      int timestamp;
      int first_line;
      int first_column;
      int last_line;
      int last_column;
      char *text;
   }
  yyltype;

#define YY_RN_BusRunParser_LTYPE yyltype
#endif
#endif
/* DEFAULT STYPE*/
#ifndef YY_RN_BusRunParser_STYPE
#define YY_RN_BusRunParser_STYPE int
#endif
/* DEFAULT MISCELANEOUS */
#ifndef YY_RN_BusRunParser_PARSE
#define YY_RN_BusRunParser_PARSE yyparse
#endif
#ifndef YY_RN_BusRunParser_LEX
#define YY_RN_BusRunParser_LEX yylex
#endif
#ifndef YY_RN_BusRunParser_LVAL
#define YY_RN_BusRunParser_LVAL yylval
#endif
#ifndef YY_RN_BusRunParser_LLOC
#define YY_RN_BusRunParser_LLOC yylloc
#endif
#ifndef YY_RN_BusRunParser_CHAR
#define YY_RN_BusRunParser_CHAR yychar
#endif
#ifndef YY_RN_BusRunParser_NERRS
#define YY_RN_BusRunParser_NERRS yynerrs
#endif
#ifndef YY_RN_BusRunParser_DEBUG_FLAG
#define YY_RN_BusRunParser_DEBUG_FLAG yydebug
#endif
#ifndef YY_RN_BusRunParser_ERROR
#define YY_RN_BusRunParser_ERROR yyerror
#endif

#ifndef YY_RN_BusRunParser_PARSE_PARAM
#ifndef __STDC__
#ifndef __cplusplus
#ifndef YY_USE_CLASS
#define YY_RN_BusRunParser_PARSE_PARAM
#ifndef YY_RN_BusRunParser_PARSE_PARAM_DEF
#define YY_RN_BusRunParser_PARSE_PARAM_DEF
#endif
#endif
#endif
#endif
#ifndef YY_RN_BusRunParser_PARSE_PARAM
#define YY_RN_BusRunParser_PARSE_PARAM void
#endif
#endif

/* TOKEN C */
#ifndef YY_USE_CLASS

#ifndef YY_RN_BusRunParser_PURE
extern YY_RN_BusRunParser_STYPE YY_RN_BusRunParser_LVAL;
#endif


/* #line 143 "/usr/lib/bison.h" */
#line 214 "RN_BusRunParser.h"
#define	LEX_EQUAL	258
#define	LEX_COMMA	259
#define	LEX_COLON	260
#define	LEX_SEMICOLON	261
#define	LEX_OAB	262
#define	LEX_CAB	263
#define	LEX_OBB	264
#define	LEX_CBB	265
#define	LEX_OCB	266
#define	LEX_CCB	267
#define	LEX_INT	268
#define	LEX_REAL	269
#define	LEX_TIME	270
#define	LEX_PAIR	271
#define	LEX_TAG	272
#define	LEX_STRING	273
#define	LEX_DUP	274
#define	LEX_END	275


#line 143 "/usr/lib/bison.h"
 /* #defines token */
/* after #define tokens, before const tokens S5*/
#else
#ifndef YY_RN_BusRunParser_CLASS
#define YY_RN_BusRunParser_CLASS RN_BusRunParser
#endif

#ifndef YY_RN_BusRunParser_INHERIT
#define YY_RN_BusRunParser_INHERIT
#endif
#ifndef YY_RN_BusRunParser_MEMBERS
#define YY_RN_BusRunParser_MEMBERS 
#endif
#ifndef YY_RN_BusRunParser_LEX_BODY
#define YY_RN_BusRunParser_LEX_BODY  
#endif
#ifndef YY_RN_BusRunParser_ERROR_BODY
#define YY_RN_BusRunParser_ERROR_BODY  
#endif
#ifndef YY_RN_BusRunParser_CONSTRUCTOR_PARAM
#define YY_RN_BusRunParser_CONSTRUCTOR_PARAM
#endif
/* choose between enum and const */
#ifndef YY_RN_BusRunParser_USE_CONST_TOKEN
#define YY_RN_BusRunParser_USE_CONST_TOKEN 0
/* yes enum is more compatible with flex,  */
/* so by default we use it */ 
#endif
#if YY_RN_BusRunParser_USE_CONST_TOKEN != 0
#ifndef YY_RN_BusRunParser_ENUM_TOKEN
#define YY_RN_BusRunParser_ENUM_TOKEN yy_RN_BusRunParser_enum_token
#endif
#endif

class YY_RN_BusRunParser_CLASS YY_RN_BusRunParser_INHERIT
{
public: 
#if YY_RN_BusRunParser_USE_CONST_TOKEN != 0
/* static const int token ... */

/* #line 182 "/usr/lib/bison.h" */
#line 277 "RN_BusRunParser.h"
static const int LEX_EQUAL;
static const int LEX_COMMA;
static const int LEX_COLON;
static const int LEX_SEMICOLON;
static const int LEX_OAB;
static const int LEX_CAB;
static const int LEX_OBB;
static const int LEX_CBB;
static const int LEX_OCB;
static const int LEX_CCB;
static const int LEX_INT;
static const int LEX_REAL;
static const int LEX_TIME;
static const int LEX_PAIR;
static const int LEX_TAG;
static const int LEX_STRING;
static const int LEX_DUP;
static const int LEX_END;


#line 182 "/usr/lib/bison.h"
 /* decl const */
#else
enum YY_RN_BusRunParser_ENUM_TOKEN { YY_RN_BusRunParser_NULL_TOKEN=0

/* #line 185 "/usr/lib/bison.h" */
#line 304 "RN_BusRunParser.h"
	,LEX_EQUAL=258
	,LEX_COMMA=259
	,LEX_COLON=260
	,LEX_SEMICOLON=261
	,LEX_OAB=262
	,LEX_CAB=263
	,LEX_OBB=264
	,LEX_CBB=265
	,LEX_OCB=266
	,LEX_CCB=267
	,LEX_INT=268
	,LEX_REAL=269
	,LEX_TIME=270
	,LEX_PAIR=271
	,LEX_TAG=272
	,LEX_STRING=273
	,LEX_DUP=274
	,LEX_END=275


#line 185 "/usr/lib/bison.h"
 /* enum token */
     }; /* end of enum declaration */
#endif
public:
 int YY_RN_BusRunParser_PARSE(YY_RN_BusRunParser_PARSE_PARAM);
 virtual void YY_RN_BusRunParser_ERROR(char *msg) YY_RN_BusRunParser_ERROR_BODY;
#ifdef YY_RN_BusRunParser_PURE
#ifdef YY_RN_BusRunParser_LSP_NEEDED
 virtual int  YY_RN_BusRunParser_LEX(YY_RN_BusRunParser_STYPE *YY_RN_BusRunParser_LVAL,YY_RN_BusRunParser_LTYPE *YY_RN_BusRunParser_LLOC) YY_RN_BusRunParser_LEX_BODY;
#else
 virtual int  YY_RN_BusRunParser_LEX(YY_RN_BusRunParser_STYPE *YY_RN_BusRunParser_LVAL) YY_RN_BusRunParser_LEX_BODY;
#endif
#else
 virtual int YY_RN_BusRunParser_LEX() YY_RN_BusRunParser_LEX_BODY;
 YY_RN_BusRunParser_STYPE YY_RN_BusRunParser_LVAL;
#ifdef YY_RN_BusRunParser_LSP_NEEDED
 YY_RN_BusRunParser_LTYPE YY_RN_BusRunParser_LLOC;
#endif
 int YY_RN_BusRunParser_NERRS;
 int YY_RN_BusRunParser_CHAR;
#endif
#if YY_RN_BusRunParser_DEBUG != 0
public:
 int YY_RN_BusRunParser_DEBUG_FLAG;	/*  nonzero means print parse trace	*/
#endif
public:
 YY_RN_BusRunParser_CLASS(YY_RN_BusRunParser_CONSTRUCTOR_PARAM);
public:
 YY_RN_BusRunParser_MEMBERS 
};
/* other declare folow */
#endif


#if YY_RN_BusRunParser_COMPATIBILITY != 0
/* backward compatibility */
#ifndef YYSTYPE
#define YYSTYPE YY_RN_BusRunParser_STYPE
#endif

#ifndef YYLTYPE
#define YYLTYPE YY_RN_BusRunParser_LTYPE
#endif
#ifndef YYDEBUG
#ifdef YY_RN_BusRunParser_DEBUG 
#define YYDEBUG YY_RN_BusRunParser_DEBUG
#endif
#endif

#endif
/* END */

/* #line 236 "/usr/lib/bison.h" */
#line 379 "RN_BusRunParser.h"

#line 206 "RN_BusRunParser.y"

  // empty
#endif
