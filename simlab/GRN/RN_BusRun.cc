//-*-c++-*------------------------------------------------------------
// FILE: RN_BusRun.C
// DATE: Thu Oct 19 17:28:39 1995
//--------------------------------------------------------------------


#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>

#include "RoadNetwork.h"
#include "RN_Label.h"
#include "RN_Link.h"
#include "RN_Node.h"
#include "RN_BusRun.h"
#include "RN_BusRunTable.h"
#include "RN_BusRoute.h"
#include "RN_BusRouteTable.h"
#include "RN_BusSchedule.h"
#include "RN_BusScheduleTable.h"
#include "RN_BusStop.h"
#include "RN_LinkTimes.h"
#include <iostream>
using namespace std;

int RN_BusRun::sorted_ = 1;	// element is sorted

RN_BusSchedule*
RN_BusRun::nextTrip(int i)
{
   if (i < nTrips() - 1) return trips_[i + 1];
   else return NULL;
}

// Called by RN_BusRunParser to initialize a bus route

int
RN_BusRun::init(int c)
{
   static int idx = 0;
   static int last = -1;
   code_ = c;
   if (sorted_ && c <= last) sorted_ = 0;
   else last = c;

   index_ = idx ++;

   return 0;
}


// Called by RN_BusRunParser to add a trip to bus run

int
RN_BusRun::addTrip(int tripcode)
{
   RN_BusSchedule *tripptr = theBusScheduleTable->findBusSchedule(tripcode);
   if (!tripptr) {
      cerr << "Error:: Unknown schedule code <" << tripcode << ">. ";
      return -1;
   }
   RN_BusSchedule *prevtrip = lastTrip();

   if (prevtrip && prevtrip->tripRoute()->lastRouteLink()->dnNode()
              != tripptr->tripRoute()->firstRouteLink()->upNode()) {
            cerr << "Error:: Trip <" << tripcode << "> is not connected to "
		   << "trip <" << prevtrip->code() << ">. ";
		   return -1;
	}

   trips_.push_back(tripptr);
   return 0;
}

void
RN_BusRun::print(ostream &os)
{
   register int i;
   os << indent << "{";
   os << code_ << endc;
   os << nTrips() << "{";
   for (i = 0; i < nTrips(); i ++) {
      os << trips_[i]->code() << endc;
   }
   os << "}" << endc;
   os << "}" << endl;
}

RN_BusSchedule* 
RN_BusRun::findTrip(int c)
{
   vector<RN_BusSchedule*>::iterator i;
   i = find_if(trips_.begin(), trips_.end(), CodeEqual(c));
   if (i != trips_.end()) return *i;
   else return NULL;
}

void
RN_BusRun::setBusRunPointers()
{
  int counter = 0; // Dan - use counter so as not to repeat index j for each entry into the outer "for" loop
   for (register int i = 0; i < nTrips(); i ++) {
     for (register int j = 0; j < trips_[i]->tripRoute()->nRouteLinks(); j ++) {
      trip(i)->tripRoute()->routelink(j)->addPathPointer(theBusRunTable->findPath(code()), counter);
      counter ++;
     }
   }
}
