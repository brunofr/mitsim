//-*-c++-*------------------------------------------------------------
// FILE: RN_LinkTimes.C
// NOTE: Stores time variant link travel times
// AUTH: Qi Yang
// DATE: Wed Oct 18 14:29:29 1995
//--------------------------------------------------------------------

#include <iostream>
#include <new>
#include <cstring>
#include <cctype>

#include <Tools/Math.h>

#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <Tools/SimulationClock.h>
#include <Tools/SimulationEngine.h>

#include <IO/Exception.h>

#include "RoadNetwork.h"
#include "Parameter.h"
#include "RN_Link.h"
#include "RN_LinkTimes.h"
#include "RN_PathTable.h"
#include "BusAssignmentTable.h"
#include "RN_DynamicRoute.h"
#include "RN_CtrlStation.h"
#include "RN_SurvStation.h"
using namespace std;

RN_LinkTimes::RN_LinkTimes(const char *filename)
   : linkTimes_(0), avgLinkTime_(0),
	 infoPeriods_(0),
     preTripGuidance_(1)
{
   if (filename) {
      filename_ = Copy(filename);
   } else {
      filename_ = NULL;
   }
}

RN_LinkTimes::~RN_LinkTimes()
{
   delete [] filename_;
   delete [] linkTimes_;
   if (infoPeriods_ > 1) {
	  delete[] avgLinkTime_;
   }
}

void
RN_LinkTimes::initTravelTimes()
{

  if (filename_) 
    cout << "RN_LinkTimes: Filename_ is true " << filename_ << endl;
  else
    cout << "RN_LinkTimes: Filename_ is false" << filename_ << endl;
   if (filename_) {

     // Parameters will be read from the file
     cout << "RN_LinkTimes: init by read\n";
     
      read(filename_);

   } else {
      
     cout << "RN_LinkTimes: init manually\n";
          
      infoStartPeriod_ = 0;
      infoStartTime_ = (long int)Round(theSimulationClock->startTime());
      infoStopTime_ = (long int)Round(theSimulationClock->stopTime());
      infoPeriodLength_ = infoStopTime_ - infoStartTime_;
      infoTotalLength_ = infoPeriodLength_;
      infoPeriods_ = 1;
      
      cout << "\nRN_LinkTimes:" << " infoStartPeriod is " << infoStartPeriod_ 
           << " infoPeriodLength is " << infoPeriodLength_ << " infoTotalLength is " 
           << infoTotalLength_ << " infoPeriods is " << infoPeriods_ 
           << " infoStartTime is " << infoStartTime_ 
           << " infoStopTime is " << infoStopTime_ << '\n';
      

      calcLinkTravelTimes();
   }

   if ((theSpFlag & INFO_FLAG_DYNAMIC) && infoPeriods() < 2) {
      cerr << "Warning:: Time invariant link travel times used "
		   << "as dynamic routing information." << endl
		   << "    File: " << filename_ << endl
		   << " Sp Flag: " << theSpFlag << endl;
   }
}


// Copy constructor

RN_LinkTimes::RN_LinkTimes(const RN_LinkTimes& rnt)
{
   *this = rnt;
}

RN_LinkTimes& 
RN_LinkTimes::operator=(const RN_LinkTimes& rnt)
{
   if (filename_) delete [] filename_;
   if (linkTimes_) delete [] linkTimes_;
   if (infoPeriods_ > 1) {
	 delete [] avgLinkTime_;
   }

   if (rnt.filename_) {
      filename_ = Copy(rnt.filename_);
   } else {
      filename_ = NULL;
   }

   cout << "RN_LinkTimes: assignment operator\n";
   infoStartPeriod_ = rnt.infoStartPeriod_;
   infoStartTime_ = rnt.infoStartTime_;
   infoStopTime_ = rnt.infoStopTime_;
   infoPeriodLength_ = rnt.infoPeriodLength_;
   infoTotalLength_ = rnt.infoTotalLength_;
   infoPeriods_ = rnt.infoPeriods_;

   cout << "\nRN_LinkTimes.cc:" << " infoStartPeriod is " << infoStartPeriod_
        << " infoPeriodLength is " << infoPeriodLength_ << " infoTotalLength is "
        << infoTotalLength_ << " infoPeriods is " << infoPeriods_ << " infoStartTime is "
        << infoStartTime_ << " infoStopTime is " << infoStopTime_ << '\n';

   int nrows = nLinks();
   unsigned long int size = nrows * infoPeriods_;
   linkTimes_ = new float [size];
   memcpy(linkTimes_, rnt.linkTimes_, sizeof(float) * size);
   if (infoPeriods_ > 1) {
      avgLinkTime_ = new float [nrows];
      memcpy(avgLinkTime_, rnt.avgLinkTime_, sizeof(float) * nrows);
   } else {
      avgLinkTime_ = linkTimes_;
   }
   return *this ;
}

int
RN_LinkTimes::nLinks()
{
   return theNetwork->nLinks();
}


int
RN_LinkTimes::nNodes()
{
   return theNetwork->nNodes();
}

int
RN_LinkTimes::nDestNodes()
{
   return theNetwork->nDestNodes();
}


// Create the default travel times for each link.  This function
// should be called only once.

void
RN_LinkTimes::calcLinkTravelTimes()
{
   int i, j, n = nLinks();

   if (!linkTimes_) {
      linkTimes_ = new float[nLinks() * infoPeriods_];
   }

   for (i = 0; i < n; i ++) {
      float x = theNetwork->link(i)->generalizedTravelTime();
      for (j = 0; j < infoPeriods_; j ++) {
		 linkTimes_[i * infoPeriods_ + j] = x;
      }
   }

   if (infoPeriods_ > 1) {
      if (!avgLinkTime_) avgLinkTime_ = new float [n];
      for (i = 0; i < n; i ++) {
		 float sum = 0.0;
		 for (j = 0; j < infoPeriods_; j ++) {
			sum += linkTimes_[i * infoPeriods_ + j];
		 }
		 avgLinkTime_[i] = sum / infoPeriods_;
      }
   } else {
      avgLinkTime_ = linkTimes_;
   }
}


// Update the link travel times.  The result is a linear combination
// of the previous data and new data calculated in the simulation
// (e.g., based on the average of the expected travel times of all
// vehicles that are currently in the link or based on the sensors
// data collected in the most recent time interval, depends on  the
// current value of RN_Link::travelTime() is defined in the
// derived class).

void
RN_LinkTimes::updateLinkTravelTimes(float alpha)
{
   float x, *py;
   register int i, j, n = nLinks();
   int k = whichPeriod(theSimulationClock->currentTime());
   for (i = 0; i < n; i ++) {
	  RN_Link *pl = theNetwork->link(i);

	  // Calculate travel time based speed/density relationship
 
	  float z = pl->calcCurrentTravelTime();

      x = pl->generalizeTravelTime(z);

	  // Update travel time for all future intervals using the same
	  // estimates. This is simulating the system that provides
	  // guidance based on prevailing/instaneous traffic condition.

      for (j = k; j < infoPeriods_; j ++) {
		 py = linkTimes_ + i * infoPeriods_ + j;
		 *py = alpha * x + (1.0 - alpha) * (*py);
	  }

	  if (infoPeriods_ > 1) {
		 avgLinkTime_[i] = x;
	  }
   }
}


// Read link travel time from a file. This is another way to update
// the link travel time.

void
RN_LinkTimes::read(const char *filename, float alpha)
{
   if (filename_ != filename) {
      if (filename_) {
		 delete [] filename_;
      }
      filename_ = Copy(filename);
   }
   Reader is(filename);
   
   const char *s;
   s = Str("Loading link travel times <%s>", filename);
   cout << "RN_LinkTimes: Reading from" << filename << '\n';
   theNetwork->updateProgress(s);

   int col;			// start column
   int ncols;			// number of colomns
   int length;			// length in seconds per period

   is >> col >> ncols >> length;

   cout << "\nRN_LinkTimes: "  "Right before if conditions\n";
   
   cout << __FILE__ << ":" << __LINE__ << ' ' << __FUNCTION__
        << "\nThe starting minute for this linktime file is infoStartPeriod=" << infoStartPeriod_
        << ";\n each column corresponds to infoPeriodLength=" << infoPeriodLength_
        << " (sec);\n infoTotalLength=" << infoTotalLength_ << " (sec);\n # of columns is infoPeriods="
        << infoPeriods_ << "\n infoStartTime is " << infoStartTime_ << " (sec);\n infoStopTime is "
        << infoStopTime_ << " (sec)" << endl;

   if (!linkTimes_) {
     
       cout << __FILE__ << ":" << __LINE__ << " linkTime_ == NULL --> to be initialized first"
           "\nRN_LinkTimes: initializing linkTimes from file with col=" << col
            <<" length=" << length << " ncols=" << ncols <<endl;  
     
     long int zero = (long int)Round(theSimulationClock->startTime());
      infoStartPeriod_ = col;
      infoPeriodLength_ = length;
      infoTotalLength_ = ncols * length;
      infoPeriods_ = ncols;
      infoStartTime_ = zero + col * length;
      infoStopTime_ = infoStartTime_ + infoTotalLength_;
      calcLinkTravelTimes();	// set the default travel times
      col = 0;			// start from 1st column and end at ncols
      
      cout << "Initialized value:\n"
          "The starting minute for this linktime file is infoStartPeriod=" << infoStartPeriod_
           << ";\n each column corresponds to infoPeriodLength=" << infoPeriodLength_
           << " (sec);\n infoTotalLength=" << infoTotalLength_ << " (sec);\n # of columns is infoPeriods="
           << infoPeriods_ << "\n infoStartTime is " << infoStartTime_ << " (sec);\n infoStopTime is "
           << infoStopTime_ << " (sec)" << endl;
      
   } else if (col < infoStartPeriod_ || col >= infoPeriods_ ||
			  ncols < 0 || col + ncols > infoPeriods_ ||
			  length != infoPeriodLength_) {
      cerr << "Error:: With header in link travel time table. ";
      is.reference();
      theException->exit(1);
   } else {
      ncols += col;		// start from 1st column and end at ncols
   }

   if (!is.findToken(OPEN_TOKEN)) theException->exit(1);

   cout << __FILE__ << ":" << __LINE__ << " Continue to parse the input file ..."
       "\nRN_LinkTimes: col ="<<col<<" ncols="<<ncols<<" infoPeriods_="<<infoPeriods_ << endl;
   
   while (is.peek() != CLOSE_TOKEN) {
      int i, j, code, type;
      float len, x, *py;
      RN_Link *pl;
      is >> code >> type >> len;
      //cout << "JUST READ IN THE HEADER STUFF" << endl;
      if (pl = theNetwork->findLink(code)) {
		 i = pl->index();
		 float sum = 0.0;
		 for (j = col; j < ncols; j ++) {
			is >> x;
			//cout << "RN_LinkTimes: JUST READ IN CHAR" << x << endl;   //florian debug
			x = pl->generalizeTravelTime(x);
			py = linkTimes_ + i * infoPeriods_ + j;      //infoPeriods_ is used as the col length of the matrix
			sum += alpha * (x - (*py));
			*py = alpha * x + (1.0 - alpha) * (*py);
		 }
		 if (infoPeriods_ > 1) {
			avgLinkTime_[i] += sum / infoPeriods_;
		 }
      } else {
		 cerr << "Error:: Unknown link <" << code << ">. ";
		 is.reference();
		 theException->exit(1);
      }
   }
   if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);

   is.close();
}


// Returns the interval that contains time t

int
RN_LinkTimes::whichPeriod(double t)
{
    // YW 4/5/2006: change int p to long int p 
    long int p = (long int) t;
   
   if (p <= infoStartTime_) {	// earlier
      return 0;
   } else if (p >= infoStopTime_) { // later
      return infoPeriods_ - 1;
   } else {                     // between
      return (int) (t - infoStartTime_) / infoPeriodLength_;
   }
}


// Returns the time that represents interval p.

double
RN_LinkTimes::whatTime(int p)
{
   return infoStartTime_ + (p + 0.5) * infoPeriodLength_;
}


// Returns the average travel time on a given link

float
RN_LinkTimes::avgLinkTime(RN_Link *i)
{
   return avgLinkTime(i->index());
}


// Returns the expected link travel time at the given entry time

float
RN_LinkTimes::linkTime(int k, double timesec)
{
   if (infoPeriods_ > 1) {
      float *y = linkTimes_ + k * infoPeriods_;
      float dt = (timesec - infoStartTime_) / infoPeriodLength_ + 0.5;
      int i = (int)dt;
      if (i < 1) {
		 return y[0];
      } else if (i >= infoPeriods_) {
		 return y[infoPeriods_ - 1];
      } else {
		 dt = timesec - infoStartTime_ - (i - 0.5) * infoPeriodLength_;
		 float z = y[i-1];
		 return z + (y[i] - z) / infoPeriodLength_ * dt;
      }
   } else {
      return linkTimes_[k];
   }
}


float
RN_LinkTimes::linkTime(RN_Link *ilink, double timesec)
{
   return linkTime(ilink->index(), timesec);
}


// Print time dependent travel times

void
RN_LinkTimes::printLinkTimes(ostream &os)
{
   if (!theSimulationEngine->skipComment()) {
      os << endl << "% GENERALIZED LINK TRAVEL TIMES" << endl
		 << infoStartPeriod() << "\t% Start period" << endl
		 << infoPeriods() << "\t% Number of periods" << endl
		 << infoPeriodLength() << "\t% Length of time period" << endl
		 << endl << OPEN_TOKEN << endl
		 << "%" << indent << "ID Type Length T1 T2 ... Tn" << endl;
   }
   RN_Link *pl;
   double len;
   int nperiods = infoPeriods();
   int col = infoStartPeriod();
   int nlinks = nLinks();
   for (int i = 0; i < nlinks; i ++) {
      pl = theNetwork->link(i);
      len = pl->length() / theBaseParameter->lengthFactor();
      os << indent << pl->code()
		 << endc << pl->type()
		 << endc << Fix(len, 0.1);
      for (int j = col; j < nperiods; j ++) {
		 double t = linkTimes_[i * nperiods + j];
		 os << endc << Fix(t, 0.1);
      }
      os << endl;
   }
   if (!theSimulationEngine->skipComment()) {
      os << CLOSE_TOKEN << endl;
   }
}
