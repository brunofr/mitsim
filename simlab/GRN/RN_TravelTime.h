//-*-c++-*------------------------------------------------------------
// NAME: Massachusetts Institue of Technology MITSIM
// NOTE: 
// AUTH: Peter J. Welch
// FILE: RN_TravelTime.h
// DATE: Mon Mar  4 10:44:56 1996
//--------------------------------------------------------------------

#ifndef RN_TRAVELTIME_HEADER
#define RN_TRAVELTIME_HEADER



//--------------------------------------------------------------------
// CLASS NAME: RN_TravelTime
// 
// Not to be confused with RN_TravelTimes whose name should be changed.
// This class is simply a storage area for the MOE TravelTimes filter.  
// It will be used by the MOE and TS
// 
//--------------------------------------------------------------------


#include <Tools/TimeInterval.h>
#include <Tools/Reader.h>

class RN_TravelTime {
      
   public:
      RN_TravelTime();
      ~RN_TravelTime() { }
      virtual int read(Reader &is);
      int checkVehicleType(int slot, int vehicle_type);
      TimeInterval & time_interval() { return time_interval_; }
      int num_veh_type() { return num_veh_type_; }

   protected:
      unsigned int * veh_type_array_;
      int num_veh_type_;
      TimeInterval time_interval_;
};

extern RN_TravelTime * theTravelTime;

#endif


