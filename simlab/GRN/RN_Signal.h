//-*-c++-*------------------------------------------------------------
// FILE: RN_Signal.h
// AUTH: Qi Yang
// DATE: Fri Oct 20 22:47:31 1995
//--------------------------------------------------------------------

#ifndef RN_SIGNAL_HEADER
#define RN_SIGNAL_HEADER

#include <iostream>

#include <Templates/Object.h>
#include <Tools/Math.h>
#include <IO/IOService.h>

#include "WcsPoint.h"
#include "Constants.h"

class RN_BisonParser;
class RoadNetwork;
class RN_Link;
class RN_Segment;
class RN_Lane;
class RN_CtrlStation;

// NOTE! The following forward declarations are required because of
// the multiple inheritance structure problem defined at the end of
// RN_Signal

class DRN_DrawingArea;
class TS_Vehicle;

class RN_Signal : public CodedObject
{
      friend class RN_BisonParser;
      friend class RoadNetwork;

   protected:

      static int sorted_;	// 1 = sorted by code

      int index_;		// index in array 
      RN_Lane* lane_;		// pointer to lane
      RN_CtrlStation* station_;	// station 
      unsigned int state_;	// current state

   public:

      RN_Signal();
      virtual ~RN_Signal() { }

      static inline int sorted() { return sorted_; }

      virtual unsigned int clearState(unsigned int flag);
      virtual unsigned int setState(unsigned int flag = 0);
      virtual unsigned int state() { return state_; }
      virtual void state(unsigned int s) { state_ = s; }
      
      // These two functions apply to traffic signal only

      unsigned int stateForExit(int i);
      unsigned int stateForExit(unsigned int, int i);

      // Iterator on the global array

      virtual RN_Signal* prev();
      virtual RN_Signal* next();

      inline int index() { return index_; }
      inline RN_CtrlStation* station() { return station_;}
      RN_Lane* lane() { return lane_; }
      RN_Segment* segment();
      RN_Link* link();

      virtual float position();
      virtual unsigned int stateIoToInternal(unsigned int s);
      virtual unsigned int stateInternalToIo(unsigned int s);

      virtual int isLinkWide();
      virtual int type();
      virtual float distance();
      
      virtual float visibility();

      virtual int init(int code, unsigned int state, int lane = -1);
	  virtual void addIntoNetwork();
      virtual void print(std::ostream &os = std::cout);

      virtual float acceleration(TS_Vehicle *, float) {
		 return FLT_INF;
      }
      virtual float squeeze(TS_Vehicle *, float) {
		 return FLT_INF;
      }

      // IMPORTANT NOTE!!
      // ----------------
      // These functions are nonsensical in the context of RN_Signal.
      // They are here to avoid casting to derived classes which is
      // not possible based on the multiple inheritance / virtual
      // inheritance we have used for signals.  See Section 10.6c of
      // The Annotated C++ Reference Manual (pg 227) by Ellis and
      // Stroustrup for more info.
      // -----------------
      // IMPORTANT NOTE!!

      // -- TollBooth Functions --

      virtual int isEtc() { return 0; }
      virtual unsigned int speed() { return 0; }
      virtual float delay(TS_Vehicle *) { return 0; }

      // -- BusStop Functions --

      virtual int leftOrRight();
      virtual unsigned int stopLength() { return 0; }

      // -- Drawing Functions --

      virtual void calcGeometricData() { }
	  // Should return Pixel* but that requires X stuff
      virtual unsigned long *calcColor(unsigned int) { return NULL; }
      virtual int draw(DRN_DrawingArea *) { return 0; }
      virtual WcsPoint& center() { return theWcsDummyPoint; }

      // -- Incident Functions --

      virtual void check() { }
      virtual float speedLimit() { return 0; }

      // -- Communication functions --
      
	  virtual void send(IOService &) { }
	  virtual void receive(IOService &) { }
};

#endif
