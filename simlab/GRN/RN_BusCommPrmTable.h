//-*-c++-*------------------------------------------------------------
// NAME: A parser for bus stop parameters
// AUTH: Qi Yang & Daniel Morgan
// FILE: RN_BusCommPrmTable.h
// DATE: Fri Nov 30 16:55:50 2001
//--------------------------------------------------------------------

#ifndef RN_BUSCOMMPRMTABLE_HEADER
#define RN_BUSCOMMPRMTABLE_HEADER

#include <iostream>
#include <vector>

#include "RN_BusStop.h"

const int NUM_CONDITIONS = 5;	// Default initial num of bus stops

class RN_BusCommPrmParser;
class RN_BusCommPrmBisonParser;

class RN_BusCommPrmTable
{
      friend class RN_BusCommPrmBisonParser;
      friend class RN_BusCommPrmParser;

   protected:

      static char * name_;		// file name
      double startTime_;		// dep time smaller than this is skipped
      double nextTime_;			// time to read od pairs

      // Dan: the following are for bus sensors
      int nConditions_;                   // number of priority conditions
      std::vector<int> routeID_;               // route to which the condition applies
      std::vector<int> conditionCode_;         /* code that describes the applicable conditions:
                                             0: load only
					     1: schedule deviation only
					     2: headway only
					     3: load AND schedule
					     4: load AND headway
					  */
      std::vector<int> minLoadThresh_;         // minimum bus load, below which priority will NOT be activated
      std::vector<double> maxAllowSchedDev_;    // max deviation ahead of schedule, above which priority will NOT be activated
      std::vector<double> maxAllowHW_;          // max headway, above which priority WILL be activated

   public:

      RN_BusCommPrmTable();

      virtual ~RN_BusCommPrmTable() {}
	
      void open(double start_time = -86400.0);
      double read();

      static inline char* name() { return name_; }
      static inline char** nameptr() { return &name_; }

      inline double startTime() { return startTime_; }
	  inline double nextTime() { return nextTime_; }

      inline int skip() {
	  return (nextTime_ < startTime_);
      }

      virtual int initBusCommPrms(double start);   

      virtual int addCondition(int type, int code);
      virtual int addCondition(int type, int code, int x);
      virtual int addCondition(int type, int code, double y);
      virtual int addCondition(int type, int code, int x, double y);

      virtual void print(std::ostream &os = std::cout);

      int nConditions();
      int minLoadThresh(int i) {
		 if (i < 0) return -1;
		 else if (i >= nConditions()) return -1;
		 else return minLoadThresh_[i];
	  }
      double maxAllowSchedDev(int i) {
		 if (i < 0) return -1;
		 else if (i >= nConditions()) return -1;
		 else return maxAllowSchedDev_[i];
	  }
      double maxAllowHW(int i) {
		 if (i < 0) return -1;
		 else if (i >= nConditions()) return -1;
		 else return maxAllowHW_[i];
	  }

      virtual bool conditionSatisfied(int bc);

   private:

      RN_BusCommPrmParser* parser_; // the parser

};

extern RN_BusCommPrmTable * theBusCommPrmTable;

#endif
