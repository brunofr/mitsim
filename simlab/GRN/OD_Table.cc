//-*-c++-*------------------------------------------------------------
// FILE: OD_Table.C
// DATE: Thu Oct 19 16:09:06 1995
//--------------------------------------------------------------------

#include <Tools/SimulationClock.h>
#include <Tools/ToolKit.h>
#include <Tools/Math.h>
#include <UTL/Misc.h>
#include <IO/Exception.h>

#include "OD_Parser.h"
#include "OD_Table.h"
#include "OD_Cell.h"
#include "RN_Node.h"
#include "RN_Vehicle.h"
#include "RN_Link.h"
#include "Parameter.h"
#include "RoadNetwork.h"
#include <iostream>
using namespace std;

OD_Table * theODTable = NULL;

char * OD_Table::name_ = strdup("od.dat");

OD_Table::OD_Table()
   : parser_(NULL), nextTime_(-DBL_INF),
     nCellsParsed_(0),
     type_(0),
     scale_(1.0)
{
   theODTable = this;
}


void
OD_Table::clean()
{
   cells_.remove();

   nextTime_ = theSimulationClock->stopTime();
   type_ = 0;
   scale_ = 1.0;
   nCellsParsed_ = 0;
   delete parser_;
   parser_ = NULL;
}


// Open trip table file and create a OD Parser

void OD_Table::open(const char *fn)
{
  if (!fn) {
	  fn = ToolKit::optionalInputFile(name_) ;
  } else {
	  fn = ExpandEnvVars(fn) ;
  }
  if (!fn) theException->exit(1) ;

  ::Copy(&name_, fn) ;

  // We will create the parser in read()
  if (parser_) delete parser_ ;
  parser_ = NULL ;
}

// Read trip tables for next time period

double OD_Table::read()
{
  const char* fn ;
  if (parser_) {
	fn = NULL ;					// continue parsing of already opened file
  } else {
	parser_ = new OD_Parser(this);
	fn = name_ ;				// open the file to be parsed
  }

  int count = 0;
  while (nextTime_ <= theSimulationClock->currentTime()) {
	theNetwork->updateProgress("Loading OD tables ...");
	parser_->parse(fn);
	count ++;
  }
  if (count) theNetwork->updateProgress();
  return nextTime_;
}


// Called by parser to setup the od matrix and update time for next
// vehicles departure.

int
OD_Table::init(double s, int t, double f)
{
   if (nextTime_ > -86400.0 && ToolKit::verbose()) {
      cout << nCellsParsed_
		   << " OD cells (type " << type_ << ") parsed at "
		   << theSimulationClock->convertTime(nextTime_)
		   << "." << endl;
   }

   nCellsParsed_ = 0;
   nextTime_ = s;
   type_ = t;
   scale_ = f;
   return 0;
}


// Read OD table and return next updating time.

void
OD_Table::insert(OD_Cell* cell)
{
   od_list_link_type matched = cells_.eq_find(cell);
   if (matched != cells_.end()) {
      cells_.remove(matched);
      delete matched;	
   }
   if (cell->rate() > RATE_EPSILON) {
      cells_.insert(cell);
   }
}


// Emit vehicles until no more vehicle wants to departure at this
// time.  When vehicles are created, the corresponding OD cell is
// dropped down in the list based on the departure time for next
// vehicle.

void
OD_Table::emitVehicles()
{
  od_list_link_type i;
  OD_Cell *c;
  while ((i = cells_.head()) && (c = i->data()) &&
		 c->nextTime() <= theSimulationClock->currentTime()) {
	cells_.remove(i);
	c->emitVehicles();
	cells_.insert(i);
  }
}
