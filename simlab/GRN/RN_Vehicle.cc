//-*-c++-*------------------------------------------------------------
// FILE: RN_Vehicle.C
// AUTH: Qi Yang
// DATE: Thu Oct 19 14:11:16 1995
//--------------------------------------------------------------------

#include <Tools/Math.h>
#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>
#include <Tools/SimulationEngine.h>
#include <IO/Exception.h>

#include "RN_Vehicle.h"
#include "RN_Node.h"
#include "RN_Segment.h"
#include "RN_Lane.h"
#include "RN_Path.h"
#include "RN_PathTable.h"
#include "RN_PathPointer.h"
#include "RN_DynamicRoute.h"
#include "RoadNetwork.h"
#include "VehicleTable.h"

//Dan - including bus assignment table header file
#include "BusAssignmentTable.h"
#include "RN_BusRunTable.h"

#include "OD_Pair.h"
#include "OD_Cell.h"
using namespace std;

int RN_Vehicle::lastId_ = 0;
ofstream RN_Vehicle::osDepRecords_;
int RN_Vehicle::nVehicles_ = 0;

RN_Vehicle::RN_Vehicle()
    : CodedObject(), attrs_(0), type_(0),
      routeID_(0), length_(DEFAULT_VEHICLE_LENGTH)
{
}


RN_Node* RN_Vehicle::desNode()
{
    return od_->desNode();
}

RN_Node* RN_Vehicle::oriNode()
{
    return od_->oriNode();
}

int
RN_Vehicle::init(int id, int t, OD_Pair *od, RN_Path *p)
{
    if (id) {					// id is specified
        code_ = (id > 0) ? -id : id;
    } else {						// not specified, assign a serial number
        code_ = (++ lastId_);
    }
    type_ = t;
    od_ = od;
    direction_ = 0;

    info_ = INT_INF;
    path_ = p;
    pathIndex_ = -1;
    nextLink_ = NULL;

    departTime_	= theSimulationClock->currentTime();
    timeEntersLink_ = departTime_;

    oriNode()->nOriCounts_ ++;
    desNode()->nDesCounts_ ++;

    initialize();				// virtual function

    return 1;
}

// Dan - initialization of buses
int
RN_Vehicle::initBus(int bid, OD_Pair *od, RN_Path *p)
{
    if (bid) {					// id is specified
        code_ = (bid > 0) ? -bid : bid;
    } else {						// not specified, assign a serial number
        code_ = (++ lastId_);
    }
    type_ = 0x4;
    od_ = od;

    info_ = 0;
    path_ = p;
    pathIndex_ = -1;
    nextLink_ = NULL;

    departTime_	= theSimulationClock->currentTime();
    timeEntersLink_ = departTime_;

    oriNode()->nOriCounts_ ++;
    desNode()->nDesCounts_ ++;

    initialize();				// virtual function

    return 1;
}

// Dan - initialization of buses for bus rapid transit
int
RN_Vehicle::initRapidBus(int t, OD_Pair *od, int rid, int bt, double hw)
{
    code_ = (++ lastId_);
    type_ = t;
    od_ = od;

    info_ = INT_INF;

    if (rid > 0 && theBusAssignmentTable != NULL) {
        if (!(path_ = theBusRunTable->findPath(rid))) {
            cerr << "Warning:: Unknown bus path <"
                 << rid << ">. ";
            return -1;
        }
    } else {
        path_ = NULL;
    }

    pathIndex_ = -1;
    nextLink_ = NULL;

    departTime_	= theSimulationClock->currentTime();
    timeEntersLink_ = departTime_;

    oriNode()->nOriCounts_ ++;
    desNode()->nDesCounts_ ++;

    theBusAssignmentTable->nBusesParsed_ ++;
    theBusAssignmentTable->addBRTAssignment(code_, bt, rid, hw);

    initialize();				// virtual function

    return 1;
}

// Static. Called once

void RN_Vehicle::openDepartureRecords(const char *name)
{
    const char *filename = ToolKit::outfile(name);
    osDepRecords_.open(filename);
    if (osDepRecords_.good()) {
        cout << "Record vehicle departures in <"
             << filename << ">." << endl;
    } else {
        theException->exit();
    }

    if (!theSimulationEngine->chosenOutput(OUTPUT_SKIP_COMMENT)) {
        if (theSimulationEngine->chosenOutput(OUTPUT_RECT_TEXT)) {
            osDepRecords_
                << "% Time ID OriNode DesNode Type [Path]" << endl;
        } else {
            osDepRecords_
                << "% Time {" << endl
                << "%   { ID OriNode DesNode Type [Path] }" << endl
                << "% }" << endl;
        }
    }
}

void RN_Vehicle::nextBlockOfDepartureRecords()
{
    if (theSimulationEngine->chosenOutput(OUTPUT_RECT_TEXT)) return;

    if (nVehicles_) {			// close previous block
        osDepRecords_ << '}' << endl;
    }

    // open a new block and write time tag

    double t = theSimulationClock->currentTime();
    osDepRecords_
        << endl
        << Fix(t, 0.1)
        << endc << '{' << endl;
}

// Save vehicles so we can load it in next run

void RN_Vehicle::saveDepartureRecord()
{
    if (theSimulationEngine->chosenOutput(OUTPUT_RECT_TEXT)) {
        double t = theSimulationClock->currentTime();
        osDepRecords_ << Fix(t, 0.1);
    }

    osDepRecords_ << endc << code_ << endc;

    if (!theSimulationEngine->chosenOutput(OUTPUT_RECT_TEXT)) {
        osDepRecords_ << '{';
    }

    osDepRecords_ << oriNode()->code() << endc
                  << desNode()->code() << endc
                  << (type_ + 1);

    if (path_) {
        osDepRecords_ << endc << path_->code(); // optional
    }

    if (!theSimulationEngine->chosenOutput(OUTPUT_RECT_TEXT)) {
        osDepRecords_ << '}';
    }

    osDepRecords_ << endl;

    nVehicles_ ++;
}

// Static. Called when simulation is done

void RN_Vehicle::closeDepartureRecords()
{
    if (!theSimulationEngine->chosenOutput(OUTPUT_RECT_TEXT)) {
        osDepRecords_
            << '}' << endl
            << endl << "<END>" << endl;
    }
    osDepRecords_.close();
}


void RN_Vehicle::print(ostream &os)
{
    static int heading = 1;
    if (heading) {
        os << "%" << indent
           << "ID" << endc
           << "Type" << endc
           << "OriNode" << endc
           << "DesNode" << endc
           << "PathID" << endl;
        heading = 0;
    }
    os << indent << code_ << endc
       << type_ << endc
       << oriNode()->code() << endc
       << desNode()->code() << endc;
    if (path_) os << path_->code(); // optional
    os << endl;
}


// This function is called by vehicle table parser. It should return 0
// if the initialization is successful, -1 if error (it causes program
// to quit) and 1 if warning error.  A none zero return value also
// indicate the caller to delete this vehicle.
      
int
RN_Vehicle::init(int id, int ori, int des, int type_id, int path_id)
{
    RN_Node *o = theNetwork->findNode(ori);
    RN_Node *d = theNetwork->findNode(des);

    if (!o) {
        cerr << "Error:: Unknown origin node <" << ori << ">. ";
        return (-1);
    } else if (!d) {
        cerr << "Error:: Unknown destination node <" << des << ">. ";
        return (-1);
    }

    OD_Pair odpair(o, d); 
    PtrOD_Pair odptr(&odpair);
    OdPairSetType::iterator i = theOdPairs.find(odptr);
    if (i == theOdPairs.end()) {
        i = theOdPairs.insert(i, new OD_Pair(odpair));
    }
    od_ = (*i).p();

    od_->oriNode()->type_ |= NODE_TYPE_ORI;
    od_->desNode()->type_ |= NODE_TYPE_DES;

    if (path_id > 0 && thePathTable != NULL) {
        if (!(path_ = thePathTable->findPath(path_id))) {
            cerr << "Warning:: Unknown path <"
                 << path_id << ">. ";
            return -1;
        }
    } else {
        path_ = NULL;
    }

    theVehicleTable->nVehiclesParsed_ ++;

    // tomer - to allow vehicle table trips to be assigned with a path

    int error = init(id, type_id, od_, path_);

    PretripChoosePath();	

    return (error);
}

// Dan - initialization of buses
int
RN_Vehicle::initBus(int bid, int ori, int des, int path_id)
{
    RN_Node *o = theNetwork->findNode(ori);
    RN_Node *d = theNetwork->findNode(des);

    if (!o) {
        cerr << "Error:: Unknown origin node <" << ori << ">. ";
        return (-1);
    } else if (!d) {
        cerr << "Error:: Unknown destination node <" << des << ">. ";
        return (-1);
    }

    OD_Pair odpair(o, d); 
    PtrOD_Pair odptr(&odpair);
    OdPairSetType::iterator i = theOdPairs.find(odptr);
    if (i == theOdPairs.end()) {
        i = theOdPairs.insert(i, new OD_Pair(odpair));
    }
    od_ = (*i).p();

    od_->oriNode()->type_ |= NODE_TYPE_ORI;
    od_->desNode()->type_ |= NODE_TYPE_DES;

    if (path_id > 0 && theBusAssignmentTable != NULL) {
        if (!(path_ = theBusRunTable->findPath(path_id))) {
            cerr << "Warning:: Unknown bus path <"
                 << path_id << ">. ";
            return -1;
        }
    } else {
        path_ = NULL;
    }

    theBusAssignmentTable->nBusesParsed_ ++;

    // tomer - to allow vehicle table trips to be assigned with a path

    int error = initBus(bid, od_, path_);

    PretripChoosePath();	

    return (error);
}

RN_Route*
RN_Vehicle::routingInfo()
{
    if (isGuided()) {

        if (theGuidedRoute->preTripGuidance()) {

            // preTripGuidance == 1 means that the Time Table stored in
            // theGuidedRoute is *always* used by guided vehicle throughout
            // the simulation
	   
            return theGuidedRoute;

        } else {

            // preTripGuidance != 1 means that the Time Table stored in
            // theGuidedRoute is used by guided vehicles only after they
            // have accessed some information

            return attr(ATTR_ACCESSED_INFO) ?
                theGuidedRoute : theUnGuidedRoute;
        }
    } else {
        return theUnGuidedRoute;
    }
}


// Find the nextLink to travel

void RN_Vehicle::OnRouteChoosePath(RN_Node* node)
{
  if (path_) {                // path assigned
    if (isType(VEHICLE_FIXEDPATH) || // path is fixed
	!enRoute()) {		// no enroute
      advancePathIndex();
    } else {                // decide whether to switch
      cout <<"RN_Vehicle.cc: this vehicle is getting a routeSwitchingModel run\n";
      node->routeSwitchingModel(this);
    }
  } else {                    // no path assigned
    // generate route dynamically
    node->routeGenerationModel(this);
  }
}

void RN_Vehicle::PretripChoosePath(OD_Cell* od)
{
    if (od->splits()) {
        // At origin and path and splits are specified
        RN_Path *p = od->chooseRoute(this);
        setPath(p, 0);
    } else if (od->nPaths()) { // At origin and path specified
        od->oriNode()->routeSwitchingModel(this, od);
    } else {   // At origin and no path specified for the OD pair
        od->oriNode()->routeGenerationModel(this);
    }
}

void RN_Vehicle::PretripChoosePath()
{
  RN_Path* p = path_;
  if (p) {    // vehicle has a path specified in the vehicle table file
              // Dan: or a path specified in the bus assignment file
        setPath(p , 0);
    } 
  else {          // vehicle has no path specified
        od_->oriNode()->routeGenerationModel(this);
    }
}


void
RN_Vehicle::advancePathIndex()
{
    int i = pathIndex_ + 1;
    RN_Link *pl = path_->link(i);

    if (!link() ||
        theNetwork->isNeighbor(link(), pl)) {
        setPathIndex(i);
    } else if (pl != nextLink_) {
        donePathIndex();
    }
}

void
RN_Vehicle::retrievePathIndex()
{
    int i = pathIndex_ - 1;
    RN_Link *pl = path_->link(i);
    if (!link() ||
        theNetwork->isNeighbor(pl, link())) {
        setPathIndex(i);
    }
}

RN_Link*
RN_Vehicle::prevLinkOnPath()
{
    int i = pathIndex_ - 2;
    return (path_->link(i));
}


void
RN_Vehicle::setPath(RN_Path *p, short int i)
{
    path_ = p;
    setPathIndex(i);
}


// CAUTION: i has double meaning, depending on whether path is
// defined.

void
RN_Vehicle::setPathIndex(short int i)
{
    pathIndex_ = i;
    if (path_) {			// has a path
        // i is index in path
        if (i >= 0 && i < path_->nLinks())
            nextLink_ = path_->link(i);
        else
            nextLink_ = NULL;
    } else {			// no path
        // i is link index
        if (i >= 0 && i < theNetwork->nLinks())
            nextLink_ = theNetwork->link(i);
        else
            nextLink_ = NULL;
    }
}


void
RN_Vehicle::donePathIndex()
{
    pathIndex_ = -1;
    nextLink_ = NULL;
}


float
RN_Vehicle::timeSinceDeparture()
{
    return theSimulationClock->currentTime() - departTime_;
}

// Returns the time spent in this link

float
RN_Vehicle::timeInLink()
{
    return (theSimulationClock->currentTime() - timeEntersLink_);
}


// Returns the average speed in this link

float
RN_Vehicle::speedInLink()
{
    const float WORSE_THAN_WALKING = 1.38889;
    float t = timeInLink();
    float v;
    if (t < 0.1) {
        v = currentSpeed_;
    } else {
        v = (link()->length() - distanceFromDownNode()) / t;
    }
   
    return Max(v, WORSE_THAN_WALKING);
}


/*
 *-------------------------------------------------------------------
 * Returns the distance from downstream node of current link.
 *-------------------------------------------------------------------
 */

float
RN_Vehicle::distanceFromDownNode()
{
    return (segment()->distance() + distance_);
}

// Return 1 if the link is in path, 0 if not, and -1 if unknown.

int
RN_Vehicle::isLinkInPath(RN_Link *plink, int depth)
{
    if (path_) {
        int n = path_->nLinks();
        if (pathIndex_ + depth < n) n = pathIndex_ + depth;
     
        for (register int i = pathIndex_; i < n; i ++) {
            if (path_->link(i) == plink) {
                return 1;
            }
        }
        return 0;
    } else {
        if (nextLink_ == plink) return 1;
        else return -1;
    }
}

void
RN_Vehicle::writePathRecord(ofstream &os)
{
    const float epsilon = 0.1;

    os << code_ << '\t'
       << oriNode()->code() << '\t'
       << desNode()->code() << '\t'
       << link()->code() << '\t'
       << Fix(timeEntersLink_, epsilon) << '\t'
       << Fix(timeInLink(), epsilon) << '\t'
       << link()->dnNode()->code() <<	endl; 
    // output modified for Tomer's path generation program
}


// Dump current data

void
RN_Vehicle::dumpState(ofstream &os)
{
    os << indent << code_ << endc
       << type_ << endc
       << oriNode()->index() << endc
       << desNode()->index() << endc
       << Fix(departTime_, (float)0.1) << endc
       << Fix(mileage_, (float)0.1) << endc;
       
    if (path_) os << path_->index() << endc;
    else os << -1 << endc;

    os << pathIndex_ << endc;

    os << Fix(timeEntersLink_, (float)0.1) << endc;

    if (lane()) {				// position specified
        os << lane()->index() << endc
           << Fix(distance_, (float)0.1);
    } else {						// yet to enter the network
        os << -1 << endc << 0;
    }
    os << endl;
}

