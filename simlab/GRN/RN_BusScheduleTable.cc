//-*-c++-*------------------------------------------------------------
// NAME: A parser for bus schedule tables
// AUTH: Qi Yang & Daniel Morgan
// FILE: BusScheduleTable.C
// DATE: Fri Nov 30 16:59:15 2001
//--------------------------------------------------------------------

#include <algorithm>

#include <Tools/ToolKit.h>
#include "RN_Link.h"
#include "RN_BusScheduleTable.h"
#include "RN_BusSchedule.h"
#include "OD_Cell.h"
#include "RN_LinkTimes.h"
#include "RN_BusRouteParser.h"
#include "BusAssignment.h"
using namespace std;

// theBusRouteTable will be initialized in the ParseBusScheduleTables() func in
// TS_Setup.C

RN_BusScheduleTable * theBusScheduleTable = NULL;

char * RN_BusScheduleTable::name_ = NULL;

RN_BusScheduleTable::~RN_BusScheduleTable()
{
   if (name_) {
	  delete [] name_;
	  name_ = NULL;
   }
}

// This may be overloaded by derived class
 
RN_BusSchedule*
RN_BusScheduleTable::newBusSchedule()
{
   return new RN_BusSchedule;
}


int
RN_BusScheduleTable::addBusSchedule(int code)
{
   RN_BusSchedule *bs = newBusSchedule();
   busschedules_.push_back(bs);
   return bs->init(code);
}

RN_BusSchedule* RN_BusScheduleTable::findBusSchedule(int c)
{
   vector<RN_BusSchedule*>::iterator i;
   i = find_if(busschedules_.begin(), busschedules_.end(), CodeEqual(c));
   if (i != busschedules_.end()) return *i;
   else return NULL;
}

void
RN_BusScheduleTable::print(ostream &os)
{
   register int i;
   os << "// Bus schedule table generated from file <" << name_ << "> on "
      << TimeTag() << " by "
      << UserName() << endl << endl;
   os << nBusSchedules() << indent << "// Number of bus schedules" << endl;
   os << "{" << endl;
   for (i = 0; i < nBusSchedules(); i ++) {
      busschedule(i)->print(os);
   }
   os << "}" << endl;
}

/*
void
RN_BusScheduleTable::setBusSchedulePointers()
{
   for (register int i = 0; i < nBusSchedules(); i ++) {
      busschedule(i)->setBusSchedulePointers();
   }
}
*/
