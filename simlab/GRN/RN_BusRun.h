//-*-c++-*------------------------------------------------------------
// FILE: RN_BusRun.h
// DATE: Thu Oct 19 17:29:19 1995
//--------------------------------------------------------------------

#ifndef RN_BUSRUN_HEADER
#define RN_BUSRUN_HEADER

#include <iosfwd>
#include <vector>

#include <Templates/Object.h>

class RoadNetwork;
class RN_Link;
class RN_Node;
class RN_BusRunParser;
class RN_BusRunTable;
class RN_BusSchedule;
class BusAssignment;
class RN_LinkTimes;
class RN_BusStop;

class RN_BusRun : public CodedObject
{
      friend class RN_BusRunParser;
      friend class RN_BusRunTable;
      friend class RoadNetwork;
      friend class RN_BusSchedule;
      friend class BusAssignment;

   protected:

      static int sorted_;	// 1=sorted 0=arbitrary order

      int index_;	// index in the array
      int oricode_;     // id of origin node
      int descode_;     // id of destination node
      std::vector<RN_BusSchedule*> trips_;	// list of links of the bus route

   public:
	
      RN_BusRun() : CodedObject() { }
      ~RN_BusRun() { }

      static inline int sorted() { return sorted_; }

      int nTrips() { return trips_.size(); }
      RN_BusSchedule* trip(int i) {
		 if (i < 0) return NULL;
		 else if (i >= nTrips()) return NULL;
		 else return trips_[i];
	  }
      RN_BusSchedule* nextTrip(int i);
      RN_BusSchedule* firstTrip() {
		if (nTrips() > 0) return trips_[0];
		else return NULL;
	  }
      RN_BusSchedule* lastTrip() {
		if (nTrips()) return trips_[nTrips() - 1];
		else return NULL;
	  }

      RN_BusSchedule* findTrip(int c);

      int index() { return index_; }

      int oricode() { return oricode_; }
      int descode() { return descode_; }

      // These two are called by RN_BusRunParser

      virtual int init(int code);
      virtual int addTrip(int c);

      virtual void print(std::ostream &os);

      void setBusRunPointers();

};

#endif
