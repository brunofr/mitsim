//-*-c++-*------------------------------------------------------------
// NAME: Drawable Road Network Node
// AUTH: Qi Yang
// FILE: RN_DrawableNode.C
// DATE: Sat Feb  3 17:45:53 1996
//--------------------------------------------------------------------


#include "RN_DrawableNode.h"
#include "RN_Link.h"
#include "RN_DrawableSegment.h"
using namespace std;


void RN_DrawableNode::calcGeometricData()
{
   RN_DrawableSegment *ps;
   double x = 0, y = 0;
   register int i;

   // End point of upstream links

   for (i = 0; i < nUpLinks(); i ++) {
      ps = (RN_DrawableSegment *)upLink(i)->endSegment();
      loc_ = ps->centerPoint(0.0);	// end point
      x += loc_.x();
      y += loc_.y();
   }
  
   // Start point of downstream links

   for (i = 0; i < nDnLinks(); i ++) {
      ps = (RN_DrawableSegment *)dnLink(i)->startSegment();
      loc_ = ps->centerPoint(1.0);	// start point
      x += loc_.x();
      y += loc_.y();
   }
  
   int num = nUpLinks() + nDnLinks();

   if (!num) {
      cerr << "Error:: No link connected to node <"
		   << code_ << ">." << endl; 
	  loc_.set(0.0, 0.0);
   } else {
	  loc_.set(x / num, y / num);
   }
}
