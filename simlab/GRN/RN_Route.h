//-*-c++-*------------------------------------------------------------
// NAME: Route choice
// NOTE: Stores time invariant routing information
// AUTH: Qi Yang
// FILE: RN_Route.h
// DATE: Tue Sep 24 09:26:54 1996
//--------------------------------------------------------------------

#ifndef RN_ROUTE_HEADER
#define RN_ROUTE_HEADER

#include <iostream>
#include <Templates/DiskData.h>
#include <Templates/Graph.h>
#include <IO/MessageTags.h>

#include "RN_LinkTimes.h"

class RN_Node;
class RN_Link;
class RN_LinkGraph;

class RN_Route : public RN_LinkTimes
{
   public:

      RN_Route(const char *filename);
      RN_Route(const RN_Route& sp);
      RN_Route& operator=(const RN_Route &r);

      void initialize(char tag);
      virtual ~RN_Route() { }

      virtual int pathPeriods() { return 1; }
      unsigned int matrixSize() { return matrixSize_; }

      // The last two parameters are used only in the derived class
      // where the time dependent path table is calculated

      virtual void updatePathTable(
		 char *altname = NULL, float alpha = 1.0,
		 double start = 0, double stop = 0);

      // Travel time from upstream end of olink to dnode

      float upRouteTime(RN_Link* olink, RN_Node* dnode, double timesec = 0.0);
      virtual float upRouteTime(int olink, int dnode, double timesec = 0.0);

      // Travel time from dnstream end of olink to dnode

      float dnRouteTime(RN_Link* olink, RN_Node* dnode, double timesec = 0.0);
      virtual float dnRouteTime(int olink, int dnode, double timesec = 0.0);

      void printShortestRouteTimes(std::ostream &os = std::cout, int p = 0);

   protected:

	  int state_;				// 0=new 1=old

      unsigned int matrixSize_;

      DiskData<float> routeTimes_; // times from each link to des nodes
      static RN_LinkGraph *linkGraph_;
      static Graph<float, RN_LinkTimes> *graph();
      
      virtual void findShortestPathTrees(double start, double stop);
      void recordShortestPathTree(int root, int p = 0);
};

extern int theSpFlag;
extern RN_Route* theGuidedRoute;
extern RN_Route* theUnGuidedRoute;

inline int isSpFlag(int flag = 0xFFFF) {
   return (flag & theSpFlag);
}

#endif
