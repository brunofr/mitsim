//-*-c++-*------------------------------------------------------------
// FILE: OD_Cell.C
// AUTH: Qi Yang
// DATE: Thu Oct 19 12:21:52 1995
//--------------------------------------------------------------------


#include "OD_Cell.h"

#include <Tools/Math.h>

#include <Tools/ToolKit.h>
#include <Tools/Random.h>
#include <Tools/SimulationClock.h>
#include <Tools/SimulationEngine.h>

#include "RoadNetwork.h"
#include "Parameter.h"
#include "OD_Table.h"
#include "OD_Cell.h"
#include "RN_PathTable.h"
#include "RN_Path.h"
#include "BusAssignmentTable.h"
#include "BusAssignment.h"
#include "RN_Node.h"
#include "RN_Vehicle.h"
#include "RN_Route.h"
using namespace std;

OD_Cell* OD_Cell::workingCell_ = NULL;
int OD_Cell::nSplits_ = 0 ;

bool OD_Cell::eq(OD_Cell *c)
{
  return (c->type_ == type_ &&
		  c->oriNode() == oriNode() &&
		  c->desNode() == desNode());
}

int OD_Cell::cmp(OD_Cell *c)
{
  const double epsilon = 1.0e-5;
  if (nextTime_ + epsilon < c->nextTime_) return -1;
  else if (nextTime_ > c->nextTime_ + epsilon) return 1;
  else return 0;
}

int OD_Cell::cmp(int c)
{
  int code = (oriNode()->code() << 16) | (desNode()->code());
  if (code < c) return -1;
  else if (code > c) return 1;
  else return 0;
}

// This function is called by OD_Parser. Return 0 if sucess or -1 if
// fail

int
OD_Cell::init(int ori, int des, double rate, double var, float r)
{
   workingCell_ = this;

   if (splits_) delete [] splits_ ;
   splits_ = 0 ;
   nSplits_ = 0 ;

   int error = 0;

   RN_Node *o = theNetwork->findNode(ori);
   RN_Node *d = theNetwork->findNode(des);

   if (!o) {
      cerr << "Error:: Unknown origin node <" << ori << ">. ";
      return (-1);
   } else if (!d) {
     cerr << "Error:: Unknown destination node <" << des << ">. ";
     return (-1);
   } else if( d->destIndex()==-1 ) {
     cerr << "Error:: Node <" << des
		  << "> is not a destination node in this network.";
     return (-1);
   }

   OD_Pair odpair(o, d); 
   PtrOD_Pair odptr(&odpair);
   OdPairSetType::iterator i = theOdPairs.find(odptr);
   if (i == theOdPairs.end()) {	// not found
	  od_ = new OD_Pair(odpair);
      theOdPairs.insert(od_);
   } else {						// found
	  od_ = (*i).p();
   }

   od_->oriNode()->type_ |= NODE_TYPE_ORI;
   od_->desNode()->type_ |= NODE_TYPE_DES;

   // Departure rate, assume a normal distribution

   rate *= theODTable->scale();
   if (var > 1.0E-4) {
	 var *= theODTable->scale();
	 rate = theRandomizers[Random::Departure]->nrandom(rate, var);
   }
   randomness_ = r;

   if (rate >= RATE_EPSILON) {
      headway_ = 3600.0 / rate;
      nextTime_ = theSimulationClock->currentTime() -
               log(theRandomizers[Random::Departure]->urandom()) * headway_;
   } else {
      headway_ = DBL_INF;
      nextTime_ = DBL_INF;
   }

   type_ = theODTable->type();
   theODTable->insert(this);

   if (ToolKit::debug()) {
      print();
   }

   theODTable->nCellsParsed_ ++;

   return error;
}

int
OD_Cell::initBRT(int ori, int des, double rate, double var, float r, int bt, int rid)
{
   workingCell_ = this;

   if (splits_) delete [] splits_ ;
   splits_ = 0 ;
   nSplits_ = 0 ;

   int error = 0;

   RN_Node *o = theNetwork->findNode(ori);
   RN_Node *d = theNetwork->findNode(des);

   if (!o) {
      cerr << "Error:: Unknown origin node <" << ori << ">. ";
      return (-1);
   } else if (!d) {
     cerr << "Error:: Unknown destination node <" << des << ">. ";
     return (-1);
   } else if( d->destIndex()==-1 ) {
     cerr << "Error:: Node <" << des
		  << "> is not a destination node in this network.";
     return (-1);
   }

   OD_Pair odpair(o, d); 
   PtrOD_Pair odptr(&odpair);
   OdPairSetType::iterator i = theOdPairs.find(odptr);
   if (i == theOdPairs.end()) {	// not found
	  od_ = new OD_Pair(odpair);
      theOdPairs.insert(od_);
   } else {						// found
	  od_ = (*i).p();
   }

   od_->oriNode()->type_ |= NODE_TYPE_ORI;
   od_->desNode()->type_ |= NODE_TYPE_DES;

   // Departure rate, assume a normal distribution

   rate *= theODTable->scale();
   if (var > 1.0E-4) {
	 var *= theODTable->scale();
	 rate = theRandomizers[Random::Departure]->nrandom(rate, var);
   }
   randomness_ = r;

   busTypeBRT_ = bt;
   runIDBRT_ = rid;

   if (rate >= RATE_EPSILON) {
      headway_ = 3600.0 / rate;
      nextTime_ = theSimulationClock->currentTime() -
		 log(theRandomizers[Random::Departure]->urandom()) * headway_;
   } else {
      headway_ = DBL_INF;
      nextTime_ = DBL_INF;
   }

   type_ = theODTable->type();
   theODTable->insert(this);

   if (ToolKit::debug()) {
      print();
   }

   theODTable->nCellsParsed_ ++;

   return error;
}

int
OD_Cell::addPath(int path_id)
{
   int error = 0;
   if (thePathTable) {
      RN_Path *p = thePathTable->findPath(path_id);
      if (!p) {
		 cerr << "Warning:: Unknown path. ";
		 error = 1;
      } else if (p->oriNode() != oriNode() ||
				 p->desNode() != desNode()) {
		 cerr << "Warning:: Path <" << path_id
			  << " does not connect OD pair ("
			  << oriNode()->code() << ","
			  << desNode()->code() << "). ";
		 error = 2;
      } else {
		 paths_.push_back(p);
      }
   } else {
      cerr << "Warning:: No path table. ";
      error = 3;
   }
   return error;
}

int OD_Cell::setSplit(float split)
{
	int n = nPaths() ;
	if (nSplits_ >= n) {
		return -1 ;				// too many splits
	}
	if (!splits_) {
		splits_ = new float[n] ;
	} else {
		split += splits_[nSplits_ - 1] ;
	}

	if (split < 0.0) return -2 ;
	if (split > 1.0) return -3 ;

	splits_[nSplits_] = split ;
	nSplits_ ++ ;
	
	return 0 ;
}


// Print for debugging

void
OD_Cell::print(ostream &os)
{
   static int header = 1;
   if (header) {
      os << "// Ori Des Rate Randomness Path" << endl;
      header = 0;
   }

   os << indent
      << OPEN_TOKEN << endc
      << oriNode()->code() << endc
      << desNode()->code() << endc
      << rate() << endc
      << (1.0 - randomness_) << endc;

   if (nPaths()) {
      os << OPEN_TOKEN << endc;
      for (register short int i = 0; i < nPaths(); i ++) {
		 os << path(i)->code() << endc;
      }
      os << CLOSE_TOKEN << endc;
   }

   os << CLOSE_TOKEN << endl;
}


// Calculate the inter departure time for the next vehicle

double
OD_Cell::randomizedHeadway()
{
   if (randomness_ < 1.0e-10 ||
       !theRandomizers[Random::Departure]->brandom(randomness_)) {
								// uniform distribution
	 return (headway_);
   } else {						// random distribution
	 return (-log(theRandomizers[Random::Departure]->urandom()) * headway_);
   }
}


// Returns a created vehicle if it is time for a vehicle to depart or
// NULL if no vehicle needs to depart at this time. It also update the
// time that next vehicle departs if a vehicle is created.

RN_Vehicle*
OD_Cell::emitVehicle(void)
{
   if (nextTime_ <= theSimulationClock->currentTime()) {
      RN_Vehicle *pv = newVehicle();

      if (type() & VEHICLE_BUS_RAPID) {
	pv->initRapidBus(type_, od_, runIDBRT_, busTypeBRT_, headway_);
      }
      else pv->init(0, type_, od_, NULL);

      // Find the first link to travel. Variable 'oriNode', 'desNode' and
      // 'type' must have valid values before the route choice model is
      // called. 

      if (type() & VEHICLE_BUS_RAPID) {
	pv->PretripChoosePath();
      }   
      else pv->PretripChoosePath(this);

      if (theSimulationEngine->chosenOutput(OUTPUT_VEHICLE_DEP_RECORDS)) {
	pv->saveDepartureRecord();
      }

      nextTime_ += randomizedHeadway();
      return pv;
   }
   return NULL;
}

// Application program should overload these functions

void
OD_Cell::emitVehicles()
{
   RN_Vehicle *pv;
   while (pv = OD_Cell::emitVehicle()) {
      cout << "Vehicle " << pv->code()
		   << " created for OD pair (" << oriNode()->code()
		   << "," << desNode()->code() << ")." << endl;
      delete pv;
   }
}


// Return a route if path table and splits are specified

RN_Path* OD_Cell::chooseRoute(RN_Vehicle*)
{
   int i = theRandomizers[Random::Routing]->drandom(nPaths(), splits_);
   return path(i) ;
}



// Return a pretrip route if path table is specified

// RN_Path*
// OD_Cell::chooseRoute(RN_Vehicle* pv)
// {
//    int n = nPaths();

//    if (n <= 0) {
// 	  // No path is specified for this OD pair
// 	  return NULL;
//    } else if (n <= 1) {
// 	  // Only one path is specified, no much freedom.
// 	  return path(0);
//    }

//    // Use logit model to calculate the probability to choose each
//    // available path

//    const double epsilon = 1.0 / DBL_INF;
//    double *util = new double [n];
//    float *costs = new float [n];
//    short int i;
//    double cost;
//    double sum = 0.0;
//    float beta;
//    int guided = pv->isGuided();

//    if (guided) {
// 	  beta = theBaseParameter->routingBeta(1);
//    } else {
// 	  beta = theBaseParameter->routingBeta(0);
//    }
//    RN_Route *info = pv->routingInfo();

//    // Find the shorted route

//    float smallest = FLT_INF;
//    for (i = 0; i < n; i ++) {
// 	  cost = path(i)->travelTime(info, 0);
// 	  costs[i] = cost;
// 	  if (cost < smallest) {
// 		 smallest = cost;
// 	  }
//    }

//    // Calculate the utility to choose each route

//    for (i = 0; i < n; i ++) {
//       util[i] = exp(beta * costs[i] / smallest);
//       sum += util[i];  
//    }

//    // Select path based on the probabilities calculated using a logit
//    // model

//    RN_Path * pp;

//    if (sum > epsilon) {

//       // a uniform (0,1] random number

//       double rnd = theRandomizers[Random::Routing]->urandom();
//       double cdf;
//       for (i = n - 1, cdf = util[i] / sum;
// 		   i > 0 && rnd > cdf;
// 		   i --) {
// 		 cdf += util[i-1] / sum;
//       }

//       pp = path(i);
//    } else {
// 	  pp = path(theRandomizers[Random::Routing]->urandom(n));
//    }

//    delete [] util;
//    delete [] costs;

//    return pp;
// }
