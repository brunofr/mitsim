//-*-c++-*------------------------------------------------------------
// NAME: A parser for vehicle table 
// AUTH: Qi Yang
// FILE: VehicleTable.C
// DATE: Tue Mar  5 21:29:28 1996
//--------------------------------------------------------------------

#include <Tools/SimulationClock.h>
#include <Tools/ToolKit.h>
#include <IO/Exception.h>

#include "VehicleTableParser.h"
#include "VehicleTable.h"
#include "RoadNetwork.h"
#include "RN_Vehicle.h"
#include <iostream>
using namespace std;

VehicleTable * theVehicleTable = NULL;

char * VehicleTable::name_ = NULL;

VehicleTable::VehicleTable()
   : parser_(NULL), nextTime_(-DBL_INF),
     nVehiclesParsed_(0)
{
   theVehicleTable = this;
}


// Open trip table file and create a OD Parser

void VehicleTable::open(double start_time)
{
   const char *fn = ToolKit::optionalInputFile(name_);
   if (!fn) theException->exit(1);
   Copy(&name_, fn) ;
   startTime_ = start_time;

   // Parser will be created in read()

   if (parser_) delete parser_ ;
   parser_ = NULL ;
}

// Read trip tables for next time period

double VehicleTable::read()
{
  const char *fn ;
  if (parser_) {				// continue parsing of current file
	fn = NULL ;
  } else {						// open the  new file
	parser_ = new VehicleTableParser(this);
	fn = name_;
  }
  while (nextTime_ <= theSimulationClock->currentTime()) {
	parser_->parse(fn);
  }
  return nextTime_;
}


// Called by parser to setup the od matrix and update time for next
// vehicles departure.

int VehicleTable::init(double s)
{
   if (nextTime_ > -86400.0 && ToolKit::debug()) {
      cout << nVehiclesParsed_
		   << " Vehicles parsed at "
		   << theSimulationClock->convertTime(nextTime_)
		   << "." << endl;
   }

   nVehiclesParsed_ = 0;
   nextTime_ = s;
   return 0;
}
