//-*-c++-*------------------------------------------------------------
// NAME: A parser for vehicle table 
// AUTH: Qi Yang
// FILE: VehicleTable.h
// DATE: Tue Mar  5 21:24:06 1996
//--------------------------------------------------------------------

#ifndef VEHICLETABLE_HEADER
#define VEHICLETABLE_HEADER

//#include <iostream>

#include "RN_Vehicle.h"

class VehicleTableParser;
class VehicleTableBisonParser;
class RN_Vehicle;

class VehicleTable
{
      friend class VehicleTableBisonParser;
      friend class VehicleTableParser;
      friend class RN_Vehicle;

   protected:

      static char * name_;		// file name
	  double startTime_;		// dep time smaller than this is skipped
      double nextTime_;			// time to read od pairs

   public:

      VehicleTable();

      virtual ~VehicleTable() { }
	
      void open(double start_time = -86400.0);
      double read();

      static inline char* name() { return name_; }
      static inline char** nameptr() { return &name_; }

      inline double startTime() { return startTime_; }
	  inline double nextTime() { return nextTime_; }

	  inline int skip() {
		return (nextTime_ < startTime_);
	  }

      virtual int init(double start);

      // These may has to be overloaded by derived class

      virtual RN_Vehicle* newVehicle() = 0;

   private:

      VehicleTableParser* parser_; // the parser
      int nVehiclesParsed_;	// vehicles parsed in this round
};

extern VehicleTable * theVehicleTable;

#endif
