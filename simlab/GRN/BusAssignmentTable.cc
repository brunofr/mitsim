//-*-c++-*------------------------------------------------------------
// NAME: Bus assignment table 
// AUTH: Qi Yang & Daniel Morgan
// FILE: BusAssignmentTable.C
// DATE: Fri Nov 30 16:59:15 2001
//--------------------------------------------------------------------

#include <algorithm>

#include <Tools/SimulationClock.h>
#include <Tools/Random.h>
#include <Tools/ToolKit.h>
#include <IO/Exception.h>

#include "BusAssignmentParser.h"
#include "BusAssignmentTable.h"
#include "RoadNetwork.h"
#include "RN_Vehicle.h"
#include "RN_Link.h"
#include "OD_Cell.h"
#include "RN_LinkTimes.h"

// theBusAssignmentTable will be initialized in the ParseBusAssignments() func in
// TS_Setup.C or MesoTS_Setup.C

BusAssignmentTable * theBusAssignmentTable = NULL;

char * BusAssignmentTable::name_ = NULL;

BusAssignmentTable::BusAssignmentTable()
   : parser_(NULL), nextTime_(-DBL_INF),
     nBusesParsed_(0)
{
   theBusAssignmentTable = this;
   BRTOnly_ = 1;
}

// Open bus assignment table file and create a OD Parser

void BusAssignmentTable::open(double start_time)
{
   const char *fn = ToolKit::optionalInputFile(name_);
   if (!fn) theException->exit(1);
   Copy(&name_, fn) ;
   startTime_ = start_time;

   // Parser will be created in read()

   if (parser_) delete parser_ ;
   parser_ = NULL ;
}

// Read bus assignment tables for next time period

double BusAssignmentTable::read()
{
  const char *fn ;
  if (parser_) {				// continue parsing of current file
	fn = NULL ;
  } else {						// open the  new file
	parser_ = new BusAssignmentParser(this);
	fn = name_;
  }
  while (nextTime_ <= theSimulationClock->currentTime()) {
	parser_->parse(fn);
  }

  return nextTime_;
}


// Called by parser to setup the od matrix and update time for next
// bus' departure.

int BusAssignmentTable::initBus(double s)
{
   if (nextTime_ > -86400.0 && ToolKit::debug()) {
      cout << nBusesParsed_
		   << " Buses parsed at "
		   << theSimulationClock->convertTime(nextTime_)
		   << "." << endl;
   }

   nBusesParsed_ = 0;
   nextTime_ = s;
   return 0;
}

// This may be overloaded by derived class
 
BusAssignment*
BusAssignmentTable::newBusAssignment()
{
   return new BusAssignment;
}

int
BusAssignmentTable::addBusAssignment(int code, int type, int run)
{
   BusAssignment *ba = newBusAssignment();
   assignments_.push_back(ba);
   return ba->init(code, type, run);
}

int
BusAssignmentTable::addBRTAssignment(int code, int type, int run, double hw)
{
   BusAssignment *ba = newBusAssignment();
   assignments_.push_back(ba);
   return ba->initBRT(code, type, run, hw);
}

int
BusAssignmentTable::addBusAssignment(int code, int type, int run, int load)
{
   BusAssignment *ba = newBusAssignment();
   assignments_.push_back(ba);
   return ba->init(code, type, run, load);
}

BusAssignment* BusAssignmentTable::findBusAssignment(int c)
{
   vector<BusAssignment*>::iterator i;
   i = find_if(assignments_.begin(), assignments_.end(), CodeEqual(c));
   if (i != assignments_.end()) return *i;
   else return NULL;
}

void
BusAssignmentTable::print(ostream &os)
{
   register int i;
   os << "// Bus assignment table generated from file <" << name_ << "> on "
      << TimeTag() << " by "
      << UserName() << endl << endl;
   os << nAssignments() << indent << "// Number of bus assignments" << endl;
   os << "{" << endl;
   for (i = 0; i < nAssignments(); i ++) {
      assignment(i)->print(os);
   }
   os << "}" << endl;
}

