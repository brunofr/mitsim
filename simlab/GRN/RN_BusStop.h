//-*-c++-*------------------------------------------------------------
// RN_BusStop.h, based on RN_TollBooth.h
//
// margaret
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institute of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef RN_BUSSTOP_HEADER
#define RN_BUSSTOP_HEADER

//#include <iostream>
#include <iosfwd>
//#include <fstream>
#include <vector>

#include "RN_Signal.h"

class RN_BusStopPrmTable;

class RN_BusStop : public virtual RN_Signal
{
      friend class RN_BusStopPrmTable;

   protected:

      // Bus Stops are treated as signals and stored after the
      // standard traffic signals

      static int startIndexAsSignals_;

      unsigned int rules_;	// lane use and change rules
      unsigned int stopLength_;	// bus stop length (in original unit)
      int waysideDummy_;        // Dan: 0 if stop is in general traffic lane, 1 if removed in a bay
      int squeezeDummy_;        // Dan: 1 if stop is on one-lane road, causing vehicles to squeeze around a bus
      int nRoutesServeStop_;    // number of routes that serve the stop
      float minDwell_;          // lower bound on dwell time
      float maxDwell_;          // upper bound on dwell time

  /* Dan: The following vectors contain one element for each bus route serving the
     bus stop, and contain the values corresponding to the LAST bus to arrive at
     the stop.  The index position in each vector correspond to the same route.
  */
      std::vector<int> lastbusid_;            // bus id of last bus
      std::vector<int> routeid_;              // route id of last bus
      std::vector<double> lastarrivaltime_;   // actual arrival time of last bus
      std::vector<double> lastschedarrival_;  // scheduled arrival time of last bus
      std::vector<double> lastheadway_;       // headway between last bus and the one before it
      std::vector<float> lastbusdwell_;       // dwell time for last bus
      std::vector<int> passarrive_;           // the number of passengers left by last full bus
      std::vector<int> passalight_;           // the number of passengers left by last full bus
      std::vector<int> passengersleft_;       // the number of passengers left by last full bus
      std::vector<int> numbusarrivals_;       // the number of buses that have arrived at stop in simulation
      std::vector<int> lastbuscycle_;         // the schedule cycle in which the last bus arrived
      std::vector<int> lastbusinterval_;      // the interval within the cycle in which the last bus arrived
      std::vector<float> lastbustime_;       // the time elapsed in the last bus' arrival interval
      std::vector<float> lastwaittime_;       // the wait time for passengers boarding the last bus

  /* Dan: The indeces in the following vectors also correspond to the route serving
     the stop in the routeid vector above.  They are assigned values if route-
     dependent arrival rates and alighting percentages are given in the dwell time 
     parameter input file
  */
      std::vector<float> arrivalrate_;  // average passenger arrival rate for corresponding route
      std::vector<float> alightpct_;    // % passengers on the bus alighting at the stop
                                   // for the corresponding route
      std::vector<float> starttime_;    // start time for bus "schedule clock"
      std::vector<float> intervala_;    // time interval over which arrival rate is zero
      std::vector<float> intervalb_;    // time interval over which arrival rate is a triangle
      std::vector<float> intervalc_;    // time interval over which arrival rate is zero
      std::vector<float> peak_;         // the peak passenger arrival rate (top of triangle)
      std::vector<int> control_;        // is equal to 1 if operations control enabled at stop for this route

   public:

      RN_BusStop()
	: RN_Signal(), squeezeDummy_(0), stopLength_(60), rules_(0)
        , nRoutesServeStop_(0), minDwell_(15), maxDwell_(45), waysideDummy_(0) { }
      virtual ~RN_BusStop() { }

      virtual unsigned int rules() { return rules_; }
      virtual unsigned int stopLength() { return stopLength_; }

      virtual int isWayside() { return waysideDummy_; }
      virtual int requiresSqueeze() { return squeezeDummy_; }
      virtual int enableControl(int rc);
      virtual bool isControlEnabled(int rc);

      virtual void addBusStopIntoNetwork();
    
      virtual int initBusStopChars(int code, int state, int lane,
		       unsigned int rule, unsigned int length,
		       int sd, int wsd);
      virtual void print(std::ostream &os);

  /* Dan: addRouteToBusStop is called by the RN_BusRouteParser when it reads a
     bus stop id.  It stores the route id in the routeid_ vector and
     creates an element in the other vectors for storing the busid, arrival
     time and delay data as buses arrive at the stop.
  */

      void addRouteToBusStop(int code); 
      int addRateParameters (int rcode, float ar, float al);
      int addArrivalParameters (int rcode, float st, float a, float b, 
                                float c, float peak, float alight);

      int nRoutesServeStop() { return nRoutesServeStop_; }

};

#endif
