//
// RN_Parser.y -- Network database parser generator
// by Qi Yang
//

%name   RN_Parser
%define ERROR_BODY	{ _scanner.check(-1, "Parser error") ; }
%define LEX_BODY	  { return _scanner.yylex() ; }

// These macros are expanded when creating the definition for named
// BisonParser class.  The functions defined as member here are
// designed for use in the grammar rules.

%define CONSTRUCTOR_PARAM RoadNetwork* pContainer_
%define CONSTRUCTOR_INIT  : _pContainer(pContainer_)
%define MEMBERS                                               \
    RoadNetwork* _pContainer ;                                \
    RN_IS* ISptr;                                              \
    CManuver* mnvptr;                                          \
    private:                                                  \
    RN_Scanner _scanner ;                                     \
    public:                                                   \
    RN_Scanner& scanner() { return _scanner ; }               \
    int lineno() { return _scanner.lineno() ; }               \
    void check(int err_no, const char* szMsg_ = 0) {          \
      _scanner.check(err_no, szMsg_) ;                        \
    }                                                         \
    const char* text() { return _scanner.text() ; }           \
    const char* value(const char* delis_ = 0) {               \
      return _scanner.value(delis_) ;                         \
    }                                                         \
    const char* filename() { return _scanner.filename() ; }   \
    void parse(const char* szFilename_) {                     \
      if (szFilename_ && _scanner.open(szFilename_)) {        \
        yyparse() ;                                           \
      } else if (_scanner.good()) {                           \
        yyparse() ;                                           \
      }                                                       \
    }                                                         \

%union {
  long        type_i ;
  double      type_f ;
  const char* type_s ;
}

%token <type_s> RN_COL
%token <type_s> RN_OCB
%token <type_s> RN_CCB
%token <type_i> RN_INT
%token <type_f> RN_REAL
%token <type_s> RN_TIME
%token <type_s> RN_PAIR
%token <type_s> RN_STRING
%token <type_s> RN_END
%token <type_s> RN_HIPHEN

%token <type_s> RN_TITLE
%token <type_s> RN_DRIVINGDIRECTION
%token <type_s> RN_LABELS
%token <type_s> RN_NODES
%token <type_s> RN_LINKS
%token <type_s> RN_CONNECTORS
%token <type_s> RN_PROHIBITORS
%token <type_s> RN_SENSORS
%token <type_s> RN_SIGNALS
%token <type_s> RN_TOLLS
%token <type_s> RN_STOPS
%token <type_s> RN_SDFNS
%token <type_s> RN_INTERSECTS
%token <type_s> RN_ISNODE


%header{
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>

using namespace std;

#include <IO/Exception.h>
#include <Tools/ToolKit.h>  
#include <UTL/Misc.h>  
#include <GRN/RoadNetwork.h>
#include <GRN/RN_Node.h>
#include <GRN/RN_Link.h>
#include <GRN/RN_Segment.h>
#include <GRN/RN_Lane.h>
#include <GRN/RN_TollBooth.h>
#include <GRN/RN_BusStop.h>
#include <GRN/RN_SdFn.h>
#include <GRN/RN_Label.h>
#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>
#include <GRN/RN_IS.h>

#undef yyFlexLexer
#define yyFlexLexer RNSFlexLexer
#include <FlexLexer.h>

class RN_Scanner : public RNSFlexLexer
{
 public:

  RN_Scanner(istream* yyin_ = 0, ostream* yyout_ = 0)
      : RNSFlexLexer(yyin_, yyout_), _szFilename(NULL), _pis(yyin_) { }

  virtual ~RN_Scanner() {  close() ; }

  int yylex() { return RNSFlexLexer::yylex() ; }

  void check(int err_no, const char* szMsg_ = NULL) ;
  const char* text() ;
  const char* value(const char* delis_ = 0) ;
  const char* filename() { return _szFilename ; }
  bool open(const char* szFilename_) ;
  bool close() ;
  bool good() ;

 protected:

  // Redefine the virtual function in yyFlexLexer

  void LexerError(const char* szMsg_) { check(-1, szMsg_) ; }

 private:

  const char* _szFilename ;
  istream* _pis ;
} ;

%}

//--------------------------------------------------------------------
// Symbols used road network database grammer rules.
//--------------------------------------------------------------------

%type <type_s> szstring
%type <type_i> inum num_objects specified unspecified
%type <type_f> fnum float_num

%type <type_s> description direction network basic_network heading body
%type <type_s> connectors prohibitors sensors controls sdfns

%type <type_s> label_allocator node_allocator link_allocator
%type <type_s> sensor_allocator signal_allocator sdfn_allocator

%type <type_s> labels multi_labels label
%type <type_s> nodes multi_nodes node
%type <type_s> links multi_links link link_data
%type <type_s> multi_segments segment segment_data
%type <type_s> segment_data3 segment_data4 segment_data5
%type <type_s> multi_lanes lane
%type <type_s> geometry arc

%type <type_s> multi_connectors connector
%type <type_s> multi_prohibitors prohibitor

%type <type_s> multi_sensor_stations
%type <type_s> sensor_station sensor_station_data sensor_data
%type <type_s> link_sensor multi_lane_sensors lane_sensor
%type <type_s> link_sensor_typed lane_sensor_typed //IEM(Apr26)

%type <type_s> signals multi_signal_stations
%type <type_s> signal_station signal_station_data signal_data
%type <type_s> link_signal multi_lane_signals lane_signal

%type <type_s> toll_plazas multi_toll_plazas
%type <type_s> toll_plaza toll_plaza_data
%type <type_s> multi_toll_booths toll_booth

%type <type_s> bus_stops multi_bus_stops
%type <type_s> bus_stop bus_stop_data
%type <type_s> multi_bus_stop_chars bus_stop_chars //margaret:  take out later (leave for compilation, 5/21)

%type <type_s> linear_sdfn non_linear_sdfn sdfn multi_sdfns

%type <type_s> IS_allocator
%type <type_s> id characs	   		 
%type <type_s> mymove
%type <type_s> conflict multi_conflict
%type <type_s> fifo multi_fifo
%type <type_s> multi_intersects intersect intersections
%type <type_s> multi_manuvers manuver 


%start everything

%%


//--------------------------------------------------------------------
// Beginniing of road network database grammer rules.
//--------------------------------------------------------------------

everything: heading body
{
  const char* s = Str("Finished parsing network database <%s>",
					  filename());
  _pContainer->updateProgress(s);
}
;

heading: description direction labels
| description direction
| description labels
| description
;

body: network sensors controls sdfns intersections
| network sensors controls sdfns
| network sensors controls intersections
| network sensors sdfns intersections
| network controls sdfns intersections
| network sensors controls
| network sensors sdfns
| network sensors intersections
| network controls sdfns
| network controls intersections
| network sdfns intersections
| network sensors
| network controls
| network sdfns
| network intersections
| network
;

description: RN_TITLE RN_COL szstring
{
  ::Copy(&_pContainer->description_, $3);
  if (ToolKit::debug()) {
	cout << indent << $3 << endl;}
}
;

network: basic_network prohibitors
| basic_network
;

basic_network: nodes links connectors
{
  _pContainer->updateProgress("Calculating network information ...");
  _pContainer->calcStaticInfo();
}
;

direction: RN_DRIVINGDIRECTION RN_COL inum
{ 
  //  ::Copy(_pContainer->drivingDirection_, (int) $3);
  
  _pContainer->drivingDirection_ = (int)$3;
  
  // _pContainer->setDrivingDirection((int)$3); tomer - this should bbe private, but temporarilly public because the function is never called.

 cout << "rama: $3 = " << _pContainer->drivingDirection_ << endl;
}
;

controls: signals toll_plazas bus_stops //margaret:  added bus stop class
| signals
| toll_plazas
| bus_stops
| signals toll_plazas
| toll_plazas bus_stops
| signals bus_stops
;

labels: RN_LABELS label_allocator RN_OCB RN_CCB
| RN_LABELS label_allocator RN_OCB multi_labels RN_CCB
{
  if (ToolKit::debug()) {
	cout << indent << _pContainer->nLabels() << " labels parsed." << endl;
  }
}
;

label_allocator: num_objects
{
  int n = ($1 > 0) ? ($1) : (NUM_LABELS);
  _pContainer->labels_.reserve(n);
}
;

multi_labels: label
| multi_labels label
;

label: RN_OCB inum szstring RN_CCB
{
  // $2=Code $3=Name
  RN_Label *ptr = _pContainer->newLabel();
  int err_no = ptr->init($2, $3);
  check(err_no);
}
;

nodes: RN_NODES node_allocator RN_OCB multi_nodes RN_CCB
{
  if (ToolKit::debug()) {
	cout << indent << _pContainer->nNodes() << " nodes parsed." << endl;
  }
  // _pContainer->nodes_.resize();
}
;

node_allocator: num_objects
{
  int n = ($1 > 0) ? ($1) : (NUM_NODES);
  _pContainer->nodes_.reserve(n);
}
;

multi_nodes: node
| multi_nodes node
;

node: RN_OCB inum inum szstring RN_CCB
{
  // $2=ID, $3=Type $4=Name
  RN_Node *ptr = _pContainer->newNode();
  int err_no = ptr->init($2, $3, $4);
  check(err_no);
}
;

links: RN_LINKS link_allocator RN_OCB multi_links RN_CCB
{
  if (ToolKit::debug()) {
	cout << indent << _pContainer->nLinks() << " links, ";
	cout << _pContainer->nSegments() << " segments, and ";
	cout << _pContainer->nLanes() << " lanes parsed." << endl;
  }
}
;

link_allocator:  num_objects num_objects num_objects
{
  int n;
  n = ($1 > 0) ? ($1) : (NUM_LINKS);
  _pContainer->links_.reserve(n);
  n = ($1 > 0) ? ($1) : (NUM_SEGMENTS);
  _pContainer->segments_.reserve(n);
  n = ($1 > 0) ? ($1) : (NUM_LANES);
  _pContainer->lanes_.reserve(n);
}
;

multi_links: link
| multi_links link
;

link: RN_OCB link_data multi_segments RN_CCB
;

link_data: inum inum inum inum inum
{
  // $1=ID $2=LinkType $3=UpNode $4=DnNode $5=NameIndex
  RN_Link *ptr = _pContainer->newLink();
  int err_no = ptr->init($1, $2, $3, $4, $5);
  check(err_no);
}
;

multi_segments: segment
| multi_segments segment
;

segment: RN_OCB segment_data geometry multi_lanes RN_CCB
;

segment_data: segment_data5 | segment_data4 | segment_data3
{
}
;

segment_data5: inum inum fnum fnum inum
{
  // $1=ID $2=DefaultSpeedLimit $3=FreeSpeed $4=Grade
  // $5=IndexToPerformance Function
  RN_Segment *ptr = _pContainer->newSegment();
  int err_no = ptr->init($1, $2, $3, $4);
  check(err_no);
  ptr->sdIndex($5);
}
;

segment_data4: inum inum fnum fnum
{
  // $1=ID $2=DefaultSpeedLimit $3=FreeSpeed $4=Grade
  RN_Segment *ptr = _pContainer->newSegment();
  int err_no = ptr->init($1, $2, $3, $4);
  check(err_no);
}
;

segment_data3: inum inum fnum
{
  // $1=ID $2=DefaultSpeedLimit $3=FreeSpeed
  RN_Segment *ptr = _pContainer->newSegment();
  int err_no = ptr->init($1, $2, $3, 0.0);
  check(err_no);
}
;
 

geometry: RN_OCB arc RN_CCB
;

arc: fnum fnum fnum fnum fnum
{
  // ($1,$2)=StartPoint $3=Bulge ($4,$5)=EndPoint
  _pContainer->lastSegment()->initArc($1, $2, $3, $4, $5);
}
;

multi_lanes: lane
| multi_lanes lane
;

lane: RN_OCB inum inum RN_CCB
{
  // $2=ID $3=Rules
  RN_Lane *ptr = _pContainer->newLane();
  int err_no = ptr->init($2, $3);
  check(err_no); 

}
;

connectors: RN_CONNECTORS num_objects RN_OCB RN_CCB
| RN_CONNECTORS num_objects RN_OCB multi_connectors RN_CCB
{
  if (ToolKit::debug()) {
	cout << indent << _pContainer->nConnectors_ 
		 << " lane connectors parsed." << endl;
  }
}
;

multi_connectors: connector
| multi_connectors connector
;

connector: RN_OCB inum inum RN_CCB
{
  // $2=UpLane $2=DnLane
  int err_no = _pContainer->addLaneConnector($2, $3);
  check(err_no);
}

prohibitors: RN_PROHIBITORS num_objects RN_OCB RN_CCB
| RN_PROHIBITORS num_objects RN_OCB multi_prohibitors RN_CCB
{
  if (ToolKit::debug()) {
	cout << indent << _pContainer->nProhibitors_
		 << " turn prohibitors parsed." << endl;
  }
}
;

multi_prohibitors: prohibitor
| multi_prohibitors prohibitor
;

prohibitor: RN_OCB inum inum RN_CCB
{
  int err_no = _pContainer->addTurnProhibitor($2, $3);
  check(err_no);
}
;

//--------------------------------------------------------------------
// Grammer rules for surveillance devices
//--------------------------------------------------------------------

sensors: RN_SENSORS sensor_allocator RN_OCB RN_CCB
| RN_SENSORS sensor_allocator RN_OCB multi_sensor_stations RN_CCB
{
  if (ToolKit::debug()) {
	cout << endc << _pContainer->nSensors()
		 << " sensors parsed." << endl;
  }
// _pContainer->sensors_.resize();
  _pContainer->sortSensors(); //IEM(May2) Sort the sensors by type in TS_Network.
			      // NOTE: Depends on master file already being read
}
;

sensor_allocator: num_objects
{
  int n = ($1 > 0) ? ($1) : (NUM_SENSORS);
  _pContainer->sensors_.reserve(n);
}
;

multi_sensor_stations: sensor_station
| multi_sensor_stations sensor_station
;

sensor_station: RN_OCB sensor_station_data sensor_data RN_CCB
;

sensor_station_data: inum inum fnum inum fnum
{
  // $1=Type $2=Task $3=Zone $4=Segment $5=Position
  RN_SurvStation *ptr = _pContainer->newSurvStation();
  int err_no = ptr->init($1, $2, $3, $4, $5);
  check(err_no);
}
;

//IEM(Apr26) added l_s_t
sensor_data: link_sensor
| link_sensor_typed
| multi_lane_sensors
;

link_sensor: RN_OCB inum fnum RN_CCB
{
  // $2=SensorID $3=WorkProb
  RN_Sensor *ptr = _pContainer->newSensor();
  int err_no = ptr->init($2, $3);
  check(err_no);
}
;

//IEM(Apr26) added this definition of l_s_t
// Use of -1 in call of init because idiotic C++ won't let me use a default
// argument if it's not rightmost.
link_sensor_typed: RN_OCB inum fnum RN_COL inum RN_CCB
{
  // $2=SensorID $3=WorkProb $5=intervalType
  RN_Sensor *ptr = _pContainer->newSensor();
  int err_no = ptr->init($2, $3, -1, $5);
  check(err_no);
}
;

//IEM(Apr26) added l_s_t and m_l_s l_s_t
multi_lane_sensors: lane_sensor
| lane_sensor_typed
| multi_lane_sensors lane_sensor
| multi_lane_sensors lane_sensor_typed
;

lane_sensor: RN_OCB inum fnum inum RN_CCB
{
  // $2=SensorID $3=WorkProb $4=LaneID
  RN_Sensor *ptr = _pContainer->newSensor();
  int err_no = ptr->init($2, $3, $4);
  check(err_no);
}
;

//IEM(Apr26) added this definition of l_s_t
lane_sensor_typed: RN_OCB inum fnum inum RN_COL inum RN_CCB
{
  // $2=SensorID $3=WorkProb $4=LaneID $6=intervalType
  RN_Sensor *ptr = _pContainer->newSensor();
  int err_no = ptr->init($2, $3, $4, $6);
  check(err_no);
}
;

//--------------------------------------------------------------------
// Grammer rules for control devices
//--------------------------------------------------------------------

signals: RN_SIGNALS signal_allocator RN_OCB RN_CCB
| RN_SIGNALS signal_allocator RN_OCB multi_signal_stations RN_CCB
{
  if (ToolKit::debug()) {
	cout << endc << _pContainer->nSignals()
		 << " traffic signal and signs parsed." << endl;
  }   
}
;

signal_allocator: num_objects
{
  int n = ($1 > 0) ? ($1) : (NUM_SIGNALS);
  _pContainer->signals_.reserve(n);
}
;

multi_signal_stations: signal_station
| multi_signal_stations signal_station
;

signal_station: RN_OCB signal_station_data signal_data RN_CCB
;

signal_station_data: inum fnum inum fnum
{
  // $1=Type $2=Visibility $3=Segment $4=Position
  RN_CtrlStation *ptr = _pContainer->newCtrlStation();
  int err_no = ptr->init($1, $2, $3, $4);
  check(err_no);
}
;

signal_data: link_signal
| multi_lane_signals
;

link_signal: RN_OCB inum inum RN_CCB
{
  // $2=SignalID $3=InitialState
  RN_Signal *ptr = _pContainer->newSignal();
  int err_no = ptr->init($2, $3);
  check(err_no);
}
;

multi_lane_signals: lane_signal
| multi_lane_signals lane_signal
;

lane_signal: RN_OCB inum inum inum RN_CCB
{
  // $2=SignalID $3=InitialState $4=LaneID
  RN_Signal *ptr = _pContainer->newSignal();
  int err_no = ptr->init($2, $3, $4);
  check(err_no);
}
;

//--------------------------------------------------------------------
// Grammer rules for toll plazas
//--------------------------------------------------------------------

toll_plazas: RN_TOLLS num_objects RN_OCB RN_CCB
| RN_TOLLS num_objects RN_OCB multi_toll_plazas RN_CCB
{
  if (ToolKit::debug()) {
	cout << endc << _pContainer->nTollBooths_
		 << " toll plazas parsed." << endl;
  }    
}
;

multi_toll_plazas: toll_plaza
| multi_toll_plazas toll_plaza
;

toll_plaza: RN_OCB toll_plaza_data multi_toll_booths RN_CCB
;

toll_plaza_data: fnum inum fnum
{
  // $1=Visibility $2=Segment $3=Position
  RN_CtrlStation *ptr = _pContainer->newCtrlStation();
  int err_no = ptr->init(CTRL_TOLLBOOTH, $1, $2, $3);
  check(err_no);
  _pContainer->lastCtrlStation_ = ptr;
}
;

multi_toll_booths: toll_booth
| multi_toll_booths toll_booth
;

toll_booth: RN_OCB inum inum inum inum inum fnum RN_CCB
{
  // $2=BoothID $3=InitialState $4=Lane
  // $5=LaneUseRule $6=Speed $7=FlowRate
  RN_TollBooth *ptr = _pContainer->newTollBooth();
  int err_no = ptr->initTollBooth($2, $3, $4, $5, $6, $7);
  check(err_no);
}
;

//--------------------------------------------------------------------
// margaret:  Grammar rules for Bus Stops 
//--------------------------------------------------------------------

bus_stops: RN_STOPS num_objects RN_OCB RN_CCB
| RN_STOPS num_objects RN_OCB multi_bus_stops RN_CCB
{
  if (ToolKit::debug()) {
	cout << endc << _pContainer->nBusStops_
		 << " bus stops parsed." << endl;
  }    
}
;

multi_bus_stops: bus_stop 
| multi_bus_stops bus_stop
;

bus_stop: RN_OCB bus_stop_data multi_bus_stop_chars RN_CCB //margaret:  what happens if I take this out?
;

bus_stop_data: fnum inum fnum
{
  // $1=Visibility $2=Segment $3=Position
  RN_CtrlStation *ptr = _pContainer->newCtrlStation();
  int err_no = ptr->init(CTRL_BUSSTOP, $1, $2, $3);
  check(err_no);
  _pContainer->lastCtrlStation_ = ptr;
}
;

multi_bus_stop_chars: bus_stop_chars
| multi_bus_stop_chars bus_stop_chars
;

bus_stop_chars: RN_OCB inum inum inum inum inum fnum inum RN_CCB
{
  // $2=StopID $3=InitialState $4=Lane
  // $5=LaneUseRule $6=Speed $7=FlowRate(or dwell time?)
  // $8=WaysideDummy (Dan)
  RN_BusStop *ptr = _pContainer->newBusStopChars();
  int err_no = ptr->initBusStopChars($2, $3, $4, $5, $6, $7, $8);
  check(err_no);
}
;

//--------------------------------------------------------------------
// Grammer rules for speed density functions
//--------------------------------------------------------------------

sdfns: RN_SDFNS sdfn_allocator RN_OCB RN_CCB
| RN_SDFNS sdfn_allocator RN_OCB multi_sdfns RN_CCB
{
  if (ToolKit::debug()) {
	cout << indent << _pContainer->nSdFns()
		 << " speed density functions parsed." << endl;
  }

}
;

sdfn_allocator: num_objects
{
  int n = ($1 > 0) ? ($1) : (NUM_SDFUNCTIONS);
  _pContainer->sdFns_.reserve(n);
}
;

multi_sdfns: sdfn
| multi_sdfns sdfn
;

sdfn: linear_sdfn
| non_linear_sdfn
;

//sdfn: non_linear_sdfn
//;

non_linear_sdfn: RN_OCB fnum fnum fnum fnum fnum RN_CCB
{
  RN_SdFn *fn = new RN_SdFnNonLinear($2, $3, $4, $5, $6);
  _pContainer->sdFns_.push_back(fn);
}
;


linear_sdfn: RN_OCB fnum fnum fnum fnum fnum fnum fnum RN_CCB
{
  RN_SdFn *fn = new RN_SdFnLinear($2, $3, $4, $5, $6, $7, $8);
  _pContainer->sdFns_.push_back(fn);
}
;



// tomer -- ------------------------------------------------------------
// grammer rules for intersections
//--------------------------------------------------------------------

intersections: RN_INTERSECTS IS_allocator multi_intersects
;

multi_intersects: intersect
| multi_intersects intersect
;

intersect: RN_ISNODE id RN_OCB multi_manuvers RN_CCB
;

id: RN_COL inum 
{
ISptr = _pContainer->newIS();
int err_no=ISptr->init($2);
check(err_no);
}
;



IS_allocator: num_objects
{
  int n= ($1>0) ? ($1) : (NUM_IS);
  _pContainer->IS_.reserve(n);
}
;


multi_manuvers: manuver
| multi_manuvers manuver
;

manuver: RN_OCB mymove RN_OCB multi_conflict RN_CCB RN_OCB multi_fifo RN_CCB characs RN_CCB
| RN_OCB mymove RN_OCB RN_CCB RN_OCB multi_fifo RN_CCB characs RN_CCB
| RN_OCB mymove RN_OCB multi_conflict RN_CCB RN_OCB RN_CCB characs RN_CCB
| RN_OCB mymove RN_OCB RN_CCB RN_OCB RN_CCB characs RN_CCB
;
 

mymove: inum RN_HIPHEN inum
{ 
mnvptr = ISptr->newManuver();
 int n=mnvptr->init($1, $3);
 ISptr->addManuver(mnvptr);
}
;

multi_conflict: conflict
|multi_conflict conflict
;

conflict: inum RN_HIPHEN inum
{
mnvptr->addConflict($1, $3);
}
;


multi_fifo: fifo
|multi_fifo fifo
;

fifo: RN_INT RN_HIPHEN RN_INT
{
mnvptr->addFifo($1, $3);
}
;


characs: fnum fnum
{
mnvptr->setCriticalGap($1);
mnvptr->setTurnSpeed($2);
mnvptr=0;
}
;


//--------------------------------------------------------------------
// Beginning of basic grammer rules.
//--------------------------------------------------------------------

inum: RN_INT
{
  char *ptr;
  $$ = (int)strtol(value(), &ptr, 0);
}
;

fnum: float_num
| inum
{
  $$ = $1;
}
;

float_num: RN_REAL
{
  $$ = atof(value());
}
;

szstring: RN_STRING
{
  static char str[256];
  strcpy(str, value("\"\"")) ;
  $$ = str ;
}
;

num_objects: specified
| unspecified
{
  $$ = $1;
}
;
 
specified: RN_COL inum
{
  $$ = $2;
}
;

unspecified: RN_COL
{
  $$ = 0;
}
;

//--------------------------------------------------------------------
// End of basic grammer rules
//--------------------------------------------------------------------


%%

// Following pieces of code will be verbosely copied into the parser.

%header{
  // Empty
%}
