//-*-c++-*------------------------------------------------------------
// NAME: Route choice
// NOTE: Stores time variant shortest path information
// AUTH: Qi Yang
// FILE: RN_DynamicRoute.C
// DATE: Tue Sep 24 09:26:21 1996
//--------------------------------------------------------------------

#include <iostream>
#include <new>
#include <cctype>

#include <Tools/VariableParser.h>
#include <Tools/ToolKit.h>
#include <IO/Exception.h>

#include "RoadNetwork.h"
#include "RN_Node.h"
#include "RN_Link.h"
#include "RN_LinkTimes.h"
#include "RN_LinkGraph.h"
#include "RN_DynamicRoute.h"
#include "RN_PathTable.h"
#include "BusAssignmentTable.h"
using namespace std;

char* RN_DynamicRoute::files_[2] = { NULL, NULL };
int RN_DynamicRoute::availability_ = 0;

// Do NOT call the ctor of the base class

RN_DynamicRoute::RN_DynamicRoute(const char *filename)
   : RN_Route(filename)
{
}


RN_DynamicRoute::RN_DynamicRoute(const RN_DynamicRoute& sp)
   : RN_Route(sp)
{
}


// ------------------------------------------------------------------
// Compute shortest paths from each link to every node for each time
// period
// ------------------------------------------------------------------

void
RN_DynamicRoute::findShortestPathTrees(double start, double stop)
{
   if (!linkGraph_) {
      linkGraph_ = new RN_LinkGraph(theNetwork, this);
   } else {
      linkGraph_->updateLinkCosts(this);
   }

   int s = whichPeriod(start);
   int t = whichPeriod(stop);
   register int i, p;
   double entry_time;
   
   int n = nLinks();
   int ni = t - s + 1;
   int nt = ni * n;

   const char* msg;
   msg = Str("Building %d x %d shortest path trees ...", ni, n);
   theNetwork->updateProgress(msg);

   /* Calculate expected shortest paths at given time */
   
   float done = 0.0;
   float step = (nt > 1) ? 1.0 / (nt - 1) : 1.0;
   ni = 0;
   for (p = s; p <= t; p ++) {  
      entry_time = whatTime(p);
      for (i = 0; i < n; i ++) {
		 linkGraph_->calcShortestPathTree(i, entry_time, this);
		 recordShortestPathTree(i, p);
		 done = ni * step * 100.0;
		 theNetwork->updateProgress(done);
		 ni ++;
      }
   }

   theNetwork->updateProgress();
}

// Return travel time on the shorted path from upstream end of link
// "olink" to node "dnode".

float
RN_DynamicRoute::upRouteTime(int olink, int dnode, double timesec)
// note: dnode is a destination node index, not a node index.
{
   unsigned long int offset;
   float dt = (timesec - infoStartTime()) / infoPeriodLength() + 0.5;
   int i = (int)dt;
   if (i < 1) {
      offset = olink * nDestNodes() + dnode;
      return routeTimes_.read(offset);
   } else if (i >= infoPeriods()) {
      offset = (infoPeriods() - 1) * matrixSize_ +
          olink * nDestNodes() + dnode;
      return routeTimes_.read(offset);
   } else {
      dt = timesec - infoStartTime() - (i - 0.5) * infoPeriodLength();
      offset = i * matrixSize_ + olink * nDestNodes() + dnode;
      float y = routeTimes_.read(offset);
      offset = (i - 1) * matrixSize_ + olink * nDestNodes() + dnode;
      float z = routeTimes_.read(offset);
      return z + (y - z) / infoPeriodLength() * dt;
   }
}


// Return travel time on the shorted path from dnstream end of link
// "olink" to node "dnode".

float
RN_DynamicRoute::dnRouteTime(int olink, int dnode, double timesec)
// note: dnode is a destination node index, not a node index.
{
   float total = upRouteTime(olink, dnode, timesec);
   float dt = linkTime(olink, timesec);
   return total - dt;
}


// Creates the empty travel time tables.  It stores the filenames and
// sets preTripGuidance_ only.

int RN_DynamicRoute::setFileNamesOfTravelTimeTables(GenericVariable &gv)
{
   const char *str;
   if (gv.nElements() > 0) {
	  if (files_[0]) free(files_[0]);
	  str = gv.element(0);
	  files_[0] = ::Copy(str);
   } else {
	  files_[0] = NULL;
   }
   if (gv.nElements() > 1) {
	  if (files_[1]) free(files_[1]);
	  str = gv.element(1);
	  files_[1] = ::Copy(str);
   } else {
	  files_[1] = NULL;
   }
   if (gv.nElements() > 2) {
	  theSpFlag = gv.element(2);
	  if (theSpFlag & INFO_FLAG_AVAILABILITY) {
		 availability_ = 1;
	  } else {
		 availability_ = 0;
	  }
   } else {
	  theSpFlag = 0;
	  availability_ = 0;
   }

   if (gv.nElements() == 1) {	// one table use for both group of drivers
	  files_[1] = ::Copy(files_[0]);
   }
   if (gv.nElements() > 3) {
	  cerr << "Error:: unexpected parameters skipped" << endl;
	  return 1;
   } else {
	  return 0;
   }
}


// Reads travel time tables, called after the network has been read

void RN_DynamicRoute::parseTravelTimeTables(char tag)
{
   char *filename[2];

   // Hibitual travel time info, if specified, used by unguided
   // drivers

   if (ToolKit::isValidInputFilename(files_[0])) {
	  filename[0] = ::Copy(ToolKit::optionalInputFile(files_[0]));
   } else {						// no info specified
	  filename[0] = NULL;
   }

   // Updated travel time info, if specified, used by guided drivers

   if (ToolKit::isValidInputFilename(files_[1])) {
	  filename[1] = ::Copy(ToolKit::optionalInputFile(files_[1]));
   } else if (filename[0]) {	// same info as unguided drivers
	  filename[1] = ::Copy(filename[0]);
   } else {						// no info specified
	  filename[1] = NULL;
   }

   // Create route table, which can be dynamic or static

   if (theSpFlag & INFO_FLAG_DYNAMIC) {
      theUnGuidedRoute = new RN_DynamicRoute(filename[0]);
      theGuidedRoute = new RN_DynamicRoute(filename[1]);
   } else {
      theUnGuidedRoute = new RN_Route(filename[0]);
      theGuidedRoute = new RN_Route(filename[1]);
   }

   // Set the availability

   theUnGuidedRoute->preTripGuidance(1);
   if (availability_) {
	  theSpFlag |= INFO_FLAG_AVAILABILITY;
      theGuidedRoute->preTripGuidance(1);
   } else {
	  theSpFlag &= ~INFO_FLAG_AVAILABILITY;
      theGuidedRoute->preTripGuidance(0);
   }

   // Calculation the path table

   theUnGuidedRoute->initTravelTimes();
   theUnGuidedRoute->initialize(tolower(tag));
   if (Cmp(theGuidedRoute->filename(),
		   theUnGuidedRoute->filename())) {
	  theGuidedRoute->initTravelTimes();
      theGuidedRoute->initialize(toupper(tag));
   } else {
      *theGuidedRoute = *theUnGuidedRoute;
   }

   if (filename[0]) free(filename[0]);
   if (filename[1]) free(filename[1]);
}
