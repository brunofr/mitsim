//
// RN_BusRouteParser.y -- RN_BusRoute table parser generator
// by Qi Yang & Daniel Morgan
//

%name   RN_BusRouteParser
%define ERROR_BODY	DEF_ERROR_BODY
%define LEX_BODY	  DEF_LEX_BODY

// These macros are expanded when creating the definition for named
// BisonParser class.  The functions defined as member here are
// designed for use in the grammar rules.

%define CONSTRUCTOR_PARAM  RN_BusRouteTable* pContainer_
%define CONSTRUCTOR_INIT   : _pContainer(pContainer_)   \
                           ,_rid(0)                     \

%define MEMBERS            DEF_MEMBERS \
    RN_BusRouteTable* _pContainer ;    \
    int _rid ;                         \

%union {
  long        type_i ;
  double      type_f ;
  const char* type_s ;
}

%token <type_s> LEX_EQUAL
%token <type_s> LEX_COMMA
%token <type_s> LEX_COLON
%token <type_s> LEX_SEMICOLON
%token <type_s> LEX_OAB
%token <type_s> LEX_CAB
%token <type_s> LEX_OBB
%token <type_s> LEX_CBB
%token <type_s> LEX_OCB
%token <type_s> LEX_CCB
%token <type_i> LEX_INT
%token <type_f> LEX_REAL
%token <type_s> LEX_TIME
%token <type_s> LEX_PAIR
%token <type_s> LEX_TAG
%token <type_s> LEX_STRING
%token <type_s> LEX_DUP
%token <type_s> LEX_END

%header{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <IO/Exception.h>

#include <Tools/Scanner.h>
#include <Tools/ToolKit.h>  

#include <GRN/RN_BusRouteTable.h>
#include <GRN/RN_BusRoute.h>
#include <GRN/RN_Link.h>
%}

//--------------------------------------------------------------------
// Symbols used road network database grammer rules.
//--------------------------------------------------------------------

%type <type_s> block_name
%type <type_i> inum num_objects specified unspecified
%type <type_f> fnum float_num
	   		 
%type <type_s> route_table bus_route_table_allocator route_data_1
%type <type_s> multi_bus_routes bus_route bus_route_data route_data_2
%type <type_s> multi_links link multi_timepoints timepoint
%type <type_s> multi_ctrl_points ctrl_point

%start everything

%%

//--------------------------------------------------------------------
// Beginning of RN_BusRoute table grammer rules.
//--------------------------------------------------------------------

everything: route_table
{
  if (ToolKit::debug()) {
	cout << "Finished parsing bus route table <"
		 << filename() << ">." << endl;
  }
}
;

route_table: block_name bus_route_table_allocator LEX_OCB LEX_CCB
| block_name bus_route_table_allocator LEX_OCB multi_bus_routes LEX_CCB
{
  int n = _pContainer->nBusRoutes();
  if (ToolKit::debug()) {
	cout << indent << n << " parsed." << endl;
  }
}
;

bus_route_table_allocator: num_objects
{
  int n = ($1 > 0) ? ($1) : (NUM_BUSROUTES);
  _pContainer->busroutes_.reserve(n);
}
;

multi_bus_routes: bus_route
| multi_bus_routes bus_route
;

bus_route: LEX_OCB bus_route_data LEX_OCB multi_links LEX_CCB LEX_CCB
| LEX_OCB bus_route_data LEX_OCB multi_links LEX_CCB LEX_OCB multi_timepoints LEX_CCB LEX_CCB
| LEX_OCB bus_route_data LEX_OCB multi_links LEX_CCB LEX_OCB multi_timepoints LEX_CCB LEX_OCB multi_ctrl_points LEX_CCB LEX_CCB
;

bus_route_data: route_data_1
| route_data_2
;

route_data_1: inum
{
  // $1=RouteID
  _rid = $1;

  int err_no = _pContainer->addBusRoute($1);
  check(err_no) ;
}
;

route_data_2: inum fnum
{
  // $1=RouteID, $2=DesignHeadway
  _rid = $1;

  int err_no = _pContainer->addBusRoute($1, $2);
  check(err_no) ;
}
;

multi_links: link
| multi_links link
;

link: inum
{
  // $1=LinkID
  int err_no = _pContainer->lastBusRoute()->addRouteLink($1);
  check(err_no);
}
;

multi_timepoints: timepoint
| multi_timepoints timepoint
;

timepoint: inum
{
  // $1=Timepoint ID
  int err_no = _pContainer->lastBusRoute()->addTimepoint($1);
  check(err_no);
}
;

multi_ctrl_points: ctrl_point
| multi_ctrl_points ctrl_point
;

ctrl_point: inum
{
  // $1=Bus Stop ID
  int err_no = _pContainer->enableControl(_rid, $1);
  check(err_no);
}
;
//--------------------------------------------------------------------
// Beginning of basic grammer rules.
//--------------------------------------------------------------------

block_name: LEX_TAG
{
  const char *s = value("[]");
  if (ToolKit::debug()) {
    cout << "Parsing " << s << " ..." << endl;
  }
  $$ = s;
}
;

inum: LEX_INT
{
  $$ = atoi(value());
}
;

fnum: float_num
| inum
{
  $$ = $1;
}
;

float_num: LEX_REAL
{
  $$ = atof(value());
}
;

num_objects: specified
| unspecified
{
  $$ = $1;
}
;
 
specified: LEX_COLON inum
{
  $$ = $2;
}
;

unspecified: LEX_COLON
{
  $$ = 0;
}
;

%%

// Following pieces of cpte will be verbosely copied into the parser.

%header{
  // empty
%}
