//-*-c++-*------------------------------------------------------------
// FILE: RN_DrawableLane.h
// DATE: Sat Nov 11 15:39:56 1995
//--------------------------------------------------------------------

#ifndef RN_SHAPEDLANE_HEADER
#define RN_SHAPEDLANE_HEADER

#include "WcsPoint.h"
#include "RN_Lane.h"
#include "RN_Curve.h"

class RN_DrawableSegment;

// This class is derived from RN_Lane and provides information needed
// for drawing the lane, sensors, signals, vehicles in the lane.  If
// you need graphics support, inherent your Lane from RN_DrawableLane;
// otherwise use just RN_Lane would be more efficient.


// The RN_Curve represents the right lane mark of the lane.

class RN_DrawableLane : public RN_Lane, public RN_Curve
{
      friend class RN_DrawableSegment;

   protected:

      float upWidth_;
      float dnWidth_;

   public:

      RN_DrawableLane() : RN_Lane(), RN_Curve() { }
      virtual ~RN_DrawableLane() { }

      inline float upWidth() { return upWidth_;}
      inline float dnWidth() { return dnWidth_;}

      // Lane width at intermediate point. r=1 is upWidth and r=0 is
      // the dnWidth.

      inline float width(float r = 0.5) {
	 return (r * upWidth_ + (1.0 - r) * dnWidth_);
      }

      // These functions returns a point at position r (0=downstream
      // 1=upstream).

      WcsPoint leftPoint(float r = 0.5);
      WcsPoint centerPoint(float r = 0.5);
      WcsPoint rightPoint(float r = 0.5);

   private:
      
      // These two variables are used in setting up the geometry data
      // for drawing.  They are the number of lanes, counting from the
      // left curb of the segment to the right lane mark.  When
      // calcWidth() is done, these are no longer needed. Should we
      // replace them with a temporay global array?

      short int upOffset_;	// unit is num of lanes
      short int dnOffset_;	// unit is num of lanes
};

#endif
