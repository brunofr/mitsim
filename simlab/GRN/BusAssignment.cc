//-*-c++-*------------------------------------------------------------
// FILE: BusAssignment.C
// DATE: Thu Oct 19 17:28:39 1995
//--------------------------------------------------------------------


#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>
#include <Tools/Random.h>

#include "RoadNetwork.h"
#include "RN_Label.h"
#include "RN_Link.h"
#include "RN_Node.h"
#include "BusAssignment.h"
#include "BusAssignmentTable.h"
#include "RN_BusSchedule.h"
#include "RN_BusScheduleTable.h"
#include "RN_BusRoute.h"
#include "RN_BusRunTable.h"
#include "RN_BusRun.h"
#include "RN_BusStop.h"
#include "RN_LinkTimes.h"

#include <iostream>
using namespace std;

int BusAssignment::sorted_ = 1;	// element is sorted

// Called by BusAssignmentParser to initialize a bus schedule

int
BusAssignment::init(int c, int t, int r)
{
   static int idx = 0;
   static int last = -1;
   tripTracker_ = 0;
   arrivalTracker_ = 0;
   code_ = busID_ = - c;
   busType_ = t;
   load_ = -1;     // load will be randomized in TS_VehicleLoader::initialize
   run_ = theBusRunTable->findBusRun(r);
   if (sorted_ && - c <= last) sorted_ = 0;
   else last = - c;

   currentTrip_ = run_->firstTrip();
   nextSchedArrival_ = currentTrip_->arrival(0);
   leftOrRight_ = 0;

   double dhw = 0.;
   if ((dhw = route()->designHeadway()) > 0) {
     float u = theRandomizers[Random::Departure]->urandom();
     headway_ = (dhw*(-log(u)));
   }
   else headway_ = 0.;      // artificial initial value

   schedDeviation_ = 0.;    // assume bus enters network "on time"
   priorityRequest_ = -1;   // artificial initial value
   priorityGranted_ = -1;   // artificial initial value

   index_ = idx ++;

   return 0;
}

int
BusAssignment::initBRT(int c, int t, int r, double hw)
{
   static int idx = 0;
   static int last = -1;
   tripTracker_ = 0;
   arrivalTracker_ = 0;
   code_ = busID_ = c;
   busType_ = t;
   load_ = -1;     // load will be randomized in TS_VehicleLoader::initialize
   run_ = theBusRunTable->findBusRun(r);
   if (sorted_ && - c <= last) sorted_ = 0;
   else last = - c;

   currentTrip_ = run_->firstTrip();
   nextSchedArrival_ = currentTrip_->arrival(0);
   leftOrRight_ = 0;

   float u = theRandomizers[Random::Departure]->urandom();
   headway_ = (hw*(-log(u)));
   //   headway_ = hw;          // assume bus enters at design headway 

   schedDeviation_ = 0.;   // assume bus enters network "on time"
   priorityRequest_ = -1;  // artificial initial value
   priorityGranted_ = -1;  // artificial initial value

   index_ = idx ++;

   return 0;
}

int
BusAssignment::init(int c, int t, int r, int l)
{
   static int idx = 0;
   static int last = -1;
   tripTracker_ = 0;
   arrivalTracker_ = 0;
   code_ = busID_ = - c;
   busType_ = t;
   load_ = l;      // initialize bus load
   run_ = theBusRunTable->findBusRun(r);
   if (sorted_ && - c <= last) sorted_ = 0;
   else last = - c;

   currentTrip_ = run_->firstTrip();
   nextSchedArrival_ = currentTrip_->arrival(0);
   leftOrRight_ = 0;

   double dhw = 0.;
   if ((dhw = route()->designHeadway()) > 0) {
     float u = theRandomizers[Random::Departure]->urandom();
     headway_ = (dhw*(-log(u)));
   }
   else headway_ = 0.;      // artificial initial value

   schedDeviation_ = 0.;   // assume bus enters network "on time"
   priorityRequest_ = -1;  // artificial initial value
   priorityGranted_ = -1;  // artificial initial value

   index_ = idx ++;

   return 0;
}

// Called by BusAssignmentParser to add a scheduled arrival time to a bus schedule

void
BusAssignment::print(ostream &os)
{
   register int i;
   os << indent << "{";
   os << code_ << endc;
   os << run_->code() << endl;
}

RN_BusSchedule*
BusAssignment::currentTrip()
{
  return currentTrip_;
}

/* This function is called from TS_Vehicle::transposeToNextLane if the bus
   has reached the last link in it's current trip, the current trip is 
   set to the next trip in the bus' run assignment and the counter is 
   incremented by one for the next call to the function. */

void
BusAssignment::updateCurrentTrip()
{
  arrivalTracker_ = 0;
  tripTracker_ ++;
  currentTrip_ = run_->trip(tripTracker_);
}

void
BusAssignment::updateNextArrival()
{
  arrivalTracker_ ++;
  nextSchedArrival_ = currentTrip_->arrival(arrivalTracker_);
}

void
BusAssignment::updateBusLoad(int l)
{
  load_ = l;
}

void
BusAssignment::updatePriorityRequest(int c)
{
  priorityRequest_ = c;
}

void
BusAssignment::updatePriorityGranted(int p)
{
  priorityGranted_ = p;
}

double
BusAssignment::designHeadway()
{
  return route()->designHeadway();
}

void
BusAssignment::updateSchedDeviation(int sd)
{
  if (currentTrip_->nArrivals() > 0) {
    /* only update the bus' schedule deviation if it 
       has scheduled arrival times */
    schedDeviation_ = sd;
  }
}

RN_BusRoute*
BusAssignment::route()
{
  return currentTrip_->tripRoute();
}
