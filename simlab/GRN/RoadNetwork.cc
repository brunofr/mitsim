//-*-c++-*------------------------------------------------------------
// RoadNetwork.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//--------------------------------------------------------------------

#include <new>
#include <fstream>
#include <algorithm>
#include <cstring>
using namespace std;

#include <sys/types.h>
#include <time.h>
#include <stdlib.h>

#include <Tools/Math.h>
#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <Tools/SimulationClock.h>
#include <Tools/SimulationEngine.h>

#include <UTL/State3D.h>
#include <UTL/Misc.h>

#include "RoadNetwork.h"
#include "Parameter.h"
#include "RN_Node.h"
#include "RN_Link.h"
#include "RN_Segment.h"
#include "RN_SdFn.h"
#include "RN_Lane.h"
#include "RN_Label.h"
#include "RN_Sensor.h"
#include "RN_Signal.h"
#include "RN_SurvStation.h"
#include "RN_CtrlStation.h"
#include "RN_TollBooth.h"
#include "RN_BusStop.h" //margaret
#include "RN_Path.h"
#include "RN_PathTable.h"
#include "BusAssignmentTable.h"
#include "OD_Cell.h"
#include "OD_Table.h"
#include "RN_DynamicRoute.h"


// tomer 

#include "RN_IS.h"

// **


#ifdef EXTERNAL_MOE
#include <MOE/MOE_TSSampler.h>
#endif

RoadNetwork * theNetwork = NULL;

char * RoadNetwork::name_ = strdup("network.dat");


RoadNetwork::RoadNetwork()
    : description_(NULL),
      nDestNodes_(0),
      nStdSignals_(0), nTollBooths_(0), nBusStops_(0), //margaret
      lastSurvStation_(NULL), lastCtrlStation_(NULL),
      nConnectors_(0), nProhibitors_(0),
      totalLinkLength_(0.0), totalLaneLength_(0.0)
{
}


RoadNetwork::~RoadNetwork()
{
    delete [] description_;
}


#ifdef EXTERNAL_MOE
char * RoadNetwork::moeSpecFile_ = "";
#endif

//--------------------------------------------------------------------
// These functions create new network objects.  They are virtual and 
// should be overloaded in the derived classes if your network objects
// differ from the defaults.
//--------------------------------------------------------------------

RN_Node* RoadNetwork::newNode() { 
    return new RN_Node; 
}

RN_Link* RoadNetwork::newLink() { 
    return new RN_Link;
}

RN_Segment* RoadNetwork::newSegment() {
    return new RN_Segment;
}

RN_Lane* RoadNetwork::newLane() {
    return new RN_Lane;
}

RN_Label* RoadNetwork::newLabel() {
    return new RN_Label;
}

RN_Sensor* RoadNetwork::newSensor() {
    return new RN_Sensor;
}

RN_Signal* RoadNetwork::newSignal() {
    return new RN_Signal;
}

RN_SurvStation* RoadNetwork::newSurvStation() {
    return new RN_SurvStation;
}

RN_CtrlStation* RoadNetwork::newCtrlStation() {
    return new RN_CtrlStation;
}

RN_TollBooth* RoadNetwork::newTollBooth() {
    return new RN_TollBooth;
}

RN_BusStop* RoadNetwork::newBusStopChars() { //margaret
    return new RN_BusStop;
}

// tomer 

RN_IS* RoadNetwork::newIS() { 
    return new RN_IS; 
}

// **


RN_Label* RoadNetwork::findLabel(int c)
{
    vector<RN_Label*>::iterator i;
    i = find_if(labels_.begin(), labels_.end(), CodeEqual(c));
    if (i != labels_.end()) return *i;
    else return NULL;
}

RN_Node* RoadNetwork::findNode(int c)
{
    vector<RN_Node*>::iterator i;
    i = find_if(nodes_.begin(), nodes_.end(), CodeEqual(c));
    if (i != nodes_.end()) return *i;
    else return NULL;
}

RN_Link* RoadNetwork::findLink(int c)
{
    vector<RN_Link*>::iterator i;
    i = find_if(links_.begin(), links_.end(), CodeEqual(c));
    if (i != links_.end()) return *i;
    else return NULL;
}

RN_Segment* RoadNetwork::findSegment(int c)
{
    vector<RN_Segment*>::iterator i;
    i = find_if(segments_.begin(), segments_.end(), CodeEqual(c));
    if (i != segments_.end()) return *i;
    else return NULL;
}

RN_Lane* RoadNetwork::findLane(int c)
{
    vector<RN_Lane*>::iterator i;
    i = find_if(lanes_.begin(), lanes_.end(), CodeEqual(c));
    if (i != lanes_.end()) return *i;
    else return NULL;
}

RN_Sensor* RoadNetwork::findSensor(int c)
{
    vector<RN_Sensor*>::iterator i;
    i = find_if(sensors_.begin(), sensors_.end(), CodeEqual(c));
    if (i != sensors_.end()) return *i;
    else return NULL;
}

RN_Signal* RoadNetwork::findSignal(int c)
{
    vector<RN_Signal*>::iterator i;
    i = find_if(signals_.begin(), signals_.end(), CodeEqual(c));
    if (i != signals_.end()) return *i;
    else return NULL;
}

RN_BusStop* RoadNetwork::findBusStop(int c)
{
    vector<RN_BusStop*>::iterator i;
    i = find_if(busstops_.begin(), busstops_.end(), CodeEqual(c));
    if (i != busstops_.end()) return *i;
    else return NULL;
}

// tomer - ?????????????????????????????????

//RN_IS* RoadNetwork::findIS(int c)
//{
//    vector<RN_IS*>::iterator i;
//    i = find_if(IS_.begin(), IS_.end(), CodeEqual(c));
//    if (i != IS_.end()) return *i;
//    else return NULL;
//}

// **


void RoadNetwork::updateProgress(const char *msg)
{
    if (!ToolKit::debug() && ToolKit::verbose()) {
        if (msg) {
            cout << msg << endl;
        } else {
            cout << endl;
        }
    }
}

void RoadNetwork::updateProgress(float pct)
{
    if (!ToolKit::debug() && ToolKit::verbose()) {
        printf("\b\b\b\b%3.0f%%", pct);
    }
}


//--------------------------------------------------------------------
// Requires: Called after network database has been sucessfully parsed
// Modifies: variables in link and node
// Effects : Sorts links and calculates topology variables.
//--------------------------------------------------------------------

void
RoadNetwork::calcStaticInfo(void)
{
    register int i;

    // Create a default speed density function if no one is defined

    if (nSdFns() <= 0) {
        sdFns_.push_back(new RN_SdFnLinear);
    }

    // Calculate the arc information in worldSpace

    for (i = 0; i < nSegments(); i ++) {

        // Make end point of each pair of connected segments snapped at
        // the same point

        segments_[i]->snapCoordinates();
    }

    // Create the world space
   
    worldSpace_.createWorldSpace();

    for (i = 0; i < nSegments(); i ++) {

        // Generate arc info such as angles and length from the two
        // endpoints and bulge.  This function also convert the
        // coordinates from database format to world space format

        segments_[i]->calcArcInfo(worldSpace_);
    }
	
    // Sort outgoing and incoming arcs at each node.
    // Make sure RN_Link::comp() is based on angle.

    for (i = 0; i < nNodes(); i ++) {
        nodes_[i]->sortUpLinks();
        nodes_[i]->sortDnLinks();
    }

    // Set destination index of all destination nodes
   
    for (i = 0; i < nNodes(); i ++) {
        nodes_[i]->destIndex_ = -1;
    }
    for (i = nDestNodes_ = 0; i < nNodes(); i ++) {
        if( nodes_[i]->type() & NODE_TYPE_DES ) 
            nodes_[i]->destIndex_ = nDestNodes_++;
    }
    if( nDestNodes_ == 0 ) { 
        cerr << "Warning:: Destination nodes not defined." << endl
             << "All nodes are treated as destinations for "
             << "shortest path calculations." << endl;
        for (i = 0; i < nNodes(); i ++) {
            nodes_[i]->destIndex_ = nDestNodes_++;
        }
    }
   
    // Set upLink and dnLink indices

    for (i = 0; i < nLinks(); i ++) {
        links_[i]->calcIndicesAtNodes();
    }

    // Set variables in links

    for (i = 0; i < nLinks(); i ++) {
        links_[i]->calcStaticInfo();
    }

    // Set variables in segments

    for (i = 0; i < nSegments(); i ++) {
        segments_[i]->calcStaticInfo();
    }

    // Set variables in lanes

    for (i = 0; i < nLanes(); i ++) {
        lanes_[i]->calcStaticInfo();
    }
}


// Print some basic information on this network

void
RoadNetwork::printBasicInfo(ostream &os)
{
    os << endl;
    os << description_ << endl;
    os << "Number of network objects:" << endl;
    os << indent << "Nodes       = " << nNodes() << endl;
    os << indent << "Dest Nodes  = " << nDestNodes_ << endl;

    // tomer

    os << indent << "Intersection= " << nIS() << endl;

    // **


    os << indent << "Links       = " << nLinks() << endl;
    os << indent << "Segments    = " << nSegments() << endl;
    os << indent << "Lanes       = " << nLanes() << endl;
    os << indent << "Sensors     = " << nSensors() << endl;
    os << indent << "Signals     = " << nSignals() << endl;
    os << indent << "Toll Booths = " << nTollBooths_ << endl;
    os << indent << "Bus Stops = " << nBusStops_ << endl; //margaret
    os << indent << "SD Functions= " << nSdFns() << endl;
    os << "Total lengths (meters):" << endl;

    os << indent << "Links = " << totalLinkLength_ << endl;
    os << indent << "Lanes = " << totalLaneLength_ << endl;
}


// Save the network database. It returns 0 if no error

int RoadNetwork::save(const char *fullname)
{
    if (!fullname) {
        fullname = ToolKit::infile(name_);
    }

    if (!ToolKit::isValidFilename(fullname)) return 1;

    // Backup the original file

    if (ToolKit::fileExists(fullname)) {
        const char *s;
        s = Str("%s~", fullname);
        int err = rename(fullname, s);
        if (err < 0) {
            return 2;
        }
    }

    ofstream os(fullname);
    if (os.good()) {
        cout << endl << "Writing network database file in <"
             << fullname << "> ... ";
        cout.flush();
        theNetwork->print(os);
        os.close();
        cout << "done." << endl;
        if (name_ != fullname) delete [] name_;
        name_ = ::Copy(ToolKit::splitName(fullname));
        return 0;
    } else {
        cerr << endl << "Error:: cannot create file <"
             << fullname << ">." << endl;
        return 3;
    }
}

//--------------------------------------------------------------------
// Requires: Network database has been parsed successfully
// Modifies: os
// Effects : Appends the network to os in a format consistent to
//           the network database.
//--------------------------------------------------------------------

void
RoadNetwork::print(ostream &os)
{
    os << "/*" << endl
       << " * Created from <" << name_ << ">" << endl
       << " * on " << TimeTag() << endl
       << " * by " << UserName() << endl
       << " */" << endl << endl;

    os << "[Title] : \"" << description_ << "\"" << endl << endl;

    if (nLabels()) {
        printLabels(os); os << endl;
    }
   
    printNodes(os); os << endl;
    printLinks(os); os << endl;

    // tomer - commented out. not used,  see .h for details

    
    // printIS(os); os << endl;
    

    // **


    printConnectors(os); os << endl;
    printProhibitors(os); os << endl;

    if (nSensors()) {
        printSensors(os); os << endl;
    }
    if (nStdSignals()) {
        printSignals(os); os << endl;
    }
    if (nTollBooths()) {
        printTollPlazas(os); os << endl;
    }
    if (nBusStops()) { //margaret
        printBusStops(os); os << endl;
    }
    if (nSdFns()) {
        printSdFns(os); os << endl;
    }
    os << "// The End" << endl;
}

void
RoadNetwork::printLabels(ostream &os)
{
    os << "[Link Labels] : " << nLabels() << endl
       << OPEN_TOKEN << endl
       << indent << "# {LabelID \"Street name\"}" << endl;
    for (register int i = 0; i < nLabels(); i ++) {
        labels_[i]->print(os);
    }
    os << CLOSE_TOKEN << endl << endl;
}

void
RoadNetwork::printNodes(ostream &os)
{
    os << "[Nodes] : " << nNodes() << endl
       << OPEN_TOKEN << endl
       << indent << "# {NodeID Type \"Name\"}" << endl;
    for (register int i = 0; i < nNodes(); i ++) {
        nodes_[i]->print(os);
    }
    os << CLOSE_TOKEN << endl << endl;
}




void
RoadNetwork::printLinks(ostream &os)
{
    os << "[Links] : "
       << nLinks() << " : " << nSegments() << " : " << nLanes() << endl;
    os << OPEN_TOKEN << endl;
   
    os << indent << "# Link: // listed in arbitrary order" << endl
       << indent << "# {LinkID LinkType UpNodeID DnNodeID LinkLabelID" << endl
       << indent << "#" << endl
       << indent << "# Segment: // listed upstrm to dnstrm within link" << endl
       << indent << "#   {SegmentID SpeedLimit FreeSpeed Grad" << endl
       << indent << "#     {StartingPntX StartingPntY Bulge EndPntX EndPntY}" <<endl
       << indent << "#" << endl
       << indent << "# Lane: // listed from left to right" << endl
       << indent << "#       {LaneID Rules}" << endl
       << indent << "#   }" << endl
       << indent << "# }" << endl << endl;

    for (register int i = 0; i < nLinks(); i ++) {
        links_[i]->print(os);
    }
    os << CLOSE_TOKEN << endl << endl;
}

void
RoadNetwork::printConnectors(ostream &os)
{
    os << "[Lane Connectors] : " << nConnectors_ << endl
       << OPEN_TOKEN << endl
       << indent << "# {UpLane DnLane}" << endl;
    for (register int i = 0; i < nLanes(); i ++) {
        lanes_[i]->printDnConnectors(os);
    }
    os << CLOSE_TOKEN << endl << endl;
}

void
RoadNetwork::printProhibitors(ostream &os)
{
    register int i, cnt = 0;

    for (i = 0; i < nLinks(); i ++) {
        cnt += links_[i]->countNotConnectedDnLinks();
    }
    if (!cnt) return;

    os << "[Turn Prohibitors] : " << cnt << endl
       << OPEN_TOKEN << endl
       << indent << "# {FromLink ToLink}" << endl;

    for (i = 0; i < nLinks(); i ++) {
        links_[i]->printNotConnectedDnLinks(os);
    }
    os << CLOSE_TOKEN << endl << endl;
}

void
RoadNetwork::printSensors(ostream &os)
{
    os << "[Sensors] : " << nSensors() << endl
       << OPEN_TOKEN << endl;
    os << indent << "# {TypeCode TaskCode ZoneLength SegmentID PosInSegment" << endl
       << indent << "#   {SensorID WorkProbability LaneID} // lane specific" << endl
       << indent << "#   or" << endl
       << indent << "#   {SensorID WorkProbability} // link specific" << endl
       << indent << "# }" << endl;

    for (register int i = 0; i < nLinks(); i ++) {
        links_[i]->printSensors(os);
    }
    os << CLOSE_TOKEN << endl << endl;
}

void
RoadNetwork::printSignals(ostream &os)
{
    os << "[Signals] : " << nStdSignals_ << endl
       << OPEN_TOKEN << endl;

    os << indent << "# {Type Visibility SegmentID PosInSegment" << endl
       << indent << "#   {SignalID InitialState} // link specific" << endl
       << indent << "#   or" << endl
       << indent << "#   {SignalID InitialState LaneID} // lane specific" << endl
       << indent << "# }" << endl;

    for (register int i = 0; i < nLinks(); i ++) {
        links_[i]->printSignals(os);
    }
    os << CLOSE_TOKEN << endl << endl;
}


void
RoadNetwork::printTollPlazas(ostream &os)
{
    os << "[Toll Plazas] : " << nTollBooths_ << endl
       << OPEN_TOKEN << endl;

    os << indent << "# {Visibility SegmentID PosInSegment" << endl
       << indent << "#   {BoothID InitialState LaneID Rules MaxSpeed StopTime}"
       << endl
       << indent << "# }" << endl;

    for (register int i = 0; i < nLinks(); i ++) {
        links_[i]->printTollPlazas(os);
    }
    os << CLOSE_TOKEN << endl << endl;
}

void  //margaret:  so we don't need this, since there is no .RNE input to translate into .DAT file
RoadNetwork::printBusStops(ostream &os) //margaret
{
    os << "[Bus Stops] : " << nBusStops_ << endl
       << OPEN_TOKEN << endl;

    os << indent << "# {Visibility SegmentID PosInSegment" << endl
       << indent << "#   {BoothID InitialState LaneID Rules MaxSpeed StopTime}"
       << endl
       << indent << "# }" << endl;

    for (register int i = 0; i < nLinks(); i ++) {
        links_[i]->printBusStops(os);
    }
    os << CLOSE_TOKEN << endl << endl;
}

void
RoadNetwork::printSdFns(ostream &os)
{
    os << "[Speed-Density Functions] : " << nSdFns() << endl
       << OPEN_TOKEN << endl;

    os << indent << "# {" << endl
       << indent << "#   {Capacity MinSpd JamDensity DensityBeta SpeedBeta}" << endl
       << indent << "#  or " << endl
       << indent << "#   {Capacity MinSpd JamDensity Kl Vh Kh Vl}" << endl
       << indent << "# }" << endl;

    for (register int i = 0; i < nSdFns(); i ++) {
        sdFns_[i]->print(os);
    }
    os << CLOSE_TOKEN << endl << endl;
}


// Connects lane 'up' with lane 'dn'.  Return -1 if error, 1 if these
// two lanes are already connected, or 0 if success.

int 
RoadNetwork::addLaneConnector(int up, int dn)
{
    if (ToolKit::debug()) {
        cout << indent << "<"
             << up << endc << dn
             << ">" << endl;
    }

    RN_Lane *ulane, *dlane;
    if (!(ulane = findLane(up))) {
        cerr << "Error:: unknown upstream lane <" << up << ">. ";
        return -1;
    } else if (!(dlane = findLane(dn))) {
        cerr << "Error:: unknown upstream lane <" << dn << ">. ";
        return -1;
    }

    // Check if this connector make sense

    if (!isNeighbor(ulane->segment(), dlane->segment())) {
        cerr << "Error:: lanes <" << up << "> and <" << dn << "> "
             << "are not neighbors. ";
        return -1;
    }

    if (ulane->findInDnLane(dn) || dlane->findInUpLane(up)) {
        cerr << "Warning:: lanes <" << up << "> and <" << dn << "> "
             << "are already connected. ";
        return 1;
    }
    ulane->dnLanes_.push_back(dlane);
    dlane->upLanes_.push_back(ulane);

    nConnectors_ ++;

    return 0;
}


// Exclude turn movements from link 'up' to link "dn". Returns 0 if
// it success, -1 if error, or 1 if the turn is already excluded.

int 
RoadNetwork::addTurnProhibitor(int up, int dn)
{
    if (ToolKit::debug()) {
        cout << indent << "<"
             << up << endc << dn
             << ">" << endl;
    }

    RN_Link *ulink, *dlink;
    if (!(ulink = findLink(up))) {
        cerr << "Error:: unknown upstream link <" << up << ">. ";
        return -1;
    } else if (!(dlink = findLink(dn))) {
        cerr << "Error:: unknown upstream link <" << dn << ">. ";
        return -1;
    }

    // Check if this prohibitor make sensor

    if (!isNeighbor(ulink, dlink)) {
        cerr << "Error:: links <" << up << "> and <" << dn << "> "
             << "are not neighbors. ";
        return -1;
    }

    ulink->excludeTurn(dlink);

    nProhibitors_ ++;

    return 0;
}


// Returns 1 if s1 is a neighbor upstream segment of s2 or 0 otherwise

int
RoadNetwork::isNeighbor(RN_Segment *s1, RN_Segment *s2)
{
    if (s1->link() == s2->link()) {
        if ((s1->index() + 1) == s2->index()) return 1;
        else return 0;
    } else if (s1->downstream() || s2->upstream()) {
        return 0;
    } else if (!isNeighbor(s1->link(), s2->link())) {
        return 0;
    }
    return 1;
}


// Returns 1 if s1 is a neighbor upstream link

int
RoadNetwork::isNeighbor(RN_Link *s1, RN_Link *s2)
{
    if (s1->dnNode()->whichDnLink(s2) < 0) return 0;
    return 1;
}


// Find the first control device in each segment

void
RoadNetwork::assignCtrlListInSegments()
{
    for (register int i = 0; i < nLinks(); i ++) {
        link(i)->assignCtrlListInSegments();
    }
}


// Find the first surveillance device in each segment

void
RoadNetwork::assignSurvListInSegments()
{
    for (register int i = 0; i < nLinks(); i ++) {
        link(i)->assignSurvListInSegments();
    }
}



void
RoadNetwork::initializeLinkStatistics()
{
    for (register int i = 0; i < nLinks(); i ++) {
        link(i)->initializeStatistics();
    }
}

void
RoadNetwork::resetLinkStatistics(int col, int ncols)
{
    for (register int i = 0; i < nLinks(); i ++) {
        link(i)->resetStatistics(col, ncols);
    }
}


// Calculate the commonality factors for route choice model

void RoadNetwork::calcPathCommonalityFactors()
{
    if (!thePathTable) return;

    updateProgress("Calculating path commonality factors ...");

    for (register int i = 0; i < nLinks(); i ++) {
        link(i)->calcPathCommonalityFactors();
    }
    // printPathCommonalityFactors();
}

void RoadNetwork::printPathCommonalityFactors(ostream &os)
{
    if (!thePathTable) return;

    os << "# Link: Path_1=CF1 Path_2=CF2 Path_n=CFn" << endl;

    for (register int i = 0; i < nLinks(); i ++) {
        link(i)->printPathCommonalityFactors(os);
    }
}

/*
 * This function should be called at the end of the simulation. It
 * writes the time-dependent link travel time in a format required by
 * RN_LinkTimes::read(), and can be used as input to the simulator
 * later.
 */

int
RoadNetwork::outputReloadableLinkTravelTimes(const char *filename)
{
    ofstream os(filename);
    if (!os.good()) return -1;

    int ncols = theGuidedRoute->infoPeriods();
    int col = theGuidedRoute->infoStartPeriod();
    int len = theGuidedRoute->infoPeriodLength();
    os << "% LINK TRAVEL TIMES" << endl;
    os << "% Generated on " << TimeTag() << endl;
    os << col
       << "\t% Start time period ("
       << theSimulationClock->convertTime(theGuidedRoute->infoStartTime())
       << ")" << endl;
    os << ncols << "\t% Number of periods" << endl;
    os << len << "\t% Seconds per period" << endl;
    os << endl << "% ID Type Length T-"
       << col << " ... T" << ncols-1 << endl;
    os << endl << OPEN_TOKEN << endl;

    for (register int i = 0; i < nLinks(); i ++) {
        link(i)->printReloadableTravelTimes(os);
    }

    os << CLOSE_TOKEN << endl;

    os.close();

    return 0;
}


/*
 * This function should be called at the end of the simulation
 */

int
RoadNetwork::outputLinkTravelTimes(const char *filename)
{
    // Open link travel time file

    ofstream os(filename);
    if (!os.good()) return -1;

    if (!theSimulationEngine->skipComment()) {
        int ncols = theGuidedRoute->infoPeriods();
        int col = theGuidedRoute->infoStartPeriod();
        int len = theGuidedRoute->infoPeriodLength();
        os << "% LINK TRAVEL TIMES" << endl;
        os << "% Generated on " << TimeTag() << endl;
        os << col << "\t% Start period ("
           << theSimulationClock->convertTime(theGuidedRoute->infoStartTime())
           << ")" << endl;
        os << ncols << "\t% Number of periods" << endl;
        os << len << "\t% Seconds per period" << endl;
        if (!theSimulationEngine->isRectangleFormat()) {
            os << "% ID Type Length {" << endl
               << "%   {Count-"
               << col << " Time-" << col << "} ... {Count-"
               << ncols-1 << " Time-" << ncols-1 << "}" << endl
               << "% }" << endl;
        }
    }

    for (register int i = 0; i < nLinks(); i ++) {
        link(i)->printTravelTimes(os);
    }

    os.close();
    return 0;
}


int
RoadNetwork::outputLinkFlowPlusTravelTimes(
    const char *filename, int col, int ncols)
{
    ofstream os(filename);
    if (!os.good()) return -1;

    int length = theGuidedRoute->infoPeriodLength(); 
    double begin_time = theGuidedRoute->infoStartTime() + col * length;
    double end_time = begin_time + ncols * length;

    os << "% SIMULATED LINK FLOW AND TRAVEL TIMES" << endl;
    os << "% For time period "
       << theSimulationClock->convertTime(begin_time) << '-'
       << theSimulationClock->convertTime(end_time)
       << endl;

    os << col << "\t% Start time period" << endl;
    os << ncols << "\t% Number of periods" << endl;
    os << length << "\t% Seconds per period" << endl;

    os << "% ID"
       << " C-" << col << " T-" << col << " ... "
       << " C-" << col+ncols-1 << " T-" << col+ncols-1 << endl;

    os << OPEN_TOKEN << endl;
    for (register int i = 0; i < nLinks(); i ++) {
        link(i)->printFlowPlusTravelTimes(os, col, ncols);
    }
    os << CLOSE_TOKEN << endl;
    os.close();
    return 0;
}


// Save network state for xdta or other postprocess viewers

void RoadNetwork::save_3d_state(int roll, int tm, const char *prefix)
{
    static State3D<float> *spd = NULL, *dsy = NULL;
    static State3D<int> *flw = NULL;
    static float *s = NULL, *d = NULL;
    static int *f = NULL;

    // Create spaces

    if (prefix) {					// creation

        if (s) delete [] s;
        if (d) delete [] d;
        if (f) delete [] f;
        if (spd) delete spd;
        if (dsy) delete dsy;
        if (flw) delete flw;

        const char *tag = ToolKit::outfile(prefix);

	// static int x =0;
	// x++;
        // char man = x;
        // cout <<"Manish : " << x << man ;
	// char *z = &man;
        // z = &man;
        // char *y = z;

        int sn = strlen(tag);
        char *fn = new char [sn+5];
        Copy(fn, tag);
        //Copy(fn+sn, z);
	
        Copy(fn+sn, ".spd");
        spd = new State3D<float>(fn, -1, nSegments());
        s = new float[nSegments()];

        Copy(fn+sn, ".dsy");
        dsy = new State3D<float>(fn, -1, nSegments());
        d = new float[nSegments()];

        Copy(fn+sn, ".flw");
        flw = new State3D<int>(fn, -1, nSegments());
        f = new int[nSegments()];

        delete [] fn;
        return;
    }

    // Get data

    RN_Segment *ps;
    for (int i = 0; i < nSegments(); i ++) {
        ps = segment(i);
        s[i] = ps->speed();
        d[i] = ps->density();
        f[i] = ps->flow();
    }

    // Write data

    spd->append(roll, tm-1, s);
    dsy->append(roll, tm-1, d);
    flw->append(roll, tm-1, f);
}

// Append files identified by prefix1 to files identified by prefix2

void RoadNetwork::append_3d_state(const char *prefix1, const char *prefix2)
{
    static State3D<float> *spd = NULL, *dsy = NULL;
    static State3D<int> *flw = NULL;

    const char *tag;
    int sn;
    char *fn;

    // Create spaces

    if (prefix2) {				// creation

        if (spd) delete spd;
        if (dsy) delete dsy;
        if (flw) delete flw;

        tag = ToolKit::outfile(prefix2);

        sn = strlen(tag);
        fn = new char [sn+5];
        Copy(fn, tag);

        Copy(fn+sn, ".spd");
        spd = new State3D<float>(fn, -1, nSegments());

        Copy(fn+sn, ".dsy");
        dsy = new State3D<float>(fn, -1, nSegments());

        Copy(fn+sn, ".flw");
        flw = new State3D<int>(fn, -1, nSegments());

        delete [] fn;
    }

    // copy data

    tag = ToolKit::outfile(prefix1);
    sn = strlen(tag);
    fn = new char [sn+5];
    Copy(fn, tag);

    Copy(fn+sn, ".spd");
    spd->append(fn);

    Copy(fn+sn, ".dsy");
    dsy->append(fn);

    Copy(fn+sn, ".flw");
    flw->append(fn);

    delete [] fn;
}

bool RoadNetwork::ExportSegmentAsMapInfo(const char* filename)
{
    const char* MapInfoHeader =
        "Version 2\n"
        "Delimiter \",\"\n"
        "CoordSys Earth Projection 1, 0\n"
        "Columns 4\n"
        "ID Integer\n"
        "Lanes Integer\n"
        "Grade Float\n"
        "Speed Integer\n"
        "Data\n";

    WcsPoint p(theWorldSpace->sw_pnt()) ;
    bool ok = false ;
    int i ;

    string keyfile = string(filename) + ".mip" ;
    string miffile = string(filename) + ".mif" ;
    string midfile = string(filename) + ".mid" ;
    keyfile = ToolKit::outfile(keyfile.c_str()) ;
    miffile = ToolKit::outfile(miffile.c_str()) ;
    midfile = ToolKit::outfile(midfile.c_str()) ;
    ofstream key(keyfile.c_str()) ;
    ofstream mif(miffile.c_str()) ;
    ofstream mid(midfile.c_str()) ;
    if (!key.good()) {
        fprintf(stderr, "Cannot write %s.\n", keyfile.c_str()) ;
    }
    key << "Coordinates in " << filename << ".mif should be:" << endl
        << "  multiplied by " << 1.0 / Parameter::lengthFactor() << " and" << endl
        << "  added to (" << p.x() << ", " << p.y() << ")" << endl ;
    key.close() ;
    if (!mif.good()) {
        fprintf(stderr, "Cannot write %s.\n", miffile.c_str()) ;
        goto done ;
    }
    if (!mid.good()) {
        fprintf(stderr, "Cannot write %s.\n", midfile.c_str()) ;
        goto done ;
    }
    mif << MapInfoHeader << endl ;
    for (i = 0; i < nSegments() ; i ++) {
        segment(i)->ExportMapInfo(mif, mid) ;
    }

    ok = true ;
 done:
    mif.close() ;
    mid.close() ;
    return ok ;
}


bool RoadNetwork::ExportSegmentAsDccText(const char* filename)
{
    const char* label[] = {
        "Speed" , "Density", "Flow"
    } ;
	int i ;

    // Create the dictionary file

    string dccfile = string(filename) + ".dcc" ;
    dccfile = ToolKit::outfile(dccfile.c_str()) ;
    ofstream dcc(dccfile.c_str()) ;
    if (!dcc.good()) {
        fprintf(stderr, "Cannot write %s.\n", dccfile.c_str()) ;
        return false ;
    }
    dcc << "Dictionary file for \"" << filename << ".txt\"\n" ;
    dcc << "0\n" ;
    dcc << "\"ID\",I,1,8,0,8,0\n" ;
    for (i = 0; i < 3; i ++) {
        dcc << "\"" << label[i] << "\",I," << i+2 << ",8,0,8,0\n" ;
    }
    dcc.close() ;
  
    // Create the data file

    string datfile = string(filename) + ".txt" ;
    datfile = ToolKit::outfile(datfile.c_str()) ;
    ofstream dat(datfile.c_str()) ;
    if (!dat.good()) {
        fprintf(stderr, "Cannot write %s.\n", datfile.c_str()) ;
        return false ;
    }

    for (i = 0; i < nSegments() ; i ++) {
        segment(i)->Export(dat) ;
    }

    dat.close() ;

    return true ;
}

int RoadNetwork::Export(int type)
{
    switch (type) {
    case 0:
        if (ExportSegmentAsMapInfo("seggeo")) return 0 ;
        else return -1 ;
    case 1:
        if (ExportSegmentAsDccText("segdat")) return 0 ;
        else return -1 ;
    case 2:
        if (ExportSegmentAsMapInfo("seggeo") &&
            ExportSegmentAsDccText("segdat")) {
            return 0 ;
        } else {
            return -1 ;
        }
    default:
        return type ;
    }
}

void RoadNetwork::setDrivingDirection (int k)
{

  drivingDirection_ =  k;

  cout << "rama: road network.h: k = " << k << " member = " << drivingDirection_ << endl;

}
