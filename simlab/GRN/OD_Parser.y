//
// OD_Parser.y -- OD trip table parser generator
// By Qi Yang
//

%name   OD_Parser
%define ERROR_BODY	DEF_ERROR_BODY
%define LEX_BODY	  DEF_LEX_BODY

// These macros are expanded when creating the definition for named
// BisonParser class.  The functions defined as member here are
// designed for use in the grammar rules.

%define CONSTRUCTOR_PARAM  OD_Table* pContainer_
%define CONSTRUCTOR_INIT   : _pContainer(pContainer_) \
                            ,_vtype(0)                \
             
%define MEMBERS DEF_MEMBERS            \
    OD_Table* _pContainer ;            \
    int _vtype ;                        

%union {
  long        type_i ;
  double      type_f ;
  const char* type_s ;
}

%token <type_s> LEX_EQUAL
%token <type_s> LEX_COMMA
%token <type_s> LEX_COLON
%token <type_s> LEX_SEMICOLON
%token <type_s> LEX_OAB
%token <type_s> LEX_CAB
%token <type_s> LEX_OBB
%token <type_s> LEX_CBB
%token <type_s> LEX_OCB
%token <type_s> LEX_CCB
%token <type_i> LEX_INT
%token <type_f> LEX_REAL
%token <type_s> LEX_TIME
%token <type_s> LEX_PAIR
%token <type_s> LEX_TAG
%token <type_s> LEX_STRING
%token <type_s> LEX_DUP
%token <type_s> LEX_END

%header{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <IO/Exception.h>

#include <Tools/Scanner.h>  
#include <Tools/ToolKit.h>  
#include <Tools/SimulationClock.h>
#include <Tools/Math.h>
#include <GRN/OD_Table.h>
#include <GRN/OD_Cell.h>
#include <GRN/Constants.h>
%}

//--------------------------------------------------------------------
// Symbols used road network database grammer rules.
//--------------------------------------------------------------------

%type <type_i> inum
%type <type_f> fnum float_num time string_time
	   		 
%type <type_s> everything multi_odtables odcell_list
%type <type_s> odtable odtable_data
%type <type_s> splits multi_splits split
%type <type_s> multi_odcells odcell
%type <type_i> odcell_data_3 odcell_data_4
%type <type_i> odcell_data_5 odcell_data_7
%type <type_s> path multi_paths paths the_end

%start everything

%%

//--------------------------------------------------------------------
// Beginniing of OD table grammer rules.
//--------------------------------------------------------------------

everything: multi_odtables the_end
| odcell_list the_end
| odcell_list multi_odtables the_end
;

the_end: LEX_END
{
  _pContainer->init(DBL_INF);
  if (ToolKit::verbose()) {
	cout << "Finished parsing <" << filename() << ">." << endl;
  }
  YYACCEPT;			// return and never back
}
;

multi_odtables: odtable
| multi_odtables odtable
;

odtable: odtable_data odcell_list
;

odcell_list: LEX_OCB multi_odcells LEX_CCB
;

odtable_data: time inum fnum
{
  // $1=StartTime $2=VehicleType $3=ScalingFactor
  _vtype = $2;
  int err = _pContainer->init($1, $2, $3); 
  check(err);
  if (_pContainer->nextTime() > theSimulationClock->currentTime()) {
	YYACCEPT;			// return and will come back later
  }
}
;

multi_odcells: odcell
| multi_odcells odcell
;

odcell: LEX_OCB odcell_data_3 LEX_CCB | LEX_OCB odcell_data_3 paths LEX_CCB 
| LEX_OCB odcell_data_4 LEX_CCB | LEX_OCB odcell_data_4 paths LEX_CCB
| LEX_OCB odcell_data_5 LEX_CCB | LEX_OCB odcell_data_5 paths LEX_CCB
| LEX_OCB odcell_data_7 LEX_CCB | LEX_OCB odcell_data_7 paths LEX_CCB
{
  check($2);
}
;

odcell_data_7: inum inum fnum fnum fnum inum inum
{
  // $1=Ori $2=Des $3=Rate $4=Variance $5=Distribution $6=busType $7=runID
  OD_Cell *ptr = _pContainer->newOD_Cell();
  $$ = ptr->initBRT($1, $2, $3, $4, $5, $6, $7);
}
;

odcell_data_5: inum inum fnum fnum fnum
{
  // $1=Ori $2=Des $3=Rate $4=Variance $5=Distribution
  OD_Cell *ptr = _pContainer->newOD_Cell();
  $$ = ptr->init($1, $2, $3, $4, $5);
}
;

odcell_data_4: inum inum fnum fnum
{
  // $1=Ori $2=Des $3=Rate $4=Variance
  OD_Cell *ptr = _pContainer->newOD_Cell();
  $$ = ptr->init($1, $2, $3, $4, 0.0);
}
;

odcell_data_3: inum inum fnum
{
  // $1=Ori $2=Des $3=Rate
   
  OD_Cell *ptr = _pContainer->newOD_Cell();
  $$ = ptr->init($1, $2, $3, 0.0, 0.0);
}
;

paths: LEX_OCB LEX_CCB | LEX_OCB multi_paths LEX_CCB |
LEX_OCB multi_paths LEX_CCB splits
;

multi_paths: path
| multi_paths path
;

path: inum
{
  // $1=PathID
  int err_no = OD_Cell::workingCell()->addPath($1);
  if (err_no) {
	fprintf(stderr, "%s:%d (%d): Path \"%d\" not found and ignored.",
			filename(), lineno(), err_no, $1) ;
  }
}
;

splits: LEX_OCB LEX_CCB | LEX_OCB multi_splits LEX_CCB
{
	int n = OD_Cell::workingCell()->nPaths() ;
	if (OD_Cell::nSplits() != n) {
		fprintf(stderr, "%s:%d: Number of splits (%d) != paths (%d).",
				filename(), lineno(), OD_Cell::nSplits(), n) ;
		theException->exit(-4) ;
	}
	double sum = OD_Cell::workingCell()->split(n-1) ;
	if (!AproxEqual(sum, 1.0)) {
		fprintf(stderr, "%s:%d: Sum of splits (%.4lf) != 1.0.",
				filename(), lineno(), sum) ;
		theException->exit(-5) ;
	}
}
;

multi_splits: split
| multi_splits split
;

split: fnum
{
	int err_no = OD_Cell::workingCell()->setSplit($1);
	if (err_no) {
		fprintf(stderr, "%s:%d: Bad split \"%.4lf\" (%d).",
				filename(), lineno(), $1, err_no) ;
		theException->exit(err_no) ;
	}
}

//--------------------------------------------------------------------
// Beginning of basic grammer rules.
//--------------------------------------------------------------------

inum: LEX_INT
{
  char *ptr;
  $$ = (int)strtol(value(), &ptr, 0);
}
;

fnum: float_num
| inum
{
  $$ = $1;
}
;

float_num: LEX_REAL
{
  $$ = atof(value());
}
;

time: string_time
| fnum
{
  $$ = $1;
}
;

string_time: LEX_TIME
{
  $$ = theSimulationClock->convertTime(value());
}
;

%%

// Following pieces of code will be verbosely copied into the parser.

%header{
  // empty
%}
