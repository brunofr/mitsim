/*-*-c++-*------------------------------------------------------------
 *  OD_Scanner.lex -- OD trip table lexical analyzer generator
 *--------------------------------------------------------------------
 */

%{
#include "OD_Parser.h"
#define YY_BREAK
%}

%option yyclass="OD_FlexLexer"
%option noyywrap
%option yylineno

C	(#.*)|(%.*)|("//".*)
WS	[\t ,;\n]+

D	[0-9]
H       [0-9A-Fa-f]
E	[eE][-+]?{D}+
N	{D}+
X       {H}+
F1	{D}+\.{D}*({E})?
F2	{D}*\.{D}+({E})?
F3	{D}+({E})?

HEXTAG  ("0x")|("0X")|("x")|("X")

I	[-+]?({N}|({HEXTAG}{X}))
R	[-+]?({F1}|{F2}|{F3})

HH	(([01]?[0-9])|([2][0-3]))
MM	([0-5]?[0-9])
SS	([0-5]?[0-9])
TIME	{HH}[:]{MM}[:]{SS}

OB	{WS}?[{]{WS}?
CB	{WS}?[}]{WS}?

END     ([<]"END"[>])

%%

"/*"	{	/* skip comments */
  int c;
  while ((c = yyinput()) != 0) {
    if (c == '*') {
      if ((c = yyinput()) == '/') break;
      else unput(c);
    }
  }
  break;
}

{WS}	{ break; }
{C}	{ break; }

{OB}	{ return OD_Parser::OD_OB; }
{CB}	{ return OD_Parser::OD_CB; }
		            
{I}	{ return OD_Parser::OD_INT; }
{R}	{ return OD_Parser::OD_REAL; }
{TIME}  { return OD_Parser::OD_TIME; }
{END}   { return OD_Parser::OD_END; }

%%
