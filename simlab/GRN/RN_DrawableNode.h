//-*-c++-*------------------------------------------------------------
// NAME: Drawable Road Network Node
// AUTH: Qi Yang
// FILE: RN_DrawableNode.h
// DATE: Sat Feb  3 17:35:07 1996
//--------------------------------------------------------------------

#ifndef RN_DRAWABLENODE_HEADER
#define RN_DRAWABLENODE_HEADER

#include "WcsPoint.h"
#include "RN_Node.h"

class RN_DrawableNode : public RN_Node
{
   protected:

      WcsPoint loc_;

   public:

      RN_DrawableNode() : RN_Node() { }
      virtual ~RN_DrawableNode() { }

      void calcGeometricData();
      WcsPoint & loc() { return loc_; }
};

#endif
