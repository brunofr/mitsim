//
// RN_BusStopPrmParser.y
// by Qi Yang & Daniel Morgan
//

%name   RN_BusStopPrmParser
%define ERROR_BODY	DEF_ERROR_BODY
%define LEX_BODY	DEF_LEX_BODY

%define CONSTRUCTOR_PARAM  RN_BusStopPrmTable* pContainer_
%define CONSTRUCTOR_INIT   : _pContainer(pContainer_)   \
                           ,_bsid(0)                    \

%define MEMBERS  DEF_MEMBERS                            \
   RN_BusStopPrmTable* _pContainer ;                  \
   int _bsid ;                                          \

%union {
  long        type_i ;
  double      type_f ;
  const char* type_s ;
}

%token <type_s> LEX_EQUAL
%token <type_s> LEX_COMMA
%token <type_s> LEX_COLON
%token <type_s> LEX_SEMICOLON
%token <type_s> LEX_OAB
%token <type_s> LEX_CAB
%token <type_s> LEX_OBB
%token <type_s> LEX_CBB
%token <type_s> LEX_OCB
%token <type_s> LEX_CCB
%token <type_i> LEX_INT
%token <type_f> LEX_REAL
%token <type_s> LEX_TIME
%token <type_s> LEX_PAIR
%token <type_s> LEX_TAG
%token <type_s> LEX_STRING
%token <type_s> LEX_DUP
%token <type_s> LEX_END

%header{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <IO/Exception.h>
#include <Tools/ToolKit.h>  
#include <Tools/Scanner.h>  
#include <Tools/SimulationClock.h>
#include <Tools/Math.h>

#include <GRN/RN_BusStopPrmTable.h>
%}

//--------------------------------------------------------------------
// Symbols used vehicle table grammer rules.
//--------------------------------------------------------------------

%type <type_f> fnum float_num time string_time
%type <type_s> everything multi_param_tables param_list the_end
%type <type_s> param_table param_table_data rate_data dwell_bounds
%type <type_s> bsid stop_without_id one_stop
%type <type_s> multi_stops multi_stops_without_id arrival_data
%type <type_i> inum

%start everything

%%

//--------------------------------------------------------------------
// Beginning of RN_BusStopPrmTable table grammer rules.
//--------------------------------------------------------------------

everything: multi_param_tables the_end
;

the_end: LEX_END
{
  _pContainer->initBusStopPrms(DBL_INF);
  if (ToolKit::verbose()) {
	cout << "Finished parsing <" << filename() << ">." << endl;
  }
  YYACCEPT;			// return and never back
}
;

multi_param_tables: param_table
| multi_param_tables param_table
;

param_table: param_table_data param_list
;

param_list: LEX_OCB multi_stops LEX_CCB
;

param_table_data: time
{
  // $1=StartTime
  int err_no = _pContainer->initBusStopPrms($1); 
  check(err_no);
  if (_pContainer->nextTime() > theSimulationClock->currentTime()) {
	YYACCEPT;					// return and will come back later
  }
}
;

multi_stops: one_stop
| multi_stops one_stop
;

one_stop: bsid multi_stops_without_id
;

bsid: inum
{
  if (ToolKit::debug()) {
	cout << $1 << endc;
  }
  _bsid = $1;
}
;

multi_stops_without_id: stop_without_id
| multi_stops_without_id stop_without_id
;

stop_without_id: LEX_OCB dwell_bounds LEX_CCB
| LEX_OCB rate_data LEX_CCB
| LEX_OCB arrival_data LEX_CCB
;

dwell_bounds: fnum fnum
{
  //$1 = dwell time lower bound, $2 = dwell time upper bound
  if (ToolKit::debug()) {
	cout << '{' << $1 << endc << $2 << '}';
  }

  _pContainer->assignDwellBounds( _bsid, $1, $2 );
}
;

rate_data: inum fnum fnum
{
  //$1 = route id  $2 = avg arrival rate  $3 = alighing percentage
  if (ToolKit::debug()) {
	cout << '{' << $1 << endc << $2 << endc << $3 << '}';
  }

  int err_no = _pContainer->assignRateParams(_bsid, $1, $2, $3);
  check(err_no);
}
;

arrival_data: inum fnum fnum fnum fnum fnum fnum
{
  //$1 = route id  $2 = start time  $3 = interval a
  //$4 = interval b  $5 = interval c  $6 = peak  $7 = alighting percentage
  if (ToolKit::debug()) {
	cout << '{' << $1 << endc << $2 << endc << $3
             << $4 << endc << $5 << endc << $6 << endc << $7 << '}';
  }

  int err_no = _pContainer->assignArrivalParams(_bsid, $1, $2, $3, $4, $5, $6, $7);
  check(err_no);
}

//--------------------------------------------------------------------
// Beginning of basic grammer rules.
//--------------------------------------------------------------------

inum: LEX_INT
{
  char *ptr;
  $$ = (int)strtol(value(), &ptr, 0);
}
;

fnum: float_num
| inum
{
  $$ = $1;
}
;

float_num: LEX_REAL
{
  $$ = atof(value());
}
;

time: string_time
| fnum
{
  $$ = $1;
}
;

string_time: LEX_TIME
{
  $$ = theSimulationClock->convertTime(value());
}
;

%%

// Following pieces of code will be verbosely copied into the parser.

%header{
  // empty
%}
