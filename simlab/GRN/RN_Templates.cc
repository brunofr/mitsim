//-*-c++-*------------------------------------------------------------
// NAME: Explicitly instantiate all the template instances we use
// AUTH: Qi Yang
// FILE: fiTemplates.C
// DATE: Thu Apr 18 23:17:31 1996
//--------------------------------------------------------------------

#ifdef FORCE_INSTANTIATE

#include "RN_SurvStation.h"
#include "RN_CtrlStation.h"
#include "RN_Label.h"
#include "RN_Sensor.h"
#include "RN_Link.h"
#include "RN_Node.h"
#include "RN_Segment.h"
#include "RN_Lane.h"
#include "RN_Signal.h"
#include "RN_Path.h"
#include "RN_PathPointer.h"
#include "RN_LinkTimes.h"
#include "OD_Cell.h"
#include "OD_Pair.h"

#include <Templates/Pointer.h>
#include <Templates/Array.h>
#include <Templates/Listp.h>
#include <Templates/Graph.h>
#include <Templates/DiskData.h>

#ifdef __sgi

#pragma instantiate vector<float>
#pragma instantiate vector<int>
#pragma instantiate Graph<float,RN_LinkTimes>
#pragma instantiate GraphNode<float,RN_LinkTimes>
#pragma instantiate GraphLink<float,RN_LinkTimes>
#pragma instantiate vector<GraphLink<float, RN_LinkTimes> >
#pragma instantiate vector<GraphNode<float, RN_LinkTimes> >

#pragma instantiate DiskData<int>
#pragma instantiate DiskData<float>
#pragma instantiate DiskData<short>

#pragma instantiate ListpNode<OD_Cell*>
#pragma instantiate Listp<OD_Cell*>

#pragma instantiate ListpNode<RN_SurvStation*>
#pragma instantiate Listp<RN_SurvStation*>

#pragma instantiate ListpNode<RN_CtrlStation*>
#pragma instantiate Listp<RN_CtrlStation*>

#pragma instantiate Pointer<OD_Pair>
#pragma instantiate Pointer<RN_PathPointer>
#pragma instantiate vector<Pointer<RN_PathPointer> >

#endif // __sgi

#ifdef __GNUC__

template class vector<int>;
template class Graph<float,RN_LinkTimes>;
template class GraphNode<float,RN_LinkTimes>;
template class GraphLink<float,RN_LinkTimes>;
template class vector<GraphLink<float, RN_LinkTimes> >;
template class vector<GraphNode<float, RN_LinkTimes> >;

template class DiskData<int>;
template class DiskData<float>;
template class DiskData<short>;

template class ListpNode<OD_Cell*>;
template class Listp<OD_Cell*>;

template class ListpNode<RN_SurvStation*>;
template class Listp<RN_SurvStation*>;

template class ListpNode<RN_CtrlStation*>;
template class Listp<RN_CtrlStation*>;

template class Pointer<OD_Pair>;
template class Pointer<RN_PathPointer>;

#endif  // __GNUC__

#endif // FORCE_INSTANTIATE
