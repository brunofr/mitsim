//
// RN_BusScheduleParser.y -- RN_BusSchedule table parser generator
// by Qi Yang & Daniel Morgan
//

%name   RN_BusScheduleParser
%define ERROR_BODY	DEF_ERROR_BODY
%define LEX_BODY	  DEF_LEX_BODY

// These macros are expanded when creating the definition for named
// BisonParser class.  The functions defined as member here are
// designed for use in the grammar rules.

%define CONSTRUCTOR_PARAM  RN_BusScheduleTable* pContainer_
%define CONSTRUCTOR_INIT   : _pContainer(pContainer_)     \
                           , _routeID(0)                  \
                           , _tripID(0)                   \
                           , _i(0)                        \

%define MEMBERS            DEF_MEMBERS \
    RN_BusScheduleTable* _pContainer ; \
    int _routeID ;                     \
    int _tripID ;                      \
    int _i ;                           \

%union {
  long        type_i ;
  double      type_f ;
  const char* type_s ;
}

%token <type_s> LEX_EQUAL
%token <type_s> LEX_COMMA
%token <type_s> LEX_COLON
%token <type_s> LEX_SEMICOLON
%token <type_s> LEX_OAB
%token <type_s> LEX_CAB
%token <type_s> LEX_OBB
%token <type_s> LEX_CBB
%token <type_s> LEX_OCB
%token <type_s> LEX_CCB
%token <type_i> LEX_INT
%token <type_f> LEX_REAL
%token <type_s> LEX_TIME
%token <type_s> LEX_PAIR
%token <type_s> LEX_TAG
%token <type_s> LEX_STRING
%token <type_s> LEX_DUP
%token <type_s> LEX_END

%header{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <IO/Exception.h>

#include <Tools/Scanner.h>
#include <Tools/ToolKit.h>  

#include <GRN/RN_BusScheduleTable.h>
#include <GRN/RN_BusSchedule.h>
%}

//--------------------------------------------------------------------
// Symbols used road network database grammer rules.
//--------------------------------------------------------------------

%type <type_s> block_name
%type <type_i> inum num_objects specified unspecified
%type <type_f> fnum float_num
	   		 
%type <type_s> bus_schedules schedule_table_allocator
%type <type_s> multi_bus_schedules bus_schedule
%type <type_s> multi_bus_stops bus_stop stop_data trip_id bus_data
%type <type_s> multi_arrivals arrival route_id arrival_data
%type <type_s> multi_trips trip

%start everything

%%

//--------------------------------------------------------------------
// Beginning of RN_BusSchedule table grammer rules.
//--------------------------------------------------------------------

everything: bus_schedules
{
  if (ToolKit::debug()) {
	cout << "Finished parsing bus schedule table <"
		 << filename() << ">." << endl;
  }
}
;

bus_schedules: block_name schedule_table_allocator LEX_OCB LEX_CCB
| block_name schedule_table_allocator LEX_OCB multi_bus_schedules LEX_CCB
{
  int n = _pContainer->nBusSchedules();
  if (ToolKit::debug()) {
	cout << indent << n << " parsed." << endl;
  }
}
;

schedule_table_allocator: num_objects
{
  int n = ($1 > 0) ? ($1) : (NUM_BUSSCHEDULES);
  _pContainer->busschedules_.reserve(n);
}
;

multi_bus_schedules: bus_schedule
| multi_bus_schedules bus_schedule
;

bus_schedule: LEX_OCB route_id multi_trips stop_data LEX_CCB
| LEX_OCB route_id multi_trips LEX_CCB
| bus_data
{
  if(_i) _i = 0;
}
;

multi_trips: trip
| multi_trips trip
;

bus_data: LEX_OCB route_id trip_id LEX_CCB
;

route_id: inum
{
  // $1 = RouteID
  _routeID = $1;
}
;

trip: trip_id arrival_data
;

trip_id: inum
{
  // $1 = TripID
  _tripID = $1;
  int err_no = _pContainer->addBusSchedule($1);
  check(err_no);
  _pContainer->lastBusSchedule()->routeID_ = _routeID;

}
;

stop_data: LEX_OCB multi_bus_stops LEX_CCB
{
  _i = 0;
}
;

multi_bus_stops: bus_stop
| multi_bus_stops bus_stop
;

bus_stop: inum
{
  // $1=busStopID

  if (_i) { 
    for (int j = (_i-1); j > -1; j -- ) {
      int err_no = _pContainer->busschedule(_pContainer->lastBusSchedule()->index()-j)->addBusStop($1);
      check(err_no);
    }
  }
}
;

arrival_data: LEX_OCB multi_arrivals LEX_CCB
{
  _i++;
}
;

multi_arrivals: arrival
| multi_arrivals arrival
;

arrival: fnum
{
  // $1=scheduledArrivalTime
  int err_no = _pContainer->lastBusSchedule()->addArrival($1);
  check(err_no);
}
;

//--------------------------------------------------------------------
// Beginning of basic grammer rules.
//--------------------------------------------------------------------

block_name: LEX_TAG
{
  const char *s = value("[]");
  if (ToolKit::debug()) {
    cout << "Parsing " << s << " ..." << endl;
  }
  $$ = s;
}
;

inum: LEX_INT
{
  $$ = atoi(value());
}
;

fnum: float_num
| inum
{
  $$ = $1;
}
;

float_num: LEX_REAL
{
  $$ = atof(value());
}
;

num_objects: specified
| unspecified
{
  $$ = $1;
}
;
 
specified: LEX_COLON inum
{
  $$ = $2;
}
;

unspecified: LEX_COLON
{
  $$ = 0;
}
;

%%

// Following pieces of cpte will be verbosely copied into the parser.

%header{
  // empty
%}
