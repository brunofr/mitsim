//-*-c++-*------------------------------------------------------------
// FILE: RN_Segment.C
// AUTH: Qi Yang
// DATE: Wed Oct 18 17:38:20 1995
//--------------------------------------------------------------------

#include <Tools/Math.h>
#include <Tools/ToolKit.h>

#include "RoadNetwork.h"
#include "Parameter.h"
#include "RN_Node.h"
#include "RN_Link.h"
#include "RN_Segment.h"
#include "RN_Lane.h"
#include "RN_SdFn.h"
#include "RN_CtrlStation.h"
#include "RN_SurvStation.h"
using namespace std;

int RN_Segment::sorted_ = 1;

#ifdef EXTERNAL_MOE
TimeInterval RN_Segment::samplingInterval_= TimeInterval(0.0,0.0);
double RN_Segment::samplingStepSize_=0.0;
double RN_Segment::reportStepSize_ = 0.0;
#endif

// Index within the link.  0 is the upstream.

int
RN_Segment::localIndex()
{
   return (index_ - link_->startSegment()->index_);
}


// Type of the linke

int
RN_Segment::type()
{
   return (link_->type());
}


// Previous element in the global array of segments

RN_Segment* 
RN_Segment::prev()
{
   if (index_ > 0) 
      return theNetwork->segment(index_ - 1);
   else return (NULL);
}

// Next element in the global array of segments

RN_Segment* 
RN_Segment::next()
{
   if (index_ < theNetwork->nSegments() - 1)
      return theNetwork->segment(index_ + 1);
   else return (NULL);
}

// Returns the upstream segment in the same link

RN_Segment *
RN_Segment::upstream()
{
   if (index_ != link_->startSegmentIndex())
      return theNetwork->segment(index_ - 1);
   else return (NULL);
}


// Returns the downstream segment in the same link

RN_Segment *
RN_Segment::downstream()
{
   if (index_ != link_->startSegmentIndex() +
       link_->nSegments() - 1)
      return theNetwork->segment(index_ + 1);
   else return (NULL);
}


// Returns the left most lane in the segment

RN_Lane *
RN_Segment::leftLane()
{
   return theNetwork->lane(leftLane_);
}

// Returns the right most lane in the segment

RN_Lane *
RN_Segment::rightLane()
{
   return theNetwork->lane(leftLane_ + nLanes_ - 1);
}

// Returns the ith lane in the segment

RN_Lane *
RN_Segment::lane(int i)
{
   return theNetwork->lane(leftLane_ + i);
}

// Returns the central lane in the segment

RN_Lane *
RN_Segment::centralLane()
{
   return theNetwork->lane(leftLane_ + nLanes_ / 2);
}


// Set segment variables. Called bu RN_Parser

int
RN_Segment::init(int id, unsigned int speed_limit,
				 float speed, float grd)
{
   if (ToolKit::debug()) {
      cout << indent << indent << "<"
		   << id << endc << speed_limit << endc
		   << speed << endc << grd << ">" << endl;
   }

   static int last = -1;
   if (link_) {
      cerr << "Error:: Segment <" << id << "> "
		   << "cannot be initialized twice. ";
      return -1;
   } else {
      link_ = theNetwork->lastLink();
   }
   code_ = id;

   if (sorted_ && code_ <= last) sorted_ = 0;
   else last = code_;

   speedLimit_ = speed_limit;
   freeSpeed_ = speed * Parameter::speedFactor();
   grade_ = grd;

   index_ = theNetwork->nSegments();
   nLanes_ = 0;
   leftLane_ = theNetwork->nLanes();
   theNetwork->addSegment(this);

   theNetwork->lastLink()->nSegments_ ++;

   return 0;
}


int
RN_Segment::initArc(double x1, double y1,
					double b,
					double x2, double y2)
{
   if (ToolKit::debug()) {
      cout << indent << indent << "<"
		   << x1 << endc << y1 << endc
		   << b << endc
		   << x2 << endc << y2
		   << ">" << endl;
   }
   int ans = RN_Arc::initArc(x1, y1, b, x2, y2);
   theNetwork->worldSpace().recordExtremePoints(startPnt_);
   theNetwork->worldSpace().recordExtremePoints(endPnt_);
   return ans;
}


// Snap the coordinates of arcs at the upstream end.

void
RN_Segment::snapCoordinates()
{
   RN_Segment* ups = upstream();

   if (ups)			// not the first segment in the link
   {
      // Find the middle point

      WcsPoint p(ups->endPnt(), startPnt(), 0.5);

      // Snap to the middle point

      ups->endPnt(p);
      startPnt(p);
   }
}


void
RN_Segment::calcArcInfo(RN_WorldSpace & world_space)
{
   startPnt_ = world_space.worldSpacePoint(startPnt_);
   endPnt_ = world_space.worldSpacePoint(endPnt_);
   computeArc();
}


void
RN_Segment::print(ostream &os)
{
   os << indent << indent << "{";
   os << code_ << endc
      << speedLimit_ << endc
      << freeSpeed_ / Parameter::speedFactor() << endc
      << grade_;

   printSdIndex(os);
   os << endl;

   arcprint(os);

   printLanes(os);

   os << indent << indent << "}" << endl;
}


void
RN_Segment::printLanes(ostream &os)
{
   os << indent << indent << indent;
   for (register int i = 0; i < nLanes_; i ++) {
      lane(i)->print(os);
   }
   os << endl;
}


// Calculate the data that do not change in the simulation.

void
RN_Segment::calcStaticInfo(void)
{
   if (!downstream()) {
	  localType_ |= SEGMENT_TYPE_HEAD;
	  if (link()->nDnLinks() < 1 ||
		  link()->dnNode()->type(NODE_TYPE_EXTERNAL)) {
		 localType_ |= SEGMENT_TYPE_THE_END;
	  }
   }
   if (!upstream()) {
	 localType_ |= SEGMENT_TYPE_TAIL;
	 if (link()->nUpLinks() < 1 ||
		  link()->upNode()->type(NODE_TYPE_EXTERNAL)) {
		 localType_ |= SEGMENT_TYPE_THE_BEGINNING;
	 }
   }
   theNetwork->totalLinkLength_ += length_;
   theNetwork->totalLaneLength_ += length_ * nLanes_;

   if (sdIndex_ < 0 || sdIndex_ >= theNetwork->nSdFns()) {
	 cerr << "Segment " << code_ << " has invalid sdIndex "
		  << sdIndex_ << "." << endl;
	 sdIndex_ = 0;
   }
}


float
RN_Segment::calcCurrentTravelTime(void)
{
   const float min_spd = 2.22;	// 5 mph

   if (length_ < 50.0) {

	  // Speed density function will not work for very short segment

	  return (length_ / freeSpeed_);
   }

   RN_SdFn *sdf = theNetwork->sdFn(sdIndex_);
   float spd = sdf->densityToSpeed(freeSpeed(), calcDensity(), nLanes());

   if (spd < min_spd) spd = min_spd;
   return (length_ / spd);
}

void RN_Segment::markConnectedUpLanes()
{
   for (int i = 0; i < nLanes(); i ++) {
	  lane(i)->markConnectedUpLanes(1 << i);
   }
}

void RN_Segment::unmarkConnectedUpLanes()
{
   for (int i = 0; i < nLanes(); i ++) {
	  lane(i)->unmarkConnectedUpLanes(1 << i);
   }
}

void RN_Segment::markConnectedDnLanes()
{
   for (int i = 0; i < nLanes(); i ++) {
	  lane(i)->markConnectedDnLanes(1 << i);
   }
}

void RN_Segment::unmarkConnectedDnLanes()
{
   for (int i = 0; i < nLanes(); i ++) {
	  lane(i)->unmarkConnectedDnLanes(1 << i);
   }
}

void RN_Segment::ExportMapInfo(ostream& mif, ostream& mid)
{
  if (isArc()) {
    double r = radius() ;
    WcsPoint p1 = center().bearing(r, 0.75 * M_PI) ;
    WcsPoint p2 = center().bearing(r, -0.25 * M_PI) ;
//#define EXPORT_AS_ARC
#if defined(EXPORT_AS_ARC)
    double a1 = startAngle() * DEGREE_PER_RADIAN ;
    double a2 = endAngle() * DEGREE_PER_RADIAN ;
    double b1, b2 ;
    if (a1 > 180 && a2 < 180) {
        // counter clockwise and cross 0 line
        b1 = a1 ; b2 = a2 ;
    } else if (a1 < 180 && a2 > 180) {
        // clockwise and cross 0 line
        b1 = a2 ; b2 = a1 ;
    } else if (a1 < a2) {
        // counter clockwise 
        b1 = a1 ; b2 = a2 ;
    } else {
        // clockwise
        b1 = a2 ; b2 = a1 ;
    }
    mif << "Arc "
        << p1.x() << endc << p1.y() << endc
        << p2.x() << endc << p2.y() << endc
        << b1 << endc << b2 << endl ;
#else
	double shell = 0.1 ;
    int i, n ;
	int m = (int) ceil(length() / shell);
	double step = acos(1.0 - shell / radius());
	n = (int) ceil(Abs(arcAngle() / step));
	if (n < 1) {
	  n = 1;
	} else if (n > m) {
	  n = m;
	}
	step = 1.0 / n;
    mif << "Pline " << n + 1 << endl ;
    for (i = 0; i <= n; i ++) {
        WcsPoint p = centerPoint(i * step);
        mif << p.x() << endc << p.y() << endl ;
    }
#endif // !EXPORT_AS_ARC
  } else {
    mif << "Line " ;
    mif << startPnt_.x() << endc << startPnt_.y() << endc ;
    mif << endPnt_.x() << endc << endPnt_.y() << endl ;
  }

  mid << code_ << ','
      << nLanes() << ','
      << grade_ << ','
      << Round(freeSpeed_ / Parameter::speedFactor()) << endl;
}

void RN_Segment::Export(ostream& os)
{
    os << code_ << ','
       << Round(speed() / Parameter::speedFactor()) << ','
       << Round(density() / Parameter::densityFactor()) << ','
       << Round(flow() / Parameter::densityFactor()) << endl ;
}
