//-*-c++-*------------------------------------------------------------
// NAME: Microscopic Traffic Simulator
// AUTH: Qi Yang
// FILE: RN_Link.h
// DATE: Fri Mar  1 16:18:20 1996
//--------------------------------------------------------------------

#ifndef RN_LINK_HEADER
#define RN_LINK_HEADER

#include <iostream>
#include <vector>

#include "RN_PathPointer.h"
#include "RN_CtrlStation.h"
#include "RN_SurvStation.h"
#include "Constants.h"

#include <Templates/Listp.h>
#include <Templates/Object.h>

enum {
  LINK_TYPE_FREEWAY = 1,
  LINK_TYPE_RAMP    = 2,
  LINK_TYPE_URBAN   = 3
};

const int LINK_TYPE_MASK = 7;
const int IN_TUNNEL_MASK = 8;

class RoadNetwork;
class RN_BisonParser;
class RN_Node;
class RN_Segment;
class RN_Label;
class RN_CtrlStation;
class RN_SurvStation;
class RN_Vehicle;

class RN_Link : public CodedObject
{
  friend class RN_BisonParser;
  friend class RoadNetwork;
  friend class RN_Segment;

protected:

  typedef Listp<RN_SurvStation*> surv_list_type;
  typedef Listp<RN_CtrlStation*> ctrl_list_type;
  typedef surv_list_type::link_type surv_list_link_type;
  typedef ctrl_list_type::link_type ctrl_list_link_type;
  typedef surv_list_link_type SurvList;
  typedef ctrl_list_link_type CtrlList;

protected:

  static int sorted_;	// 1=sorted 0=arbitrary order

  int index_;		// index in the array
  RN_Label* label_;		// link label
  int type_;		// link type
  RN_Node* upNode_;		// pointer to upstream node
  RN_Node* dnNode_;		// pointer to dnstream node
  short int nSegments_;	// number of segments
  int startSegment_;	// index to the start segment

  // THESE VARIABLES HELP TO REPRESENT TOPOLOGY AND SPEED UP SOME
  // CALCULATION.

  // rightDnIndex_ is the position of the right most downstream
  // link in the array dnLinks of dnNode

  unsigned char rightDnIndex_;

  // dnIndex_ is the position of this link in the array dnLinks of
  // the upNode

  unsigned char dnIndex_;

  // Each bit of this variable represents a connection to dnLink

  unsigned char dnLegal_;	// 0=turn is prohibited

  // upIndex_ is the position of this link in the array upLinks of
  // the dnNode

  unsigned char upIndex_;

  unsigned int laneUseRules_;

  // Control and surveillance devices

  ctrl_list_type ctrlStationList_;
  surv_list_type survStationList_;

  double length_;		// length of the link
  double travelTime_;	// latest travel time
  double freeSpeed_;	// free flow speed

  unsigned int state_;

  std::vector<RN_PathPointer *> pathPointers_;

  // Link travel time

  static int nPeriods_;
  static int nSecondsPerPeriod_;

  // These functions are used to record the travel time vehicles
  // spend in each link, aggregated by the time they enter the
  // link.

  int   *nSamplesTravelTimeEnteringAt_;
  float *sumOfTravelTimeEnteringAt_;
      
public:

  RN_Link();
  virtual ~RN_Link();

  static inline int sorted() { return sorted_; }
  int index() { return index_; }

  inline unsigned int state(unsigned int mask = 0xFFFF) {
	return (state_ & mask);
  }
  inline void setState(unsigned int s) {
	state_ |= s;
  }
  inline void unsetState(unsigned int s) {
	state_ &= ~s;
  }

  inline int type() { return type_; }
  inline int linkType() { return type_ & LINK_TYPE_MASK; }
  inline int isInTunnel() { return type_ & IN_TUNNEL_MASK; }

  inline RN_Node* upNode() { return upNode_; }
  inline RN_Node* dnNode() { return dnNode_; }

  inline short int nSegments() { return nSegments_; }

  inline int startSegmentIndex() { return startSegment_; }
  RN_Segment* segment(int i);
  RN_Segment* startSegment();
  RN_Segment* endSegment();

  int nUpLinks();
  int nDnLinks();
  RN_Link* upLink(short int i);
  RN_Link* dnLink(short int i);

  inline RN_Label* label() { return label_; }
  char* name();

  virtual float calcCurrentTravelTime();

  virtual float calcTravelTime() { return travelTime_; }
  inline float travelTime() { return travelTime_; }

  inline double length() { return length_; }
  inline double freeSpeed() { return freeSpeed_; }

  // Generalized travel time used in RN_TravelTime and shortest
  // path calculation.  It takes into account the freeway biases.

  inline float generalizedTravelTime() {
	return generalizeTravelTime(travelTime_);
  }
  float generalizeTravelTime(float);

  double inboundAngle();
  double outboundAngle();

  void calcIndicesAtNodes();
  void calcStaticInfo();
  int signalIndex(RN_Link *);
  inline unsigned char dnLegal() { return dnLegal_; }
  inline unsigned char dnIndex() { return dnIndex_; }
  inline unsigned char rightDnIndex() { return rightDnIndex_; }
  inline unsigned char upIndex() { return upIndex_; }
  void includeTurn(RN_Link *);
  void excludeTurn(RN_Link *);
  int isMovementAllowed(RN_Link *);
  unsigned int laneUseRules() { return laneUseRules_; }

  void assignCtrlListInSegments();
  void assignSurvListInSegments();

  inline ctrl_list_type& ctrlStationList() {
	return ctrlStationList_;
  }
  inline surv_list_type& survStationList() {
	return survStationList_;
  }

  ctrl_list_link_type ctrlList();
  surv_list_link_type survList();
  ctrl_list_link_type lastCtrl();
  surv_list_link_type lastSurv();
  ctrl_list_link_type prevCtrl(double);
  ctrl_list_link_type nextCtrl(double);
	
  surv_list_link_type prevSurv(double);
  surv_list_link_type nextSurv(double);

  virtual int init(int code, int type, int up, int dn,
				   int label = 0);

  void print(std::ostream &os = std::cout);
  void printSensors(std::ostream &os = std::cout);
  void printSignals(std::ostream &os = std::cout);
  void printTollPlazas(std::ostream &os = std::cout);
  void printBusStops(std::ostream &os = std::cout); //margaret
	  
  void printNotConnectedDnLinks(std::ostream &os = std::cout);
  int countNotConnectedDnLinks();

  RN_Link* upRightNeighbor();
  RN_Link* dnRightNeighbor();

  inline int isMarked() { return state_ & STATE_MARKED; }

  void unmarkLanes();
  void unmarkLanesInUpLinks();
  void unmarkLanesInDnLinks();

  void addPathPointer(RN_Path *path, short int i);

  short int nPathPointers() { return pathPointers_.size(); }

  RN_PathPointer* pathPointer(short int i) {
	if (i < 0) {
	  return NULL;
	} else if (i >= pathPointers_.size()) {
	  return NULL;
	} else {
	  return pathPointers_[i];
	}
  }
	  
  void calcPathCommonalityFactors();
  void printPathCommonalityFactors(std::ostream &os = std::cout);

  virtual int isCorrect(RN_Vehicle *);

  // Link travel time

  void initializeStatistics();
  void resetStatistics(int col, int ncols);

  // called when a vehicle leaves the link

  void recordTravelTime(RN_Vehicle *);

  // called for each vehicle in the network, include these in
  // the pretrip queues, at the end of the simulation

  void recordExpectedTravelTime(RN_Vehicle *);
	  
  // These are valid only after all the vehicles entered the
  // link in interval i have left the link.

  float averageTravelTimeEnteringAt(double enter);
  float averageTravelTimeEnteringAt(int i);

  void printTravelTimes(std::ostream& os=std::cout);
  void printReloadableTravelTimes(std::ostream& os=std::cout);
  void printFlowPlusTravelTimes(std::ostream& os, int istart, int iend);

  static int infoPeriods();
  static int infoPeriodLength();
};

#endif
