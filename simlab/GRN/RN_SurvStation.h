//-*-c++-*------------------------------------------------------------
// FILE: RN_SurvStation.h
// AUTH: Qi Yang
// DATE: Sat Oct 21 16:46:14 1995
//--------------------------------------------------------------------

#ifndef RN_SURVSTATION_HEADER
#define RN_SURVSTATION_HEADER

#include <iostream>
#include <vector>

#include "Constants.h"
#include "RN_Sensor.h"

#include <Templates/Object.h>
#include <Templates/Listp.h>

class RN_Link;
class RN_Segment;

//--------------------------------------------------------------------
// CLASS NAME: RN_SurvStation -- a surveillance station consists of
// one or more sensors.  Surverllace station is stored in a sorted
// list in each link according to their distance from the end of the
// link.  The sorting is in descending order (Upstream =
// LongerDistance = Top)
//--------------------------------------------------------------------

class RN_SurvStation : public CodedObject
{
  friend class RN_BisonParser;
  friend class RoadNetwork;
  friend class RN_Sensor;

protected:

  int type_;				// sensor type 
  unsigned int task_;		// data items
  RN_Segment* segment_;		// pointer to segment
  float distance_;			// distance from the link end 
  float zoneLength_;		// length of detection zone 
  float position_;			// position in % from segment end
  std::vector<RN_Sensor *> sensors_; // array of pointers to sensors 

public:

  RN_SurvStation() : CodedObject() { }
  ~RN_SurvStation() { }

  inline int type() { return (SENSOR_DEVICE_TYPE & type_); }
  inline int isLinkWide() { return (SENSOR_LINKWIDE & type_); }
  inline int isForEqBus() { return (SENSOR_FOR_EQUIPPED_BUS & type_); } //IEM(Jul2) add 16 if for equipped buses only
  inline unsigned int tasks(unsigned flag = 0x0FFFFFFF) {
	return (task_ & flag);
  }
  inline unsigned int atask() {
	return (task_ & SENSOR_AGGREGATE);
  }
  inline unsigned int itask() {
	return (task_ & SENSOR_INDIVIDUAL);
  }
  inline unsigned int stask() {
	return (task_ & SENSOR_SNAPSHOT);
  }
  
  inline RN_Segment* segment() { return segment_; }
  RN_Link* link();

  short int nLanes();

  // Returns pointer to the sensor in ith lane. It may be NULL if
  // there is no sensor in the ith lane.

  RN_Sensor* sensor(int i = 0);

  // Connect ith point to the sensor

  void sensor(int i, RN_Sensor *sensor);

  inline float distance() { return distance_; }
  inline float zoneLength() { return zoneLength_; }
  float position() { return position_; }
	
  virtual int init(int type, int task, float zone,
				   int seg, float pos);
  virtual void print(std::ostream &os = std::cout);

  int cmp(CodedObject*) const;
  int cmp(int c) const { return CodedObject::cmp(c); }

  // functions for incident detection

  virtual int prmGrpID() { return -1; }
  virtual int stationState() { return -1;}

  virtual void setprmGrpID(int n) { }
  virtual unsigned int setState(unsigned int n){ return 1; }
  virtual unsigned int clearState(unsigned int n) { return 1; }
  virtual unsigned int addState(unsigned int n) { return 1; }
  virtual int incDcrPersistCount() { return -1; }

  // computes the flow across the section - used in incident detection

  int count();
};

typedef Listp<RN_SurvStation*>::link_type SurvList;

#endif
