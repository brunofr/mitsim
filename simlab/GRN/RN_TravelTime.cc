//-*-c++-*------------------------------------------------------------
// NAME: Massachusetts Institue of Technology MITSIM
// NOTE: 
// AUTH: Peter J. Welch
// FILE: RN_TravelTime.C
// DATE: Mon Mar  4 10:49:58 1996
//--------------------------------------------------------------------

#include "Constants.h"
#include "RN_TravelTime.h"
#include <Tools/ToolKit.h>


RN_TravelTime * theTravelTime = NULL;

RN_TravelTime::RN_TravelTime() 
{
  theTravelTime = this;
}

int RN_TravelTime::read(Reader &is) {
  int collection_code;
  is >> time_interval_.start_time_;
  is >> time_interval_.end_time_;

  is >> num_veh_type_;
  veh_type_array_ = new unsigned int[num_veh_type_];
  for (int i = 0; i < num_veh_type_; i++)
     veh_type_array_[i] = is.gethex();
  is >> collection_code;
  is.findToken(OPEN_TOKEN);
  switch (collection_code) {
    case 0:
    cout << "Collecting travel times for all OD pairs." << endl;
    break;
    case 1:
    break;
    case 2:
    break;
  };
  is.findToken(CLOSE_TOKEN);
  return 0;
}


int
RN_TravelTime::checkVehicleType(int slot, int vehicle_type)
{
  if (slot >= num_veh_type_) {
    cerr << "Error!  RN_TravelTime::checkVehicleType " << slot << 
      " out of bounds." << endl;
    return -1;
  }
  int slot_prefix = veh_type_array_[slot] & ~VEHICLE_CLASS;
  int vehicle_prefix = vehicle_type & ~VEHICLE_CLASS;
  int slot_class = veh_type_array_[slot] & VEHICLE_CLASS;
  int vehicle_class = vehicle_type & VEHICLE_CLASS;

  return (slot_prefix & vehicle_prefix &&
	  (slot_class == 0 || slot_class == vehicle_class));
      
}



