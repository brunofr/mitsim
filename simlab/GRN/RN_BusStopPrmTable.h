//-*-c++-*------------------------------------------------------------
// NAME: A parser for bus stop parameters
// AUTH: Qi Yang & Daniel Morgan
// FILE: RN_BusStopPrmTable.h
// DATE: Fri Nov 30 16:55:50 2001
//--------------------------------------------------------------------

#ifndef RN_BUSSTOPPRMTABLE_HEADER
#define RN_BUSSTOPPRMTABLE_HEADER

#include <iostream>
//#include <vector>

#include "RN_BusStop.h"

const int NUM_STOPS = 10;	// Default initial num of bus stops

class RN_BusStopPrmParser;
class RN_BusStopPrmBisonParser;

class RN_BusStopPrmTable
{
      friend class RN_BusStopPrmBisonParser;
      friend class RN_BusStopPrmParser;

   protected:

      static char * name_;		// file name
      double startTime_;		// dep time smaller than this is skipped
      double nextTime_;			// time to read od pairs

   public:

      RN_BusStopPrmTable();

      virtual ~RN_BusStopPrmTable() {}
	
      void open(double start_time = -86400.0);
      double read();

      static inline char* name() { return name_; }
      static inline char** nameptr() { return &name_; }

      inline double startTime() { return startTime_; }
	  inline double nextTime() { return nextTime_; }

	  inline int skip() {
		return (nextTime_ < startTime_);
	  }

      virtual int initBusStopPrms(double start);

      // The assignDwellBounds is called from the parser and assigns
      // the upper and lower dwell time bounds to a bus stop     

      virtual void assignDwellBounds(int code, int lower, int upper);
      virtual int assignRateParams(int code, int routecode, float arr, float alight);
      virtual int assignArrivalParams(int code, int routecode, float st, float a, 
                                      int b, float c, float peak, float alight);

      virtual void print(std::ostream &os = std::cout);

   private:

      RN_BusStopPrmParser* parser_; // the parser

};

extern RN_BusStopPrmTable * theBusStopPrmTable;

#endif
