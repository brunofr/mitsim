//-*-c++-*------------------------------------------------------------
// RN_Lane.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <algorithm>
#include "RoadNetwork.h"

#include "RN_Node.h"
#include "RN_Link.h"
#include "RN_Segment.h"
#include "RN_Lane.h"
#include "RN_CtrlStation.h"
#include "RN_SurvStation.h"
#include "Parameter.h"

#include <Tools/ToolKit.h>
using namespace std;

int RN_Lane::sorted_ = 1;

RN_Lane::RN_Lane()
  : segment_(NULL),
	laneType_(0),
	state_(0),
	cmarker_(0)
{
}

RN_Link*
RN_Lane::link()
{
  return segment_->link();
}


double
RN_Lane::length()
{
  return segment_->length();
}


int
RN_Lane::linkType()
{
  return segment_->type();
}


int
RN_Lane::localIndex()
{
  return (index_ - segment_->leftLaneIndex());
}


RN_Lane*
RN_Lane::right()
{
  if (index_ != segment_->leftLaneIndex() + segment_->nLanes() - 1)
	return theNetwork->lane(index_ + 1);
  else return (NULL);
}


RN_Lane*
RN_Lane::left()
{
  if (index_ != segment_->leftLaneIndex())
	return theNetwork->lane(index_ - 1);
  else return (NULL);
}


RN_Lane*
RN_Lane::prev()
{
  if (index_ > 0)
	return theNetwork->lane(index_ - 1);
  else return NULL;
}


RN_Lane*
RN_Lane::next()
{
  if (index_ < theNetwork->nLanes() - 1)
	return theNetwork->lane(index_ - 1);
  else return NULL;
}


int
RN_Lane::init(int c, int r)
{
  if (ToolKit::debug()) {
	cout << indent << indent << indent
		 << "<" << c << endc << r
		 << ">" << endl;
  }

  static int last = -1;
  if (segment_) {
	cerr << "Error:: Lane <" << c << "> "
		 << "cannot be initialized twice. ";
	return -1;
  } else {
	segment_ = theNetwork->lastSegment();
  }
  code_ = c;
  rules_ = r;

  if (sorted_ && code_ <= last) sorted_ = 0;
  else last = code_;

  index_ = theNetwork->nLanes();
  theNetwork->addLane(this);

  segment_->nLanes_ ++;

  return 0;
}

void
RN_Lane::print(ostream &os)
{
  os << "{" << code_ << endc << rules_ << "}" << endc;
}


void
RN_Lane::printDnConnectors(ostream &os)
{
  register int i;
  if (nDnLanes()) os << indent;
  for (i = 0; i < nDnLanes(); i ++) {
	if (i) os << endc;
	os << OPEN_TOKEN;
	os << code_ << endc << dnLane(i)->code_;
	os << CLOSE_TOKEN;
  }
  if (nDnLanes()) os << endl;
}


void
RN_Lane::calcStaticInfo()
{
  //upLanes_.resize();
  //dnLanes_.resize();

  if (!left()) rulesExclude(LANE_CHANGE_LEFT);
  if (!right()) rulesExclude(LANE_CHANGE_RIGHT);

  setLaneType();
}


/*
 *--------------------------------------------------------------------
 * Returns the limiting speed in this lane.  Speed is in meter/sec.
 * This function should be overloaded to consider lane position (see
 * class TS_Lane for more details)
 *--------------------------------------------------------------------
 */

double
RN_Lane::freeSpeed()
{
  return segment_->freeSpeed();
}


/*
 *--------------------------------------------------------------------
 * Returns the grade of the lane, in percent. Used to calculate the
 * Acceleration and limiting speed.
 *--------------------------------------------------------------------
 */

double
RN_Lane::grade()
{
  return segment_->grade();
}


/*
 *--------------------------------------------------------------------
 * Returns the 1st surveillance station in this lane (upstream end)
 *--------------------------------------------------------------------
 */

RN_Lane::surv_list_link_type RN_Lane::survList()
{
  return (segment_->survList());
}


/*
 *--------------------------------------------------------------------
 * Returns the 1st control station in this lane (upstream end)
 *--------------------------------------------------------------------
 */

RN_Lane::ctrl_list_link_type RN_Lane::ctrlList()
{
  return (segment_->ctrlList());
}


/*
 *--------------------------------------------------------------------
 * Check if a lane is one of the downstream lanes
 *--------------------------------------------------------------------
 */

RN_Lane* RN_Lane::findInDnLane(int c)
{
  vector<RN_Lane*>::iterator i;
  i = find_if(dnLanes_.begin(), dnLanes_.end(), CodeEqual(c));
  if (i != dnLanes_.end()) return *i;
  else return NULL;
}

bool RN_Lane::isInDnLanes(RN_Lane *plane)
{
  for (register int i = 0; i < nDnLanes(); i ++)
	{
      if (dnLane(i)->code_ == plane->code_) return true;
	}
  return false;
}


/*
 *--------------------------------------------------------------------
 * Check if a lane is one of the upstream lanes
 *--------------------------------------------------------------------
 */

RN_Lane* RN_Lane::findInUpLane(int c)
{
  vector<RN_Lane*>::iterator i;
  i = find_if(upLanes_.begin(), upLanes_.end(), CodeEqual(c));
  if (i != upLanes_.end()) return *i;
  else return NULL;
}

bool RN_Lane::isInUpLanes(RN_Lane * plane)
{
  for (register int i = 0; i < nUpLanes(); i ++)
	{
      if (upLane(i)->code_ == plane->code_) return true;
	}
  return false;
}


void 
RN_Lane::markConnectedUpLanes(unsigned int signature, float depth)
{
  if (cmarker_ & signature) return; // opps, a loop found

  cmarker_ |= signature;		// mark this lane

  depth += length();			// accumulate the searched distance

  if (depth > theBaseParameter->visibility()) return;

  for (register int i = 0; i < nUpLanes(); i ++) {
	upLane(i)->markConnectedUpLanes(signature, depth);
  }
}


void 
RN_Lane::unmarkConnectedUpLanes(unsigned int signature, float depth)
{
  if (!(cmarker_ & signature)) return; // opps, a loop found

  cmarker_ &= ~signature;		// mark this lane

  depth += length();			// accumulate the searched distance

  if (depth > theBaseParameter->visibility()) return;

  for (register int i = 0; i < nUpLanes(); i ++) {
	upLane(i)->unmarkConnectedUpLanes(signature, depth);
  }
}


void
RN_Lane::markConnectedDnLanes(unsigned int signature, float depth)
{
  if (cmarker_ & signature) return; // opps, a loop found

  cmarker_ |= signature;		// mark this lane
  depth += length();			// accumulate the searched distance

  if (depth > theBaseParameter->visibility()) return;

  for (register int i = 0; i < nDnLanes(); i ++) {
	dnLane(i)->markConnectedDnLanes(signature, depth);
  }
}

void
RN_Lane::unmarkConnectedDnLanes(unsigned int signature, float depth)
{
  if (!(cmarker_ & signature)) return; // opps, a loop found

  cmarker_ &= ~signature;		// mark this lane
  depth += length();			// accumulate the searched distance

  if (depth > theBaseParameter->visibility()) return;

  for (register int i = 0; i < nDnLanes(); i ++) {
	dnLane(i)->unmarkConnectedDnLanes(signature, depth);
  }
}

void
RN_Lane::unMarkAllLanes()
{
  for (register int i = 0; i < theNetwork->nLanes(); i ++) {
	theNetwork->lane(i)->unsetState(STATE_MARKED);
  }
}

/*
 *--------------------------------------------------------------------
 * Set laneType based on link type and info on connectivity.
 *--------------------------------------------------------------------
 */

void
RN_Lane::setLaneType()
{
  /* check if this lane is a shoulder lane
   */
  if (!right()) laneType_ |= LANE_TYPE_RIGHT_MOST;
  if (!left()) laneType_ |= LANE_TYPE_LEFT_MOST;
	
  register int i, j;
  RN_Lane *plane;

   /* check if this lane is connected to an on-ramp at the upstream
    * end */

  for (i = 0; i < nUpLanes(); i ++) {
	if (upLane(i)->linkType() & LINK_TYPE_RAMP) {
	  laneType_ |= LANE_TYPE_UP_ONRAMP;
	  break;
	}
  }

  /* check if this lane shares the same upstream lane with an
    * off-ramp lane (actually this info is not very useful as other
    * info being calculated in this function in terms of drive
    * behavior. It is coded anyway just in case some other algorithm
    * may use it) */

  for (i = 0;
	   i < nUpLanes() && !(laneType_ & LANE_TYPE_UP_OFFRAMP);
	   i ++) {
	plane = upLane(i);
	for (j = 0; j < plane->nDnLanes(); j ++) {
	  if (plane->dnLane(j)->linkType() & LINK_TYPE_RAMP) {
	    laneType_ |= LANE_TYPE_UP_OFFRAMP;
	    break;
	  }
	}
  }

  /* check if this lane is connected to an off-ramp at the downstream
    * end.  */

  for (i = 0; i < nDnLanes(); i ++) {
	if (dnLane(i)->linkType() & LINK_TYPE_RAMP) {
	  laneType_ |= LANE_TYPE_DN_OFFRAMP;
	  break;
	}
  }

  /* check if this lane merges into the same downstream lane with an
    * on-ramp lane. */

  for (i = 0;
	   i < nDnLanes() && !(laneType_ & LANE_TYPE_DN_ONRAMP);
	   i ++) {
	plane = dnLane(i);
	for (j = 0; j < plane->nUpLanes(); j ++) {
	  if (plane->upLane(j)->linkType() & LINK_TYPE_RAMP) {
	    laneType_ |= LANE_TYPE_DN_ONRAMP;
	    break;
	  }
	}
  }

  // Check if this is the last lane

  if (!segment()->downstream() &&  // last segment ('->downstream()' added by Angus)
	  link()->dnNode()->type(NODE_TYPE_EXTERNAL) && // external node
	  nDnLanes() <= 0) {	// no downstream lane
	laneType_ |= LANE_TYPE_BOUNDARY;
  }

  if (!laneType(LANE_TYPE_BOUNDARY) &&	// not at the boundary
	  nDnLanes() <= 0) {	// no downstream lane
	laneType_ |= LANE_TYPE_DROPPED;
  }
}
