//-*-c++-*------------------------------------------------------------
// FILE: RN_BusRoute.h
// DATE: Thu Oct 19 17:29:19 1995
//--------------------------------------------------------------------

#ifndef RN_BUSROUTE_HEADER
#define RN_BUSROUTE_HEADER

#include <iosfwd>
#include <vector>

#include <Templates/Object.h>

class RoadNetwork;
class RN_Link;
class RN_Node;
class RN_BusRouteParser;
class RN_BusRouteTable;
class RN_BusSchedule;
class BusAssignment;
class RN_LinkTimes;
class RN_BusStop;

class RN_BusRoute : public CodedObject
{
      friend class RN_BusRouteParser;
      friend class RN_BusRouteTable;
      friend class RoadNetwork;
      friend class RN_BusSchedule;
      friend class BusAssignment;

   protected:

      static int sorted_;	        // 1=sorted 0=arbitrary order

      int index_;		        // index in the array
      std::vector<RN_Link*> routelinks_;	// list of links of the bus route
      std::vector<RN_BusStop*> timepoints_;  // list of bus stops/sensors on the bus route
      double designHeadway_;		// design of buses traveling on this route

   public:
	
      RN_BusRoute() : CodedObject() { }
      ~RN_BusRoute() { }

      static inline int sorted() { return sorted_; }

      int nRouteLinks() { return routelinks_.size(); }
      RN_Link* routelink(int i) {
		 if (i < 0) return NULL;
		 else if (i >= nRouteLinks()) return NULL;
		 else return routelinks_[i];
	  }
      RN_Link* nextRouteLink(int i);
      RN_Link* firstRouteLink() {
		if (nRouteLinks() > 0) return routelinks_[0];
		else return NULL;
	  }
      RN_Link* lastRouteLink() {
		if (nRouteLinks()) return routelinks_[nRouteLinks() - 1];
		else return NULL;
	  }

      int nTimepoints() { return timepoints_.size(); }
      RN_BusStop* timepoint(int i) {
		 if (i < 0) return NULL;
		 else if (i >= nTimepoints()) return NULL;
		 else return timepoints_[i];
	  }
      RN_BusStop* nextTimepoint(int i);
      RN_BusStop* firstTimepoint() {
		if (nTimepoints() > 0) return timepoints_[0];
		else return NULL;
	  }
      RN_BusStop* lastTimepoint() {
		if (nTimepoints()) return timepoints_[nTimepoints() - 1];
		else return NULL;
	  }

      RN_Link* findRouteLink(int c);
      RN_BusStop* findTimepoint(int c);

      double designHeadway() { return designHeadway_; }

      int index() { return index_; }

      // These two are called by RN_BusRouteParser

      virtual int init(int code, double hw);
      virtual int addRouteLink(int c);
      virtual int addTimepoint(int c);

      virtual void print(std::ostream &os);

      int firstNode();

      int endNode();
};

#endif
