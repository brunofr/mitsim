//
// RN_PathParser.y -- RN_Path trip table parser generator
// by Qi Yang
//

%name   RN_PathParser
%define ERROR_BODY	DEF_ERROR_BODY
%define LEX_BODY	  DEF_LEX_BODY

// These macros are expanded when creating the definition for named
// BisonParser class.  The functions defined as member here are
// designed for use in the grammar rules.

%define CONSTRUCTOR_PARAM  RN_PathTable* pContainer_
%define CONSTRUCTOR_INIT   : _pContainer(pContainer_)
%define MEMBERS            DEF_MEMBERS \
    RN_PathTable* _pContainer ;

%union {
  long        type_i ;
  double      type_f ;
  const char* type_s ;
}

%token <type_s> LEX_EQUAL
%token <type_s> LEX_COMMA
%token <type_s> LEX_COLON
%token <type_s> LEX_SEMICOLON
%token <type_s> LEX_OAB
%token <type_s> LEX_CAB
%token <type_s> LEX_OBB
%token <type_s> LEX_CBB
%token <type_s> LEX_OCB
%token <type_s> LEX_CCB
%token <type_i> LEX_INT
%token <type_f> LEX_REAL
%token <type_s> LEX_TIME
%token <type_s> LEX_PAIR
%token <type_s> LEX_TAG
%token <type_s> LEX_STRING
%token <type_s> LEX_DUP
%token <type_s> LEX_END

%header{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <IO/Exception.h>

#include <Tools/Scanner.h>
#include <Tools/ToolKit.h>  

#include <GRN/RN_PathTable.h>
#include <GRN/RN_Path.h>
#include <GRN/RN_Link.h>
%}

//--------------------------------------------------------------------
// Symbols used road network database grammer rules.
//--------------------------------------------------------------------

%type <type_s> block_name
%type <type_i> inum num_objects specified unspecified
%type <type_f> fnum float_num travel_time
	   		 
%type <type_s> paths path_table_allocator path_data
%type <type_s> multi_paths path
%type <type_s> multi_links link

%start everything

%%

//--------------------------------------------------------------------
// Beginniing of RN_Path table grammer rules.
//--------------------------------------------------------------------

everything: paths
{
  if (ToolKit::debug()) {
	cout << "Finished parsing path table <"
		 << filename() << ">." << endl;
  }
}
;

paths: block_name path_table_allocator LEX_OCB LEX_CCB
| block_name path_table_allocator LEX_OCB multi_paths LEX_CCB
{
  int n = _pContainer->nPaths();
  if (ToolKit::debug()) {
	cout << indent << n << " parsed." << endl;
  }
}
;

path_table_allocator: num_objects
{
  int n = ($1 > 0) ? ($1) : (NUM_PATHS);
  _pContainer->paths_.reserve(n);
}
;

multi_paths: path
| multi_paths path
;

path: LEX_OCB path_data LEX_OCB multi_links LEX_CCB travel_time LEX_CCB
;

path_data: inum inum inum
{
  // $1=PathID $2=Ori $3=Des
  int err_no = _pContainer->addPath($1, $2, $3);
  check(err_no) ;
}
;

multi_links: link
| multi_links link
;

link: inum
{
  // $1=LinkID
  int err_no = _pContainer->lastPath()->addLink($1);
  check(err_no);
}
;

travel_time: fnum
{
  // _pContainer->lastPath()->resize();
  // $1=TravelTimeOnThisPath
  _pContainer->lastPath()->setPathTravelTime($1);
}
;

//--------------------------------------------------------------------
// Beginning of basic grammer rules.
//--------------------------------------------------------------------

block_name: LEX_TAG
{
  const char *s = value("[]");
  if (ToolKit::debug()) {
    cout << "Parsing " << s << " ..." << endl;
  }
  $$ = s;
}
;

inum: LEX_INT
{
  $$ = atoi(value());
}
;

fnum: float_num
| inum
{
  $$ = $1;
}
;

float_num: LEX_REAL
{
  $$ = atof(value());
}
;

num_objects: specified
| unspecified
{
  $$ = $1;
}
;
 
specified: LEX_COLON inum
{
  $$ = $2;
}
;

unspecified: LEX_COLON
{
  $$ = 0;
}
;

%%

// Following pieces of cpte will be verbosely copied into the parser.

%header{
  // empty
%}
