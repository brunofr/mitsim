//-*-c++-*------------------------------------------------------------
// NAME: Simulation Widges
// AUTH: Qi Yang
// FILE: NameTable.h
// DATE: Thu Nov  2 11:07:37 1995
//--------------------------------------------------------------------

#ifndef NAME_TABLE_HEADER
#define NAME_TABLE_HEADER


//--------------------------------------------------------------------
// Type IDs for various objects used in the simulation are not obvious
// to be understood. This class is used to translate a code into a 
// string name.
//--------------------------------------------------------------------

class NameTable
{
   public:

      NameTable() { }
      ~NameTable() { }

      static const char* controller(int);
      static const char* signal(int);
      static const char* sensor(int);
};

#endif
