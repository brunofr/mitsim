//-*-c++-*------------------------------------------------------------
// FILE: RN_Node.h
// DATE: Wed Oct 18 12:36:24 1995
//--------------------------------------------------------------------

#ifndef RN_NODE_HEADER
#define RN_NODE_HEADER

//#include <iostream>
#include <iosfwd>
#include <vector>

#include <Templates/Object.h>

#include "RN_Link.h"
#include "RN_IS.h"

const int NODE_TYPE_GEOMETRY		= 0x000F; // mask
const int NODE_TYPE_CENTRIOD		= 0x0000; // enumeration
const int NODE_TYPE_EXTERNAL        = 0x0001; // enumeration
const int NODE_TYPE_INTERSECTION    = 0x0002; // enumeration

const int NODE_TYPE_OD			    = 0x0030; // sum = 24
const int NODE_TYPE_ORI             = 0x0010; // 08
const int NODE_TYPE_DES             = 0x0020; // 16

class RN_Path ;
class RN_Cell ;
class RN_Vehicle;

class RN_Node : public NamedObject
{
    friend class RoadNetwork;
    friend class RN_BisonParser;
    friend class OD_Cell;
    friend class RN_Vehicle;
    friend class RN_IS;


protected:

    static int sorted_;	// 1=sorted 0=arbitrary
    int index_;		// index in array
    int type_;		// node type
    int destIndex_;           // index among destinations in array 
                                // (only valid if type_ & NODE_TYPE_DES is true)

    std::vector< RN_Link *> upLinks_; // pointers to inbound links
    std::vector< RN_Link *> dnLinks_; // pointers to outbound links
   
    

    // tomer - pointer to the RN_IS object. null if it doesn't exist

    RN_IS* InterS_;

    // **



    unsigned int state_;
   
    int nOriCounts_;		// vehicle counts
    int nDesCounts_;

public:

    RN_Node()
        : NamedObject(),
	

  // tomer - initializing InterS_ to be null
        
    InterS_(NULL), state_(0), nOriCounts_(0), nDesCounts_(0) { }

    
  // **


 

    virtual ~RN_Node() { }

    static inline int sorted() { return sorted_; }

    inline int type(int flag) { return type_ & flag; } // '*' changed to '&' by Angus
    inline int type() { return type_; }
    inline int odType() { return type_ & NODE_TYPE_OD; }
    inline int geometryType() { return type_ & NODE_TYPE_GEOMETRY; }

    inline unsigned int state(unsigned int mask = 0xFFFF) {
        return (state_ & mask);
    }
    inline void setState(unsigned int s) {
        state_ |= s;
    }
    inline void unsetState(unsigned int s) {
        state_ &= ~s;
    }

    void setInterS(RN_IS* is); 

    RN_IS* getInterS();

    inline int nUpLinks() { return upLinks_.size(); }
    inline RN_Link* upLink(int i) { return upLinks_[i]; } 

    inline int nDnLinks() { return dnLinks_.size(); }
    inline RN_Link* dnLink(int i) { return dnLinks_[i]; }

    void addUpLink(RN_Link *);
    void addDnLink(RN_Link *);
    int whichUpLink(RN_Link *);
    int whichDnLink(RN_Link *);
    void sortUpLinks();
    void sortDnLinks();

    inline int index() { return index_; }
    inline int destIndex() { return destIndex_; }

    virtual int init(int code, int type, const char *name);
    virtual void print(std::ostream &os);
    virtual void calcStaticInfo() { }
    virtual void calcGeometricData() { }

    // Dynamically generate vehicle path

    void routeGenerationModel(RN_Vehicle *);

    // For vehicles that already has a path, they use this function
    // to check whether they should keep their current paths or
    // enroute
      
    void routeSwitchingModel(RN_Vehicle *, OD_Cell* od = 0);
};

#endif
