/*-*-c++-*------------------------------------------------------------
 *  OD_Scanner.lex -- OD trip table lexical analyzer generator
 *--------------------------------------------------------------------
 */

%{
#include "RN_PathParser.h"
#define YY_BREAK
%}

%option yyclass="RN_PathFlexLexer"
%option noyywrap
%option yylineno

C	(#.*)|(%.*)|("//".*)
WS	[\t ,;\n]+

D	[0-9]
N	{D}+
E       [eE][-+]?{D}+

F1      {D}+\.{D}*({E})?
F2      {D}*\.{D}+({E})?
F3      {D}+({E})?

I	[-+]?{N}
R       [-+]?({F1}|{F2}|{F3})
NAME    [[]([A-Za-z \t]*)[]]

%%

"/*"	{	/* skip comments */
  int c;
  while ((c = yyinput()) != 0) {
    if (c == '*') {
      if ((c = yyinput()) == '/') break;
      else unput(c);
    }
  }
  break;
}

{WS}	{ break; }
{C}	{ break; }

[:=]    { return RN_PathParser::RN_Path_COL; }

"{"	{ return RN_PathParser::RN_Path_OB; }
"}"	{ return RN_PathParser::RN_Path_CB; }
		            
{NAME}  { return RN_PathParser::RN_Path_NAME; }
{I}	{ return RN_PathParser::RN_Path_INT; }
{R}     { return RN_PathParser::RN_Path_REAL; }

%%
