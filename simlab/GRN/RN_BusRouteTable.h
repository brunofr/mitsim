//-*-c++-*------------------------------------------------------------
// NAME: A parser for bus route 
// AUTH: Qi Yang & Daniel Morgan
// FILE: BusRouteTable.h
// DATE: Fri Nov 30 16:55:50 2001
//--------------------------------------------------------------------

#ifndef RN_BUSROUTETABLE_HEADER
#define RN_BUSROUTETABLE_HEADER

#include <iostream>
#include <vector>

#include "RN_BusRoute.h"

const int NUM_BUSROUTES = 20;	// Default initial num of bus routes

class RN_BusRouteParser;
class ArgsParser;

class RN_BusRouteTable
{
      friend class RN_BusRouteParser;
      friend class ArgsParser;

   protected:

      std::vector<RN_BusRoute*> busroutes_;	// array of pointers to bus routes

   public:

      static char * name_;	// file name
      static char * name() { return name_; }
      static char ** nameptr() { return &name_; }

      RN_BusRouteTable() { }

      virtual ~RN_BusRouteTable();

      // This may be overloaded by derived class

      virtual RN_BusRoute* newBusRoute();

      virtual int addBusRoute(int code, double hw = 0.0);
      inline RN_BusRoute* lastBusRoute() { return busroutes_.back(); }

      inline int nBusRoutes() { return busroutes_.size(); }
      inline RN_BusRoute* busroute(int i) { return busroutes_[i]; }
      RN_BusRoute* findBusRoute(int c);
      int enableControl(int rc, int sc);

      virtual void print(std::ostream &os = std::cout);

};

extern RN_BusRouteTable * theBusRouteTable;

#endif
