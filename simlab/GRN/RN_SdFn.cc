//-*-c++-*------------------------------------------------------------
// RN_SdFn.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <Tools/ToolKit.h>
#include <Tools/GenericVariable.h>
#include <Tools/Math.h>

#include "RN_SdFn.h"
#include "Parameter.h"
using namespace std;

// Default parameters in speed density functions

RN_SdFn::RN_SdFn()
  : capacity_(0.5),				// 1800 vph in vps
	jamSpeed_(0),			// 5 mph in mps
	jamDensity_(180.0)			// in vehicles/km
{
}

RN_SdFn::RN_SdFn(float cap, float spd, float kjam)
{
  capacity_ = cap / 3600.0;
  jamSpeed_ = spd * theBaseParameter->speedFactor();
  jamDensity_ = kjam * theBaseParameter->densityFactor();
}


void
RN_SdFn::print(ostream& os)
{
  double cap = capacity_ * 3600.0;
  double spd = jamSpeed_ / theBaseParameter->speedFactor();
  double den = jamDensity_ / theBaseParameter->densityFactor();
  os << Fix(cap, 1.0) << endc
	 << Fix(spd, 1.0) << endc
	 << Fix(den, 1.0) << endc;
}

// Linear Speed-Density Function

RN_SdFnLinear::RN_SdFnLinear()
  : RN_SdFn(),
	kl_(0.35),
	vh_(0.76),
	kh_(0.56),
	vl_(0.20)
{
  initialize();
}

RN_SdFnLinear::RN_SdFnLinear(float cap, float spd, float kjam,
							 float kl, float vh, float kh, float vl)
  : RN_SdFn(cap, spd, kjam),
	kl_(kl),
	vh_(vh),
	kh_(kh),
	vl_(vl)
{
  initialize();
  cout << "Capacity " << capacity_ << "jamSpeed_ " << jamSpeed_ << "jamDensity_ " << jamDensity_ ;
}

void RN_SdFnLinear::initialize()
{
  delta_l_ = (1.0 - vh_) / kl_;
  delta_m_ = (vh_ - vl_) / (kh_ - kl_);
  delta_h_ = vl_ / (1.0 - kh_);
}

float
RN_SdFnLinear::densityToSpeed(float free_speed, float density, int nlanes)
{
  //  cout << " Linear SDFN" ;
  if (density < 1.0) {
	return free_speed;
  } else if (density + 1.0 > jamDensity_) {
	return jamSpeed_ * (nlanes - 1);
  } else {
	float y = density / jamDensity_;
	float v0 = jamSpeed_ * (nlanes - 1);
	float r;
	if (y < kl_) {
	  r = 1.0 - delta_l_ * y;
	} else if (y > kh_) {
	  r = vh_ - delta_m_ * (y - kl_);
	} else {
	  r = vl_ - delta_h_ * (y - kh_);
	}
	//	cout << " r " << r <<" free speed" << free_speed <<" jam speed " << jamSpeed_ << "Jam Density " << jamDensity_<< "Capacity " << capacity_;
	return v0 + r * (free_speed - v0);
  }
}

void RN_SdFnLinear::print(ostream &os)
{
  os << indent << "{" << endc;
  RN_SdFn::print(os);
  os << kl_ << endc
	 << vh_ << endc
	 << kh_ << endc
	 << vl_ << endc;
  os << "}" << endl;
}


// Non Linear Speed-Density Function

RN_SdFnNonLinear::RN_SdFnNonLinear()
  : RN_SdFn(),
	densityBeta_(1.8),
	speedBeta_(5.0)
{
}

RN_SdFnNonLinear::RN_SdFnNonLinear(float cap, float spd, float kjam,
								   float alpha, float beta)
  : RN_SdFn(cap, spd, kjam),
	densityBeta_(alpha), speedBeta_(beta)
{
}

float
RN_SdFnNonLinear::densityToSpeed(float free_speed, float density,
								 int nlanes)
{
  // cout << " Non-Linear SDFN" ;
  if (density < 1.0) {
	return free_speed;
  } else if (density + 1.0 > jamDensity_) {
	return jamSpeed_ * (nlanes - 1);
  } else {
	float y = density / jamDensity_;
	float k = pow(y, densityBeta_);
	float r = pow(1.0 - k, speedBeta_);
	float v0 = jamSpeed_ * (nlanes - 1);
	// cout << " r " << r <<" free speed" << free_speed <<" jam speed " << jamSpeed_ ;
	return v0 + r * (free_speed - v0);
  }
}

void RN_SdFnNonLinear::print(ostream &os)
{
  os << indent << "{" << endc;
  RN_SdFn::print(os);
  os << densityBeta_ << endc
	 << speedBeta_ << endc;
  os << "}" << endl;
}
