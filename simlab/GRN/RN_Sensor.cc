//-*-c++-*------------------------------------------------------------
// FILE: RN_Sensor.C
// AUTH: Qi Yang
// DATE: Sat Oct 21 14:35:21 1995
//--------------------------------------------------------------------

#include "RN_Sensor.h"

#include <Tools/ToolKit.h>
#include <Tools/Random.h>

#include "Parameter.h"
#include "Constants.h"

#include "RoadNetwork.h"
#include "RN_Node.h"
#include "RN_Link.h"
#include "RN_Segment.h"
#include "RN_Lane.h"
#include "RN_Sensor.h"
#include "RN_SurvStation.h"
using namespace std;


int RN_Sensor::sorted_ = 1;

// Turn off particular bits

unsigned int
RN_Sensor::clearState(unsigned int flag)
{
  return (state_ &= ~flag);
}


// Turn on particular bits

unsigned int
RN_Sensor::setState(unsigned int flag)
{
  return (state_ |= flag);
}

// used in incident detection

void
RN_Sensor::addState(unsigned int flag)
{
  state_ += flag;
}
  
// Iterators on global array in the network

RN_Sensor*
RN_Sensor::prev()
{
  if (index_ > 0)
	return theNetwork->sensor(index_ - 1);
  else
	return NULL;
}


RN_Sensor*
RN_Sensor::next()
{
  if (index_ < (theNetwork->nSensors() - 1))
	return theNetwork->sensor(index_ + 1);
  else
	return NULL;
}


// Returns 1 if the sensor is like-wide or 0 if it is lane specific

int
RN_Sensor::isLinkWide()
{
  return station_->isLinkWide();
}


float
RN_Sensor::width()
{
  if (station_->isLinkWide()) {
	return segment()->nLanes() * LANE_WIDTH;
  } else {
	return LANE_WIDTH;
  }
}


// Returns the sensor type

int
RN_Sensor::type()
{
  return station_->type();
}


// Returns data type this sensor collects

unsigned int RN_Sensor::tasks(unsigned int flag)
{
  return station_->tasks(flag);
}

unsigned int RN_Sensor::atask()
{
  return station_->atask();
}
unsigned int RN_Sensor::itask()
{
  return station_->itask();
}
unsigned int RN_Sensor::stask()
{
  return station_->stask();
}


// Distance from the end of the link

float
RN_Sensor::distance()
{
  return station_->distance();
}


// relative distance from the end of the segment

float
RN_Sensor::position()
{
  return station_->position();
}


// Length of the detection zone

float
RN_Sensor::zoneLength()
{
  return station_->zoneLength();
}


// Segment that contains this sensor

RN_Segment*
RN_Sensor::segment()
{
  return station_->segment();
}


// Link that contains this sensor

RN_Link*
RN_Sensor::link()
{
  return station_->segment()->link();
}


// Called by RN_Parser to set sensor variables. Returns 0 if no error.

int
RN_Sensor::init(int c, float p, int l, int i) //IEM(Apr26) added i for interval
{
  static int last = -1;

  if (ToolKit::debug()) {
	cout << indent << indent << "<"
		 << c << endc << p << endc << l
		 << ">" << endl;
  }

  if (station_) {
	cerr << "Error:: Sensor <" << c << "> "
		 << "cannot be initialized twice. ";
	return -1;
  } else {
	station_ = theNetwork->lastSurvStation();
  }

  if (sorted_ && c <= last) sorted_ = 0;
  else last = c;
  code_ = c;
   
  prob_ = p;
  if (theRandomizers[Random::Misc]->brandom(prob_)) {
	state_ = 0;
  } else {
	state_ = SENSOR_BROKEN; 
  }

  if (l < 0) {
	RN_Segment* ps = segment();

	// The central lane

	lane_ = theNetwork->lane(ps->leftLaneIndex() + ps->nLanes() / 2);

  } else {
	if (!(lane_ = theNetwork->findLane(l)) ||
		(lane_->segment() != segment())) {
	  cerr << "Error:: Unknown lane ID <" << l << ">. ";
	  return -1;
	}
  }

  intervalType_ = i; //IEM(Apr26) Perhaps add error checking later.

  index_ = theNetwork->nSensors();
  theNetwork->addSensor(this);

  if (station_->isLinkWide()) {
	station_->sensor(0, this);
  } else {
	station_->sensor(lane_->localIndex(), this);
  }

  return 0;
}

void
RN_Sensor::print(ostream &os)
{
  os << indent << indent << "{";
  os << code_ << endc << prob_;
  if (!isLinkWide()) {
	os << endc << lane_->code();
  }
  os << "}" << endl;
}
