//-*-c++-*------------------------------------------------------------
// FILE: RN_CtrlStation.h
// DATE: Sat Oct 21 16:46:14 1995
//--------------------------------------------------------------------

#ifndef RN_CTRLSTATION_HEADER
#define RN_CTRLSTATION_HEADER

#include <iostream>
#include <vector>

#include <Templates/Object.h>
#include <Templates/Listp.h>

#include "Constants.h"
#include "RN_Signal.h"

class RN_Link;
class RN_Segment;

//--------------------------------------------------------------------
// CLASS NAME: RN_CtrlStation -- a surveillance station consists of
// one or more signals.  Ctrlerllace station is stored in a sorted
// list in each link according to their distance from the end of the
// link.  The sorting is in descending order (Upstream =
// LongerDistance = Top)
//--------------------------------------------------------------------

class RN_CtrlStation : public CodedObject
{
      friend class RoadNetwork;
      friend class RN_BisonParser;
      friend class RN_Signal;

   protected:

	  static float maxVisibility_;

      int type_;		// signal type 
      RN_Segment* segment_;	// pointer to segment
      float distance_;		// distance from the link end 
      float visibility_;	// length of detection zone 
      float position_;		// position in % from segment end
      std::vector<RN_Signal *> signals_; // array of pointers to signals 

   public:

      RN_CtrlStation() : CodedObject() { }
      ~RN_CtrlStation() { }

	  static inline float maxVisibility() { return maxVisibility_; }

      inline int type() { return (CTRL_SIGNAL_TYPE & type_); }
      inline int isLinkWide() { return (CTRL_LINKWIDE & type_); }
  
      inline RN_Segment* segment() { return segment_; }
      RN_Link* link();

      short int nLanes();

      // Returns pointer to the signal in ith lane. It may be NULL if
      // there is no signal in the ith lane.

      RN_Signal* signal(int i = 0);

      // Connect ith point to the signal

      void signal(int i, RN_Signal *signal);

      inline float distance() { return distance_; }
      inline float visibility() { return visibility_; }
	  void visibility(float d);
      float position() { return position_; }
	
	  int initWithoutInsert(int ty, float vis, int seg, float pos);
      virtual int init(int type, float vis, int seg, float pos);
	  void addIntoNetwork();

      virtual void print(std::ostream &os = std::cout);

      int cmp(CodedObject*) const;
	  int cmp(int c) const { return CodedObject::cmp(c); }
};

typedef Listp<RN_CtrlStation*>::link_type CtrlList;

#endif
