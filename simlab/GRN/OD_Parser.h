#ifndef YY_OD_Parser_h_included
#define YY_OD_Parser_h_included

#line 1 "/usr/lib/bison.h"
/* before anything */
#ifdef c_plusplus
#ifndef __cplusplus
#define __cplusplus
#endif
#endif
#ifdef __cplusplus
#ifndef YY_USE_CLASS
#define YY_USE_CLASS
#endif
#else
#endif
#include <stdio.h>

/* #line 14 "/usr/lib/bison.h" */
#line 21 "OD_Parser.h"
#define YY_OD_Parser_ERROR_BODY 	DEF_ERROR_BODY
#define YY_OD_Parser_LEX_BODY 	  DEF_LEX_BODY
#define YY_OD_Parser_CONSTRUCTOR_PARAM   OD_Table* pContainer_
#define YY_OD_Parser_CONSTRUCTOR_INIT    : _pContainer(pContainer_) \
                            ,_vtype(0)                \
             
#define YY_OD_Parser_MEMBERS  DEF_MEMBERS            \
    OD_Table* _pContainer ;            \
    int _vtype ;                        

#line 22 "OD_Parser.y"
typedef union {
  long        type_i ;
  double      type_f ;
  const char* type_s ;
} yy_OD_Parser_stype;
#define YY_OD_Parser_STYPE yy_OD_Parser_stype
#line 47 "OD_Parser.y"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <IO/Exception.h>

#include <Tools/Scanner.h>  
#include <Tools/ToolKit.h>  
#include <Tools/SimulationClock.h>
#include <Tools/Math.h>
#include <GRN/OD_Table.h>
#include <GRN/OD_Cell.h>
#include <GRN/Constants.h>

#line 14 "/usr/lib/bison.h"
 /* %{ and %header{ and %union, during decl */
#ifndef YY_OD_Parser_COMPATIBILITY
#ifndef YY_USE_CLASS
#define  YY_OD_Parser_COMPATIBILITY 1
#else
#define  YY_OD_Parser_COMPATIBILITY 0
#endif
#endif

#if YY_OD_Parser_COMPATIBILITY != 0
/* backward compatibility */
#ifdef YYLTYPE
#ifndef YY_OD_Parser_LTYPE
#define YY_OD_Parser_LTYPE YYLTYPE
/* WARNING obsolete !!! user defined YYLTYPE not reported into generated header */
/* use %define LTYPE */
#endif
#endif
#ifdef YYSTYPE
#ifndef YY_OD_Parser_STYPE 
#define YY_OD_Parser_STYPE YYSTYPE
/* WARNING obsolete !!! user defined YYSTYPE not reported into generated header */
/* use %define STYPE */
#endif
#endif
#ifdef YYDEBUG
#ifndef YY_OD_Parser_DEBUG
#define  YY_OD_Parser_DEBUG YYDEBUG
/* WARNING obsolete !!! user defined YYDEBUG not reported into generated header */
/* use %define DEBUG */
#endif
#endif
#ifdef YY_OD_Parser_STYPE
#ifndef yystype
#define yystype YY_OD_Parser_STYPE
#endif
#endif
/* use goto to be compatible */
#ifndef YY_OD_Parser_USE_GOTO
#define YY_OD_Parser_USE_GOTO 1
#endif
#endif

/* use no goto to be clean in C++ */
#ifndef YY_OD_Parser_USE_GOTO
#define YY_OD_Parser_USE_GOTO 0
#endif

#ifndef YY_OD_Parser_PURE

/* #line 63 "/usr/lib/bison.h" */
#line 107 "OD_Parser.h"

#line 63 "/usr/lib/bison.h"
/* YY_OD_Parser_PURE */
#endif

/* #line 65 "/usr/lib/bison.h" */
#line 114 "OD_Parser.h"

#line 65 "/usr/lib/bison.h"
/* prefix */
#ifndef YY_OD_Parser_DEBUG

/* #line 67 "/usr/lib/bison.h" */
#line 121 "OD_Parser.h"

#line 67 "/usr/lib/bison.h"
/* YY_OD_Parser_DEBUG */
#endif
#ifndef YY_OD_Parser_LSP_NEEDED

/* #line 70 "/usr/lib/bison.h" */
#line 129 "OD_Parser.h"

#line 70 "/usr/lib/bison.h"
 /* YY_OD_Parser_LSP_NEEDED*/
#endif
/* DEFAULT LTYPE*/
#ifdef YY_OD_Parser_LSP_NEEDED
#ifndef YY_OD_Parser_LTYPE
typedef
  struct yyltype
    {
      int timestamp;
      int first_line;
      int first_column;
      int last_line;
      int last_column;
      char *text;
   }
  yyltype;

#define YY_OD_Parser_LTYPE yyltype
#endif
#endif
/* DEFAULT STYPE*/
#ifndef YY_OD_Parser_STYPE
#define YY_OD_Parser_STYPE int
#endif
/* DEFAULT MISCELANEOUS */
#ifndef YY_OD_Parser_PARSE
#define YY_OD_Parser_PARSE yyparse
#endif
#ifndef YY_OD_Parser_LEX
#define YY_OD_Parser_LEX yylex
#endif
#ifndef YY_OD_Parser_LVAL
#define YY_OD_Parser_LVAL yylval
#endif
#ifndef YY_OD_Parser_LLOC
#define YY_OD_Parser_LLOC yylloc
#endif
#ifndef YY_OD_Parser_CHAR
#define YY_OD_Parser_CHAR yychar
#endif
#ifndef YY_OD_Parser_NERRS
#define YY_OD_Parser_NERRS yynerrs
#endif
#ifndef YY_OD_Parser_DEBUG_FLAG
#define YY_OD_Parser_DEBUG_FLAG yydebug
#endif
#ifndef YY_OD_Parser_ERROR
#define YY_OD_Parser_ERROR yyerror
#endif

#ifndef YY_OD_Parser_PARSE_PARAM
#ifndef __STDC__
#ifndef __cplusplus
#ifndef YY_USE_CLASS
#define YY_OD_Parser_PARSE_PARAM
#ifndef YY_OD_Parser_PARSE_PARAM_DEF
#define YY_OD_Parser_PARSE_PARAM_DEF
#endif
#endif
#endif
#endif
#ifndef YY_OD_Parser_PARSE_PARAM
#define YY_OD_Parser_PARSE_PARAM void
#endif
#endif

/* TOKEN C */
#ifndef YY_USE_CLASS

#ifndef YY_OD_Parser_PURE
extern YY_OD_Parser_STYPE YY_OD_Parser_LVAL;
#endif


/* #line 143 "/usr/lib/bison.h" */
#line 207 "OD_Parser.h"
#define	LEX_EQUAL	258
#define	LEX_COMMA	259
#define	LEX_COLON	260
#define	LEX_SEMICOLON	261
#define	LEX_OAB	262
#define	LEX_CAB	263
#define	LEX_OBB	264
#define	LEX_CBB	265
#define	LEX_OCB	266
#define	LEX_CCB	267
#define	LEX_INT	268
#define	LEX_REAL	269
#define	LEX_TIME	270
#define	LEX_PAIR	271
#define	LEX_TAG	272
#define	LEX_STRING	273
#define	LEX_DUP	274
#define	LEX_END	275


#line 143 "/usr/lib/bison.h"
 /* #defines token */
/* after #define tokens, before const tokens S5*/
#else
#ifndef YY_OD_Parser_CLASS
#define YY_OD_Parser_CLASS OD_Parser
#endif

#ifndef YY_OD_Parser_INHERIT
#define YY_OD_Parser_INHERIT
#endif
#ifndef YY_OD_Parser_MEMBERS
#define YY_OD_Parser_MEMBERS 
#endif
#ifndef YY_OD_Parser_LEX_BODY
#define YY_OD_Parser_LEX_BODY  
#endif
#ifndef YY_OD_Parser_ERROR_BODY
#define YY_OD_Parser_ERROR_BODY  
#endif
#ifndef YY_OD_Parser_CONSTRUCTOR_PARAM
#define YY_OD_Parser_CONSTRUCTOR_PARAM
#endif
/* choose between enum and const */
#ifndef YY_OD_Parser_USE_CONST_TOKEN
#define YY_OD_Parser_USE_CONST_TOKEN 0
/* yes enum is more compatible with flex,  */
/* so by default we use it */ 
#endif
#if YY_OD_Parser_USE_CONST_TOKEN != 0
#ifndef YY_OD_Parser_ENUM_TOKEN
#define YY_OD_Parser_ENUM_TOKEN yy_OD_Parser_enum_token
#endif
#endif

class YY_OD_Parser_CLASS YY_OD_Parser_INHERIT
{
public: 
#if YY_OD_Parser_USE_CONST_TOKEN != 0
/* static const int token ... */

/* #line 182 "/usr/lib/bison.h" */
#line 270 "OD_Parser.h"
static const int LEX_EQUAL;
static const int LEX_COMMA;
static const int LEX_COLON;
static const int LEX_SEMICOLON;
static const int LEX_OAB;
static const int LEX_CAB;
static const int LEX_OBB;
static const int LEX_CBB;
static const int LEX_OCB;
static const int LEX_CCB;
static const int LEX_INT;
static const int LEX_REAL;
static const int LEX_TIME;
static const int LEX_PAIR;
static const int LEX_TAG;
static const int LEX_STRING;
static const int LEX_DUP;
static const int LEX_END;


#line 182 "/usr/lib/bison.h"
 /* decl const */
#else
enum YY_OD_Parser_ENUM_TOKEN { YY_OD_Parser_NULL_TOKEN=0

/* #line 185 "/usr/lib/bison.h" */
#line 297 "OD_Parser.h"
	,LEX_EQUAL=258
	,LEX_COMMA=259
	,LEX_COLON=260
	,LEX_SEMICOLON=261
	,LEX_OAB=262
	,LEX_CAB=263
	,LEX_OBB=264
	,LEX_CBB=265
	,LEX_OCB=266
	,LEX_CCB=267
	,LEX_INT=268
	,LEX_REAL=269
	,LEX_TIME=270
	,LEX_PAIR=271
	,LEX_TAG=272
	,LEX_STRING=273
	,LEX_DUP=274
	,LEX_END=275


#line 185 "/usr/lib/bison.h"
 /* enum token */
     }; /* end of enum declaration */
#endif
public:
 int YY_OD_Parser_PARSE(YY_OD_Parser_PARSE_PARAM);
 virtual void YY_OD_Parser_ERROR(char *msg) YY_OD_Parser_ERROR_BODY;
#ifdef YY_OD_Parser_PURE
#ifdef YY_OD_Parser_LSP_NEEDED
 virtual int  YY_OD_Parser_LEX(YY_OD_Parser_STYPE *YY_OD_Parser_LVAL,YY_OD_Parser_LTYPE *YY_OD_Parser_LLOC) YY_OD_Parser_LEX_BODY;
#else
 virtual int  YY_OD_Parser_LEX(YY_OD_Parser_STYPE *YY_OD_Parser_LVAL) YY_OD_Parser_LEX_BODY;
#endif
#else
 virtual int YY_OD_Parser_LEX() YY_OD_Parser_LEX_BODY;
 YY_OD_Parser_STYPE YY_OD_Parser_LVAL;
#ifdef YY_OD_Parser_LSP_NEEDED
 YY_OD_Parser_LTYPE YY_OD_Parser_LLOC;
#endif
 int YY_OD_Parser_NERRS;
 int YY_OD_Parser_CHAR;
#endif
#if YY_OD_Parser_DEBUG != 0
public:
 int YY_OD_Parser_DEBUG_FLAG;	/*  nonzero means print parse trace	*/
#endif
public:
 YY_OD_Parser_CLASS(YY_OD_Parser_CONSTRUCTOR_PARAM);
public:
 YY_OD_Parser_MEMBERS 
};
/* other declare folow */
#endif


#if YY_OD_Parser_COMPATIBILITY != 0
/* backward compatibility */
#ifndef YYSTYPE
#define YYSTYPE YY_OD_Parser_STYPE
#endif

#ifndef YYLTYPE
#define YYLTYPE YY_OD_Parser_LTYPE
#endif
#ifndef YYDEBUG
#ifdef YY_OD_Parser_DEBUG 
#define YYDEBUG YY_OD_Parser_DEBUG
#endif
#endif

#endif
/* END */

/* #line 236 "/usr/lib/bison.h" */
#line 372 "OD_Parser.h"

#line 260 "OD_Parser.y"

  // empty
#endif
