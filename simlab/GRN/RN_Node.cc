//-*-c++-*------------------------------------------------------------
// FILE: RN_Node.C
// DATE: Wed Oct 18 12:36:02 1995
//--------------------------------------------------------------------

#include <cstdlib>
#include <vector>
#include <Tools/Math.h>
#include <Templates/Pointer.h>

#include <Tools/Random.h>
#include <Tools/SimulationClock.h>
#include <Tools/ToolKit.h>

#include "RoadNetwork.h"
#include "Parameter.h"

#include "RN_Node.h"
#include "RN_Link.h"
#include "RN_Path.h"
#include "RN_Vehicle.h"
#include "RN_PathTable.h"
#include "BusAssignmentTable.h"
#include "RN_BusRunTable.h"
#include "RN_LinkTimes.h"
#include "RN_DynamicRoute.h"
#include "RN_PathPointer.h"
#include "RN_IS.h"
#include <iostream>
using namespace std;

int RN_Node::sorted_ = 1;

//--------------------------------------------------------------------
// Requires: called by RN_Parser
// Modifies: code, type, and name of the node
//--------------------------------------------------------------------

int
RN_Node::init(int c, int t, const char *n)
{
   static int last = -1;
   type_ = t;
   code(c);
   name(n);
   if (sorted_ && code_ <= last) sorted_ = 0;
   else last = code_;
   index_ = theNetwork->nNodes();
   theNetwork->addNode(this);

   if (ToolKit::debug()) {
      cout << indent << "<"
		   << c << endc << t << endc << n
		   << ">" << endl;
   }

   return 0;
}

void
RN_Node::print(ostream &os)
{
   os << indent << "{";
   os << code() << endc << type_ << endc 
      << "\"" << name() << "\"";
   os << "}" << endl;
}


// Add a link into the upLink array

void
RN_Node::addUpLink(RN_Link *link)
{
   upLinks_.push_back(link);
}

// Add a link into the dnLink array

void
RN_Node::addDnLink(RN_Link *link)
{
   dnLinks_.push_back(link);
}


// Return local index of an outbound link or -1 if 'link' is not a
// downstream link of this node

int
RN_Node::whichDnLink(RN_Link* link)
{
   register int i;
   for (i = nDnLinks() - 1; i >= 0; i --) {
      if (dnLinks_[i] == link) break;
   }
   return i;
}


// Return local index of an inbound link or -1 if 'link' is not a
// upstream link of this node

int
RN_Node::whichUpLink(RN_Link* link)
{
   register int i;
   for (i = nUpLinks() - 1; i >= 0; i --) {
      if (upLinks_[i] == link) return (i);
   }
   return i;
}


// Sort outbound links based on their directions.  Direction is
// represebted by the angle counter-clockwise, with 3 O'clock
// being 0.

void
RN_Node::sortDnLinks(void)
{
   register int i, j;
   RN_Link* lastarc;
   RN_Link* toparc;
   // dnLinks_.resize();
   for (i = nDnLinks() - 1; i > 0; i--) {
      lastarc = dnLinks_[i];
      for (j = i - 1; j >= 0; j--) {
		 toparc = dnLinks_[j];
		 if (toparc->outboundAngle() > lastarc->outboundAngle()) {
			// swap toparc and lastarc
			dnLinks_[i] = toparc;
			dnLinks_[j] = lastarc;
			lastarc = toparc;
		 }
      }
   }
}


// Sort inbound links based on their directions.  Direction is
// represebted by the angle counter-clockwise, with 3 O'clock
// being 0.
 
void
RN_Node::sortUpLinks(void)
{
   register int i, j;
   RN_Link* lastarc;
   RN_Link* toparc;
   // upLinks_.resize();
   for (i = nUpLinks() - 1; i > 0; i --) {
      lastarc = upLinks_[i];
      for (j = i - 1; j >= 0; j --) {
		 toparc = upLinks_[j];
		 if (toparc->inboundAngle() > lastarc->inboundAngle()) {
			// swap toparc and lastarc
			upLinks_[i] = toparc;
			upLinks_[j] = lastarc;
			lastarc = toparc;
		 }
      }
   }
}


// Dynamically generate the next link of the vehicle's path

void
RN_Node::routeGenerationModel(RN_Vehicle* pv)
{
   // the link by which vehicle pv came

   RN_Link* slink = pv->link();

   if (slink && slink->dnNode() != this) {

      // Error in calling this function
      cout << "RN_Node.cc: Error in routeGenerationModel" << endl;
      pv->donePathIndex();
      return;
   }

   // destination node of this vehicle

   RN_Node * dnode = pv->desNode();

   if (nDnLinks() < 1 || this == dnode) {

      // reached destination

      pv->donePathIndex();
      return;
   }

   double util[MAX_NUM_OF_OUT_LINKS];
   const double NOT_CONNECTED = FLT_INF - 1.0;

   RN_Route* info = pv->routingInfo();
   
   double sum = 0.0;		// sum of utilities
   double cost;			// travel time
   register short int i;
   int itype = pv->infoType();
   float beta = theBaseParameter->routingBeta(itype);
   double entry = theSimulationClock->currentTime();

   // expected travel time from this node to destination node

   double cost0;
   RN_Link * plink;

   if (slink) {
      
	  cost0 = info->dnRouteTime(slink, dnode, entry);
	  entry += info->linkTime(slink->index(), entry);

   } else {
	  cost0 = FLT_INF;
	  for (i = 0; i < nDnLinks(); i ++) {
		 plink = dnLink(i);
		 cost = info->upRouteTime(plink, dnode, entry);
		 if (cost < cost0) {
			cost0 = cost;
		 }
	  }
   }

   plink = NULL;

   for (i = 0; i < nDnLinks(); i ++) {
      
	  plink = dnLink(i);

	  if (slink && !(slink->isMovementAllowed(plink))) {

		 util[i] = 0.0;	// Turn is not allowed

	  } else if (!(plink->isCorrect(pv))) {

		 util[i] = 0.0;	// Can not use the link

	  } else if ((cost = info->upRouteTime(
		 plink, dnode, entry)) >= NOT_CONNECTED) {

		 util[i] = 0.0;	// Not connected to destination

	  } else if (cost < cost0 * theBaseParameter->validPathFactor()) {

		 // Cost1 is the travel time on next link.

		 double cost1 = info->linkTime(plink, entry);
	 
		 // Cost2 is the travel time on the shorted path from the
		 // downstream end of plink to pv's destination.
	 
		 double cost2 = cost - cost1;
	 
		 // Add the diversion penalty if change from freeway to
		 // ramp/urban
		

		 // tomer - adding restriction such that the driver doesn't choose 
		 // a link that takes it further away from the destination.			

		 if (theBaseParameter->rationalLinkFactor() * cost2 <= cost0) {
			double cost3;
	      
		 	if (slink != NULL &&
				slink->linkType() == LINK_TYPE_FREEWAY &&
			 	plink->linkType() != LINK_TYPE_FREEWAY) {
				cost3 = theBaseParameter->diversionPenalty();
		 	} else {
				cost3 = 0;
		 	}

		 	cost = cost1 + cost2 + cost3;

		 	util[i] = exp(beta * cost / cost0);
		 	sum += util[i];
	    	}
	  } else { 
	   
		 util[i] = 0.0;	// this path is too long or contains a cycle
	  }
   }
		
   // Select one of the outgoing links based on the probabilities
   // calculated using a logit model
   
   if (sum > DBL_EPSILON) {	// At least one link is valid
      
	  // a uniform (0,1] random number
      
	  double rnd = theRandomizers[Random::Routing]->urandom();
	  double cdf;
	  for (i = nDnLinks() - 1, cdf = util[i] / sum;
		   i > 0 && rnd > cdf;
		   i --) {
		 cdf += util[i-1] / sum;
	  }
	  pv->setPathIndex(dnLink(i)->index());

   } else {			// No link is valid

	  if (!slink ||		// not enter the network yet
		  type(NODE_TYPE_EXTERNAL)) {

		 // will be removed (ori or external node)

		 pv->donePathIndex();

	  } else if ((i = slink->rightDnIndex()) != 0xFF) {

		 // choose the right most link, hopefully it is a off-ramp if
		 // freeway
	 
		 pv->setPathIndex(dnLink(i)->index());

	  } else {			// no where to go

		 pv->donePathIndex();
	  }
   }
}












// For vehicles that already has a path, they use this function to
// check whether they should keep their current paths or enroute.

void
RN_Node::routeSwitchingModel(RN_Vehicle* pv, OD_Cell* od)
{
   // the link from which the vehicle pv came

   RN_Link *slink = pv->link();

   if (slink && slink->dnNode() != this) {

      // Error in calling this function

     cout <<"RN_Node: there is an error in routeswitchingmodel\n";
      pv->donePathIndex();
      return;
   }

   // destination node of this vehicle

   RN_Node * dnode = pv->desNode();

   // Information used to routing the vehicle

   RN_Route* info = pv->routingInfo();

   RN_PathPointer* pp;
   int flag;
   register short int i, j, n;
   float cost;

   vector<Pointer<RN_PathPointer> ALLOCATOR> choices;
   vector<float ALLOCATOR> costs;





   // Prepare the choice sets and find the shortest route
   
   float smallest = FLT_INF;
   if (slink) {					// enroute
	  for (i = 0; i < slink->nPathPointers(); i ++) {
		 pp = slink->pathPointer(i);
		 if (pp->desNode() == dnode) { // goes to my desination
			// add to my choice set
			cost = pp->cost(info);
			costs.push_back(cost);
			choices.push_back(pp);
			if (cost < smallest) {
			   smallest = cost;
			}
		 }
	  }
	  flag = 1;
   } else {						// at the origin
	  for (i = 0; i < nDnLinks(); i ++) { // check each out going link
		 slink = dnLink(i);
		 for (j = 0; j < slink->nPathPointers(); j ++) { // each path
			pp = slink->pathPointer(j);

			// Dan: bus run paths should not be considered

			if (!theBusAssignmentTable || 
                                (theBusAssignmentTable && !theBusRunTable->findPath(pp->path()->code()))) {
		          if (pp->desNode() == dnode && // goes to my desination
		              (!od || pp->IsUsedBy(od))) {
			       // add to my choice set
			       cost = pp->cost(info);
			       costs.push_back(cost);
			       choices.push_back(pp);
			       if (cost < smallest) {
			            smallest = cost;
			       }
		          }
			}
		 }
	  }
	  flag = 0;
   }

   n = choices.size();

   if (n > 1) {

	  // Find the utility of choosing each route

	  double *util = new double [n];
	  int itype = pv->infoType();
	  float beta = theBaseParameter->routingBeta(itype);
	  float alpha = theBaseParameter->commonalityFactor();
	  double sum = 0.0;		// sum of utilities

	  for (i = 0; i < n; i ++) {
		 pp = choices[i]();
		 // diversion penalty
		 if (flag && pp->path() != pv->path()) {
			cost = theBaseParameter->pathDiversionPenalty();
		 } else {
			cost = 0;
		 }
		 cost += costs[i];
//Rama's change:
		 //util[i] = exp(beta * cost / smallest + alpha * pp->cf());
         util[i] = exp(beta * cost + alpha * pp->cf());
		 sum += util[i];
	  }
	 
	  // Select path based on the probabilities calculated using a
	  // logit model

	  if (sum > DBL_EPSILON) {

		 // a uniform (0,1] random number

		 double rnd = theRandomizers[Random::Routing]->urandom();

		 double cdf;
		 for (i = n - 1, cdf = util[i] / sum;
			  i > 0 && rnd > cdf;
			  i --) {
			cdf += util[i-1] / sum;
		 }
	  } else {
		 i = theRandomizers[Random::Routing]->urandom(n);
	  }

	  pp = choices[i]();
	  pv->setPath(pp->path(), pp->position() + flag);

	  delete [] util;

   } else if (n) {				// n == 1

	  pp = choices[0]();
	  pv->setPath(pp->path(), pp->position() + flag);

   } else {

      // No available path at this node. Switch to route generation
      // model

      pv->setPath(NULL);
      routeGenerationModel(pv);
   }
}

// tomer -set and get IS pointer functions

void 
 RN_Node::setInterS(RN_IS* is)
 {
      InterS_=is; 
 }

RN_IS*
    RN_Node::getInterS()
{
      return InterS_;
}

// **
