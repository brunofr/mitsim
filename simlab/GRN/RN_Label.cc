//-*-c++-*------------------------------------------------------------
// FILE: RN_Label.C
// DATE: Wed Oct 18 12:08:47 1995
//--------------------------------------------------------------------

#include <cstdlib>
#include <cstring>

#include "RoadNetwork.h"
#include "RN_Label.h"
#include <Tools/ToolKit.h>
using namespace std;

// we assume labels are sorted by code

int RN_Label::sorted_ = 1;

// Set value for a label. Called by RN_Parser

int
RN_Label::init(int c, const char *n)
{
  static int last = -1;
  if (c == 0) {
    cerr << "Error:: Label code <0> is not allowed. ";
    return -1;
  }
  name(n);
  code(c);
  length_ = name_ ? strlen(name_) : 0;
  if (sorted_ && code_ <= last) sorted_ = 0;
  else last = code_;

  theNetwork->addLabel(this);

  if (ToolKit::debug()) {
	cout << indent << "<"
		 << c << endc << n
		 << ">" << endl;
  }

  return 0;
}

void
RN_Label::print(ostream &os)
{
  os << indent << "{";
  os << code_ << endc << "\"" << name_ << "\"";
  os << "}" << endl;
}
