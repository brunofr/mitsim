//-*-c++-*------------------------------------------------------------
// FILE: RN_BusStop.cc
// DATE: June 2001
//--------------------------------------------------------------------


#include <Tools/ToolKit.h>
#include "RN_BusStop.h"
#include "RN_Lane.h"
#include "RN_CtrlStation.h"
#include "RN_SurvStation.h"
#include "RoadNetwork.h"
#include "BusAssignmentTable.h"
#include <iostream>
using namespace std;

int RN_BusStop::startIndexAsSignals_ = 0;

int
RN_BusStop::initBusStopChars(int c, int s, int l,
		   unsigned int r, unsigned int sl,
		   int sd, int wsd)
{
   if (!startIndexAsSignals_) {
      theNetwork->nStdSignals_ = theNetwork->nSignals();
      startIndexAsSignals_ = theNetwork->nSignals();
   }

   if (ToolKit::debug()) {
      cout << indent << indent << "<"
	   << c << endc
	   << hextag << hex << s << dec << endc
	   << l << endc << sd << endc << sl << endc
	   << ">" << endl;
   }
   rules_ = r;
   stopLength_ = sl;
   squeezeDummy_ = sd;
   waysideDummy_ = wsd;

   int error = (RN_Signal::init(c, s, l));
   theNetwork->nBusStops_ ++;
   addBusStopIntoNetwork();
   return error;
}


void
RN_BusStop::print(ostream &os)
{
   os << indent << indent << "{";
   os << code_ << endc << hextag << hex << state_ << dec << endc
      << lane_->code() << endc;
   os << rules_ << endc << stopLength_ << endc << squeezeDummy_
      << endc << waysideDummy_;
   os << "}" << endl;
}

void RN_BusStop::addBusStopIntoNetwork()
{
   index_ = theNetwork->nBusStops();
   theNetwork->addBusStop(this);
}

void
RN_BusStop::addRouteToBusStop (int code)
{ 
  nRoutesServeStop_++;
  routeid_.push_back(code); 
  lastbusid_.push_back(0);
  lastarrivaltime_.push_back(0.);
  lastschedarrival_.push_back(0.);
  lastheadway_.push_back(0.);
  lastbusdwell_.push_back(0.);
  passarrive_.push_back(0);
  passalight_.push_back(0);
  passengersleft_.push_back(0);
  arrivalrate_.push_back(0.);
  alightpct_.push_back(0.);
  numbusarrivals_.push_back(0);
  starttime_.push_back(0.);
  intervala_.push_back(0.);
  intervalb_.push_back(0.);
  intervalc_.push_back(0.);
  peak_.push_back(0.);
  lastbuscycle_.push_back(0);
  lastbusinterval_.push_back(0);
  lastbustime_.push_back(0.);
  lastwaittime_.push_back(0.);
  control_.push_back(0);
}

int
RN_BusStop::addRateParameters (int rcode, float ar, float al)
{ 
  vector<int>::iterator i;
  i = find_if(routeid_.begin(), routeid_.end(), CodeEqual(rcode));

  if (i != routeid_.end()) {
    for (int j = 0; j < nRoutesServeStop(); j ++) {
      if (routeid_[j] == rcode) {
        arrivalrate_[j] = ar;
        alightpct_[j] = al;
      }
    }
    return 0;
  }
  else {
    cerr << "Error:: Route number " << rcode << " does not serve bus stop <" << this->code() << ">. ";
    return (-1);
  }
}

int
RN_BusStop::addArrivalParameters(int rcode, float st, float a, float b, 
                                  float c, float peak, float alight)
{ 
  vector<int>::iterator i;
  i = find_if(routeid_.begin(), routeid_.end(), CodeEqual(rcode));

  if (i != routeid_.end()) {
    for (int j = 0; j < nRoutesServeStop(); j ++) {
      if (routeid_[j] == rcode) {
        starttime_[j] = st;
        intervala_[j] = a;
        intervalb_[j] = b;
        intervalc_[j] = c;
        peak_[j] = peak;
        alightpct_[j] = alight;
      }
    }
    return 0;
  }
  else {
    cerr << "Error:: Route number " << rcode << " does not serve bus stop <" << this->code() << ">. ";
    return (-1);
  }
}

int
RN_BusStop::enableControl(int rc)
{ 
  vector<int>::iterator i;
  i = find_if(routeid_.begin(), routeid_.end(), CodeEqual(rc));

  if (i != routeid_.end()) {
    for (int j = 0; j < nRoutesServeStop(); j ++) {
      if (routeid_[j] == rc) {
        control_[j] = 1;
      }
    }
    return 0;
  }
  else {
    cerr << "Error:: Route number " << rc << " does not serve bus stop <" << this->code() << ">. ";
    return (-1);
  }
}

bool
RN_BusStop::isControlEnabled(int rc)
{ 
  vector<int>::iterator i;
  i = find_if(routeid_.begin(), routeid_.end(), CodeEqual(rc));

  if (i != routeid_.end()) {
    for (int j = 0; j < nRoutesServeStop(); j ++) {
      if (routeid_[j] == rc) {
        if (control_[j] > 0) return true;
      }
    }
    return false;
  }
  else return false;
}
