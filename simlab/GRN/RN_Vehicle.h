//-*-c++-*------------------------------------------------------------
// FILE: RN_Vehicle.h
// DATE: Thu Oct 19 14:02:43 1995
//--------------------------------------------------------------------

#ifndef RN_VEHICLE_HEADER
#define RN_VEHICLE_HEADER

#include <fstream>

#include <Templates/Object.h>

#include "Constants.h"

const double DEFAULT_VEHICLE_WIDTH     = 1.8; /* meters */
const double DEFAULT_VEHICLE_LENGTH    = 5.5; /* meters */

class RN_Node;
class RN_Link;
class RN_Segment;
class RN_Path;
class RN_Route;
class RN_Lane;
class OD_Pair;
class OD_Cell;

// Driver attributes

const unsigned int ATTR_FAMILIARITY       = 0x00000001;

const unsigned int ATTR_TS_COMPLY         = 0x00000010;
const unsigned int ATTR_PS_COMPLY         = 0x00000020;
const unsigned int ATTR_RAMPMETER_COMPLY  = 0x00000040;

const unsigned int ATTR_LUS_COMPLY        = 0x00000300;	// sum
const unsigned int ATTR_LUS_COMPLY_RED    = 0x00000100;
const unsigned int ATTR_LUS_COMPLY_YELLOW = 0x00000200;

const unsigned int ATTR_GLU_RULE_COMPLY   = 0x00000400;
const unsigned int ATTR_GLC_RULE_COMPLY   = 0x00000800;

const unsigned int ATTR_MSG_TYPE_LU_COMPLY= 0x00001000;
const unsigned int ATTR_MSG_PATH_LU_COMPLY= 0x00002000;
const unsigned int ATTR_MSG_GUIDE_COMPLY  = 0x00004000;

const unsigned int ATTR_ACCESSED_INFO     = 0x00010000;

const int NO_EXPLICIT_PATH   = -1;
const int NO_EXPLICIT_LANE   = -1;

const unsigned char OVER_HEIGHT_TRUE  = 1;
const unsigned char OVER_HEIGHT_FALSE = 0;

class RN_Vehicle : public CodedObject
{
   protected:

      int direction_;            // vehicle direction

      int type_;		// vehicle type and class

      int busType_;             // if vehicle is a bus, type of bus != 0
                                // if not a bus, type of bus = 0 (Dan)
      int routeID_;             // if vehicle is a bus, route ID != 0
                                //if not a bus, route ID = 0 (Dan)

      float length_;		// vehicle length

      unsigned int attrs_;	// driver attributes

      OD_Pair* od_;		// od pair

      RN_Path* path_;		// path this vehicle will follow

      // index in path (list of links) if path is defined or index to
      // network link array if path is not defined.

      int pathIndex_;
      unsigned int info_;	// previous received info (e.g. vms)

      RN_Link *nextLink_;	// next link on its path
      float departTime_;
      float timeEntersLink_;	// time enters current link
      float distance_;			// distance from downstream end
      float mileage_;		// total distance traveled
      float currentSpeed_;		// current speed in meter/sec

	  static int lastId_;		// id of the last vehicle generated

   public:

      RN_Vehicle();
      virtual ~RN_Vehicle() { }

      // Driver atributes

      void toggleAttr(unsigned int attr) {
		 attrs_ ^= attr;
      }
      inline unsigned int attr(unsigned int mask = 0xFFFFFFFF) {
         return (attrs_ & mask);
      }
      inline void setAttr(unsigned int s) {
         attrs_ |= s;
      }
      inline void unsetAttr(unsigned int s) {
         attrs_ &= ~s;
      }

      virtual float length() { return length_; }
      
	  virtual void initialize() = 0; // called by init()

      virtual int init(int id, int t, OD_Pair *od, RN_Path *p);

      virtual int initBus(int bid, OD_Pair *od, RN_Path *p);

      virtual int initRapidBus(int t, OD_Pair *od, int rid, int bt, double hw);

      // This function is called by vehicle table parser. It sould
      // returns 0 if the initialization is successful, -1 if error
      // (it causes program to quit) and 1 if warning error.  A none
      // zero return value also indicate the caller to delete this
      // vehicle.  The last two arguments are optional
      
      virtual int init(int id,
					   int ori_node_id, int des_node_id,
					   int type_id = 0, int path_id = -1);

      virtual int initBus(int bid,
					   int ori_node_id, int des_node_id,
					   int path_id);

      virtual void print(std::ostream &os = std::cout);

      inline OD_Pair* od() { return od_; }
	  RN_Node* desNode();
	  RN_Node* oriNode();
      inline RN_Path* path() { return path_; }

      inline int isType(int flag) { return type_ & flag; }
      inline int types() { return type_; }
      inline int type() { return type_ & VEHICLE_CLASS; }
      inline int group() { return type_ & VEHICLE_GROUP; }
      inline int isGuided() {
		 return (type_ & VEHICLE_GUIDED) ? 1 : 0;
      }
      inline int infoType() { return isGuided(); }
      inline float departTime() { return departTime_; }

      inline float currentSpeed() { return currentSpeed_; }
      inline float distance() { return distance_; }
	  inline float mileage() { return mileage_; }
      float distanceFromDownNode();

      // Current link the vehicle stays

      virtual RN_Link* link() { return NULL; }
      virtual RN_Segment* segment() { return NULL; }
	  virtual RN_Lane* lane() { return NULL; }

      // Path
      inline RN_Link* nextLink() { return nextLink_; }
      void setPathIndex(short int i);
      void donePathIndex();
      virtual int enRoute() { return 0; } // check if enroute
      virtual int isLinkInPath(RN_Link *, int depth = 0xFFFF);

      // These functions are called only if the path is defined.

      void setPath(RN_Path *p) { path_ = p; pathIndex_ = 0; }
      void setPath(RN_Path *p, short int i);
      void advancePathIndex();
      void retrievePathIndex();

      RN_Link* prevLinkOnPath();


      // Route choice model

	  virtual RN_Route* routingInfo();
      virtual void OnRouteChoosePath(RN_Node *);
      virtual void PretripChoosePath(OD_Cell *);
     
  // tomer - for the vehicles read from vehicle table file
  
      virtual void PretripChoosePath();

      // MOE

      float timeSinceDeparture();
      float timeEntersLink() { return timeEntersLink_; }
      float timeInLink();
      float speedInLink();
      void writePathRecord(std::ofstream &os);

	  void dumpState(std::ofstream& os);

	  static void resetLastId() { lastId_ = 0; }

	  static void openDepartureRecords(const char *name);
	  static void nextBlockOfDepartureRecords();
	  void saveDepartureRecord();
	  static void closeDepartureRecords();

   private:
	  
	  static int nVehicles_;
	  static std::ofstream osDepRecords_;

      void setNextLink();
};

#endif
