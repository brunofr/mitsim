#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <fstream>
using namespace std;

#include <IO/Exception.h>
#include "RN_Parser.h"

bool RN_Scanner::open(const char* szFilename_)
{
    _szFilename = szFilename_ ;
    _pis = new ifstream(szFilename_) ;
    if (_pis->good()) {
        switch_streams(_pis, 0);
        return (true) ;
    } else {
        delete _pis ;
        _pis = NULL ;
        fprintf(stderr, "Cannot open input file \"%s\".\n", szFilename_);
        perror(szFilename_) ;
        return (false) ;
    }
}

bool RN_Scanner::close()
{
  if (!_pis) return (false);
  switch_streams(0, 0);
  delete _pis ;
  _pis = NULL ;
  return (true) ;
}

bool RN_Scanner::good()
{
  if (!_pis || _pis->eof() || !_pis->good())
	return (false) ;
  else
	return (true) ;
}

// Returns the text currently in the buffer.

const char* RN_Scanner::text()
{
    return (const char *) YYText() ;
}

// Returns the text currently in the buffer.  It can optionally remove
// leading and trailing delimeters such as [], {} or "" and spaces
// from the parsed text.

const char* RN_Scanner::value(const char* delis_) {
    char *szStr = (char *) YYText() ;
    if (!delis_ || !szStr) return szStr ;
    while (*szStr != '\0' &&
           (*szStr == delis_[0] || isspace(*szStr))) {
        szStr ++ ;
    }
    int n = strlen(szStr) - 1 ;
    while (n >= 0 &&
           (szStr[n] == delis_[1] || isspace(szStr[n]))) {
        szStr[n] = '\0' ;
    }
    return (szStr);
}


// Show error message and quit if the error is fatal.

void RN_Scanner::check(int err_no, const char* szMsg_)
{
    static const char* fallback_name = "Line" ;
    static const char* fallback_msgs[] = {
        "Problem",
        "Catastrophic error"
    } ;

    int i ;
    if (err_no > 0) i = 0 ;
    else if (err_no < 0) i = 1 ;
    else return ;

    const char* name = _szFilename ? _szFilename : fallback_name ;
    const char* msg = szMsg_ ? szMsg_ : fallback_msgs[i] ;

    fprintf(stderr, "%s:%d (%d): %s.", name, lineno(), err_no, msg) ;

    if (YYLeng() > 0) {
	    const char* str = YYText() ;
        fprintf(stderr, " Scanner jammed at \"%s\".\n", str) ;
    } else {
        fprintf(stderr, "\n") ;
    }
	theException->exit(err_no) ;
}
