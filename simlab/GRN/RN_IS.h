// Intersection



#ifndef RN_IS_HEADER
#define RN_IS_HEADER

#include <new>
#include <vector>
#include <GRN/RN_Node.h>
#include <GRN/RoadNetwork.h>
#include <Tools/ToolKit.h>
#include "Parameter.h"

class CManuver
{
  friend class RN_IS;

 public:
	typedef unsigned long MID ;


private:
	MID _id ;			// id of the movement (from/to)

	std::vector <MID> _confs ;
	std::vector <MID> _fifos ;

	float _critical_gap ;
	float _turn_speed ;

public:

        
	MID getID() {return _id;}

	void setCriticalGap(float cg) {
	  _critical_gap=cg;
	}

	void setTurnSpeed(float ts) {
	  if (ts>0){
	  _turn_speed = theBaseParameter->speedFactor() * ts;
	  }
	  else {
	    _turn_speed = SPEED_EPSILON;
	  }
	}

	float getTurnSpeed() {return _turn_speed; }

	float getCriticalGap() {return _critical_gap; }

	int uplane() { return (_id >> 16) ; }
	int dnlane() { return (_id & 0xFFFF) ; }

	int uplane(MID id_) { return (id_ >> 16) ; }
	int dnlane(MID id_) { return (id_ & 0xFFFF) ; }

	inline int init (int uplane_, int dnlane_)
	  { 
	    _id = (uplane_ << 16) | dnlane_ ;
	    return 0;
	  }
	
 

	void addConflict (int uplane_, int dnlane_) {
	  MID c = (uplane_ << 16) | dnlane_ ; 
	  _confs.push_back (c);
	}


	MID getConfs(int n) { return _confs[n]; }

	int NumOfConfs() { return _confs.size(); }



	void addFifo (int uplane_, int dnlane_) {
	  MID f = (uplane_ << 16) | dnlane_ ; 
	  _fifos.push_back (f);
	}
};


// ------------------------------------------------------------------ //


class RN_IS
{
  friend class RN_Node;
  friend class TS_Vehicle;
 
protected:  
 
  std::vector <CManuver*> _manuvers ;
  RN_Node* _node ;
  int _indx ;	       			// index of intersection in array
 
public:

  void setnode(int n);

  virtual int init(int c);
   
  virtual void addManuver(CManuver* m);
  
  virtual CManuver* newManuver();
  
  CManuver* getManuver(int i) {return _manuvers[i];}

  int NumOfManuvers() {			// # of movements in the intersection
	  return _manuvers.size() ;
  }

};  

#endif
