//-*-c++-*------------------------------------------------------------
// FILE: RN_BusSchedule.h
// DATE: Thu Oct 19 17:29:19 1995
//--------------------------------------------------------------------

#ifndef RN_BUSSCHEDULE_HEADER
#define RN_BUSSCHEDULE_HEADER

#include <iostream>
#include <vector>

#include <Templates/Object.h>

class RoadNetwork;
class RN_Link;
class RN_Node;
class RN_BusScheduleParser;
class RN_BusScheduleTable;
class RN_BusRoute;
class RN_BusRouteTable;
class RN_BusRun;
class BusAssignment;
class RN_LinkTimes;
class RN_BusStop;

class RN_BusSchedule : public CodedObject
{
      friend class RN_BusScheduleParser;
      friend class RN_BusScheduleTable;
      friend class RoadNetwork;
      friend class RN_BusRun;
      friend class BusAssignment;

  // Define "arrival" as a a scheduled arrival time at any stop

   protected:

      static int sorted_;	// 1=sorted 0=arbitrary order

      int index_;		// index in the schedule table array
      int routeID_;              // route to which the scheduled trip is assigned
      std::vector<double> arrivals_;	// list of scheduled arrival times on the bus route
      std::vector<RN_BusStop*> stops_; // list of stops on trip, corresponding to arrivals vector

   public:
	
      RN_BusSchedule() : CodedObject() { }
      ~RN_BusSchedule() { }

      static inline int sorted() { return sorted_; }

      int nArrivals() { return arrivals_.size(); }
      double arrival(int i) {
		 if (i < 0) return -1;
		 else if (i >= nArrivals()) return -1;
		 else return arrivals_[i];
	  }
      double nextArrival(int i);
      double firstArrival() {
		if (nArrivals() > 0) return arrivals_[0];
		else return -1;
	  }
      double lastArrival() {
		if (nArrivals()) return arrivals_[nArrivals() - 1];
		else return -1;
	  }

      int index() { return index_; }

      int routeID() { return routeID_; }

      int nBusStops() { return stops_.size(); }
      RN_BusStop* findBusStop(int c);
      virtual int addBusStop(int c);
      RN_BusStop* lastBusStop() {
		if (nBusStops()) return stops_[nBusStops() - 1];
		else return NULL;
	  }

      // These two are called by RN_BusScheduleParser

      virtual int init(int code);
      virtual int addArrival(double t);

      virtual void print(std::ostream &os);

      RN_BusRoute* RN_BusSchedule::tripRoute();
};

#endif
