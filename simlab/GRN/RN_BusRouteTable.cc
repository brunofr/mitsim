//-*-c++-*------------------------------------------------------------
// NAME: A parser for bus route tables
// AUTH: Qi Yang & Daniel Morgan
// FILE: BusRouteTable.C
// DATE: Fri Nov 30 16:59:15 2001
//--------------------------------------------------------------------

#include <algorithm>

#include <Tools/ToolKit.h>
#include "RoadNetwork.h"
#include "RN_Link.h"
#include "RN_BusRouteTable.h"
#include "RN_BusRoute.h"
#include "RN_BusStop.h"
#include "OD_Cell.h"
#include "RN_LinkTimes.h"
#include "RN_BusRouteParser.h"
#include "BusAssignmentTable.h"
using namespace std;

// theBusRouteTable will be initialized in the ParseBusRouteTables() func in
// TS_Setup.C

RN_BusRouteTable * theBusRouteTable = NULL;

char * RN_BusRouteTable::name_ = NULL;

RN_BusRouteTable::~RN_BusRouteTable()
{
   if (name_) {
	  delete [] name_;
	  name_ = NULL;
   }
}

// This may be overloaded by derived class
 
RN_BusRoute*
RN_BusRouteTable::newBusRoute()
{
   return new RN_BusRoute;
}


int
//RN_BusRouteTable::addBusRoute(int code, double hw = 0.0)
RN_BusRouteTable::addBusRoute(int code, double hw)
{
   RN_BusRoute *br = newBusRoute();
   busroutes_.push_back(br);
   return br->init(code, hw);
}

RN_BusRoute* RN_BusRouteTable::findBusRoute(int c)
{
   vector<RN_BusRoute*>::iterator i;
   i = find_if(busroutes_.begin(), busroutes_.end(), CodeEqual(c));
   if (i != busroutes_.end()) return *i;
   else return NULL;
}

void
RN_BusRouteTable::print(ostream &os)
{
   register int i;
   os << "// Bus route table generated from file <" << name_ << "> on "
      << TimeTag() << " by "
      << UserName() << endl << endl;
   os << nBusRoutes() << indent << "// Number of bus routes" << endl;
   os << "{" << endl;
   for (i = 0; i < nBusRoutes(); i ++) {
      busroute(i)->print(os);
   }
   os << "}" << endl;
}

int
RN_BusRouteTable::enableControl(int rc, int sc)
{
   RN_BusStop *tpptr = theNetwork->findBusStop(sc);
   if (!tpptr) {
      cerr << "Error:: Unkonwn bus stop/sensor code <" << sc << ">. ";
      return -1;
   }
   return(tpptr->enableControl(rc));
}
