// Intersection



#include <new>
#include <vector>
#include <GRN/RN_Node.h>
#include <GRN/RoadNetwork.h>
#include <Tools/ToolKit.h>
#include <GRN/RN_IS.h>
using namespace std;


int
RN_IS::init(int c)
{
   _indx = theNetwork->nIS();
   theNetwork->addIS(this);
   setnode (c) ;
   _node->setInterS(this) ;
RN_IS* q=_node->getInterS();

   if (ToolKit::debug()) {
     cout << indent << "<"
	  << c << endc << ">" << endl;
   }
   
   return 0;
}
  
void 
RN_IS::setnode(int n) 
{
  _node = theNetwork->findNode(n) ;
}

void 
RN_IS::addManuver(CManuver* m) 
{
  _manuvers.push_back(m);
}

  
CManuver* 
RN_IS::newManuver() 
{
  return new CManuver;
}
