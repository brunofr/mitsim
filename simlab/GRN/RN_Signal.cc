//-*-c++-*------------------------------------------------------------
// FILE: RN_Signal.C
// AUTH: Qi Yang
// DATE: Sat Oct 21 14:35:21 1995
//--------------------------------------------------------------------


#include "RN_Signal.h"

#include <Tools/ToolKit.h>

#include "Parameter.h"
#include "Constants.h"

#include "RoadNetwork.h"
#include "RN_Node.h"
#include "RN_Link.h"
#include "RN_Segment.h"
#include "RN_Lane.h"
#include "RN_Signal.h"
#include "RN_CtrlStation.h"
using namespace std;

int RN_Signal::sorted_ = 1;


RN_Signal::RN_Signal()
   : CodedObject(),
     lane_(NULL),
     station_(NULL),
     index_(-1),
     state_(0)
{
}

// Turn off particular bits

unsigned int
RN_Signal::clearState(unsigned int flag)
{
   return (state_ &= ~flag);
}


// Turn on particular bits

unsigned int
RN_Signal::setState(unsigned int flag)
{
   return (state_ |= flag);
}


// Iterators on global array in the network

RN_Signal*
RN_Signal::prev()
{
   if (index_ > 0)
      return theNetwork->signal(index_ - 1);
   else
      return NULL;
}


RN_Signal*
RN_Signal::next()
{
   if (index_ < (theNetwork->nSignals() - 1))
      return theNetwork->signal(index_ + 1);
   else
      return NULL;
}


// Returns 1 if the signal is like-wide or 0 if it is lane specific

int
RN_Signal::isLinkWide()
{
   return station_->isLinkWide();
}


// Returns the signal type

int
RN_Signal::type()
{
   return station_->type();
}


// Distance from the end of the link

float
RN_Signal::distance()
{
   return station_->distance();
}

// Relative distance from the end of the segment

float
RN_Signal::position()
{
   return station_->position();
}


// Visibility distance

float
RN_Signal::visibility()
{
   return station_->visibility();
}


// Segment that contains this signal

RN_Segment*
RN_Signal::segment()
{
   if (station_) return station_->segment();
   else if (lane_) return lane_->segment();
   else return NULL;
}


// Link that contains this signal

RN_Link*
RN_Signal::link()
{
   return segment()->link();
}

unsigned int
RN_Signal::stateIoToInternal(unsigned int sign)
{
   if (type() != CTRL_VMS) return sign;
   unsigned int s = SIGN_TYPE & sign; 
   if (s == SIGN_LANE_USE_PATH || s == SIGN_PATH) {
      // A path related message.  Use link index instead of id
      s = SignSuffix(sign);
      RN_Link *pl = theNetwork->findLink(s);
      if (pl) {
		 sign = (SignPrefix(sign) | pl->index());
      } else {
		 cerr << "Error:: Unknown link ID <" << s << ">. ";
		 sign = SIGN_ERROR;
      }
   }
   return sign;
}

unsigned int
RN_Signal::stateInternalToIo(unsigned int sign)
{
   if (type() != CTRL_VMS) return sign;
   unsigned int s = SIGN_TYPE & sign; 
   if (s == SIGN_LANE_USE_PATH || s == SIGN_PATH) {
      // A path related message.  Convert id to link index
      s = SignSuffix(sign);
      RN_Link *pl = theNetwork->link(s);
      sign = (SignPrefix(sign) | pl->code());
   }
   return sign;
}

// Called by RN_Parser to set signal variables. Returns 0 if no error.

int
RN_Signal::init(int c, unsigned int s, int l)
{
   static int last = -1;

   if (ToolKit::debug()) {
      cout << indent << indent << "<" << c << endc
		   << hextag << hex << s << dec << endc
		   << l << ">" << endl;
   }

   if (station_) {
      cerr << "Error:: Signal <" << c << "> "
		   << "cannot be initialized twice. ";
      return -1;
   } else {
      station_ = theNetwork->lastCtrlStation();
   }

   if (sorted_ && c <= last) sorted_ = 0;
   else last = c;
   code_ = c;

   state_ = stateIoToInternal(s);
   if (state_ == SIGN_ERROR) return -1;	// error

   if (l < 0) {

      RN_Segment* ps = segment();

      // central lane

      lane_ = theNetwork->lane(ps->leftLaneIndex() + ps->nLanes() / 2);

   } else {
      if (!(lane_ = theNetwork->findLane(l)) ||
		  (lane_->segment() != segment())) {
		 cerr << "Error:: Unknown lane ID <" << l << ">. ";
		 return -1;
      }
   }

   addIntoNetwork();

   return 0;
}

void RN_Signal::addIntoNetwork()
{
   index_ = theNetwork->nSignals();
   theNetwork->addSignal(this);
   if (isLinkWide())
      station_->signal(0, this);
   else
      station_->signal(lane_->localIndex(), this);
}


void
RN_Signal::print(ostream &os)
{
   os << indent << indent << "{";
   os << code_ << endc << hextag << hex << state_ << dec;
   if (!isLinkWide()) {
	  os << endc << lane_->code();
   }
   os << "}" << endl;
}


// These two functions applies to traffic signals only

unsigned int
RN_Signal::stateForExit(int i)
{
   return ((state_ >> (i * SIGNAL_BITS)) & SIGNAL_STATE);
}

unsigned int
RN_Signal::stateForExit(unsigned int s, int i)
{
   return ((s >> (i * SIGNAL_BITS)) & SIGNAL_STATE);
}

// Dan: This function is used for bus stops, to determine
// whether the stop is in the leftmost or rightmost lane

int 
RN_Signal::leftOrRight()
{
  if ((lane_->code()) == (segment()->leftLane()->code())) {
    return -1;
  }
  else if ((lane_->code()) == (segment()->rightLane()->code())) {
    return 1;
  }
  else return 0;
} 


