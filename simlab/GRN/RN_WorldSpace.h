//-*-c++-*------------------------------------------------------------
// NAME: Massachusetts Institue of Technology
// AUTH: Peter J. Welch and Qi Yang                
// FILE: DRN_WorldSpace.h
// DATE: Thu Mar 23 12:01:24 1995
//--------------------------------------------------------------------

#ifndef RN_WorldSpace_HEADER
#define RN_WorldSpace_HEADER


// CLASS NAME : DRN_WorldSpace This class simply represents the world
// coordinate system which is defined and initialized by the road
// network as it is read in.
//
// This class is created by the DMNetwork since there is a one-to-one
// correspondance between a Network and its WorldSpace.
// DRN_WorldSpace is referenced by DMViewWindows so they can define a
// transformation from world to view space.

#include "WcsPoint.h"

class RN_WorldSpace
{
   public:

      RN_WorldSpace();
      virtual ~RN_WorldSpace() { }

      void recordExtremePoints(const WcsPoint & point);
      void createWorldSpace();

      // Convert a point from original network database cooridinates
      // to work space coordinates.

      WcsPoint worldSpacePoint(const WcsPoint &p);
      WcsPoint worldSpacePoint(double x, double y);

      // Convert a point from work space coordinates to original
      // network database cooridinates

      WcsPoint databasePoint(const WcsPoint &p);
      WcsPoint databasePoint(double x, double y);

      inline WcsPoint& sw_pnt() { return sw_pnt_; }
      inline WcsPoint& ne_pnt() { return ne_pnt_; }
      inline WcsPoint& southWestPoint() { return sw_pnt_; }
      inline WcsPoint& northEastPoint() { return ne_pnt_; }
      
      inline WcsPoint& ll_pnt() { return ll_pnt_; }
      inline WcsPoint& ur_pnt() { return ur_pnt_; }
      inline WcsPoint& lowerLeftPoint() { return ll_pnt_; }
      inline WcsPoint& upperRightPoint() { return ur_pnt_; }

      inline WcsPoint& center() { return center_; }
      inline double width() { return width_; }
      inline double height() { return height_; }
      inline double angle() { return angle_; }

   private:

      // These two coordinates define the boundaries of world space.
      // They will be set dynamically as a network is loaded, and at
      // the end of the load they will coorespond to the maximum end
      // points of the network

      WcsPoint ne_pnt_;
      WcsPoint sw_pnt_;

      // These are point in world space

      WcsPoint ll_pnt_;		// for sw_pnt_, should be (0, 0)
      WcsPoint ur_pnt_;		// for ne_pnt_

      // These varables are set by function createWorldSpace(), which
      // is called after the network has been loaded.  Their values
      // will not change afterwards.

      double width_;	/* west-east width */
      double height_;	/* south-north height */
      WcsPoint center_;
      double angle_;	/* angle from center to
			 * farthest east point */
};

extern RN_WorldSpace * theWorldSpace;

#endif
