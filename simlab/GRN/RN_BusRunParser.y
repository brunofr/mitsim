//
// RN_BusRunParser.y -- RN_BusRun table parser generator
// by Qi Yang & Daniel Morgan
//

%name   RN_BusRunParser
%define ERROR_BODY	DEF_ERROR_BODY
%define LEX_BODY	  DEF_LEX_BODY

// These macros are expanded when creating the definition for named
// BisonParser class.  The functions defined as member here are
// designed for use in the grammar rules.

%define CONSTRUCTOR_PARAM  RN_BusRunTable* pContainer_
%define CONSTRUCTOR_INIT   : _pContainer(pContainer_)  \
                           ,_current(NULL)             \
                           ,_ori(0)                    \
                           ,_des(0)                    \
                           ,_pathid(0)                 \

%define MEMBERS            DEF_MEMBERS \
    RN_BusRunTable* _pContainer ;      \
    RN_Path* _current ;                \
    int _ori ;                         \
    int _des ;                         \
    int _pathid ;                      \

%union {
  long        type_i ;
  double      type_f ;
  const char* type_s ;
}

%token <type_s> LEX_EQUAL
%token <type_s> LEX_COMMA
%token <type_s> LEX_COLON
%token <type_s> LEX_SEMICOLON
%token <type_s> LEX_OAB
%token <type_s> LEX_CAB
%token <type_s> LEX_OBB
%token <type_s> LEX_CBB
%token <type_s> LEX_OCB
%token <type_s> LEX_CCB
%token <type_i> LEX_INT
%token <type_f> LEX_REAL
%token <type_s> LEX_TIME
%token <type_s> LEX_PAIR
%token <type_s> LEX_TAG
%token <type_s> LEX_STRING
%token <type_s> LEX_DUP
%token <type_s> LEX_END

%header{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <IO/Exception.h>

#include <Tools/Scanner.h>
#include <Tools/ToolKit.h>  

#include <GRN/RN_BusRunTable.h>
#include <GRN/RN_BusSchedule.h>
#include <GRN/RN_BusRoute.h>
#include <GRN/RN_Link.h>
%}

//--------------------------------------------------------------------
// Symbols used road network database grammer rules.
//--------------------------------------------------------------------

%type <type_s> block_name
%type <type_i> inum num_objects specified unspecified
	   		 
%type <type_s> run_table run_table_allocator bus_run_id
%type <type_s> multi_bus_runs bus_run
%type <type_s> multi_trips trip

%start everything

%%

//--------------------------------------------------------------------
// Beginning of RN_BusRoute table grammer rules.
//--------------------------------------------------------------------

everything: run_table
{
  if (ToolKit::debug()) {
	cout << "Finished parsing bus run table <"
		 << filename() << ">." << endl;
  }
}
;

run_table: block_name run_table_allocator LEX_OCB LEX_CCB
| block_name run_table_allocator LEX_OCB multi_bus_runs LEX_CCB
{
  int n = _pContainer->nBusRuns();
  if (ToolKit::debug()) {
	cout << indent << n << " parsed." << endl;
  }
}
;

run_table_allocator: num_objects
{
  int n = ($1 > 0) ? ($1) : (NUM_BUSRUNS);
  _pContainer->paths_.reserve(n);
  _pContainer->runs_.reserve(n);
}
;

multi_bus_runs: bus_run
| multi_bus_runs bus_run
;

bus_run: LEX_OCB bus_run_id LEX_OCB multi_trips LEX_CCB LEX_CCB
{
   _ori = _pContainer->lastBusRun()->firstTrip()->tripRoute()->firstNode();
   _pContainer->lastBusRun()->oricode_ = _ori;
   _des = _pContainer->lastBusRun()->lastTrip()->tripRoute()->endNode();
   _pContainer->lastBusRun()->descode_ = _des;
   int err_no = _pContainer->addPath(_pathid, _ori, _des);
   check(err_no);

   for(int i = 0; i < _pContainer->lastBusRun()->nTrips(); i ++) {
     for (int j = 0; j < _pContainer->lastBusRun()->trip(i)-> \
	    tripRoute()->nRouteLinks(); j ++) {

       int linkcode = _pContainer->lastBusRun()->trip(i)-> \
            tripRoute()->routelink(j)->code();

       int err_no = _pContainer->lastPath()->addLink(linkcode);
       check(err_no);
     }
   }
}
;

bus_run_id: inum
{
  // $1=RunID
  _pathid = $1;
  int err_no = _pContainer->addBusRun($1);
  check(err_no) ;
}
;

multi_trips: trip
| multi_trips trip
;

trip: inum
{
  // $1=TripID
  int err_no = _pContainer->lastBusRun()->addTrip($1);
  check(err_no);
}
;

//--------------------------------------------------------------------
// Beginning of basic grammer rules.
//--------------------------------------------------------------------

block_name: LEX_TAG
{
  const char *s = value("[]");
  if (ToolKit::debug()) {
    cout << "Parsing " << s << " ..." << endl;
  }
  $$ = s;
}
;

inum: LEX_INT
{
  $$ = atoi(value());
}
;

num_objects: specified
| unspecified
{
  $$ = $1;
}
;
 
specified: LEX_COLON inum
{
  $$ = $2;
}
;

unspecified: LEX_COLON
{
  $$ = 0;
}
;

%%

// Following pieces of cpte will be verbosely copied into the parser.

%header{
  // empty
%}
