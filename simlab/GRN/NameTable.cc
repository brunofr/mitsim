//-*-c++-*------------------------------------------------------------
// NAME: Simulation Widges
// NOTE: 
// AUTH: Qi Yang
// FILE: NameTable.C
// DATE: Thu Nov  2 11:16:33 1995
//--------------------------------------------------------------------

#include <string>

#include "NameTable.h"
#include "Constants.h"
using namespace std;

const char* NameTable::controller(int code)
{
   switch (code) {
	  case CONTROLLER_UNSIGNALIZED:
		 return "Unsignalized";
	  case CONTROLLER_PRETIMED:
		 return "Pretimed";
	  case CONTROLLER_ACTIVATED:
		 return "Traffic Activated";
	  case CONTROLLER_GENERIC:
		 return "Generic"; // (Angus)
	  case CONTROLLER_RESPONSIVE_LOCAL:
		 return "Local Responsive";
	  case CONTROLLER_RESPONSIVE_AREA:
		 return "Area Responsive";
	  case CONTROLLER_RESPONSIVE_BILEVEL:
		 return "Bilevel Responsive";
	  case CONTROLLER_RESPONSIVE_FLOW:
		 return "Flow Responsive";
	  case CONTROLLER_RESPONSIVE_BOTTLENECK:
		 return "Bottleneck Responsive";
	  case CONTROLLER_WEBSTER:
		 return "Webster";
	  case CONTROLLER_SMITH:
		 return "Smith";
	  case CONTROLLER_DELAYMIN:
		 return "Delay Minimum";
	  case CONTROLLER_BILEVELDELAYMIN:
		 return "Bilevel Delaymin";
   }
   return "\0";
}

const char* NameTable::sensor(int code)
{
   string label;
   if (SENSOR_ACODE(code)) {
	  label += "A";
   }
   if (SENSOR_ICODE(code)) {
	  label += "I";
   }
   if (SENSOR_SCODE(code)) {
	  label += "S";
   }
   return label.c_str();
}

const char* NameTable::signal(int code)
{
   code &= CTRL_SIGNAL_TYPE;
   switch (code) {
	  case CTRL_TS:
		 return "Traffic Signal";
	  case CTRL_PS:
		 return "Portal Signal";
	  case CTRL_VSLS:
		 return "VSLS";
	  case CTRL_VMS:
		 {
			code &= SIGN_TYPE;
			switch (code) {
			   case SIGN_LANE_USE_RULE:
				  return "VMS / Lane Use Rule";
			   case SIGN_LANE_USE_PATH:
				  return "VMS / Traffic Stream";
			   case SIGN_PATH:
				  return "VMS / PRG";
			   case SIGN_ENROUTE:
				  return "VMS / DRG";
			   default:
				  return "VMS";
			}
		 }
	  case CTRL_LUS:
		 return "LUS";
	  case CTRL_RAMPMETER:
		 return "Ramp Meter";
	  case CTRL_TOLLBOOTH:
		 return "Toll Booth";
          case CTRL_BUSSTOP:          // margaret:  bus stops
		 return "Bus Stop";  	  
          case CTRL_INCIDENT:
		 return "Incident";
   }
   return "\0";
}
