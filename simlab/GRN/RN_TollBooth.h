//-*-c++-*------------------------------------------------------------
// RN_TollBooth.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef RN_TOLLBOOTH_HEADER
#define RN_TOLLBOOTH_HEADER

#include "RN_Signal.h"
#include <iosfwd>

class RN_TollBooth : public virtual RN_Signal
{
   protected:

      // Toll booths are treated as signals and stored after the
      // standard traffic signals

      static int startIndexAsSignals_;

      unsigned int rules_;	// lane use and change rules
      unsigned int speed_;	// maximum speed (in original unit)
      float serviceTime_;	// stop time at the booth

   public:

      RN_TollBooth()
	: RN_Signal(), serviceTime_(0), speed_(0), rules_(0) { }
      virtual ~RN_TollBooth() { }

      virtual unsigned int rules() { return rules_; }
      virtual unsigned int speed() { return speed_; }
      virtual float serviceTime() { return serviceTime_; }
      virtual int isEtc();
    
      virtual int initTollBooth(int code, int state, int lane,
		       unsigned int rule, unsigned int speed,
		       float st);
      virtual void print(std::ostream &os);
};

#endif
