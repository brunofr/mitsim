//-*-c++-*------------------------------------------------------------
// FILE: RN_SurvStation.C
// DATE: Sat Oct 21 16:51:05 1995
// Surverllace station is stored in a sorted list in each link
// according to their distance from the end of the link.  The sorting
// is in descending order (Upstream=LongerDistance=Top)
//--------------------------------------------------------------------

#include <Tools/Math.h>

#include <Tools/ToolKit.h>
#include "RN_SurvStation.h"

#include "RoadNetwork.h"
#include "RN_Link.h"
#include "RN_Segment.h"
#include "RN_Lane.h"
#include "RN_Sensor.h"
#include "Parameter.h"
#include "Constants.h"
using namespace std;


// This function overloads the virtual function defined in
// CodedObject(now sorting objects in descending order)

int RN_SurvStation::cmp(CodedObject*other) const
{
  RN_SurvStation * surv = (RN_SurvStation *)other;
  if (distance_ < surv->distance_) return 1;
  else if (distance_ > surv->distance_) return -1;
  else return 0;
}

// Returns the link contains the surveillance station

RN_Link*
RN_SurvStation::link()
{
  return segment_->link();
}


// Returns pointer to the sensor in ith lane. It may be NULL if there
// is no sensor in the ith lane.

RN_Sensor*
RN_SurvStation::sensor(int i)
{
  if (isLinkWide())
	return sensors_[0];
  else
	return sensors_[i];
}

void RN_SurvStation::sensor(int i, RN_Sensor *s)
{
  if (isLinkWide()) i = 0;
  sensors_[i] = s;
}

short int
RN_SurvStation::nLanes()
{
  return segment_->nLanes();
}


// This function is called by RN_Parser to set information of a
// surveillance station from network database.  It returns zero if no
// error, -1 if fatal error or a positive number if warning error.

int
RN_SurvStation::init(int ty, int ta, float zone, int seg,
					 float pos)
{
  static int serial_no = 0;

  if (ToolKit::debug()) {
	cout << indent << "<"
		 << ty << endc << ta << endc << zone << endc
		 << seg << endc << pos
		 << ">" << endl;
  }

  type_ = ty;					// sensor type
  task_ = ta;					// data type
  zoneLength_ = zone * Parameter::lengthFactor();
  position_ = 1.0 - pos;		// position in % from segment end
  
  if (!(segment_ = theNetwork->findSegment(seg))) {
	cerr << "Error:: Unknown segment <" << seg << ">. ";
	return -1;
  }
  distance_ = segment_->distance() + position_ * segment_->length();

  if (isLinkWide()) {
	sensors_.reserve(1);
	sensors_[0] = NULL;
  } else {
	int n = segment_->nLanes();
	sensors_.reserve(n);
	while (n > 0) {
	  n --;
	  sensors_[n] = NULL;
	}
  }

  link()->survStationList().insert(this);

  theNetwork->lastSurvStation(this);

  code_ = ++serial_no;

  return 0;
}


void
RN_SurvStation::print(ostream &os)
{
  os << indent << "{";
  os << type_ << endc
	 << task_ << endc
	 << (zoneLength_ / Parameter::lengthFactor()) << endc
	 << segment_->code() << endc 
	 << (1.0 - position()) << endl;
   
  if (isLinkWide()) {
	sensors_[0]->print(os);
  } else {
	int num = segment_->nLanes();
	for (register int i = 0; i < num; i ++) {
	  if (sensors_[i]) {
	    sensors_[i]->print(os);
	  } else {
	    os << indent << indent << "% none" << endl;
	  }
	}
  }
  os << indent << "}" << endl;
}

int 
RN_SurvStation::count()
{
  int sum = 0;

  int nlanes = nLanes();

  for(register int i = 0; i < nlanes; i++)
	sum += (sensor(i))->flow();

  return sum;
}
