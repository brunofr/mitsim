//-*-c++-*------------------------------------------------------------
// FILE: RN_BusRoute.C
// DATE: Thu Oct 19 17:28:39 1995
//--------------------------------------------------------------------


#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>

#include "RoadNetwork.h"
#include "RN_Label.h"
#include "RN_Link.h"
#include "RN_Node.h"
#include "RN_BusRoute.h"
#include "RN_BusRouteTable.h"
#include "RN_BusStop.h"
#include "RN_LinkTimes.h"
#include <iostream>
using namespace std;

int RN_BusRoute::sorted_ = 1;	// element is sorted

RN_Link*
RN_BusRoute::nextRouteLink(int i)
{
   if (i < nRouteLinks() - 1) return routelinks_[i + 1];
   else return NULL;
}

RN_BusStop*
RN_BusRoute::nextTimepoint(int i)
{
   if (i < nTimepoints() - 1) return timepoints_[i + 1];
   else return NULL;
}

// Called by RN_BusRouteParser to initialize a bus route
// design headway dh is an optional argument, default = 0

int
RN_BusRoute::init(int c, double hw) 
{
   static int idx = 0;
   static int last = -1;
   code_ = c;
   designHeadway_ = hw;
   if (sorted_ && c <= last) sorted_ = 0;
   else last = c;

   index_ = idx ++;

   return 0;
}

// Called by RN_BusRouteParser to add a link to a bus route

int
RN_BusRoute::addRouteLink(int linkcode)
{
   RN_Link *linkptr = theNetwork->findLink(linkcode);
   if (!linkptr) {
      cerr << "Error:: Unkonwn link code <" << linkcode << ">. ";
      return -1;
   }
   RN_Link *prevlink = lastRouteLink();
   if (prevlink && prevlink->dnNode() != linkptr->upNode()) {
      cerr << "Error:: Link <" << linkcode << "> is not connected to "
		   << "link <" << prevlink->code() << ">. ";
      return -1;
   }
   routelinks_.push_back(linkptr);
   return 0;
}

// Called by RN_BusRouteParser to add a timepoint (bus stop or sensor) to a bus route

int
RN_BusRoute::addTimepoint(int timepointcode)
{
   RN_BusStop *tpptr = theNetwork->findBusStop(timepointcode);
   if (!tpptr) {
      cerr << "Error:: Unkonwn bus stop/sensor code <" << timepointcode << ">. ";
      return -1;
   }
   tpptr->addRouteToBusStop(code_);
   RN_BusStop *prevtimepoint = lastTimepoint();
   timepoints_.push_back(tpptr);
   return 0;
}


void
RN_BusRoute::print(ostream &os)
{
   register int i;
   os << indent << "{";
   os << code_ << endc;
   os << nRouteLinks() << "{";
   for (i = 0; i < nRouteLinks(); i ++) {
      os << routelinks_[i]->code() << endc;
   }
   os << "}" << endc;
   os << "} # " << firstRouteLink()->upNode()->name();
   os << "->" << lastRouteLink()->dnNode()->name() << endl;
   os << nTimepoints() << "{";
   for (i = 0; i < nTimepoints(); i ++) {
      os << timepoints_[i]->code() << endc;
   }
   os << "}" << endl;
}

int
RN_BusRoute::firstNode()
{
  if(nRouteLinks()) return routelinks_[0]->upNode()->code();
}

int
RN_BusRoute::endNode()
{
  if(nRouteLinks()) return routelinks_[nRouteLinks()-1]->dnNode()->code();
}

RN_Link* 
RN_BusRoute::findRouteLink(int c)
{
   vector<RN_Link*>::iterator i;
   i = find_if(routelinks_.begin(), routelinks_.end(), CodeEqual(c));
   if (i != routelinks_.end()) return *i;
   else return NULL;
}

RN_BusStop* 
RN_BusRoute::findTimepoint(int c)
{
   vector<RN_BusStop*>::iterator i;
   i = find_if(timepoints_.begin(), timepoints_.end(), CodeEqual(c));
   if (i != timepoints_.end()) return *i;
   else return NULL;
}
