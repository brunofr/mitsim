//-*-c++-*------------------------------------------------------------
// FILE: RN_PathTable.h
// DATE: Thu Oct 19 21:23:13 1995
//--------------------------------------------------------------------

#ifndef RN_PATHTABLE_HEADER
#define RN_PATHTABLE_HEADER

#include <iostream>
#include <vector>

#include "RN_Path.h"

const int NUM_PATHS = 20;	// Default initial num of paths

class RN_PathParser;
class ArgsParser;
class RN_LinkTimes;

class RN_PathTable
{
      friend class RN_PathParser;
      friend class ArgsParser;

   protected:

      std::vector<RN_Path*> paths_;	// array of pointers to paths

   public:

      static char * name_;	// file name
      static char * name() { return name_; }
      static char ** nameptr() { return &name_; }

      RN_PathTable() { }

      virtual ~RN_PathTable();

      // This may be overloaded by derived class

      virtual RN_Path* newPath();

      virtual int addPath(int code, int ori, int des);
      inline RN_Path* lastPath() { return paths_.back(); }

      inline int nPaths() { return paths_.size(); }
      inline RN_Path* path(int i) { return paths_[i]; }
      RN_Path* findPath(int c);

      virtual void print(std::ostream &os = std::cout);

      void setPathPointers();
};

extern RN_PathTable * thePathTable;

#endif
