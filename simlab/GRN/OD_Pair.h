//-*-c++-*------------------------------------------------------------
// NAME: Traffic simulator
// NOTE: 
// AUTH: Qi Yang
// FILE: OD_Pair.h
// DATE: Mon Aug 12 13:42:15 1996
//--------------------------------------------------------------------

#ifndef OD_PAIR_HEADER
#define OD_PAIR_HEADER

#include <set>
#include <Templates/Pointer.h>
#include <Templates/STL_Configure.h>

class RN_Node;

class OD_Pair
{
   public:

      OD_Pair() : oriNode_(NULL), desNode_(NULL) { }
      OD_Pair(RN_Node *, RN_Node *);
      OD_Pair(OD_Pair &);
      OD_Pair(const OD_Pair &);
      ~OD_Pair() { }

      OD_Pair& operator = (const OD_Pair &);
      bool operator == (const OD_Pair &);
      bool operator < (const OD_Pair &);

      inline RN_Node* oriNode() const { return oriNode_; }
      inline RN_Node* desNode() const { return desNode_; }
      int ori();
      int des();

   protected:

      RN_Node *oriNode_;
      RN_Node *desNode_;
};

typedef Pointer <OD_Pair> PtrOD_Pair;

typedef std::set <PtrOD_Pair, std::less<PtrOD_Pair> ALLOCATOR > OdPairSetType;

extern OdPairSetType theOdPairs;

#endif
