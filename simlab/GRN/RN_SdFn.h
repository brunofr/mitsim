//-*-c++-*------------------------------------------------------------
// RN_SdFn.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef RN_SDFN_HEADER
#define RN_SDFN_HEADER

#include <iostream>

class RN_SdFn
{
protected:

  float capacity_;			// default capacity
  float jamSpeed_;			// minimum speed
  float jamDensity_;		// jam density (v/km)

public:

  virtual ~RN_SdFn() { }

  float jamDensity() { return jamDensity_; }
  float capacity() { return capacity_; }

  virtual void print(std::ostream &os = std::cout);

  virtual float densityToSpeed(
							   float freeSpeed, float /* density */,
							   int /* nlanes */) {
	return freeSpeed;
  }

 
  //virtual float densityToNLSpeed(
  //						   float freeSpeed, float /* density */,
  //							   int /* nlanes */) {
  //	return freeSpeed;
  // }

 
protected:

  RN_SdFn();
  RN_SdFn(float cap, float spd, float kjam);
};

class RN_SdFnLinear : public RN_SdFn
{
private:

  float kl_, vh_;
  float kh_, vl_;
  float delta_l_, delta_m_, delta_h_;

public:

  RN_SdFnLinear();
  RN_SdFnLinear(float cap, float spd, float kjam,
				float kl, float vh, float kh, float vl);
  ~RN_SdFnLinear() { }

  void print(std::ostream &os = std::cout);
  float densityToSpeed(float freeSpeed, float density, int nlanes);

private:

  void initialize();
};

class RN_SdFnNonLinear : public RN_SdFn
{
private:

  float speedBeta_;			// parameters in nonlinear sd functions
  float densityBeta_;

public:

  RN_SdFnNonLinear();
  RN_SdFnNonLinear(float cap, float spd, float kjam,
				   float alpha, float beta);
  ~RN_SdFnNonLinear() { }

  void print(std::ostream &os = std::cout);
  float densityToSpeed(float freeSpeed, float density, int nlanes);
};

#endif
