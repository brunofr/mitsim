#ifndef YY_VehicleTableParser_h_included
#define YY_VehicleTableParser_h_included

#line 1 "/usr/lib/bison.h"
/* before anything */
#ifdef c_plusplus
#ifndef __cplusplus
#define __cplusplus
#endif
#endif
#ifdef __cplusplus
#ifndef YY_USE_CLASS
#define YY_USE_CLASS
#endif
#else
#endif
#include <stdio.h>

/* #line 14 "/usr/lib/bison.h" */
#line 21 "VehicleTableParser.h"
#define YY_VehicleTableParser_ERROR_BODY 	DEF_ERROR_BODY
#define YY_VehicleTableParser_LEX_BODY 	DEF_LEX_BODY
#define YY_VehicleTableParser_CONSTRUCTOR_PARAM   VehicleTable* pContainer_
#define YY_VehicleTableParser_CONSTRUCTOR_INIT    : _pContainer(pContainer_)   \
                           ,_current(NULL)              \
                           ,_vid(0)                     \
                           ,_type(0)                    \

#define YY_VehicleTableParser_MEMBERS   DEF_MEMBERS                            \
   VehicleTable* _pContainer ;                          \
   RN_Vehicle* _current ;                               \
   int _vid ;                                           \
   int _type ;                                          \


#line 22 "VehicleTableParser.y"
typedef union {
  long        type_i ;
  double      type_f ;
  const char* type_s ;
} yy_VehicleTableParser_stype;
#define YY_VehicleTableParser_STYPE yy_VehicleTableParser_stype
#line 47 "VehicleTableParser.y"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <IO/Exception.h>
#include <Tools/ToolKit.h>  
#include <Tools/Scanner.h>  
#include <Tools/SimulationClock.h>
#include <Tools/Math.h>

#include <GRN/VehicleTable.h>
#include <GRN/RN_Vehicle.h>
#include <GRN/RN_Path.h>
#include <GRN/RN_Node.h>

#line 14 "/usr/lib/bison.h"
 /* %{ and %header{ and %union, during decl */
#ifndef YY_VehicleTableParser_COMPATIBILITY
#ifndef YY_USE_CLASS
#define  YY_VehicleTableParser_COMPATIBILITY 1
#else
#define  YY_VehicleTableParser_COMPATIBILITY 0
#endif
#endif

#if YY_VehicleTableParser_COMPATIBILITY != 0
/* backward compatibility */
#ifdef YYLTYPE
#ifndef YY_VehicleTableParser_LTYPE
#define YY_VehicleTableParser_LTYPE YYLTYPE
/* WARNING obsolete !!! user defined YYLTYPE not reported into generated header */
/* use %define LTYPE */
#endif
#endif
#ifdef YYSTYPE
#ifndef YY_VehicleTableParser_STYPE 
#define YY_VehicleTableParser_STYPE YYSTYPE
/* WARNING obsolete !!! user defined YYSTYPE not reported into generated header */
/* use %define STYPE */
#endif
#endif
#ifdef YYDEBUG
#ifndef YY_VehicleTableParser_DEBUG
#define  YY_VehicleTableParser_DEBUG YYDEBUG
/* WARNING obsolete !!! user defined YYDEBUG not reported into generated header */
/* use %define DEBUG */
#endif
#endif
#ifdef YY_VehicleTableParser_STYPE
#ifndef yystype
#define yystype YY_VehicleTableParser_STYPE
#endif
#endif
/* use goto to be compatible */
#ifndef YY_VehicleTableParser_USE_GOTO
#define YY_VehicleTableParser_USE_GOTO 1
#endif
#endif

/* use no goto to be clean in C++ */
#ifndef YY_VehicleTableParser_USE_GOTO
#define YY_VehicleTableParser_USE_GOTO 0
#endif

#ifndef YY_VehicleTableParser_PURE

/* #line 63 "/usr/lib/bison.h" */
#line 113 "VehicleTableParser.h"

#line 63 "/usr/lib/bison.h"
/* YY_VehicleTableParser_PURE */
#endif

/* #line 65 "/usr/lib/bison.h" */
#line 120 "VehicleTableParser.h"

#line 65 "/usr/lib/bison.h"
/* prefix */
#ifndef YY_VehicleTableParser_DEBUG

/* #line 67 "/usr/lib/bison.h" */
#line 127 "VehicleTableParser.h"

#line 67 "/usr/lib/bison.h"
/* YY_VehicleTableParser_DEBUG */
#endif
#ifndef YY_VehicleTableParser_LSP_NEEDED

/* #line 70 "/usr/lib/bison.h" */
#line 135 "VehicleTableParser.h"

#line 70 "/usr/lib/bison.h"
 /* YY_VehicleTableParser_LSP_NEEDED*/
#endif
/* DEFAULT LTYPE*/
#ifdef YY_VehicleTableParser_LSP_NEEDED
#ifndef YY_VehicleTableParser_LTYPE
typedef
  struct yyltype
    {
      int timestamp;
      int first_line;
      int first_column;
      int last_line;
      int last_column;
      char *text;
   }
  yyltype;

#define YY_VehicleTableParser_LTYPE yyltype
#endif
#endif
/* DEFAULT STYPE*/
#ifndef YY_VehicleTableParser_STYPE
#define YY_VehicleTableParser_STYPE int
#endif
/* DEFAULT MISCELANEOUS */
#ifndef YY_VehicleTableParser_PARSE
#define YY_VehicleTableParser_PARSE yyparse
#endif
#ifndef YY_VehicleTableParser_LEX
#define YY_VehicleTableParser_LEX yylex
#endif
#ifndef YY_VehicleTableParser_LVAL
#define YY_VehicleTableParser_LVAL yylval
#endif
#ifndef YY_VehicleTableParser_LLOC
#define YY_VehicleTableParser_LLOC yylloc
#endif
#ifndef YY_VehicleTableParser_CHAR
#define YY_VehicleTableParser_CHAR yychar
#endif
#ifndef YY_VehicleTableParser_NERRS
#define YY_VehicleTableParser_NERRS yynerrs
#endif
#ifndef YY_VehicleTableParser_DEBUG_FLAG
#define YY_VehicleTableParser_DEBUG_FLAG yydebug
#endif
#ifndef YY_VehicleTableParser_ERROR
#define YY_VehicleTableParser_ERROR yyerror
#endif

#ifndef YY_VehicleTableParser_PARSE_PARAM
#ifndef __STDC__
#ifndef __cplusplus
#ifndef YY_USE_CLASS
#define YY_VehicleTableParser_PARSE_PARAM
#ifndef YY_VehicleTableParser_PARSE_PARAM_DEF
#define YY_VehicleTableParser_PARSE_PARAM_DEF
#endif
#endif
#endif
#endif
#ifndef YY_VehicleTableParser_PARSE_PARAM
#define YY_VehicleTableParser_PARSE_PARAM void
#endif
#endif

/* TOKEN C */
#ifndef YY_USE_CLASS

#ifndef YY_VehicleTableParser_PURE
extern YY_VehicleTableParser_STYPE YY_VehicleTableParser_LVAL;
#endif


/* #line 143 "/usr/lib/bison.h" */
#line 213 "VehicleTableParser.h"
#define	LEX_EQUAL	258
#define	LEX_COMMA	259
#define	LEX_COLON	260
#define	LEX_SEMICOLON	261
#define	LEX_OAB	262
#define	LEX_CAB	263
#define	LEX_OBB	264
#define	LEX_CBB	265
#define	LEX_OCB	266
#define	LEX_CCB	267
#define	LEX_INT	268
#define	LEX_REAL	269
#define	LEX_TIME	270
#define	LEX_PAIR	271
#define	LEX_TAG	272
#define	LEX_STRING	273
#define	LEX_DUP	274
#define	LEX_END	275


#line 143 "/usr/lib/bison.h"
 /* #defines token */
/* after #define tokens, before const tokens S5*/
#else
#ifndef YY_VehicleTableParser_CLASS
#define YY_VehicleTableParser_CLASS VehicleTableParser
#endif

#ifndef YY_VehicleTableParser_INHERIT
#define YY_VehicleTableParser_INHERIT
#endif
#ifndef YY_VehicleTableParser_MEMBERS
#define YY_VehicleTableParser_MEMBERS 
#endif
#ifndef YY_VehicleTableParser_LEX_BODY
#define YY_VehicleTableParser_LEX_BODY  
#endif
#ifndef YY_VehicleTableParser_ERROR_BODY
#define YY_VehicleTableParser_ERROR_BODY  
#endif
#ifndef YY_VehicleTableParser_CONSTRUCTOR_PARAM
#define YY_VehicleTableParser_CONSTRUCTOR_PARAM
#endif
/* choose between enum and const */
#ifndef YY_VehicleTableParser_USE_CONST_TOKEN
#define YY_VehicleTableParser_USE_CONST_TOKEN 0
/* yes enum is more compatible with flex,  */
/* so by default we use it */ 
#endif
#if YY_VehicleTableParser_USE_CONST_TOKEN != 0
#ifndef YY_VehicleTableParser_ENUM_TOKEN
#define YY_VehicleTableParser_ENUM_TOKEN yy_VehicleTableParser_enum_token
#endif
#endif

class YY_VehicleTableParser_CLASS YY_VehicleTableParser_INHERIT
{
public: 
#if YY_VehicleTableParser_USE_CONST_TOKEN != 0
/* static const int token ... */

/* #line 182 "/usr/lib/bison.h" */
#line 276 "VehicleTableParser.h"
static const int LEX_EQUAL;
static const int LEX_COMMA;
static const int LEX_COLON;
static const int LEX_SEMICOLON;
static const int LEX_OAB;
static const int LEX_CAB;
static const int LEX_OBB;
static const int LEX_CBB;
static const int LEX_OCB;
static const int LEX_CCB;
static const int LEX_INT;
static const int LEX_REAL;
static const int LEX_TIME;
static const int LEX_PAIR;
static const int LEX_TAG;
static const int LEX_STRING;
static const int LEX_DUP;
static const int LEX_END;


#line 182 "/usr/lib/bison.h"
 /* decl const */
#else
enum YY_VehicleTableParser_ENUM_TOKEN { YY_VehicleTableParser_NULL_TOKEN=0

/* #line 185 "/usr/lib/bison.h" */
#line 303 "VehicleTableParser.h"
	,LEX_EQUAL=258
	,LEX_COMMA=259
	,LEX_COLON=260
	,LEX_SEMICOLON=261
	,LEX_OAB=262
	,LEX_CAB=263
	,LEX_OBB=264
	,LEX_CBB=265
	,LEX_OCB=266
	,LEX_CCB=267
	,LEX_INT=268
	,LEX_REAL=269
	,LEX_TIME=270
	,LEX_PAIR=271
	,LEX_TAG=272
	,LEX_STRING=273
	,LEX_DUP=274
	,LEX_END=275


#line 185 "/usr/lib/bison.h"
 /* enum token */
     }; /* end of enum declaration */
#endif
public:
 int YY_VehicleTableParser_PARSE(YY_VehicleTableParser_PARSE_PARAM);
 virtual void YY_VehicleTableParser_ERROR(char *msg) YY_VehicleTableParser_ERROR_BODY;
#ifdef YY_VehicleTableParser_PURE
#ifdef YY_VehicleTableParser_LSP_NEEDED
 virtual int  YY_VehicleTableParser_LEX(YY_VehicleTableParser_STYPE *YY_VehicleTableParser_LVAL,YY_VehicleTableParser_LTYPE *YY_VehicleTableParser_LLOC) YY_VehicleTableParser_LEX_BODY;
#else
 virtual int  YY_VehicleTableParser_LEX(YY_VehicleTableParser_STYPE *YY_VehicleTableParser_LVAL) YY_VehicleTableParser_LEX_BODY;
#endif
#else
 virtual int YY_VehicleTableParser_LEX() YY_VehicleTableParser_LEX_BODY;
 YY_VehicleTableParser_STYPE YY_VehicleTableParser_LVAL;
#ifdef YY_VehicleTableParser_LSP_NEEDED
 YY_VehicleTableParser_LTYPE YY_VehicleTableParser_LLOC;
#endif
 int YY_VehicleTableParser_NERRS;
 int YY_VehicleTableParser_CHAR;
#endif
#if YY_VehicleTableParser_DEBUG != 0
public:
 int YY_VehicleTableParser_DEBUG_FLAG;	/*  nonzero means print parse trace	*/
#endif
public:
 YY_VehicleTableParser_CLASS(YY_VehicleTableParser_CONSTRUCTOR_PARAM);
public:
 YY_VehicleTableParser_MEMBERS 
};
/* other declare folow */
#endif


#if YY_VehicleTableParser_COMPATIBILITY != 0
/* backward compatibility */
#ifndef YYSTYPE
#define YYSTYPE YY_VehicleTableParser_STYPE
#endif

#ifndef YYLTYPE
#define YYLTYPE YY_VehicleTableParser_LTYPE
#endif
#ifndef YYDEBUG
#ifdef YY_VehicleTableParser_DEBUG 
#define YYDEBUG YY_VehicleTableParser_DEBUG
#endif
#endif

#endif
/* END */

/* #line 236 "/usr/lib/bison.h" */
#line 378 "VehicleTableParser.h"

#line 291 "VehicleTableParser.y"

  // empty
#endif
