//-*-c++-*------------------------------------------------------------
// FILE: RN_LinkGraph.h
// AUTH: Qi Yang
// DATE: Wed Oct 18 14:29:29 1995
//--------------------------------------------------------------------

#ifndef RN_LINK_GRAPH_HEADER
#define RN_LINK_GRAPH_HEADER

//#include <iostream>
#include <Templates/Graph.h>

#include "RN_LinkTimes.h"

class RoadNetwork;

class RN_LinkGraph
{
   public:
	
      RN_LinkGraph(RoadNetwork *, RN_LinkTimes *);
      virtual ~RN_LinkGraph() { delete graph_; }

      void updateLinkCosts(RN_LinkTimes *);
      void calcShortestPathTree(int r, double t = 0,
				RN_LinkTimes *info = NULL) {
	 graph_->labelCorrecting(r, t, info);
      }
      Graph<float, RN_LinkTimes> *graph() { return graph_; }

   private:

      RoadNetwork* network_;
      Graph<float, RN_LinkTimes> *graph_;
};

#endif
