//-*-c++-*------------------------------------------------------------
// FILE: RN_TollBooth.C
// DATE: Sat Oct 21 16:28:20 1995
//--------------------------------------------------------------------


#include <Tools/ToolKit.h>
#include "RN_TollBooth.h"
#include "RN_Lane.h"
#include "RN_CtrlStation.h"
#include "RN_SurvStation.h"
#include "RoadNetwork.h"
using namespace std;

int RN_TollBooth::startIndexAsSignals_ = 0;

int
RN_TollBooth::isEtc()		// virtual
{
   return (rules_ & VEHICLE_ETC);
}


int
RN_TollBooth::initTollBooth(int c, int s, int l,
		   unsigned int r, unsigned int spd,
		   float st)
{
   if (!startIndexAsSignals_) {
      theNetwork->nStdSignals_ = theNetwork->nSignals();
      startIndexAsSignals_ = theNetwork->nSignals();
   }

   if (ToolKit::debug()) {
      cout << indent << indent << "<"
	   << c << endc
	   << hextag << hex << s << dec << endc
	   << l << endc << st << endc << spd << endc << serviceTime_
	   << ">" << endl;
   }
   rules_ = r;
   speed_ = spd;
   serviceTime_ = st;

   int error = (RN_Signal::init(c, s, l));
   theNetwork->nTollBooths_ ++;
   return error;
}


void
RN_TollBooth::print(ostream &os)
{
   os << indent << indent << "{";
   os << code_ << endc << hextag << hex << state_ << dec << endc
      << lane_->code() << endc;
   os << rules_ << endc << speed_ << endc << serviceTime_;
   os << "}" << endl;
}
