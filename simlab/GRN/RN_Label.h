//-*-c++-*------------------------------------------------------------
// FILE: RN_Label.h
// DATE: Wed Oct 18 12:08:04 1995
//--------------------------------------------------------------------

#ifndef RN_LABEL_HEADER
#define RN_LABEL_HEADER

#include <iostream>
#include <Templates/Object.h>

class RN_Label : public NamedObject
{
      friend class RN_BisonParser;
      friend class RoadNetwork;

   protected:

      static int sorted_;	// =1 if sorted by code

      short int length_;	// length of the string

   public:

      RN_Label() : NamedObject(), length_(0) { }
      virtual ~RN_Label() { }

      inline short int length() { return length_; }
      static inline int sorted() { return sorted_; }
      virtual int init(int c, const char *n);
      virtual void print(std::ostream &os = std::cout);
};

#endif
