/*-----------------------------------------------------------------------------
 * ListTree	A list widget that displays a file manager style tree
 *
 * Copyright (c) 1996 Robert W. McMullen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *
 * Author: Rob McMullen <rwmcm@mail.ae.utexas.edu>
 *         http://www.ae.utexas.edu/~rwmcm
 */

#define _ListTree_

#include <X11/IntrinsicP.h>
#include <X11/StringDefs.h>

#include <stdio.h>
#include <stdlib.h>

#include "ListTreeP.h"
/* #define DEBUG */

#ifndef __GNUC__
#ifndef __FUNCTION__
#define __FUNCTION__ "-unknown-"
#endif
#endif

static void focus_in(Widget aw, XEvent *event, String *params, Cardinal *num_params);
static void focus_out(Widget aw, XEvent *event, String *params, Cardinal *num_params);
static void notify(Widget aw, XEvent *event, String *params, Cardinal *num_params);
static void select_start(Widget aw, XEvent *event, String *params, Cardinal *num_params);
static void extend_select_start(Widget aw, XEvent *event, String *params, Cardinal *num_params);
static void extend_select(Widget aw, XEvent *event, String *params, Cardinal *num_params);
static void unset(Widget aw, XEvent *event, String *params, Cardinal *num_params);
static void menu(Widget aw, XEvent *event, String *params, Cardinal *num_params);
static void keypress(Widget aw, XEvent *event, String *params, Cardinal *num_params);

#ifdef DEBUG
#include <stdlib.h>
#include <stdarg.h>
void DBG(int line, char *fcn, char *fmt, ...)
{
   va_list ap;
  
   fprintf(stderr, "%s:%d %s()  ",__FILE__,line,fcn);
   va_start(ap, fmt);
   vfprintf(stderr, fmt, ap);
   va_end(ap);
}
#define DARG __LINE__,__FUNCTION__
#define DBGW(a) fprintf(stderr,"%s:%d %s()   %s\n",__FILE__,__LINE__,__FUNCTION__, a)
#else
void DBG(int, char *, char *, ...)
{
}
#define DARG __LINE__,__FUNCTION__
#define DBGW(a)
#endif

#define folder_width 16
#define folder_height 12
static char folder_bits[] =
{
   0x00, 0x1f, 0x80, 0x20, 0x7c, 0x5f, 0x02, 0x40, 0x02, 0x40, 0x02, 0x40,
   0x02, 0x40, 0x02, 0x40, 0x02, 0x40, 0x02, 0x40, 0x02, 0x40, 0xfc, 0x3f,
};

#define folderopen_width 16
#define folderopen_height 12
static char folderopen_bits[] =
{
   0x00, 0x3e, 0x00, 0x41, 0xf8, 0xd5, 0xac, 0xaa, 0x54, 0xd5, 0xfe, 0xaf,
   0x01, 0xd0, 0x02, 0xa0, 0x02, 0xe0, 0x04, 0xc0, 0x04, 0xc0, 0xf8, 0x7f,
};

#define document_width 9
#define document_height 14
static char document_bits[] =
{
   0x1f, 0x00, 0x31, 0x00, 0x51, 0x00, 0x91, 0x00, 0xf1, 0x01, 0x01, 0x01,
   0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
   0x01, 0x01, 0xff, 0x01,};

#define offset(field) XtOffsetOf(ListTreeRec, list.field)
static XtResource resources[] =
{
   {XtNforeground, XtCForeground, XtRPixel, sizeof(Pixel),
    offset(foreground_pixel), XtRString, (XtPointer) XtDefaultForeground},
   {XtNmargin, XtCMargin, XtRDimension, sizeof(Dimension),
    offset(Margin), XtRImmediate, (XtPointer) 2},
   {XtNindent, XtCMargin, XtRDimension, sizeof(Dimension),
    offset(Indent), XtRImmediate, (XtPointer) 0},
   {XtNhorizontalSpacing, XtCMargin, XtRDimension, sizeof(Dimension),
    offset(HSpacing), XtRImmediate, (XtPointer) 2},
   {XtNverticalSpacing, XtCMargin, XtRDimension, sizeof(Dimension),
    offset(VSpacing), XtRImmediate, (XtPointer) 0},
   {XtNlineWidth, XtCMargin, XtRDimension, sizeof(Dimension),
    offset(LineWidth), XtRImmediate, (XtPointer) 0},
   {XtNfont, XtCFont, XtRFontStruct, sizeof(XFontStruct *),
    offset(font), XtRString, (XtPointer) XtDefaultFont},
   {XtNhighlightPath, XtCBoolean, XtRBoolean, sizeof(Boolean),
	offset(HighlightPath), XtRImmediate, (XtPointer) False},
   {XtNclickPixmapToOpen, XtCBoolean, XtRBoolean, sizeof(Boolean),
	offset(ClickPixmapToOpen), XtRImmediate, (XtPointer) True},
   {XtNdoIncrementalHighlightCallback, XtCBoolean, XtRBoolean, sizeof(Boolean),
	offset(DoIncrementalHighlightCallback), XtRImmediate, (XtPointer) False},
   {XtNbranchPixmap, XtCPixmap, XtRBitmap, sizeof(Pixmap),
    offset(Closed.bitmap), XtRImmediate, (XtPointer) XtUnspecifiedPixmap},
   {XtNbranchOpenPixmap, XtCPixmap, XtRBitmap, sizeof(Pixmap),
    offset(Open.bitmap), XtRImmediate, (XtPointer) XtUnspecifiedPixmap},
   {XtNleafPixmap, XtCPixmap, XtRBitmap, sizeof(Pixmap),
    offset(Leaf.bitmap), XtRImmediate, (XtPointer) XtUnspecifiedPixmap},
   {XtNleafOpenPixmap, XtCPixmap, XtRBitmap, sizeof(Pixmap),
    offset(LeafOpen.bitmap), XtRImmediate, (XtPointer) XtUnspecifiedPixmap},
   {XtNhighlightCallback, XtCCallback, XtRCallback, sizeof(XtPointer),
    offset(HighlightCallback), XtRCallback, NULL},
   {XtNactivateCallback, XtCCallback, XtRCallback, sizeof(XtPointer),
    offset(ActivateCallback), XtRCallback, NULL},
   {XtNmenuCallback, XtCCallback, XtRCallback, sizeof(XtPointer),
	offset(MenuCallback), XtRCallback, NULL},
   {XtNdestroyItemCallback, XtCCallback, XtRCallback, sizeof(XtPointer),
	offset(DestroyItemCallback), XtRCallback, NULL},
};

#undef offset

static void Initialize(Widget request, Widget tnew, ArgList args, Cardinal * num);
static void Destroy(Widget w);
static void Redisplay(Widget aw, XEvent * event, Region region);
static void Resize(Widget w);
static Boolean SetValues(Widget current, Widget request, Widget reply, ArgList args, Cardinal * nargs);
static void Realize(Widget aw, XtValueMask * value_mask, XSetWindowAttributes * attributes);
static XtGeometryResult QueryGeometry(Widget w, XtWidgetGeometry *proposed, XtWidgetGeometry *answer);

static void Draw(Widget w, int yevent,int hevent);
static void DrawAll(Widget w);
static void DrawChanged(Widget w);
static void DrawItemHighlight(Widget w, ListTreeItem *item);
static void DrawItemHighlightClear(Widget w, ListTreeItem *item);
static ListTreeItem *GetItem(Widget w, int findy);

static void DeleteChildren(Widget w, ListTreeItem *item);
static void CountAll(Widget w);
static void GotoPosition(Widget w);
static void VSBCallback (Widget w, XtPointer client_data, XtPointer call_data);
static void HSBCallback (Widget w, XtPointer client_data, XtPointer call_data);
void ListTreeGetHighlighted(Widget w,ListTreeMultiReturnStruct *ret);
void ListTreeSetHighlighted(Widget w,ListTreeItem **items,
							int count,Boolean clear);

/* Actions */
static void focus_in();
static void focus_out();
static void notify();
static void unset();
static void menu();
static void select_start();
static void extend_select_start();
static void extend_select();
static void keypress();

#ifdef max  /* just in case--we don't know, but these are commonly set */
#undef max  /* by arbitrary unix systems.  Also, we cast to int! */
#endif
/* redefine "max" and "min" macros to take into account "unsigned" values */
#define max(a,b) ((int)(a)>(int)(b)?(int)(a):(int)(b))
#define min(a,b) ((int)(a)<(int)(b)?(int)(a):(int)(b))

/* Font convenience macros */

#define FontHeight(f)  (int)(f->max_bounds.ascent + f->max_bounds.descent)
#define FontDescent(f) (int)(f->max_bounds.descent)
#define FontAscent(f)  (int)(f->max_bounds.ascent)
#define FontTextWidth(f,c) (int)XTextWidth(f, c, strlen(c))

static char defaultTranslations[] = "\
<FocusIn>:		focus-in()\n\
<FocusOut>:		focus-out()\n\
<Btn3Down>:             menu()\n\
<Key>Return:            keypress()\n\
<Key>osfUp:             keypress()\n\
Shift <Btn1Down>:	extend-select-start()\n\
<Btn1Down>:		select-start()\n\
<Btn1Up>:		notify()\n\
Button1 <Btn1Motion>:	extend-select()\n\
";

static XtActionsRec actions[] =
{
   {"focus-in", focus_in},
   {"focus-out", focus_out},
   {"notify", notify},
   {"select-start", select_start},
   {"extend-select", extend_select},
   {"extend-select-start", extend_select_start},
   {"unset", unset},
   {"menu", menu},
   {"keypress", keypress},
};

static XmBaseClassExtRec listtreeCoreClassExtRec =
{
   /* next_extension            */ NULL,
								   /* record_type               */ NULLQUARK,
								   /* version                   */ XmBaseClassExtVersion,
								   /* size                      */ sizeof(XmBaseClassExtRec),
								   /* initialize_prehook        */ NULL, /* FIXME */
								   /* set_values_prehook        */ NULL, /* FIXME */
								   /* initialize_posthook       */ NULL, /* FIXME */
								   /* set_values_posthook       */ NULL, /* FIXME */
								   /* secondary_object_class    */ NULL, /* FIXME */
								   /* secondary_object_create   */ NULL, /* FIXME */
								   /* get_secondary_resources   */ NULL, /* FIXME */
								   /* fast_subclass             */ {0},  /* FIXME */
								   /* get_values_prehook        */ NULL, /* FIXME */
								   /* get_values_posthook       */ NULL, /* FIXME */
								   /* class_part_init_prehook   */ NULL,
								   /* class_part_init_posthook  */ NULL,
								   /* ext_resources             */ NULL,
								   /* compiled_ext_resources    */ NULL,
								   /* num_ext_resources         */ 0,
								   /* use_sub_resources         */ FALSE,
								   /* widget_navigable          */ XmInheritWidgetNavigable,
								   /* focus_change              */ XmInheritFocusChange,
								   /* wrapper_data              */ NULL
};

XmPrimitiveClassExtRec listtreePrimClassExtRec =
{
   /* next_extension      */ NULL,
							 /* record_type         */ NULLQUARK,
							 /* version             */ XmPrimitiveClassExtVersion,
							 /* record_size         */ sizeof(XmPrimitiveClassExtRec),
							 /* widget_baseline     */ NULL,
							 /* widget_display_rect */ NULL,
							 /* widget_margins      */ NULL
};

ListTreeClassRec listtreeClassRec =
{
   {
	  /* core_class fields     */
	  /* MOTIF superclass      */ (WidgetClass) & xmPrimitiveClassRec,
								  /* class_name            */ "ListTree",
								  /* widget_size           */ sizeof(ListTreeRec),
								  /* class_initialize      */ NULL,
								  /* class_part_initialize */ NULL,
								  /* class_inited          */ False,
								  /* initialize            */ Initialize,
								  /* initialize_hook       */ NULL,
								  /* realize               */ Realize,
								  /* actions               */ actions,
								  /* num_actions           */ XtNumber(actions),
								  /* resources             */ resources,
								  /* num_resources         */ XtNumber(resources),
								  /* xrm_class             */ NULLQUARK,
								  /* compress_motion       */ True,
								  /* compress_exposure     */ XtExposeCompressMultiple,
								  /* compress_enterleave   */ True,
								  /* visible_interest      */ True,
								  /* destroy               */ Destroy,
								  /* resize                */ Resize,
								  /* expose                */ Redisplay,
								  /* set_values            */ SetValues,
								  /* set_values_hook       */ NULL,
								  /* set_values_almost     */ XtInheritSetValuesAlmost,
								  /* get_values_hook       */ NULL,
								  /* accept_focus          */ NULL,
								  /* version               */ XtVersion,
								  /* callback_private      */ NULL,
								  /* tm_table              */ defaultTranslations,
								  /* query_geometry        */ QueryGeometry,
								  /* display_accelerator   */ XtInheritDisplayAccelerator,
								  /* extension             */ (XtPointer) & listtreeCoreClassExtRec
   },
   /* Primitive Class part */
   {
	  /* border_highlight      */ XmInheritBorderHighlight,
								  /* border_unhighlight    */ XmInheritBorderUnhighlight,
								  /* translations          */ NULL,
								  /* arm_and_activate_proc */ XmInheritArmAndActivate,
								  /* synthetic resources   */ NULL,
								  /* num syn res           */ 0,
								  /* extension             */ (XtPointer) & listtreePrimClassExtRec,
   },
   {
	  /* some stupid compilers barf on empty structures */ 0
   }
};

WidgetClass listtreeWidgetClass = (WidgetClass) & listtreeClassRec;

static void
MakePixmap(Widget w, Pixinfo * pix)
{
   Window root;
   int x, y;
   unsigned int width, height, bw, depth;
   ListTreeWidget tree = (ListTreeWidget) w;

   if (pix->bitmap && XGetGeometry(XtDisplay((Widget) w), pix->bitmap,
								   &root, &x, &y, &width, &height, &bw, &depth)) {
	  pix->width = (int) width;
	  pix->height = (int) height;
	  if (pix->height>tree->list.maxPixHeight)
		 tree->list.maxPixHeight=pix->height;

      /* Xmu dependency removed by Alan Marcinkowski */
	  if (depth == 1) {
		 GC gc;
		 XGCValues gcv;

		 gcv.background = tree->core.background_pixel;
		 gcv.foreground = tree->list.foreground_pixel;
		 gc = XCreateGC(XtDisplay((Widget) w),
						RootWindowOfScreen(XtScreen((Widget) w)),
						GCForeground | GCBackground, &gcv);
		 pix->pix = XCreatePixmap (XtDisplay((Widget) w),
								   RootWindowOfScreen(XtScreen((Widget) w)),
								   width, height, tree->core.depth);
		 XCopyPlane (XtDisplay((Widget) w), pix->bitmap, pix->pix, gc, 0, 0,
					 width, height, 0, 0, 1);
		 XFreeGC (XtDisplay((Widget) w), gc);
	  }
	  else
		 pix->pix = pix->bitmap;
   }
   else {
	  pix->width = pix->height = 0;
	  pix->pix = (Pixmap) NULL;
   }
}

static void
FreePixmap(Widget w, Pixinfo * pix)
{
   if (pix->pix)
	  XFreePixmap(XtDisplay(w), pix->pix);
}


static void
InitializePixmaps(Widget w)
{
   ListTreeWidget tree = (ListTreeWidget) w;
   tree->list.maxPixHeight=0;
  
   if (tree->list.Closed.bitmap == XtUnspecifiedPixmap)
	  tree->list.Closed.bitmap = XCreateBitmapFromData(XtDisplay((Widget) w),
													RootWindowOfScreen(XtScreen((Widget) w)),
													folder_bits, folder_width, folder_height);
   MakePixmap(w, &tree->list.Closed);

   if (tree->list.Open.bitmap == XtUnspecifiedPixmap)
	  tree->list.Open.bitmap = XCreateBitmapFromData(XtDisplay((Widget) w),
												  RootWindowOfScreen(XtScreen((Widget) w)),
												  folderopen_bits, folderopen_width, folderopen_height);
   MakePixmap(w, &tree->list.Open);

   if (tree->list.Leaf.bitmap == XtUnspecifiedPixmap)
	  tree->list.Leaf.bitmap = XCreateBitmapFromData(XtDisplay((Widget) w),
												  RootWindowOfScreen(XtScreen((Widget) w)),
												  document_bits, document_width, document_height);
   MakePixmap(w, &tree->list.Leaf);

   if (tree->list.LeafOpen.bitmap == XtUnspecifiedPixmap)
	  tree->list.LeafOpen.bitmap = XCreateBitmapFromData(XtDisplay((Widget) w),
													  RootWindowOfScreen(XtScreen((Widget) w)),
													  document_bits, document_width, document_height);
   MakePixmap(w, &tree->list.LeafOpen);

   tree->list.pixWidth = tree->list.Closed.width;
   if (tree->list.Open.width > tree->list.pixWidth)
	  tree->list.pixWidth = tree->list.Open.width;
   if (tree->list.Leaf.width > tree->list.pixWidth)
	  tree->list.pixWidth = tree->list.Leaf.width;
   if (tree->list.LeafOpen.width > tree->list.pixWidth)
	  tree->list.pixWidth = tree->list.LeafOpen.width;
   tree->list.Closed.xoff = (tree->list.pixWidth - tree->list.Closed.width) / 2;
   tree->list.Open.xoff = (tree->list.pixWidth - tree->list.Open.width) / 2;
   tree->list.Leaf.xoff = (tree->list.pixWidth - tree->list.Leaf.width) / 2;
   tree->list.LeafOpen.xoff = (tree->list.pixWidth - tree->list.LeafOpen.width) / 2;
}


static void
InitializeGC(Widget w)
{
   XGCValues values;
   XtGCMask mask;
   ListTreeWidget tree = (ListTreeWidget) w;

   values.line_style = LineSolid;
   values.line_width = tree->list.LineWidth;
   values.fill_style = FillSolid;
   values.font = tree->list.font->fid;
   values.background = tree->core.background_pixel;
   values.foreground = tree->list.foreground_pixel;

   mask = GCLineStyle | GCLineWidth | GCFillStyle | GCForeground | GCBackground | GCFont;
   tree->list.drawGC = XtGetGC((Widget) w, mask, &values);

   values.function = GXinvert;
   mask = GCLineStyle | GCLineWidth | GCFillStyle | GCForeground | GCBackground | GCFont | GCFunction;
   tree->list.eorGC = XtGetGC((Widget) w, mask, &values);

   values.background = tree->list.foreground_pixel;
   values.foreground = tree->core.background_pixel;
   mask = GCLineStyle | GCLineWidth | GCFillStyle | GCForeground | GCBackground | GCFont;
   tree->list.highlightGC = XtGetGC((Widget) w, mask, &values);
}

static void
InitializeScrollBars(Widget w)
{
   ListTreeWidget tree = (ListTreeWidget) w;
   if (XmIsScrolledWindow(XtParent(w)))
	  tree->list.mom = XtParent(w);
   else
	  tree->list.mom = NULL;
  
   if(tree->list.mom) {
	  char *name = XtMalloc(strlen(XtName(w))+4);
    
	  strcpy(name,XtName(w));
	  strcat(name,"HSB");
	  tree->list.hsb = XtVaCreateManagedWidget(name, 
											xmScrollBarWidgetClass,tree->list.mom,
											XmNorientation, XmHORIZONTAL, 
											NULL);
	  XtAddCallback(tree->list.hsb, XmNdecrementCallback,HSBCallback,(XtPointer)w);
	  XtAddCallback(tree->list.hsb, XmNdragCallback,HSBCallback,(XtPointer)w);
	  XtAddCallback(tree->list.hsb, XmNincrementCallback,HSBCallback,(XtPointer)w);
	  XtAddCallback(tree->list.hsb, XmNpageDecrementCallback,HSBCallback,(XtPointer)w);
	  XtAddCallback(tree->list.hsb, XmNpageIncrementCallback,HSBCallback,(XtPointer)w);
	  XtAddCallback(tree->list.hsb, XmNtoBottomCallback,HSBCallback,(XtPointer)w);
	  XtAddCallback(tree->list.hsb, XmNtoTopCallback,HSBCallback,(XtPointer)w);
	  XtAddCallback(tree->list.hsb, XmNvalueChangedCallback,HSBCallback,(XtPointer)w);
    
	  strcpy(name,XtName(w));
	  strcat(name,"VSB");
	  tree->list.vsb = XtVaCreateManagedWidget(name,
											xmScrollBarWidgetClass,XtParent(w),
											NULL);
	  XtAddCallback(tree->list.vsb, XmNdecrementCallback,VSBCallback,(XtPointer)w);
	  XtAddCallback(tree->list.vsb, XmNdragCallback,VSBCallback,(XtPointer)w);
	  XtAddCallback(tree->list.vsb, XmNincrementCallback,VSBCallback,(XtPointer)w);
	  XtAddCallback(tree->list.vsb, XmNpageDecrementCallback,VSBCallback,(XtPointer)w);
	  XtAddCallback(tree->list.vsb, XmNpageIncrementCallback,VSBCallback,(XtPointer)w);
	  XtAddCallback(tree->list.vsb, XmNtoBottomCallback,VSBCallback,(XtPointer)w);
	  XtAddCallback(tree->list.vsb, XmNtoTopCallback,VSBCallback,(XtPointer)w);
	  XtAddCallback(tree->list.vsb, XmNvalueChangedCallback,VSBCallback,(XtPointer)w);
    
	  XtVaSetValues(tree->list.mom,
					XmNscrollBarDisplayPolicy, XmSTATIC,
					XmNscrollingPolicy, XmAPPLICATION_DEFINED,
					XmNvisualPolicy, XmVARIABLE,
					/* Instead of call to XmScrolledWindowSetAreas() */
					XmNworkWindow, w, 
					XmNhorizontalScrollBar, tree->list.hsb,
					XmNverticalScrollBar, tree->list.vsb,
					NULL);
	  XtFree(name);
   }
}

static void
InitializeGeometry(Widget w)
{
   ListTreeWidget tree = (ListTreeWidget) w;
   tree->list.XOffset = 0;

   if (XtHeight(w) < 10) {
	  int working;

	  working = FontHeight(tree->list.font);
	  if (tree->list.maxPixHeight>working) working=tree->list.maxPixHeight;
	  working+=tree->list.VSpacing;
    
	  if (tree->list.visibleCount==0)
		 tree->list.visibleCount=1;

	  tree->list.preferredHeight=working*tree->list.visibleCount;
	  tree->list.preferredWidth=200;
	  XtWidth(w)=tree->list.preferredWidth + 2*Prim_ShadowThickness(w)
		 + 2*Prim_HighlightThickness(w);
	  XtHeight(w)=tree->list.preferredHeight + 2*Prim_ShadowThickness(w)
		 + 2*Prim_HighlightThickness(w);
   }
   else {
	  tree->list.preferredWidth = XtWidth(w) - 2*Prim_ShadowThickness(w)
		 - 2*Prim_HighlightThickness(w);
	  tree->list.preferredHeight = XtHeight(w) - 2*Prim_ShadowThickness(w)
		 - 2*Prim_HighlightThickness(w);
   }    
   /* DBG(DARG,"prefWidth=%d prefHeight=%d\n", */
   /* 	   tree->list.preferredWidth,tree->list.preferredHeight); */
}


static void
Initialize(Widget request, Widget w, ArgList args, Cardinal * num)
{
   ListTreeWidget tree = (ListTreeWidget) w;

   tree->list.ret_item_list = NULL;
   tree->list.ret_item_alloc = 0;
   tree->list.first = tree->list.highlighted = tree->list.topItem = NULL;
   tree->list.topItemPos = tree->list.lastItemPos = tree->list.bottomItemPos =
	  tree->list.itemCount = tree->list.visibleCount = 0;

   tree->list.Refresh = True;
   tree->list.HasFocus=False;

   tree->list.timer_id = (XtIntervalId) 0;
   tree->list.multi_click_time = XtGetMultiClickTime(XtDisplay(w));

   tree->list.hsb = tree->list.vsb=NULL;
   tree->list.hsbPos=0;
   tree->list.hsbMax=1;

   InitializeScrollBars(w);

   InitializeGC(w);

   InitializePixmaps(w);

   tree->list.visibleCount=10;
   InitializeGeometry(w);
}

static void
Destroy(Widget w)
{
   ListTreeWidget tree = (ListTreeWidget) w;
   ListTreeItem *item, *sibling;

   XtReleaseGC((Widget) w, tree->list.drawGC);
   XtReleaseGC((Widget) w, tree->list.highlightGC);
   item = tree->list.first;
   while (item) {
	  if (item->firstchild) {
		 DeleteChildren(w, item->firstchild);
	  }
	  sibling = item->nextsibling;
	  XtFree((char *) item->text);
	  XtFree((char *) item);
	  item = sibling;
   }
   FreePixmap(w, &tree->list.Closed);
   FreePixmap(w, &tree->list.Open);
   FreePixmap(w, &tree->list.Leaf);
   FreePixmap(w, &tree->list.LeafOpen);
}

static void
Redisplay(Widget w, XEvent * ievent, Region region)
{
   /* Qi changed input argument type for event from XExposeEvent to XEvent */
   XExposeEvent *event = (XExposeEvent *) ievent;

   /* ListTreeWidget tree = (ListTreeWidget) w; */

   if (!XtIsRealized(w))
	  return;

   if (event) {
	  Draw(w, (int) event->y, (int) event->height);
   }
   else {			/* event==NULL ==> repaint the entire list */
	  DrawChanged(w);
   }

   /*
	 _XmDrawShadows(XtDisplay(w),
	 XtWindow(w),
	 Prim_TopShadowGC(w),
	 Prim_BottomShadowGC(w),
	 Prim_HighlightThickness(w), Prim_HighlightThickness(w),
	 XtWidth(w) - 2 * Prim_HighlightThickness(w),
	 XtHeight(w) - 2 * Prim_HighlightThickness(w),
	 Prim_ShadowThickness(w),
	 XmSHADOW_IN);
   */
}

static Boolean
SetValues(Widget current, Widget request, Widget reply, ArgList args, Cardinal * nargs)
{
   if (!XtIsRealized(current))
	  return False;

   return True;
}

static void
ResizeStuff(Widget w)
{
   XRectangle clip;
   ListTreeWidget tree = (ListTreeWidget) w;

   tree->list.viewWidth = tree->core.width - 2*Prim_ShadowThickness(w)
    - 2*Prim_HighlightThickness(w);
  tree->list.viewHeight = tree->core.height - 2*Prim_ShadowThickness(w)
    - 2*Prim_HighlightThickness(w);
  
  tree->list.viewX=Prim_ShadowThickness(w)+Prim_HighlightThickness(w);
  tree->list.viewY=Prim_ShadowThickness(w)+Prim_HighlightThickness(w);

  clip.x = tree->list.viewX;
  clip.y = tree->list.viewY;
  clip.width = tree->list.viewWidth;
  clip.height = tree->list.viewHeight;
  XSetClipRectangles(XtDisplay((Widget) w), tree->list.drawGC,
    0, 0, &clip, 1, Unsorted);
  XSetClipRectangles(XtDisplay((Widget) w), tree->list.eorGC,
    0, 0, &clip, 1, Unsorted);
  XSetClipRectangles(XtDisplay((Widget) w), tree->list.highlightGC,
    0, 0, &clip, 1, Unsorted);

  CountAll(w);

  tree->list.visibleCount = 1;
  if (tree->list.itemHeight>0) {
    tree->list.visibleCount=tree->list.viewHeight/(tree->list.itemHeight +
      tree->list.VSpacing);
  }
}

#define HSB2X(w) tree->list.XOffset=-((int) tree->list.Margin - tree->list.Indent + \
    (tree->list.Indent + tree->list.pixWidth)*tree->list.hsbPos);

static void
SetScrollbars(Widget w)
{
   ListTreeWidget tree = (ListTreeWidget) w;
  if (tree->list.vsb) {
    if (tree->list.itemCount == 0) {
      XtVaSetValues(tree->list.vsb,
        XmNvalue, 0,
        XmNsliderSize, 1,
        XmNpageIncrement, 1,
        XmNmaximum, 1,
        NULL);
    }
    else {
      int top,bot,size;

      top=tree->list.topItemPos;
      bot=tree->list.itemCount;
      size=tree->list.visibleCount;
      /* DBG(DARG,"BEFORE: top=%d bot=%d size=%d ",top,bot,size); */
      if (top+size>bot) bot=top+size;
      /* DBG(DARG,"  AFTER: bot=%d\n",bot); */

      XtVaSetValues(tree->list.vsb,
        XmNvalue, top,
        XmNsliderSize, size,
        XmNpageIncrement, tree->list.visibleCount,
        XmNmaximum, bot,
        NULL);
      if (size==bot)
        XmScrollBarSetValues(tree->list.vsb,top,size,1,size,False);
    }
  }

  if (tree->list.hsb) {
    int divisor,view;

    divisor=tree->list.Indent + tree->list.pixWidth;
    view=(tree->list.viewWidth+divisor-1)/divisor;
    tree->list.hsbMax=(tree->list.preferredWidth+divisor-1)/divisor;
    if (tree->list.hsbPos>0 &&
      tree->list.hsbPos+view>tree->list.hsbMax) {
      int save=tree->list.hsbPos;

      tree->list.hsbPos=tree->list.hsbMax-view;
      if (tree->list.hsbPos<0) tree->list.hsbPos=0;
      if (save!=tree->list.hsbPos) {
        HSB2X(w);
        DrawAll(w);
      }
    }
    if (tree->list.itemCount == 0 || tree->list.preferredWidth==0) {
      XtVaSetValues(tree->list.hsb,
        XmNvalue, 0,
        XmNsliderSize, 1,
        XmNpageIncrement, 1,
        XmNmaximum, 1,
        NULL);
    }
    else {
      XtVaSetValues(tree->list.hsb,
        XmNvalue, tree->list.hsbPos,
        XmNsliderSize, min(tree->list.hsbMax,view),
        XmNpageIncrement, view,
        XmNmaximum, tree->list.hsbMax,
        NULL);
    }
  }
  
  /* DBG(DARG,"item=%d visible=%d\n", tree->list.itemCount, tree->list.visibleCount); */
}

static void
VSBCallback(Widget scrollbar, XtPointer client_data, XtPointer call_data)
{
  ListTreeWidget tree = (ListTreeWidget) client_data;
  Widget w = (Widget) client_data;

  XmScrollBarCallbackStruct *cbs = (XmScrollBarCallbackStruct *)call_data;
  
  tree->list.topItemPos=cbs->value;

  /* DBG(DARG,"topItemPos=%d\n",tree->list.topItemPos); */
#if 0
  DBG(DARG,"VSBCallback: cbs->reason=%d ",cbs->reason);
  if (cbs->reason==XmCR_INCREMENT) {
    DBG(DARG,"increment\n");
  }
  else if (cbs->reason==XmCR_DECREMENT) {
    DBG(DARG,"decrement\n");
  }
  else if (cbs->reason==XmCR_VALUE_CHANGED) {
    DBG(DARG,"value_changed\n");
    SetScrollbars(w);
  }
#else  
  if (tree->list.topItemPos!=tree->list.lastItemPos) {
    GotoPosition(w);
    DrawAll(w);
    SetScrollbars(w);
  }
#endif
}

static void
HSBCallback(Widget scrollbar, XtPointer client_data, XtPointer call_data)
{
  Widget w = (Widget) client_data;
  ListTreeWidget tree = (ListTreeWidget) client_data;
  XmScrollBarCallbackStruct *cbs = (XmScrollBarCallbackStruct *)call_data;

  tree->list.hsbPos=cbs->value;
  HSB2X(w);

  /* DBG(DARG,"XOffset=%d prefWidth=%d viewWidth=%d\n", */
  /*   tree->list.XOffset,tree->list.preferredWidth,tree->list.viewWidth); */
  if (tree->list.XOffset!=tree->list.lastXOffset) {
    DrawAll(w);
  }
}

static void
Resize(Widget w)
{
  if (!XtIsRealized(w))
    return;

  ResizeStuff(w);
  SetScrollbars(w);
}

static XtGeometryResult
QueryGeometry(Widget w, XtWidgetGeometry *proposed, XtWidgetGeometry *answer)
{
   ListTreeWidget tree = (ListTreeWidget) w;
  answer->request_mode = CWWidth | CWHeight;
  answer->width = tree->list.preferredWidth + 2*Prim_ShadowThickness(w)
    + 2*Prim_HighlightThickness(w);
  answer->height = tree->list.preferredHeight + 2*Prim_ShadowThickness(w)
    + 2*Prim_HighlightThickness(w);
  
  /* DBG(DARG,"w=%d h=%d\n", answer->width, answer->height); */
  
  if (proposed->width >= answer->width && proposed->height >= answer->height) 
    return XtGeometryYes;
  else if (answer->width == XtWidth(w) && answer->height == XtHeight(w)) {
    answer->request_mode = 0;
    return XtGeometryNo;
  }
  else 
    return XtGeometryAlmost;    
}

static void
Realize(Widget w, XtValueMask * value_mask, XSetWindowAttributes * attributes)
{
   /* ListTreeWidget tree = (ListTreeWidget) w; */

#define	superclass	(&xmPrimitiveClassRec)
  (*superclass->core_class.realize) (w, value_mask, attributes);
#undef	superclass

  ResizeStuff(w);
  SetScrollbars(w);
}



/* DEBUGGING FUNCTIONS */
#ifdef DEBUG_TREE
void
ItemCheck(Widget w, ListTreeItem * item)
{
  ListTreeItem *p;
  char text[1024];

  p = item;
/*      if (p->parent) fprintf(stderr,"%x %x \t",p,p->parent); */
/*      else fprintf(stderr,"%x 00000000 \t",p); */
/*      while (p) { fprintf(stderr," "); p=p->parent; } */
/*      p=item; */
/*      while (p) { */
/*              fprintf(stderr,"%s/",p->text); */
/*              p=p->parent; */
/*      } */
/*      fprintf(stderr,"\n"); */

  if (strcmp(item->text, "pixmaps") == 0) {
    fprintf(stderr, "parent:      %x\n", item->parent);
    fprintf(stderr, "firstchild:  %x\n", item->firstchild);
    fprintf(stderr, "prevsibling: %x\n", item->prevsibling);
    fprintf(stderr, "nextsibling: %x\n", item->nextsibling);
  }
}

void
ChildrenCheck(Widget w, ListTreeItem * item)
{
  while (item) {
    ItemCheck(w, item);
    if (item->firstchild)
      ChildrenCheck(w, item->firstchild);
    item = item->nextsibling;
  }
}

void
TreeCheck(Widget w, char *txt)
{
  ListTreeItem *item;

  fprintf(stderr, "\n\n%s\n", txt);
  item = tree->list.first;
  while (item) {
    ItemCheck(w, item);
    if (item->firstchild)
      ChildrenCheck(w, item->firstchild);
    item = item->nextsibling;
  }
}
#else
#define TreeCheck(a,b)
#endif



/* Highlighting Utilities ----------------------------------------------- */

static void
HighlightItem(Widget w, ListTreeItem * item, Boolean state, Boolean draw)
{
   ListTreeWidget tree = (ListTreeWidget) w;
  if (item) {
    if (item == tree->list.highlighted && !state) {
      tree->list.highlighted = NULL;
      if (draw && item->count>=tree->list.topItemPos)
	DrawItemHighlightClear(w, item);
    }
    else if (state != item->highlighted) {
      /*      printf("Highlighting '%s' state=%d x=%d y=%d\n", item->text, draw, item->x, item->ytext); */
      item->highlighted = state;
      if (draw &&
        item->count>=tree->list.topItemPos &&
        item->count<=tree->list.bottomItemPos) DrawItemHighlightClear(w, item);
      
    }
  }
}

static void
HighlightChildren(Widget w, ListTreeItem * item, Boolean state, Boolean draw)
{
  while (item) {
    HighlightItem(w, item, state, draw);
    if (item->firstchild) {
      Boolean drawkids;

      if (item->open)
	drawkids = draw;
      else
	drawkids = False;
      HighlightChildren(w, item->firstchild, state, drawkids);
    }
    item = item->nextsibling;
  }
}

static void
HighlightAll(Widget w, Boolean state, Boolean draw)
{
   ListTreeWidget tree = (ListTreeWidget) w;
  HighlightChildren(w,tree->list.first,state,draw);
}

static void
HighlightVisibleChildren(Widget w, ListTreeItem * item, Boolean state, Boolean draw)
{
  while (item) {
    HighlightItem(w, item, state, draw);
    if (item->firstchild && item->open) {
      HighlightVisibleChildren(w, item->firstchild, state, draw);
    }
    item = item->nextsibling;
  }
}

static void
HighlightAllVisible(Widget w, Boolean state, Boolean draw)
{
  ListTreeItem *item;
   ListTreeWidget tree = (ListTreeWidget) w;

  item = tree->list.first;
  while (item) {
    HighlightItem(w, item, state, draw);
    if (item->firstchild && item->open) {
      HighlightVisibleChildren(w, item->firstchild, state, draw);
    }
    item = item->nextsibling;
  }
}

static void
AddItemToReturnList(Widget w, ListTreeItem * item, int loc)
{
   ListTreeWidget tree = (ListTreeWidget) w;
  if (loc >= tree->list.ret_item_alloc) {
    tree->list.ret_item_alloc += ListTreeRET_ALLOC;
    tree->list.ret_item_list = (ListTreeItem **) XtRealloc((char *) tree->list.ret_item_list, tree->list.ret_item_alloc * sizeof(ListTreeItem *));
  }
  tree->list.ret_item_list[loc] = item;
}

static void
MultiAddToReturn(Widget w, ListTreeItem * item, ListTreeMultiReturnStruct * ret)
{
   ListTreeWidget tree = (ListTreeWidget) w;
  AddItemToReturnList(w, item, ret->count);
  ret->items = tree->list.ret_item_list;
  ret->count++;
}

static void
HighlightCount(Widget w, ListTreeItem * item, ListTreeMultiReturnStruct * ret)
{
  while (item) {
    if (item->highlighted)
      MultiAddToReturn(w, item, ret);
    if (item->firstchild && item->open)
      HighlightCount(w, item->firstchild, ret);
    item = item->nextsibling;
  }
}

static void
MakeMultiCallbackStruct(Widget w, ListTreeMultiReturnStruct * ret)
{
  ListTreeItem *item;
   ListTreeWidget tree = (ListTreeWidget) w;

  ret->items = NULL;
  ret->count = 0;
  item = tree->list.first;
  while (item) {
    if (item->highlighted)
      MultiAddToReturn(w, item, ret);
    if (item->firstchild && item->open)
      HighlightCount(w, item->firstchild, ret);
    item = item->nextsibling;
  }
}

static void
HighlightDoCallback(Widget w)
{
  ListTreeMultiReturnStruct ret;
   ListTreeWidget tree = (ListTreeWidget) w;

  if (tree->list.HighlightCallback) {
    MakeMultiCallbackStruct(w, &ret);
    XtCallCallbacks((Widget) w, XtNhighlightCallback, &ret);
  }
}

/* Events ------------------------------------------------------------------ */


static void
MakeActivateCallbackStruct(Widget w, ListTreeItem * item, ListTreeActivateStruct * ret)
{
  int count;
  ListTreeItem *parent;
   ListTreeWidget tree = (ListTreeWidget) w;

  count = 1;
  parent = item;
  while (parent->parent) {
    parent = parent->parent;
    count++;
  }

  ret->item = item;
  ret->count = count;
  ret->open = item->open;
  if (item->firstchild)
    ret->reason = XtBRANCH;
  else
    ret->reason = XtLEAF;
  while (count > 0) {
    count--;
    AddItemToReturnList(w, item, count);
    item = item->parent;
  }
  ret->path = tree->list.ret_item_list;
}

static void
SelectDouble(Widget w)
{
  ListTreeActivateStruct ret;
   ListTreeWidget tree = (ListTreeWidget) w;

  TreeCheck(w, "in SelectDouble");
  if (tree->list.timer_item) {
    tree->list.timer_type = TIMER_DOUBLE;
    tree->list.timer_item->open = !tree->list.timer_item->open;
    tree->list.highlighted = tree->list.timer_item;
    HighlightAll(w, False, True);

    MakeActivateCallbackStruct(w, tree->list.timer_item, &ret);
    
    /* Highlight the path if we need to */
    if (tree->list.HighlightPath) {
      Boolean save;

      save=tree->list.Refresh;
      tree->list.Refresh=False;
      ListTreeSetHighlighted(w,ret.path,ret.count,True);
      tree->list.Refresh=save;
/*       ListTreeGetHighlighted(w,&ret2); */
/*       ListTreeSetHighlighted(w,ret2.items,ret2.count,True); */
    }
    
    if (tree->list.ActivateCallback) {
      XtCallCallbacks((Widget) w, XtNactivateCallback, (XtPointer) & ret);
    }
    
    tree->list.timer_item->highlighted = True;
    tree->list.recount = True;
    DrawChanged(w);
  }
  TreeCheck(w, "exiting SelectDouble");
}

/* ARGSUSED */
static void
SelectSingle(XtPointer client_data, XtIntervalId * idp)
{
  ListTreeWidget tree = (ListTreeWidget) client_data;
  Widget w = (Widget) client_data;

  tree->list.timer_id = (XtIntervalId) 0;
  if (tree->list.timer_item) {
    if (tree->list.ClickPixmapToOpen && tree->list.timer_x < tree->list.timer_item->x) {
      SelectDouble(w);
    }
    else {
      HighlightAll(w, False, True);
      HighlightItem(w, tree->list.timer_item, True, True);
      if (tree->list.timer_type != TIMER_CLEAR && tree->list.DoIncrementalHighlightCallback)
        HighlightDoCallback(w);
      tree->list.timer_type = TIMER_SINGLE;
    }
  }
}

/* ARGSUSED */
static void
select_start(Widget w, XEvent *event, String *params, Cardinal *num_params)
{
  ListTreeWidget tree = (ListTreeWidget) w;

  tree->list.timer_item=NULL;
  tree->list.timer_x = event->xbutton.x - tree->list.XOffset;
  tree->list.timer_y = event->xbutton.y;
  tree->list.timer_type = TIMER_WAITING;
  tree->list.timer_item = GetItem(w, event->xbutton.y);
  
  if (!tree->list.timer_item) {
    if (tree->list.timer_id) {
      XtRemoveTimeOut(tree->list.timer_id);
      tree->list.timer_id = (XtIntervalId) 0;
    }
  }
  else {
    if (tree->list.timer_id) {
      XtRemoveTimeOut(tree->list.timer_id);
      tree->list.timer_id = (XtIntervalId) 0;
      SelectDouble(w);
    }
    else {
      tree->list.timer_id = XtAppAddTimeOut(
        XtWidgetToApplicationContext((Widget) w),
        (unsigned long) tree->list.multi_click_time,
        SelectSingle,
        (XtPointer) w);
      
    }
  }
}

/* ARGSUSED */
static void
extend_select_start(Widget w, XEvent *event, String *params, Cardinal *num_params)
{
  ListTreeWidget tree = (ListTreeWidget) w;

  tree->list.timer_item=NULL;
  tree->list.timer_x = event->xbutton.x;
  tree->list.timer_y = event->xbutton.y;
  tree->list.timer_type = TIMER_WAITING;
  tree->list.timer_item = GetItem(w, event->xbutton.y);
  tree->list.timer_id = (XtIntervalId) 0;
  HighlightItem(w, tree->list.timer_item, True, True);
  if (tree->list.timer_type != TIMER_CLEAR &&
      tree->list.DoIncrementalHighlightCallback)
    HighlightDoCallback(w);
}


/* ARGSUSED */
static void
extend_select(Widget w, XEvent *event, String *params, Cardinal *num_params)
{
  ListTreeItem *item;
  ListTreeWidget tree = (ListTreeWidget) w;
  int y, yend;

/* If we are waiting for a double click, return before doing anything */
  if (tree->list.timer_id)
    return;

/* We need the timer_item to be pointing to the first selection in this */
/* group.  If we got here without it being set, something is very wrong. */
  if (tree->list.timer_item) {
    y = tree->list.timer_y;
    yend = event->xbutton.y;
    item = GetItem(w, y);
    if (y < yend) {
      while (item && y < yend && y < tree->list.viewY+tree->list.viewHeight) {
        if (item) {
          /* DBG(DARG,"Highlighting y=%d item=%s\n",y,item->text); */
          HighlightItem(w, item, True, True);
          y += item->height + tree->list.VSpacing;
        }
        item = GetItem(w, y);
      }
    }
    else {
      while (item && y > yend && y > 0) {
        if (item) {
          /* DBG(DARG,"Highlighting y=%d item=%s\n",y,item->text); */
          HighlightItem(w, item, True, True);
          y -= item->height + tree->list.VSpacing;
        }
        item = GetItem(w, y);
      }
    }
    if (tree->list.timer_type != TIMER_CLEAR && tree->list.DoIncrementalHighlightCallback)
      HighlightDoCallback(w);
  }
}

/* ARGSUSED */
static void
unset(Widget w, XEvent *event, String *params, Cardinal *num_params)
{
  ListTreeItem *item;
  /* ListTreeWidget tree = (ListTreeWidget) w; */

  item = GetItem(w, event->xbutton.y);
  if (item) {
/*              item->open=False; */
/*              ltree->list.highlighted=item; */
/*              DrawAll(lw); */
/*              ListTreeDelete(lw,item); */
  }
}

/* ARGSUSED */
static void
notify(Widget w, XEvent *event, String *params, Cardinal *num_params)
{
  ListTreeWidget tree = (ListTreeWidget) w;

  if (tree->list.timer_id) {
    /* don't call highlightCallback if we are waiting for a double click */
  }
  else if (tree->list.timer_type != TIMER_CLEAR && !tree->list.DoIncrementalHighlightCallback) {
    HighlightDoCallback(w);
    tree->list.timer_type = TIMER_CLEAR;
  }
}

/* ARGSUSED */
static void
focus_in(Widget w, XEvent *event, String *params, Cardinal *num_params)
{
  ListTreeWidget tree = (ListTreeWidget) w;

  DBGW("focus_in");

  if (!tree->list.HasFocus) {
    XtCallActionProc(w, "PrimitiveFocusIn", event, params, *num_params);

    tree->list.HasFocus=True;
  }
}

/* ARGSUSED */
static void
focus_out(Widget w, XEvent *event, String *params, Cardinal *num_params)
{
  ListTreeWidget tree = (ListTreeWidget) w;

  DBGW("focus_out");
  
  if (tree->list.HasFocus) {
    XtCallActionProc(w, "PrimitiveFocusOut", event, params, *num_params);

    tree->list.HasFocus=False;
  }
}

/* ARGSUSED */
static void menu(Widget w, XEvent *event, String *params, Cardinal *num_params)
{
  ListTreeWidget tree = (ListTreeWidget) w;
  ListTreeItem *item;
  ListTreeItemReturnStruct ret;

  if (tree->list.MenuCallback) {
    
      /* See if there is an item at the position of the click */
    item=GetItem (w, event->xbutton.y);
  
    if (item) {
      ret.reason = XtMENU;
      ret.item = item;
      ret.event = event;
      XtCallCallbacks((Widget) w, XtNmenuCallback, &ret);
    }
  }
}

/* ARGSUSED */
static void
keypress(Widget w, XEvent *event, String *params, Cardinal *num_params)
{
  // ListTreeWidget tree = (ListTreeWidget) w;
  /* DBG(DARG,"keypress\n"); */
}




/* ListTree private drawing functions ------------------------------------- */

/* Select the pixmap to use, if any */
static Pixinfo *
GetItemPix(Widget w, ListTreeItem *item)
{
   ListTreeWidget tree = (ListTreeWidget) w;
  Pixinfo *pix;

  pix=NULL;

    /* Another enhancement from Alan Marcinkowski */
  if (item->openPixmap || item->closedPixmap) {
      /* Guess that it is closed. */
      Pixmap pixmap = item->closedPixmap;

      Window       root;
      unsigned int pixwidth, pixheight, pixbw, pixdepth;
	  int pixx, pixy;

      /* If it is not closed and there is a pixmap for it, then use that one
       * instead.
       */
      if (item->open && item->openPixmap) {
          pixmap = item->openPixmap;
      }

      /* Make sure we got one. */
      if (pixmap) {
          /* Get the geometry of the pixmap. */
          XGetGeometry(XtDisplay((Widget) w), pixmap, &root, &pixx, &pixy,
                       &pixwidth, &pixheight, &pixbw, &pixdepth);

          /* Setup the temporary one that will be used and point to it. */
          tree->list.ItemPix.width  = (int) pixwidth;
          tree->list.ItemPix.height = (int) pixheight;
          tree->list.ItemPix.xoff   = 0;
          tree->list.ItemPix.pix    = pixmap;
          pix = &tree->list.ItemPix;
      }
  }

    /* If we don't have a pixmap yet... */
  if (!pix) {
    if (item->firstchild || item->type == ItemBranchType) {
      if (item->open)
        pix = &tree->list.Open;
      else
        pix = &tree->list.Closed;
    }
    else {
      if (item->open)
        pix = &tree->list.LeafOpen;
      else
        pix = &tree->list.Leaf;
    }
  }
  
  return pix;
}

static void
DrawItemHighlight(Widget w, ListTreeItem *item)
{
   ListTreeWidget tree = (ListTreeWidget) w;
  int width;

  if (item->highlighted || item == tree->list.highlighted) {
    width = tree->core.width - item->x - tree->list.XOffset;
    XFillRectangle(XtDisplay(w), XtWindow(w),
      tree->list.drawGC,
      item->x + tree->list.XOffset, item->ytext,
      width, FontHeight(tree->list.font));
    XDrawString(XtDisplay(w), XtWindow(w), tree->list.highlightGC,
      item->x + tree->list.XOffset, item->ytext + FontAscent(tree->list.font),
      item->text, item->length);
  }
  else {
    XDrawString(XtDisplay(w), XtWindow(w), tree->list.drawGC,
      item->x + tree->list.XOffset, item->ytext + FontAscent(tree->list.font),
      item->text, item->length);
  }
}

static void
DrawItemHighlightClear(Widget w, ListTreeItem *item)
{
   ListTreeWidget tree = (ListTreeWidget) w;
  int width;

  width = tree->core.width - item->x - tree->list.XOffset;
  if (item->highlighted || item == tree->list.highlighted) {
    XFillRectangle(XtDisplay(w), XtWindow(w),
      tree->list.drawGC,
      item->x + tree->list.XOffset, item->ytext,
      width, FontHeight(tree->list.font));
    XDrawString(XtDisplay(w), XtWindow(w), tree->list.highlightGC,
      item->x + tree->list.XOffset, item->ytext + FontAscent(tree->list.font),
      item->text, item->length);
  }
  else {
    XFillRectangle(XtDisplay(w), XtWindow(w),
      tree->list.highlightGC,
      item->x + tree->list.XOffset, item->ytext,
      width, FontHeight(tree->list.font));
    XDrawString(XtDisplay(w), XtWindow(w), tree->list.drawGC,
      item->x + tree->list.XOffset, item->ytext + FontAscent(tree->list.font),
      item->text, item->length);
  }
}

static void
DrawItem(Widget w, ListTreeItem *item,
  int y, int *xroot, int *yroot,
  int *retwidth, int *retheight)
{
   ListTreeWidget tree = (ListTreeWidget) w;
  int height, xpix, ypix, xbranch, ybranch, xtext, ytext, yline;
  Pixinfo *pix;

  if (item->count<tree->list.topItemPos) {
    *xroot = item->x - (int) tree->list.HSpacing - tree->list.pixWidth/2;
    *yroot = 0;
    *retwidth = *retheight = 0;
    return;
  }
  
  pix=GetItemPix(w,item);

/* Compute the height of this line */
  height = FontHeight(tree->list.font);
  xtext = item->x;
  xpix = xtext - (int) tree->list.HSpacing - tree->list.pixWidth + pix->xoff;
  
  if (pix) {
    if (pix->height > height) {
      ytext = y + ((pix->height - height) / 2);
      height = pix->height;
      ypix = y;
    }
    else {
      ytext = y;
      ypix = y + ((height - pix->height) / 2);
    }
    ybranch = ypix + pix->height;
    yline = ypix + (pix->height / 2);
  }
  else {
    ypix = ytext = y;
    yline = ybranch = ypix + (height / 2);
    yline = ypix + (height / 2);
  }
  xbranch=item->x - (int) tree->list.HSpacing - tree->list.pixWidth/2;

/* Save the basic graphics info for use by other functions */
  item->y = y;
  item->ytext = ytext;
  item->height = (Dimension)height;

  if ((*xroot >= 0) &&
    ((*yroot >= tree->list.exposeTop && *yroot <= tree->list.exposeBot) ||
      (yline >= tree->list.exposeTop && yline <= tree->list.exposeBot) ||
      (*yroot < tree->list.exposeTop && yline > tree->list.exposeBot)))
    XDrawLine(XtDisplay(w), XtWindow(w), tree->list.drawGC,
      *xroot + tree->list.XOffset, *yroot,
      *xroot + tree->list.XOffset, yline);
  if (y >= tree->list.exposeTop && y <= tree->list.exposeBot) {
    if (*xroot >= 0)
      XDrawLine(XtDisplay(w), XtWindow(w), tree->list.drawGC,
	*xroot + tree->list.XOffset, yline,
	xbranch + tree->list.XOffset, yline);
    if (pix && pix->pix)
      XCopyArea(XtDisplay(w), pix->pix, XtWindow(w),
	tree->list.drawGC,
	0, 0, pix->width, pix->height,
	xpix + tree->list.XOffset, ypix);
    DrawItemHighlight(w, item);
  }
  *xroot = xbranch;
  *yroot = ybranch;
  *retwidth = FontTextWidth(tree->list.font, item->text);
  *retheight = height;
}

static int
DrawChildren(Widget w, ListTreeItem *item, ListTreeItem **last,
  int y, int xroot, int yroot)
{
   ListTreeWidget tree = (ListTreeWidget) w;
  int width, height;
  int xbranch, ybranch;

  while (item && y<tree->list.exposeBot) {
    xbranch = xroot;
    ybranch = yroot;
    DrawItem(w, item, y, &xbranch, &ybranch, &width, &height);

    width += item->x + (int) tree->list.Margin;

    if (width > tree->list.preferredWidth)
      tree->list.preferredWidth = width;

    if (height>0)
      y += height + (int) tree->list.VSpacing;
    
    if (last) *last=item;
    
    if ((item->firstchild) && (item->open))
      y = DrawChildren(w, item->firstchild, last, y, xbranch, ybranch);

    item = item->nextsibling;
  }
  return y;
}

/*
 * Draws vertical lines connecting items to their siblings below the last
 * visible item
 */
static void
DrawVertical(Widget w, ListTreeItem *item)
{
   ListTreeWidget tree = (ListTreeWidget) w;
  int xroot,yroot;
  /* int pos = item->count; */

  while (item->parent) {
      /* If this parent has another child, that means that a line extends off
       * the screen to the bottom. */
    if (item->nextsibling) {
      xroot = item->parent->x - (int) tree->list.HSpacing - tree->list.pixWidth/2;
      if (item->parent->count<tree->list.topItemPos)
        yroot=0;
      else
        yroot=item->parent->y + item->parent->height;
      
      /* DBG(DARG,"parent=%s drawing x=%d y=%d\n", */
      /*   item->parent->text,xroot,yroot); */
      XDrawLine(XtDisplay(w), XtWindow(w), tree->list.drawGC,
        xroot + tree->list.XOffset, yroot,
        xroot + tree->list.XOffset, tree->list.exposeBot);
    }
    else {
      /* DBG(DARG,"parent=%s  NOT DRAWING\n",item->parent->text); */
    }
    
    
    item=item->parent;
  }
}

/* Draws items starting from topItemPos */
static void
Draw(Widget w, int yevent,int hevent)
{
   ListTreeWidget tree = (ListTreeWidget) w;
  int y, xbranch, ybranch;
  ListTreeItem *item,*lastdrawn;

  TreeCheck(w, "Draw");

  if (tree->list.recount)
    CountAll(w);
  
/* Overestimate the expose region to be sure to draw an item that gets */
/* cut by the region */
  tree->list.exposeTop = yevent - FontHeight(tree->list.font);
  tree->list.exposeBot = yevent + hevent + FontHeight(tree->list.font);
  tree->list.preferredWidth = 0;
  
  item = tree->list.topItem;
  if (!item) return; /* skip if this is an empty list */
  
  while (item->parent) item=item->parent;
  y = (int)tree->list.viewY + (int) tree->list.Margin;
  xbranch=-1;
  ybranch=0;
  
  DrawChildren(w, item, &lastdrawn, y, xbranch, ybranch);

  /* DBG(DARG,"lastdrawn=%s\n",lastdrawn->text); */
  tree->list.bottomItemPos=lastdrawn->count;

  DrawVertical(w,lastdrawn);

/*   SetScrollbars(w); */

  tree->list.lastItemPos=tree->list.topItemPos;
  tree->list.lastXOffset=tree->list.XOffset;
}

static void
DrawAll(Widget w)
{
   ListTreeWidget tree = (ListTreeWidget) w;
  XClearArea(XtDisplay(w), XtWindow(w),
    tree->list.viewX,tree->list.viewY,
    tree->list.viewWidth,tree->list.viewHeight,False);
  if (tree->list.recount)
    CountAll(w);
  Draw(w, tree->list.viewY, tree->list.viewY+tree->list.viewHeight);
}

static void
DrawChanged(Widget w)
{
  DrawAll(w);
  SetScrollbars(w);
}


/* Counting functions ------------------------------------------------------- */
static int
GotoPositionChildren(Widget w, ListTreeItem *item, int i)
{
   ListTreeWidget tree = (ListTreeWidget) w;
  while (item && i < tree->list.topItemPos) {
    i++;
    tree->list.topItem=item;
    
    if (item->firstchild && item->open && i<tree->list.topItemPos)
      i=GotoPositionChildren(w,item->firstchild,i);

    item=item->nextsibling;
  }
  return i;
}

static void
GotoPosition(Widget w)
{
   ListTreeWidget tree = (ListTreeWidget) w;
  tree->list.topItem=tree->list.first;
  GotoPositionChildren(w,tree->list.topItem,-1);
}

static int
CountItem(Widget w, ListTreeItem *item, int x, int y)
{
   ListTreeWidget tree = (ListTreeWidget) w;
  int height, /* xpix, */ xtext;
  Pixinfo *pix;

  item->count=tree->list.itemCount;
  tree->list.itemCount++;

/* Select the pixmap to use, if any */
  pix=GetItemPix(w,item);

/* Compute the height of this line */
  height = FontHeight(tree->list.font);
  /* xpix = x - tree->list.pixWidth + pix->xoff; */
  xtext = x + (int) tree->list.HSpacing;
  if (pix && pix->height > height) {
    height = pix->height;
  }

/* Save the basic graphics info for use by other functions */
  item->x = xtext;
  item->y = item->ytext = -1;
  item->height = (Dimension)height;

  if (item->height > tree->list.itemHeight) tree->list.itemHeight=item->height;

  return height;
}

static int
CountChildren(Widget w, ListTreeItem *item, int x, int y)
{
   ListTreeWidget tree = (ListTreeWidget) w;
  int height;

  x += (int) tree->list.Indent + tree->list.pixWidth;
  while (item) {
    height = CountItem(w, item, x, y);

    y += height + (int) tree->list.VSpacing;
    if ((item->firstchild) && (item->open))
      y = CountChildren(w, item->firstchild, x, y);

    item = item->nextsibling;
  }
  return y;
}

void
CountAll(Widget w)
{
   ListTreeWidget tree = (ListTreeWidget) w;
  int x,y;
  
  tree->list.itemCount = 0;
  tree->list.itemHeight = 0;
  tree->list.recount = False;

    /*
     * the first x must be tree->list.Margin + tree->list.pixWidth, so set it up
     * so CountChildren gets it right for the root items
     */
  x = (int)tree->list.viewX + (int) tree->list.Margin - tree->list.Indent;
  y = (int)tree->list.viewY + (int) tree->list.Margin;
  CountChildren(w,tree->list.first,x,y);
}


/* Private Functions --------------------------------------------------------- */


/* This function removes the specified item from the linked list.  It does */
/* not do anything with the data contained in the item, though. */
static void
RemoveReference(Widget w, ListTreeItem *item)
{
   ListTreeWidget tree = (ListTreeWidget) w;

/* If there exists a previous sibling, just skip over item to be dereferenced */
  if (item->prevsibling) {
    item->prevsibling->nextsibling = item->nextsibling;
    if (item->nextsibling)
      item->nextsibling->prevsibling = item->prevsibling;
  }
/* If not, then the deleted item is the first item in some branch. */
  else {
    if (item->parent)
      item->parent->firstchild = item->nextsibling;
    else
      tree->list.first = item->nextsibling;
    if (item->nextsibling)
      item->nextsibling->prevsibling = NULL;
  }

/* Don't forget to update topItem (Paul Newton <pkn@Cs.Nott.AC.UK> caught this )*/
  if (item == tree->list.topItem)
    tree->list.topItem = item->nextsibling;
}

static void
DeleteChildren(Widget w, ListTreeItem *item)
{
   ListTreeWidget tree = (ListTreeWidget) w;
  ListTreeItem *sibling;
  ListTreeItemReturnStruct ret;

  while (item) {
    if (item->firstchild) {
      DeleteChildren(w, item->firstchild);
      item->firstchild = NULL;
    }
    sibling = item->nextsibling;
    
    if (tree->list.DestroyItemCallback) {
      ret.reason = XtDESTROY;
      ret.item = item;
      ret.event = NULL;
      XtCallCallbacks((Widget) w, XtNdestroyItemCallback, &ret);
    }

    XtFree((char *) item->text);
    XtFree((char *) item);
    item = sibling;
  }
}

static void
InsertChild(Widget w, ListTreeItem *parent, ListTreeItem *item)
{
   ListTreeWidget tree = (ListTreeWidget) w;
   ListTreeItem *i;

   item->parent = parent;
   item->nextsibling = item->prevsibling = NULL;
   if (parent) {
	  if (parent->firstchild) {
		 i = parent->firstchild;
		 while (i->nextsibling) {
			i = i->nextsibling;
		 }
		 i->nextsibling = item;
		 item->prevsibling = i;
	  }
	  else {
		 parent->firstchild = item;
	  }

   }
   else {			/* if parent==NULL, this is a top level entry */
	  if (tree->list.first) {
		 i = tree->list.first;
		 while (i->nextsibling) {
			i = i->nextsibling;
		 }
		 i->nextsibling = item;
		 item->prevsibling = i;
	  }
	  else {
		 tree->list.first = tree->list.topItem = item;
	  }
   }
  tree->list.recount = True;
}

/* Insert a list of ALREADY LINKED children into another list */
static void
InsertChildren(Widget w, ListTreeItem *parent, ListTreeItem *item)
{
  ListTreeItem *next, *newnext;

/*      while (item) { */
/*              next=item->nextsibling; */
/*              InsertChild(w,parent,item); */
/*              item=next; */
/*      } */
/*      return; */


/* Save the reference for the next item in the new list */
  next = item->nextsibling;

/* Insert the first item in the new list into the existing list */
  InsertChild(w, parent, item);

/* The first item is inserted, with its prev and next siblings updated */
/* to fit into the existing list.  So, save the existing list reference */
  newnext = item->nextsibling;

/* Now, mark the first item's next sibling to point back to the new list */
  item->nextsibling = next;

/* Mark the parents of the new list to the new parent.  The order of the */
/* rest of the new list should be OK, and the second item should still */
/* point to the first, even though the first was reparented. */
  while (item->nextsibling) {
    item->parent = parent;
    item = item->nextsibling;
  }

/* Fit the end of the new list back into the existing list */
  item->nextsibling = newnext;
  if (newnext)
    newnext->prevsibling = item;
}

static int
SearchChildren(Widget w, ListTreeItem *item, ListTreeItem **last,
  int y, int findy, ListTreeItem **finditem)
{
   ListTreeWidget tree = (ListTreeWidget) w;
  while (item) {
    /* DBG(DARG,"searching y=%d item=%s\n",y,item->text); */
    if (findy >= y && findy <= y + item->height + tree->list.VSpacing) {
      *finditem = item;
      return -1;
    }
    y += item->height + (int) tree->list.VSpacing;
    if ((item->firstchild) && (item->open)) {
      y = SearchChildren(w, item->firstchild, NULL,
	y, findy, finditem);
      if (*finditem)
	return -1;
    }
    if (last) *last=item;
    item = item->nextsibling;
  }
  return y;
}

static ListTreeItem *
GetItem(Widget w, int findy)
{
   ListTreeWidget tree = (ListTreeWidget) w;
  int y;
  ListTreeItem *item, *finditem, *lastdrawn;

  TreeCheck(w, "in GetItem");
  y = (int)tree->list.viewY + (int) tree->list.Margin;
  item = tree->list.topItem;
  finditem=NULL;
  lastdrawn=item;
  while (!finditem && lastdrawn && y < tree->core.height) {
    y = SearchChildren(w, item, &lastdrawn, y, findy, &finditem);
    
      /*
       * If there are still more items to draw, but the previous group of
       * siblings ran out, start checking up through the parents for more
       * items.
       */
    if (lastdrawn->parent && y<tree->core.height) {
      ListTreeItem *parent;

        /* continue with the item after the parent of the previous group */
      parent=lastdrawn;
      do {
        parent=parent->parent;
        if (parent) item=parent->nextsibling;
        else item=NULL;
      } while (parent && !item);
     if (!item) lastdrawn=NULL;
    }
    else lastdrawn=NULL;
  }
  TreeCheck(w, "exiting GetItem");
  return finditem;
}

static int
SearchPosition(Widget w, ListTreeItem *item, ListTreeItem *finditem,
			   int y, Boolean *found)
{
   ListTreeWidget tree = (ListTreeWidget) w;
  int height;
  Pixinfo *pix;

  while (item) {
/*              DBG(DARG,"Checking y=%d  item=%s\n",y,item->text); */
    if (item == finditem) {
      *found = True;
      return y;
    }

    pix=GetItemPix(w,item);

	/* Compute the height of this line */

    height = FontHeight(tree->list.font);
    if (pix && pix->height > height)
      height = pix->height;

    y += height + (int) tree->list.VSpacing;
    if ((item->firstchild) && (item->open)) {
      y = SearchPosition(w, item->firstchild, finditem, y, found);
      if (*found)
		return y;
    }
    item = item->nextsibling;
  }
  return y;
}

static Position
GetPosition(Widget w, ListTreeItem *finditem)
{
   ListTreeWidget tree = (ListTreeWidget) w;

  int y, height;
  ListTreeItem *item;
  Pixinfo *pix;
  Boolean found;

  TreeCheck(w, "in GetPosition");
  y = (int)tree->list.viewY + (int) tree->list.Margin;
  item = tree->list.first;
  found = False;
  while (item && item != finditem) {

    pix=GetItemPix(w,item);
    
/* Compute the height of this line */
    height = FontHeight(tree->list.font);
    if (pix && pix->height > height)
      height = pix->height;

    y += height + (int) tree->list.VSpacing;
    if ((item->firstchild) && (item->open)) {
       y = SearchPosition(w, item->firstchild, finditem, y, &found);
	   if (found)
		  return (Position) y;
    }
    item = item->nextsibling;
  }
  TreeCheck(w, "exiting GetPosition");
  if (item != finditem)
    y = 0;
  return (Position) y;
}



/* Public Functions --------------------------------------------------------- */

void
ListTreeRefresh(Widget w)
{
   ListTreeWidget tree = (ListTreeWidget) w;
   if (XtIsRealized(w) && tree->list.Refresh)
	  DrawChanged(w);
}

void
ListTreeRefreshOff(Widget w)
{
  ((ListTreeWidget)w)->list.Refresh = False;
}

void
ListTreeRefreshOn(Widget w)
{
   ListTreeWidget tree = (ListTreeWidget) w;
   tree->list.Refresh = True;
   ListTreeRefresh(w);
}

static ListTreeItem *
AddItem(Widget w, ListTreeItem * parent, char *string,
  ListTreeItemType type)
{
  ListTreeItem *item;
  int len;
  char *copy;

  TreeCheck(w, "in ListTreeAdd");
  len = strlen(string);
  item = (ListTreeItem *) XtMalloc(sizeof(ListTreeItem));
  copy = (char *) XtMalloc(len + 1);
  strcpy(copy, string);
  item->text = copy;
  item->length = len;
  item->type=type;
  item->parent = parent;
  item->open = False;
  item->highlighted = False;
  item->openPixmap = item->closedPixmap = (Pixmap)NULL;
  item->firstchild = item->prevsibling = item->nextsibling = NULL;
  InsertChild(w, parent, item);

  ListTreeRefresh(w);

  return item;
}

ListTreeItem *
ListTreeAdd(Widget w, ListTreeItem *parent,char *string)
{
    return (AddItem (w,parent,string,ItemDetermineType));
}

ListTreeItem *
ListTreeAddType(Widget w,ListTreeItem *parent,char *string,
                ListTreeItemType type)
{
    return (AddItem (w,parent,string,type));
}

ListTreeItem *
ListTreeAddBranch(Widget w,ListTreeItem *parent,char *string)
{
    return (AddItem (w,parent,string,ItemBranchType));
}

ListTreeItem *
ListTreeAddLeaf(Widget w,ListTreeItem *parent,char *string)
{
   return (AddItem (w,parent,string,ItemLeafType));
}

void
ListTreeSetItemPixmaps (Widget w, ListTreeItem *item,
                        Pixmap openPixmap, Pixmap closedPixmap)
{
    item->openPixmap   = openPixmap;
    item->closedPixmap = closedPixmap;
}

void
ListTreeRenameItem(Widget w, ListTreeItem * item, char *string)
{
  int len;
  char *copy;

  TreeCheck(w, "in ListTreeRename");
  XtFree(item->text);
  len = strlen(string);
  copy = (char *) XtMalloc(len + 1);
  strcpy(copy, string);
  item->text = copy;
  item->length = len;

  ListTreeRefresh(w);
}

int
ListTreeDelete(Widget w, ListTreeItem * item)
{
  if (item->firstchild)
    DeleteChildren(w, item->firstchild);
  item->firstchild = NULL;

  RemoveReference(w, item);

  XtFree((char *) item->text);
  XtFree((char *) item);

  ListTreeRefresh(w);

  return 1;
}

int
ListTreeDeleteChildren(Widget w, ListTreeItem * item)
{
  if (item->firstchild)
    DeleteChildren(w, item->firstchild);
  item->firstchild = NULL;

  ListTreeRefresh(w);

  return 1;
}

int
ListTreeReparent(Widget w, ListTreeItem * item, ListTreeItem * newparent)
{
  TreeCheck(w, "in ListTreeReparent");
/* Remove the item from its old location. */
  RemoveReference(w, item);

/* The item is now unattached.  Reparent it.                     */
  InsertChild(w, newparent, item);

  ListTreeRefresh(w);

  return 1;
}

int
ListTreeReparentChildren(Widget w, ListTreeItem * item, ListTreeItem * newparent)
{
  ListTreeItem *first;

  TreeCheck(w, "in ListTreeReparentChildren");
  if (item->firstchild) {
    first = item->firstchild;
    item->firstchild = NULL;

    InsertChildren(w, newparent, first);

    ListTreeRefresh(w);
    return 1;
  }
  return 0;
}

int
ListTreeUserOrderSiblings(Widget w, ListTreeItem * item,
						  int (*func) (const void *, const void *))
{
   ListTreeWidget tree = (ListTreeWidget) w;
  ListTreeItem *first, *parent, **list;
  size_t i, count, size;

  TreeCheck(w, "in ListTreeUserOrderSiblings");
/* Get first child in list; */
  while (item->prevsibling)
    item = item->prevsibling;
  first = item;
  parent = first->parent;

/* Count the children */
  count = 1;
  while (item->nextsibling)
    item = item->nextsibling, count++;
  if (count <= 1)
    return 1;

  size = sizeof(ListTreeItem *);
  list = (ListTreeItem **) XtMalloc(size * count);
  list[0] = first;
  count = 1;
  while (first->nextsibling) {
    list[count] = first->nextsibling;
    count++;
    first = first->nextsibling;
  }

  qsort(list, count, size, func);

  list[0]->prevsibling = NULL;
  for (i = 0; i < count; i++) {
    if (i < count - 1)
      list[i]->nextsibling = list[i + 1];
    if (i > 0)
      list[i]->prevsibling = list[i - 1];
  }
  list[count - 1]->nextsibling = NULL;
  if (parent)
    parent->firstchild = list[0];
  else
    tree->list.first = list[0];
  XtFree((char *) list);

  ListTreeRefresh(w);
  TreeCheck(w, "exiting ListTreeOrderSiblings");

  return 1;
}

int
AlphabetizeItems(const void *item1, const void *item2)
{
  return strcmp((*((ListTreeItem **) item1))->text,
    (*((ListTreeItem **) item2))->text);
}

int
ListTreeOrderSiblings(Widget w, ListTreeItem * item)
{
  TreeCheck(w, "in ListTreeOrderSiblings");
  return ListTreeUserOrderSiblings(w, item, AlphabetizeItems);
}

int
ListTreeUserOrderChildren(Widget w, ListTreeItem * item,
						  int (*func) (const void *, const void *))
{
   ListTreeWidget tree = (ListTreeWidget) w;
  ListTreeItem *first;

  TreeCheck(w, "in ListTreeUserOrderChildren");
  if (item) {
    first = item->firstchild;
    if (first)
      ListTreeUserOrderSiblings(w, first, func);
  }
  else {
    if (tree->list.first)
      ListTreeUserOrderSiblings(w, tree->list.first, func);
  }
  TreeCheck(w, "exiting ListTreeUserOrderChildren");
  return 1;
}

int
ListTreeOrderChildren(Widget w, ListTreeItem * item)
{
   ListTreeWidget tree = (ListTreeWidget) w;
  ListTreeItem *first;

  TreeCheck(w, "in ListTreeOrderChildren");
  if (item) {
    first = item->firstchild;
    if (first)
      ListTreeOrderSiblings(w, first);
  }
  else {
    if (tree->list.first)
      ListTreeOrderSiblings(w, tree->list.first);
  }
  TreeCheck(w, "exiting ListTreeOrderChildren");
  return 1;
}

ListTreeItem *
ListTreeFindSiblingName(Widget w, ListTreeItem * item, char *name)
{
  /* ListTreeItem *first; */

  TreeCheck(w, "in ListTreeFindSiblingName");
/* Get first child in list; */
  if (item) {
    while (item->prevsibling)
      item = item->prevsibling;
    /* first = item; */

    while (item) {
      if (strcmp(item->text, name) == 0)
	return item;
      item = item->nextsibling;
    }
    return item;
  }
  return NULL;
}

ListTreeItem *
ListTreeFindChildName(Widget w, ListTreeItem * item, char *name)
{
   ListTreeWidget tree = (ListTreeWidget) w;
  TreeCheck(w, "in ListTreeFindChildName");
/* Get first child in list; */
  if (item && item->firstchild) {
    item = item->firstchild;
  }
  else if (!item && tree->list.first) {
    item = tree->list.first;
  }
  else
    item = NULL;

  while (item) {
    if (strcmp(item->text, name) == 0)
      return item;
    item = item->nextsibling;
  }
  return NULL;
}

void
ListTreeHighlightItem(Widget w, ListTreeItem * item)
{
  HighlightAll(w, False, False);
  HighlightItem(w, item, True, False);
  ListTreeRefresh(w);
}

void
ListTreeHighlightAll(Widget w)
{
  HighlightAllVisible(w, True, False);
  ListTreeRefresh(w);
}

void
ListTreeClearHighlighted(Widget w)
{
  HighlightAll(w, False, False);
  ListTreeRefresh(w);
}

void
ListTreeGetHighlighted(Widget w, ListTreeMultiReturnStruct * ret)
{
  if (ret)
    MakeMultiCallbackStruct(w, ret);
}

void
ListTreeSetHighlighted(Widget w, ListTreeItem ** items, int count, Boolean clear)
{
  if (clear)
    HighlightAll(w, False, False);
  if (count < 0) {
    while (*items) {
      HighlightItem(w, *items, True, False);
      items++;
    }
  }
  else {
    int i;

    for (i = 0; i < count; i++) {
      HighlightItem(w, items[i], True, False);
    }
  }
  ListTreeRefresh(w);
}

ListTreeItem *
ListTreeFirstItem(Widget w)
{
  ListTreeItem *first;

/* Get first child in widget */
  first = ((ListTreeWidget) w)->list.first;
  return first;
}

Position
ListTreeGetItemPosition(Widget w, ListTreeItem * item)
{
  return GetPosition(w, item);
}

void
ListTreeGetPathname(ListTreeReturnStruct * ret, char *dir)
{
  int count;

  if (*ret->path[0]->text != '/')
    strcpy(dir, "/");
  else
    strcpy(dir, "");
  strcat(dir, ret->path[0]->text);
  count = 1;
  while (count < ret->count) {
    strcat(dir, "/");
    strcat(dir, ret->path[count]->text);
    count++;
  }
}

void
ListTreeGetPathnameFromItem(ListTreeItem * item, char *dir)
{
  char tmppath[1024];

  *dir = '\0';
  while (item) {
    sprintf(tmppath, "/%s%s", item->text, dir);
    strcpy(dir, tmppath);
    item = item->parent;
  }
}

Widget
XmCreateScrolledListTree(Widget parent, char *name, Arg *args, Cardinal count)
{
  Widget sw;
  char *sname;
  int i;
  Arg *al;
  
  sname = XtMalloc(strlen(name)+3);
  strcpy(sname, name);
  strcat(sname, "SW");
  
  al = (Arg *)XtCalloc(count + 4, sizeof(Arg));
  for (i=0; i<count; i++) {
    al[i].name = args[i].name;
    al[i].value = args[i].value;
  }

  XtSetArg(al[i], XmNscrollingPolicy, XmAPPLICATION_DEFINED); i++;
  XtSetArg(al[i], XmNvisualPolicy, XmVARIABLE); i++;
  XtSetArg(al[i], XmNscrollBarDisplayPolicy, XmSTATIC); i++;
  XtSetArg(al[i], XmNshadowThickness, 0); i++;
  
  sw = XtCreateManagedWidget(sname, xmScrolledWindowWidgetClass, parent, al, i);
  XtFree((char *)al);
  
  return XtCreateWidget(name, listtreeWidgetClass, sw, args, count);
}
