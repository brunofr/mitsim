//-*-c++-*------------------------------------------------------------
// NAME: Exception handler.
// NOTE: When error occurs, instead of caling standard exit(int),
//       Exception::exit() should be called.
// AUTH: Qi Yang
// FILE: Exception.h
// DATE: Sat Mar 16 22:22:12 1996
//--------------------------------------------------------------------

#ifndef EXCEPTION_HEADER
#define EXCEPTION_HEADER

const int ERROR_WARNING	= 1;
const int ERROR_SERIOUS	= 2;
const int ERROR_FATAL	= 3;

class Exception
{
      char * name_;

   public:

      Exception(const char * name = 0);
      virtual ~Exception();

      virtual void exit(int code = 0);
      virtual void done(int code = 0);

      virtual void errorBailOut(const char *file, const int line,
				const char *msg = 0);
      virtual void errorMsg(const char *file, const int line,
			    const char *msg = 0);
};

extern Exception * theException;
extern void FreeRamException();

#define ErrorBailOut(s) theException->errorBailOut(__FILE__, __LINE__, s)
#define Error_Bail_Out() theException->errorBailOut(__FILE__, __LINE__)
#define ErrorMsg(s) theException->errorMsg(__FILE__, __LINE__, s)
#define Error_Msg() theException->errorMsg(__FILE__, __LINE__);

#endif
