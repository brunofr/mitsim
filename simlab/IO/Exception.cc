//-*-c++-*------------------------------------------------------------
// NAME: Error handle.
// AUTH: Qi Yang
// FILE: Exception.cc
// DATE: Sat Mar 16 22:34:11 1996
//--------------------------------------------------------------------

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <new>
#include <cctype>
#include <ctime>

#include <iostream>

#include <UTL/Misc.h>

#include "Exception.h"
using namespace std;

Exception * theException = new Exception;

// This function is registrated by  "set_new_handler(...)".

void
FreeRamException()
{
   cerr << "Error:: Free RAM Exception" << endl;
   theException->exit(1);
}


Exception::Exception(const char *name)
  :name_(Copy(name))
{
   theException = this;
}


Exception::~Exception()
{
   if (name_) {
	  delete [] name_;
	  name_ = NULL;
   }
}


// When error occurs, instead of caling standard exit(int),
// Exception::exit() should be called.

void
Exception::exit(int code)
{
   if (name_) {
	  time_t t = time(NULL);
	  char *s = ctime(&t);
	  cout << endl << name_
		   << " terminated (" << dec << code << ") on "
		   << s << endl;
   }
   ::exit(code);
}


void
Exception::done(int code)
{
   if (name_) {
	  time_t t = time(NULL);
	  char *s = ctime(&t);
	  cout << endl << name_
		   << " done (" << dec << code << ") on "
		   << s << endl;
   }
   ::exit(code);
}


// After a failed system call, it prints an error message that
// shows the name of the source file and line number where this
// function is involked by the macro ErrorBailOut(msg).

void
Exception::errorBailOut(const char *file, const int line, const char *msg)
{
   errorMsg(file, line, msg); 
   this->exit(1);
}

void
Exception::errorMsg(const char *file, const int line, const char *msg)
{
   cerr << "Error:: ";
   if (msg) cerr << msg << " ";
   cerr << "(" << file << ":" << line << ")." << endl;
}
