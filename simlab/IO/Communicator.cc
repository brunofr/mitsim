//-*-c++-*------------------------------------------------------------
// NAME: Base for communication between SIMLAB modules
// AUTH: Qi Yang
// FILE: Communicator.cc
// DATE: Sat Mar 16 11:19:58 1996
//--------------------------------------------------------------------

#include <iostream>

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>

#include "Communicator.h"
using namespace std;

Communicator * theBaseCommunicator = NULL;

double
Communicator::batchStepSize()
{
   return 60.0;
}

// Make connection to its parent process (Simlab).

Communicator::Communicator(int sndtag)
{
   theBaseCommunicator = this;

   int parent_id = IOService::parent();

   cout << ToolKit::RealPath(theExec) << " started";
   if (parent_id < 0) {
	  cout << " in stand alone mode." << endl;

      // Since the engine is running alone, there is no need to wait
      // other processes after each cycle.

      theSimulationClock->masterTime(86400.0);

      return;
   }
   cout << "." << endl;

   // The simulation is controlled by the master controller (MC). It
   // will wait until a message from MC updates the masterTime.

   theSimulationClock->masterTime(-86400);

   simlab_.init("SMC", sndtag, MSG_TAG_MC);

   simlab_.connect(parent_id);
}


// Tell master controller the time for calling this module again

void
Communicator::sendNextTimeToSimlab()
{
   if (!simlab_.isConnected()) return;

   SimulationClock *sc = theSimulationClock;
   
   if (sc->currentTime() < sc->masterTime()) return;

   double current_time = sc->currentTime();
   double usage = sc->recentCpuUsage();

   // Alternatively, we may use
   // double usage = lastStepCpuUsage();

   simlab_ << MSG_END_CYCLE
		   << current_time
		   << usage;
   simlab_.flush(SEND_IMMEDIATELY);
}

void Communicator::skipUnknownMsg(const char *sender, unsigned int id,
								  const char *msg)
{
  cerr << "Warning: " << theExec << " skipped unknown msg <"
	   << hex << id << dec << "> received from " << sender << ".";
  if (msg) cerr << " " << msg << ".";
  cerr << endl;
}
