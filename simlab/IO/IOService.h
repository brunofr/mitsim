//-*-c++-*------------------------------------------------------------
// NAME: Choosing the communication wrapper
// AUTH: Qi Yang
// FILE: IOService.h
// DATE: Fri Feb 23 19:00:06 1996
//--------------------------------------------------------------------

#ifndef IOSERVICE_HEADER
#define IOSERVICE_HEADER

#include "PVM_Service.h"
#define IOService PVM_Service

#endif
