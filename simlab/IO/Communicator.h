//-*-c++-*------------------------------------------------------------
// NAME: Base class for simulation communicators
// NOTE: 
// AUTH: Qi Yang
// FILE: Communicator.h
// DATE: Sat Mar 16 11:36:29 1996
//--------------------------------------------------------------------

#ifndef COMMUNICATOR_HEADER
#define COMMUNICATOR_HEADER

#include <IO/IOService.h>

class Communicator
{
protected:

  IOService simlab_;

public:

  Communicator(int sndtag);
  virtual ~Communicator() { }
      
  inline int isSimlabConnected() {
	return simlab_.isConnected();
  }
  inline int isRunningAlone() {
	return !simlab_.isConnected();
  }

  IOService& simlab() { return simlab_; }

  virtual void makeFriends() { }

  virtual int receiveMessages(double) { return 0; }

  virtual void sendNextTimeToSimlab();

  virtual double batchStepSize();

  static void skipUnknownMsg(const char *send, unsigned int id,
							 const char *msg = 0);
};

extern Communicator * theBaseCommunicator;

#endif
