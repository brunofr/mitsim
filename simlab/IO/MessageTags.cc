//-*-c++-*------------------------------------------------------------
// NAME: Communication
// AUTH: Qi Yang
// FILE: MessageTags.cc
// DATE: Sun Apr 21 16:09:25 1996
//--------------------------------------------------------------------

#include "MessageTags.h"

// If current process is waiting for message from other process, it
// uses this value in calling a timeout message receiving.

double DEFAULT_NO_MSG_WAITING_TIME = 0.1;

double NO_MSG_WAITING_TIME = DEFAULT_NO_MSG_WAITING_TIME;

