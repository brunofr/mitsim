#ifndef YY_TMS_McmasterPrmBisonParser_h_included
#define YY_TMS_McmasterPrmBisonParser_h_included

#line 1 "/usr/lib/bison.h"
/* before anything */
#ifdef c_plusplus
#ifndef __cplusplus
#define __cplusplus
#endif
#endif
#ifdef __cplusplus
#ifndef YY_USE_CLASS
#define YY_USE_CLASS
#endif
#else
#endif
#include <stdio.h>

/* #line 14 "/usr/lib/bison.h" */
#line 21 "TMS_McmasterPrmParser.h"
#define YY_TMS_McmasterPrmBisonParser_ERROR_BODY  =0
#define YY_TMS_McmasterPrmBisonParser_LEX_BODY  =0
#define YY_TMS_McmasterPrmBisonParser_MEMBERS    TMS_McmasterPrmScanner pts; TMS_McmasterPrmTable *ptr1; 
#define YY_TMS_McmasterPrmBisonParser_CONSTRUCTOR_PARAM  const char *f
#define YY_TMS_McmasterPrmBisonParser_CONSTRUCTOR_INIT  : pts(f), ptr1(NULL)

#line 22 "TMS_McmasterPrmParser.y"
typedef union {
      int    itype;
      float  ftype;
      char  *stype;
} yy_TMS_McmasterPrmBisonParser_stype;
#define YY_TMS_McmasterPrmBisonParser_STYPE yy_TMS_McmasterPrmBisonParser_stype
#line 36 "TMS_McmasterPrmParser.y"

#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <string>
using namespace std;

#include <IO/Exception.h>
#include <Tools/ToolKit.h>  

#include "TMS_McmasterParameters.h"
#include "TMS_McmasterPrmTable.h"

#undef yyFlexLexer
#define yyFlexLexer TMS_McmasterPrmFlexLexer
#include <FlexLexer.h>

   class TMS_McmasterPrmScanner : public TMS_McmasterPrmFlexLexer
   {
      private:

	 char *name_;			// file name

      public:

	 TMS_McmasterPrmScanner() : name_(NULL), TMS_McmasterPrmFlexLexer() { }
	 TMS_McmasterPrmScanner(const char *fname)
	    : name_(Copy(fname)),
	      TMS_McmasterPrmFlexLexer() {
		 if (ToolKit::fileExists(fname)) {
		    switch_streams(new ifstream(fname), 0);
		 } else {
		    cerr << "Error:: cannot open input file <"
				 << name_ << ">" << endl;
		    theException->exit();
		 }
	 }
	 ~TMS_McmasterPrmScanner() { delete [] name_; delete yyin; }

	 char* name() const { return name_; }

	 char* removeDelimeters(const char *deli="\"\"") {
	    char *s, *str = (char *)YYText();
	    if (*str == deli[0]) str ++;
	    s = str;
	    while (*s != '\0' && *s != deli[1]) s ++;
	    *s = '\0';
	    return (str);
	 }

	 char* value() { return (char*)YYText(); }

	 void errorQuit(int err) {
	    if (err == 0) return;
	    cerr << "Problem in parsing"
		 << " (" << name() << ":" << lineno() << ")" << endl;
	    if (err < 0) theException->exit();
	 }
   };

#line 14 "/usr/lib/bison.h"
 /* %{ and %header{ and %union, during decl */
#ifndef YY_TMS_McmasterPrmBisonParser_COMPATIBILITY
#ifndef YY_USE_CLASS
#define  YY_TMS_McmasterPrmBisonParser_COMPATIBILITY 1
#else
#define  YY_TMS_McmasterPrmBisonParser_COMPATIBILITY 0
#endif
#endif

#if YY_TMS_McmasterPrmBisonParser_COMPATIBILITY != 0
/* backward compatibility */
#ifdef YYLTYPE
#ifndef YY_TMS_McmasterPrmBisonParser_LTYPE
#define YY_TMS_McmasterPrmBisonParser_LTYPE YYLTYPE
/* WARNING obsolete !!! user defined YYLTYPE not reported into generated header */
/* use %define LTYPE */
#endif
#endif
#ifdef YYSTYPE
#ifndef YY_TMS_McmasterPrmBisonParser_STYPE 
#define YY_TMS_McmasterPrmBisonParser_STYPE YYSTYPE
/* WARNING obsolete !!! user defined YYSTYPE not reported into generated header */
/* use %define STYPE */
#endif
#endif
#ifdef YYDEBUG
#ifndef YY_TMS_McmasterPrmBisonParser_DEBUG
#define  YY_TMS_McmasterPrmBisonParser_DEBUG YYDEBUG
/* WARNING obsolete !!! user defined YYDEBUG not reported into generated header */
/* use %define DEBUG */
#endif
#endif
#ifdef YY_TMS_McmasterPrmBisonParser_STYPE
#ifndef yystype
#define yystype YY_TMS_McmasterPrmBisonParser_STYPE
#endif
#endif
/* use goto to be compatible */
#ifndef YY_TMS_McmasterPrmBisonParser_USE_GOTO
#define YY_TMS_McmasterPrmBisonParser_USE_GOTO 1
#endif
#endif

/* use no goto to be clean in C++ */
#ifndef YY_TMS_McmasterPrmBisonParser_USE_GOTO
#define YY_TMS_McmasterPrmBisonParser_USE_GOTO 0
#endif

#ifndef YY_TMS_McmasterPrmBisonParser_PURE

/* #line 63 "/usr/lib/bison.h" */
#line 148 "TMS_McmasterPrmParser.h"

#line 63 "/usr/lib/bison.h"
/* YY_TMS_McmasterPrmBisonParser_PURE */
#endif

/* #line 65 "/usr/lib/bison.h" */
#line 155 "TMS_McmasterPrmParser.h"

#line 65 "/usr/lib/bison.h"
/* prefix */
#ifndef YY_TMS_McmasterPrmBisonParser_DEBUG

/* #line 67 "/usr/lib/bison.h" */
#line 162 "TMS_McmasterPrmParser.h"

#line 67 "/usr/lib/bison.h"
/* YY_TMS_McmasterPrmBisonParser_DEBUG */
#endif
#ifndef YY_TMS_McmasterPrmBisonParser_LSP_NEEDED

/* #line 70 "/usr/lib/bison.h" */
#line 170 "TMS_McmasterPrmParser.h"

#line 70 "/usr/lib/bison.h"
 /* YY_TMS_McmasterPrmBisonParser_LSP_NEEDED*/
#endif
/* DEFAULT LTYPE*/
#ifdef YY_TMS_McmasterPrmBisonParser_LSP_NEEDED
#ifndef YY_TMS_McmasterPrmBisonParser_LTYPE
typedef
  struct yyltype
    {
      int timestamp;
      int first_line;
      int first_column;
      int last_line;
      int last_column;
      char *text;
   }
  yyltype;

#define YY_TMS_McmasterPrmBisonParser_LTYPE yyltype
#endif
#endif
/* DEFAULT STYPE*/
#ifndef YY_TMS_McmasterPrmBisonParser_STYPE
#define YY_TMS_McmasterPrmBisonParser_STYPE int
#endif
/* DEFAULT MISCELANEOUS */
#ifndef YY_TMS_McmasterPrmBisonParser_PARSE
#define YY_TMS_McmasterPrmBisonParser_PARSE yyparse
#endif
#ifndef YY_TMS_McmasterPrmBisonParser_LEX
#define YY_TMS_McmasterPrmBisonParser_LEX yylex
#endif
#ifndef YY_TMS_McmasterPrmBisonParser_LVAL
#define YY_TMS_McmasterPrmBisonParser_LVAL yylval
#endif
#ifndef YY_TMS_McmasterPrmBisonParser_LLOC
#define YY_TMS_McmasterPrmBisonParser_LLOC yylloc
#endif
#ifndef YY_TMS_McmasterPrmBisonParser_CHAR
#define YY_TMS_McmasterPrmBisonParser_CHAR yychar
#endif
#ifndef YY_TMS_McmasterPrmBisonParser_NERRS
#define YY_TMS_McmasterPrmBisonParser_NERRS yynerrs
#endif
#ifndef YY_TMS_McmasterPrmBisonParser_DEBUG_FLAG
#define YY_TMS_McmasterPrmBisonParser_DEBUG_FLAG yydebug
#endif
#ifndef YY_TMS_McmasterPrmBisonParser_ERROR
#define YY_TMS_McmasterPrmBisonParser_ERROR yyerror
#endif

#ifndef YY_TMS_McmasterPrmBisonParser_PARSE_PARAM
#ifndef __STDC__
#ifndef __cplusplus
#ifndef YY_USE_CLASS
#define YY_TMS_McmasterPrmBisonParser_PARSE_PARAM
#ifndef YY_TMS_McmasterPrmBisonParser_PARSE_PARAM_DEF
#define YY_TMS_McmasterPrmBisonParser_PARSE_PARAM_DEF
#endif
#endif
#endif
#endif
#ifndef YY_TMS_McmasterPrmBisonParser_PARSE_PARAM
#define YY_TMS_McmasterPrmBisonParser_PARSE_PARAM void
#endif
#endif

/* TOKEN C */
#ifndef YY_USE_CLASS

#ifndef YY_TMS_McmasterPrmBisonParser_PURE
extern YY_TMS_McmasterPrmBisonParser_STYPE YY_TMS_McmasterPrmBisonParser_LVAL;
#endif


/* #line 143 "/usr/lib/bison.h" */
#line 248 "TMS_McmasterPrmParser.h"
#define	TMS_McmasterPrm_COL	258
#define	TMS_McmasterPrm_OB	259
#define	TMS_McmasterPrm_CB	260
#define	TMS_McmasterPrm_INT	261
#define	TMS_McmasterPrm_REAL	262
#define	TMS_McmasterPrm_STRING	263
#define	TMS_McmasterPrm_NAME	264


#line 143 "/usr/lib/bison.h"
 /* #defines token */
/* after #define tokens, before const tokens S5*/
#else
#ifndef YY_TMS_McmasterPrmBisonParser_CLASS
#define YY_TMS_McmasterPrmBisonParser_CLASS TMS_McmasterPrmBisonParser
#endif

#ifndef YY_TMS_McmasterPrmBisonParser_INHERIT
#define YY_TMS_McmasterPrmBisonParser_INHERIT
#endif
#ifndef YY_TMS_McmasterPrmBisonParser_MEMBERS
#define YY_TMS_McmasterPrmBisonParser_MEMBERS 
#endif
#ifndef YY_TMS_McmasterPrmBisonParser_LEX_BODY
#define YY_TMS_McmasterPrmBisonParser_LEX_BODY  
#endif
#ifndef YY_TMS_McmasterPrmBisonParser_ERROR_BODY
#define YY_TMS_McmasterPrmBisonParser_ERROR_BODY  
#endif
#ifndef YY_TMS_McmasterPrmBisonParser_CONSTRUCTOR_PARAM
#define YY_TMS_McmasterPrmBisonParser_CONSTRUCTOR_PARAM
#endif
/* choose between enum and const */
#ifndef YY_TMS_McmasterPrmBisonParser_USE_CONST_TOKEN
#define YY_TMS_McmasterPrmBisonParser_USE_CONST_TOKEN 0
/* yes enum is more compatible with flex,  */
/* so by default we use it */ 
#endif
#if YY_TMS_McmasterPrmBisonParser_USE_CONST_TOKEN != 0
#ifndef YY_TMS_McmasterPrmBisonParser_ENUM_TOKEN
#define YY_TMS_McmasterPrmBisonParser_ENUM_TOKEN yy_TMS_McmasterPrmBisonParser_enum_token
#endif
#endif

class YY_TMS_McmasterPrmBisonParser_CLASS YY_TMS_McmasterPrmBisonParser_INHERIT
{
public: 
#if YY_TMS_McmasterPrmBisonParser_USE_CONST_TOKEN != 0
/* static const int token ... */

/* #line 182 "/usr/lib/bison.h" */
#line 300 "TMS_McmasterPrmParser.h"
static const int TMS_McmasterPrm_COL;
static const int TMS_McmasterPrm_OB;
static const int TMS_McmasterPrm_CB;
static const int TMS_McmasterPrm_INT;
static const int TMS_McmasterPrm_REAL;
static const int TMS_McmasterPrm_STRING;
static const int TMS_McmasterPrm_NAME;


#line 182 "/usr/lib/bison.h"
 /* decl const */
#else
enum YY_TMS_McmasterPrmBisonParser_ENUM_TOKEN { YY_TMS_McmasterPrmBisonParser_NULL_TOKEN=0

/* #line 185 "/usr/lib/bison.h" */
#line 316 "TMS_McmasterPrmParser.h"
	,TMS_McmasterPrm_COL=258
	,TMS_McmasterPrm_OB=259
	,TMS_McmasterPrm_CB=260
	,TMS_McmasterPrm_INT=261
	,TMS_McmasterPrm_REAL=262
	,TMS_McmasterPrm_STRING=263
	,TMS_McmasterPrm_NAME=264


#line 185 "/usr/lib/bison.h"
 /* enum token */
     }; /* end of enum declaration */
#endif
public:
 int YY_TMS_McmasterPrmBisonParser_PARSE(YY_TMS_McmasterPrmBisonParser_PARSE_PARAM);
 virtual void YY_TMS_McmasterPrmBisonParser_ERROR(char *msg) YY_TMS_McmasterPrmBisonParser_ERROR_BODY;
#ifdef YY_TMS_McmasterPrmBisonParser_PURE
#ifdef YY_TMS_McmasterPrmBisonParser_LSP_NEEDED
 virtual int  YY_TMS_McmasterPrmBisonParser_LEX(YY_TMS_McmasterPrmBisonParser_STYPE *YY_TMS_McmasterPrmBisonParser_LVAL,YY_TMS_McmasterPrmBisonParser_LTYPE *YY_TMS_McmasterPrmBisonParser_LLOC) YY_TMS_McmasterPrmBisonParser_LEX_BODY;
#else
 virtual int  YY_TMS_McmasterPrmBisonParser_LEX(YY_TMS_McmasterPrmBisonParser_STYPE *YY_TMS_McmasterPrmBisonParser_LVAL) YY_TMS_McmasterPrmBisonParser_LEX_BODY;
#endif
#else
 virtual int YY_TMS_McmasterPrmBisonParser_LEX() YY_TMS_McmasterPrmBisonParser_LEX_BODY;
 YY_TMS_McmasterPrmBisonParser_STYPE YY_TMS_McmasterPrmBisonParser_LVAL;
#ifdef YY_TMS_McmasterPrmBisonParser_LSP_NEEDED
 YY_TMS_McmasterPrmBisonParser_LTYPE YY_TMS_McmasterPrmBisonParser_LLOC;
#endif
 int YY_TMS_McmasterPrmBisonParser_NERRS;
 int YY_TMS_McmasterPrmBisonParser_CHAR;
#endif
#if YY_TMS_McmasterPrmBisonParser_DEBUG != 0
public:
 int YY_TMS_McmasterPrmBisonParser_DEBUG_FLAG;	/*  nonzero means print parse trace	*/
#endif
public:
 YY_TMS_McmasterPrmBisonParser_CLASS(YY_TMS_McmasterPrmBisonParser_CONSTRUCTOR_PARAM);
public:
 YY_TMS_McmasterPrmBisonParser_MEMBERS 
};
/* other declare folow */
#endif


#if YY_TMS_McmasterPrmBisonParser_COMPATIBILITY != 0
/* backward compatibility */
#ifndef YYSTYPE
#define YYSTYPE YY_TMS_McmasterPrmBisonParser_STYPE
#endif

#ifndef YYLTYPE
#define YYLTYPE YY_TMS_McmasterPrmBisonParser_LTYPE
#endif
#ifndef YYDEBUG
#ifdef YY_TMS_McmasterPrmBisonParser_DEBUG 
#define YYDEBUG YY_TMS_McmasterPrmBisonParser_DEBUG
#endif
#endif

#endif
/* END */

/* #line 236 "/usr/lib/bison.h" */
#line 380 "TMS_McmasterPrmParser.h"

#line 276 "TMS_McmasterPrmParser.y"


   class TMS_McmasterPrmParser : public TMS_McmasterPrmBisonParser
   {
      public:
	 TMS_McmasterPrmParser(const char *fname)
	    : TMS_McmasterPrmBisonParser(fname) { }
	 ~TMS_McmasterPrmParser() { }

	 char* name() const { return pts.name(); }

	 void yyerror(char *msg) {
	    cerr << "\nError:: " << msg
			 << " (" << name() << ":"
			 << pts.lineno() << ")" << endl;
	    theException->exit();
	 }

	 int yylex() { return pts.yylex(); }

	 void parse(TMS_McmasterPrmTable *pt){
	   cout << "Parsing Sensor Parameter file<";
	   cout << name() << "> ..." << endl;;
	   ptr1 = pt;
	   yyparse();
	 }
   };
#endif
