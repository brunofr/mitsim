//-*-c++-*------------------------------------------------------------
// TMS_OutputDialog.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <assert.h>

#include <Xmt/Xmt.h>
#include <Xmt/Chooser.h>
#include <Xmt/InputField.h>
#include <Xm/ToggleB.h>

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>

#include "TMS_OutputDialog.h"
#include "TMS_Interface.h"
#include "TMS_Engine.h"
#include "TMS_FileManager.h"
#include "TMS_Interface.h"
#include "TMS_Menu.h"

TMS_OutputDialog::TMS_OutputDialog ( Widget parent )
   : XmwDialogManager(parent, "outputDialog", NULL, 0)
{
   XtVaSetValues(widget_,
				 XmNdialogStyle, XmDIALOG_PRIMARY_APPLICATION_MODAL,
				 NULL);

   outDirFld_ = XmtNameToWidget(widget_, "outDir");
   assert(outDirFld_);
   isHeadingOnFld_ = XmtNameToWidget(widget_, "header");
   assert(isHeadingOnFld_);
   formatFld_ = XmtNameToWidget(widget_, "format");
   assert(formatFld_);

}

TMS_OutputDialog::~TMS_OutputDialog()
{
}


// These overload the functions in base class

void TMS_OutputDialog::cancel(Widget, XtPointer, XtPointer)
{
   unmanage();
}


void TMS_OutputDialog::okay(Widget, XtPointer, XtPointer)
{
   // Copy the data from dialog 

   ToolKit::outdir(XmtInputFieldGetString(outDirFld_));

   if (XmToggleButtonGetState(isHeadingOnFld_)) {
	  theEngine->chooseOutput(OUTPUT_SKIP_COMMENT);
   } else {
	  theEngine->removeOutput(OUTPUT_SKIP_COMMENT);
   }

   if (XmtChooserGetState(formatFld_)) {
	  theEngine->chooseOutput(OUTPUT_RECT_TEXT);
   } else {
	  theEngine->removeOutput(OUTPUT_RECT_TEXT);
   }

   // Pop down the dialog

   unmanage();
   theMenu->needSave();
}


void TMS_OutputDialog::help(Widget, XtPointer, XtPointer)
{
   theDrnInterface->openUrl("chooseOutput", "tms.html");
}


void TMS_OutputDialog::post()
{
   // Copy data to dialog

   XmtInputFieldSetString(outDirFld_, ToolKit::outdir());

   if (theEngine->skipComment()) {
	  XmToggleButtonSetState(isHeadingOnFld_, False, False);
   } else {
	  XmToggleButtonSetState(isHeadingOnFld_, True, False);
   }

   if (theEngine->isRectangleFormat()) {
	  XmtChooserSetState(formatFld_, 1, False);
   } else {
	  XmtChooserSetState(formatFld_, 0, False);
   }

   // Do not allow changes after simulation is started

   if (theSimulationClock->isStarted()) {
	  deactivate();
   } else {
	  activate();
   }
   
   XmwDialogManager::post();
}

void TMS_OutputDialog::activate()
{
   if (XtIsSensitive(okay_)) return;
   XmwDialogManager::activate(outDirFld_);
   XmwDialogManager::activate(isHeadingOnFld_);
   XmwDialogManager::activate(formatFld_);
   XmwDialogManager::activate(okay_);
}

void TMS_OutputDialog::deactivate()
{
   if (!XtIsSensitive(okay_)) return;
   XmwDialogManager::deactivate(outDirFld_);
   XmwDialogManager::deactivate(isHeadingOnFld_);
   XmwDialogManager::deactivate(formatFld_);
   XmwDialogManager::deactivate(okay_);
}

#endif // INTERNAL_GUI
