//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_Parameter.C
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#ifndef TMS_PARAMETER_HEADER
#define TMS_PARAMETER_HEADER

#include <GRN/Parameter.h>
#include <Tools/Scaler.h>

class TMS_Parameter : public Parameter
{
   public:

      TMS_Parameter();
      ~TMS_Parameter() { }
    
      void load();

      inline double vehicleLength() { return vehicleLength_; }
	  inline float inciCapStepSize() { return inciCapStepSize_; }
	  inline int inciCapTimeStepSize() { return inciCapTimeStepSize_; }

   private:

      int parseVariable(GenericVariable &);
	  int loadInciCapParams(GenericVariable &);

   protected:

      double vehicleLength_;	// meters, convert occ to density

	  // When incident is cleaned, the capacity is restored in
	  // steps. Each step has a prespecified length of time.

	  float inciCapStepSize_;		// step size to increase capacity
	  int inciCapTimeStepSize_;		// number of seconds per step
};

extern TMS_Parameter * theParameter;

#endif
