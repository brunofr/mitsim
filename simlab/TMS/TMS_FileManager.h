//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_FileManager.h
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#ifndef TMS_FILEMANAGER_HEADER
#define TMS_FILEMANAGER_HEADER

//#include <fstream>
#include <Tools/GenericSwitcher.h>

class GenericVariable;

class TMS_FileManager : public GenericSwitcher
{
public:

  TMS_FileManager();
  ~TMS_FileManager() { }

  void set(char **ptr, const char *s);

  char* title() { return title_; }
  void title(const char *s) { set(&title_, s); }

  void readMasterFile();

  // This overload the function in base class and will be classed
  // by the parser

  int parseVariable(GenericVariable &gv);

  void openOutputFiles();
  void closeOutputFiles();

private:

  char* title_;

  int parseVariable(char ** varptr, GenericVariable &gv);

  int isEqual(const char *s1, const char *s2);
};

extern TMS_FileManager * theFileManager;

#endif // THE END OF THIS FILE
