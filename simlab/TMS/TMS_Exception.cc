//-*-c++-*------------------------------------------------------------
// NAME: Exception handler
// AUTH: Qi Yang
// FILE: TMS_Exception.C
// DATE: Sun Mar 17 14:36:22 1996
//--------------------------------------------------------------------


#include "TMS_Exception.h"
#include "TMS_Communicator.h"

void
TMS_Exception::exit(int code)
{
   if (theCommunicator->isSimlabConnected()) {
      theCommunicator->simlab() << MSG_QUIT;
      theCommunicator->simlab().flush(SEND_IMMEDIATELY);
   }
   IOService::exit();
   Exception::exit(code);
}

void
TMS_Exception::done(int code)
{
   if (theCommunicator->isSimlabConnected()) {
      theCommunicator->simlab() << MSG_DONE;
      theCommunicator->simlab().flush(SEND_IMMEDIATELY);
   }
   IOService::exit();
   Exception::done(code);
}
