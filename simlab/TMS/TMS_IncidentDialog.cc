//-*-c++-*------------------------------------------------------------
// TMS_IncidentDialog.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <assert.h>

#include <Xmt/Xmt.h>
#include <Xmt/Chooser.h>
#include <Xm/ToggleB.h>

#include <DRN/DRN_DrawingArea.h>

#include <Tools/SimulationClock.h>

#include "TMS_IncidentDialog.h"
#include "TMS_Interface.h"
#include "TMS_Symbols.h"
#include "TMS_Menu.h"
#include "TMS_Engine.h"

TMS_IncidentDialog::TMS_IncidentDialog ( Widget parent )
  : XmwDialogManager(parent, "incidentDialog", NULL, 0)
{
  algoFld_ = XtNameToWidget(widget_, "*algoType");
  assert(algoFld_);

  typesFld_ = theSymbols()->associate(widget_, "incidentTypes");
  assert(typesFld_);
  addCallback(typesFld_, XmtNvalueChangedCallback,
			  &TMS_IncidentDialog::clickTypesCB, this);
}

// These overload the functions in base class

void TMS_IncidentDialog::cancel(Widget, XtPointer, XtPointer)
{
  // Restore the original code before close the box
   
  theSymbols()->incidentTypes().set(types_);

  theDrawingArea->setState(DRN_DrawingArea::REDRAW);
  unmanage();
}


void TMS_IncidentDialog::okay(Widget, XtPointer, XtPointer)
{
  
  theDrawingArea->setState(DRN_DrawingArea::REDRAW);
  theMenu->needSave();
  unmanage();

  if (theSimulationClock->isStarted()) return;

  int algo = XmtChooserGetState(algoFld_);
  theEngine->unsetControlLogic(CONTROL_INC_DETECTION);
  switch (algo) {
  case TMS_Symbols::MC_MASTER:
	{
	  theEngine->setControlLogic(CONTROL_INC_DETECTION_MCMASTER);
	  break;
	}
  case TMS_Symbols::APID:
	{
	  theEngine->setControlLogic(CONTROL_INC_DETECTION_APID);
	  break;
	}
  case TMS_Symbols::NONE:
  default:
	{
	  break;
	}
  }
}


void TMS_IncidentDialog::help(Widget, XtPointer, XtPointer)
{
  theInterface->openUrl("incidentDetection", "drn.html");
}


void TMS_IncidentDialog::post()
{
  // Record the value when the box is posted.

  int algo;
  types_ = theSymbols()->incidentTypes().value();

  switch (theEngine->controlLogic() & CONTROL_INC_DETECTION) {
  case CONTROL_INC_DETECTION_MCMASTER:
	algo = TMS_Symbols::MC_MASTER;
	break;
  case CONTROL_INC_DETECTION_APID:
	algo = TMS_Symbols::APID;
	break;
  default:
	algo = 0;
	break;
  }

  // BUG COMMENTS BEGIN: The state of choosers do not replect the
  // latest value of the linked symbols when after the cancel button
  // is pressed and the dialog box is posed again.  This should be a
  // bug somewhere, but I could not figure out where.  Following is a
  // work around which force to update the chooser state based on
  // latest symbol value.  This is not necessary if the Xmt symbols
  // work as supported.

  XmtChooserSetState(algoFld_, algo, False);
  XmtChooserSetState(typesFld_, types_, False);

  // BUG COMMENTS END

  if (theSimulationClock->isStarted()) {
	XmwDialogManager::deactivate(algoFld_);
  } else {
	XmwDialogManager::activate(algoFld_);
  }

  XmwDialogManager::post();
}

void TMS_IncidentDialog::clickTypesCB(Widget, XtPointer, XtPointer)
{
  theDrawingArea->redraw();
}

#endif // INTERNAL_GUI
