//-*-c++-*------------------------------------------------------------
// TMS_McmasterPrmTable.cc
//
// Sreeram Radhakrishnan
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <cstdio>

#include <Tools/ToolKit.h>
#include <IO/Exception.h>

#include "TMS_McmasterParameters.h"
#include "TMS_McmasterPrmParser.h"
#include "TMS_McmasterPrmTable.h"
#include "TMS_Network.h"
#include "TMS_Link.h"
#include "TMS_Segment.h"
using namespace std;

char *TMS_McmasterPrmTable::name_;

TMS_McmasterPrmTable *theMcmasterPrmTable = NULL;

TMS_McmasterPrmTable* TMS_McmasterPrmTable::parseMcmasterParameters()
{
   const char *filename = TMS_McmasterPrmTable::name();

   if (!ToolKit::isValidInputFilename(filename)) {
      cerr << "Invalid file name for McMaster incident detection." << endl;
      return NULL;
   }

   filename = ToolKit::optionalInputFile(filename); 

   TMS_McmasterPrmTable *p = new TMS_McmasterPrmTable;

   TMS_McmasterPrmParser theMcmasterPrmParser(filename);
   theMcmasterPrmParser.parse(p);

   return p;
};


void TMS_McmasterPrmTable::assigntotalPrmGrps(int n)
{
   totalPrmGrps_ = n;
};


void TMS_McmasterPrmTable::assigndegreeOfInterpolation(int n)
{
   degreeOfInterpolation_ = n; 
};


void TMS_McmasterPrmTable::assignsignificanceLevel(double f)
{
   significanceLevel_ = f;
};

void TMS_McmasterPrmTable::assignincPersistency(int n)
{
   incPersistency_ = n;
};

void TMS_McmasterPrmTable::assignnormalPersistency(int n)
{
   normalPersistency_ = n;
};

void TMS_McmasterPrmTable::print(ostream &os)
{
   int i, imax, jmax;

   if(this) {
   imax = prmGrps().size();
   jmax = degreeOfInterpolation_ + 1;

   os << "/*" << endl
	  << " * " << name()
	  << " Parameters for incident detection using Mcmaster algorithm" << endl
	  << " */" << endl << endl;

	os << totalPrmGrps_ << endc;
    os << degreeOfInterpolation_ << endc;
    os << significanceLevel_ << endl;

	os << OPEN_TOKEN << endl;

   for (i = 0; i < imax; i++)
   {
	  prmGrps(i)->print(os,jmax);
   }
   os << CLOSE_TOKEN << endl;
   }
};

void TMS_McmasterPrmTable::assignPrmGrpstoStations()
{
  TMS_Network *thenetwork = tmsNetwork();

  for(register int i=0; i < totalPrmGrps_;i++) {
	TMS_McmasterParameters *ptr = getprmgrpptr(i);
	vector<int> *sensorlistptr = ptr->sensorListptr();

	for(register int j=0; j < (*sensorlistptr).size();j++) {

	  int index = (*sensorlistptr)[j];
	  int prmgrp = ptr->prmGrpID();
	  RN_Sensor *rns = thenetwork->findSensor(index);
	  RN_SurvStation *surv = rns->station();
	  surv->setprmGrpID(prmgrp);
	}
  }
}
