//-*-c++-*------------------------------------------------------------
// TMS_Sensor.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef TMS_SENSOR_HEADER
#define TMS_SENSOR_HEADER

//#include <iostream>
#include <vector>

#include <Templates/STL_Configure.h>
#include <TC/TC_Sensor.h>

class TMS_Segment;
class TMS_Lane;
class Reader;
class GenericVariable;

enum {
  INC_DETECTION_STATUS_NONE = 0,
  INC_DETECTION_STATUS_DN   = 1,
  INC_DETECTION_STATUS_UP   = 2,
  INC_DETECTION_RECURRENT   = 3
};

// --------------------------------------------------------------------
// TMS_TrafficSensorPoint -- Point data traffic sensor.  Traffic data
// can be either read from data file or received from MITMSIM
// --------------------------------------------------------------------

class TMS_Sensor : public TC_Sensor
{
  friend class TMS_FileManager;
  friend class TMS_Communicator;
  friend class TMS_Engine;

#ifdef INTERNAL_GUI
  friend class TMS_SensorDataDialog;
#endif

public:

  TMS_Sensor();
  ~TMS_Sensor() { }

  static int loadDatabaseSetting(GenericVariable&);
  static void loadDatabases();

  int count();				// virtual
  int flow();				// virtual
  float speed();			// virtual

  // function to set the parameter group id

  int getRegion(double);
  int sensorIncState() { return (state_ & SENSOR_INC_FLAG) ; }
  int incClrPersistCount() { return IncClrPersistCount(state_); }
  int incDcrPersistCount() { return IncDcrPersistCount(state_); }

  // needed for incident detection
  float occPrev(int i);

  // needed for incident detection
  void setOccPrev(int i, float f);

  // needed for incident detection
  float spdPrev(int i);

  // needed for incident detection
  void setSpdPrev(int i, float f);

private:

  static void skipRecord(Reader&);
  void loadDbFlow(Reader&);
  void loadDbSpeed(Reader&);

  // Call these only if the data base are read

  int dbFlow(int i) { return (*dbFlow_)[i]; }
  float dbSpeed(int i) { return (*dbSpeed_)[i]; }
  int dbPeriod(double t);

  static void dbFlowFile(const char *s);
  static void dbSpeedFile(const char *s);
  static void calcDbEndTime();

protected:

  static long startTime_;	// begin time for the received data
  static long endTime_;		// time received the data
      
  static double dbStartTime_;
  static double dbStepSize_;
  static double dbEndTime_;
  static int dbNumPeriods_;

  static char *dbFlowFile_;
  static float dbFlowScaler_;
  std::vector<int> *dbFlow_;

  static char *dbSpeedFile_;
  static float dbSpeedScaler_;
  std::vector<float> *dbSpeed_;

  float occPrev_[2];			// parameter used in incident detection
  float spdPrev_[2];			// parameter used in incident detection
};

#endif
