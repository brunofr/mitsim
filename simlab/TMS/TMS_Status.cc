//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_Status.C
// DATE: Fri Nov 17 10:12:59 1995
//--------------------------------------------------------------------


#include <cstdio>
#include <iomanip>
#include <cstring>
#include <cctype>
using namespace std;

#include <sys/types.h>
#include <unistd.h>

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>

#ifdef INTERNAL_GUI
#include "TMS_Modeline.h"
#include "TMS_Interface.h"
#endif

#include "TMS_Status.h"
#include "TMS_Engine.h"

TMS_Status *theStatus = new TMS_Status;

TMS_Status::TMS_Status()
   : nErrors_(0),
	 nMsgs_(0),
	 logFile_(NULL)
{
}

TMS_Status::~TMS_Status()
{
   clean();
}

void TMS_Status::clean()
{
   if (logFile_) {
	  delete [] logFile_;
	  logFile_ = NULL;
   }
}

void
TMS_Status::openLogFile()
{
   logFile_ = Copy(ToolKit::outfile("tms.out"));
   osLogFile_.open(logFile_);
}


void
TMS_Status::closeLogFile()
{
   osLogFile_.close();
   if (nErrors_) {
      cout << endl << nErrors_ << " error(s) detected. "
	   << "See <" << logFile_ << "> for details." << endl;
   } else if (!nMsgs_) {
      ::remove(logFile_);
   }
}


void TMS_Status::report(ostream &os)
{
   os << endl << "Scenario Summary:" << endl
      << indent << "Control Logic = "
      << theEngine->controlLogic() << endl
      << indent << "Information = "
      << theEngine->information() << endl
      << indent << "Length of Rolling Horizon = "
      << theEngine->rollingLength() << endl;

   os << indent << "Rolling Step Size = ";
   if (theEngine->rollingStepSize() < 86400) {
     os << theEngine->rollingStepSize() << endl;
   } else {
     os << "INF" << endl;
   }

   os << indent << "Path Step Size = ";
   if (theEngine->pathStepSize() < 86400) {
     os << theEngine->pathStepSize() << endl;
   } else {
     os << "INF" << endl;
   }

   os << indent << "Computational Delay = "
      << theEngine->routingTableDelay() << endl;

   os << endl << "Running Time Summary:" << endl
      << indent << "Simulated = "
      << theSimulationClock->timeSimulated() << endl
      << indent << "Real Time = "
      << theSimulationClock->timeSinceStart() << endl
      << indent << "CPU Time  = "
      << theSimulationClock->cpuTimeSinceStart() << endl;

   os << endl;
}

void
TMS_Status::print(ostream &os)
{
   static int header = 1;
   if (ToolKit::verbose()) {
	 if (header) {
	   os << endl << indent;
	   os.fill(' ');
	   os << setw(10) << "Time" << endl;
	   header = 0;
	 }
	 os << indent << setw(10)
		<< theSimulationClock->currentStringTime() << endl;
   }
}


#ifdef INTERNAL_GUI

void TMS_Status::showStatus()
{
}

#endif
