//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_Engine.h
// DATE: Tue Nov 21 21:55:21 1995
//--------------------------------------------------------------------

#ifndef TMS_ENGINE_HEADER
#define TMS_ENGINE_HEADER

#include <iostream>
#include <cstdio>
#include <Tools/SimulationEngine.h>

const int CONTROL_INCIDENT_RESPONSE	    = 0x0001;
const int CONTROL_GATING_LOGIC		    = 0x0002;
const int CONTROL_INC_DETECTION         = 0x00F0;
const int CONTROL_INC_DETECTION_MCMASTER= 0x0010;
const int CONTROL_INC_DETECTION_APID    = 0x0020;

// If these constants need to be changes, please check
// MESO/MESO_Engine.h also. They should be consistent.

enum {
  INFORMATION_HISTORICAL  = 0,
  INFORMATION_MEASUREMENT = 1,
  INFORMATION_PREDICTION  = 2
};


class TMS_Engine : public SimulationEngine
{
  friend class TMS_Communicator;
  friend class TMS_FileManager;
  friend class TMS_CmdArgsParser;

#ifdef INTERNAL_GUI
  friend class TMS_SetupDialog;
  friend class TMS_SensorDataDialog;
#endif // !INTERNAL_GUI

protected:

  int controlLogic_;
  int information_;

  int source_;				// sensor data source

  double batchStepSize_;	// update console and queue info
				 
  double batchTime_;		// time to update console

  int iteration_;			// current run
  int nTotalIterations_;	// total number of runs

  // The delay between sensor data becomes available and routing
  // table becomes available

  double routingTableDelay_;
  double routingTableDueTime_;
  double routingTableTime_;	// time to update MITSIM routing table

  int pathStepSize_;	// min of MesoTS and MITSIM 

  int roll_;				// prediction counter
  int rollingStepSize_;	// a multipler of pathStepSize
  int rollingLength_;	// a multipler of rollingStepSize_

  double rollingTime_;	// time to call MesoTS
  double beginTime_;	// start time for this run
  double endTime_;		// end time for this run

  // Modify the experimental guidance based on most recent
  // prediction

  void modifyGuidance(const char *tag);

  // Save network state

  void saveState3d(const char *tag);

private:
	  
  FILE *incLogFpo_;			// save incident, will be read by MesoTS

public:
      
  TMS_Engine();
  ~TMS_Engine() { }

  const char *ext();		//  virtual

  void init();

  void save(std::ostream &os = std::cout);

  FILE *incLogFpo() { return incLogFpo_; }

  // This function starts the simulation loops. It will NOT return
  // until simulation is done (or cancelled in graphical mode).
  // This function calls simulationLoop() iteratively or as a
  // recusive timeout callback.

  void run();
  void quit(int s);			// virtual

  int controlLogic() { return controlLogic_; }
  int information() { return information_; }
      
  void controlLogic(int x) { controlLogic_ = x; }
  void setControlLogic(int flag) {
	controlLogic_ |= flag;
  }
  void unsetControlLogic(int flag) {
	controlLogic_ &= ~flag;
  }
  void information(int x) { information_ = x; }
      
  void toggleControlLogic(int flag);

  inline double batchStepSize() { return batchStepSize_; }

  inline double batchTime() { return batchTime_; }

  inline double pathStepSize() { return pathStepSize_; }

  inline double rollingStepSize() { return rollingStepSize_; }
  inline double  rollingLength() {
	return rollingLength_;
  }

  inline double routingTableDueTime() {
	return routingTableDueTime_;
  }
  inline double routingTableDelay() {
	return routingTableDelay_;
  }
  inline double routingTableTime() {
	return routingTableTime_;
  }

  int loadSimulationFiles();
  int loadMasterFile();
  int simulationLoop();

  int isWaiting();			// virtual

  int receiveMessages(double sec = 0.0); // pure virtual

  void routingTableDelay(double d) { routingTableDelay_ = d; }
};

extern TMS_Engine * theEngine;

#endif
