//-*-c++-*------------------------------------------------------------
// NAME: Class for communication to MITSIM and MesoTS.
// AUTH: Qi Yang
// FILE: TMS_Communicator.h
// DATE: Sun Nov 19 12:33:35 1995
//--------------------------------------------------------------------

#ifndef TMS_COMMUNICATOR_HEADER
#define TMS_COMMUNICATOR_HEADER

#include <IO/Communicator.h>

class TMS_Communicator : public Communicator
{
   protected:

      IOService mitsim_;
      IOService meso_;
      IOService mdi_;
      IOService tmca_;

   public:

      TMS_Communicator(int sndtag);
      ~TMS_Communicator() { }
      
      inline int isMitsimConnected() {
		 return mitsim_.isConnected();
      }

      inline int isMesoConnected() {
		 return meso_.isConnected();
      }
      
      inline int isMdiConnected() {
		 return mdi_.isConnected();
      }
  
      inline int isTmcaConnected() {
	         return tmca_.isConnected();
      }
      
      IOService& mitsim() { return mitsim_; }
      IOService& meso() { return meso_; }
      IOService& mdi() { return mdi_; }
      IOService& tmca() { return tmca_; }

      void makeFriends();		// virtual

      int receiveMessages(double sec = 0.0);	// virtual

      int receiveSimlabMessages(double sec = 0.0);
      int receiveMitsimMessages(double sec = 0.0);
      int receiveMesoMessages(double sec = 0.0);
      int receiveMdiMessages(double sec = 0.0);
      int receiveTmcaMessages(double sec = 0.0 );

      double batchStepSize();	// virtual

      void startStateEstimation();
};

extern TMS_Communicator* theCommunicator;

#endif
