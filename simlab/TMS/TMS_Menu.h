//-*-c++-*------------------------------------------------------------
// TMS_Menu.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef TMS_MENU_HEADER
#define TMS_MENU_HEADER

#include <DRN/DRN_Menu.h>

class TMS_Menu : public DRN_Menu
{
  friend class TMS_Interface;

  CallbackDeclare(TMS_Menu);

public:

  TMS_Menu(Widget parent);
  ~TMS_Menu() { }

protected:

  // New callbacks
	  
  void randomizer ( Widget, XtPointer, XtPointer );
  void setup ( Widget, XtPointer, XtPointer );
  void output ( Widget, XtPointer, XtPointer );
  void sensorData ( Widget, XtPointer, XtPointer );
  void incident ( Widget, XtPointer, XtPointer );
};

#endif
