//-*-c++-*------------------------------------------------------------
// TMS_ApidParameters.h
//
// Sreeram Radhakrishnan
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef TMS_APIDPARAMETERS_HEADER
#define TMS_APIDPARAMETERS_HEADER

#include <cstdio>
#include <vector>
#include <iostream>

const int SENSORS_PER_LINE = 20;

class TMS_ApidParameters
{
private:

  int prmGrpID_;
  std::vector<int> sensorList_;
  double compWaveTh1_;
  double compWaveTh2_;
  int compWaveTestEnabled_;
  int persistTestEnabled_;
  int medTrfEnabled_;
  int ligtTrfEnabled_;
  double incDetTh1_; // Th stands for threshold
  double incDetTh2_;
  double incDetTh3_;
  double medTrfIncDet1_;
  double medTrfIncDet2_;
  double medTrfTh_;
  double lowTrfTh_;
  double incClrTh_;
  double persistencyTh_;


public:

  TMS_ApidParameters() : prmGrpID_(-99) { }
  ~TMS_ApidParameters() { }

  std::vector<int >*  sensorListptr() { return &sensorList_; }

  void print(std::ostream& os = std::cout);
  void assignprmGrpID(int n) { prmGrpID_ = n; }
  void assigncompWaveTestEnabled(int n) { compWaveTestEnabled_  = n; }
  void assignmedTrfEnabled(int n) { medTrfEnabled_  = n; }
  void assignligtTrfEnabled(int n) { ligtTrfEnabled_  = n; }
  void assignincDetTh1(double f) { incDetTh1_  = f; }
  void assignincDetTh2(double f) { incDetTh2_  = f; }
  void assignincDetTh3(double f) { incDetTh3_ = f; }
  void assignmedTrfIncDet1(double f) { medTrfIncDet1_ = f; }
  void assignmedTrfIncDet2(double f) { medTrfIncDet2_ = f; }
  void assignmedTrfTh(double f) { medTrfTh_ = f; }
  void assignlowTrfTh(double f) { lowTrfTh_ = f; }
  void assignincClrTh(double f) { incClrTh_ = f; }
  void assignpersistencyTh(double f) { persistencyTh_ = f; }
  void assignpersistTestEnabled(int n) { persistTestEnabled_  = n; }
  int compWaveTh1() { return int(compWaveTh1_); }
  int compWaveTh2() { return int(compWaveTh2_); }
  int compWaveTestEnabled() { return compWaveTestEnabled_; }
  int persistTestEnabled() { return persistTestEnabled_; }
  double medTrfEnabled() { return medTrfEnabled_; }
  double ligtTrfEnabled() { return ligtTrfEnabled_; }
  double incDetTh1() { return incDetTh1_; }
  double incDetTh2() { return incDetTh2_; }
  double incDetTh3() { return incDetTh3_; }
  double medTrfIncDet1() { return medTrfIncDet1_; }
  double medTrfIncDet2() { return medTrfIncDet2_; }
  double medTrfTh() { return medTrfTh_; }
  double lowTrfTh() { return lowTrfTh_; }
  double incClrTh() { return incClrTh_; }
  double persistencyTh() { return persistencyTh_; }
  void assignlistdim(int n) { sensorList_.reserve(n); };
  void addsensorid(int n) { sensorList_.push_back(n); };
  void assigncompWaveTh1(float f) { compWaveTh1_  = f; }
  void assigncompWaveTh2(float f) { compWaveTh2_  = f; }
  int prmGrpID() { return prmGrpID_; }
};

#endif
