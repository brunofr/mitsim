//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_Action.h
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#include "TMS_Action.h"

// This is a base class for TMS_LusAction, TMS_VslsAction, and
// TMS_PsAction.  It defines only some data members, inline functions.

