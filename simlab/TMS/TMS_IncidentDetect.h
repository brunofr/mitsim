//-*-c++-*------------------------------------------------------------
// TMS_IncidentDetect.h
//
// Sreeram Radhakrishnan
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef TMS_INCIDENTDETECT_HEADER
#define TMS_INCIDENTDETECT_HEADER

#include <fstream>
#include "TMS_SurvStation.h"

#ifdef INTERNAL_GUI
#include "TMS_DetectedInc.h"
#endif

class TMS_IncidentDetector
{
protected:

  int timeStep_;

  std::ofstream osIncLogFile_;

public:

  TMS_IncidentDetector() 
	{
	  const char *fn = ToolKit::outfile(logfname());
	  osIncLogFile_.open(fn, std::ios::out);
	  osIncLogFile_.close();
	  osIncLogFile_.open(fn, std::ios::app | std::ios::out);
	}

  ~TMS_IncidentDetector()
	{
	  osIncLogFile_.close();
	}

  inline int gettimeStep() { return timeStep_; }
  inline int settimeStep(int n) { timeStep_ = n; return timeStep_;}

  static char *incLogFileName_;
  
  static inline char **logfnameptr(){ return &incLogFileName_ ; }
  static inline char *logfname(){ return incLogFileName_ ; }

  void declareIncident(SurvList);
  void declareClearence(SurvList);
  void declareRecurrent(SurvList);

  SurvList next(SurvList );
  SurvList prev(SurvList );
  RN_Sensor *nextsensor(RN_Sensor *);

  virtual void incidentDetect(int){}
  virtual void checkforIncident(SurvList ){}
  virtual void checkforClearence(SurvList ){}

#ifdef INTERNAL_GUI
  detected_inc_list * addIncident(SurvList);
#endif

};

extern TMS_IncidentDetector *theIncidentDetector;

#endif
