//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_VslsLogic.C
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#include <sstream>
using namespace std;

#include <Tools/Reader.h>
#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>

#include <Tools/Math.h>

#include <GRN/RN_CtrlStation.h>

#include "TMS_Parameter.h"
#include "TMS_CtrlLogic.h"
#include "TMS_VslsAction.h"
#include "TMS_Incident.h"
#include "TMS_Link.h"
#include "TMS_Segment.h"
#include "TMS_Signal.h"

void
TMS_VslsAction::read(Reader& is)
{
   static int nactions = 0;
   code_ = ++ nactions;

   is >> situation_ >> start_ >> step_;
   is >> fromRef_ >> fromDistance_;
   is >> toRef_ >> toDistance_;

   fromDistance_ *= theParameter->lengthFactor();
   toDistance_ *= theParameter->lengthFactor();
}


void
TMS_VslsAction::print(ostream &os)
{
   os << indent
      << situation_ << endc
      << Round(start_/theParameter->speedFactor()) << endc
      << Round(step_/theParameter->speedFactor()) << endc
      << fromRef_ << endc
      << (fromDistance_/theParameter->lengthFactor()) << endc
      << toRef_ << endc
      << (toDistance_/theParameter->lengthFactor()) << endl;
}


/*
 *------------------------------------------------------------------
 * Set speed limit of VSLS to "speed".  It returns the speed limit
 * to be used by the next sign.
 *------------------------------------------------------------------
 */

int
TMS_VslsAction::update(CtrlList ctrl, int speed)
{
   RN_Signal *vsls = (*ctrl)->signal();
   int ub;
   if (speed < theCtrlLogic->speedLimitLowerBound()) {
      speed = theCtrlLogic->speedLimitLowerBound();
   } else if (speed >
			  (ub = Round(vsls->segment()->speedLimit() /
						  theParameter->speedFactor()))) {
      speed = ub;
   }
   vsls->state(speed);
   return (speed + step_);
}


/*
 *------------------------------------------------------------------
 * Update the VSLS signs based on this TMS_VslsAction rule.
 *
 * CAUTION: Only are the signs in the current link and the upstream
 * and downstream links with depth of 1 is searched.
 *------------------------------------------------------------------
 */

void
TMS_VslsAction::applyTo(TMS_Incident& inc)
{
   TMS_Link * link = (TMS_Link *) inc.link();
   CtrlList ctrl;
   double start_pos, stop_pos;

   switch (fromRef_) {
      case INCIDENT_POSITION:
      {
		 start_pos = inc.distance();
		 break;
      }
      case PORTAL_POSITION:
      {
		 ctrl = link->searchTypedCtrl(
			inc.distance(),
			theCtrlLogic->searchingDistUpbound(),
			CTRL_PS);
		 if (ctrl) start_pos = (*ctrl)->distance();
		 else {
			if (ToolKit::debug()) {
			   cout << "No UpPortal signal found. VSLS command "
					<< code_ << " ignored." << endl;
			}
			return;
		 }
		 break;
      }
      default:
      {
		 cerr << "Error:: Unknown fromRef <" << fromRef_
			  << ">. VSLS command " << code_ << " ignored." << endl;
		 return;
      }
   }

   start_pos += fromDistance_;

   switch (toRef_) {
      case INCIDENT_POSITION:
      {
		 stop_pos = inc.distance();
		 break;
      }
      case PORTAL_POSITION:
      {
		 ctrl = link->searchTypedCtrl(
			inc.distance(),
			theCtrlLogic->searchingDistUpbound(),
			CTRL_PS);
		 if (ctrl) stop_pos = (*ctrl)->distance();
		 else {
			if (ToolKit::debug()) {
			   cout << "No UpPortal signal found. ";
			   cout << "VSLS command " << code_
					<< " applied within "
					<< theCtrlLogic->searchingDistUpbound()/
				  theParameter->lengthFactor()
					<< " upstream." << endl;
			}
			stop_pos = inc.distance() +
			   theCtrlLogic->searchingDistUpbound();
		 }
		 break;
      }
      default:
      {
		 cerr << "Error:: Unknown toRef <" << toRef_
			  << ">. VSLS command " << code_ << " ignored." << endl;
		 return;
      }
   }

   stop_pos += toDistance_;

   double start_dis, stop_dis;
   int speed = start_;			/* initial speed limit */
   int counts = 0;
   ostringstream os ;

   if (start_pos < stop_pos)	/* walk upstream */
   {
      /* If the control range goes beyond downstream end of the
       * current link, set the signs in the downstream links.  */

      if (start_pos < 0.0) {
		 int narcs = link->nDnLinks();
		 for (int k = 0; k < narcs; k ++) {
			RN_Link* plink = link->dnLink(k);
			stop_dis = plink->length();
			start_dis = start_pos + stop_dis;
			ctrl = plink->prevCtrl(start_dis);
			while (ctrl != NULL &&
				   (*ctrl)->distance() <= stop_dis )
			{
			   if ((*ctrl)->type() == CTRL_VSLS)
			   {
				  speed = update(ctrl, speed);
				  counts ++;
				  os << " " << (*ctrl)->signal()->code() ;
			   }
			   ctrl = ctrl->prev();
			}
		 }
      }

      /* walking upstream from "start_dis" until "stop_dis".  Set all
       * the VSLSs in the marked link to the desired state.  */

      start_dis = Max(start_pos, 0.0);
      stop_dis = Min(stop_pos, link->length());
      ctrl = link->prevCtrl(start_dis);

      while (ctrl != NULL &&
			 (*ctrl)->distance() <= stop_dis )
      {
		 if ((*ctrl)->type() == CTRL_VSLS)
		 {
			speed = update(ctrl, speed);
			counts ++;
			os << " " << (*ctrl)->signal()->code() ;
		 }
		 ctrl = ctrl->prev();
      }

      /* If the control range goes beyond upstream end of the current
       * link, continute to set the signs in the upstream links.  */

      start_dis = 0.0;
      stop_dis = stop_pos - link->length();
      if (stop_dis > 0.0) {
		 int narcs = link->nUpLinks();
		 for (int k = 0; k < narcs; k ++) {
			ctrl = link->upLink(k)->lastCtrl();
			while (ctrl != NULL &&
				   (*ctrl)->distance() <= stop_dis ) {
			   if ((*ctrl)->type() == CTRL_VSLS) {
				  speed = update(ctrl, speed);
				  counts ++;
				  os << " " << (*ctrl)->signal()->code() ;
			   }
			   ctrl = ctrl->prev();
			}
		 }
      }
   } else {			/* walk downstream */

      /* If the control range goes beyond upstream end of the current
       * link, set the signs in the upstream links.  */

      start_dis = start_pos - link->length();
      stop_dis = 0.0;

      if (start_pos > 0.0) {
		 int narcs = link->nUpLinks();
		 for (int k = 0; k < narcs; k ++) {
			ctrl = link->upLink(k)->nextCtrl(start_dis);
			while (ctrl != NULL &&
				   (*ctrl)->distance() >= stop_dis ) {
			   if ((*ctrl)->type() == CTRL_VSLS) {
				  speed = update(ctrl, speed);
				  counts ++;
				  os << " " << (*ctrl)->signal()->code() ;
			   }
			   ctrl = ctrl->next();
			}
		 }
      }

      /* walking upstream from "start_dis" until "stop_dis".  Set all
       * the VSLSs in the marked link to the desired state.  */

      start_dis = Min(start_pos, link->length());
      stop_dis = Max(stop_pos, 0.0);
      ctrl = link->nextCtrl(start_dis);

      while (ctrl != NULL &&
			 (*ctrl)->distance() >= stop_dis ) {
		 if ((*ctrl)->type() == CTRL_VSLS) {
			speed = update(ctrl, speed);
			counts ++;
			os << " " << (*ctrl)->signal()->code() ;
		 }
		 ctrl = ctrl->next();
      }

      /* If the control range goes beyond downstream end of the
       * current link, set the signs in the downstream links.  */

      if (stop_pos < 0.0) {
		 int narcs = link->nDnLinks();
		 for (int k = 0; k < narcs; k ++) {
			RN_Link* plink = link->dnLink(k);
			start_dis = plink->length();
			stop_dis = stop_pos + start_dis;
			ctrl = plink->ctrlList(); /* first ctrl */
			while (ctrl != NULL &&
				   (*ctrl)->distance() >= stop_dis ) {
			   if ((*ctrl)->type() == CTRL_VSLS) {
				  speed = update(ctrl, speed);
				  counts ++;
				  os << " " << (*ctrl)->signal()->code() ;
			   }
			   ctrl = ctrl->next();
			}
		 }
      }
   }
   if (ToolKit::verbose() && counts > 0) {
       //os << '\0' ;
     cout << "VSLS command " << parent_->code()
	  << "/" << code_ << " applied to " << counts
	  << " signs (" << os.str() << " ) at "
	  << theSimulationClock->currentStringTime() << endl;
   }

}


