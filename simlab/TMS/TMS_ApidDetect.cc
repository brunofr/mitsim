//-*-c++-*------------------------------------------------------------
// TMS_ApidDetect.cc
//
// Sreeram Radhakrishnan
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

//#include <fstream>
#include <cstdio>
#include <vector>
using namespace std;

#include <GRN/RoadNetwork.h>
#include <GRN/RN_Link.h>
#include <Tools/SimulationClock.h>
#include <Tools/ToolKit.h>
#include "TMS_Segment.h"
#include "TMS_Network.h"
#include "TMS_SurvStation.h"
#include "TMS_Sensor.h"

#include "TMS_ApidParameters.h"
#include "TMS_ApidPrmTable.h"
#include "TMS_ApidDetect.h"

#ifdef INTERNAL_GUI
#include "TMS_DetectedInc.h"
#endif

void TMS_ApidDetector::incidentDetect(int timestep)
{
  timeStep_ = timestep;
  int nlinks = theNetwork->nLinks(), stationstate;
  int nsensors = theNetwork->nSensors();
  register int i;

  SurvList surv;
  for(i = 0; i < nlinks; i ++) {  
	if(theNetwork->link(i)->linkType() == LINK_TYPE_FREEWAY){
	  surv = theNetwork->link(i)->survList(); 
	  while ((surv != NULL)&&((*surv)->prmGrpID() > 0)) { 
		stationstate = (*surv)->stationState(); 
		int test = (*surv)->prmGrpID();
		if(stationstate & CONFIRMED_INC){
		  checkforClearence(surv);
		} else if(stationstate & TENTATIVE_INC){
		  checkforTentativeInc(surv);
		} else {
		  checkforIncident(surv); 
		}
		surv = surv->next();
	  }
	}
  }
  for(i = 0; i < nsensors; i++) {
	RN_Sensor *currSens = theNetwork->sensor(i);
	currSens->setOccPrev(1, currSens->occPrev(0));
	currSens->setOccPrev(0, currSens->occupancy());
	currSens->setSpdPrev(1, currSens->spdPrev(0));
	currSens->setSpdPrev(0, currSens->speed());
  }
}

void TMS_ApidDetector::checkforIncident(SurvList surv)
{
  int n_Lanes = (*surv)->nLanes(), result;
  TMS_ApidParameters *p 
	= theApidPrmTable->prmGrps((*surv)->prmGrpID() - 1);

  if(p->medTrfEnabled() ||
	 p->ligtTrfEnabled())	{

	for(register int j = 0; ((j < n_Lanes)&&((*surv)->sensor(j))); j++) {
	  RN_Sensor *currSens = (*surv)->sensor(j);
	  int code = currSens->code();

	  double occ = currSens->occupancy();
		
	  if (occ >= p->lowTrfTh()) {

		if (occ >= p->medTrfTh())
		  result = incDetectCheck((*surv)->sensor(j));	
		else
		  result = medVolIncDetectCheck((*surv)->sensor(j));
	  }
	  // else -- to be implemented.
	  // result = lowVolIncDetectCheck((*surv)->sensor(j));

	  switch (result) {

	  case CONFIRMED_INC:
		{
		  (*surv)->clearState(INC_STATUS);
		  (*surv)->setState(CONFIRMED_INC);
		  (*surv)->setState(INC_DCR_PERSIST_CNT);
#ifdef INTERNAL_GUI
		  theDetectedIncs = theIncidentDetector->addIncident(surv);
#endif
		  declareIncident(surv);
		  break;
		}
		
	  case TENTATIVE_INC:
		{
		  (*surv)->clearState(INC_STATUS);
		  (*surv)->setState(TENTATIVE_INC);
		  (*surv)->addState(INC_DCR_PERSIST_ONE);
		  break;
		}
	  default:
		break;
	  }
	}
  }
}

void TMS_ApidDetector::checkforClearence(SurvList surv)
{
  int n_Lanes = (*surv)->nLanes();
  TMS_ApidParameters *p 
	= theApidPrmTable->prmGrps((*surv)->prmGrpID() - 1);


  for(register int j=0; j < n_Lanes;j++) {
	double occrdf = getOccrdf((*surv)->sensor(j));
	if(occrdf != ERROR_MESG){

	  if((occrdf < p->incClrTh())
		 &&((*surv)->stationState() & CONFIRMED_INC)){

		(*surv)->clearState(CONFIRMED_INC);
		(*surv)->clearState(INC_DCR_PERSIST_CNT);
		(*surv)->setState(CLEARED_INC);
	  }
	}
	if((*surv)->stationState() == CLEARED_INC) {
	  declareClearence(surv);
	  break;
	}
  }

}

void TMS_ApidDetector::checkforTentativeInc(SurvList surv)
{
  int n_Lanes = (*surv)->nLanes();

  TMS_ApidParameters *p 
	= theApidPrmTable->prmGrps((*surv)->prmGrpID() - 1);

  for(register int j=0;j < n_Lanes; j++)
	{
	  RN_Sensor *currSens = (*surv)->sensor(j);

	  double occrdf = getOccrdf(currSens), occnext = getDocc(currSens);

	  double docctd = getDocctd(currSens);

	  if((occrdf != ERROR_MESG)&&(docctd != ERROR_MESG)){
		if(occrdf > p->persistencyTh()) {

		  (*surv)->addState(INC_DCR_PERSIST_ONE);

		  int incPeriodCount = (*surv)->incDcrPersistCount();

		  if(incPeriodCount >= theApidPrmTable->incPersistency()) {
  
			(*surv)->clearState(TENTATIVE_INC);
			(*surv)->setState(CONFIRMED_INC);

#ifdef INTERNAL_GUI
			theDetectedIncs = theIncidentDetector->addIncident(surv);
#endif
			declareIncident(surv);

		  }
		  break;
		} else {
		  
		  (*surv)->clearState(INC_DCR_PERSIST_CNT);
		  (*surv)->clearState(TENTATIVE_INC);

		  if(p->compWaveTestEnabled()) {
	  
			if((occnext > p->compWaveTh1())
			   &&(docctd <= p->compWaveTh2()))

			  (*surv)->setState(NEW_COMP_WAVE);
			++compWavePeriodCnt_;
		  }
		}
	  }
	}
}

int TMS_ApidDetector::incDetectCheck(RN_Sensor *currSens)
{
  RN_SurvStation *surv = currSens->station();

  TMS_ApidParameters *p 
	= theApidPrmTable->prmGrps(surv->prmGrpID() - 1);

  if(currSens != NULL){
		
	double docctd = getDocctd(currSens);

	if(docctd != ERROR_MESG){

	  if(p->compWaveTestEnabled()){
	
		if(docctd > p->compWaveTh1()){
		  
		  if(docctd > p->compWaveTh2()){

			//		if(newCompWave()){

			//updateCompWaveHistory(surv);
			//}
		  }
		  else {
	  
			//setState(surv,NEW_COMP_WAVE);

			//++compWavePeriodCnt_;
		  }
		}
		else {
			  
		  //	  if(newCompWave()){
		  //updateCompWaveHistory(surv);
		  //}
		  //else{
		  //califIncDetection(surv);
		  //}
		}
	  }
	  else{
		int result = califIncDetection(currSens);
		return result;
	  }
	}
  }
  return ERROR_MESG;
}

void TMS_ApidDetector::updateCompWaveHistory(SurvList surv)
{
  if(compWavePeriodCnt_){

	if(compWavePeriodCnt_ == theApidPrmTable->compWaveTestPeriod()){
	  compWavePeriodCnt_ = 0;
	}
	else{
	  ++compWavePeriodCnt_;
	}
  }
}

int TMS_ApidDetector::califIncDetection(RN_Sensor *currSens)
{
  RN_SurvStation *surv = currSens->station();

  TMS_ApidParameters *p 
	= theApidPrmTable->prmGrps(surv->prmGrpID() - 1);

  double occdf = getOccdf(currSens);
  double occrdf = getOccrdf(currSens);
  double docc = getDocc(currSens);

  if((occdf != ERROR_MESG)&&(occrdf != ERROR_MESG)&&(docc != ERROR_MESG))
	if((occdf > p->incDetTh1())
	   &&(occrdf > p->incDetTh2())
	   &&(docc < p->incDetTh3())) {

	  if(p->persistTestEnabled()){
		int incPeriodCount = surv->incDcrPersistCount();

		if(incPeriodCount < theApidPrmTable->incPersistency()){
		  return TENTATIVE_INC;
		} else {
		  return CONFIRMED_INC;
		}
	  }
	}
  return ERROR_MESG;
}


int TMS_ApidDetector::medVolIncDetectCheck(RN_Sensor *currSens)
{
  RN_SurvStation *surv = currSens->station();

  TMS_ApidParameters *p 
	= theApidPrmTable->prmGrps(surv->prmGrpID()-1);

  double occrdf = getOccrdf(currSens), spdtdf = getSpdtdf(currSens);
  //  updateCompWaveHistory(surv);

  if((p->medTrfEnabled()) && 
	 (occrdf != ERROR_MESG) &&
	 (spdtdf != ERROR_MESG)) {

	if((occrdf > p->medTrfIncDet1())
	   &&(spdtdf > p->medTrfIncDet2())) {

	  if(p->persistTestEnabled()){
		int incPeriodCount = surv->incDcrPersistCount();

		if(incPeriodCount < theApidPrmTable->incPersistency()){
		  return TENTATIVE_INC;
		}
		else {
		  return CONFIRMED_INC;
		}
	  }
	}
  }
  return ERROR_MESG;
}

int TMS_ApidDetector::lowVolIncDetectCheck(RN_Sensor *currSens)
{
  //updateCompWaveHistory(surv);
  RN_SurvStation *surv = currSens->station();

  TMS_ApidParameters *p = theApidPrmTable->prmGrps(surv->prmGrpID()-1);

  if(p->ligtTrfEnabled()) {
  } else {
  }
  return ERROR_MESG;
}

double TMS_ApidDetector::getDocc(RN_Sensor *sensor)
{
  RN_Sensor *nxtSens = nextsensor(sensor);
  if(nxtSens)
	return(nxtSens->occupancy());
  else
	return ERROR_MESG;
}

double TMS_ApidDetector::getOccdf(RN_Sensor *sensor)
{
  double docc = getDocc(sensor);
  if(docc != ERROR_MESG)
	return(sensor->occupancy() - docc);
  else
	return ERROR_MESG;
}

double TMS_ApidDetector::getOccrdf(RN_Sensor *sensor)
{
  double occdf = getOccdf(sensor);
  if((occdf != ERROR_MESG)&&(sensor->occupancy() > 0))
	return(getOccdf(sensor)/(sensor->occupancy()));
  else 
	return ERROR_MESG;
}

double TMS_ApidDetector::getDocctd(RN_Sensor *sensor)
{
  RN_Sensor *nxtSens = nextsensor(sensor);
  if((nxtSens)&&(nxtSens->occPrev(1) > 0))
	return ((nxtSens->occPrev(1) - nxtSens->occupancy())/nxtSens->occPrev(1));
  else
	return ERROR_MESG;
}

double TMS_ApidDetector::getSpdtdf(RN_Sensor *sensor)
{
  if(sensor->spdPrev(1) > 0)
	return((sensor->spdPrev(1) - sensor->speed())/sensor->spdPrev(1));
  else
	return ERROR_MESG;
}	 

