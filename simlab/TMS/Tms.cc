//-*-c++-*------------------------------------------------------------
// Tms.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------



#include <Tools/ToolKit.h>

#include "TMS_Engine.h"
#include "TMS_Setup.h"
#include "TMS_Version.h"
#include "TMS_CmdArgsParser.h"

#ifdef INTERNAL_GUI
#include "TMS_Interface.h"
#endif

int main ( int argc, char **argv )
{
   ::Welcome(argv[0]);

   ToolKit::initialize();

   TMS_Engine engine;

#ifdef INTERNAL_GUI
   theInterface = new TMS_Interface(&argc, argv);
#endif

   TMS_CmdArgsParser cp;
   cp.parse(&argc, argv);

   // Run the simulation.

   engine.run();

   engine.quit(STATE_DONE);

   return 0;
}
