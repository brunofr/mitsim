//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_Link.h
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#ifndef TMS_LINK_HEADER
#define TMS_LINK_HEADER

//#include <iostream>
#include <GRN/RN_Link.h>

class TMS_Link : public RN_Link
{
public:

  TMS_Link() : RN_Link() { }
  ~TMS_Link() { }

  CtrlList searchCtrl(double startDistance,
					  double maxDistance, CtrlList ctrl = NULL);

  CtrlList searchTypedCtrl(double startDistance,
						   double maxDistance,
						   int deviceType,
						   CtrlList ctrl = NULL);

  SurvList searchSurv(double startDistance,
					  double maxDistance, SurvList surv = NULL);
	
  SurvList searchTypedSurv(double startDistance,
						   double maxDistance,
						   int deviceType,
						   int dataType = 0xFFFF,
						   SurvList surv = NULL);
};

#endif  // THE END OF THIS FILE

