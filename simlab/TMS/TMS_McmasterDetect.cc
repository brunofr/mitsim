#include <iostream>
#include <cstdio>
#include <vector>
using namespace std;

#include <GRN/RoadNetwork.h>
#include <GRN/RN_Link.h>
#include <GRN/Constants.h>
#include <Tools/SimulationClock.h>
#include <Tools/ToolKit.h>
#include "TMS_Network.h"
#include "TMS_Segment.h"
#include "TMS_SurvStation.h"
#include "TMS_Sensor.h"

#include "TMS_McmasterParameters.h"
#include "TMS_McmasterPrmTable.h"
#include "TMS_McmasterDetect.h"

#ifdef INTERNAL_GUI
#include "TMS_DetectedInc.h"
#endif

void TMS_McmasterDetector::incidentDetect(int timestep)
{
  timeStep_ = timestep;

  int nlinks = theNetwork->nLinks(), stationstate;
  for(register int i = 0; i < nlinks; i ++) { 
	if (theNetwork->link(i)->linkType() != LINK_TYPE_FREEWAY) continue;
	SurvList surv = theNetwork->link(i)->survList();
	while ((surv != NULL)&&((*surv)->prmGrpID() > 0)) {
	  stationstate = (*surv)->stationState();
	  if(stationstate & CONFIRMED_INC){
		checkforClearence(surv);
	  } else {
		checkforIncident(surv);
	  }
	  surv = surv->next();
	}
  }
}

void TMS_McmasterDetector::checkforIncident(SurvList surv)
{
  int clrCnt, dcrCnt;

  RN_Sensor *curSens, *nxtSens;
  
  int n_Lanes = (*surv)->segment()->nLanes();
			   
  int stationstate = (*surv)->stationState();
			   
  for(register int j = 0; j < n_Lanes; j++) {

	curSens = (*surv)->sensor(j);

	if (curSens == NULL) continue;

	nxtSens = nextsensor(curSens);

	if (nxtSens) { 

	  clrCnt = curSens->incClrPersistCount();
	  dcrCnt = curSens->incDcrPersistCount();

	  switch(incidentCouldExist(surv, curSens, nxtSens)) {

	  case RESET_DCR_COUNT: {
		if(dcrCnt > 0){
		  curSens->clearState(SENSOR_INC_DCR_PERSIST_COUNT);
		  curSens->clearState(SENSOR_INC_FLAG);
		}
		break;
	  }

	  case RECURRENT_CONGESTION: {
		declareRecurrent(surv);
		if(dcrCnt > 0){
		  curSens->clearState(SENSOR_INC_DCR_PERSIST_COUNT);
		  curSens->clearState(SENSOR_INC_FLAG);
		}
		if(clrCnt > 0)
		  curSens->clearState(SENSOR_INC_CLR_PERSIST_COUNT);
		break;
	  }

	  case RESET_ALL_COUNT: {
		if(dcrCnt > 0){
		  curSens->clearState(SENSOR_INC_DCR_PERSIST_COUNT);
		  curSens->clearState(SENSOR_INC_FLAG);
		}
		  
		if(clrCnt > 0)
		  curSens->clearState(SENSOR_INC_CLR_PERSIST_COUNT);
		break;
	  }

	  case INCR_DCR_COUNT: {

		if (dcrCnt < theMcmasterPrmTable->incPersistency()) {
		  curSens->addState(SENSOR_INC_DCR_PERSIST_ONE);
		  dcrCnt = curSens->incDcrPersistCount();

		  if ((dcrCnt >= theMcmasterPrmTable->incPersistency()) &&
			  (curSens->state() & SENSOR_INC_FLAG)) {
			// (*surv)->clearState(NO_INCIDENT);
			stationstate = (*surv)->setState(fixstationstate(surv));
			curSens->clearState(SENSOR_INC_CLR_PERSIST_COUNT);
			if(stationstate & SECONDARY_INC)
			  curSens->clearState(SENSOR_INC_FLAG);
		  }
		} else {
		  curSens->clearState(SENSOR_INC_DCR_PERSIST_COUNT);
		}
		break;
	  }

	  default:
		break;
	  }
	  if (stationstate & CONFIRMED_INC) {
#ifdef INTERNAL_GUI
		theDetectedIncs = theIncidentDetector->addIncident(surv);
#endif
		declareIncident(surv);
		for(register int k = 0; k < n_Lanes; k++) {
		  (*surv)->sensor(k)->setState(SENSOR_INC_FLAG);
		  (*surv)->sensor(k)->setState(SENSOR_INC_DCR_PERSIST_COUNT);
		  (*surv)->sensor(k)->clearState(SENSOR_INC_CLR_PERSIST_COUNT);
		}
		break;
	  }
	}
  }
}

int TMS_McmasterDetector::incidentCouldExist(SurvList surv, RN_Sensor *curSens,
											 RN_Sensor *nxtSens)
{
  double speed = curSens->speed();
			  
  int currSenReg = curSens->getRegion(timeStep_);
			  
  int nextSenReg = nxtSens->getRegion(timeStep_);
			  
  int currCount = (*surv)->count(),nextCount = (*(next(surv)))->count();
			  
  TMS_McmasterParameters *p =
	theMcmasterPrmTable->prmGrps((*surv)->prmGrpID()-1);
  
  if (speed < p->getavSpeed()) {
	if (((currSenReg == INC_DETECTION_STATUS_DN) &&
		 (nextSenReg == INC_DETECTION_STATUS_DN)) ||
		((currSenReg == INC_DETECTION_STATUS_UP) &&
		 (nextSenReg == INC_DETECTION_STATUS_DN))) {
	  if (((curSens->incDcrPersistCount() == 0) &&
		   (currCount > nextCount)) ||
		  (curSens->incDcrPersistCount() > 0)) {
		if (curSens->sensorIncState() == 0) {
		  if ((currSenReg == INC_DETECTION_STATUS_UP) &&
			  (nextSenReg == INC_DETECTION_STATUS_DN))
			curSens->setState(SENSOR_INC_FLAG);
		}
		return INCR_DCR_COUNT; // increment declaration count
	  } else 
		return RESET_DCR_COUNT; // reset incident period counts
	} else {
	  if (((currSenReg == INC_DETECTION_STATUS_DN) &&
		   (nextSenReg == INC_DETECTION_RECURRENT)) ||
		  ((currSenReg == INC_DETECTION_STATUS_UP) &&
		   (nextSenReg == INC_DETECTION_RECURRENT)))
		return RECURRENT_CONGESTION; 
	  else 
		return RESET_ALL_COUNT;	// reset both incident and normal counts 
	}
  }
  return RESET_ALL_COUNT;
}  

void TMS_McmasterDetector::checkforClearence(SurvList surv)
{
  int clrCnt;

  int stationstate = (*surv)->stationState();

  RN_Sensor *curSens, *nxtSens;

  int n_Lanes = (*surv)->segment()->nLanes();

  for(register int j = 0; j < n_Lanes; j++)	{

	curSens = (*surv)->sensor(j);

	if (!curSens) continue;
	nxtSens = nextsensor(curSens);
		
	if (!nxtSens) continue;

	clrCnt = curSens->incClrPersistCount();
		  
	switch(incidentIsOver(surv,curSens,nxtSens)) {
	case RECURRENT_CONGESTION: {
	  declareRecurrent(surv);
	  break;
	}
	case INCR_CLR_COUNT: {
	  curSens->addState(SENSOR_INC_CLR_PERSIST_ONE);
	  clrCnt = curSens->incClrPersistCount();
	  if (clrCnt == theMcmasterPrmTable->normalPersistency()) {
		(*surv)->clearState(CONFIRMED_INC);
		stationstate = (*surv)->setState(CLEARED_INC);
		clearallstationsahead(surv);
		int dcrCnt = curSens->incDcrPersistCount();
		curSens->clearState(SENSOR_INC_DCR_PERSIST_COUNT);
	  }
	  break;
	}
	case RESET_CLR_COUNT: { 
	  curSens->clearState(SENSOR_INC_CLR_PERSIST_COUNT);
	  clrCnt = curSens->incClrPersistCount();
	  break;
	}
	default:
	  break;
	}					
	if(stationstate & CLEARED_INC) {
	  declareClearence(surv);
	  for(register int k=0; k < n_Lanes; k++) {
		(*surv)->sensor(k)->clearState(SENSOR_INC_FLAG);
		(*surv)->sensor(k)->clearState(SENSOR_INC_DCR_PERSIST_COUNT);
		(*surv)->sensor(k)->clearState(SENSOR_INC_CLR_PERSIST_COUNT);
	  }
	  break;
	}
  }
}

int TMS_McmasterDetector::incidentIsOver(SurvList surv, RN_Sensor *curSens,
										 RN_Sensor *nxtSens)
{
  int currSenReg = curSens->getRegion(timeStep_);
			  
  int nextSenReg = nxtSens->getRegion(timeStep_);
			  
  int currCount = (*surv)->count(), nextCount = (*(next(surv)))->count();
			  
  TMS_McmasterParameters *p = theMcmasterPrmTable->prmGrps((*surv)->prmGrpID()-1);
  
  if (((currSenReg == INC_DETECTION_STATUS_NONE) &&
	   (nextSenReg == INC_DETECTION_STATUS_NONE)) ||
	  ((currSenReg == INC_DETECTION_STATUS_NONE) &&
	   (nextSenReg == INC_DETECTION_STATUS_DN))) {
	return INCR_CLR_COUNT; // increment clearence counts
  }
  
  if(((currSenReg == INC_DETECTION_STATUS_DN) &&
	  (nextSenReg == INC_DETECTION_STATUS_DN)) ||
	 ((currSenReg == INC_DETECTION_STATUS_UP) &&
	  (nextSenReg == INC_DETECTION_STATUS_DN)))
	return RESET_CLR_COUNT; // reset clearence counts
  
  if(((currSenReg == INC_DETECTION_STATUS_DN) &&
	  (nextSenReg == INC_DETECTION_STATUS_UP)) ||
	 ((currSenReg == INC_DETECTION_STATUS_UP) && 
	  (nextSenReg == INC_DETECTION_STATUS_UP)))
	return INCR_CLR_COUNT; // increment clearence counts

  if(((currSenReg == INC_DETECTION_STATUS_DN) &&
	  (nextSenReg == INC_DETECTION_RECURRENT)) ||
	 ((currSenReg == INC_DETECTION_STATUS_UP) &&
	  (nextSenReg == INC_DETECTION_RECURRENT)))
	return RECURRENT_CONGESTION; // recurrent congestion
  
  return RESET_ALL_COUNT;
}  

int TMS_McmasterDetector::fixstationstate(SurvList surv)
{
  SurvList prevstn = prev(surv);

  while((prevstn)
		&&(!(((*prevstn)->stationState()) & CONFIRMED_INC))
		&&(!(((*prevstn)->stationState()) & SECONDARY_INC)))
	prevstn = prev(prevstn);

  if(prevstn == NULL)
	return CONFIRMED_INC;
  else
	return SECONDARY_INC;
}

void TMS_McmasterDetector::clearallstationsahead(SurvList surv)
{
  int n_Lanes;

  int incPersistency = theMcmasterPrmTable->incPersistency();

  SurvList nextstn = next(surv);

  while((nextstn)&&((*nextstn)->stationState() & SECONDARY_INC)) {
	(*nextstn)->clearState(SECONDARY_INC);
	n_Lanes = (*nextstn)->segment()->nLanes();
	for(register int i=0; i < n_Lanes ; i++) {
	  if((*nextstn)->sensor(i)->incDcrPersistCount() >= incPersistency)
		(*nextstn)->sensor(i)->clearState(SENSOR_INC_DCR_PERSIST_COUNT);
	  
	  (*nextstn)->sensor(i)->clearState(SENSOR_INC_CLR_PERSIST_COUNT);
	}
	nextstn = next(nextstn);
  }
}

  
