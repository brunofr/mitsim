/*-*-c++-*------------------------------------------------------------
 *  TMS_McmasterPrmScanner.lex -- Sensor paremeter table lexical analyzer generator
 *--------------------------------------------------------------------
 */

%{
#include "TMS_McmasterPrmParser.h"
#define YY_BREAK
%}

%option yyclass="TMS_McmasterPrmFlexLexer"
%option noyywrap
%option yylineno

C	(#.*)|(%.*)|("//".*)
WS	[\t ,;\n]+

D	[0-9]
N	{D}+
E       [eE][-+]?{D}+

F1      {D}+\.{D}*({E})?
F2      {D}*\.{D}+({E})?
F3      {D}+({E})?

I	[-+]?{N}
R       [-+]?({F1}|{F2}|{F3})
NAME    [[]([A-Za-z \t]*)[]]

%%

"/*"	{	/* skip comments */
  int c;
  while ((c = yyinput()) != 0) {
    if (c == '*') {
      if ((c = yyinput()) == '/') break;
      else unput(c);
    }
  }
  break;
}

{WS}	{ break; }
{C}	{ break; }

[:=]    { return TMS_McmasterPrmParser::TMS_McmasterPrm_COL; }

"{"	{ return TMS_McmasterPrmParser::TMS_McmasterPrm_OB; }
"}"	{ return TMS_McmasterPrmParser::TMS_McmasterPrm_CB; }
		            
{NAME}  { return TMS_McmasterPrmParser::TMS_McmasterPrm_NAME; }
{I}	{ return TMS_McmasterPrmParser::TMS_McmasterPrm_INT; }
{R}     { return TMS_McmasterPrmParser::TMS_McmasterPrm_REAL; }

%%
