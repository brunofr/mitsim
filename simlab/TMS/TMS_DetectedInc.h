//-*-c++-*------------------------------------------------------------
// TMS_DetectedInc.h
//
// Qi Yang and Sreeram Radhakrishnan
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef TMS_DETECTEDINC_HEADER
#define TMS_DETECTEDINC_HEADER

#include <Templates/Listp.h>

#include <Xmw/XmwColor.h>
#include <GRN/WcsPoint.h>
#include <GRN/RN_SurvStation.h>

class DRN_DrawingArea;

class TMS_DetectedInc
{
protected:

  WcsPoint center_;				// position
  double angle_;                // direction
  unsigned int shownState_;		// current shown state
  bool onScreen_;				// 1 if visible
  SurvList station_;

public:

  TMS_DetectedInc(SurvList s);
  ~TMS_DetectedInc() { }

  int cmp(int c) const {		// for search and identification
	if (station_->data()->code() < c) return -1;
	else if (station_->data()->code() > c) return 1;
	else return 0;
  }
  bool eq(TMS_DetectedInc *i) const {	// for identification
	return (station_->data()->code() == i->station_->data()->code());
  }

  static void drawAllDetectIncidents(DRN_DrawingArea * area);

private:

  Pixel calcColor();
  bool isChosen();
  bool draw(DRN_DrawingArea * area);
  bool drawShape(DRN_DrawingArea * area);
};

typedef Listp<TMS_DetectedInc *> detected_inc_list;

extern detected_inc_list * theDetectedIncs;

#endif
