//-*-c++-*------------------------------------------------------------
// FILE: TMS_Segment.h
// AUTH: Qi Yang
// DATE: Wed Oct 18 17:38:54 1995
//--------------------------------------------------------------------

#include "TMS_Parameter.h"
#ifndef TMS_SEGMENT_HEADER
#define TMS_SEGMENT_HEADER

#ifdef INTERNAL_GUI
#include <DRN/DRN_Segment.h>
#else  // batch mode
#include <GRN/RN_Segment.h>
#endif // !INTERNAL_GUI

class TMS_Segment :
#ifdef INTERNAL_GUI
  public DRN_Segment
#else
  public RN_Segment
#endif
{
   public:

      TMS_Segment();
      ~TMS_Segment() { }

      float density() { return density_/theParameter->densityFactor(); }
      float speed() { return speed_/theParameter->speedFactor(); }
      int flow() { return flow_; }

      float calcDensity(void);
      float calcSpeed(void);
      int calcFlow(void);

      int interpolateStateFromNearbySensors(int max_degree);
      void updateParameters();

  private:

      float density_;
      float speed_;
      int flow_;
      short int degree_;
};

#endif
