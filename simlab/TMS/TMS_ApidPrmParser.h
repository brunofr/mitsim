#ifndef YY_TMS_ApidPrmBisonParser_h_included
#define YY_TMS_ApidPrmBisonParser_h_included

#line 1 "/usr/lib/bison.h"
/* before anything */
#ifdef c_plusplus
#ifndef __cplusplus
#define __cplusplus
#endif
#endif
#ifdef __cplusplus
#ifndef YY_USE_CLASS
#define YY_USE_CLASS
#endif
#else
#endif
#include <stdio.h>

/* #line 14 "/usr/lib/bison.h" */
#line 21 "TMS_ApidPrmParser.h"
#define YY_TMS_ApidPrmBisonParser_ERROR_BODY  =0
#define YY_TMS_ApidPrmBisonParser_LEX_BODY  =0
#define YY_TMS_ApidPrmBisonParser_MEMBERS    TMS_ApidPrmScanner pts; TMS_ApidPrmTable *ptr1; 
#define YY_TMS_ApidPrmBisonParser_CONSTRUCTOR_PARAM  const char *f
#define YY_TMS_ApidPrmBisonParser_CONSTRUCTOR_INIT  : pts(f), ptr1(NULL)

#line 22 "TMS_ApidPrmParser.y"
typedef union {
      int    itype;
      float  ftype;
      char  *stype;
} yy_TMS_ApidPrmBisonParser_stype;
#define YY_TMS_ApidPrmBisonParser_STYPE yy_TMS_ApidPrmBisonParser_stype
#line 36 "TMS_ApidPrmParser.y"

#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;

#include <IO/Exception.h>
#include <Tools/ToolKit.h>  

#include "TMS_ApidParameters.h"
#include "TMS_ApidPrmTable.h"

#undef yyFlexLexer
#define yyFlexLexer TMS_ApidPrmFlexLexer
#include <FlexLexer.h>

   class TMS_ApidPrmScanner : public TMS_ApidPrmFlexLexer
   {
      private:

	 char *name_;			// file name

      public:

	 TMS_ApidPrmScanner() : name_(NULL), TMS_ApidPrmFlexLexer() { }
	 TMS_ApidPrmScanner(const char *fname)
	    : name_(Copy(fname)),
	      TMS_ApidPrmFlexLexer() {
		 if (ToolKit::fileExists(fname)) {
		    switch_streams(new ifstream(fname), 0);
		 } else {
		    cerr << "Error:: cannot open input file <"
				 << name_ << ">" << endl;
		    theException->exit();
		 }
	 }
	 ~TMS_ApidPrmScanner() { delete [] name_; delete yyin; }

	 char* name() const { return name_; }

	 char* removeDelimeters(const char *deli="\"\"") {
	    char *s, *str = (char *)YYText();
	    if (*str == deli[0]) str ++;
	    s = str;
	    while (*s != '\0' && *s != deli[1]) s ++;
	    *s = '\0';
	    return (str);
	 }

	 char* value() { return (char*)YYText(); }

	 void errorQuit(int err) {
	    if (err == 0) return;
	    cerr << "Problem in parsing"
		 << " (" << name() << ":" << lineno() << ")" << endl;
	    if (err < 0) theException->exit();
	 }
   };

#line 14 "/usr/lib/bison.h"
 /* %{ and %header{ and %union, during decl */
#ifndef YY_TMS_ApidPrmBisonParser_COMPATIBILITY
#ifndef YY_USE_CLASS
#define  YY_TMS_ApidPrmBisonParser_COMPATIBILITY 1
#else
#define  YY_TMS_ApidPrmBisonParser_COMPATIBILITY 0
#endif
#endif

#if YY_TMS_ApidPrmBisonParser_COMPATIBILITY != 0
/* backward compatibility */
#ifdef YYLTYPE
#ifndef YY_TMS_ApidPrmBisonParser_LTYPE
#define YY_TMS_ApidPrmBisonParser_LTYPE YYLTYPE
/* WARNING obsolete !!! user defined YYLTYPE not reported into generated header */
/* use %define LTYPE */
#endif
#endif
#ifdef YYSTYPE
#ifndef YY_TMS_ApidPrmBisonParser_STYPE 
#define YY_TMS_ApidPrmBisonParser_STYPE YYSTYPE
/* WARNING obsolete !!! user defined YYSTYPE not reported into generated header */
/* use %define STYPE */
#endif
#endif
#ifdef YYDEBUG
#ifndef YY_TMS_ApidPrmBisonParser_DEBUG
#define  YY_TMS_ApidPrmBisonParser_DEBUG YYDEBUG
/* WARNING obsolete !!! user defined YYDEBUG not reported into generated header */
/* use %define DEBUG */
#endif
#endif
#ifdef YY_TMS_ApidPrmBisonParser_STYPE
#ifndef yystype
#define yystype YY_TMS_ApidPrmBisonParser_STYPE
#endif
#endif
/* use goto to be compatible */
#ifndef YY_TMS_ApidPrmBisonParser_USE_GOTO
#define YY_TMS_ApidPrmBisonParser_USE_GOTO 1
#endif
#endif

/* use no goto to be clean in C++ */
#ifndef YY_TMS_ApidPrmBisonParser_USE_GOTO
#define YY_TMS_ApidPrmBisonParser_USE_GOTO 0
#endif

#ifndef YY_TMS_ApidPrmBisonParser_PURE

/* #line 63 "/usr/lib/bison.h" */
#line 148 "TMS_ApidPrmParser.h"

#line 63 "/usr/lib/bison.h"
/* YY_TMS_ApidPrmBisonParser_PURE */
#endif

/* #line 65 "/usr/lib/bison.h" */
#line 155 "TMS_ApidPrmParser.h"

#line 65 "/usr/lib/bison.h"
/* prefix */
#ifndef YY_TMS_ApidPrmBisonParser_DEBUG

/* #line 67 "/usr/lib/bison.h" */
#line 162 "TMS_ApidPrmParser.h"

#line 67 "/usr/lib/bison.h"
/* YY_TMS_ApidPrmBisonParser_DEBUG */
#endif
#ifndef YY_TMS_ApidPrmBisonParser_LSP_NEEDED

/* #line 70 "/usr/lib/bison.h" */
#line 170 "TMS_ApidPrmParser.h"

#line 70 "/usr/lib/bison.h"
 /* YY_TMS_ApidPrmBisonParser_LSP_NEEDED*/
#endif
/* DEFAULT LTYPE*/
#ifdef YY_TMS_ApidPrmBisonParser_LSP_NEEDED
#ifndef YY_TMS_ApidPrmBisonParser_LTYPE
typedef
  struct yyltype
    {
      int timestamp;
      int first_line;
      int first_column;
      int last_line;
      int last_column;
      char *text;
   }
  yyltype;

#define YY_TMS_ApidPrmBisonParser_LTYPE yyltype
#endif
#endif
/* DEFAULT STYPE*/
#ifndef YY_TMS_ApidPrmBisonParser_STYPE
#define YY_TMS_ApidPrmBisonParser_STYPE int
#endif
/* DEFAULT MISCELANEOUS */
#ifndef YY_TMS_ApidPrmBisonParser_PARSE
#define YY_TMS_ApidPrmBisonParser_PARSE yyparse
#endif
#ifndef YY_TMS_ApidPrmBisonParser_LEX
#define YY_TMS_ApidPrmBisonParser_LEX yylex
#endif
#ifndef YY_TMS_ApidPrmBisonParser_LVAL
#define YY_TMS_ApidPrmBisonParser_LVAL yylval
#endif
#ifndef YY_TMS_ApidPrmBisonParser_LLOC
#define YY_TMS_ApidPrmBisonParser_LLOC yylloc
#endif
#ifndef YY_TMS_ApidPrmBisonParser_CHAR
#define YY_TMS_ApidPrmBisonParser_CHAR yychar
#endif
#ifndef YY_TMS_ApidPrmBisonParser_NERRS
#define YY_TMS_ApidPrmBisonParser_NERRS yynerrs
#endif
#ifndef YY_TMS_ApidPrmBisonParser_DEBUG_FLAG
#define YY_TMS_ApidPrmBisonParser_DEBUG_FLAG yydebug
#endif
#ifndef YY_TMS_ApidPrmBisonParser_ERROR
#define YY_TMS_ApidPrmBisonParser_ERROR yyerror
#endif

#ifndef YY_TMS_ApidPrmBisonParser_PARSE_PARAM
#ifndef __STDC__
#ifndef __cplusplus
#ifndef YY_USE_CLASS
#define YY_TMS_ApidPrmBisonParser_PARSE_PARAM
#ifndef YY_TMS_ApidPrmBisonParser_PARSE_PARAM_DEF
#define YY_TMS_ApidPrmBisonParser_PARSE_PARAM_DEF
#endif
#endif
#endif
#endif
#ifndef YY_TMS_ApidPrmBisonParser_PARSE_PARAM
#define YY_TMS_ApidPrmBisonParser_PARSE_PARAM void
#endif
#endif

/* TOKEN C */
#ifndef YY_USE_CLASS

#ifndef YY_TMS_ApidPrmBisonParser_PURE
extern YY_TMS_ApidPrmBisonParser_STYPE YY_TMS_ApidPrmBisonParser_LVAL;
#endif


/* #line 143 "/usr/lib/bison.h" */
#line 248 "TMS_ApidPrmParser.h"
#define	TMS_ApidPrm_COL	258
#define	TMS_ApidPrm_OB	259
#define	TMS_ApidPrm_CB	260
#define	TMS_ApidPrm_INT	261
#define	TMS_ApidPrm_REAL	262
#define	TMS_ApidPrm_STRING	263
#define	TMS_ApidPrm_NAME	264


#line 143 "/usr/lib/bison.h"
 /* #defines token */
/* after #define tokens, before const tokens S5*/
#else
#ifndef YY_TMS_ApidPrmBisonParser_CLASS
#define YY_TMS_ApidPrmBisonParser_CLASS TMS_ApidPrmBisonParser
#endif

#ifndef YY_TMS_ApidPrmBisonParser_INHERIT
#define YY_TMS_ApidPrmBisonParser_INHERIT
#endif
#ifndef YY_TMS_ApidPrmBisonParser_MEMBERS
#define YY_TMS_ApidPrmBisonParser_MEMBERS 
#endif
#ifndef YY_TMS_ApidPrmBisonParser_LEX_BODY
#define YY_TMS_ApidPrmBisonParser_LEX_BODY  
#endif
#ifndef YY_TMS_ApidPrmBisonParser_ERROR_BODY
#define YY_TMS_ApidPrmBisonParser_ERROR_BODY  
#endif
#ifndef YY_TMS_ApidPrmBisonParser_CONSTRUCTOR_PARAM
#define YY_TMS_ApidPrmBisonParser_CONSTRUCTOR_PARAM
#endif
/* choose between enum and const */
#ifndef YY_TMS_ApidPrmBisonParser_USE_CONST_TOKEN
#define YY_TMS_ApidPrmBisonParser_USE_CONST_TOKEN 0
/* yes enum is more compatible with flex,  */
/* so by default we use it */ 
#endif
#if YY_TMS_ApidPrmBisonParser_USE_CONST_TOKEN != 0
#ifndef YY_TMS_ApidPrmBisonParser_ENUM_TOKEN
#define YY_TMS_ApidPrmBisonParser_ENUM_TOKEN yy_TMS_ApidPrmBisonParser_enum_token
#endif
#endif

class YY_TMS_ApidPrmBisonParser_CLASS YY_TMS_ApidPrmBisonParser_INHERIT
{
public: 
#if YY_TMS_ApidPrmBisonParser_USE_CONST_TOKEN != 0
/* static const int token ... */

/* #line 182 "/usr/lib/bison.h" */
#line 300 "TMS_ApidPrmParser.h"
static const int TMS_ApidPrm_COL;
static const int TMS_ApidPrm_OB;
static const int TMS_ApidPrm_CB;
static const int TMS_ApidPrm_INT;
static const int TMS_ApidPrm_REAL;
static const int TMS_ApidPrm_STRING;
static const int TMS_ApidPrm_NAME;


#line 182 "/usr/lib/bison.h"
 /* decl const */
#else
enum YY_TMS_ApidPrmBisonParser_ENUM_TOKEN { YY_TMS_ApidPrmBisonParser_NULL_TOKEN=0

/* #line 185 "/usr/lib/bison.h" */
#line 316 "TMS_ApidPrmParser.h"
	,TMS_ApidPrm_COL=258
	,TMS_ApidPrm_OB=259
	,TMS_ApidPrm_CB=260
	,TMS_ApidPrm_INT=261
	,TMS_ApidPrm_REAL=262
	,TMS_ApidPrm_STRING=263
	,TMS_ApidPrm_NAME=264


#line 185 "/usr/lib/bison.h"
 /* enum token */
     }; /* end of enum declaration */
#endif
public:
 int YY_TMS_ApidPrmBisonParser_PARSE(YY_TMS_ApidPrmBisonParser_PARSE_PARAM);
 virtual void YY_TMS_ApidPrmBisonParser_ERROR(char *msg) YY_TMS_ApidPrmBisonParser_ERROR_BODY;
#ifdef YY_TMS_ApidPrmBisonParser_PURE
#ifdef YY_TMS_ApidPrmBisonParser_LSP_NEEDED
 virtual int  YY_TMS_ApidPrmBisonParser_LEX(YY_TMS_ApidPrmBisonParser_STYPE *YY_TMS_ApidPrmBisonParser_LVAL,YY_TMS_ApidPrmBisonParser_LTYPE *YY_TMS_ApidPrmBisonParser_LLOC) YY_TMS_ApidPrmBisonParser_LEX_BODY;
#else
 virtual int  YY_TMS_ApidPrmBisonParser_LEX(YY_TMS_ApidPrmBisonParser_STYPE *YY_TMS_ApidPrmBisonParser_LVAL) YY_TMS_ApidPrmBisonParser_LEX_BODY;
#endif
#else
 virtual int YY_TMS_ApidPrmBisonParser_LEX() YY_TMS_ApidPrmBisonParser_LEX_BODY;
 YY_TMS_ApidPrmBisonParser_STYPE YY_TMS_ApidPrmBisonParser_LVAL;
#ifdef YY_TMS_ApidPrmBisonParser_LSP_NEEDED
 YY_TMS_ApidPrmBisonParser_LTYPE YY_TMS_ApidPrmBisonParser_LLOC;
#endif
 int YY_TMS_ApidPrmBisonParser_NERRS;
 int YY_TMS_ApidPrmBisonParser_CHAR;
#endif
#if YY_TMS_ApidPrmBisonParser_DEBUG != 0
public:
 int YY_TMS_ApidPrmBisonParser_DEBUG_FLAG;	/*  nonzero means print parse trace	*/
#endif
public:
 YY_TMS_ApidPrmBisonParser_CLASS(YY_TMS_ApidPrmBisonParser_CONSTRUCTOR_PARAM);
public:
 YY_TMS_ApidPrmBisonParser_MEMBERS 
};
/* other declare folow */
#endif


#if YY_TMS_ApidPrmBisonParser_COMPATIBILITY != 0
/* backward compatibility */
#ifndef YYSTYPE
#define YYSTYPE YY_TMS_ApidPrmBisonParser_STYPE
#endif

#ifndef YYLTYPE
#define YYLTYPE YY_TMS_ApidPrmBisonParser_LTYPE
#endif
#ifndef YYDEBUG
#ifdef YY_TMS_ApidPrmBisonParser_DEBUG 
#define YYDEBUG YY_TMS_ApidPrmBisonParser_DEBUG
#endif
#endif

#endif
/* END */

/* #line 236 "/usr/lib/bison.h" */
#line 380 "TMS_ApidPrmParser.h"

#line 333 "TMS_ApidPrmParser.y"


   class TMS_ApidPrmParser : public TMS_ApidPrmBisonParser
   {
      public:
	 TMS_ApidPrmParser(const char *fname)
	    : TMS_ApidPrmBisonParser(fname) { }
	 ~TMS_ApidPrmParser() { }

	 char* name() const { return pts.name(); }

	 void yyerror(char *msg) {
	    cerr << "\nError:: " << msg
			 << " (" << name() << ":"
			 << pts.lineno() << ")" << endl;
	    theException->exit();
	 }

	 int yylex() { return pts.yylex(); }

	 void parse(TMS_ApidPrmTable *pt){
	   cout << "Parsing Sensor Parameter file<";
	   cout << name() << "> ..." << endl;;
	   ptr1 = pt;
	   yyparse();
	 }
   };
#endif
