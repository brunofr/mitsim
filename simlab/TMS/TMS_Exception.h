//-*-c++-*------------------------------------------------------------
// NAME: Exception handler
// AUTH: Qi Yang
// FILE: TMS_Exception.h
// DATE: Sun Mar 17 14:36:47 1996
//--------------------------------------------------------------------

#ifndef TMS_EXCEPTION_HEADER
#define TMS_EXCEPTION_HEADER

#include <IO/Exception.h>

class TMS_Exception : public Exception
{
   public:

      TMS_Exception(const char *name) : Exception(name) { }
      ~TMS_Exception() { }

      void exit(int code = 0);
      void done(int code = 0);
};

#endif
