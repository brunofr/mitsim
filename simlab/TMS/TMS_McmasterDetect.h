//-*-c++-*------------------------------------------------------------
// TMS_McmasterDetect.h
//
// Sreeram Radhakrishnan
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef TMS_MCMASTERDETECT_HEADER
#define TMS_MCMASTERDETECT_HEADER

#include "TMS_SurvStation.h"
#include "TMS_IncidentDetect.h"

const int RESET_ALL_COUNT      = 0;
const int INCR_DCR_COUNT       = 1;
const int INCR_CLR_COUNT       = 1;
const int RESET_DCR_COUNT      = 2;
const int RESET_CLR_COUNT      = 2;
const int RECURRENT_CONGESTION = 3;

class TMS_McmasterDetector: public TMS_IncidentDetector
{
   public:

      TMS_McmasterDetector(): TMS_IncidentDetector() 
        {}	  

      ~TMS_McmasterDetector() 
	    {}

      void incidentDetect(int);
      void checkforIncident(SurvList );
      void checkforClearence(SurvList );

      void clearallstationsahead(SurvList );
      int fixstationstate(SurvList );

      int incidentCouldExist(SurvList ,RN_Sensor *,RN_Sensor *);
      int incidentIsOver(SurvList ,RN_Sensor *,RN_Sensor *);

};

#endif
