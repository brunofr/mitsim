//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_LusAction.C
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#include <sstream>
using namespace std;

#include <Tools/Reader.h>
#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>

#include <Tools/Math.h>

#include <GRN/RN_CtrlStation.h>

#include "TMS_Parameter.h"
#include "TMS_LusAction.h"
#include "TMS_CtrlLogic.h"
#include "TMS_Incident.h"
#include "TMS_Link.h"
#include "TMS_Segment.h"
#include "TMS_Lane.h"
#include "TMS_Signal.h"

void
TMS_LusAction::read(Reader& is)
{
	static int nactions = 0;
	code_ = ++ nactions;

	is >> situation_;
	state_ = is.gethex();
	is >> fromRef_ >> fromDistance_;
	is >> toRef_ >> toDistance_ ;

	fromDistance_ *= theParameter->lengthFactor();
	toDistance_ *= theParameter->lengthFactor();
}

void
TMS_LusAction::print(ostream &os)
{
	os << indent
	   << situation_ << endc
	   << hextag << hex << state_ << endc
	   << fromRef_ << endc
	   << fromDistance_ / theParameter->lengthFactor() << endc
	   << toRef_ << endc
	   << toDistance_ / theParameter->lengthFactor() << endl;
}


/*
 *------------------------------------------------------------------
 * Update the LUS signs based on this TMS_LusAction rule.
 *
 * CAUTION: Only are the signs in the current link and the upstream
 * and downstream links with depth of 1 is searched.
 *
 *------------------------------------------------------------------
 */

void
TMS_LusAction::applyTo(TMS_Incident& inc)
{
	TMS_Link* link = (TMS_Link*)inc.link();
	CtrlList ctrl;
	double start_pos, stop_pos;
	unsigned int signature = 1 << inc.lane()->localIndex();
  
	switch (fromRef_) 
    {
    case INCIDENT_POSITION:
	{
		start_pos = inc.distance();
		break;
	}
    case PORTAL_POSITION:
	{
		ctrl = link->searchTypedCtrl(
			inc.distance(),
			theCtrlLogic->searchingDistUpbound(),
			CTRL_PS);
		if (ctrl) start_pos = (*ctrl)->distance();
		else {
			if (ToolKit::debug()) {
				cout << "No UpPortal signal found. LUS command ";
				cout << code_ << " ignored." << endl;
			}
			return;
		}
		break;
	}
    default:
	{
		cerr << "Error:: Unknown fromRef <" << fromRef_
			 << ">. LUS command "
			 << code_ << " ignored." << endl;
		return;
	}
    }

	start_pos += fromDistance_;

	switch (toRef_) {
	case INCIDENT_POSITION:
	{
		stop_pos = inc.distance();
		break;
	}
	case PORTAL_POSITION:
	{
		ctrl = link->searchTypedCtrl(
			inc.distance(),
			theCtrlLogic->searchingDistUpbound(),
			CTRL_PS);
		if (ctrl) stop_pos = (*ctrl)->distance();
		else
	    {
			if (ToolKit::debug()) {
				cout << "No UpPortal signal found. ";
				cout << "LUS command " << code_
					 << " applied within "
					 << theCtrlLogic->searchingDistUpbound()/
					theParameter->lengthFactor()
					 << " upstream." << endl;
			}
			stop_pos = inc.distance() +
				theCtrlLogic->searchingDistUpbound();
	    }
		break;
	}
	default:
	{
		cerr << "Error:: Unknown toRef <" << toRef_
			 << ">. LUS command " << code_ << " ignored." << endl;
		return;
	}
	}

	stop_pos += toDistance_;

	RN_Signal *lus;
	const int UPSTREAM   = 1;
	const int DOWNSTREAM = -1;
	int walk;
	double start_dis, stop_dis;

	if (start_pos < stop_pos)	/* walk upstream */
	{
		walk = UPSTREAM;
		start_dis = Max(start_pos, 0.0);
		stop_dis = Min(stop_pos, link->length());
		ctrl = link->prevCtrl(start_dis);
	} else			/* walk downstream */
	{
		walk = DOWNSTREAM;
		start_dis = Min(start_pos, link->length());
		stop_dis = Max(stop_pos, 0.0);
		ctrl = link->nextCtrl(start_dis);
	}

	int counts = 0;
	ostringstream os ;

	/* walking from "start_dis" until "stop_dis" Set all the LUSs in
	 * the marked lane to the desired state.  */

	while (ctrl != NULL &&
		   (walk == UPSTREAM && (*ctrl)->distance() <= stop_dis ||
			walk == DOWNSTREAM && (*ctrl)->distance() >= stop_dis))
	{
		if ((*ctrl)->type() == CTRL_LUS)
		{
			RN_Segment* ps = (*ctrl)->segment();
			int n = ps->nLanes();
			for (register int i = 0; i < n; i ++) {
				if (ps->lane(i)->isConnected(signature) &&
					(lus = (*ctrl)->signal(i)) != NULL)
				{
					lus->state(state_);
					counts ++;
					os << " " << lus->code();
				}
			}
		}
		ctrl = (walk == UPSTREAM) ? ctrl->prev() : ctrl->next();
	}

	/* If the control range goes beyond upstream end of the current
	 * link, continute to set the signs in the upstream links.  */

	if (walk == UPSTREAM) {
		start_dis = 0.0;
		stop_dis = stop_pos - link->length();
	} else {
		start_dis = start_pos - link->length();
		stop_dis = 0.0;
	}
	if (walk == UPSTREAM && stop_dis > 0.0 ||
		walk == DOWNSTREAM && start_pos > 0.0) {
		int narcs = link->nUpLinks();
		for (int k = 0; k < narcs; k ++) {
			if (walk == UPSTREAM) {
				ctrl = link->upLink(k)->lastCtrl();
			} else {
				ctrl = link->upLink(k)->nextCtrl(start_dis);
			}
			while (ctrl != NULL &&
				   (walk == UPSTREAM && (*ctrl)->distance() <= stop_dis ||
					walk == DOWNSTREAM && (*ctrl)->distance() >= stop_dis) ) {
				if ((*ctrl)->type() == CTRL_LUS)
				{
					RN_Segment* ps = (*ctrl)->segment();
					int n = ps->nLanes();
					for (register int i = 0; i < n; i ++)
					{
						if (ps->lane(i)->isConnected(signature) &&
							(lus = (*ctrl)->signal(i)) != NULL)
						{
							lus->state(state_);
							counts ++;
							os << " " << lus->code();
						}
					}
				}
				ctrl = (walk == UPSTREAM) ? ctrl->prev() : ctrl->next();
			}
		}
	}

	/* If the control range goes beyond downstream end of the current
	 * link, continute to set the signs in the downstream links.  */

	if (walk == UPSTREAM && start_pos < 0.0 ||
		walk == DOWNSTREAM && stop_pos < 0.0) {
		int narcs = link->nDnLinks();
		for (int k = 0; k < narcs; k ++) {
			RN_Link* plink = link->dnLink(k);
			if (walk == UPSTREAM) {
				stop_dis = plink->length();
				start_dis = start_pos + stop_dis;
				ctrl = plink->prevCtrl(start_dis);
			} else {
				start_dis = plink->length();
				stop_dis = stop_pos + start_dis;
				ctrl = plink->ctrlList(); /* first ctrl */
			}
			while (ctrl != NULL &&
				   (walk == UPSTREAM && (*ctrl)->distance() <= stop_dis ||
					walk == DOWNSTREAM && (*ctrl)->distance() >= stop_dis)) {
				if ((*ctrl)->type() == CTRL_LUS) {
					RN_Segment* ps = (*ctrl)->segment();
					int n = ps->nLanes();
					for (int i = 0; i < n; i ++) {
						if (ps->lane(i)->isConnected(signature) &&
							(lus = (*ctrl)->signal(i)) != NULL) {
							lus->state(state_);
							counts ++;
							os << " " << lus->code();
						}
					}
				}
				ctrl = (walk == UPSTREAM) ? ctrl->prev() : ctrl->next();
			}
		}
	}
	if (ToolKit::verbose() && counts > 0) {
		//os << '\0' ;
		cout << "LUS command " << parent_->code()
			 << "/" << code_ << " applied to " << counts
			 << " signs (" << os.str() << " ) at "
			 << theSimulationClock->currentStringTime() << endl; 
	}

}
