//-*-c++-*------------------------------------------------------------
// TMS_Sensor.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <Tools/Math.h>

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>
#include <Tools/GenericVariable.h>
#include <Tools/Reader.h>

#include "TMS_Network.h"
#include "TMS_Link.h"
#include "TMS_Segment.h"
#include "TMS_Lane.h"
#include "TMS_Sensor.h"
#include "TMS_Engine.h"
#include "TMS_Parameter.h"
#include "TMS_FileManager.h"
#include "TMS_Communicator.h"
#include "TMS_McmasterParameters.h"
#include "TMS_McmasterPrmTable.h"
#include <iostream>
using namespace std;

long TMS_Sensor::startTime_ = 0;
long TMS_Sensor::endTime_   = 0;

double TMS_Sensor::dbStartTime_  = 0;
double TMS_Sensor::dbStepSize_   = 300;
double TMS_Sensor::dbEndTime_    = 0;
int    TMS_Sensor::dbNumPeriods_ = 0;
char * TMS_Sensor::dbSpeedFile_  = NULL;
float  TMS_Sensor::dbSpeedScaler_= 4.0;
char * TMS_Sensor::dbFlowFile_   = NULL;
float  TMS_Sensor::dbFlowScaler_ = 1.0;


// Static function


void TMS_Sensor::dbSpeedFile(const char *s)
{
   if (dbSpeedFile_) free(dbSpeedFile_);
   dbSpeedFile_ = strdup(s);
};

void TMS_Sensor::dbFlowFile(const char *s)
{
   if (dbFlowFile_) free(dbFlowFile_);
   dbFlowFile_ = strdup(s);
}

void TMS_Sensor::calcDbEndTime()
{
   dbEndTime_ = dbStartTime_ + dbStepSize_ * dbNumPeriods_;
}

int TMS_Sensor::loadDatabaseSetting(GenericVariable& gv)
{
   int n = gv.nElements();
   if (n >= 3) {
	  dbStartTime_ = gv.element(0);
	  dbStepSize_ = gv.element(1);
	  dbNumPeriods_ = gv.element(2);
   }
   if (n >= 5) {
	  dbSpeedFile_ = Copy((const char *)gv.element(3));
	  dbSpeedScaler_ = gv.element(4);
   }
   if (n >= 7) {
	  dbFlowFile_ = Copy((const char *)gv.element(5));
	  dbFlowScaler_ = gv.element(6);
   }
   return 0;
}

// Static function

void TMS_Sensor::loadDatabases()
{
   // Load flow

   if (ToolKit::isValidInputFilename(dbFlowFile_)) {

      Reader is(ToolKit::infile(dbFlowFile_));
      cout << endl << "Loading sensor traffic count database <"
		   << is.name() << "> ..." << endl;
      int id, ext;
      RN_Sensor *s;

      for (is.eatwhite(); !is.eof() && is.good(); is.eatwhite()) {
		 is >> id >> ext;

		 if (s = theNetwork->findSensor(id)) {

			if (ext) {		// not used
			   skipRecord(is);

			} else if (s->tasks() & SENSOR_AGGREGATE) {
			   s->loadDbFlow(is);

			} else {

			   skipRecord(is);

			   if (ToolKit::verbose()) {
				  cerr << "Warning:: Sensor " << id << "." << ext
					   << " is not a traffic sensor. ";
				  is.reference();
			   }
			}

		 } else {

			skipRecord(is);

			if (ToolKit::verbose()) {
			   cerr << "Warning:: Sensor " << id << "." << ext
					<< " not found. ";
			   is.reference();
			}
		 }
      }
      is.close();
   }

   // Load Speed

   if (ToolKit::isValidInputFilename(dbSpeedFile_)) {

      Reader is(ToolKit::infile(dbSpeedFile_));

      cout << endl << "Loading sensor speed database <"
		   << is.name() << "> ..." << endl;

      int id, ext;
      RN_Sensor *s;

      for (is.eatwhite(); !is.eof() && is.good(); is.eatwhite()) {
		 is >> id >> ext;

		 if (s = theNetwork->findSensor(id)) {

			if (ext) {		// not used
			   skipRecord(is);

			} else if (s->tasks() & SENSOR_AGGREGATE) {
			   s->loadDbSpeed(is);

			} else {

			   skipRecord(is);

			   if (ToolKit::verbose()) {
				  cerr << "Warning:: Sensor " << id << "." << ext
					   << " is not a traffic sensor. ";
				  is.reference();
			   }
			}
		 } else {

			skipRecord(is);

			if (ToolKit::verbose()) {
			   cerr << "Warning:: Sensor " << id << "." << ext
					<< " not found. ";
			   is.reference();
			}
		 }
      }
      is.close();
   }
}


/*
 *--------------------------------------------------------------------
 * Point data traffic sensors -- accumulated and reported at a fixed
 * time intervals
 *--------------------------------------------------------------------
 */

TMS_Sensor::TMS_Sensor()
   : TC_Sensor(),
     dbFlow_(NULL),
     dbSpeed_(NULL)
{
	 occPrev_[0] = occPrev_[1] = spdPrev_[0] = spdPrev_[1] = 0;
}


int TMS_Sensor::dbPeriod(double t)
{
   if (t < dbStartTime_) return 0;
   else if (t >= dbEndTime_) return dbNumPeriods_ - 1;
   else {
      float i = (t - dbStartTime_) / dbStepSize_;
      return (int) i;
   }
}


void TMS_Sensor::skipRecord(Reader& is)
{
   float x;
   for (register int i = 0; i < dbNumPeriods_; i ++) {
      is >> x;
   }
}

void TMS_Sensor::loadDbFlow(Reader& is)
{
   float x;
   dbFlow_ = new vector<int ALLOCATOR>(dbNumPeriods_);
   for (register int i = 0; i < dbNumPeriods_; i ++) {
      is >> x;
      (*dbFlow_)[i] = (int)(x * dbFlowScaler_ + 0.5);
   }
}

void TMS_Sensor::loadDbSpeed(Reader& is)
{
   float x;
   dbSpeed_ = new vector<float ALLOCATOR>(dbNumPeriods_);
   for (register int i = 0; i < dbNumPeriods_; i ++) {
      is >> x;
      (*dbSpeed_)[i] = x * dbSpeedScaler_;
   }
}


// Calculate the count since data was transmitted (based on received
// flow rate)

int TMS_Sensor::count()		// virtual
{
   float dt = endTime_ - startTime_;
   float x = flow() * dt / 3600.0;
   return (int) (x + 0.5);
}


// Taffic flow in vehicles/lane/hour

int TMS_Sensor::flow()		// virtual
{
   if (theCommunicator->isSimlabConnected()) {
      return TC_Sensor::flow();
   } else if (dbFlow_) {
      int i = dbPeriod(theSimulationClock->currentTime());
      return dbFlow(i);
   } else {
      return 0;
   }
}


// Average speed in meter/second

float TMS_Sensor::speed()		// virtual
{
   if (theCommunicator->isSimlabConnected()) {
      return TC_Sensor::speed();
   } else if (dbSpeed_) {
      int i = dbPeriod(theSimulationClock->currentTime());
      return dbSpeed(i);
   } else {
      return segment()->freeSpeed();
   }
}

float TMS_Sensor::occPrev(int i) 
{ 
  return occPrev_[i]; 
}

void TMS_Sensor::setOccPrev(int i, float f) 
{ 
  occPrev_[i] = f;
}

float TMS_Sensor::spdPrev(int i) 
{ 
  return spdPrev_[i]; 
}

void TMS_Sensor::setSpdPrev(int i, float f) 
{ 
  spdPrev_[i] = f; 
}

int TMS_Sensor::getRegion(double timestep)
{
   double flowCalculated, flow, flowcrit;

   int prmGrpID = station()->prmGrpID();

   TMS_McmasterParameters *p = theMcmasterPrmTable->prmGrps(prmGrpID-1);

   flow = count_*3600/timestep;

   flowcrit = p->getvCrit()*120;

   if ((flow < 0)||(occupancy_ <= 0))
	 return -1;

   if (occupancy_ > p->getocMax()) {
	 if (flow > flowcrit)
	   return INC_DETECTION_RECURRENT;
	 else
	   return INC_DETECTION_STATUS_UP;	   
   } else {
	 flowCalculated = p->calculateFlow(occupancy_) * 120;

	 if(flowCalculated < 0) return -1;
	 else return ((flow > flowCalculated) ? INC_DETECTION_STATUS_NONE : INC_DETECTION_STATUS_DN) ;
   }
}
