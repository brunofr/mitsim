//-*-c++-*------------------------------------------------------------
// NAME: Command line arguments parser for TMS
// AUTH: Qi Yang
// FILE: TMS_CmdArgsParser.h
// DATE: Tue Nov 21 17:58:27 1995
//--------------------------------------------------------------------

#ifndef TMS_CMDARGSPARSER_HEADER
#define TMS_CMDARGSPARSER_HEADER

// This class register additional command line arguments for TMS

#include <Tools/CmdArgsParser.h>

class TMS_CmdArgsParser : public CmdArgsParser
{
   public:
      
      TMS_CmdArgsParser();
      ~TMS_CmdArgsParser() { }
};

#endif
