#ifndef MCMASTERPARAMETERS_HEADER
#define MCMASTERPARAMETERS_HEADER

#include <cstdio>
#include <vector>
#include <iosfwd>

class TMS_McmasterParameters
{
private:

  double ocMax_;
  double avSpeed_;
  double vCrit_;
  int prmGrpID_;
  std::vector<double> polyCoeffs_;
  std::vector<int> sensorList_;

public:

  TMS_McmasterParameters()
	{
	  avSpeed_ = ocMax_ = 0;
	  prmGrpID_ = -99;
	};
  ~TMS_McmasterParameters() 
	{};

  void assignpolyCoeffs(int,double,double,double,double,double);
  void assignprmGrpID(int);
  void assignocMax(double f);
  void assignavSpeed(double f);
  void assignvCrit(double f);
  void print(std::ostream&,int);
  void assignlistdim(int);
  void addsensorid(int);
  double calculateFlow(double);
  std::vector<int >* sensorListptr() { return &sensorList_; }
  double getocMax() { return ocMax_; }
  double getavSpeed() { return avSpeed_; }
  double getvCrit() { return vCrit_; }
  std::vector<double>  *getpolyCoeffsptr() { return &polyCoeffs_; }
  int prmGrpID() { return prmGrpID_; }
};

#endif
