//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_CtrlLogic.C
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#include <new>
using namespace std;

#include <Tools/Reader.h>
#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>

#include <IO/Exception.h>

#include "TMS_Link.h"
#include "TMS_Parameter.h"
#include "TMS_Engine.h"
#include "TMS_CtrlLogic.h"

TMS_CtrlLogic *theCtrlLogic = NULL;
char * TMS_CtrlLogic::name_ = NULL;


/*
 *------------------------------------------------------------------
 * Read specs of control logic from database
 *------------------------------------------------------------------
 */

void
TMS_CtrlLogic::load()
{
   if (!ToolKit::isValidInputFilename(name_)) {
      cout << "No incident response logic." << endl;
      return;
   }

   Reader is(ToolKit::optionalInputFile(name_));

   cout << "Loading incident response logic <"
	<< is.name() << "> ..." << endl; 

   theCtrlLogic = new TMS_CtrlLogic;

   theCtrlLogic->readParameters(is);
   theCtrlLogic->readLusResponseTable(is);
   theCtrlLogic->readVslsResponseTable(is);
   theCtrlLogic->readPsResponseTable(is);

   is.close();
}


void
TMS_CtrlLogic::readParameters(Reader& is)
{
   if (!is.findToken(OPEN_TOKEN)) theException->exit(1);

   /* Delay for incident detection (in seconds) */

   is >> detectionDelay_;
   is >> speedLimitLowerBound_;
   is >> searchingDistUpbound_;

   searchingDistUpbound_ *= theParameter->lengthFactor();

   if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
}


void TMS_CtrlLogic::readLusResponseTable(Reader& is )
{
   if (ToolKit::verbose()) {
      cout << indent << "Loading lane use signs logic ..." << endl;
   }
   int i, num;
   is >> num;			/* number of phases */
   lusPhases_.reserve(num);
   for (i = 0; i < num; i ++) {
	  lusPhases_.push_back(new TMS_ResponsePhase);
      lusPhase(i)->read(CTRL_LUS, is);
      if (ToolKit::debug()) lusPhase(i)->print();
      else if (ToolKit::verbose()) {
		 cout << indent << indent << "Phase ";
		 cout.width(2); cout << (i + 1) << ": ";
		 cout.width(2); cout << num;
		 cout << " commands loaded." << endl;
      }
   }
}


void TMS_CtrlLogic::readVslsResponseTable(Reader& is)
{
   if (ToolKit::verbose()) {
      cout << indent
	   << "Loading variable speed limit sign logic ..." << endl;
   }
   int i, num;

   is >> num;			/* number of phases */
   vslsPhases_.reserve(num);

   for (i = 0; i < num; i ++) {
      vslsPhases_.push_back(new TMS_ResponsePhase);
	  vslsPhase(i)->read(CTRL_VSLS, is);
      if (ToolKit::debug()) vslsPhase(i)->print();
      else if (ToolKit::verbose()) {
		 cout << indent << indent << "Phase ";
		 cout.width(2); cout << (i + 1) << ": ";
		 cout.width(2); cout << num;
		 cout << " commands loaded." << endl;
      }
   }
}


void TMS_CtrlLogic::readPsResponseTable(Reader& is)
{	
   if (ToolKit::verbose()) {
      cout << indent << "Loading portal closure logic ..." << endl;
   }
   int i, num;

   is >> num;			/* number of phases */
   psPhases_.reserve(num);

   for (i = 0; i < num; i ++) {
      psPhases_.push_back(new TMS_ResponsePhase);
	  psPhase(i)->read(CTRL_PS, is);
      if (ToolKit::debug()) psPhase(i)->print();
      else if (ToolKit::verbose()) {
		 cout << indent << indent << "Phase ";
		 cout.width(2); cout << (i + 1) << ": ";
		 cout.width(2); cout << num;
		 cout << " commands loaded." << endl;
      }
   }
}
