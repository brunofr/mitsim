//-*-c++-*------------------------------------------------------------
// TMS_Modeline.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef TMS_MODELINE_HEADER
#define TMS_MODELINE_HEADER

#include <DRN/DRN_Modeline.h>

class TMS_Modeline : public DRN_Modeline
{
   public:

	  TMS_Modeline(Widget parent);
	  ~TMS_Modeline() { }

	  void updateCounter(int i, int n);
	  void updateCounter(int i, const String msg);

   protected:
	  
	  Widget counters_;
};

#endif
