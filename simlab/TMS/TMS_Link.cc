//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_Link.C
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#include "TMS_Link.h"

#include <GRN/RN_SurvStation.h>
#include <GRN/RN_CtrlStation.h>

/*
 * Search the first "CtrlList" between "startDistance" and
 * "startDistance + maxDistance". If the optional arg "ctrl" is not
 * NULL, it start the search from the device "ctrl".
 */

CtrlList
TMS_Link::searchCtrl(double startDistance,
					 double maxDistance, CtrlList ctrl)
{
  if (maxDistance > 0)		/* search upstream */
	{
      if (ctrl) ctrl = ctrl->prev();
      else ctrl = this->prevCtrl(startDistance);
      if (ctrl &&
		  (*ctrl)->distance() - startDistance < maxDistance)
		{
		  return (ctrl);
		} else
		  {
			return (NULL);
		  }
	} else						/* search downstream */
	  {
		if (ctrl) ctrl = ctrl->next();
		else ctrl = this->nextCtrl(startDistance);
		if (ctrl &&
			(*ctrl)->distance() > startDistance + maxDistance)
		  {
			return (ctrl);
		  } else
			{
			  return (NULL);
			}
	  }
}

/*
 *------------------------------------------------------------------
 * Search the first "CtrlList" of type "deviceType" between
 * "startDistance" and "startDistance + maxDistance". If the
 * optional "ctrl" is not NULL, it start the search from the
 * device "ctrl".
 *------------------------------------------------------------------
 */

CtrlList
TMS_Link::searchTypedCtrl(double startDistance,
						  double maxDistance,
						  int deviceType,
						  CtrlList ctrl)
{
  if (maxDistance > 0)		/* search upstream */
	{
      if (ctrl) ctrl = ctrl->prev();
      else ctrl = this->prevCtrl(startDistance);
      while (ctrl &&		/* more devices exist */
			 (*ctrl)->type() != deviceType && /* wanted not found */
			 (*ctrl)->distance() - startDistance < maxDistance)
		{
		  ctrl = ctrl->prev();
		}
	} else			/* search downstream */
	  {
		if (ctrl) ctrl = ctrl->next();
		else ctrl = this->nextCtrl(startDistance);
		while (ctrl &&		/* more devices exist */
			   (*ctrl)->type() != deviceType && /* wanted not found */
			   (*ctrl)->distance() > startDistance + maxDistance)
		  {
			ctrl = ctrl->next();
		  }
	  }
  if (ctrl &&
	  ctrl && (*ctrl)->type() == deviceType) return (ctrl);
  else return (NULL);
}


/*
 * Search the first "SurvList" between "startDistance" and
 * "startDistance + maxDistance". If the optional arg "surv" is
 * not NULL, it start the search from the device "surv".
 */

SurvList
TMS_Link::searchSurv(double startDistance,
					 double maxDistance,
					 SurvList surv)
{
  if (maxDistance > 0)		/* search upstream */
	{
      if (surv) surv = surv->prev();
      else surv = this->prevSurv(startDistance);
      if (surv &&
		  (*surv)->distance() - startDistance < maxDistance)
		{
		  return (surv);
		} else
		  {
			return (NULL);
		  }
	} else			/* search downstream */
	  {
		if (surv) surv = surv->next();
		else surv = this->nextSurv(startDistance);
		if (surv &&
			(*surv)->distance() > startDistance + maxDistance)
		  {
			return (surv);
		  } else
			{
			  return (NULL);
			}
	  }
}


/*
 * Search the first "SurvList" of type "deviceType" between
 * "startDistance" and "startDistance + maxDistance". If the optional
 * "surv" is not NULL, it start the search from the device "surv".
 */

SurvList
TMS_Link::searchTypedSurv(double startDistance,
						  double maxDistance,
						  int deviceType, int dataType,
						  SurvList surv)
{
  if (maxDistance > 0)		/* search upstream */
	{
      if (surv) surv = surv->prev();
      else surv = this->prevSurv(startDistance);
      while (surv &&		/* more devices exist */
			 (*surv)->type() != deviceType &&
			 ((*surv)->tasks() & dataType) &&
			 (*surv)->distance() - startDistance < maxDistance)
		{
		  surv = surv->prev();
		}
	} else			/* search downstream */
	  {
		if (surv) surv = surv->next();
		else surv = this->nextSurv(startDistance);
		while (surv &&		/* more devices exist */
			   (*surv)->type() != deviceType &&
			   ((*surv)->tasks() & dataType) &&
			   (*surv)->distance() > startDistance + maxDistance)
		  {
			surv = surv->next();
		  }
	  }
  if (surv &&
	  (*surv)->type() == deviceType &&
	  ((*surv)->tasks() & dataType))
	{
      return (surv);
	} else
	  {
		return (NULL);
	  }
}

// THE END OF THIS FILE
