//-*-c++-*------------------------------------------------------------
// TMS_Symbols.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#ifndef TMS_SYMBOLS_HEADER
#define TMS_SYMBOLS_HEADER

#include <DRN/DRN_Symbols.h>

class TMS_Symbols : public DRN_Symbols
{
public:

  enum {
	NONE      = 0,
	MC_MASTER = 1,
	APID      = 2
  };

  enum {
	TENTATIVE         = 0x1,
	CONFIRMED         = 0x2,
	CLEARED           = 0x4
  };
  
  TMS_Symbols() : DRN_Symbols() { };
  ~TMS_Symbols() { }

  void registerAll();		// virtual

  XmwSymbol<int>& incidentTypes() {
	return incidentTypes_;
  }

protected:

  XmwSymbol<int> incidentTypes_;
};

#endif // TMS_SYMBOLS_HEADER
#endif // INTERNAL_GUI
