//-*-c++-*------------------------------------------------------------
// FILE: TMS_Lane.h
// AUTH: Qi Yang
// DATE: Wed Oct 18 14:29:29 1995
//--------------------------------------------------------------------

#ifndef TMS_LANE_H
#define TMS_LANE_H

//#include <iostream>

#ifdef INTERNAL_GUI
#include <DRN/DRN_Lane.h>
#else  // batch mode
#include <GRN/RN_Lane.h>
#endif // !INTERNAL_GUI

#ifdef INTERNAL_GUI
class TMS_Lane : public DRN_Lane
#else
class TMS_Lane : public RN_Lane
#endif // !INTERNAL_GUI

{				// TMS_Lane
   public:

      TMS_Lane();
      ~TMS_Lane() { }

      TMS_Lane * upLane(int i) {
		return (TMS_Lane *)upLanes_[i];
      }
      TMS_Lane * dnLane(int i) {
		return (TMS_Lane *)dnLanes_[i];
      }
      
      inline TMS_Lane * left()  {
		return (TMS_Lane*) RN_Lane::left();
      }
      inline TMS_Lane * right() {
		return (TMS_Lane*) RN_Lane::right();
      }
};

#endif // THE END OF THIS FILE
