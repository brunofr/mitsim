//-*-c++-*------------------------------------------------------------
// TMS_IncidentDialog.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef TMS_INCIDENT_DIALOG_HEADER
#define TMS_INCIDENT_DIALOG_HEADER

#include <Xmw/XmwDialogManager.h>
#include <Xmw/XmwCallback.h>

class TMS_IncidentDialog: public XmwDialogManager
{
  CallbackDeclare(TMS_IncidentDialog);

public:

  TMS_IncidentDialog(Widget parent);
  ~TMS_IncidentDialog() { }
  void post();				// virtual

private:

  // overload the virtual functions in base class

  void okay(Widget, XtPointer, XtPointer);
  void cancel(Widget, XtPointer, XtPointer);
  void help(Widget, XtPointer, XtPointer);

  // new callbacks

  void clickTypesCB(Widget, XtPointer, XtPointer);

private:
	  
  Widget typesFld_;				// check box
  int types_;

  Widget algoFld_;				// radio box
};

#endif
