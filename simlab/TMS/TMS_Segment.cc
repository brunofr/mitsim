//-*-c++-*------------------------------------------------------------
// FILE: TMS_Segment.C
// AUTH: Qi Yang
// DATE: Wed Oct 18 14:29:29 1995
//--------------------------------------------------------------------

#include <Tools/Math.h>

#include <GRN/RN_SurvStation.h>

#include <TC/TC_Sensor.h>

#include "TMS_Link.h"
#include "TMS_Segment.h"
#include "TMS_Parameter.h"
#include "TMS_Communicator.h"

TMS_Segment::TMS_Segment()
#ifdef INTERNAL_GUI
   : DRN_Segment(),
#else
     : RN_Segment(),
#endif
     density_(-1),
     speed_(-1),
     flow_(-1),
     degree_(0)
{
}


/*
 *------------------------------------------------------------------
 * Calculate the density of a segment based on sensor data,
 * in vehicle/kilometer
 *------------------------------------------------------------------
 */

float
TMS_Segment::calcDensity(void)
{
   SurvList surv = survList_;
   int n = 0;
   density_ = 0.0;
   while (surv != NULL &&
	  (*surv)->segment()->code() == code_)
   {
      if ((*surv)->tasks() & SENSOR_AGGREGATE)
      {
	 RN_Sensor *ss;
	 for (int i = 0; i < nLanes_; i ++)
	 {
	    ss = (*surv)->sensor(i);
	    if (ss != NULL &&
		!(ss->state() & SENSOR_BROKEN))
	    {
	       n ++;
	       density_ += ss->occupancy();
	    }
	 }
      }
      surv = surv->next();
   }

   if (n > 0) {
      density_ *= 10.0 / theParameter->vehicleLength() / (float)n;
   } else {
      density_ = -1;		// no data available
   }

   return (density_);
}


/*
 *-------------------------------------------------------------------
 * Calculate the average speed of the vehicles in a segment, in
 * meter/sec.
 *-------------------------------------------------------------------
 */

float
TMS_Segment::calcSpeed(void)
{
   SurvList surv = survList_;
   int n = 0;
   speed_ = 0.0;
   while (surv != NULL &&
	  (*surv)->segment()->code() == code_)
   {
      if ((*surv)->tasks() & SENSOR_AGGREGATE)
      {
	 RN_Sensor *ss;
	 for (int i = 0; i < nLanes_; i ++)
	 {
	    ss = (*surv)->sensor(i);
	    if (ss != NULL &&
		!(ss->state() & SENSOR_BROKEN)) {
	       n ++;
	       speed_ += ss->speed();
	    }
	 }
      }
      surv = surv->next();
   }

   if (n > 0) {
      speed_ /= n;
   } else {
      speed_ = -1;
   }

   return speed_;
}


int
TMS_Segment::calcFlow(void)
{
#ifdef EQUATION_BASED_FLOW

   if (speed_ >= 0.0 && density_ >= 0.0) { 
      float x = speed_ * density_ * 3.6;
      return flow_ = Fix(x, 1.0);
   } else {
      return flow_ = -1;
   }

#else  // based flow equation


   SurvList surv = survList_;
   int n = 0;
   flow_ = 0;
   while (surv != NULL &&
	  (*surv)->segment()->code() == code_)
   {
      if ((*surv)->tasks() & SENSOR_AGGREGATE)
      {
	 RN_Sensor *ss;
	 for (int i = 0; i < nLanes_; i ++)
	 {
	    ss = (*surv)->sensor(i);
	    if (ss != NULL &&
		!(ss->state() & SENSOR_BROKEN)) {
	       n ++;
	       flow_ += ss->flow();
	    }
	 }
      }
      surv = surv->next();
   }

   if (n > 0) {
      flow_ /= n;
      degree_ = 1;
   } else {
      flow_ = -1;
      degree_ = 0;
   }
   return flow_;
#endif
}


// This is a temporary solution for Qi's Amsterdam network

int
TMS_Segment::interpolateStateFromNearbySensors(int degree)
{
   if (degree_) return 0;	// already done

   if (link_->linkType() != LINK_TYPE_FREEWAY) {
      return 0;			// I do not have data on ramps
   }

   float spd = 0, flw = 0, dst = 0;
   int cnt = 0;
   short int i, n;
   TMS_Segment *ps;

   // Upstream segment(s)

   if (ps = (TMS_Segment *) upstream()) {
      if (ps->degree_ && ps->degree_ <= degree) {
	 spd += ps->speed();
	 flw += ps->flow();
	 dst += ps->density();
	 cnt ++;
      }
   } else {
      n = link()->nUpLinks();
      for (i = 0; i < n; i ++) {
	 ps = (TMS_Segment *) link()->upLink(i)->endSegment();
	 if (ps->degree_ && ps->degree_ <= degree) {
	    spd += ps->speed();
	    flw += ps->flow();
	    dst += ps->density();
	    cnt ++;
	 }
      }
   }
   
   // Downstream segment(s)

   if (ps = (TMS_Segment *) downstream()) {
      if (ps->degree_ && ps->degree_ <= degree) {
	 spd += ps->speed();
	 flw += ps->flow();
	 dst += ps->density();
	 cnt ++;
      }
   } else {
      n = link()->nDnLinks();
      for (i = 0; i < n; i ++) {
	 ps = (TMS_Segment *) link()->dnLink(i)->startSegment();
	 if (ps->degree_ && ps->degree_ <= degree) {
	    spd += ps->speed();
	    flw += ps->flow();
	    dst += ps->density();
	    cnt ++;
	 }
      }
   }

   // Take the average

   if (cnt) {
      speed_ = spd / cnt;
      flow_ = (int) flw / cnt;
      density_ = dst / cnt;
      degree_ = degree + 1;
      return 1;
   } else {
      return 0;
   }
}


void
TMS_Segment::updateParameters()
{
   // Right now I only update freeway parameters because I have no
   // data on other types of links
 
   if (link_->linkType() != LINK_TYPE_FREEWAY) return;

   if (speed() < 0.0) return;	// no data available

   // Assume the minimum speed limit and free flow speed is 5 mph

   const float minspd = 5.0;

   // Assume on average people travel 12.2 mph faster than speed limit
 
   const float spd_bias = 12.2;

   // Both the free flow speed and speed limits are rounded to 5 mph

   const float step = 5.0;

   // speedLimit_ is in mph

   float spd = speed() / theParameter->speedFactor() - spd_bias;
   speedLimit_ = (int) Ceil(spd, step);
   speedLimit_ = (int) ((speedLimit_ > minspd) ? speedLimit_ : minspd);

   // freeSpeed_ is in meter/sec

   float dv = step * theParameter->speedFactor();
   freeSpeed_ = Ceil(speed(), dv);
   spd = minspd * theParameter->speedFactor();
   freeSpeed_ = (freeSpeed_ > spd) ? freeSpeed_ : spd;
}
