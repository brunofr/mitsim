//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_Action.h
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#ifndef TMS_ACTION_HEADER
#define TMS_ACTION_HEADER

#include <iostream>
#include <Templates/Object.h>

class Reader;
class TMS_Incident;
class TMS_ResponsePhase;

class TMS_Action : public CodedObject
{
   public:

      TMS_Action() : CodedObject(),
		 parent_(NULL) { }

      virtual ~TMS_Action() { }

      virtual void read(Reader&) = 0;
      virtual void print(std::ostream &os = std::cout) = 0;

      virtual void applyTo(TMS_Incident&) = 0;
 

      inline int situation() {
		 return situation_;
	  }
      inline void parent(TMS_ResponsePhase* p) {
		 parent_ = p;
	  }

   protected:
	
      TMS_ResponsePhase* parent_; /* pointer to the container */

      int situation_;		/* Blockage + EVA access */
      short int fromRef_;	/* flag for ref of distance */
      float fromDistance_;	/* start distance */
      short int toRef_;		/* flag for ref of distance */
      float toDistance_;	/* end distance */
};

#endif
