//-*-c++-*------------------------------------------------------------
// TMS_SetupDialog.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <assert.h>

#include <Xmt/Xmt.h>
#include <Xmt/Chooser.h>
#include <Xmt/InputField.h>
#include <Xm/ToggleB.h>

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>
#include <Tools/Math.h>

#include "TMS_SetupDialog.h"
#include "TMS_Interface.h"
#include "TMS_Engine.h"
#include "TMS_FileManager.h"
#include "TMS_Interface.h"
#include "TMS_Parameter.h"
#include "TMS_Network.h"
#include "TMS_Menu.h"
#include "TMS_CtrlLogic.h"
#include "TMS_Sensor.h"

#include <TC/TC_ControlSystem.h>

TMS_SetupDialog::TMS_SetupDialog ( Widget parent )
   : XmwDialogManager(parent, "setupDialog", NULL, 0)
{
   XtVaSetValues(widget_,
				 XmNdialogStyle, XmDIALOG_PRIMARY_APPLICATION_MODAL,
				 NULL);

   title_ = XmtNameToWidget(widget_, "title");
   assert(title_);

   Widget group;

   group = XmtNameToWidget(widget_, "*DirectoryGroup");
   assert(group);

   paraDirFld_.lookup(group, "*paraDir");
   inDirFld_.lookup(group, "*inDir");
   workDirFld_.lookup(group, "*workDir");
   outDirFld_.lookup(group, "*outDir");

   group = XmtNameToWidget(widget_, "*InputGroup");
   assert(group);

   paraFileFld_.lookup(group, "*paraFile");
   netFileFld_.lookup(group, "*netFile");

   ctrlLogicFileFld_.lookup(group, "*ctrlLogicFile");

   signalPlansFileFld_.lookup(group, "*signalPlansFile");

   group = XmtNameToWidget(widget_, "*TimingGroup");
   assert(group);

   startTimeFld_.lookup(group, "*startTime");
   stopTimeFld_.lookup(group, "*stopTime");
   stepSizeFld_.lookup(group, "*stepSize");
   batchStepSizeFld_.lookup(group, "*batchStepSize");

   group = XmtNameToWidget(widget_, "*MiscGroup");
   assert(group);

   information_ = XmtNameToWidget(group, "*information");
   assert(information_);
}

void TMS_SetupDialog::set(char **ptr, const char *s)
{
   if (*ptr) {
	  free(*ptr);
   }
   if (s) {
	  *ptr = strdup(s);
   } else {
	  *ptr = strdup("");
   }
}

// These overload the functions in base class

void TMS_SetupDialog::cancel(Widget, XtPointer, XtPointer)
{
   unmanage();
}


void TMS_SetupDialog::okay(Widget, XtPointer, XtPointer)
{
   // Copy the data from dialog 

   theFileManager->title(XmtInputFieldGetString(title_));

   ToolKit::paradir(paraDirFld_.get());
   ToolKit::indir(inDirFld_.get());
   ToolKit::workdir(workDirFld_.get());
   ToolKit::outdir(outDirFld_.get());

   set(TMS_Parameter::nameptr(), paraFileFld_.get());
   set(RoadNetwork::nameptr(), netFileFld_.get());

   set(TMS_CtrlLogic::nameptr(), ctrlLogicFileFld_.get());
   set(TC_ControlSystem::nameptr(), signalPlansFileFld_.get());

   theSimulationClock->startTime(theInterface->convertTime(startTimeFld_.get()));
   theSimulationClock->stopTime(theInterface->convertTime(stopTimeFld_.get()));
   theSimulationClock->stepSize(theInterface->convertTime(stepSizeFld_.get()));
   theEngine->batchStepSize_ = theInterface->convertTime(batchStepSizeFld_.get());

   theEngine->information_ = XmtChooserGetState(information_);

   unmanage();

   theMenu->needSave();
}


void TMS_SetupDialog::help(Widget, XtPointer, XtPointer)
{
   theInterface->openUrl("setup", "tms.html");
}


void TMS_SetupDialog::post()
{
   XmtInputFieldSetString(title_, theFileManager->title());

   paraDirFld_.set(ToolKit::paradir());
   inDirFld_.set(ToolKit::indir());
   workDirFld_.set(ToolKit::workdir());
   outDirFld_.set(ToolKit::outdir());

   paraFileFld_.set(TMS_Parameter::name());
   netFileFld_.set(RoadNetwork::name());

   ctrlLogicFileFld_.set(TMS_CtrlLogic::name());
   signalPlansFileFld_.set(TC_ControlSystem::name());

   startTimeFld_.set(theSimulationClock->startStringTime());
   stopTimeFld_.set(theSimulationClock->stopStringTime());
   stepSizeFld_.set(theSimulationClock->stepSize());
   batchStepSizeFld_.set(theEngine->batchStepSize_);

   XmtChooserSetState(information_, theEngine->information_, False);

   // Do not allow changes after simulation is started

   if (theSimulationClock->isStarted()) {
	  deactivate();
   } else {
	  activate();
   }

   XmwDialogManager::post();
}

void TMS_SetupDialog::activate()
{
   if (XtIsSensitive(okay_)) return;

   XmwDialogManager::activate(title_);

   XmwDialogManager::activate(paraDirFld_.widget());
   XmwDialogManager::activate(inDirFld_.widget());
   XmwDialogManager::activate(workDirFld_.widget());
   XmwDialogManager::activate(outDirFld_.widget());

   XmwDialogManager::activate(paraFileFld_.widget());
   XmwDialogManager::activate(netFileFld_.widget());
   XmwDialogManager::activate(ctrlLogicFileFld_.widget());
   XmwDialogManager::activate(signalPlansFileFld_.widget());
   
   XmwDialogManager::activate(startTimeFld_.widget());
   XmwDialogManager::activate(stopTimeFld_.widget());
   XmwDialogManager::activate(stepSizeFld_.widget());
   XmwDialogManager::activate(batchStepSizeFld_.widget());

   XmwDialogManager::activate(information_);

   XmwDialogManager::activate(okay_);
}

void TMS_SetupDialog::deactivate()
{
   if (!XtIsSensitive(okay_)) return;

   XmwDialogManager::deactivate(title_);

   XmwDialogManager::deactivate(paraDirFld_.widget());
   XmwDialogManager::deactivate(inDirFld_.widget());
   XmwDialogManager::deactivate(workDirFld_.widget());
   XmwDialogManager::deactivate(outDirFld_.widget());

   XmwDialogManager::deactivate(paraFileFld_.widget());
   XmwDialogManager::deactivate(netFileFld_.widget());
   XmwDialogManager::deactivate(ctrlLogicFileFld_.widget());
   XmwDialogManager::deactivate(signalPlansFileFld_.widget());
   
   XmwDialogManager::deactivate(startTimeFld_.widget());
   XmwDialogManager::deactivate(stopTimeFld_.widget());
   XmwDialogManager::deactivate(stepSizeFld_.widget());
   XmwDialogManager::deactivate(batchStepSizeFld_.widget());

   XmwDialogManager::deactivate(information_);

   XmwDialogManager::deactivate(okay_);
}

#endif // INTERNAL_GUI
