/*-*-c++-*------------------------------------------------------------
 * TMS_ApidPrmParser.y
 *
 * Sreeram Radhakrishnan
 * Copyright (C) 1997
 * Massachusetts Institue of Technology
 * All Rights Reserved
 *
 *--------------------------------------------------------------------
 */

%name TMS_ApidPrmBisonParser
%define ERROR_BODY =0
%define LEX_BODY =0

%define MEMBERS   TMS_ApidPrmScanner pts; TMS_ApidPrmTable *ptr1; 
 
%define CONSTRUCTOR_PARAM const char *f

%define CONSTRUCTOR_INIT : pts(f), ptr1(NULL)

%union {
      int    itype;
      float  ftype;
      char  *stype;
}

%token <itype> TMS_ApidPrm_COL
%token <stype> TMS_ApidPrm_OB
%token <stype> TMS_ApidPrm_CB
%token <itype> TMS_ApidPrm_INT
%token <ftype> TMS_ApidPrm_REAL
%token <stype> TMS_ApidPrm_STRING
%token <stype> TMS_ApidPrm_NAME

%header{
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;

#include <IO/Exception.h>
#include <Tools/ToolKit.h>  

#include "TMS_ApidParameters.h"
#include "TMS_ApidPrmTable.h"

#undef yyFlexLexer
#define yyFlexLexer TMS_ApidPrmFlexLexer
#include <FlexLexer.h>

   class TMS_ApidPrmScanner : public TMS_ApidPrmFlexLexer
   {
      private:

	 char *name_;			// file name

      public:

	 TMS_ApidPrmScanner() : name_(NULL), TMS_ApidPrmFlexLexer() { }
	 TMS_ApidPrmScanner(const char *fname)
	    : name_(Copy(fname)),
	      TMS_ApidPrmFlexLexer() {
		 if (ToolKit::fileExists(fname)) {
		    switch_streams(new ifstream(fname), 0);
		 } else {
		    cerr << "Error:: cannot open input file <"
				 << name_ << ">" << endl;
		    theException->exit();
		 }
	 }
	 ~TMS_ApidPrmScanner() { delete [] name_; delete yyin; }

	 char* name() const { return name_; }

	 char* removeDelimeters(const char *deli="\"\"") {
	    char *s, *str = (char *)YYText();
	    if (*str == deli[0]) str ++;
	    s = str;
	    while (*s != '\0' && *s != deli[1]) s ++;
	    *s = '\0';
	    return (str);
	 }

	 char* value() { return (char*)YYText(); }

	 void errorQuit(int err) {
	    if (err == 0) return;
	    cerr << "Problem in parsing"
		 << " (" << name() << ":" << lineno() << ")" << endl;
	    if (err < 0) theException->exit();
	 }
   };
%}

//--------------------------------------------------------------------
// Symbols used in Sensor Parameter grammer rules.
//--------------------------------------------------------------------

%type <stype> ob cb
%type <itype> inum persistenabled medtrfenabled ligttrfenabled 
%type <ftype> fnum compwaveth1 compwaveth2 incdetth1 incdetth2 incdetth3 

%type <ftype> medtrfincdet1 medtrfincdet2 persistencyth medtrfth lowtrfth 
%type <ftype> incclrth
%type <itype> compwavetestenabled totalprmgrps multiparamgrp paramgrp
%type <itype> incpersistency compwavetestperiod  sensorlist totalsensors
%type <itype> sensor_id sensorids prmgrpnumber
%type <stype> allthedata
%start everything

%%

//--------------------------------------------------------------------
// Beginniing of TMS_ApidParameters grammer rules.
//--------------------------------------------------------------------

everything: totalprmgrps incpersistency compwavetestperiod allthedata 
{
   cout << endl << "Finished Parsing <"
		<< pts.name() << ">." << endl;
}
;

totalprmgrps: inum
{
   int n = ($1 >= 0) ? ($1) : -99;
   ptr1->assigntotalPrmGrps(n);	
   ptr1->prmGrps().reserve(n);
}
;

incpersistency: inum 
{
   int n = ($1 >= 0) ? ($1) : -99;
   ptr1->assignincPersistency(n);
}
;

compwavetestperiod: inum
{
   int n = ($1 >= 0) ? ($1) : -99;
   ptr1->assigncompWaveTestPeriod(n);
}
;

allthedata: ob multiparamgrp cb
;

multiparamgrp: paramgrp
| multiparamgrp paramgrp
;

paramgrp: prmgrpnumber ob compwavetestenabled compwaveth1 compwaveth2 persistenabled medtrfenabled ligttrfenabled incdetth1 incdetth2 incdetth3 medtrfincdet1 medtrfincdet2 persistencyth medtrfth lowtrfth incclrth  ob sensorlist cb cb
;

prmgrpnumber: inum
{
   int n = ($1 >= 0) ? ($1) : -99;

   TMS_ApidParameters *p = new TMS_ApidParameters;
   ptr1->prmGrps().push_back(p);

   ptr1->last()->assignprmGrpID(n);
}
;

compwavetestenabled: inum
{
   int n = ($1 >= 0) ? ($1) : -99;
   ptr1->last()->assigncompWaveTestEnabled(n);
}
;

compwaveth1: fnum
{
   float f = ($1 >= 0) ? ($1) : -99;
   ptr1->last()->assigncompWaveTh1(f);
}
;

compwaveth2: fnum
{
   float f = ($1 > 0) ? ($1) : -99;
   ptr1->last()->assigncompWaveTh2(f);
}
;

persistenabled: inum
{
   int n = ($1 >= 0) ? ($1) : -99;
   ptr1->last()->assignpersistTestEnabled(n);
}
;

medtrfenabled: inum
{
   int n = ($1 >= 0) ? ($1) : -99;
   ptr1->last()->assignmedTrfEnabled(n);
}
;

ligttrfenabled: inum
{
   int n = ($1 >= 0) ? ($1) : -99;
   ptr1->last()->assignligtTrfEnabled(n);
}
;

incdetth1: fnum
{
   float f = ($1 > 0) ? ($1) : -99;
   ptr1->last()->assignincDetTh1(f);
}
;

incdetth2: fnum
{
   float f = ($1 > 0) ? ($1) : -99;
   ptr1->last()->assignincDetTh2(f);
}
;

incdetth3: fnum
{
   float f = ($1 > 0) ? ($1) : -99;
   ptr1->last()->assignincDetTh3(f);
}
;

medtrfincdet1: fnum
{
   float f = ($1 > 0) ? ($1) : -99;
   ptr1->last()->assignmedTrfIncDet1(f);
}
;

medtrfincdet2: fnum
{
   float f = ($1 > 0) ? ($1) : -99;
   ptr1->last()->assignmedTrfIncDet2(f);
}
;

persistencyth: fnum
{
   float f = ($1 > 0) ? ($1) : -99;
   ptr1->last()->assignpersistencyTh(f);
}
;

medtrfth: fnum
{
   float f = ($1 > 0) ? ($1) : -99;
   ptr1->last()->assignmedTrfTh(f);
}
;

lowtrfth: fnum
{
   float f = ($1 >= 0) ? ($1) : -99;
   ptr1->last()->assignlowTrfTh(f);
}
;

incclrth: fnum
{
   float f = ($1 > 0) ? ($1) : -99;
   ptr1->last()->assignincClrTh(f);
}
;

sensorlist: totalsensors sensorids
;

totalsensors: inum
{
   ptr1->last()->assignlistdim($1);
}
;

sensorids: sensor_id
| sensorids sensor_id
;

sensor_id: inum
{
   ptr1->last()->addsensorid($1);
}
;

//--------------------------------------------------------------------
// End of TMS_ApidParameters grammer rules.
//--------------------------------------------------------------------


//--------------------------------------------------------------------
// Beginning of basic grammer rules.
//--------------------------------------------------------------------


ob: TMS_ApidPrm_OB
;

cb: TMS_ApidPrm_CB
;

inum: TMS_ApidPrm_INT
{
   $$ = atoi(pts.value());
}
;

fnum: TMS_ApidPrm_REAL 
{
   $$ = atof(pts.value()); 
}
;


//--------------------------------------------------------------------
// End of basic grammer rules
//--------------------------------------------------------------------


%%

//--------------------------------------------------------------------
// Following pieces of cpte will be verbosely copied into the parser.
//--------------------------------------------------------------------

%header{

   class TMS_ApidPrmParser : public TMS_ApidPrmBisonParser
   {
      public:
	 TMS_ApidPrmParser(const char *fname)
	    : TMS_ApidPrmBisonParser(fname) { }
	 ~TMS_ApidPrmParser() { }

	 char* name() const { return pts.name(); }

	 void yyerror(char *msg) {
	    cerr << "\nError:: " << msg
			 << " (" << name() << ":"
			 << pts.lineno() << ")" << endl;
	    theException->exit();
	 }

	 int yylex() { return pts.yylex(); }

	 void parse(TMS_ApidPrmTable *pt){
	   cout << "Parsing Sensor Parameter file<";
	   cout << name() << "> ..." << endl;;
	   ptr1 = pt;
	   yyparse();
	 }
   };
%}
