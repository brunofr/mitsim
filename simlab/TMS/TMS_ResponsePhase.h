//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_ResponsePhase.h
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#ifndef TMS_RESPONSE_PHASE_HEADER
#define TMS_RESPONSE_PHASE_HEADER

#include <iostream>
#include <list>
#include "TMS_Action.h"

class TMS_ResponsePhase : public CodedObject
{
   public:

      TMS_ResponsePhase();
      virtual ~TMS_ResponsePhase() { }

      void read(int, Reader&);
      void print(std::ostream &os = std::cout);

      inline double delay() { return delay_; }
      void process(TMS_Incident&);
      inline int nActions() {
		 return actionList_.size();
      }

   protected:
	
      double delay_;		/* delay since begin or previous stage */
      std::list<TMS_Action *> actionList_; /* list of actions */
};

#endif
