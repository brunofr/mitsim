//-*-c++-*------------------------------------------------------------
// FILE: TMS_Lane.C
// AUTH: Qi Yang
// DATE: Wed Oct 18 14:29:29 1995
//--------------------------------------------------------------------

#include <GRN/RN_SurvStation.h>
#include <GRN/RN_CtrlStation.h>

#include "TMS_Lane.h"

TMS_Lane::TMS_Lane()
#ifdef INTERNAL_GUI
  : DRN_Lane()
#else
    : RN_Lane()
#endif
{
}

// Most functionalities for TMS_Lane have been defined in base classes
// (DRN_Lane or RN_Lane). See files in DrwableRoadNetwork and
// GeneralRoadNetwork for details.
