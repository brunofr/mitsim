//-*-c++-*------------------------------------------------------------
// TMS_Interface.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class implementation for the class
// TMS_Interface
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <iostream>
#include <cstdlib>
using namespace std;

#include <Xmt/Xmt.h>
#include <Xmt/Create.h>

#include <Xmw/XmwImage.h>
#include <Xmw/XmwColor.h>

#include "TMS_Symbols.h"
#include "TMS_Interface.h"
#include "TMS_Menu.h"
#include "TMS_Modeline.h"
#include "TMS_Icon.cc"

TMS_Interface* theInterface = NULL;

TMS_Interface::TMS_Interface(int *argc, char **argv)
   : DRN_Interface(argc, argv,
				   new XmwImage("tms", icon_bits, NULL,
								icon_width, icon_height))
{
   theInterface = this;
   create();
}

TMS_Interface::~TMS_Interface()
{
}

void TMS_Interface::create()		// virtual
{
   DRN_Interface::create();
   manage ();
}

XmwSymbols* TMS_Interface::createSymbols()
{
   return new TMS_Symbols;
}

XmwMenu* TMS_Interface::menu(Widget parent)
{
   return new TMS_Menu(parent);
}

XmwModeline* TMS_Interface::modeline(Widget parent)
{
   return new TMS_Modeline(parent);
}

#endif // INTERNAL_GUI
