//-*-c++-*------------------------------------------------------------
// TMS_SensorDataDialog.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <assert.h>
#include <stdlib.h>

#include <Xmt/Xmt.h>
#include <Xmt/Chooser.h>
#include <Xmt/InputField.h>
#include <Xm/ToggleB.h>

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>

#include "TMS_SensorDataDialog.h"
#include "TMS_Interface.h"
#include "TMS_Engine.h"
#include "TMS_FileManager.h"
#include "TMS_Interface.h"
#include "TMS_Menu.h"
#include "TMS_Sensor.h"

TMS_SensorDataDialog::TMS_SensorDataDialog ( Widget parent )
   : XmwDialogManager(parent, "sensorDataDialog", NULL, 0)
{
   source_ = XmtNameToWidget(widget_, "source");
   assert(source_);
   addCallback(source_, XmNvalueChangedCallback,
			   &TMS_SensorDataDialog::sourceClickCB,
			   NULL);

   dbStartTime_.lookup(widget_, "dbStartTime");
   dbStepSize_.lookup(widget_, "dbStepSize");
   dbNumPeriods_.lookup(widget_, "dbNumPeriods");
   dbSpeedFile_ .lookup(widget_, "dbSpeedFile");
   dbSpeedScaler_.lookup(widget_, "dbSpeedScaler");
   dbFlowFile_  .lookup(widget_, "dbFlowFile");
   dbFlowScaler_.lookup(widget_, "dbFlowScaler");
}

TMS_SensorDataDialog::~TMS_SensorDataDialog()
{
}

void TMS_SensorDataDialog::sourceClickCB(Widget, XtPointer, XtPointer)
{
   if (theSimulationClock->isStarted()) {
	  deactivate();
   } else if (XmtChooserGetState(source_)) {
	  activate();
   } else {
	  deactivate();
   }
}


// These overload the functions in base class

void TMS_SensorDataDialog::cancel(Widget, XtPointer, XtPointer)
{
   unmanage();
}


void TMS_SensorDataDialog::okay(Widget, XtPointer, XtPointer)
{
   // Copy the data from dialog 

   theEngine->source_ = XmtChooserGetState(source_);

   TMS_Sensor::dbStartTime_ = atof(dbStartTime_.get());
   TMS_Sensor::dbStepSize_ = atof(dbStepSize_.get());

   TMS_Sensor::dbNumPeriods_ = atoi(dbNumPeriods_.get());
   TMS_Sensor::calcDbEndTime();

   TMS_Sensor::dbSpeedFile(dbSpeedFile_.get());
   TMS_Sensor::dbSpeedScaler_= atof(dbSpeedScaler_.get());

   TMS_Sensor::dbFlowFile(dbFlowFile_.get());
   TMS_Sensor::dbFlowScaler_ = atof(dbFlowScaler_.get());

   // Pop down the dialog

   unmanage();
   theMenu->needSave();
}


void TMS_SensorDataDialog::help(Widget, XtPointer, XtPointer)
{
   theDrnInterface->openUrl("chooseSensorData", "tms.html");
}


void TMS_SensorDataDialog::post()
{
   // Copy data to dialog

   XmtChooserSetState(source_, theEngine->source_, False);

   dbStartTime_.set(TMS_Sensor::dbStartTime_);   
   dbStepSize_.set(TMS_Sensor::dbStepSize_);
   dbNumPeriods_.set(TMS_Sensor::dbNumPeriods_); 
   dbSpeedFile_.set(TMS_Sensor::dbSpeedFile_);
   dbSpeedScaler_.set(TMS_Sensor::dbSpeedScaler_);
   dbFlowFile_.set(TMS_Sensor::dbFlowFile_);  
   dbFlowScaler_.set(TMS_Sensor::dbFlowScaler_);


   // Do not allow changes after simulation is started

   if (theSimulationClock->isStarted()) {
	  deactivate();
   } else if (XmtChooserGetState(source_)) {
	  activate();
   } else {
	  deactivate();
   }
   
   XmwDialogManager::post();
}

void TMS_SensorDataDialog::activate()
{
   XmwDialogManager::activate(dbStartTime_.widget());
   XmwDialogManager::activate(dbStepSize_.widget());
   XmwDialogManager::activate(dbNumPeriods_.widget());
   XmwDialogManager::activate(dbSpeedFile_.widget());
   XmwDialogManager::activate(dbSpeedScaler_.widget());
   XmwDialogManager::activate(dbFlowFile_.widget());
   XmwDialogManager::activate(dbFlowScaler_.widget());
}

void TMS_SensorDataDialog::deactivate()
{
   XmwDialogManager::deactivate(dbStartTime_.widget());
   XmwDialogManager::deactivate(dbStepSize_.widget());
   XmwDialogManager::deactivate(dbNumPeriods_.widget());
   XmwDialogManager::deactivate(dbSpeedFile_.widget());
   XmwDialogManager::deactivate(dbSpeedScaler_.widget());
   XmwDialogManager::deactivate(dbFlowFile_.widget());
   XmwDialogManager::deactivate(dbFlowScaler_.widget());
}

#endif // INTERNAL_GUI
