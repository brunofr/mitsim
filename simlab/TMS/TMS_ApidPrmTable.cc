//-*-c++-*------------------------------------------------------------
// TMS_ApidPrmTable.cc
//
// Sreeram Radhakrishnan
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <cstdio>

#include <Tools/ToolKit.h>
#include <IO/Exception.h>

#include "TMS_ApidParameters.h"
#include "TMS_ApidPrmParser.h"
#include "TMS_ApidPrmTable.h"
#include "TMS_Network.h"
#include "TMS_Link.h"
#include "TMS_Segment.h"
using namespace std;

char *TMS_ApidPrmTable::name_;

TMS_ApidPrmTable *theApidPrmTable = NULL;

TMS_ApidPrmTable* TMS_ApidPrmTable::parseApidParameters()
{
  const char *filename = TMS_ApidPrmTable::name();

  if (!ToolKit::isValidInputFilename(filename)) {
	cout << "Invalid filename for APID incident detection." << endl;
	return NULL;
  }

  filename = ToolKit::optionalInputFile(filename); 

  TMS_ApidPrmTable *p = new TMS_ApidPrmTable;

  TMS_ApidPrmParser theApidPrmParser(filename);

  theApidPrmParser.parse(p);

  return p;
}

void TMS_ApidPrmTable::assignincPersistency(int n)
{
  incPersistency_ = n;
}

void TMS_ApidPrmTable::assigncompWaveTestPeriod(int n)
{
  compWaveTestPeriod_ = n;
}

void TMS_ApidPrmTable::assigntotalPrmGrps(int n)
{
  totalPrmGrps_ = n;
};

int TMS_ApidPrmTable::incPersistency()
{
  return incPersistency_;
}

int TMS_ApidPrmTable::compWaveTestPeriod()
{
  return compWaveTestPeriod_;
}

void TMS_ApidPrmTable::print(ostream &os)
{
  if(this) {

	int imax = prmGrps().size();

	os << "/*" << endl
	   << " * " << name()
	   << " -- Parameters for incident dectection using APID algorithm" << endl
	   << " */" << endl << endl;

	os << totalPrmGrps_ << endc;
    os << compWaveTestPeriod_ << endc;
    os << incPersistency_ << endc;
	os << endl << OPEN_TOKEN;

	for (register int i = 0; i < imax; i++)
	  {
		prmGrps(i)->print(os);
	  }
	os << CLOSE_TOKEN << endl;
  }
}

void TMS_ApidPrmTable::assignPrmGrpstoStations()
{
  TMS_Network *thenetwork = tmsNetwork();

  for(register int i=0; i < totalPrmGrps_;i++) {
	TMS_ApidParameters *ptr = getprmgrpptr(i);
	vector<int> *sensorlistptr = ptr->sensorListptr();

	for(register int j=0; j < (*sensorlistptr).size();j++) {

	  int index = (*sensorlistptr)[j];
	  int prmgrp = ptr->prmGrpID();
	  RN_Sensor *rns = thenetwork->findSensor(index);
	  RN_SurvStation *surv = rns->station();
	  surv->setprmGrpID(prmgrp);
	}
  }
}
