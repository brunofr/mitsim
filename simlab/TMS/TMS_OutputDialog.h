//-*-c++-*------------------------------------------------------------
// TMS_OutputDialog.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI
#ifndef TMS_OUTPUTDIALOG_HEADER
#define TMS_OUTPUTDIALOG_HEADER

#include <Templates/Pointer.h>
#include <Xmw/XmwDialogManager.h>
#include <Xmw/XmwCallback.h>

class TMS_OutputDialog : public XmwDialogManager
{
	  CallbackDeclare(TMS_OutputDialog);

   public:

	  TMS_OutputDialog(Widget parent);
	  ~TMS_OutputDialog();
	  void post();				// virtual

   private:

	  void activate();
	  void deactivate();

	  // overload the virtual functions in base class

	  void okay(Widget, XtPointer, XtPointer);
	  void cancel(Widget, XtPointer, XtPointer);
	  void help(Widget, XtPointer, XtPointer);

   private:

	  Widget outDirFld_;		// XmtInputField
	  Widget isHeadingOnFld_;	// XmToggleButton
	  Widget formatFld_;		// XmwChooser
};

#endif
#endif // INTERNAL_GUI
