//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_Signal.C
// DATE: Sun Mar 31 22:30:25 1996
//--------------------------------------------------------------------

#include "TMS_Signal.h"
#include "TMS_Engine.h"
#include "TMS_Communicator.h"

#ifdef INTERNAL_GUI
#include <DRN/DRN_DrawingArea.h>
#endif

void
TMS_Signal::state(unsigned int s)
{
   TC_Signal::state(s);

   TMS_Communicator *communicator = (TMS_Communicator *) theCommunicator;

   if (communicator->isMitsimConnected()) {
      send(communicator->mitsim());
   }
}
