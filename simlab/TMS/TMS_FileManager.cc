//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_FileManager.C
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#include <cstring>

#include "TMS_FileManager.h"

#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <Tools/SimulationClock.h>

// This file is generated from VariableParser.y

#include <Tools/VariableParser.h>

#include <IO/MessageTags.h>

#include <GRN/RN_SurvStation.h>
#include <GRN/RN_CtrlStation.h>

#include <TC/TC_Controller.h>
#include <TC/TC_ControlRegion.h>
#include <TC/TC_ControlSystem.h>

#ifdef INTERNAL_GUI
#include <DRN/DRN_MapFeature.h>
#include <DRN/DRN_Segment.h>
#include <DRN/DRN_ViewMarker.h>
#include "TMS_Symbols.h"
#include "TMS_Interface.h"
#endif

#include "TMS_Engine.h"
#include "TMS_Network.h"
#include "TMS_Parameter.h"
#include "TMS_CtrlLogic.h"
#include "TMS_Sensor.h"

#include "TMS_McmasterParameters.h"
#include "TMS_McmasterPrmTable.h"
#include "TMS_ApidPrmTable.h"
#include "TMS_IncidentDetect.h"
#include <iostream>
using namespace std;

TMS_FileManager* theFileManager = new TMS_FileManager;

TMS_FileManager::TMS_FileManager()
  : GenericSwitcher(),
	title_(NULL)
{
}


// Read the control variables from master file.

void
TMS_FileManager::readMasterFile()
{
  VariableParser vp(this);
  vp.parse(theEngine->master());
}


void TMS_FileManager::set(char **ptr, const char *s)
{
  if (*ptr) {
	free(*ptr);
  }
  if (s) {
	*ptr = strdup(s);
  } else {
	*ptr = NULL;
  }
}

// Check if two token are equal.

int TMS_FileManager::isEqual(const char *s1, const char *s2)
{
  return IsEqual(s1, s2);
}


// This function returns 0 if the variable is parsed or 1 if it is
// skipped (in case command line provides the value), or -1 if error
// occurs.

int TMS_FileManager::parseVariable(char ** varptr, GenericVariable &gv)
{
  *varptr = Copy(gv.string());
  return 0;
}


// All the variables provided in the master file should be parsed in
// this function.

int
TMS_FileManager::parseVariable(GenericVariable &gv) 
{
  char * token = compact(gv.name());

  if (isEqual(token, "Title")) {
	title_ = Copy(gv.string());

  } else if (isEqual(token, "DefaultParameterDirectory")) {
	ToolKit::paradir(gv.string());
  } else if (isEqual(token, "InputDirectory")) {
	ToolKit::indir(gv.string());
  } else if (isEqual(token, "OutputDirectory")) {
	ToolKit::outdir(gv.string());
  } else if (isEqual(token, "WorkingDirectory")) {
	ToolKit::workdir(gv.string());
 
  } else if (isEqual(token, "NetworkDatabaseFile")) {
	return parseVariable(RoadNetwork::nameptr(), gv);

  } else if (isEqual(token, "GDSFiles")) {
#ifdef INTERNAL_GUI
	theMapFeatures = new DRN_MapFeature;
	theMapFeatures->load(gv);
#endif
	return 0;

  } else if (isEqual(token, "ParameterFile")) {
	return parseVariable(TMS_Parameter::nameptr(), gv);
  } else if (isEqual(token, "ControlLogicFile")) {
	return parseVariable(TMS_CtrlLogic::nameptr(), gv);
  } else if (isEqual(token, "SignalPlanFile")) {
	return parseVariable(TC_ControlSystem::nameptr(), gv);
  } else if (isEqual(token, "SensorDatabase")) {
	return TMS_Sensor::loadDatabaseSetting(gv);

  } else if (isEqual(token, "StartTime")) {
	theSimulationClock->startTime(gv.getTime());
  } else if (isEqual(token, "StopTime")) {
	theSimulationClock->stopTime(gv.getTime());
  } else if (isEqual(token, "StepSize")) {
	theSimulationClock->stepSize(gv.getTime());

  } else if (isEqual(token, "BreakPoints")) {
	return theEngine->parseBreakPoints(gv);

  } else if (isEqual(token, "RollingStepSize")) {
	theEngine->rollingStepSize_ = (int) gv.getTime();
  } else if (isEqual(token, "RollingLength")) {
	theEngine->rollingLength_ = (int) gv.getTime();

  } else if (isEqual(token, "ConsoleMessageStepSize")) {
	theEngine->batchStepSize_ = gv.getTime();

  } else if (isEqual(token, "ControlLogic")) {
	theEngine->controlLogic(gv.element());

  } else if (isEqual(token, "Information")) {
	theEngine->information(gv.element());

  } else if (isEqual(token, "McMasterParametersFile")) {
	return parseVariable(TMS_McmasterPrmTable::nameptr(), gv); 

  } else if (isEqual(token, "ApidParametersFile")) {
	return parseVariable(TMS_ApidPrmTable::nameptr(), gv);

  } else if (isEqual(token, "IncidentLogFile")) {
	return parseVariable(TMS_IncidentDetector::logfnameptr(), gv);

  } else if (isEqual(token, "NumOfDTAInterations")) {
	theEngine->nTotalIterations_ = gv.element();

  } else if (isEqual(token, "DTAComputationalDelay")) {
	theEngine->routingTableDelay_ = gv.getTime();

  } else if (isEqual(token, "Signals")) {
#ifdef INTERNAL_GUI
	theDrnSymbols()->signalTypes().set(gv.element());
#endif
	return 0;

  } else if (isEqual(token, "SensorTypes")) {
#ifdef INTERNAL_GUI
	theSymbols()->sensorTypes().set(gv.element());
#endif
	return 0;

  } else if (isEqual(token, "SensorColorCode")) {
#ifdef INTERNAL_GUI
	theSymbols()->sensorColorCode().set(gv.element());
#endif
	return 0;

  } else if (isEqual(token, "Segments")) {
#ifdef INTERNAL_GUI
	theSymbols()->segmentColorCode().set(gv.element());
#endif
	return 0;

  } else if (isEqual(token, "ViewMarkers")) {
#ifdef INTERNAL_GUI
	DRN_ViewMarker::LoadViewMarkers(gv) ;
#endif
	return 0;

  } else if (isEqual(token, "Output")) {
	theEngine->chooseOutput(gv.element());
 
  } else if (isEqual(token, "Timeout")) {

	// This will overwrite the value parsed from command line

	NO_MSG_WAITING_TIME = gv.element();

  } else if (ToolKit::verbose()) {

	return 1;
  }
  return 0;
}


void
TMS_FileManager::openOutputFiles()
{
  /* Open vehicle log file */

  cerr << "No output file opened" << endl;
}

void
TMS_FileManager::closeOutputFiles()
{
  cout << "Output files are in directory <"
	   << ToolKit::outDir() << ">." << endl;
}
