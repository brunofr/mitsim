//-*-c++-*------------------------------------------------------------
// NAME: Microcscopic Traffic Simulator
// AUTH: Qi Yang
// FILE: TMS_Setup.C
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <new>
#include <cstdio>
#include <cstring>
using namespace std;

#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <Tools/SimulationClock.h>
#include <Tools/Random.h>

#include "TMS_Setup.h"
#include "TMS_Network.h"
#include "TMS_Parameter.h"
#include "TMS_Engine.h"
#include "TMS_CtrlLogic.h"
#include "TMS_Status.h"
#include "TMS_FileManager.h"
#include "TMS_Communicator.h"
#include "TMS_CmdArgsParser.h"
#include "TMS_Exception.h"
#include "TMS_Incident.h"

#include "TMS_McmasterPrmTable.h"
#include "TMS_ApidPrmTable.h"
#include "TMS_McmasterDetect.h"
#include "TMS_ApidDetect.h"
#include "TMS_IncidentDetect.h"

#include <TC/TC_ControlSystem.h>
#include <TC/TC_LogFile.h>

#include <GRN/RN_Parser.h>

void 
ParseParameters()
{
  if (theParameter) {
	delete (TMS_Parameter *)theParameter;
  }
  theParameter = new TMS_Parameter;
  theParameter->load();
}


// --------------------------------------------------------------------
// Read the network database. This include network objects (nodes,
// link labels, links, segments, lanes), traffic control objects
// (traffic signals, message signs, toll booths, etc.) and
// surveillance objects (sensors).
// --------------------------------------------------------------------

void ParseNetwork()
{
  if (theNetwork) {
	if (ToolKit::verbose()) {
	  cout << "Unloading <" << theNetwork->name()
		   << ">" << endl;
	}
	delete (TMS_Network *)theNetwork;
  }
  theNetwork = new TMS_Network;
  const char *filename = ToolKit::optionalInputFile(theNetwork->name());

  if (!filename) theException->exit(1);

  RN_Parser rn_parser(theNetwork);
  rn_parser.parse(filename);

#ifdef INTERNAL_GUI
  ((TMS_Network*)theNetwork)->calcGeometricData();
#endif   

  if (ToolKit::verbose()) {
	theNetwork->printBasicInfo();
  }
}


// Setup some other stuffs. 

void SetupMiscellaneous()
{
  // Load incident response logic
   
  theCtrlLogic->load();

   // Open log file for traffic signals

  const char *filename = ToolKit::outfile("tc.out");
  TC_LogFile::the(filename);

   // Load signal plans

  TC_ControlSystem::openSignalFile();

  // Assign the pointers to control and surveillance devices in
  // TMS_Segment objects.

  theNetwork->assignCtrlListInSegments();
  theNetwork->assignSurvListInSegments();

  theFileManager->openOutputFiles();
}
    
void ParseIncidentDetectPrms()
{

  switch(theEngine->controlLogic() & CONTROL_INC_DETECTION) {
		
  case 0:
	{
	  cout << endl << "No incident detection." << endl;
	  break;
	}

  case CONTROL_INC_DETECTION_MCMASTER:
	{
	  cout << endl << "McMaster incident detection Logic used." << endl;
	  theMcmasterPrmTable = TMS_McmasterPrmTable::parseMcmasterParameters();
	  if (theMcmasterPrmTable) {
		//theMcmasterPrmTable->print();
		theMcmasterPrmTable->assignPrmGrpstoStations();
		theIncidentDetector = new TMS_McmasterDetector; 
	  }
	  break;
	}

  case CONTROL_INC_DETECTION_APID:
	{
	  cout << endl << "APID incident detection Logic used." << endl;
	  theApidPrmTable = TMS_ApidPrmTable::parseApidParameters();
	  if(theApidPrmTable) {
		theApidPrmTable->print();
		theApidPrmTable->assignPrmGrpstoStations();
		theIncidentDetector = new TMS_ApidDetector;
	  }
	  break;
	}

  default:
	break;
  }
}
