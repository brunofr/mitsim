/*-*-c++-*------------------------------------------------------------
 * TMS_McmasterPrmParser.y
 *
 * Sreeram Radhakrishnan
 * Copyright (C) 1997
 * Massachusetts Institue of Technology
 * All Rights Reserved
 *
 *--------------------------------------------------------------------
 */

%name TMS_McmasterPrmBisonParser
%define ERROR_BODY =0
%define LEX_BODY =0

%define MEMBERS   TMS_McmasterPrmScanner pts; TMS_McmasterPrmTable *ptr1; 
 
%define CONSTRUCTOR_PARAM const char *f

%define CONSTRUCTOR_INIT : pts(f), ptr1(NULL)

%union {
      int    itype;
      float  ftype;
      char  *stype;
}

%token <itype> TMS_McmasterPrm_COL
%token <stype> TMS_McmasterPrm_OB
%token <stype> TMS_McmasterPrm_CB
%token <itype> TMS_McmasterPrm_INT
%token <ftype> TMS_McmasterPrm_REAL
%token <stype> TMS_McmasterPrm_STRING
%token <stype> TMS_McmasterPrm_NAME

%header{
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <string>
using namespace std;

#include <IO/Exception.h>
#include <Tools/ToolKit.h>  

#include "TMS_McmasterParameters.h"
#include "TMS_McmasterPrmTable.h"

#undef yyFlexLexer
#define yyFlexLexer TMS_McmasterPrmFlexLexer
#include <FlexLexer.h>

   class TMS_McmasterPrmScanner : public TMS_McmasterPrmFlexLexer
   {
      private:

	 char *name_;			// file name

      public:

	 TMS_McmasterPrmScanner() : name_(NULL), TMS_McmasterPrmFlexLexer() { }
	 TMS_McmasterPrmScanner(const char *fname)
	    : name_(Copy(fname)),
	      TMS_McmasterPrmFlexLexer() {
		 if (ToolKit::fileExists(fname)) {
		    switch_streams(new ifstream(fname), 0);
		 } else {
		    cerr << "Error:: cannot open input file <"
				 << name_ << ">" << endl;
		    theException->exit();
		 }
	 }
	 ~TMS_McmasterPrmScanner() { delete [] name_; delete yyin; }

	 char* name() const { return name_; }

	 char* removeDelimeters(const char *deli="\"\"") {
	    char *s, *str = (char *)YYText();
	    if (*str == deli[0]) str ++;
	    s = str;
	    while (*s != '\0' && *s != deli[1]) s ++;
	    *s = '\0';
	    return (str);
	 }

	 char* value() { return (char*)YYText(); }

	 void errorQuit(int err) {
	    if (err == 0) return;
	    cerr << "Problem in parsing"
		 << " (" << name() << ":" << lineno() << ")" << endl;
	    if (err < 0) theException->exit();
	 }
   };
%}

//--------------------------------------------------------------------
// Symbols used in Sensor Parameter grammer rules.
//--------------------------------------------------------------------

%type <stype> ob cb polycoeffs 
%type <itype> prmgrpnumber inum degreeofinterpolation totalprmgrps 
%type <ftype> fnum float_num ocmax avspeed significancelevel    

%type <itype> multiparamgrp paramgrp sensorlist totalsensors
%type <itype> sensor_id sensorids
%type <stype> allthedata
%start everything

%%

//--------------------------------------------------------------------
// Beginning of TMS_McmasterParameters grammer rules.
//--------------------------------------------------------------------

everything: totalprmgrps degreeofinterpolation significancelevel incpersistency normalpersistency allthedata 
{
   cout << endl << "Finished Parsing <"
		<< pts.name() << ">." << endl;
}
;

totalprmgrps: inum 
{
   int n = ($1 > 0) ? ($1) : 0;
   ptr1->assigntotalPrmGrps(n);	
   ptr1->prmGrps().reserve(n);
}
;

degreeofinterpolation: inum
{
   int n = ($1 > 0) ? ($1) : -99;
   ptr1->assigndegreeOfInterpolation(n);
}
;

significancelevel: fnum 
{
   float f = ($1 > 0) ? ($1) : -99;
   ptr1->assignsignificanceLevel(f);
}
;

incpersistency: inum 
{
   int f = ($1 > 0) ? ($1) : -99;
   ptr1->assignincPersistency(f);
}
;

normalpersistency: inum 
{
   int f = ($1 > 0) ? ($1) : -99;
   ptr1->assignnormalPersistency(f);
}
;

allthedata: ob multiparamgrp cb 
{
}
;

multiparamgrp: paramgrp
| multiparamgrp paramgrp
;

paramgrp: prmgrpnumber ob polycoeffs ocmax avspeed vcrit ob sensorlist cb cb
;

prmgrpnumber: inum
{
   int n = ($1 >= 0) ? ($1) : -99;

   TMS_McmasterParameters *p = new TMS_McmasterParameters;
   ptr1->prmGrps().push_back(p);

   ptr1->last()->assignprmGrpID(n);
}
;

polycoeffs: fnum fnum fnum fnum fnum
{
   int dim = ptr1->degreeOfInterpolation() + 1;
   ptr1->last()->assignpolyCoeffs(dim,$1,$2,$3,$4,$5);
}
;

ocmax: fnum
{
   float f = $1;
   ptr1->last()->assignocMax(f);
}
;

avspeed: fnum
{
   double f = $1;
   ptr1->last()->assignavSpeed(f);
}
;

vcrit: fnum
{
   double f = $1;
   ptr1->last()->assignvCrit(f);
}
;

sensorlist: totalsensors sensorids
;

totalsensors: inum
{
   ptr1->last()->assignlistdim($1);
}
;

sensorids: sensor_id
| sensorids sensor_id
;

sensor_id: inum
{
   ptr1->last()->addsensorid($1);
}
;

//--------------------------------------------------------------------
// End of TMS_McmasterParameters grammer rules.
//--------------------------------------------------------------------


//--------------------------------------------------------------------
// Beginning of basic grammer rules.
//--------------------------------------------------------------------


ob: TMS_McmasterPrm_OB
;

cb: TMS_McmasterPrm_CB
;

inum: TMS_McmasterPrm_INT
{
   $$ = atoi(pts.value());
}
;

fnum: float_num
| inum
{
   $$ = $1;
}
;

float_num: TMS_McmasterPrm_REAL
{
   $$ = atof(pts.value());
}
;


//--------------------------------------------------------------------
// End of basic grammer rules
//--------------------------------------------------------------------


%%

//--------------------------------------------------------------------
// Following pieces of cpte will be verbosely copied into the parser.
//--------------------------------------------------------------------

%header{

   class TMS_McmasterPrmParser : public TMS_McmasterPrmBisonParser
   {
      public:
	 TMS_McmasterPrmParser(const char *fname)
	    : TMS_McmasterPrmBisonParser(fname) { }
	 ~TMS_McmasterPrmParser() { }

	 char* name() const { return pts.name(); }

	 void yyerror(char *msg) {
	    cerr << "\nError:: " << msg
			 << " (" << name() << ":"
			 << pts.lineno() << ")" << endl;
	    theException->exit();
	 }

	 int yylex() { return pts.yylex(); }

	 void parse(TMS_McmasterPrmTable *pt){
	   cout << "Parsing Sensor Parameter file<";
	   cout << name() << "> ..." << endl;;
	   ptr1 = pt;
	   yyparse();
	 }
   };
%}
