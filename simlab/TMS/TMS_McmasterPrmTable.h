//-*-c++-*------------------------------------------------------------
// TMS_McmasterPrmTable.h
//
// Sreeram Radhakrishnan
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef TMS_MCMASTERPRMTABLE_HEADER
#define TMS_MCMASTERPRMTABLE_HEADER

#include <cstdio>
#include <vector>
#include <iostream>

#include "TMS_McmasterParameters.h"

class TMS_McmasterPrmTable
{
   private:

      int incPersistency_;
      int normalPersistency_;
	  int totalPrmGrps_;
	  int degreeOfInterpolation_;
	  double significanceLevel_;

	  std::vector<TMS_McmasterParameters*> prmGrps_;

   public:

	  TMS_McmasterPrmTable() {};  
	  ~TMS_McmasterPrmTable() 
	  {
		 if(name_)
		   {
			 delete [] name_;
			 name_ = NULL;
		   }
	  }
	  static char *name_ ;

	  static inline char **nameptr(){ return &name_ ; }
	  static inline char *name(){ return name_ ; }

	  static TMS_McmasterPrmTable* parseMcmasterParameters();

      void assignPrmGrpstoStations();
	  void print(std::ostream &os = std::cout);

	  TMS_McmasterParameters* last() {
		 if (prmGrps_.size() > 0) {
			return prmGrps_[prmGrps_.size()-1];
		 } else {
			return NULL;
		 }
	  }

	  std::vector<TMS_McmasterParameters*> &prmGrps() {
		 return prmGrps_;
	  }

	  TMS_McmasterParameters * prmGrps(int i) {
		 return prmGrps_[i];
	  }

      TMS_McmasterParameters * &getprmgrpptr(int i) {
         return prmGrps_[i];
	  }

	  void assigntotalPrmGrps(int n);
	  void assigndegreeOfInterpolation(int n);
	  void assignsignificanceLevel(double f);
      void assignincPersistency(int n);
      void assignnormalPersistency(int n);

      inline int degreeOfInterpolation() { return degreeOfInterpolation_;}
      inline int incPersistency() { return incPersistency_;}
      inline int normalPersistency() { return normalPersistency_;}
};

extern TMS_McmasterPrmTable *theMcmasterPrmTable;

#endif
