//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_Engine.C
// DATE: Fri Nov 17 10:12:59 1995
//--------------------------------------------------------------------

#include <cstring>
#include <new>
using namespace std;

#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <Tools/SimulationClock.h>

#include <IO/Exception.h>

#include <Tools/Math.h>
#include <Tools/Options.h>
#include <Tools/Random.h>

#include <TC/TC_ControlSystem.h>
#include <GDS/GDS_Glyph.h>

#include "TMS_Communicator.h"
#include "TMS_Engine.h"
#include "TMS_Status.h"
#include "TMS_Network.h"
#include "TMS_Incident.h"
#include "TMS_Sensor.h"
#include "TMS_FileManager.h"
#include "TMS_Setup.h"
#include "TMS_Exception.h"
#include "TMS_Guidance.h"
#include "TMS_Parameter.h"
#include "TMS_CtrlLogic.h"
#include "TMS_McmasterPrmTable.h"
#include "TMS_ApidPrmTable.h"
#include "TMS_IncidentDetect.h"

#ifdef INTERNAL_GUI
#include <Xmt/Xmt.h>
#include <Xmt/Dialogs.h>
#include <DRN/DRN_DrawingArea.h>
#include <DRN/DRN_MapFeature.h>
#include <DRN/DRN_ViewMarker.h>
#include "TMS_Symbols.h"
#include "TMS_Menu.h"
#include "TMS_Interface.h"
#include "TMS_Modeline.h"
#endif

TMS_Engine * theEngine = NULL;

TMS_Engine::TMS_Engine()
   : SimulationEngine(),
	 source_(0),
     controlLogic_(0),
     information_(INFORMATION_HISTORICAL),
     iteration_(0),
     nTotalIterations_(1),
     routingTableDelay_(0.0),
	 routingTableDueTime_(ONE_DAY),
     routingTableTime_(ONE_DAY),
     pathStepSize_(ONE_DAY),
     rollingStepSize_(ONE_DAY),
	 roll_(0),
     rollingLength_(0),
     rollingTime_(ONE_DAY),
	 incLogFpo_(NULL)
{
   theException = new TMS_Exception("TMS");

   theEngine = this;  
   if (!theSimulationClock) {
      theSimulationClock = new SimulationClock;
   }

   theCommunicator = new TMS_Communicator(MSG_TAG_TMS);

   theSimulationClock->init(28800.0, 32400.0, 0.1);

   // Default parameter filename

   Copy(theParameter->nameptr(), "ctrlpara.dat");
}



const char *TMS_Engine::ext()
{
   return ".tms";
}


int
TMS_Engine::loadMasterFile()
{
   if (SimulationEngine::loadMasterFile() != 0) {
      const char *msg;
      msg = Str("Error:: Cannot find the master file <%s>.", master());
#ifdef INTERNAL_GUI
	  Boolean ok;
  	  XmtAskForBoolean(theInterface->widget(),	// widget
					   "masterNoFound",  // query_name for customize
					   msg, // prompt_default
					   "Continue",	// yes_default
					   "Quit",	// no_default
					   NULL,	// cancel_default
					   XmtNoButton, // default_button
					   0,		// icon_type_default
					   0,		// show_cancel_button
					   &ok,		// value_return
					   "Check if the file exist.");	// help_text_default
	  if (!ok) {
		 quit(STATE_ERROR_QUIT);
      }
#else
      cerr << msg << endl;
      quit(STATE_ERROR_QUIT);
#endif
      return 1;
   }

#ifdef INTERNAL_GUI
   theInterface->showBusyCursor ();
#endif

   theFileManager->readMasterFile();
   state_ = STATE_OK;

   // if (::GetUserName()) theException->exit(1);
   // ::GetUserName();

#ifdef INTERNAL_GUI
   theInterface->tellLoadingIsDone();
   theInterface->showDefaultCursor ();
#endif

   return 0;
}


// This function is called every time a new master file is loaded.
// It return 0 if no error.

int
TMS_Engine::loadSimulationFiles()
{
   if (state_ == STATE_NOT_STARTED) {
	  if (loadMasterFile() != 0) return 1;
   }

  int err_no = ToolKit::makedirs();
  if (err_no) {
#ifdef INTERNAL_GUI
	theInterface->msgSet("Cannot create directory ");
	if (err_no & 0x1) theInterface->msgAppend(ToolKit::outdir()) ;
	else if (err_no & 0x2) theInterface->msgAppend(ToolKit::workdir()) ;
#else
	theException->exit(STATE_ERROR_QUIT) ;
#endif
	return 1;
  }

#ifdef INTERNAL_GUI
   theInterface->showBusyCursor ();
#endif

   init();

   theStatus->openLogFile();

   ::ParseParameters();

   ::ParseNetwork();

   TMS_Sensor::loadDatabases();

   ::ParseIncidentDetectPrms();

   ::SetupMiscellaneous();

   start();

#ifdef INTERNAL_GUI
   theInterface->setTitle(theFileManager->title());
   theDrawingArea->setState(DRN_DrawingArea::REDRAW |
							DRN_DrawingArea::NETWORK_LOADED);
   theDrawingArea->redraw();
   theInterface->showDefaultCursor ();
   theInterface->updateButtons();
   theInterface->handlePendingEvents();
#endif

   // Make connection to other sibling modules if they exist and send
   // timing date to simlab.

   if (theCommunicator->isSimlabConnected()) {
#ifdef INTERNAL_GUI
	  const char *msg = XmtLocalize2(theInterface->widget(),
									 "Waiting data from other module(s).", "prompt", "waiting");
	  theInterface->msgSet(msg);
#endif	  
	  theCommunicator->makeFriends();
#ifdef INTERNAL_GUI
	  theInterface->msgClear();
#endif
   }

   return 0;
}


int TMS_Engine::isWaiting()
{
   if (SimulationEngine::isWaiting()) {
	  return 1;
   } else if (theSimulationClock->currentTime() >= routingTableDueTime_) {
	  return 1;
   }
   return 0;
}


void
TMS_Engine::init()
{
   theSimulationClock->init();
   double s = theSimulationClock->currentTime();

   rollingTime_ = s + rollingStepSize_;
   batchTime_ = s;

   TMS_Sensor::startTime_ = (long) s;
   TMS_Sensor::endTime_   = (long) s;

   Random::create(1);
}


// This procedure starts the simulation loops.

void TMS_Engine::run()
{
   if (canStart()) {
      loadMasterFile();
   }

#ifdef INTERNAL_GUI

   // In graphical mode, the simulation is started manually. When the
   // simulation is done (or cancelled), the quit() is called
   // automatically.

   theInterface->mainloop();

#else  // batch mode

   loadSimulationFiles();

   while (state_ >= 0 && (receiveMessages() > 0 ||
						  isWaiting() &&
						  receiveMessages(NO_MSG_WAITING_TIME) >= 0 ||
						  simulationLoop() >= 0)) {
      ;
   }
#endif // !INTERNAL_GUI
}


void
TMS_Engine::quit(int state)
{
   theFileManager->closeOutputFiles();

   theStatus->report();

   SimulationEngine::quit(state);

   theStatus->closeLogFile();

#ifdef INTERNAL_GUI
   if (theMenu->isSaveNeeded()) {
	  Boolean yes;
	  XmtAskForBoolean(theInterface->widget(),
					   "isSaveNeeded", // reource querey name
					   "Save changes?", // defaul prompt
					   "Save",	// yes default
					   "Discard",	// no default
					   "Cancel",	// cancel default
					   XmtYesButton, // default button
					   0,	// default icon type
					   False, // show cancel button
					   &yes,
					   NULL);
	  if (yes) {
		 theMenu->save();
	  }
   }
#endif

   theException->done(0);
}


void
TMS_Engine::toggleControlLogic(int flag)
{
   if (controlLogic_ & flag)
      controlLogic_ &= ~flag;	// turn off
   else
      controlLogic_ |= flag;	// turn on
}


// This function is called by ::TimeOutCallback(), defined in
// <DrawableRoadNetwork/DRN_Application.C>, in graphical mode or a
// while loop of main() in batch mode.  Graphics related functions
// should be placed inside INTERNAL_GUI block.

int
TMS_Engine::simulationLoop()
{
   static int first = 1;
   SimulationClock *clock = theSimulationClock;
   double now = clock->currentTime();

#ifdef INTERNAL_GUI
   static double db_time = 0.0;
#endif

   if (first) {

      first = 0;
	  
	  theParameter->checkVisibility();
	  resetBreakPoint(now);

#ifdef INTERNAL_GUI
      if (TMS_Sensor::dbSpeedFile_ ||
		  TMS_Sensor::dbFlowFile_) {
		 db_time = now;
      } else {
		 db_time = DBL_INF;
      }
#endif

	  if (theCommunicator->isMesoConnected()) {
		 char *inc_logfile = createFilename("inc");
		 if ((incLogFpo_ = fopen(inc_logfile, "w")) == NULL) {
			cerr << "Error:: Failed open output file <"
				 << inc_logfile << ">" << endl;
			theException->exit();
		 }
		 free(inc_logfile);
	  }
   }


#ifdef INTERNAL_GUI
   theInterface->handlePendingEvents();
#endif

   // Sychronize with the master clock

   if (now >= clock->masterTime()) {

	  // TMS has completed the tasks assigned so far. Wait until the
	  // master clock time is renewed by the message received from
	  // SIMLAB.

	  return (state_ = STATE_WAITING);
   }

#ifdef INTERNAL_GUI
   if (isTimeToBreak(now)) {
	 const char *msg;
	 msg = Str("Break point at %s",  clock->currentStringTime());
	  XmtDisplayWarningAndAsk(theInterface->widget(), "bpsDialog",
		 msg,
		 "Continue", "Quit", XmtYesButton,
		 "\nClick @fBContinue@fR to proceed or @fBQuit@fB to\n"
		 "Terminate the simulation");
	  nextBreakPoint(now + clock->stepSize());
   }
#endif

   // Routing table "update" time is DBL_INF until it is set to "due"
   // time when a given number of MSG_MESO_DONE messages have been
   // received.

   if (now >= routingTableTime_) {

#ifdef INTERNAL_GUI
	  static const char *fmt = "Prediction for %s completed.";
	  char *t1 = Copy(clock->convertTime(beginTime_)) ;
	  char *t2 = Copy(clock->convertTime(endTime_)) ;
	  const char *tw = Str("%s-%s", t1, t2);
	  StrFree(t1);
	  StrFree(t2);

	  if (theEngine->mode(MODE_DEMO_PAUSE)) {
		 XmtDisplayWarningMsgAndWait(theInterface->widget(), "dtaEnd",
			fmt,				// default msg
			"TMS",				// default title
			NULL,				// default help
			tw);			// args
	  } else if (theEngine->mode(MODE_DEMO)) {
		 XmtDisplayWarningMsg(theInterface->widget(), "dtaEnd",
			fmt,
			"TMS",				// default title
			NULL,				// default help
			tw);			// args
	  }
	  theInterface->msgClear();
	  XmtMsgLinePrintf(theInterface->msgline(), fmt, tw);
#endif
      // Tell MITSIM to switch to the new routing table

      theCommunicator->mitsim() << MSG_MESO_DONE
								<< beginTime_
								<< endTime_;
      theCommunicator->mitsim().flush(SEND_IMMEDIATELY);

      routingTableTime_ = DBL_INF;
   }

   // Call MesoTS to predict the congestion and calculate the dynamic
   // shortest path trees.  We send a message to ask MITSIM to send
   // the current state of the network to MesoTS.  When MesoTS
   // receives network state, it starts to run and will send a
   // MSG_MESO_DONE to TMS.

   if (theCommunicator->isMitsimConnected() &&
	   (theCommunicator->isMesoConnected() ||
		theCommunicator->isMdiConnected()) &&
	   now >= rollingTime_) {	// was now + 1

	  roll_ ++;
	  iteration_ = 1;
	  beginTime_ = now; // was now + 1;
	  endTime_ = beginTime_ + rollingLength_;
	  routingTableDueTime_ = beginTime_ + routingTableDelay_;

#ifdef INTERNAL_GUI
	  static const char *fmt = "(%d) Prediction for %s started.";
	  char *t1 = Copy(clock->convertTime(beginTime_)) ;
	  char *t2 = Copy(clock->convertTime(endTime_)) ;
	  const char *tw = Str("%s-%s", t1, t2);
	  StrFree(t1);
	  StrFree(t2);

	  if (theEngine->mode(MODE_DEMO)) {
		 XmtDisplayWarningMsgAndWait(theInterface->widget(), "dtaStart",
			fmt,
			"TMS",				// default title
			NULL,				// default help
			roll_, tw);			// args
	  } else if (theEngine->mode(MODE_DEMO)) {
		 XmtDisplayWarningMsg(theInterface->widget(), "dtaStart",
			fmt,
			"TMS",				// default title
			NULL,				// default help
			roll_, tw);			// args
	  }
	  theInterface->msgClear();
	  XmtMsgLinePrintf(theInterface->msgline(), fmt, roll_, tw);
#endif

	  theCommunicator->startStateEstimation();

	  rollingTime_ = beginTime_ + rollingStepSize_;

	  if (clock->stopTime() - rollingTime_ < 5.0) {

		 // skip the last run scheduled just before terminating the
		 // simulation
		 
		 rollingTime_ = ONE_DAY;
	  }
   }


   if (theEngine->controlLogic() & CONTROL_INCIDENT_RESPONSE) {
      CheckIncidents();
   }

   // Load new signal plans

   while (now >= TC_ControlSystem::nextTime()) {
      TC_ControlSystem::loadControllers();
      TC_ControlSystem::readTime();
   }

   TC_ControlSystem::controllersSwitchIntervals();

   if (now >= batchTime_) {

#ifndef INTERNAL_GUI
      theStatus->print();
#endif
      batchTime_ = now + batchStepSize_;
   }

#ifdef INTERNAL_GUI

   if (now >= db_time) {
      tmsNetwork()->calcSegmentStates();
      tmsNetwork()->interpolateSegmentStates();
      
      // This line is tempoary
      tmsNetwork()->updateSegmentParameters();

      tmsNetwork()->drawNetwork(theDrawingArea);
      db_time = now + TMS_Sensor::dbStepSize_;
   }

   theStatus->showStatus();

   theInterface->handlePendingEvents();

#endif /* !INTERNAL_GUI */

   // Advance the clock

   clock->advance();

   // Tell the master controller that this cycle is done

   theCommunicator->sendNextTimeToSimlab();

   if (theCommunicator->isSimlabConnected()) {
	  return (state_ = STATE_WAITING);
   } else if (now >= clock->stopTime()) {
      return (state_ = STATE_DONE);
   } else {
	  return (state_ = STATE_OK);
   }
}

// Process the messages received from other processes

int
TMS_Engine::receiveMessages(double sec)
{
   if (!theCommunicator->isSimlabConnected()) return 0;
   else return theCommunicator->receiveMessages(sec);
}


// Modify the experimental guidance based on most recent
// prediction. Save the result if it is the best found so far.

void
TMS_Engine::modifyGuidance(const char *state_tag)
{
   static double min_error = DBL_INF;
   float alpha = theParameter->pathAlpha();

   if (alpha < 1.0E-5) {		// use MSA like algorithm
	  alpha = 1.0 / (1.0 + iteration_);
   }

   if (iteration_ == 1) {			// this is the 1st sub-run
	  min_error = DBL_INF;
   }

   char *o;

   // Guidance used as input in the previous MESO sub-run (predicted
   // travel times)

   char *i = createFilename("ltt", 0);

   if (chosenOutput(0x10000000)) {

	  // save input guidance for debugging

	  char *os;
	  o = createFilename("ltt");
	  os = StrCopy("%s.%d-%d", o, roll_, iteration_);
	  if (ToolKit::copyFile(i, os) != 0) {
		 theException->exit();
	  }
	  free(o);
	  StrFree(os);
   }

   // Read the input guidence into a table

   TMS_Guidance table(i);

   // Output of the previous MESO subrun (predicted flows and travel
   // tiems)

   o = createFilename("lft");

   if (chosenOutput(0x10000000)) {

	  // Save prediction result if desired

	  char *os = StrCopy("%s.%d-%d", o, roll_, iteration_);
	  if (ToolKit::copyFile(o, os) != 0) {
		 theException->exit();
	  }
	  StrFree(os);
   }

   // Calculate the measure of consistency and update the guidance to
   // be used in the next run.

   TG_Evaluation e = table.modify(o, alpha);

   free (o);

   theStatus->osLogFile()
	  << Fix(beginTime_, 1.0) << endc
	  << roll_ << endc << iteration_ << endc
	  << Fix(e.consistency_, 0.01) << endc
	  << Fix(e.optimality_, 0.01) << endl;
   theStatus->nMsgs(1);

   // Save the new guidance if it is the "best" found so far

   if (e.consistency_ < min_error) {

	  min_error = e.consistency_;

	  // Not needed for the 1st subrun because file i is just a copy
	  // of o file (see TMC_Communicator.C).

	  if (iteration_ > 1) {

		 // This one is better, save it.

		 o = createFilename("ltt");
		 if (rename(i, o) != 0) {
			theException->exit();
		 }
		 delete [] o;
	  }

	  if (!IsEmpty(state_tag)) { // save the state
		const char *tag;
		char *fn1, *fn2;
		int sn1, sn2;

		tag = ToolKit::outfile(state_tag);
		sn1 = strlen(tag);
		fn1 = new char [sn1+5];
		Copy(fn1, tag);

		tag = ToolKit::outfile("t3d");
		sn2 = strlen(tag);
		fn2 = new char [sn2+5];
		Copy(fn2, tag);
		
		Copy(fn1+sn1, ".spd");
		Copy(fn2+sn2, ".spd");
		rename(fn1, fn2);

		Copy(fn1+sn1, ".dsy");
		Copy(fn2+sn2, ".dsy");
		rename(fn1, fn2);

		Copy(fn1+sn1, ".flw");
		Copy(fn2+sn2, ".flw");
		rename(fn1, fn2);

		delete [] fn1;
		delete [] fn2;
	  }
   }

   // Create input for the next run

   table.printLinkTimeTable(i);

   delete [] i;
}


void
TMS_Engine::saveState3d(const char *tag)
{
  if (IsEmpty(tag)) return;
  static const char *s = "p3d";
  theNetwork->append_3d_state(tag, s);
  if (s) {						// first time
	if (theCommunicator->isMdiConnected()) {
	  theCommunicator->mdi() << MSG_STATE_START << s;
	  theCommunicator->mdi().flush(SEND_IMMEDIATELY);
	}
    s = 0;
  }
}

// Save the master file

void TMS_Engine::save(ostream &os)
{
   os << "/* " << endl
	  << " * TMS master file" << endl
	  << " */" << endl << endl;

   os << "[Title] = \"" << theFileManager->title() << "\"" << endl
	  << endl;

   os << "[Default Parameter Directory]     = \""
	  << NoNull(ToolKit::paradir())
	  << "\"" << endl;
	 
   os << "[Input Directory]                 = \""
	  << NoNull(ToolKit::indir())
	  << "\"" << endl;

   os << "[Output Directory]                = \""
	  << NoNull(ToolKit::outdir())
	  << "\"" << endl;

   os << "[Working Directory]               = \""
	  << NoNull(ToolKit::workdir())
	  << "\"" << endl;

   os << endl;

   os << "[Network Database File]           = \""
	  << NoNull(RoadNetwork::name())
	  << "\"" << endl;

#ifdef INTERNAL_GUI
   os << "[GDS Files]                       = {" << endl
	  << "%\tFilename \tMinScale \tMaxScale" << endl;
   if (theMapFeatures) {
	  GDS_Glyph *glyph;
	  for (int i = 0; i < theMapFeatures->nGlyphs(); i ++) {
		 glyph = theMapFeatures->glyph(i);
		 os << "\t\"" << glyph->name() << "\""
			<< "\t" << glyph->minScale()
			<< "\t" << glyph->maxScale()
			<< endl;
	  }
   }
   os << "}" << endl;
#endif

   os << "[Parameter File]                  = \""
	  << NoNull(theParameter->name())
	  << "\"" << endl;

   os << "[Control Logic File]              = \""
	  << NoNull(TMS_CtrlLogic::name())
	  << "\"" << endl;

   os << "[Signal Plan File]                = \""
	  << TC_ControlSystem::name()
	  << "\"" << endl;

   os << "[Control Logic]                   = "
	  << controlLogic_ << endl
	  << "%  0x00 = None" << endl
	  << "%  0x01 = A1 incident response" << endl
	  << "%  0x02 = Gating logic" << endl
	  << "%  0x10 = McMaster incident detection" << endl
	  << "%  0x20 = APID incident detection" << endl;

   os << "[Information]                     = "
	  << information_ << endl
	  << "%   0 = Historical data" << endl
	  << "%   1 = Real time measurement" << endl
	  << "%   2 = Prediction" << endl;

   if (information_ == 2) {
	  os << "[Rolling Step Size]               = "
		 << rollingStepSize_ << endl;
	  os << "[Rolling Length]                  = "
		 << rollingLength_ << endl;
	  os << "[Number of DTA Interations]       = "
		 << nTotalIterations_ << endl;
	  os << "[DTA Computational Delay]         = "
		 << routingTableDelay_ << endl;
   }

   if (TMS_McmasterPrmTable::name())
	 os << "[McMaster Parameters File]\t\t  = " 
		<< "\"" << TMS_McmasterPrmTable::name() << "\"" << endl;

   if (TMS_ApidPrmTable::name())
	 os << "[Apid Parameters File]\t\t  = " << "\""
		<< TMS_ApidPrmTable::name() << "\"" <<  endl;

   if (TMS_IncidentDetector::logfname())
	 os << "[Incident Log File]\t\t  = " << "\""
		<< TMS_IncidentDetector::logfname() << "\"" <<  endl;

   os << endl;

   os << "[Start Time]                      = "
	  << theSimulationClock->startStringTime() << endl;

   os << "[Stop Time]                       = "
	  << theSimulationClock->stopStringTime() << endl;

   os << "[Step Size]                       = "
	  << Fix(theSimulationClock->stepSize(), 0.1) << endl;

   os << endl;

   os << "[Console Message Step Size]       = "
	  << Fix(batchStepSize_, 0.1) << endl;

   os << endl;

#ifdef INTERNAL_GUI

   os << "[Segments] = "
	  << theSymbols()->segmentColorCode().value() << endl
	  << "%   0 = Link type" << endl
	  << "%   1 = Density" << endl
	  << "%   2 = Speed" << endl
	  << "%   3 = Flow" << endl << endl;

   os << "[Signals]  = 0x" << hex
	  << theDrnSymbols()->signalTypes().value() << dec << endl
	  << "%   0x01 = Traffic signals" << endl
	  << "%   0x02 = Portal signals" << endl
	  << "%   0x04 = Variable speed limit signs" << endl
	  << "%   0x08 = Variable message signs" << endl
	  << "%   0x10 = Lane use signs" << endl
	  << "%   0x20 = Ramp meters" << endl << endl;

   os << "[Sensor Types] = 0x" << hex
	  << theSymbols()->sensorTypes().value() << dec << endl
	  << "%   0x1 = Loop detectors" << endl
	  << "%   0x2 = AVI sensors" << endl
	  << "%   0x4 = Area sensors" << endl << endl;

   os << "[Sensor Color Code]  = "
	  << theSymbols()->sensorColorCode().value() << endl
	  << "%   0 = Count" << endl
	  << "%   1 = Flow" << endl
	  << "%   2 = Speed" << endl
	  << "%   3 = Occupancy" << endl << endl;
   

#ifdef INTERNAL_GUI
  DRN_ViewMarker::SaveMarkers(os) ;
#endif

   theOptions->printOptions(os);

#endif

}
