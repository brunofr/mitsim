//-*-c++-*------------------------------------------------------------
// TMS_ApidPrmTable.h
//
// Sreeram Radhakrishnan
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef TMS_APIDPRMTABLE_HEADER
#define TMS_APIDPRMTABLE_HEADER

#include <cstdio>
#include <iostream>
#include <vector>

#include "TMS_ApidParameters.h"

class TMS_ApidPrmTable
{
private:

  int totalPrmGrps_;
  int incPersistency_;
  int compWaveTestPeriod_;
  int compWavePeriodCnt_;

  int persistTestEnabled_;
  int medTrfEnabled_;
  int ligtTrfEnabled_;

  std::vector<TMS_ApidParameters*> prmGrps_;

public:

  TMS_ApidPrmTable() {};  
  ~TMS_ApidPrmTable() 
	{
	  if(name_)
		{
		  delete [] name_;
		  name_ = NULL;
		}
	}

  static char *name_ ;

  static inline char **nameptr(){ return &name_ ; }
  static inline char *name(){ return name_ ; }

  static TMS_ApidPrmTable* parseApidParameters();

  void assignPrmGrpstoStations();
  void print(std::ostream& os = std::cout);

  TMS_ApidParameters* last() {
	if (prmGrps_.size() > 0) {
	  return prmGrps_[prmGrps_.size()-1];
	} else {
	  return NULL;
	}
  }

  std::vector<TMS_ApidParameters*> &prmGrps() {
	return prmGrps_;
  }

  TMS_ApidParameters * prmGrps(int i) {
	return prmGrps_[i];
  }

  TMS_ApidParameters * &getprmgrpptr(int i) {
	return prmGrps_[i];
  }

  void assigntotalPrmGrps(int n);
  void assignincPersistency(int n);
  void assignnormalPersistency(int n);
  void assigncompWaveTestPeriod(int n);

  int incPersistency();
  int normalPersistency();
  int compWaveTestPeriod();
};

extern TMS_ApidPrmTable *theApidPrmTable;

#endif
