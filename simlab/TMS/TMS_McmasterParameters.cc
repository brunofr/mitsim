//-*-c++-*------------------------------------------------------------
// TMS_McmasterParameters.cc
//
// Sreeram Radhakrishnan
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <stdio.h>
#include <math.h>

#include <Tools/ToolKit.h>
#include <IO/Exception.h>
#include "TMS_McmasterParameters.h"
#include "TMS_McmasterPrmParser.h"

void TMS_McmasterParameters::assignpolyCoeffs(
   int dim,double p1,double p2,double p3,double p4,double p5)
{
   polyCoeffs_.reserve(dim); 

   polyCoeffs_.push_back(p1);
   polyCoeffs_.push_back(p2);
   polyCoeffs_.push_back(p3);
   polyCoeffs_.push_back(p4);
   polyCoeffs_.push_back(p5);
};

void TMS_McmasterParameters::assignprmGrpID(int n)
{
   prmGrpID_ = n;
};
void TMS_McmasterParameters::assignocMax(double f)
{
   ocMax_ = f;
};

void TMS_McmasterParameters::assignavSpeed(double f)
{
   avSpeed_ = f;
};

void TMS_McmasterParameters::assignvCrit(double f)
{
   vCrit_ = f;
};

void TMS_McmasterParameters::assignlistdim(int n)
{
   sensorList_.reserve(n);
};

void TMS_McmasterParameters::addsensorid(int n)
{
   sensorList_.push_back(n);
};

void TMS_McmasterParameters::print(ostream &os,int jmax)
{
   register int j;
   os << indent << prmGrpID_ << endc << OPEN_TOKEN;
   for (j = 0; j < jmax; j++) {
	  os << endc << polyCoeffs_[j];
   }
   os << endc << ocMax_ ;
   os << endc << avSpeed_ << endc << vCrit_ << endc << OPEN_TOKEN;
   os << endc << sensorList_.size();
   for (j = 0; j < sensorList_.size(); j++) {
	 os << endc << sensorList_[j];
   }
   os << endc << CLOSE_TOKEN << endc;
   os << CLOSE_TOKEN << endl;
};

double TMS_McmasterParameters::calculateFlow(double occupancy)
{
   int degreeofinterpolation = polyCoeffs_.size() - 1;   
   double sum=0;

   for(register int i = degreeofinterpolation; i >= 0; -- i) {
	   sum += polyCoeffs_[degreeofinterpolation - i] * pow(occupancy,i);
   }
   return sum;
};

