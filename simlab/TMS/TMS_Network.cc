//-*-c++-*------------------------------------------------------------
// NAME: Microcscopic Traffic Simulator
// AUTH: Qi Yang
// FILE: TMS_Network.C
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#include <new>
using namespace std;

#include <Tools/Math.h>

#include <Tools/SimulationClock.h>
#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <Tools/Random.h>

#include <IO/IOService.h>

#include <TC/TC_Sensor.h>

#include "TMS_Engine.h"
#include "TMS_Network.h"
#include "TMS_Node.h"
#include "TMS_Link.h"
#include "TMS_Segment.h"
#include "TMS_SurvStation.h"
#include "TMS_Lane.h"
#include "TMS_Status.h"
#include "TMS_FileManager.h"
#include "TMS_Parameter.h"
#include "TMS_TollBooth.h"
#include "TMS_Incident.h"
#include "TMS_Signal.h"
#include "TMS_Sensor.h"

#ifdef INTERNAL_GUI
#include <DRN/DRN_DrawingArea.h>
#include "TMS_Interface.h"
#include "TMS_Symbols.h"
#include "TMS_DetectedInc.h"
#endif

TMS_Network::TMS_Network()
#ifdef INTERNAL_GUI
  : DrawableRoadNetwork()
#else  // batch mode
	: RoadNetwork()
#endif // !INTERNAL_GUI
{
}

TMS_Network::~TMS_Network() {
}


RN_Node* TMS_Network::newNode() { 
  return new TMS_Node; 
}

RN_Link* TMS_Network::newLink() { 
  return new TMS_Link;
}

RN_Segment* TMS_Network::newSegment() {
  return new TMS_Segment;
}

RN_Lane* TMS_Network::newLane() {
  return new TMS_Lane;
}

RN_Sensor* TMS_Network::newSensor() {
  return new TMS_Sensor;
}

RN_SurvStation* TMS_Network::newSurvStation() {
  return new TMS_SurvStation;
}

RN_Signal* TMS_Network::newSignal() {
  return new TMS_Signal;
}

RN_TollBooth* TMS_Network::newTollBooth() {
  return new TMS_TollBooth;
}

void
TMS_Network::calcStaticInfo(void)
{
  RoadNetwork::calcStaticInfo();
}


/*
 * Calculate the density for each segments
 */

void
TMS_Network::calcSegmentDensity()
{
  for (register int i = 0; i < nSegments(); i ++) {
	tmsSegment(i)->calcDensity();
  }
}


/*
 * Calculate the speed for each segments
 */

void
TMS_Network::calcSegmentSpeed()
{
  for (register int i = 0; i < nSegments(); i ++) {
	tmsSegment(i)->calcSpeed();
  }
}


/*
 * Calculate the flow for each segments
 */

void
TMS_Network::calcSegmentFlow()
{
  for (register int i = 0; i < nSegments(); i ++) {
	tmsSegment(i)->calcFlow();
  }
}


/*
 * Calculate the density, speed and flow for each segment.
 */

void
TMS_Network::calcSegmentStates()
{
  for (register int i = 0; i < nSegments(); i ++) {
	tmsSegment(i)->calcDensity();
	tmsSegment(i)->calcSpeed();
	tmsSegment(i)->calcFlow();
  }
}


/*
 * Interpolate the state of segments which have no sensors from the
 * upstream and downsteam segments
 */

void
TMS_Network::interpolateSegmentStates(const int max_degree)
{
  int d = 0;
  int modified;
  do {
	d ++;
	modified = 0;
	for (register int i = 1; i < nSegments(); i ++) {
	  modified += tmsSegment(i)->interpolateStateFromNearbySensors(d);
	}
  } while (d < max_degree && modified);
}


void
TMS_Network::updateSegmentParameters()
{
  for (register int i = 0; i < nSegments(); i ++ ) {
	tmsSegment(i)->updateParameters();
  }
}


/*
 * Before receiving sensor data for nect time period, all sensors are
 * reset to zero state.
 */

void
TMS_Network::resetSensorReadings()
{
  for (register int i = 0; i < nSensors(); i ++ ) {
	sensor(i)->resetSensorReadings();
  }
}


#ifdef INTERNAL_GUI

void
TMS_Network::drawNetwork(DRN_DrawingArea * area)
{
  DrawableRoadNetwork::drawNetwork(area);

  if (theSymbols()->viewType().is(DRN_Symbols::VIEW_MICRO)) {
	drawIncidents(area);
  }

  TMS_DetectedInc::drawAllDetectIncidents(area);
}


void
TMS_Network::drawIncidents(DRN_DrawingArea* area)
{
  if (!area->isSignalDrawable()) {
	return;
  }
  inc_list_type::iterator it(theIncidentList);
  TMS_Incident* inc;
  while (it != theIncidentList->end()) {
	inc = *it;
	if (((DRN_Segment*)inc->segment())->isVisible()) {
	  inc->draw(area);	// Show on screen
	}
	it ++;
  } 
}

#endif
