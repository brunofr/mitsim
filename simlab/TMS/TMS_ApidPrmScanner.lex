/*-*-c++-*------------------------------------------------------------
 *  TMS_ApidPrmScanner.h Lexical Analyzer Generator for APID Inc Det algo
 *--------------------------------------------------------------------
 */

%{
#include "TMS_ApidPrmParser.h"
#define YY_BREAK
%}

%option yyclass="TMS_ApidPrmFlexLexer"
%option noyywrap
%option yylineno

C	(#.*)|(%.*)|("//".*)
WS	[\t ,;\n]+

D	[0-9]
N	{D}+
E       [eE][-+]?{D}+

F1      {D}+\.{D}*({E})?
F2      {D}*\.{D}+({E})?
F3      {D}+({E})?

I	[-+]?{N}
R       [-+]?({F1}|{F2}|{F3})
NAME    [[]([A-Za-z \t]*)[]]

%%

"/*"	{	/* skip comments */
  int c;
  while ((c = yyinput()) != 0) {
    if (c == '*') {
      if ((c = yyinput()) == '/') break;
      else unput(c);
    }
  }
  break;
}

{WS}	{ break; }
{C}	{ break; }

[:=]    { return TMS_ApidPrmParser::TMS_ApidPrm_COL; }

"{"	{ return TMS_ApidPrmParser::TMS_ApidPrm_OB; }
"}"	{ return TMS_ApidPrmParser::TMS_ApidPrm_CB; }
		            
{NAME}  { return TMS_ApidPrmParser::TMS_ApidPrm_NAME; }
{I}	{ return TMS_ApidPrmParser::TMS_ApidPrm_INT; }
{R}     { return TMS_ApidPrmParser::TMS_ApidPrm_REAL; }

%%
