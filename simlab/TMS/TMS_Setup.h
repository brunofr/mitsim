//-*-c++-*------------------------------------------------------------
// NAME: Microcscopic Traffic Simulator
// AUTH: Qi Yang
// FILE: TMS_Setup.h
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#ifndef TMS_SETUP_HEADER
#define TMS_SETUP_HEADER

extern void ParseParameters();

extern void ParseNetwork();

extern void SetupScenarios();
extern void SetupMiscellaneous();

extern void ParseIncidentDetectPrms();

#endif
