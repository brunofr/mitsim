//-*-c++-*------------------------------------------------------------
// NAME: Command line arguments parser for TMS
// AUTH: Qi Yang
// FILE: TMS_CmdArgsParser.C
// DATE: Fri Nov 17 17:28:06 1995
//--------------------------------------------------------------------


#include "TMS_CmdArgsParser.h"
#include "TMS_Engine.h"

TMS_CmdArgsParser::TMS_CmdArgsParser()
  : CmdArgsParser()
{
  add(new Clo("-output", &theEngine->chosenOutput_, 0,
			  "Output mask"));
}
