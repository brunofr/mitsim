//-*-c++-*------------------------------------------------------------
// NAME: Traffic Simulation
// NOTE: 
// AUTH: Qi Yang
// FILE: TMS_Guidance.h
// DATE: Mon Dec  9 18:35:50 1996
//--------------------------------------------------------------------

#ifndef TMS_GUIDANCE_HEADER
#define TMS_GUIDANCE_HEADER

#include <iostream>

#include <map>
#include <vector>

#include <Templates/STL_Configure.h>

class Reader;
class TMS_Guidance;
class TG_LinkOutcome;
class TG_LinkInfo;


class TG_Evaluation
{
   public:

	  TG_Evaluation() : consistency_(0.0), optimality_(0) {}
	  ~TG_Evaluation() {}

	  double consistency_;
	  double optimality_;

	  void sum(const TG_Evaluation &i) {
		 consistency_ += i.consistency_;
		 optimality_ += i.optimality_;
	  }
};


class TG_LinkInfo
{
      friend class TMS_Guidance;
      friend class TG_LinkOutcome;

   public:

      TG_LinkInfo() : type_(0), length_(0.0) { }
	  ~TG_LinkInfo() { }

      void read(Reader &is);
	  TG_Evaluation combine(TG_LinkOutcome &, float alpha);
	  void print(std::ostream& os = std::cout);

   protected:

      int type_;
      float length_;
	  std::vector<float ALLOCATOR> cost_;

	  static TMS_Guidance *parent_;
};

class TG_LinkOutcome
{
	  friend class TMS_Guidance;
	  friend class TG_LinkInfo;

   public:

	  TG_LinkOutcome() { }
	  ~TG_LinkOutcome() { }

	  void read(Reader &is);

   protected:  

      std::vector<int ALLOCATOR> count_;
	  std::vector<float ALLOCATOR> cost_;

	  static TMS_Guidance *parent_;
};

class TMS_Guidance
{
	  friend class TG_LinkInfo;
	  friend class TG_LinkOutcome;

   public:

	  TMS_Guidance();
	  TMS_Guidance(const char* file);
		
	  ~TMS_Guidance();

	  void read(const char *file);

	  // Merge: table1 = (1-r) * table1 + r * table2

	  TG_Evaluation modify(
		 const char* o,			// actual flow and travel time
		 float alpha			// weight for predicted travel time
		 );

	  // Implement the best guidance found so far

	  void printLinkTimeTable(const char *g);

   private:

	  char *filename_;
      int iStart_, oStart_;		// start interval
	  int offset_;
      int iNumCols_, oNumCols_;	// number of intervals
      int length_;				// second per interval
      std::map<int, TG_LinkInfo, std::less<int> ALLOCATOR> links_;
};

#endif
