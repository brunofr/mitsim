//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_VslsLogic.h
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------


#ifndef TMS_VSLS_ACTION_HEADER
#define TMS_VSLS_ACTION_HEADER

#include <GRN/RN_CtrlStation.h>

#include "TMS_Action.h"

class TMS_VslsAction : public TMS_Action 
{
   public:

      TMS_VslsAction() : TMS_Action() { }
      ~TMS_VslsAction() { }

      void read(Reader&);
      void print(ostream &os = cout);

      void applyTo(TMS_Incident&);
      int update(CtrlList, int);

   protected:

      int start_;		/* start value */
      int step_;		/* step size */
};

#endif
