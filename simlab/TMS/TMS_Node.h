/*-*-c++-*-******************************************************/
/*    NAME: Traffic Management Simulation                       */
/*    AUTH: Qi Yang                                             */
/*    FILE: TMS_Node.h                                          */
/*    DATE: Sat Jun  3 02:01:51 1995                            */
/****************************************************************/

#ifndef TMS_NODE_HEADER
#define TMS_NODE_HEADER

#ifdef INTERNAL_GUI
#include <DRN/DRN_Node.h>
#else
#include <GRN/RN_Node.h>
#endif

#ifdef INTERNAL_GUI
class TMS_Node : public DRN_Node
#else
class TMS_Node : public RN_Node
#endif
{
   public:

#ifdef INTERNAL_GUI
      TMS_Node() : DRN_Node() { }
#else
      TMS_Node() : RN_Node() { }
#endif

      ~TMS_Node() { }
};

#endif // THE END OF THIS FILE







