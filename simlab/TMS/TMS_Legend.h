//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_Legend.h
// DATE: Sun Feb 11 16:32:40 1996
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#ifndef TMS_LEGEND_HEADER
#define TMS_LEGEND_HEADER

#include <DRN/DRN_Legend.h>

class TMS_Legend : public DRN_Legend
{
   public:
      TMS_Legend(DRN_DrawingArea *d) : DRN_Legend(d) { }
      ~TMS_Legend() { }

      void draw(int mode);
      void draw_more();
};

#endif

#endif // INTERNAL_GUI
