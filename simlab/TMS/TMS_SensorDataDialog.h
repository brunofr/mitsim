//-*-c++-*------------------------------------------------------------
// TMS_SensorDataDialog.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI
#ifndef TMS_SENSORDATADIALOG_HEADER
#define TMS_SENSORDATADIALOG_HEADER

#include <Templates/Pointer.h>
#include <Xmw/XmwDialogManager.h>
#include <Xmw/XmwCallback.h>
#include <Xmw/XmwInputField.h>

class TMS_SensorDataDialog : public XmwDialogManager
{
	  CallbackDeclare(TMS_SensorDataDialog);

   public:

	  TMS_SensorDataDialog(Widget parent);
	  ~TMS_SensorDataDialog();
	  void post();				// virtual

   private:

	  void activate();
	  void deactivate();

	  void sourceClickCB(Widget, XtPointer, XtPointer);
   
	  // overload the virtual functions in base class

	  void okay(Widget, XtPointer, XtPointer);
	  void cancel(Widget, XtPointer, XtPointer);
	  void help(Widget, XtPointer, XtPointer);

   private:

	  Widget source_;			// XmwChooser

	  XmwInputField dbStartTime_;
	  XmwInputField dbStepSize_;
	  XmwInputField dbNumPeriods_;
	  XmwInputField dbSpeedFile_;
	  XmwInputField dbSpeedScaler_;
	  XmwInputField dbFlowFile_;
	  XmwInputField dbFlowScaler_;
};

#endif
#endif // INTERNAL_GUI
