//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_Incident.C
// DATE: Fri Nov 17 10:12:59 1995
//--------------------------------------------------------------------

#include <Tools/Reader.h>
#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>

#ifdef INTERNAL_GUI
#include <DRN/DRN_DrawingArea.h>
#endif // !INTERNAL_GUI

#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>

#include "TMS_Incident.h"
#include "TMS_Parameter.h"
#include "TMS_CtrlLogic.h"
#include "TMS_Engine.h"
#include "TMS_Communicator.h"
#include "TMS_Network.h"
#include "TMS_Segment.h"
#include "TMS_Lane.h"
using namespace std;

// All the known incidents in the network are stored in a list.

inc_list_type *theIncidentList = new inc_list_type;

// Incidents are stored in a linked list in TMS and array of signals
// in MITSIM.  We overload these two functions so that it works in the
// linked list only.

RN_Signal*
TMS_Incident::prev()
{
  return container_->prev()->data();
}


RN_Signal*
TMS_Incident::next()
{
  return container_->next()->data();
}


void
TMS_Incident::receive(IOService& mitsim)
{
  int lanecode;

  mitsim >> state_
		 >> severity_
		 >> startTime_
		 >> duration_
		 >> lanecode
		 >> distance_;

  lane_ = theNetwork->findLane(lanecode);

  if (!lane_) {
	  cerr << "Error::  Unknown incident location. Lane <"
		   << lanecode << "> not found." << endl;
	  return;
  }

  RN_Segment *ps = segment();
  position_ = (distance_ - ps->distance()) / ps->length();

  if ((state_ & INCI_ACTIVE) && (state_ & INCI_MAJOR)) {
	  
	  psPhase_ = 0;		// first phase
	  vslsPhase_ = 0;
	  lusPhase_ = 0;

	  if (theCtrlLogic) {			// logic specified
		  timeFound_ = startTime_ + theCtrlLogic->detectionDelay();
		  psTime_ = timeFound_ + theCtrlLogic->psPhase(0)->delay();
		  vslsTime_ = timeFound_ + theCtrlLogic->vslsPhase(0)->delay();
		  lusTime_ = timeFound_ + theCtrlLogic->lusPhase(0)->delay();;
	  } else {						// no logic specified
		  timeFound_ = startTime_;
		  psTime_ = vslsTime_ = lusTime_ = ONE_DAY;
	  }

#ifdef INTERNAL_GUI
	  calcGeometricData();
	  draw(theDrawingArea);		// Show on screen
#endif /* INTERNAL_GUI */

	  // Put this incident in the list

	  container_ = theIncidentList->insert(this);
  }

  // Log the incident state so that MesoTS can read

  if (theEngine->incLogFpo()) { // in simlab
	if (state_ & INCI_ACTIVE) {
	  double erase_time = startTime_ + duration_; 
	  int segcode = lane_->segment()->code();
	  
	  // Reduce the capacity

	  fprintf(theEngine->incLogFpo(), "%.1lf %d %.2f\n",
			  timeFound_, segcode, -severity_);

	  // Restore the capacity

	  float value = 0;
	  while (value < severity_) {
		float step = severity_ - value;
		if (step > theParameter->inciCapStepSize()) {
		  step = theParameter->inciCapStepSize();
		}
		fprintf(theEngine->incLogFpo(), "%.1lf %d %.2f\n",
				erase_time, segcode, step);
		erase_time += theParameter->inciCapTimeStepSize();
		value += theParameter->inciCapStepSize();
	  }

	  fflush(theEngine->incLogFpo());

	  if (ToolKit::verbose()) {
		cout << "Incident in lane <"
			 << lanecode << "> received and logged." << endl;
	  }
	} else {
	  if (ToolKit::verbose()) {
		cout << "Incident clearance in lane <"
			 << lanecode << "> received." << endl;
	  }
	}
  }
}


/*
 * Schedule the next stage of the incident response logic
 */

void
TMS_Incident::response()
{
  if (!theCtrlLogic) return;

  const double epsilon = TIME_EPSILON;

  if (timeFound_ - theSimulationClock->currentTime() >= TIME_EPSILON) {
	return;			/* wait until detected */
  }
  state_ |= INCI_KNOWN;

  static RN_Segment *cache_segment = NULL;
  if (cache_segment != segment()) {
	cache_segment = segment();
	cache_segment->markConnectedUpLanes();
	cache_segment->markConnectedDnLanes();
  }

  /* Portal closure logic invoked here */

  if (psTime_ < theSimulationClock->currentTime() + epsilon) {
	theCtrlLogic->psPhase(vslsPhase_)->process(*this);
	if (psPhase_ < theCtrlLogic->nPsPhases() - 2) {
	  psPhase_ ++;
	  psTime_ += theCtrlLogic->psPhase(psPhase_)->delay();
	} else {			/* last stage is done */
	  psTime_ = ONE_DAY;
	}
  }

  /* VSLS logic invoked here */

  if (vslsTime_ < theSimulationClock->currentTime() + epsilon) {
	theCtrlLogic->vslsPhase(vslsPhase_)->process(*this);
	if (vslsPhase_ < theCtrlLogic->nVslsPhases() - 2) {
	  vslsPhase_ ++;
	  vslsTime_ += theCtrlLogic->vslsPhase(vslsPhase_)->delay();
	} else {			/* last stage is done */
	  vslsTime_ = ONE_DAY;
	}
  }

  /* LUS logic invoked here */

  if (lusTime_ < theSimulationClock->currentTime() + epsilon) {
	theCtrlLogic->lusPhase(lusPhase_)->process(*this);
	if (lusPhase_ < theCtrlLogic->nLusPhases() - 2) {
	  lusPhase_ ++;
	  lusTime_ += theCtrlLogic->lusPhase(lusPhase_)->delay();
	} else {			/* last stage is done */
	  lusTime_ = ONE_DAY;
	}
  }
}


/*
 * Schedule when the last stage of the incident response logic will be
 * invoked (clearance logic).
 */

void
TMS_Incident::clearance()
{
  if (!theCtrlLogic) return;

  /* Schedule the time for restoring signals */

  lusPhase_ = theCtrlLogic->nLusPhases() - 1;
  lusTime_ = theSimulationClock->currentTime() +
	theCtrlLogic->lusPhase(lusPhase_)->delay();

  vslsPhase_ = theCtrlLogic->nVslsPhases() - 1;
  vslsTime_ = theSimulationClock->currentTime() +
	theCtrlLogic->vslsPhase(vslsPhase_)->delay();

  psPhase_ = theCtrlLogic->nPsPhases() - 1;
  psTime_ = theSimulationClock->currentTime() +
	theCtrlLogic->psPhase(psPhase_)->delay();
}


/*
 * Erase the incident from the screen and send a message to TS to
 * clear this incident (it may have been cleared already).
 */

void
TMS_Incident::remove()
{
  TMS_Communicator *communicator = (TMS_Communicator *)theCommunicator;

  state_ &= (~INCI_ACTIVE);
  state_ |= INCI_REMOVED;

  if (communicator->isMitsimConnected()) {

	// Let TS know this incident has been cleared.

	this->send(communicator->mitsim());
  }

#ifdef INTERNAL_GUI
  draw(theDrawingArea);	// Show on screen
#endif // INTERNAL_GUI
}


void
TMS_Incident::print(ostream &os)
{
  os << indent << indent
	 << index_ << endc
	 << lane_->code() << endc
	 << distance_/theParameter->lengthFactor() << endc
	 << timeFound_ << endc
	 << duration_ << endc
	 << state_ << endl;
}


#ifdef INTERNAL_GUI

Pixel *
TMS_Incident::calcColor(unsigned int s)
{
  static Pixel clr[2];

  if (s & INCI_REMOVED) {	// removed
	clr[0] = clr[1] = theColorTable->lightGray();
	return clr;
  } else if (state_ & INCI_ACTIVE) {	// active
	if (s & INCI_SEVERITY) {
	  clr[0] = theColorTable->magenta();
	} else {
	  clr[0] = theColorTable->yellow();
	}
  } else {			/* wrong state id */	
	clr[0] = theColorTable->yellow();
  }

  // border color

  if (state_ & INCI_KNOWN) {	/* detected */
	clr[1] = theColorTable->red();
  } else {
	clr[1] = theColorTable->cyan();
  }
  return clr;
}

#endif /* INTERNAL_GUI */

/*
 * Announce the clearance of an incident
 */

void
TMS_Incident::send(IOService& cli)
{
	// code() is actually the signal index in MITSIM

	cli << (MSG_TYPE_INCI | code()) << state_;
	cli.flush(SEND_IMMEDIATELY);
}


/*
 * Loop every incident in the network and invoke each scheduled
 * response logic.
 */

void
CheckIncidents()
{
  inc_list_type::iterator it(theIncidentList);
  TMS_Incident* inc;

  while (it != theIncidentList->end()) {
	inc = *it;
	if (!(inc->state() & INCI_REMOVED) && 
		inc->clearanceTime() < theSimulationClock->currentTime()) {
	  inc->clearance();
	  inc->remove();

	  /* CAUTION: Do NOT delete "inc" after it is removed.  The
	   * invoke of clearance logic may have delay!  */
	}
	inc->response();
	it ++;
  }
}


/*
 * Return the situation ID for the Blockage + EVA
 *
 * NOTE: Need to figure out where is the best location to calculate
 * EVA (emergency vehicle access) value. Current we set it to
 * EVA_DOWNSTREAM if all lanes in the segment are blocked and
 * EVA_UPSTREAM otherwise.
 */

int
TMS_Incident::situation()
{
  int num = 0;
  
  inc_list_type::iterator it(theIncidentList);
  TMS_Incident* inc;
  
  /* Loop all incidents in the network and find the ones at the same
   * position. */

  while (it!= theIncidentList->end()) {
	inc = *it;
	if (inc->severity() >= 1.0 &&
	    inc->segment() == segment() &&
	    inc->distance() == distance()) {
	  num ++;
	}
	it ++;
  }
	
  int value;
  if (num == segment()->nLanes()) {

	// All lanes are blocked. Emergency vehicle may access from
	// downstream

	value = COMPLETE_BLOCKAGE + EVA_DOWNSTREAM;

  } else {

	// The segment is partial blocked. Emergency vehicle may access
	// from upstream.

	value = PARTIAL_BLOCKAGE + EVA_UPSTREAM;

  }
  return (value);
}
