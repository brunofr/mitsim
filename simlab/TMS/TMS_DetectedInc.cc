//-*-c++-*------------------------------------------------------------
// TMS_DetectedInc.cc
//
// Sreeram Radhakrishnan
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <DRN/DRN_DrawingArea.h>
#include <GRN/RN_CtrlStation.h>

#include "TMS_DetectedInc.h"
#include "TMS_SurvStation.h"
#include "TMS_Symbols.h"
#include "TMS_Segment.h"
#include "TMS_Lane.h"
#include "TMS_Interface.h"

detected_inc_list * theDetectedIncs = new detected_inc_list;

TMS_DetectedInc::TMS_DetectedInc(SurvList s)
   : station_(s),
	 shownState_(0),
	 onScreen_(0)
{
  RN_DrawableSegment * ps = (RN_DrawableSegment *)((*s)->segment());
  center_ = ps->centerPoint((*s)->position());
  theDetectedIncs->append(this);
  draw(theDrawingArea);
}


// ------------------------------------------------------------------
//  Find a color to show the signal state.
// ------------------------------------------------------------------

Pixel TMS_DetectedInc::calcColor()
{
  static Pixel clr;

  switch (shownState_ & INC_STATUS) {

  case TENTATIVE_INC:
	{
	  clr = theColorTable->yellow();
	  break;
	}
  case CONFIRMED_INC:
	{
	  clr = theColorTable->lightRed();
	  break;
	}
  case CLEARED_INC:
	{
	  clr = theColorTable->green();
	  break;
	}
  default:
	{
	  clr = theColorTable->cyan();
	  break;
	}
  }
  return clr;
}


bool TMS_DetectedInc::isChosen()
{
  int i = ((*station_)->stationState() & INC_STATUS);
  if (i) {
	XmwSymbol<int>& c = theSymbols()->incidentTypes();
	int k = c.value();
	return (c.isOn(i));
  } else {
	return false;
  }

  return (i != 0);
}


// -----------------------------------------------------------------
// Draw a detected incident. It returns true if the incident is
// visible or false otherwise.
// -----------------------------------------------------------------

bool TMS_DetectedInc::draw(DRN_DrawingArea * area)
{
  if (!isChosen()) {
	return 0;
  }			

  area->setXorMode();
  int isIncOn = area->state(DRN_DrawingArea::INCIDENT_ON);
  int on = ((onScreen_)&&(isIncOn));
  if (on) {
	if ((*station_)->stationState() == shownState_) {
	  return on;
	} else {					// erase the old image
	  drawShape(area);
	}
  }
  
  shownState_ = (*station_)->stationState();
  onScreen_ = drawShape(area);
  return onScreen_;
}


bool TMS_DetectedInc::drawShape(DRN_DrawingArea * area)
{
   static XmwPoint points[4],point;

   Pixel clr = calcColor();

   const double size = 2.25 * LANE_WIDTH;
   const int radius = Round(1.25 * size*((area->meter2Pixels() > 1) ? 
                                              area->meter2Pixels() : 1));

   WcsPoint p0(center_.bearing(size, angle_ + 0.75 * ONE_PI));
   if (!area->world2Window(p0, points[0])) return false;
      
   WcsPoint p1(center_.bearing(size, angle_ - 0.75 * ONE_PI));
   if (!area->world2Window(p1, points[1])) return false;

   WcsPoint p2(center_.bearing(size, angle_ - 0.25 * ONE_PI));
   if (!area->world2Window(p2, points[2])) return false;

   WcsPoint p3(center_.bearing(size, angle_ + 0.25 * ONE_PI));
   if (!area->world2Window(p3, points[3])) return false;

   if (!area->world2Window(center_, point)) return false;
   
   area->lineWidth(area->thickLine());

   area->foreground(clr);
   area->XmwDrawingArea::drawLine(points[0], points[2]);
   area->XmwDrawingArea::drawLine(points[1], points[3]);

   area->XmwDrawingArea::drawCircle(point, radius);

   area->lineWidth(0);

   return true;
}

void TMS_DetectedInc::drawAllDetectIncidents(DRN_DrawingArea * area)
{  
  if(theDetectedIncs) {
	detected_inc_list::iterator it(theDetectedIncs);
	TMS_DetectedInc *detinc;
	while(it != theDetectedIncs->end()) {
	  detinc = *it;
	  detinc->draw(theDrawingArea);
	  it++;
	}
  area->setState(DRN_DrawingArea::INCIDENT_ON);
  }
}

#endif
