//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_CtrlLogic.h
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#ifndef TMS_CTRL_LOGIC_HEADER
#define TMS_CTRL_LOGIC_HEADER

#include <vector>

#include "TMS_ResponsePhase.h"

const int PARTIAL_BLOCKAGE		= 1;
const int COMPLETE_BLOCKAGE		= 2;

const int EVA_UPSTREAM			= 16;
const int EVA_DOWNSTREAM		= 32;

const int INCIDENT_POSITION		=  0;
const int PORTAL_POSITION		=  1;

class Reader;

class TMS_CtrlLogic
{
   public:

      TMS_CtrlLogic() { }
      ~TMS_CtrlLogic() { }

      static inline char* name() { return name_; }
      static inline char** nameptr() { return &name_; }

      static void load();
      void readParameters(Reader&);
      void readLusResponseTable(Reader&);
      void readVslsResponseTable(Reader&);
      void readPsResponseTable(Reader&);

      inline double detectionDelay() {
		 return detectionDelay_;
      }
      inline int speedLimitLowerBound() {
		 return speedLimitLowerBound_;
      }
      inline double searchingDistUpbound() {
		 return searchingDistUpbound_;
      }

      inline int nPsPhases() { return psPhases_.size(); }
      inline TMS_ResponsePhase* psPhase(int i) {
		 return psPhases_[i];
      }

      inline int nVslsPhases() { return vslsPhases_.size(); }
      inline TMS_ResponsePhase* vslsPhase(int i) {
		 return vslsPhases_[i];
      }

      inline int nLusPhases() { return lusPhases_.size(); }
      inline TMS_ResponsePhase* lusPhase(int i) {
		 return lusPhases_[i];
      }

   protected:
	
      static char* name_;	/* file name of ctrl database */

      std::vector<TMS_ResponsePhase *> psPhases_;
      std::vector<TMS_ResponsePhase *> vslsPhases_;
      std::vector<TMS_ResponsePhase *> lusPhases_;

      double detectionDelay_;
      double searchingDistUpbound_;
      int speedLimitLowerBound_;
};

extern TMS_CtrlLogic *theCtrlLogic;

#endif

