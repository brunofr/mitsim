//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_Status.C
// DATE: Fri Nov 17 10:12:59 1995
//--------------------------------------------------------------------

#ifndef TMS_STATUS_HEADER
#define TMS_STATUS_HEADER

#include <fstream>
#include <iostream>

class TMS_Status
{
   public:

      TMS_Status();
      ~TMS_Status();
	  void clean();

      std::ofstream& osLogFile() { return osLogFile_; }

      void openLogFile();
      void closeLogFile();

      inline void nErrors(int n) { nErrors_ += n; }
      inline int nErrors() { return nErrors_; }
	  inline void nMsgs(int n) { nMsgs_ += n; }
      void report(std::ostream &os = std::cout);

      void print(std::ostream &os = std::cout);

#ifdef INTERNAL_GUI
      void showStatus();
#endif

   protected:

      char* logFile_;

	  // number of errors and msg (a fatal error will terminate the
	  // program), if both zero. The log file is treated as zero and
	  // removed.

      int nErrors_;				// number of errors
	  int nMsgs_;				// number of messages
      std::ofstream osLogFile_;		// error log
};

extern TMS_Status *theStatus;	// current simulation status

#endif
