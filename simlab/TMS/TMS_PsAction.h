//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_PsLogic.h
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#ifndef TMS_PS_ACTION_HEADER
#define TMS_PS_ACTION_HEADER

#include "TMS_Action.h"

class TMS_PsAction : public TMS_Action 
{
   public:

      TMS_PsAction() : TMS_Action() { }
      ~TMS_PsAction() { }

      void read(Reader&);
      void print(ostream &os = cout);

      void applyTo(TMS_Incident&);

   protected:

      int state_;		/* signal state */
};

#endif
