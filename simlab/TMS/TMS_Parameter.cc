//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_Parameter.C
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#include <iostream>
#include <new>
using namespace std;

#include <Tools/Math.h>

#include <Tools/ToolKit.h>
#include <Tools/Random.h>
#include <Tools/Scaler.h>
#include <Tools/GenericVariable.h>
#include <Tools/VariableParser.h>

#include <IO/Exception.h>

#ifdef INTERNAL_GUI
#include <Xmw/XmwFont.h>
#include <DRN/DRN_DrawingArea.h>
#endif

#include "TMS_Parameter.h"
#include "TMS_Network.h"
#include "TMS_Engine.h"

TMS_Parameter * theParameter;

TMS_Parameter::TMS_Parameter()
  : Parameter(),
	vehicleLength_(6.096),
	inciCapStepSize_(0.2),
	inciCapTimeStepSize_(150)	// seconds per step
{
}

void
TMS_Parameter::load()
{
   const char * filename = ToolKit::optionalInputFile(name_);
   if (!filename) theException->exit(1);
   VariableParser vp(this);
   vp.parse( filename );
}


int
TMS_Parameter::parseVariable(GenericVariable &gv)
{
   if (Parameter::parseVariable(gv) == 0) return 0;

   char * token = compact(gv.name());

   if (isEqual(token, "AverageVehicleLength")) {
      vehicleLength_ = gv.element();
	  vehicleLength_ *= speedFactor();

   } else if (isEqual(token, "IncidentCapacityParameters")) {
	  return loadInciCapParams(gv);

   } else if (isEqual(token, "FontSizes")) {
#ifdef INTERNAL_GUI
	  return theDrawingArea->loadScalableFontSizes(gv);
#else
	  return 0;
#endif

   } else {
      return 1;
   }

   return 0;
}


int
TMS_Parameter::loadInciCapParams(GenericVariable &gv)
{
  if (gv.nElements() != 2) {
	cerr << "Error:: 2 parameters expected. "
		 << gv.nElements() << " found." << endl;
	return -1;
  }

  inciCapStepSize_ = gv.element(0);
  inciCapTimeStepSize_ = gv.element(1);

  return 0;
}
