//-*-c++-*------------------------------------------------------------
// TMS_Menu.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <fstream>
#include <cassert>
using namespace std;

#include <Xmt/Xmt.h>
#include <Xmt/Create.h>
#include <Xmt/Menu.h>
#include <Xmt/Dialogs.h>
#include <Xmt/Dialog.h>

#include <IO/Exception.h>
#include <Tools/ToolKit.h>
#include <Tools/Random.h>

#include <Xmw/XmwDialogManager.h>
#include <Xmw/XmwRandomizer.h>

#include "TMS_Menu.h"
#include "TMS_Interface.h"
#include "TMS_Network.h"
#include "TMS_Symbols.h"
#include "TMS_SensorDataDialog.h"
#include "TMS_OutputDialog.h"
#include "TMS_SetupDialog.h"
#include "TMS_IncidentDialog.h"

TMS_Menu::TMS_Menu(Widget parent) : DRN_Menu(parent)
{
  Widget w;

  // Simulation pane

  w = getItemWidget("Setup");
  assert(w);
  addCallback(w,
			  XmNactivateCallback, &TMS_Menu::setup,
			  NULL );

  w = getItemWidget("Output");
  assert(w);
  addCallback(w,
			  XmNactivateCallback, &TMS_Menu::output,
			  NULL );

  w = getItemWidget("SensorData");
  assert(w);
  addCallback(w,
			  XmNactivateCallback, &TMS_Menu::sensorData,
			  NULL );

  w = getItemWidget("Incident");
  assert(w);
  addCallback(w,
			  XmNactivateCallback, &TMS_Menu::incident,
			  NULL );
}

void TMS_Menu::randomizer ( Widget, XtPointer, XtPointer )
{
  static XmwRandomizer dialog(theInterface->widget(),1);
  dialog.post();
}

void TMS_Menu::setup ( Widget, XtPointer, XtPointer )
{
  static TMS_SetupDialog dialog(theInterface->widget());
  dialog.post();
}

void TMS_Menu::output ( Widget, XtPointer, XtPointer )
{
  static TMS_OutputDialog dialog(theInterface->widget());
  dialog.post();
}

void TMS_Menu::sensorData ( Widget, XtPointer, XtPointer )
{
  static TMS_SensorDataDialog dialog(theInterface->widget());
  dialog.post();
}

void TMS_Menu::incident ( Widget, XtPointer, XtPointer )
{
  static TMS_IncidentDialog dialog(theInterface->widget());
  dialog.post();
}

#endif // INTERNAL_GUI
