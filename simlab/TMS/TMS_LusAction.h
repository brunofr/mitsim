//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_LusLogic.h
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------


#ifndef LUS_ACTION_HEADER
#define LUS_ACTION_HEADER

#include <iostream>


#include "TMS_Action.h"

class TMS_LusAction : public TMS_Action
{
   public:

      TMS_LusAction() : TMS_Action() { }
      ~TMS_LusAction() { }

      void read(Reader&);
      void print(std::ostream &os = std::cout);

      void applyTo(TMS_Incident&);

   protected:

      int state_;		/* signal state */
};

#endif
