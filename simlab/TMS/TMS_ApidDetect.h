//-*-c++-*------------------------------------------------------------
// TMS_APIDDetect.h
//
// Sreeram Radhakrishnan
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef TMS_APIDDETECT_HEADER
#define TMS_APIDDETECT_HEADER

#include "TMS_SurvStation.h"
#include "TMS_IncidentDetect.h"

class TMS_ApidDetector: public TMS_IncidentDetector
{
protected:

  int compWavePeriodCnt_;

public:

  TMS_ApidDetector(): TMS_IncidentDetector() { }	  

  ~TMS_ApidDetector() { }

  void incidentDetect(int);
  void checkforIncident(SurvList );
  void checkforClearence(SurvList );

  void checkforTentativeInc(SurvList );
  int incDetectCheck(RN_Sensor *);
  int califIncDetection(RN_Sensor *);

  void updateCompWaveHistory(SurvList );

  int medVolIncDetectCheck(RN_Sensor *);
  int lowVolIncDetectCheck(RN_Sensor *);

  double getOccdf(RN_Sensor *);
  double getOccrdf(RN_Sensor *);
  double getDocc(RN_Sensor *);
  double getDocctd(RN_Sensor *);
  double getSpdtdf(RN_Sensor *);
};

#endif
