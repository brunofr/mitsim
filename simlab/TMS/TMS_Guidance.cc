//-*-c++-*------------------------------------------------------------
// NAME: Traffic Simulation
// NOTE: 
// AUTH: Qi Yang
// FILE: TMS_Guidance.C
// DATE: Mon Dec  9 18:43:38 1996
//--------------------------------------------------------------------

#include <cstdlib>
#include <cstring>
#include <fstream>
using namespace std;


#include <Tools/Math.h>
#include <Tools/Reader.h>
#include <Tools/ToolKit.h>

#include "TMS_Guidance.h"
#include "TMS_Exception.h"

// Current table

TMS_Guidance* TG_LinkInfo::parent_ = NULL;
TMS_Guidance* TG_LinkOutcome::parent_ = NULL;

// Read the travel time info for a link

void TG_LinkInfo::read(Reader& is)
{
   float value;
   is >> type_ >> length_;
   cost_.reserve(parent_->iNumCols_);
   for (register int i = 0; i < parent_->iNumCols_; i ++) {
      is >> value;
	  cost_.push_back(value);
   }
}

// Modify the guidance based on latest prediction and returns the
// evaluation metric

TG_Evaluation TG_LinkInfo::combine(TG_LinkOutcome &o, float alpha)
{
   float beta = 1 - alpha;
   TG_Evaluation e;
   for (register int i = 0; i < parent_->oNumCols_; i ++) {
	  float &g = cost_[parent_->offset_ + i];
	  float &a = o.cost_[i];
	  float d = (g - a) / 3600.0;
	  e.consistency_ += ((d < 0) ? (-d) : (d)) * o.count_[i];
	  e.optimality_ += a * o.count_[i] / 3600.0;
	  g = beta * g + alpha * a;
   }
   return e;
}

void TG_LinkInfo::print(ostream& os)
{
   float x;
   os << indent << type_ << endc << length_;
   for (register int i = parent_->offset_; i < cost_.size(); i ++) {
      x = cost_[i];
      x = (int) (x * 10.0 + 0.5) / 10.0;
      os << endc << x;
   }
   os << endl;
}

// Read the result of the traffic prediction for a link

void TG_LinkOutcome::read(Reader& is)
{
   float value;
   int cnt;
   count_.reserve(parent_->oNumCols_);
   cost_.reserve(parent_->oNumCols_);
   for (register int i = 0; i < parent_->oNumCols_; i ++) {
	  is >> cnt >> value;
      count_.push_back(cnt);
	  cost_.push_back(value);
   }
}


TMS_Guidance::TMS_Guidance()
   : filename_(NULL),
	 iStart_(0),
	 oStart_(0),
	 offset_(0),
	 iNumCols_(0),
	 oNumCols_(0),
	 length_(0)
{
   TG_LinkInfo::parent_ = this;
   TG_LinkOutcome::parent_ = this;
}

TMS_Guidance::TMS_Guidance(const char *file)
   : filename_(NULL),
	 iStart_(0),
	 oStart_(0),
	 offset_(0),
	 iNumCols_(0),
	 oNumCols_(0),
	 length_(0)
{
   TG_LinkInfo::parent_ = this;
   TG_LinkOutcome::parent_ = this;
   read(file);
}

TMS_Guidance::~TMS_Guidance()
{
   if (filename_) delete [] filename_;
}


// Read the current guidance

void TMS_Guidance::read(const char *file)
{
   Reader is(file);

   if (filename_) {
	  links_.erase(links_.begin(), links_.end());
	  delete [] filename_;
   }

   if (filename_) delete [] filename_;
   filename_ = Copy(file);

   int code;

   is >> iStart_ >> iNumCols_ >> length_;

   if (!is.findToken('{')) theException->exit();
   for (is.eatwhite(); is.peek() != '}'; is.eatwhite()) {
      is >> code;
      links_[code].read(is);
   }
   if (!is.findToken('}')) theException->exit();

   is.close();
}


// Modifies the guidance (link travel time table) based on the
// previous guidance and the predicted flow and travel times.

TG_Evaluation TMS_Guidance::modify(
   const char* a,				// actual flow and travel time
   float alpha					// replace corresponsing cells
   )
{
   int length;
   
   // Load the outcome from the given guidance

   Reader is(a);

   is >> oStart_ >> oNumCols_ >> length;

   if (oStart_ < iStart_ ||
	   oStart_ + oNumCols_ > iStart_ + iNumCols_ ||
	   length_ != length) {
	  cerr << "Error:: The header in file <"
		   << a << "> is incompatible." << endl;
      theException->exit();
   }

   offset_ = oStart_ - iStart_;

   int code;
   TG_Evaluation e;

   map<int, TG_LinkInfo, less<int> ALLOCATOR>::iterator i;

   if (!is.findToken('{')) theException->exit();
   for (is.eatwhite(); is.peek() != '}'; is.eatwhite()) {
      TG_LinkOutcome p;
      is >> code;
	  i = links_.find(code);
	  if (i == links_.end()) {
		 cerr << "Error:: Link <" << code
			  << "> in <" << is.name() << ":" << is.line_number()
			  << "> not found in <" << filename_ << ">." << endl;
		 theException->exit();
	  }
      p.read(is);
	  TG_LinkInfo &h = (*i).second;
      e.sum(h.combine(p, alpha));
   }
   if (!is.findToken('}')) theException->exit();

   is.close();

   return e;
}


// Output the guidance to a file, using the format readable by
// RN_LinkTime

void
TMS_Guidance::printLinkTimeTable(const char *g)
{
   ofstream os(g);

   if (!os.good()) {
	  cerr << "Error:: Failed opening output file <" << g << ">" << endl;
	  theException->exit();
   }

   os << "% GENERATED GUIDANCE: LINK TRAVEL TIMES" << endl
      << oStart_ << "\t% Start period" << endl
      << (iNumCols_ - offset_) << "\t% Number of periods" << endl
      << length_ << "\t% Seconds per period" << endl;
   os << "% ID Type Length"
	  << " T-" << oStart_ << " ... "
	  << " T-" << oStart_+iNumCols_-1 << endl;

   os << "{" << endl;

   map<int, TG_LinkInfo, less<int> ALLOCATOR>::iterator i;
   for (i = links_.begin(); i != links_.end(); i ++) {
      os << "  " << (*i).first;
      (*i).second.print(os);
   }

   os << "}" << endl;
   os.close();
}
