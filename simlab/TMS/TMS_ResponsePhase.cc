//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_ResponsePhase.C
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#include <new>
using namespace std;

#include <Tools/Reader.h>
#include <Tools/ToolKit.h>

#include <IO/Exception.h>

#include <GRN/Constants.h>
#include <GRN/RN_SurvStation.h>

#include "TMS_Incident.h"
#include "TMS_ResponsePhase.h"
#include "TMS_LusAction.h"
#include "TMS_VslsAction.h"
#include "TMS_PsAction.h"
#include "TMS_Lane.h"

TMS_ResponsePhase::TMS_ResponsePhase() : CodedObject()
{
}


/*
 *------------------------------------------------------------------
 * Read a response table (one phase)
 *------------------------------------------------------------------
 */

void
TMS_ResponsePhase::read(int type, Reader& is)
{
   static int nphases = 0;
   code_ = ++ nphases;

   /* delay for applying actions in this logic table */

   is >> delay_;

   if (!is.findToken(OPEN_TOKEN)) theException->exit(1);
   for (is.eatwhite();
		is.peek() != CLOSE_TOKEN;
		is.eatwhite()) {
	  TMS_Action *action = NULL;
	  switch (type) {
		 case CTRL_LUS:
			{
			   action = new TMS_LusAction;
			   break;
			}
		 case CTRL_VSLS:
			{
			   action = new TMS_VslsAction;
			   break;
			}
		 case CTRL_PS:
			{
			   action = new TMS_PsAction;
			   break;
			}
		 default:
			{
			   cerr << "Error:: Unknown action type <"
					<< type << "> in control logic table. ";
			   is.reference();
			   theException->exit();
			   break;
			}
	  }

      action->read(is);
      action->parent(this);
      actionList_.push_back(action);
   }
   if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
}


void
TMS_ResponsePhase::print(ostream &os)
{
   os << endl << delay_ << OPEN_TOKEN << endl;
   list<TMS_Action*>::iterator it;
   for (it = actionList_.begin(); 
		it != actionList_.end();
		it ++ ) {
      (*it)->print(os);
   }
   os << CLOSE_TOKEN << endl;
}


void
TMS_ResponsePhase::process(TMS_Incident& inc)
{
	// if (inc.severity() < 1.0) return ;

  list<TMS_Action*>::iterator it;
  int situation = inc.situation();

  /* Loop all action in this table and apply only the ones
   * that marth the situation.  If the situation id of an
   * action is zero, it is applied to any situation.
   */

  for (it = actionList_.begin();
       it != actionList_.end();
       it ++) {
	 TMS_Action *a = *it;
	 if (a->situation() == situation ||
	     a->situation() == 0) {
	   a->applyTo(inc);
	 }
  }
}
