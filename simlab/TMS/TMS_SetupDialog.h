//-*-c++-*------------------------------------------------------------
// TMS_SetupDialog.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI
#ifndef TMS_SETUPDIALOG_HEADER
#define TMS_SETUPDIALOG_HEADER

#include <Xmw/XmwDialogManager.h>
#include <Xmw/XmwInputField.h>

class TMS_SetupDialog : public XmwDialogManager
{
public:

  TMS_SetupDialog(Widget parent);
  ~TMS_SetupDialog() { }
  void post();				// virtual

private:

  void set(char **ptr, const char *s);

  void activate();
  void deactivate();

  // overload the virtual functions in base class

  void okay(Widget, XtPointer, XtPointer);
  void cancel(Widget, XtPointer, XtPointer);
  void help(Widget, XtPointer, XtPointer);

private:

  Widget title_;

  XmwInputField paraDirFld_;
  XmwInputField inDirFld_;
  XmwInputField workDirFld_;
  XmwInputField outDirFld_;

  XmwInputField netFileFld_;
  XmwInputField paraFileFld_;
  XmwInputField ctrlLogicFileFld_;
  XmwInputField signalPlansFileFld_;
  XmwInputField sensorFileFld_;

  XmwInputField startTimeFld_;
  XmwInputField stopTimeFld_;
  XmwInputField stepSizeFld_;
  XmwInputField batchStepSizeFld_;

  Widget information_;		// radio box
};

#endif
#endif // INTERNAL_GUI
