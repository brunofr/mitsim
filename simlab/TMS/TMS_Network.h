//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_Network.h
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#ifndef TMS_NETWORK_HEADER
#define TMS_NETWORK_HEADER

#ifdef INTERNAL_GUI
class DRN_DrawingArea;
#include <DRN/DrawableRoadNetwork.h>
#else  // match mode
#include <GRN/RoadNetwork.h>
#endif // !INTERNAL_GUI

class Reader;
class IOService;

class TMS_Node;
class TMS_Link;
class TMS_Segment;
class TMS_SurvStation;
class TMS_Lane;

#ifdef INTERNAL_GUI
class TMS_Network : public DrawableRoadNetwork
#else  // match mode
class TMS_Network : public RoadNetwork
#endif // !INTERNAL_GUI

{				// TMS_Network
   public:

      TMS_Network();
      ~TMS_Network();

      // Overload the virtual function defined in the base class

      RN_Node* newNode();
      RN_Link* newLink();
      RN_Segment* newSegment();
      RN_Lane* newLane(); 
      RN_Sensor* newSensor();
      RN_SurvStation* newSurvStation();
      RN_Signal* newSignal();
      RN_TollBooth* newTollBooth();

      // This overload the virtual function defined in base class to
      // provide a chance to call network initialization stuff in
      // TMS_Network

      void calcStaticInfo(void);

      TMS_Node * tmsNode(int i) { return (TMS_Node *) node(i); }
      TMS_Link * tmsLink(int i) { return (TMS_Link *) link(i); }
      TMS_Segment * tmsSegment(int i) { return (TMS_Segment *) segment(i); }
      TMS_Lane * tmsLane(int i) { return (TMS_Lane *) lane(i); }

      void calcSegmentDensity();
      void calcSegmentSpeed();
      void calcSegmentFlow();
      void calcSegmentStates();	// every thing but INDEPENDENTLY

      void interpolateSegmentStates(const int max_degree = 10);
      void updateSegmentParameters();

      void resetSensorReadings();

#ifdef INTERNAL_GUI
      void drawNetwork(DRN_DrawingArea *);
      void drawIncidents(DRN_DrawingArea *);
#endif

};

inline TMS_Network * tmsNetwork() {
   return (TMS_Network *)theNetwork;
}

#endif
