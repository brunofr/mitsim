//-*-c++-*------------------------------------------------------------
// NAME: Explicitly instantiate all the template instances we use
// NOTE: We needs this file to get the templates compiled in g++
// AUTH: Qi Yang
// FILE: fiTemplates.C
// DATE: Sat Apr 20 13:55:49 1996
//--------------------------------------------------------------------

#ifdef FORCE_INSTANTIATE

#include <Templates/Listp.h>

#include "TMS_Incident.h"

#ifdef __sgi

#pragma instantiate ListpNode<TMS_Incident*>
#pragma instantiate Listp<TMS_Incident*>

#endif // __sgi

#ifdef __GNUC__

template class ListpNode<TMS_Incident*>;
template class Listp<TMS_Incident*>;

#endif  // __GNUC__

#endif // FORCE_INSTANTIATE
