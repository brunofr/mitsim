//-*-c++-*------------------------------------------------------------
// TMS_IncidentDetect.cc
//
// Sreeram Radhakrishnan
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <iostream>
#include <cstdio>
#include <vector>
using namespace std;

#include <GRN/RoadNetwork.h>
#include <GRN/RN_SurvStation.h>
#include <GRN/RN_Link.h>
#include <GRN/RN_Lane.h>
#include <Tools/SimulationClock.h>
#include <Tools/ToolKit.h>
#include "TMS_Network.h"
#include "TMS_Segment.h"

#include "TMS_IncidentDetect.h"
#include "TMS_DetectedInc.h"

char *TMS_IncidentDetector::incLogFileName_;
TMS_IncidentDetector *theIncidentDetector = NULL;

void TMS_IncidentDetector::declareIncident(SurvList surv)
{
  int timeOfDetection = (int)(theSimulationClock->currentTime());
  
  osIncLogFile_ << "Inc detected at { ";
  int n_Lanes = (*surv)->segment()->nLanes();
  for(register int k = 0; k < n_Lanes; k++) {
	osIncLogFile_ << (*surv)->sensor(k)->code() << " ";
  }
  osIncLogFile_ << "} "  << "on link "
				<< (*surv)->segment()->link()->index()
				<< " at " << timeOfDetection
				<< " seconds from the start." << endl;
}


void TMS_IncidentDetector::declareRecurrent(SurvList surv)
{
  int timeOfDetection = (int)(theSimulationClock->currentTime());
  
  osIncLogFile_ << "Recurrent congestion at { ";

  int n_Lanes = (*surv)->segment()->nLanes();

  for (register int k = 0; k < n_Lanes; k++)
	  osIncLogFile_ << (*surv)->sensor(k)->code() << " ";

  osIncLogFile_ << "} "  << "on link "
				<< (*surv)->segment()->link()->index()
				<< " at " << timeOfDetection
				<< " seconds from the start." << endl;
}


void TMS_IncidentDetector::declareClearence(SurvList surv)
{
  int timeOfClearence = (int)(theSimulationClock->currentTime());
  
  osIncLogFile_ << "Inc cleared at { ";

  int n_Lanes = (*surv)->segment()->nLanes();

  for (register int k = 0; k < n_Lanes; k++)
	  osIncLogFile_ << (*surv)->sensor(k)->code() << " ";

  osIncLogFile_ << "} "  << "on link "
				<< (*surv)->segment()->link()->index()
				<< " at " << timeOfClearence
				<< " seconds from the start." << endl;
}



SurvList TMS_IncidentDetector::next(SurvList surv)
{
   RN_Link *curLink,*dnlink;

   int ndnlinks;

   curLink = (*surv)->segment()->link();

   ndnlinks = curLink->nDnLinks();

   if(surv->next()!=NULL) {
	  return surv->next();
   } else {
	 for(register int i=0; i < ndnlinks; i++) {
	   dnlink = curLink->dnLink(i);
	   if((dnlink->survList()) && 
		  (dnlink->linkType()) == curLink->linkType())
		 return (dnlink->survList());
	 }
   }
   return NULL;
}


SurvList TMS_IncidentDetector::prev(SurvList surv)
{
   RN_Link *curLink,*uplink;

   int nuplinks;

   curLink = (*surv)->segment()->link();

   nuplinks = curLink->nUpLinks();

   if(surv->prev() != NULL) {
	   return surv->prev();
   } else {
	 for(register int i = 0; i < nuplinks; i++) {
	   uplink = curLink->upLink(i);
	   if((uplink->survList()) &&
		  (uplink->linkType() == curLink->linkType()))
		 return (uplink->lastSurv());
	 }
   }
   return NULL;
}

RN_Sensor *TMS_IncidentDetector::nextsensor(RN_Sensor *curSensor)
{
   RN_Link *curLink = curSensor->link(), *nxtLink;

   SurvList surv;

   int ndnlinks;

   int currentLane = curSensor->lane()->localIndex();

   for(surv = curLink->survList();
	   (*surv)->code() != curSensor->station()->code();
	   surv = surv->next());

   if (surv->next()) {
	  return ((*(surv->next()))->sensor(currentLane));
   } else if ((ndnlinks = curLink->nDnLinks()) > 0) {
	 ndnlinks = curLink->nDnLinks(); // to be changed for more links
	 for(register int i=0; i < ndnlinks; i++) {
	   nxtLink = curLink->dnLink(i);
	   if ((nxtLink->survList()) &&
		   (curLink->linkType() == nxtLink->linkType()))
		 return ((*(nxtLink->survList()))->sensor(currentLane));
	 }
   }
   return NULL;
}

#ifdef INTERNAL_GUI
detected_inc_list * TMS_IncidentDetector::addIncident(SurvList surv)
{
  TMS_DetectedInc * inc = new TMS_DetectedInc(surv);
  //theDetectedIncs->append(inc);
  return theDetectedIncs;
}
#endif
