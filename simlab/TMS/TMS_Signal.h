//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// NOTE: 
// AUTH: Qi Yang
// FILE: TMS_Signal.h
// DATE: Sun Mar 31 22:21:09 1996
//--------------------------------------------------------------------

#ifndef TMS_SIGNAL_HEADER
#define TMS_SIGNAL_HEADER

#include <TC/TC_Signal.h>

class TMS_Signal : public TC_Signal
{
   public:

      TMS_Signal() : TC_Signal() { }
      ~TMS_Signal() { }

      unsigned int state() { return TC_Signal::state(); }

      void state(unsigned int s);
};


#endif
