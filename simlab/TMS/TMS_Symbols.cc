//-*-c++-*------------------------------------------------------------
// TMS_Symbols.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include "TMS_Symbols.h"

void TMS_Symbols::registerAll()
{
   DRN_Symbols::registerAll();
   incidentTypes_.create("incidentTypes", XtRInt, CONFIRMED);

}

#endif // INTERNAL_GUI
