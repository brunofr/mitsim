//-*-c++-*------------------------------------------------------------
// TMS_Interface.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef TMS_INTERFACE_HEADER
#define TMS_INTERFACE_HEADER

#include <DRN/DRN_Interface.h>

class TMS_Menu;
class TMS_Symbols;

class TMS_Interface : public DRN_Interface
{
   public:

	  TMS_Interface(int *argc, char **argv);
	  ~TMS_Interface();

	  void create();			// virtual

	  XmwSymbols* createSymbols(); // virtual
	  TMS_Symbols* symbols() { return (TMS_Symbols*) symbols_; }

	  XmwMenu *menu(Widget parent);	// virtual
	  TMS_Menu *menu() { return menu_; }

	  XmwModeline* modeline(Widget parent);	// virtual
	  XmwModeline* modeline() { return modeline_; }

   private:

	  TMS_Menu *menu_;
};

extern TMS_Interface *theInterface;
inline TMS_Symbols* theSymbols() {
   return theInterface->symbols();
}

#endif
