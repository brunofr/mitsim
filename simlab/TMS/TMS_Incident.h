//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_Incident.h
// DATE: Fri Nov 17 10:12:59 1995
//--------------------------------------------------------------------

#ifndef TMS_INCIDENT_HEADER
#define TMS_INCIDENT_HEADER

#include <iostream>

#include <Templates/Listp.h>

#include <GRN/Constants.h>

#include "TMS_Signal.h"

class TMS_Incident;

typedef Listp<TMS_Incident*> inc_list_type;
typedef inc_list_type::link_type inc_list_link_type;

/*
 * TMS_Incident. An accident that blocks two lanes of a segment is
 * treated as TWO incidents.
 */

class TMS_Incident : public TMS_Signal
{
public:

  TMS_Incident() : TMS_Signal() { }
  ~TMS_Incident() { }

  void print(std::ostream& os = std::cout);

  inline double timeFound() { return timeFound_; }
  inline double clearanceTime() {
	return startTime_ + duration_;
  }
  int situation();		/* Blockage + EVA */

  void send(IOService&);
  void receive(IOService&);

  /* The order of processing an incident is as follows:
   *  (1) receive()
   *  (2) response()	(invoked every iteration)
   *  (3) clearance()
   *  (4) remove()		(invoked when cleared)
   */

  void response();
  void clearance();
  void remove();

  float severity() { return severity_; }
  float position() { return position_; } // virtual
  float distance() { return distance_; } // virtual

  RN_Signal* prev();	// virtual
  RN_Signal* next();	// virtual
  int isLinkWide() { return 0; } // virtual
  int type() { return CTRL_INCIDENT; } // virtual

#ifdef INTERNAL_GUI
  Pixel * calcColor(unsigned int);
#endif

protected:

  inc_list_link_type container_;

  float severity_;			// capacity deduction
  float distance_;			// distance from the end of the link
  float position_;			// position (0-1) from the end of the segment

  double timeFound_;		// clock time in seconds
  double startTime_;		// time this incident actually occured
  double duration_;			// received and/or calculated in occurrenceLogic()

  int psPhase_;				// id for next portal closure stage
  double psTime_;			// time for next porta closure stage

  int vslsPhase_;			// id for next vsls stage
  double vslsTime_;			// time for next vsls stage

  int lusPhase_;			// id for next lus stage
  double lusTime_;			// time for next lus stage
};

// All the known incidents in the network are stored in a list.

extern inc_list_type *theIncidentList;

void CheckIncidents();

#endif
