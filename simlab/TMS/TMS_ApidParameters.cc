//-*-c++-*------------------------------------------------------------
// TMS_ApidParameters.cc
//
// Sreeram Radhakrishnan
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <stdio.h>
#include <math.h>

#include <Tools/ToolKit.h>
#include <IO/Exception.h>
#include "TMS_ApidParameters.h"
#include "TMS_ApidPrmParser.h"

void TMS_ApidParameters::print(ostream &os)
{
  os << indent << prmGrpID_ << endc << OPEN_TOKEN;
  os << endc << compWaveTh1_;
  os << endc << compWaveTh2_;
  os << endc << compWaveTestEnabled_;
  os << endc << persistTestEnabled_;
  os << endc << medTrfEnabled_;
  os << endc << ligtTrfEnabled_;
  os << endc << incDetTh1_;
  os << endc << incDetTh2_;
  os << endc << incDetTh3_;
  os << endc << medTrfIncDet1_;
  os << endc << medTrfIncDet2_;
  os << endc << medTrfTh_;
  os << endc << lowTrfTh_;
  os << endc << incClrTh_ << endl;
  os << endc << endc << endc << OPEN_TOKEN;

  os << endc << sensorList_.size() << endl;

  int nlines    = sensorList_.size() / SENSORS_PER_LINE;
  int last_line = sensorList_.size() % SENSORS_PER_LINE;
  register int j;

  for (j = 0; j < nlines; j ++) {
	for (register int k = 0; k < SENSORS_PER_LINE ; k ++) { 
	  os << endc << sensorList_[j*SENSORS_PER_LINE + k];
	}
	os << endl;
  }
  
  for(j = 0; j < last_line; j ++) {
	os << endc << sensorList_[nlines*SENSORS_PER_LINE + j];
  }

  os << endc << CLOSE_TOKEN << endc;
  os << CLOSE_TOKEN << endl;
};

