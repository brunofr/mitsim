//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_TollBooth.h
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#ifndef TMS_TOLLBOOTH_HEADER
#define TMS_TOLLBOOTH_HEADER

#include <GRN/RN_TollBooth.h>
#include "TMS_Signal.h"

class TMS_TollBooth : public RN_TollBooth, public TMS_Signal
{
   public:

      TMS_TollBooth();
      ~TMS_TollBooth() { }
};

#endif

