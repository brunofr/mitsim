//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_Legend.C
// DATE: Sun Feb 11 16:37:38 1996
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <DRN/DRN_DrawingArea.h>

#include "TMS_Legend.h"

void TMS_Legend::draw_more()
{
   // Common elements are alreay done in DRN_Legend::draw(). This
   // function append application specific legend
}


void TMS_Legend::draw(int mode)
{
   DRN_Legend::draw(mode);
}

#endif // THE END OF THIS FILE
