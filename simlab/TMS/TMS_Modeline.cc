//-*-c++-*------------------------------------------------------------
// TMS_Modeline.cc
//
// qiyang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <assert.h>
#include <stdio.h>

#include "TMS_Modeline.h"
#include "TMS_Interface.h"

TMS_Modeline::TMS_Modeline(Widget parent) : DRN_Modeline(parent)
{
   static String help_timers[3];
   help_timers[0] = XmtLocalize2(timers_,
								 "Current time", "timers", "current");
   help_timers[1] = XmtLocalize2(timers_,
								 "Real time left", "timers", "left");
   help_timers[2] = XmtLocalize2(timers_,
								 "Sensor time", "timers", "sensor");
   XtAddCallback(timers_,
			   XmNenterCellCallback, &XmwModeline::cellEnterCB,
			   help_timers);

   counters_ = XmtNameToWidget(parent, "*counters");
   assert (counters_);

   XtVaSetValues(counters_, XmNtraversalOn, False, NULL);
   static String help_counters[2];
   help_counters[0] = XmtLocalize2(counters_,
								   "Run/Total", "counters", "run");
   help_counters[1] = XmtLocalize2(counters_,
								   "Reserved", "counters", "reserved");
   XtAddCallback(counters_,
			   XmNenterCellCallback, &XmwModeline::cellEnterCB,
			   help_counters);
}

void TMS_Modeline::updateCounter(int i, int n)
{
   static char msg[6];
   sprintf(msg, "%5d", n);
   updateCounter(i, msg);
}

void TMS_Modeline::updateCounter(int i, const String msg)
{
   const char *s = XbaeMatrixGetCell(counters_, 0, i);
   if (strcmp(s, msg)) {
	  XbaeMatrixSetCell(counters_, 0, i, msg);
   }
}

#endif
