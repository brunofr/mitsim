//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_PsLogic.C
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#include <sstream>
using namespace std;

#include <Tools/Reader.h>
#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>

#include <Tools/Math.h>

#include <GRN/RN_CtrlStation.h>

#include "TMS_Parameter.h"
#include "TMS_CtrlLogic.h"
#include "TMS_PsAction.h"
#include "TMS_Incident.h"
#include "TMS_Link.h"
#include "TMS_Segment.h"
#include "TMS_Signal.h"

void
TMS_PsAction::read(Reader& is)
{
   static int nactions = 0;
   code_ = ++ nactions;

   is >> situation_;
   state_ = is.gethex();
   is >> fromDistance_;
   fromRef_ = toRef_ = 0;

   fromDistance_ *= theParameter->lengthFactor();
   toDistance_ = fromDistance_;
}

void
TMS_PsAction::print(ostream &os)
{
   os << indent 
      << situation_ << endc
      << hextag << hex << state_ << endc
      << (fromDistance_ / theParameter->lengthFactor()) << endl;
}


/*
 *------------------------------------------------------------------
 * Update the PS signs based on this TMS_PsAction rule.
 *
 * CAUTION: Only are the signs in the current link and the upstream
 * and downstream links with depth of 1 is searched.
 *
 *------------------------------------------------------------------
 */

void
TMS_PsAction::applyTo(TMS_Incident& inc)
{
   TMS_Link* link = (TMS_Link *) inc.link();
   CtrlList ctrl;
   double start_pos, stop_pos;

   start_pos = inc.distance();

   ctrl = link->searchTypedCtrl(
	  inc.distance(),
	  theCtrlLogic->searchingDistUpbound(),
	  CTRL_PS);
   if (ctrl) {
	  stop_pos = (*ctrl)->distance() + DIS_EPSILON;
   } else {
	  if (ToolKit::debug()) {
		 cout << "No UpPortal signal found. ";
		 cout << "PS command " << code_
			  << " applied within "
			  << theCtrlLogic->searchingDistUpbound() /
			theParameter->lengthFactor()
			  << " upstream." << endl;
	  }
	  stop_pos = inc.distance() +
		 theCtrlLogic->searchingDistUpbound();
   }
   stop_pos += toDistance_;

   double start_dis, stop_dis;
   int counts = 0;
   ostringstream os ;

   /* walk upstream */

   /* If the control range goes beyond downstream end of the
	* current link, set the signs in the downstream links.  */

   if (start_pos < 0.0) {
	  int narcs = link->nDnLinks();
	  for (int k = 0; k < narcs; k ++) {
		 RN_Link* plink = link->dnLink(k);
		 stop_dis = plink->length();
		 start_dis = start_pos + stop_dis;
		 ctrl = plink->prevCtrl(start_dis);
		 while (ctrl != NULL &&
				(*ctrl)->distance() <= stop_dis ) {
			if ((*ctrl)->type() == CTRL_PS) {
			   (*ctrl)->signal()->state(state_);
			   counts ++;
			   os << " " << (*ctrl)->signal()->code() ;
			}
			ctrl = ctrl->prev();
		 }
	  }
   }

   /* walking upstream from "start_dis" until "stop_dis".  Set all
	* the PSs in the marked link to the desired state.  */

   start_dis = Max(start_pos, 0.0);
   stop_dis = Min(stop_pos, link->length());
   ctrl = link->prevCtrl(start_dis);
   while (ctrl != NULL &&
		  (*ctrl)->distance() <= stop_dis ) {
	  if ((*ctrl)->type() == CTRL_PS) {
		 (*ctrl)->signal()->state(state_);
		 counts ++;
		 os << " " << (*ctrl)->signal()->code() ;
	  }
	  ctrl = ctrl->prev();
   }

   /* If the control range goes beyond upstream end of the current
	* link, continute to set the signs in the upstream links.  */

   start_dis = 0.0;
   stop_dis = stop_pos - link->length();
   if (stop_dis > 0.0) {
	  int narcs = link->nUpLinks();
	  for (int k = 0; k < narcs; k ++) {
		 ctrl = link->upLink(k)->lastCtrl();
		 while (ctrl != NULL &&
				(*ctrl)->distance() <= stop_dis ) {
			if ((*ctrl)->type() == CTRL_PS) {
			   (*ctrl)->signal()->state(state_);
			   counts ++;
			   os << " " << (*ctrl)->signal()->code() ;
			}
			ctrl = ctrl->prev();
		 }
	  }
   }

   if (ToolKit::verbose() && counts > 0) {
       //os << '\0' ;
     cout << "PS command " << parent_->code()
	  << "/" << code_ << " applied to " << counts
	  << " signs (" << os.str() << " ) at "
	  << theSimulationClock->currentStringTime() << endl;
   }

}
