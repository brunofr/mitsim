//-*-c++-*------------------------------------------------------------
// TMS_Communicator.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <iostream>
using namespace std;
#include <sys/param.h>

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>

#include <Tools/Math.h>
#include <TC/TC_ControlSystem.h>

#include "TMS_Communicator.h"
#include "TMS_Engine.h"
#include "TMS_Network.h"
#include "TMS_CtrlLogic.h"
#include "TMS_Incident.h"
#include "TMS_Sensor.h"
#include "TMS_Exception.h"
#include "TMS_FileManager.h"
#include "TMS_McmasterParameters.h"
#include "TMS_McmasterPrmTable.h"
#include "TMS_McmasterDetect.h"
#include "TMS_ApidPrmTable.h"
#include "TMS_ApidDetect.h"

#ifdef INTERNAL_GUI
#include <Xmt/Xmt.h>
#include <Xmt/Dialogs.h>
#include <DRN/DRN_DrawingArea.h>
#include "TMS_Interface.h"
#include "TMS_Modeline.h"
#endif


TMS_Communicator *theCommunicator = NULL;

TMS_Communicator::TMS_Communicator(int sndtag)
	: Communicator(sndtag)
{
}

double
TMS_Communicator::batchStepSize()
{
	return theEngine->batchStepSize();
}

// Make connection to its parent process (Simlab) and processes for
// other sibling modules.

void TMS_Communicator::makeFriends()
{
	if (!simlab_.isConnected()) return;

	// Send simlab the timing information

	simlab_ << MSG_TIMING_DATA
			<< theSimulationClock->startTime()
			<< theSimulationClock->stopTime()
			<< theSimulationClock->stepSize()
			<< batchStepSize()
			<< ToolKit::workdir();
	simlab_.flush(SEND_IMMEDIATELY);

	int sndtag = simlab_.sndtag();

	mitsim_.init("MITSIM", sndtag, MSG_TAG_MITSIM);
	meso_.init("MesoTS", sndtag, MSG_TAG_MESO);
	mdi_.init("MDI", sndtag, MSG_TAG_MDI);
	tmca_.init("TMCA", sndtag, MSG_TAG_TMCA);

	if (ToolKit::verbose()) {
		cout << theExec
			 << " waiting process ids from SIMLAB ..." << endl;
	}
}


// Response to the messages received from other processes

int
TMS_Communicator::receiveMessages(double sec)
{
	int msg, flag = 0;

	msg = receiveSimlabMessages(sec);
	if (msg < 0) {
		theEngine->quit(STATE_ERROR_QUIT);
		return -1;
	} else if (msg > 0) {
		flag |= 1;
	}
   
	msg = receiveMitsimMessages(sec);
	if (msg < 0) {
		theEngine->quit(STATE_ERROR_QUIT);
		return -1;
	} else if (msg > 0) {
		flag |= 2;
	}

	msg = receiveMesoMessages(sec);
	if (msg < 0) {
		theEngine->quit(STATE_ERROR_QUIT);
		return -1;
	} else if (msg > 0) {
		flag |= 4;
	}

	msg = receiveMdiMessages(sec);
	if (msg < 0) {
		theEngine->quit(STATE_ERROR_QUIT);
		return -1;
	} else if (msg > 0) {
		flag |= 8;
	}

	msg = receiveTmcaMessages(sec);
	if (msg < 0) {
		theEngine->quit(STATE_ERROR_QUIT);
		return -1;
	} else if (msg > 0) {
   	        flag |= 16;
	}	

	return flag;
}


/*
 * This function receives command from the Simlab -- the master
 * controller
 */

int
TMS_Communicator::receiveSimlabMessages(double sec)
{
	if (!simlab_.isConnected()) return 0;

	// Limited blocking receive (wait upto sec seconds)

	int flag = simlab_.receive(sec);
	if (flag <= 0) return flag;

	unsigned int type;
	simlab_ >> type;

	switch (type) {

	case MSG_PROCESS_IDS:
		int id1, id2, id3, id4;
		simlab_ >> id1 >> id2 >> id3 >> id4;
		if (id1 > 0) {
			mitsim_.connect(id1);
		}
		if (id2 > 0) {
			meso_.connect(id2);
		}
		if (id3 > 0) {
			mdi_.connect(id3);
		}
		if (id4 > 0 ) {
		        tmca_.connect(id4);
		}
		if (ToolKit::verbose()) {
			cout << theExec
				 << " received process ids from SIMLAB." << endl;
		}
		break;

	case MSG_CURRENT_TIME:
	{
		double now;
		simlab_ >> now;
		theSimulationClock->masterTime(now);
		break;
	}

#ifdef INTERNAL_GUI
	case MSG_PAUSE:
		theInterface->pause(0);
		break;

	case MSG_RESUME:
		theInterface->run(0);
		break;


	case MSG_WINDOW_SHOW:
		theInterface->manage();
		break;

	case MSG_WINDOW_HIDE:
		theInterface->unmanage();
		break;

#else
	case MSG_PAUSE:
	case MSG_RESUME:
	case MSG_WINDOW_SHOW:
	case MSG_WINDOW_HIDE:
	{
		break;			// not used in batch mode
	}
#endif

	case MSG_QUIT:
		theEngine->state(STATE_QUIT);
		break;

	case MSG_DONE:
		theEngine->state(STATE_DONE);
		break;

	default:
		skipUnknownMsg(simlab_.name(), type);
		break;

	}
	return flag;
}


// This function processes the surveillance data received from the
// MITSIM and updates the sensor state

int
TMS_Communicator::receiveMitsimMessages(double sec)
{
	if (!mitsim_.isConnected()) return 0;

	// Limited blocking receive (wait upto sec seconds)

	int flag = mitsim_.receive(sec);
	if (flag <= 0) return flag;

	unsigned int id;
	mitsim_ >> id;

	unsigned int type = id & MSG_TYPE ;

	if (type) {
		switch (type) {
		case MSG_TYPE_INCI:
		{
			TMS_Incident *inc = new TMS_Incident;
			inc->code(id & MSG_DATA_CODE) ;
			inc->receive(mitsim_);
			if (!(inc->state() & INCI_ACTIVE)) {
				delete inc ;
			}
			break ;
		}
		case MSG_TYPE_INCI_CALLINS:
		{
			skipUnknownMsg(mitsim_.name(), id, "Incident callins");
			break ;
		}
		default:
			do {
				id &= MSG_DATA_CODE;
				if (id < theNetwork->nSensors()) {
					theNetwork->sensor(id)->receive(mitsim_);
				} else {
					skipUnknownMsg(mitsim_.name(), id,
								   "Sensor index out of range");
				}
				mitsim_ >> id;
			} while (id & MSG_TYPE);
			break ;
		}
		return flag ;
	}

	switch (id) {

	case MSG_TIMING_DATA:
		mitsim_ >> theEngine->pathStepSize_;
		break;

	case MSG_SENSOR_RESET:
		mitsim_ >> type;
		if (type == SENSOR_AGGREGATE) {
			tmsNetwork()->resetSensorReadings();
		}
		break;

	case MSG_SENSOR_READY:

		mitsim_ >> type;

		if (type == SENSOR_AGGREGATE) {

			TMS_Sensor::startTime_ = TMS_Sensor::endTime_;
			TMS_Sensor::endTime_ = (long int)theSimulationClock->currentTime();
		
			tmsNetwork()->calcSegmentStates();
			tmsNetwork()->interpolateSegmentStates();

#ifdef INTERNAL_GUI
			theModeline()->updateTimer(2, theSimulationClock->currentTime());
			tmsNetwork()->drawNetwork(theDrawingArea);
#endif

			if(theEngine->controlLogic() & CONTROL_INC_DETECTION) {
				int timestep = TMS_Sensor::endTime_ - TMS_Sensor::startTime_;
				theIncidentDetector->incidentDetect(timestep);
			}

			TC_ControlSystem::controllersCalculateParameters();
		}

		break;

	case MSG_NEW_SURVEILLANCE_READY:
	  	if (tmca_.isConnected())
		{
		  double fromTime, toTime;
		  mitsim_ >> fromTime >> toTime;
		  tmca_ << MSG_NEW_SURVEILLANCE_READY 
			<< fromTime 
			<< toTime ;
		  tmca_.flush(SEND_IMMEDIATELY);
		}
		break;
	case MSG_STATE_DONE:
		if (meso_.isConnected()) {

			// PREPARE THE INITIAL GUIDANCE

			// Guidance currently used in MITSIM (the historical or
			// the best one found in the PREVIOUS cycle)

			char *i = theEngine->createFilename("ltt");

			// Guidance to be used in the first MESO sub-run
			// (previously predicted travel times)

			char *o = theEngine->createFilename("ltt", 0);;

			// Make a copy of i. All the MesoTS runs in this
			// prediction cycle will use o.

			if (ToolKit::copyFile(i, o)) {
				theException->exit();
			}
			free (o);
			free (i);

			if (theEngine->chosenOutput(0x10000000)) {
				// backup the state if desired
				o = theEngine->createFilename("dmp");
				const char *os = Str("%s.%d", o, theEngine->roll_);
				if (ToolKit::copyFile(o, os) != 0) {
					theException->exit();
				}
				free(o);
			}

			// Ask dynamit to start the OD estimation and prediction

			if (mdi_.isConnected()) {
				long now = (long)theEngine->beginTime_;
				long prev = now - (long)theEngine->rollingStepSize_;
				long next = (long)theEngine->endTime_;
				mdi_ << MSG_MESO_START
					 << theEngine->nTotalIterations_
					 << prev << now
					 << now << next;
				mdi_.flush(SEND_IMMEDIATELY);
			} else {
				// Now start run the MesoTS
				meso_ << MSG_MESO_START
					  << theEngine->iteration_
					  << theEngine->rollingLength_;
				meso_.flush(SEND_IMMEDIATELY);
			}
		}
		break;

	default:
		skipUnknownMsg(mitsim_.name(), id);
	}

	return flag;
}


int
TMS_Communicator::receiveMesoMessages(double sec)
{
	if (!meso_.isConnected()) return 0;

	// Limited blocking receive (wait upto sec seconds)

	int flag = meso_.receive(sec);
	if (flag <= 0) return flag;

	unsigned int id;
	meso_ >> id;

	switch (id) {

	case MSG_TIMING_DATA:
	{
		meso_ >> theEngine->pathStepSize_;
		break;
	}

	case MSG_CURRENT_TIME:
	{
		double now;
		meso_ >> now;
		if (mdi_.isConnected()) {
			mdi_ << MSG_CURRENT_TIME << now;
			mdi_.flush(SEND_IMMEDIATELY);
		}
		break;
	}

	case MSG_MESO_DONE:
	{
		char tag[128];
		meso_ >> tag;

		theEngine->modifyGuidance(tag);

		if (theEngine->iteration_ < theEngine->nTotalIterations_) {

			theEngine->iteration_++;

			// Repeat MesoTS

			meso_ << MSG_MESO_START
				  << theEngine->iteration_
				  << theEngine->rollingLength_;
			meso_.flush(SEND_IMMEDIATELY);

		} else {					// last iteration for a prediction
			if (mitsim_.isConnected()) {

				// Now the routing table is ready to use. Schedule the
				// time to update the MITSIM routing table

				theEngine->routingTableTime_ =
					theEngine->routingTableDueTime_;

				// We set this so that TMS will not wait for MesoTS result.

				theEngine->routingTableDueTime_ = DBL_INF;
			}
			theEngine->saveState3d("t3d");
			if (mdi_.isConnected()) {
				mdi_ << theEngine->roll_ << MSG_STATE_DONE;
				mdi_.flush(SEND_IMMEDIATELY);
			}
		}

		if (mdi_.isConnected()) {
			mdi_ << MSG_MESO_DONE
				 << theEngine->iteration_;
			mdi_.flush(SEND_IMMEDIATELY);
		}
		break;
	}
	default:
	{
		skipUnknownMsg(meso_.name(), id);
		break;
	}
	}

	return flag;
}

int TMS_Communicator::receiveMdiMessages(double sec)
{
	if (!mdi_.isConnected()) return 0;

	// Limited blocking receive (wait upto sec seconds)

	int flag = mdi_.receive(sec);
	if (flag <= 0) return flag;

	unsigned int id;
	mdi_ >> id;

	switch (id) {
	case MSG_MDI_DONE:
	{
		char filename[MAXPATHLEN+1];
		mdi_ >> filename;

		// Now start run the MesoTS

		if (meso_.isConnected()) {

			// Now the predicted OD table is ready to use.  MesoTS will be
			// started.

			meso_ << MSG_MDI_DONE << filename
				  << theEngine->iteration_
				  << theEngine->rollingLength_;
			meso_.flush(SEND_IMMEDIATELY);
		}

#ifdef INTERNAL_GUI
		if (theEngine->isDemoMode()) {
			XmtDisplayWarningMsg(theInterface->widget(), "dtaDone",
								 "Package %s arrived.",
								 "TMS", // default title
								 NULL, // default help
								 filename); // args
		}
#endif
		break;
	}
	default:
	{
		skipUnknownMsg(mdi_.name(), id);
		break;
	}
	}

	return flag;
}

int TMS_Communicator::receiveTmcaMessages(double sec)
{
	if (!tmca_.isConnected()) return 0;

	// Limited blocking receive (wait upto sec seconds)
	
	int flag = tmca_.receive(sec);
	if (flag <= 0) return flag;

	unsigned int id;
	tmca_ >> id;


	switch (id) {
	case MSG_DUMP_SURVEILLANCE:
	{
                int sensorDumpNeeded;
                tmca_ >> sensorDumpNeeded; 
	        mitsim_ << MSG_DUMP_SURVEILLANCE 
			<< sensorDumpNeeded;
		mitsim_.flush(SEND_IMMEDIATELY);
		break;
	}


	default:
	{
		skipUnknownMsg(tmca_.name(), id);
		break;
	}
	}

	return flag;
}


void TMS_Communicator::startStateEstimation()
{
	mitsim_ << MSG_STATE_START << theEngine->beginTime_;
	mitsim_.flush(SEND_IMMEDIATELY);
}
