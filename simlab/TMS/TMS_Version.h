//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: TMS_Version.h
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#ifndef TMS_VERSION_HEADER
#define TMS_VERSION_HEADER

extern char*  g_majorVersionNumber;
extern char*  g_minorVersionNumber;
extern char*  g_codeDate;

extern void Welcome(char *);

#endif
