//-*-c++-*------------------------------------------------------------
// TMS_SurvStation.h
//
// Sreeram Radhakrishnan
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef TMS_SURVSTATION_HEADER
#define TMS_SURVSTATION_HEADER

#include <GRN/RN_SurvStation.h>


// All following constants are used together with stationState

const int NEW_COMP_WAVE         = 0x1000; // used by APID
const int INC_DCR_PERSIST_ONE   = 0x0010; // used by APID
const int INC_DCR_PERSIST_CNT   = 0x00F0; // used by APID
const int ERROR_MESG            = 0x8000; // used by APID

const int SECONDARY_INC         = 0x0001; // used by McMaster

//const int NO_INCIDENT           = 0x0001; // used by both
const int TENTATIVE_INC         = 0x0001; // used by APID
const int CONFIRMED_INC         = 0x0002; // used by both
const int CLEARED_INC           = 0x0004; // used by both
const int INC_STATUS            = 0x000F; // used by both

class TMS_SurvStation : public RN_SurvStation
{
protected:

  int prmGrpID_;     // parameter group ID used in incident detection
  int stationState_; // state of the station based on the sensor state
  
public:

  TMS_SurvStation() : RN_SurvStation(),
	prmGrpID_(-1), stationState_(0) {
  }

  ~TMS_SurvStation() { }

  int prmGrpID() { return prmGrpID_; }
  void setprmGrpID(int n) { prmGrpID_ = n; }
  int stationState() { return stationState_; }

  unsigned int setState(unsigned int flag){ 
	return (stationState_ |= flag);
  }

  unsigned int clearState(unsigned int flag) {
	return (stationState_ &= ~flag);
  }

  unsigned int addState(unsigned int flag) {
	return (stationState_ += flag);
  }

  int incDcrPersistCount() {
	return (((stationState_) & INC_DCR_PERSIST_CNT) >> 4);
  }
};

#endif
