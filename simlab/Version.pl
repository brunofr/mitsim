#!/usr/bin/perl

# This tracks the user and time that built the executable

$user = getlogin ;
($ss,$mm,$hh,$dd,$mm,$yy) = localtime ;
$mm ++ ;                        # Opps, month starts from 0
$yy += 1900;
print "// Code automatically generated code by $0\n" ;
print
  "const char* theVersionInfo[2] = {\n",
  "\t\"$user\",\n",
  "\t\"$hh:$mm:$ss $mm/$dd/$yy\"\n",
  "};\n" ;
