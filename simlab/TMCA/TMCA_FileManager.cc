//-*-c++-*------------------------------------------------------------
// TMCA_FileManager.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <cstring>
#include <iostream>
#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <Tools/SimulationClock.h>

// This file is generated from VariableParser.y

#include <Tools/VariableParser.h>

#include <IO/MessageTags.h>
#include <IO/Exception.h>

#include "TMCA_FileManager.h"
#include "TMCA_Engine.h"
#include "TMCA_Communicator.h"
using namespace std;

TMCA_FileManager* theFileManager = new TMCA_FileManager;

TMCA_FileManager::TMCA_FileManager()
   : GenericSwitcher(),
     title_(NULL)
{
}


void TMCA_FileManager::set(char **ptr, const char *s)
{
   if (*ptr) {
	  free(*ptr);
   }
   if (s) {
	  *ptr = strdup(s);
   } else {
	  *ptr = NULL;
   }
}


// Read the variables from master file.

void
TMCA_FileManager::readMasterFile()
{
  //  GenericSwitcher gs ;
  VariableParser vp( this );
  vp.parse(theEngine->master());
}


// Check if two token are equal.

int TMCA_FileManager::isEqual(const char *s1, const char *s2)
{
   return IsEqual(s1, s2, 1);
}


// This function returns 0 if the variable is parsed or 1 if it is
// skipped (in case command line provides the value), or -1 if error
// occurs.

int TMCA_FileManager::parseVariable(char ** varptr, GenericVariable &gv)
{
   *varptr = Copy(gv.string());
   return 0;
}


// All the variables provided in the master file are parsed in this
// function.

int
TMCA_FileManager::parseVariable(GenericVariable &gv) 
{
   char * token = compact(gv.name());

   if (isEqual(token, "Title")) {
      title_ = Copy(gv.string());

   } else if (isEqual(token, "DefaultParameterDirectory")) {
	  ToolKit::paradir(gv.string());
   } else if (isEqual(token, "InputDirectory")) {
      ToolKit::indir(gv.string());
   } else if (isEqual(token, "OutputDirectory")) {
      ToolKit::outdir(gv.string());
   } else if (isEqual(token, "WorkingDirectory")) {
      ToolKit::workdir(gv.string());
   } else if (ToolKit::verbose()) {

      return 1;
   }

   return 0;
}


void
TMCA_FileManager::openOutputFiles()
{
   if (theEngine->chosenOutput()) {
	  cout << "Chosen output = " << theEngine->chosenOutput() << endl;
   }

   // Code for opening output files goes here
}

void
TMCA_FileManager::closeOutputFiles()
{
   cout << "Output files are in directory <"
		<< ToolKit::outDir() << ">." << endl;

   // Code closing output files goes here
}
