//-*-c++-*------------------------------------------------------------
// TMCA_Version.cc
//
// Bruno Fernandez
//
//--------------------------------------------------------------------

#include <iostream>
#include <new>
using namespace std;

#include <Tools/ToolKit.h>

#include "TMCA_Exception.h"
#include "TMCA_Version.h"

char*  g_majorVersionNumber = "0";
char*  g_minorVersionNumber = "1 Alpha";
char*  g_codeDate = "1999";

void
Welcome(char *exec)
{
   theExec = Copy(ToolKit::RealPath(exec));

   cout << "Traffic Management Center Adaptor Release "
	<< g_majorVersionNumber << "." << g_minorVersionNumber << endl
	<< "Copyright (c) " << g_codeDate << endl
	<< "Massachusetts Institute of Technology" << endl
	<< "All Right Reserved" << endl << endl;

#ifndef FINAL_VERSION
   PrintVersionInfo();
#endif

   cout.flush();

   set_new_handler(::FreeRamException);
}
