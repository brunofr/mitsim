//-*-c++-*------------------------------------------------------------
// TMCA_Engine.h
//
// Bruno Fernandez
//--------------------------------------------------------------------

#ifndef TMCA_ENGINE_HEADER
#define TMCA_ENGINE_HEADER

#include <Tools/SimulationEngine.h>

class TMCA_Engine : public SimulationEngine
{
      friend class TMCA_CmdArgsParser;
      friend class TMCA_Communicator;
      friend class TMCA_FileManager;

   public:
      
      TMCA_Engine();
      ~TMCA_Engine() { }

      const char *ext();

      void init();

      // This function starts the simulation loops. It will NOT return
      // until simulation is done (or cancelled in graphical mode).
      // This function calls simulationLoop() iteratively or as a
      // recusive timeout callback.

      void run();
      void quit(int state);

      int loadSimulationFiles();
      int loadMasterFile();
      int simulationLoop();

      int receiveMessages(double sec = 0.0);	// virtual
      void save(ostream &os = cout); // virtual
};

extern TMCA_Engine * theEngine;

#endif
