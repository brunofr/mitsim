//-*-c++-*------------------------------------------------------------
// NAME: Functions for communication to MITSIM and TMS
// AUTH: Bruno Fernandez
// FILE: TMCA_Communicator.cc
//--------------------------------------------------------------------


#include <iostream>
using namespace std;

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>
#include <Tools/Math.h>

#include "TMCA_Communicator.h"
#include "TMCA_Engine.h"
#include "TMCA_Exception.h"
#include "TMCA_FileManager.h"

TMCA_Communicator* theCommunicator = NULL;

TMCA_Communicator::TMCA_Communicator(int sndtag)
  : Communicator(sndtag)
{
}


// Make connection to its parent process (Simlab) and processes for
// other sibling modules.

void TMCA_Communicator::makeFriends()
{
  if (!simlab_.isConnected()) 
      return;
  
  simlab_ << MSG_TIMING_DATA
		  << ToolKit::workdir();
  simlab_.flush(SEND_IMMEDIATELY);

  tms_.init("TMS", simlab_.sndtag(), MSG_TAG_TMS);

  if (ToolKit::verbose()) {
	cout << theExec << " waiting process ids from SIMLAB ..." << endl; 
  }
}


// Response to the messages received from other processes

int
TMCA_Communicator::receiveMessages(double sec)
{
  int msg, flag = 0;

  msg = receiveSimlabMessages(sec);
  if (msg < 0) {
	theEngine->quit(STATE_ERROR_QUIT);
	return -1;
  } else if (msg > 0) {
	flag |= 1;
  }
   
  msg = receiveTmsMessages(sec);
  if (msg < 0) {
	theEngine->quit(STATE_ERROR_QUIT);
	return -1;
  } else if (msg > 0) {
	flag |= 4;
  }

  return flag;
}


// Response to the messages received from other processes

int
TMCA_Communicator::receiveSimlabMessages(double sec)
{
  if (!simlab_.isConnected())
    return 0;

  // Limited blocking receive (wait upto sec seconds)

  int flag = simlab_.receive(sec);

  if (flag <= 0) return flag;

  unsigned int type;
  simlab_ >> type;

  switch (type) {

  case MSG_PROCESS_IDS:
	{
	  int id;
	  simlab_ >> id;
	  if (id > 0) {
		tms_.connect(id);
	  }
	  if (ToolKit::verbose()) {
		cout << theExec << " received TMS process id <"
			 << hex << id << dec << "> from SIMLAB." << endl;
	  }
	  requestSurveillance( 1 );
	  break;
	}

  case MSG_CURRENT_TIME:
	{
	  double now;
	  simlab_ >> now;
	  theSimulationClock->masterTime(now);
	  // this is the place to check for
	  // messages from CORBA modules
	  break;
	}

  case MSG_PAUSE:
	theSimulationClock->pause();
	break;

  case MSG_RESUME:
	theSimulationClock->resume();
	break;

  case MSG_WINDOW_SHOW:
  case MSG_WINDOW_HIDE:
	break;			// not used in batch mode

  case MSG_QUIT:
	theEngine->state(STATE_QUIT);
	break;

  case MSG_DONE:
	theEngine->state(STATE_DONE);
	break;

  default:
	cerr << "Warning:: Message type <"
		 << type << "> received from "
		 << simlab_.name() << " not supported." << endl;
	break;
  }
  return flag;
}


// This function checks and receives messages from TMS

int
TMCA_Communicator::receiveTmsMessages(double sec)
{
  int nMaxIterations = 0;

  if (!tms_.isConnected()) return 0;

  // Limited blocking receive (wait upto sec seconds)

  int flag = tms_.receive(sec);
  if (flag <= 0) return flag;

  unsigned int id;
  tms_ >> id;

  switch (id) {
  case MSG_STATE_START:
	{
	  break;
	}

  case MSG_NEW_SURVEILLANCE_READY:
	{
	  double fromTime, toTime;
	  tms_ >> fromTime >> toTime;
	  DEBUG_MESSAGE( "TMCA has received surveillance for ( " 
			 << fromTime << " , " << toTime << " )" );
	  break;
	}        
  case MSG_STATE_DONE:
	{
	}
  case MSG_CURRENT_TIME:
	{
	  break;
	}

  default:
	{
	  cerr << "Warning:: Unknown TMS message <" << id
		   << "> received." << endl;
	  break;
	}
  }
   
  return flag;
}

void
TMCA_Communicator::sendNextTimeToSimlab()
{
  if (!simlab_.isConnected()) return;

  SimulationClock *sc = theSimulationClock;
   
  simlab_ << MSG_END_CYCLE
		  << sc->currentTime()
		  << sc->lastStepCpuUsage();
  simlab_.flush(SEND_IMMEDIATELY);
}

void
TMCA_Communicator::requestSurveillance( int sensorDumpNeeded )
{
  if( !tms_.isConnected() ) return;

  tms_ << MSG_DUMP_SURVEILLANCE
       << sensorDumpNeeded;
  tms_.flush(SEND_IMMEDIATELY);
}
