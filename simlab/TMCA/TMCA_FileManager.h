//-*-c++-*------------------------------------------------------------
// TMCA_FileManager.h
//
// Bruno Fernandez
//
// This is a class that manages all the input and output files.  It
// parses the master file to obtain this information.
//--------------------------------------------------------------------

#ifndef TMCA_FILEMANAGER_HEADER
#define TMCA_FILEMANAGER_HEADER

//#include <fstream>
#include <Tools/GenericSwitcher.h>

class GenericVariable;

class TMCA_FileManager : public GenericSwitcher
{
	  friend class TMCA_Engine;
	  friend class TMCA_Communicator;

  public:

      TMCA_FileManager();
      ~TMCA_FileManager() { }

	  void set(char **ptr, const char *s);

      char* title() { return title_; }
	  void title(const char *s) { set(&title_, s); }

      void readMasterFile();

	  void openOutputFiles();
	  void closeOutputFiles();

      int parseVariable(GenericVariable &gv); // virtual

   private:

      char* title_;

   private:

      int parseVariable(char ** varptr, GenericVariable &gv);
      int isEqual(const char *s1, const char *s2);
};

extern TMCA_FileManager * theFileManager;

#endif
