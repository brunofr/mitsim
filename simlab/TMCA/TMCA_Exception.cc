//-*-c++-*------------------------------------------------------------
// TMCA_Exception.cc
//
// Bruno Fernandez
//
//--------------------------------------------------------------------


#include <Tools/SimulationClock.h>

#include "TMCA_Exception.h"
#include "TMCA_Communicator.h"

void
TMCA_Exception::exit(int code)
{
   if (theCommunicator->isSimlabConnected()) {
      theCommunicator->simlab() << MSG_QUIT;
      theCommunicator->simlab().flush(SEND_IMMEDIATELY);
   }
   IOService::exit();
   Exception::exit(code);
}

void
TMCA_Exception::done(int code)
{
   if (theCommunicator->isSimlabConnected()) {
      theCommunicator->simlab() << MSG_DONE;
      theCommunicator->simlab().flush(SEND_IMMEDIATELY);
   }
   IOService::exit();
   Exception::done(code);
}
