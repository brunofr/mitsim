//-*-c++-*------------------------------------------------------------
// Main.cc
//
// Bruno Fernandez
//
//--------------------------------------------------------------------

#include <iostream>
using namespace std;

#include <Tools/ToolKit.h>

#include "TMCA_Engine.h"
#include "TMCA_Version.h"
#include "TMCA_CmdArgsParser.h"

int main ( int argc, char **argv )
{
  ::Welcome(argv[0]);

  ToolKit::initialize();

  TMCA_Engine engine;

  TMCA_CmdArgsParser cp;
  cp.parse(&argc, argv);

  engine.run();

  engine.quit(STATE_DONE);

  return 0;
}
