//-*-c++-*------------------------------------------------------------
// TMCA_CmdArgsParser.h
//
// Bruno Fernandez
//
//--------------------------------------------------------------------

#ifndef TMCA_CMDARGSPARSER_HEADER
#define TMCA_CMDARGSPARSER_HEADER

#include <Tools/CmdArgsParser.h>

class TMCA_CmdArgsParser : public CmdArgsParser
{
   public:
      
      TMCA_CmdArgsParser();
      virtual ~TMCA_CmdArgsParser() { }
};

#endif
