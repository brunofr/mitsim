//-*-c++-*------------------------------------------------------------
// TMCA_Engine.cc
//
// Bruno Fernandez
//
//--------------------------------------------------------------------

#include <cstring>
#include <new>
using namespace std;

#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <Tools/SimulationClock.h>
#include <Tools/Math.h>

#include "TMCA_Communicator.h"
#include "TMCA_Engine.h"
#include "TMCA_FileManager.h"
#include "TMCA_Exception.h"

TMCA_Engine* theEngine = NULL;

TMCA_Engine::TMCA_Engine()
  : SimulationEngine()
{
  theException = new TMCA_Exception("tmca");

  theEngine = this;  
  if (!theSimulationClock) {
	theSimulationClock = new SimulationClock;
  }

  theCommunicator = new TMCA_Communicator(MSG_TAG_TMCA);

  theSimulationClock->init(28800.0, 32400.0, 1.0);
}


const char* TMCA_Engine::ext()
{
  return ".tmca";
}


void
TMCA_Engine::init()
{
  theSimulationClock->init();
  //  double s = theSimulationClock->currentTime();
}


int
TMCA_Engine::loadMasterFile()
{
  if (SimulationEngine::loadMasterFile() != 0) {
	cerr << "Error:: Cannot find the master file <"
		 << master() << ">." << endl;
	quit(STATE_ERROR_QUIT);
	return 1;
  }

  theFileManager->readMasterFile();
  state_ = STATE_OK;

  return 0;
}


// This function is called every time a new master file is loaded.
// It return 0 if no error.

int
TMCA_Engine::loadSimulationFiles()
{
  if (state_ == STATE_NOT_STARTED) {
	if (loadMasterFile() != 0) return 1;
  }
   
  init();

  theFileManager->openOutputFiles();

  // Now let the engine start

  start();

  // Make connection to other modules if they exist.

  if (!theCommunicator->isSimlabConnected()) return 0;

  theCommunicator->makeFriends();

  // Read necessary input files here

  return 0;
}

// This procedure starts the simulation loops.

void TMCA_Engine::run()
{
  if (canStart()) {
	loadMasterFile();
  }
 
  loadSimulationFiles();

  while ( state_ >= 0 && ( 
			    receiveMessages() > 0 || 
                            isWaiting() && receiveMessages(NO_MSG_WAITING_TIME) >= 0 ||
                            simulationLoop() >= 0
			   )
	 ) 
  {
	;
  }
}


void
TMCA_Engine::quit(int state)
{
  theFileManager->closeOutputFiles();
  SimulationEngine::quit(state);
  theException->done(0);
}


int TMCA_Engine::simulationLoop()
{
  SimulationClock *clock = theSimulationClock;

  // This function is called once every simulation cycle when there
  // is no IPC and X events awaiting.

  // Real simulation code goes here

  double now = clock->currentTime();

  if (now >= clock->masterTime()) {
	return (state_ = STATE_WAITING);
  }

  // Real simulation code end here

  // Advance the clock

  clock->advance();

  if (theCommunicator->isTmsConnected()) {
	if (clock->currentTime() > clock->masterTime()) {
	  clock->masterTime(clock->startTime()-1);
	  return (state_ = STATE_WAITING);
	}
  } else if(now > clock->stopTime()) {
	return (state_ = STATE_DONE);
  }

  return state_ = STATE_OK;
}


// Process the messages received from other processes

int
TMCA_Engine::receiveMessages(double sec)
{
  if (!theCommunicator->isSimlabConnected()) return 0;
  else return  theCommunicator->receiveMessages(sec);
}

// Save the master file

void TMCA_Engine::save(ostream &os)
{
  os << "/* " << endl
	 << " * TMCA Communicator master file" << endl
	 << " */" << endl << endl;

  os << "[Title] = \"" << theFileManager->title() << "\"" << endl
	 << endl;

  os << "[Default Parameter Directory]     = \""
	 << NoNull(ToolKit::paradir())
	 << "\"" << endl;
	 
  os << "[Input Directory]                 = \""
	 << NoNull(ToolKit::indir())
	 << "\"" << endl;

  os << "[Output Directory]                = \""
	 << NoNull(ToolKit::outdir())
	 << "\"" << endl;

  os << "[Working Directory]               = \""
	 << NoNull(ToolKit::workdir())
	 << "\"" << endl;

  os << endl;
}

