//-*-c++-*------------------------------------------------------------
// TMCA_Communicator.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef TMCA_COMMUNICATOR_HEADER
#define TMCA_COMMUNICATOR_HEADER

#include <IO/Communicator.h>

#define DEBUG_MESSAGE(message) \
cout << __FILE__ << ":" << __LINE__ << " " << message << endl;


class TMCA_Communicator : public Communicator
{
protected:

  IOService tms_;

public:

  TMCA_Communicator(int sndtag);
  ~TMCA_Communicator() { }
      
  inline int isTmsConnected() {
	return tms_.isConnected();
  }
      
  IOService& tms() { return tms_; }

  int receiveSimlabMessages(double sec = 0.0);
  int receiveTmsMessages(double sec = 0.0);

  void requestSurveillance( int sensorDumpNeeded );

  // Functions defined in base class

  void makeFriends();
  int receiveMessages(double sec = 0.0);
  void sendNextTimeToSimlab();
};

extern TMCA_Communicator* theCommunicator;

#endif
