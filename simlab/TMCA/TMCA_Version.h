//-*-c++-*------------------------------------------------------------
// TMCA_Version.h
//
// Bruno Fernandez
//
//--------------------------------------------------------------------

#ifndef TMCA_VERSION_HEADER
#define TMCA_VERSION_HEADER

extern char*  g_majorVersionNumber;
extern char*  g_minorVersionNumber;
extern char*  g_codeDate;

extern void Welcome(char *);

#endif
