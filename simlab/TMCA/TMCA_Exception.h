//-*-c++-*------------------------------------------------------------
// TMCA_Exception.h
//
// Bruno Fernandez
//
// Because this program is a module in a distributed system and works
// together with other modules, it should call exit(code) or
// done(code) instead of the C function exit() to terminate the
// program.  This allows a message is sent to other modules for the
// killing event (they may want to kill themselves too).
//
//--------------------------------------------------------------------

#ifndef TMCA_EXCEPTION_HEADER
#define TMCA_EXCEPTION_HEADER

#include <IO/Exception.h>

class TMCA_Exception : public Exception
{
   public:

      TMCA_Exception(const char *name) : Exception(name) { }
      ~TMCA_Exception() { }

      void exit(int code = 0);
      void done(int code = 0);
};

#endif
