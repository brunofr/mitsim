//-*-c++-*------------------------------------------------------------
// TMCA_CmdArgsParser.cc
//
// Bruno Fernandez
//
//--------------------------------------------------------------------

#include "TMCA_CmdArgsParser.h"
#include "TMCA_Engine.h"

TMCA_CmdArgsParser::TMCA_CmdArgsParser() : CmdArgsParser()
{
   add(new Clo("-output", &theEngine->chosenOutput_, 0, "Output mask"));
}
