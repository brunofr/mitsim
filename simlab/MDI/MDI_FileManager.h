//-*-c++-*------------------------------------------------------------
// MDI_FileManager.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This is a class that manager all the input and output files.  It
// parses the master file to obtain these information.
//--------------------------------------------------------------------

#ifndef MDI_FILEMANAGER_HEADER
#define MDI_FILEMANAGER_HEADER

#include <fstream.h>
#include <Tools/GenericSwitcher.h>

class GenericVariable;

class MDI_FileManager : public GenericSwitcher
{
	  friend class MDI_Engine;
	  friend class MDI_Communicator;

  public:

      MDI_FileManager();
      ~MDI_FileManager() { }

	  void set(char **ptr, const char *s);

      char* title() { return title_; }
	  void title(const char *s) { set(&title_, s); }

      void readMasterFile();

	  void openOutputFiles();
	  void closeOutputFiles();

      int parseVariable(GenericVariable &gv); // virtual

   private:

      char* title_;
	  char* paraFile_;
	  char* sensorFile_;
	  char* vrcFile_;

   private:

      int parseVariable(char ** varptr, GenericVariable &gv);
      int isEqual(const char *s1, const char *s2);
};

extern MDI_FileManager * theFileManager;

#endif
