//-*-c++-*------------------------------------------------------------
// NAME: Functions for communication to MITSIM and TMS
// AUTH: Qi Yang
// FILE: MDI_Communicator.C
// DATE: Mon Feb 26 15:35:54 1996
//--------------------------------------------------------------------


// DTA includes

#include <bindUtilities.h>
#include <dtaTimeInterval.h>
#include <dtaAbsTime.h>

#include <iostream.h>

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>
#include <Tools/Math.h>

#include "MDI_Communicator.h"
#include "MDI_Engine.h"
#include "MDI_Exception.h"
#include "MDI_FileManager.h"

MDI_Communicator* theCommunicator = NULL;

MDI_Communicator::MDI_Communicator(int sndtag)
  : Communicator(sndtag),
	dynamit_(idlMitsimDemand::_nil()),
	stage_(0)
{
}


// Make connection to its parent process (Simlab) and processes for
// other sibling modules.

void MDI_Communicator::makeFriends()
{
  if (!simlab_.isConnected()) return;
  
  simlab_ << MSG_TIMING_DATA
		  << ToolKit::workdir();
  simlab_.flush(SEND_IMMEDIATELY);

  tms_.init("TMS", simlab_.sndtag(), MSG_TAG_TMS);

  if (ToolKit::verbose()) {
	cout << theExec << " waiting process ids from SIMLAB ..." << endl; 
	cout << theExec << " waiting for DynaMIT ..." << endl ;
  }

  TRY {
	dynamit_ = bindObject(idlMitsimDemand::name, dynamit_) ;
  }
  CATCH(CORBA::SystemException, sysEx) {
	cerr << "Corba system error " << &sysEx << endl ;
	theException->exit();
  }                                               
  CATCHANY {                                      
	cerr << "Corba bind to DynaMIT failed. Exception "
		 << IT_X << endl ; 
	theException->exit();
  }
  ENDTRY ;

  if (CORBA::is_nil(dynamit_)) {
	cerr << "Error:: Failed connecting to DynaMIT." << endl;
	theException->exit();
  }

  cout << "CORBA Object: " << dynamit_->name << endl ;
  dynamit_->init() ;

  TRY {
	dtaStatus_ = bindObject(idlStatusManager::name, dtaStatus_) ;
  }
  CATCH(CORBA::SystemException, sysEx) {
	cerr << "Corba system error " << &sysEx << endl ;
	theException->exit();
  }                                               
  CATCHANY {                                      
	cerr << "Corba bind to Status Manager failed. Exception "
		 << IT_X << endl ; 
	theException->exit();
  }
  ENDTRY ;
   
}


// Response to the messages received from other processes

int
MDI_Communicator::receiveMessages(double sec)
{
  int msg, flag = 0;

  msg = receiveSimlabMessages(sec);
  if (msg < 0) {
	theEngine->quit(STATE_ERROR_QUIT);
	return -1;
  } else if (msg > 0) {
	flag |= 1;
  }
   
  msg = receiveTmsMessages(sec);
  if (msg < 0) {
	theEngine->quit(STATE_ERROR_QUIT);
	return -1;
  } else if (msg > 0) {
	flag |= 4;
  }

  if (stage_ == 0) return flag;

  // Check the dta status
   
  if (CORBA::is_nil(dtaStatus())) {
	cerr << "Null CORBA pointer." << endl ;
	theException->exit() ;
  }

  idlStatus status;

  TRY {
	if (stage_ == STAGE_OD_ESTIMATION) {
	  status = dtaStatus()->getOdEstimation() ;
	} else if (stage_ == STAGE_OD_PREDICTION) {
	  status = dtaStatus()->getOdPrediction() ;
	} else {
	  cerr << "Stage " << stage_ << "? Impossible!" << endl;
	  theException->exit();
	}
  }
  CATCHANY {
	cerr << "Unable to get status: " << IT_X << endl ;
	theException->exit() ;
  }
  ENDTRY;

  const char* what[3] = {
	"DynaMIT",
	"OD estimation",
	"OD prediction"
  };

  switch (status) {

  case idlStatusRunning:
	break;

  case idlStatusFinished:
	if (stage_ == STAGE_OD_ESTIMATION) {
	  askDynamitToStartPrediction();
	} else if (stage_ == STAGE_OD_PREDICTION) {
	  announceDynamitIsDone();			
	}

	break;

  case idlStatusError:
	cerr << "Error in " << what[stage_] << "." << endl;
	theException->exit();
	break;

  case idlStatusUnknown:
	break;

  default:
	cerr << "Unknown " << what[stage_] << " status "
		 << status << "." << endl;
	break;
  }

  return flag;
}


// Response to the messages received from other processes

int
MDI_Communicator::receiveSimlabMessages(double sec)
{
  if (!simlab_.isConnected()) return 0;

  // Limited blocking receive (wait upto sec seconds)

  int flag = simlab_.receive(sec);
  if (flag <= 0) return flag;

  unsigned int type;
  simlab_ >> type;

  switch (type) {

  case MSG_PROCESS_IDS:
	{
	  int id;
	  simlab_ >> id;
	  if (id > 0) {
		tms_.connect(id);
	  }
	  if (ToolKit::verbose()) {
		cout << theExec << " received TMS process id <"
			 << hex << id << dec << "> from SIMLAB." << endl;
	  }
	  break;
	}

  case MSG_CURRENT_TIME:
	{
	  double now;
	  simlab_ >> now;
	  theSimulationClock->masterTime(now);
	  idlUnitTime t = dynamitTime(long(now));
	  dtaStatus_->setRealTime(t);
	  break;
	}

  case MSG_PAUSE:
	theSimulationClock->pause();
	break;

  case MSG_RESUME:
	theSimulationClock->resume();
	break;

  case MSG_WINDOW_SHOW:
  case MSG_WINDOW_HIDE:
	break;			// not used in batch mode

  case MSG_QUIT:
	theEngine->state(STATE_QUIT);
	break;

  case MSG_DONE:
	theEngine->state(STATE_DONE);
	break;

  default:
	cerr << "Warning:: Message type <"
		 << type << "> received from "
		 << simlab_.name() << " not supported." << endl;
	break;
  }
  return flag;
}


// This function checks and receives messages from TMS

int
MDI_Communicator::receiveTmsMessages(double sec)
{
  int nMaxIterations = 0;

  if (!tms_.isConnected()) return 0;

  // Limited blocking receive (wait upto sec seconds)

  int flag = tms_.receive(sec);
  if (flag <= 0) return flag;

  unsigned int id;
  tms_ >> id;

  switch (id) {

  case MSG_MESO_START:
	{
	  tms_ >> nMaxIterations	// max number of iterations
		   >> estStartTime_  >> estEndTime_
		   >> predStartTime_ >> predEndTime_;

	  idlTimeInterval interval;

	  interval = dynamitTimeInterval(estStartTime_, estEndTime_);
	  dtaStatus_->setEstimationInterval(interval);

	  interval = dynamitTimeInterval(predStartTime_, predEndTime_);
	  dtaStatus_->setPredictionInterval(interval);

	  dtaStatus_->setPredGuidanceMaxiter(nMaxIterations);
	  dtaStatus_->setPredGuidanceIteration(0);

	  askDynamitToStartEstimation();

	  break;
	}

  case MSG_MESO_DONE:
	{
	  int i;
	  tms_ >> i;				// iteration counter
	  dtaStatus_->setPredGuidanceIteration(i);
	  if (i >= nMaxIterations) {
		dtaStatus_->setPredictionGuidanceStatus(idlStatusFinished);
	  }
	  break;
	}

  case MSG_STATE_START:
	{
	  const char *tag = ToolKit::outfile("p3d");

	  int sn = strlen(tag);
	  char *fn = new char [sn+5];
	  Copy(fn, tag);

	  Copy(fn+sn, ".spd");
	  dtaStatus_->setSpeed3dFilename(fn);

	  Copy(fn+sn, ".dsy");
	  dtaStatus_->setDensity3dFilename(fn);

	  Copy(fn+sn, ".flw");
	  dtaStatus_->setFlow3dFilename(fn);

	  delete [] fn;

	  break;
	}

  case MSG_STATE_DONE:
	{
	  int i;
	  tms_ >> i;				// rolling horizon counter
	  dtaStatus_->setLastSheet3dIndex(i-1);
	  break;
	}
  case MSG_CURRENT_TIME:
	{
	  double now;
	  tms_ >> now;
	  idlUnitTime t = dynamitTime(long(now));
	  dtaStatus_->setSimulTime(t);	  
	  break;
	}

  default:
	{
	  cerr << "Warning:: Unknown TMS message <" << id
		   << "> received." << endl;
	  break;
	}
  }
   
  return flag;
}


// This function is called when sensor data is arrived from TMS.  This
// function should NOT take more than a second to run because it block
// the process.  The actual processing of the data and computational
// extensive job should be forked out as a separate process or as
// short functions called in simulation mainloop.

void MDI_Communicator::askDynamitToStartEstimation()
{
  static int counter = 0;
  counter ++;

  stage_ = STAGE_OD_ESTIMATION;
   
  theSimulationClock->currentTime(estStartTime_);
  theSimulationClock->masterTime(estEndTime_);

  // DynaMIT is estimating OD for the last interval

   
  idlTimeInterval timeInt = dynamitTimeInterval(estStartTime_, estEndTime_);
   
  if (ToolKit::verbose()) {
	cout << "DynaMIT is estimating OD for " 
		 << dtaTimeInterval(timeInt) << endl ;
  }
   
  if (CORBA::is_nil(dynamit())) {
	cerr << "No connection with DynaMIT." <<  endl ;
	theException->exit() ;
  }
   
                                    // This should be the sole responsibility
                                    // of DynaMIT. Included for demo only.

  dtaStatus_->setEstimationStatus(idlStatusRunning);
  dtaStatus_->setPredictionGuidanceStatus(idlStatusFinished);

  dynamit()->estimateOD(timeInt) ;

}

idlTimeInterval MDI_Communicator::dynamitTimeInterval(long start, long stop)
{
  idlTimeInterval timeInt;
  dtaAbsTime t1(0,0,0) ;
  t1 += start ;
  dtaAbsTime t2(0,0,0) ;
  t2 += stop ;
  timeInt.startOfInterval = t1.getSeconds() ;
  timeInt.endOfInterval = t2.getSeconds() ;
  return timeInt;
}

idlUnitTime MDI_Communicator::dynamitTime(long t)
{
  dtaAbsTime t1(0,0,0) ;
  t1 += t ;
  return t1.getIDL() ;
}

// This function is called when DynaMIT finished the estimation.  This
// function should NOT take more than a second to run because it block
// the process.  The actual processing of the data and computational
// extensive job should be forked out as a separate process or as
// short functions called in simulation mainloop.

void MDI_Communicator::askDynamitToStartPrediction()
{
                                    // This should be the sole responsibility
                                    // of DynaMIT. Included for demo only.

  dtaStatus_->setEstimationStatus(idlStatusFinished);
  dtaStatus_->setPredictionGuidanceStatus(idlStatusRunning);

  static int counter = 0;
  counter ++;

  stage_ = STAGE_OD_PREDICTION;

  theSimulationClock->currentTime(predStartTime_);
  // theSimulationClock->masterTime(predEndTime_);

  // DynaMIT is estimating OD for the last interval

  idlTimeInterval timeInt = dynamitTimeInterval(predStartTime_, predEndTime_);

  if (ToolKit::verbose()) {
	cout << "DynaMIT is predicting OD for " 
		 << dtaTimeInterval(timeInt) << endl ;
  }
   
  if (CORBA::is_nil(dynamit())) {
	cerr << "No connection with DynaMIT." <<  endl ;
	theException->exit() ;
  }
   
  TRY {
	dynamit()->writeODfile(timeInt) ;
  }
  CATCHANY {
	cerr << "DynaMIT: exception raised in OD prediction for "
		 << dtaTimeInterval(timeInt) << endl 
		 << IT_X << endl ;
  }
  ENDTRY ;

}


// Call this function when calculation is done.  The file pointed by
// filename contains the information TMS needs.

void MDI_Communicator::announceDynamitIsDone()
{
                                    // This should be the sole responsibility
                                    // of DynaMIT. Included for demo only.

  dtaStatus_->setEstimationStatus(idlStatusFinished);
  dtaStatus_->setPredictionGuidanceStatus(idlStatusFinished);

  if (ToolKit::verbose()) {
	cout << "DynaMIT is done" << endl ;
  }

  stage_ = 0;

  char* filename = NULL ;
  TRY {
	filename = strdup(dynamit()->odFileName()) ;
  }
  CATCHANY {
	cerr << "Communication problem when reading file name from DynaMIT" 
		 << endl;
	theException->exit() ;
  }
  ENDTRY ;

  tms_ << MSG_MDI_DONE << filename;
  tms_.flush(SEND_IMMEDIATELY);

  if (filename) free (filename);
}

void
MDI_Communicator::sendNextTimeToSimlab()
{
  if (!simlab_.isConnected()) return;

  SimulationClock *sc = theSimulationClock;
   
  simlab_ << MSG_END_CYCLE
		  << sc->currentTime()
		  << sc->lastStepCpuUsage();
  simlab_.flush(SEND_IMMEDIATELY);
}
