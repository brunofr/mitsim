//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MDI_CmdArgsParser.h
// DATE: Mon Feb 26 15:26:26 1996
//--------------------------------------------------------------------

#ifndef MDI_CMDARGSPARSER_HEADER
#define MDI_CMDARGSPARSER_HEADER

#include <Tools/CmdArgsParser.h>

class MDI_CmdArgsParser : public CmdArgsParser
{
   public:
      
      MDI_CmdArgsParser();
      virtual ~MDI_CmdArgsParser() { }
};

#endif
