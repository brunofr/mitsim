//-*-c++-*------------------------------------------------------------
// Main.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <iostream.h>

#include <Tools/ToolKit.h>

#include "MDI_Engine.h"
#include "MDI_Version.h"
#include "MDI_CmdArgsParser.h"

int main ( int argc, char **argv )
{
  ::Welcome(argv[0]);

  ToolKit::initialize();

  MDI_Engine engine;

  MDI_CmdArgsParser cp;
  cp.parse(&argc, argv);

  engine.run();

  engine.quit(STATE_DONE);

  return 0;
}
