//-*-c++-*------------------------------------------------------------
// MDI_Engine.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef MDI_ENGINE_HEADER
#define MDI_ENGINE_HEADER

#include <Tools/SimulationEngine.h>

class MDI_Engine : public SimulationEngine
{
      friend class MDI_CmdArgsParser;
      friend class MDI_Communicator;
      friend class MDI_FileManager;

   public:
      
      MDI_Engine();
      ~MDI_Engine() { }

      const char *ext();

      void init();

      // This function starts the simulation loops. It will NOT return
      // until simulation is done (or cancelled in graphical mode).
      // This function calls simulationLoop() iteratively or as a
      // recusive timeout callback.

      void run();
      void quit(int state);

      int loadSimulationFiles();
      int loadMasterFile();
      int simulationLoop();

      int receiveMessages(double sec = 0.0);	// virtual
	  void save(ostream &os = cout); // virtual
};

extern MDI_Engine * theEngine;

#endif
