//-*-c++-*------------------------------------------------------------
// MDI_Communicator.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef MDI_COMMUNICATOR_HEADER
#define MDI_COMMUNICATOR_HEADER

#include <IO/Communicator.h>
#include <idlMitsimDemand_idl.h>
#include <idlStatusManager_idl.h>

class MDI_Communicator : public Communicator
{
protected:

  IOService tms_;
  idlMitsimDemand_ptr dynamit_;
  idlStatusManager_ptr dtaStatus_;

public:

  MDI_Communicator(int sndtag);
  ~MDI_Communicator() { }
      
  inline int isTmsConnected() {
	return tms_.isConnected();
  }
      
  IOService& tms() { return tms_; }
  idlMitsimDemand_ptr& dynamit() { return dynamit_; }
  idlStatusManager_ptr& dtaStatus() { return dtaStatus_ ;}

  int receiveSimlabMessages(double sec = 0.0);
  int receiveTmsMessages(double sec = 0.0);

  // Functions defined in base class

  void makeFriends();
  int receiveMessages(double sec = 0.0);
  void sendNextTimeToSimlab();
  idlUnitTime dynamitTime(long t);
  idlTimeInterval dynamitTimeInterval(long start, long stop);
  void askDynamitToStartEstimation();
  void askDynamitToStartPrediction();
  void announceDynamitIsDone();

private:

  long int estStartTime_;	// start time for estimation
  long int estEndTime_;		// end time for estimation

  long int predStartTime_;	// start time for prediction
  long int predEndTime_;	// end time for prediction

  enum {
	STAGE_OD_ESTIMATION = 1,
	STAGE_OD_PREDICTION = 2
  };
  unsigned int stage_;		// current stage
};

extern MDI_Communicator* theCommunicator;

#endif
