//-*-c++-*------------------------------------------------------------
// MDI_Engine.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <string.h>
#include <new.h>

#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <Tools/SimulationClock.h>
#include <Tools/Math.h>

#include "MDI_Communicator.h"
#include "MDI_Engine.h"
#include "MDI_FileManager.h"
#include "MDI_Exception.h"

// Include CORBA header file
#include <CORBA.h>
#include <dtaException.h>
#include <bindUtilities.h>
#include <dtaAbsTime.h>

MDI_Engine * theEngine = NULL;

MDI_Engine::MDI_Engine()
  : SimulationEngine()
{
  theException = new MDI_Exception("mdi");

  theEngine = this;  
  if (!theSimulationClock) {
	theSimulationClock = new SimulationClock;
  }

  theCommunicator = new MDI_Communicator(MSG_TAG_MDI);

  theSimulationClock->init(28800.0, 32400.0, 1.0);
}


const char* MDI_Engine::ext()
{
  return ".mdi";
}


void
MDI_Engine::init()
{
  theSimulationClock->init();
  double s = theSimulationClock->currentTime();
}


int
MDI_Engine::loadMasterFile()
{
  if (SimulationEngine::loadMasterFile() != 0) {
	cerr << "Error:: Cannot find the master file <"
		 << master() << ">." << endl;
	quit(STATE_ERROR_QUIT);
	return 1;
  }

  theFileManager->readMasterFile();
  state_ = STATE_OK;

  return 0;
}


// This function is called every time a new master file is loaded.
// It return 0 if no error.

int
MDI_Engine::loadSimulationFiles()
{
  if (state_ == STATE_NOT_STARTED) {
	if (loadMasterFile() != 0) return 1;
  }
   
  init();

  theFileManager->openOutputFiles();

  // Now let the engine start

  start();

  // Make connection to other modules if they exist.

  if (!theCommunicator->isSimlabConnected()) return 0;

  theCommunicator->makeFriends();

  // Read necessary input files here

  // Tell DynaMIT server about the master file

  cout << "Calling dynamit->readParameters(" << master_ << ")" << endl;
   
  char *dtaPara = ::Copy(ToolKit::infile(theFileManager->paraFile_));
  char *dtaSensor = ::Copy(ToolKit::outfile(theFileManager->sensorFile_));
  char *dtaVrc = ::Copy(ToolKit::outfile(theFileManager->vrcFile_));

  if (CORBA::is_nil(theCommunicator->dynamit())) {
	cout << "No connection with DynaMIT." <<  endl ;
	theException->exit() ;
  }

  TRY {
	dtaAbsTime t(0,0,0) ;
	dtaUnitTime simTime = dtaUnitTime(theSimulationClock->startTime()) ;
	t += simTime ;
	theCommunicator->dynamit()->readParameters((idlString)dtaPara,
											   t.getSeconds(),
											   (idlString)dtaSensor,
											   (idlString)dtaVrc);
	if (dtaPara) free (dtaPara);
	if (dtaSensor) free (dtaSensor);
	if (dtaVrc) free (dtaVrc);
  }
  CATCHANY {
	cout << "DynaMIT: problem in reading parameter file " 
		 << dtaPara << endl ;
	cout << IT_X << endl ;
	theException->exit() ;
  }
  ENDTRY ;

  return 0;
}

// This procedure starts the simulation loops.

void MDI_Engine::run()
{
  if (canStart()) {
	loadMasterFile();
  }
 

  loadSimulationFiles();

  while (state_ >= 0 && (receiveMessages() > 0 ||
						 isWaiting() &&
						 receiveMessages(NO_MSG_WAITING_TIME) >= 0 ||
						 simulationLoop() >= 0)) {
	;
  }
}


void
MDI_Engine::quit(int state)
{
  theFileManager->closeOutputFiles();
  SimulationEngine::quit(state);
  theException->done(0);
}


int MDI_Engine::simulationLoop()
{
  SimulationClock *clock = theSimulationClock;

  // This function is called once every simulation cycle when there
  // is no IPC and X events awaiting.

  // Real simulation code goes here

  double now = clock->currentTime();

  if (now >= clock->masterTime()) {
	return (state_ = STATE_WAITING);
  }

  // Real simulation code end here

  // Advance the clock

  clock->advance();

  if (theCommunicator->isTmsConnected()) {
	if (clock->currentTime() > clock->masterTime()) {
	  clock->masterTime(clock->startTime()-1);
	  return (state_ = STATE_WAITING);
	}
  } else if(now > clock->stopTime()) {
	return (state_ = STATE_DONE);
  }

  return state_ = STATE_OK;
}


// Process the messages received from other processes

int
MDI_Engine::receiveMessages(double sec)
{
  if (!theCommunicator->isSimlabConnected()) return 0;
  else return  theCommunicator->receiveMessages(sec);
}

// Save the master file

void MDI_Engine::save(ostream &os)
{
  os << "/* " << endl
	 << " * MITSIM/DynaMIT Communicator master file" << endl
	 << " */" << endl << endl;

  os << "[Title] = \"" << theFileManager->title() << "\"" << endl
	 << endl;

  os << "[Default Parameter Directory]     = \""
	 << NoNull(ToolKit::paradir())
	 << "\"" << endl;
	 
  os << "[Input Directory]                 = \""
	 << NoNull(ToolKit::indir())
	 << "\"" << endl;

  os << "[Output Directory]                = \""
	 << NoNull(ToolKit::outdir())
	 << "\"" << endl;

  os << "[Working Directory]               = \""
	 << NoNull(ToolKit::workdir())
	 << "\"" << endl;

  os << endl;

  os << "[DTA Parameter File]              = \""
	 << theFileManager->paraFile_
	 << "\"" << endl;

  os << endl;

  os << "[Start Time]                      = "
	 << theSimulationClock->startStringTime() << endl;

  os << "[Stop Time]                       = "
	 << theSimulationClock->stopStringTime() << endl;

  os << "[Step Size]                       = "
	 << Fix(theSimulationClock->stepSize(), 0.1) << endl;

  os << endl;

  os << "[Output]   = 0x" << hex
	 << chosenOutput() << dec << endl;
}
