//-*-c++-*------------------------------------------------------------
// MDI_Exception.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// Because this program is a module in a distributed system and works
// together with other modules, it should call exit(code) or
// done(code) instead of the C function exit() to terminate the
// program.  This allows a message is sent to other modules for the
// killing event (they may want to kill themselves too).
//--------------------------------------------------------------------

#ifndef MDI_EXCEPTION_HEADER
#define MDI_EXCEPTION_HEADER

#include <IO/Exception.h>

class MDI_Exception : public Exception
{
   public:

      MDI_Exception(const char *name) : Exception(name) { }
      ~MDI_Exception() { }

      void exit(int code = 0);
      void done(int code = 0);
};

#endif
