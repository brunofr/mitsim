//-*-c++-*------------------------------------------------------------
// MDI_Version.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <iostream.h>
#include <new.h>

#include <Tools/ToolKit.h>

#include "MDI_Exception.h"
#include "MDI_Version.h"

char*  g_majorVersionNumber = "1";
char*  g_minorVersionNumber = "0 Beta";
char*  g_codeDate = "1997";

void
Welcome(char *exec)
{
   theExec = Copy(ToolKit::RealPath(exec));

   cout << "MITSIM/DynaMIT Communicator Release "
	<< g_majorVersionNumber << "." << g_minorVersionNumber << endl
	<< "Copyright (c) " << g_codeDate << endl
	<< "Massachusetts Institute of Technology" << endl
	<< "All Right Reserved" << endl << endl;

#ifndef FINAL_VERSION
   PrintVersionInfo();
#endif

   cout.flush();

   set_new_handler(::FreeRamException);
}
