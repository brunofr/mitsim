//-*-c++-*------------------------------------------------------------
// MDI_Exception.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------


#include <Tools/SimulationClock.h>

#include "MDI_Exception.h"
#include "MDI_Communicator.h"

void
MDI_Exception::exit(int code)
{
   if (theCommunicator->isSimlabConnected()) {
      theCommunicator->simlab() << MSG_QUIT;
      theCommunicator->simlab().flush(SEND_IMMEDIATELY);
   }
   IOService::exit();
   Exception::exit(code);
}

void
MDI_Exception::done(int code)
{
   if (theCommunicator->isSimlabConnected()) {
      theCommunicator->simlab() << MSG_DONE;
      theCommunicator->simlab().flush(SEND_IMMEDIATELY);
   }
   IOService::exit();
   Exception::done(code);
}
