//-*-c++-*------------------------------------------------------------
// MDI_FileManager.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <string.h>

#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <Tools/SimulationClock.h>

// This file is generated from VariableParser.y

#include <Tools/VariableParser.h>

#include <IO/MessageTags.h>
#include <IO/Exception.h>

#include "MDI_FileManager.h"
#include "MDI_Engine.h"
#include "MDI_Communicator.h"

MDI_FileManager* theFileManager = new MDI_FileManager;

MDI_FileManager::MDI_FileManager()
   : GenericSwitcher(),
     title_(NULL),
	 paraFile_(strdup("dtaparam.dat")),
	 sensorFile_(strdup("sensor.out")),
	 vrcFile_(strdup("vrc.out"))
{
}


void MDI_FileManager::set(char **ptr, const char *s)
{
   if (*ptr) {
	  free(*ptr);
   }
   if (s) {
	  *ptr = strdup(s);
   } else {
	  *ptr = NULL;
   }
}


// Read the variables from master file.

void
MDI_FileManager::readMasterFile()
{
  //  GenericSwitcher gs ;
  VariableParser vp( this );
  vp.parse(theEngine->master());
}


// Check if two token are equal.

int MDI_FileManager::isEqual(const char *s1, const char *s2)
{
   return IsEqual(s1, s2, 1);
}


// This function returns 0 if the variable is parsed or 1 if it is
// skipped (in case command line provides the value), or -1 if error
// occurs.

int MDI_FileManager::parseVariable(char ** varptr, GenericVariable &gv)
{
   *varptr = Copy(gv.string());
   return 0;
}


// All the variables provided in the master file are parsed in this
// function.

int
MDI_FileManager::parseVariable(GenericVariable &gv) 
{
   char * token = compact(gv.name());

   if (isEqual(token, "Title")) {
      title_ = Copy(gv.string());

   } else if (isEqual(token, "DefaultParameterDirectory")) {
	  ToolKit::paradir(gv.string());
   } else if (isEqual(token, "InputDirectory")) {
      ToolKit::indir(gv.string());
   } else if (isEqual(token, "OutputDirectory")) {
      ToolKit::outdir(gv.string());
   } else if (isEqual(token, "WorkingDirectory")) {
	  ToolKit::workdir(gv.string());

   } else if (isEqual(token, "DTAParameterFile")) {
	  paraFile_ = Copy(gv.string());

   } else if (isEqual(token, "SensorFile")) {
	  sensorFile_ = ::Copy(gv.string());
   } else if (isEqual(token, "VRCSensorFile")) {
	  vrcFile_ = ::Copy(gv.string());

   } else if (isEqual(token, "StartTime")) {
      theSimulationClock->startTime(gv.number());
   } else if (isEqual(token, "StopTime")) {
      theSimulationClock->stopTime(gv.number());
   } else if (isEqual(token, "StepSize")) {
      theSimulationClock->stepSize(gv.number());

   } else if (isEqual(token, "Output")) {
      theEngine->chooseOutput(gv.element());
 
   } else if (isEqual(token, "Timeout")) {

      // This variable is used for tuning the network communication.

      NO_MSG_WAITING_TIME = gv.element();

   } else if (ToolKit::verbose()) {

      return 1;
   }

   return 0;
}


void
MDI_FileManager::openOutputFiles()
{
   if (theEngine->chosenOutput()) {
	  cout << "Chosen output = " << theEngine->chosenOutput() << endl;
   }

   // Code for opening output files goes here
}

void
MDI_FileManager::closeOutputFiles()
{
   cout << "Output files are in directory <"
		<< ToolKit::outDir() << ">." << endl;

   // Code closing output files goes here
}
