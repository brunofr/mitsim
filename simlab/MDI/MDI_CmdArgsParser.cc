//-*-c++-*------------------------------------------------------------
// MDI_CmdArgsParser.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include "MDI_CmdArgsParser.h"
#include "MDI_Engine.h"

MDI_CmdArgsParser::MDI_CmdArgsParser() : CmdArgsParser()
{
   add(new Clo("-output", &theEngine->chosenOutput_, 0, "Output mask"));
}
