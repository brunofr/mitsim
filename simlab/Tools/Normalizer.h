//-*-c++-*------------------------------------------------------------
// NAME: Normalize a value based on predefined max and min value.
// AUTH: Qi Yang
// FILE: Normalizer.h
// DATE: Sat Feb 10 18:22:29 1996
//--------------------------------------------------------------------

#ifndef NORMALIZER_HEADER
#define NORMALIZER_HEADER

class Normalizer
{
   protected:
      
      double minimum_;
      double maximum_;
      double range_;
      short int reverse_;

      double lower_;		// lower bound
      double upper_;		// upper bound
      short int decimals_;	// number of decimal

   public:

      Normalizer() : minimum_(0.0), maximum_(1.0),
	reverse_(0), decimals_(0), range_(1.0) { }
      Normalizer(double minvalue, double maxvalue,
		 short int reverse = 0,
		 short int dec = 0);
      virtual ~Normalizer() { }

      virtual void set(double minvalue, double maxvalue,
		       short int reverse = 0,
		       short int dec = 0);

      short int decimals() { return decimals_; }

      inline double lower() { return lower_; }
      inline double upper() { return upper_; }

      inline double minimum() { return minimum_; }
      inline double maximum() { return maximum_; }
      inline short int isReverse() {
	 return reverse_;
      }

      virtual void minimum(double minvalue);
      virtual void maximum(double maxvalue);

      inline virtual short int isLogarithm() {
	 return 0;
      }
      virtual double normalize(double value);
};

class LogNormalizer : public Normalizer
{
   protected:

      double logminimum_;
      double logmaximum_;
      double logrange_;

   public:

      LogNormalizer() : Normalizer(),
	logminimum_(0.0), logmaximum_(0.0), logrange_(0.0) { }
      LogNormalizer(double minvalue, double maxvalue,
		    short int reverse = 0, short int dec = 0);
      virtual ~LogNormalizer() { }
      
      virtual void set(double minvalue, double maxvalue,
		       short int reverse = 0, short int dec = 0);

      virtual void minimum(double minvalue);
      virtual void maximum(double maxvalue);

      inline virtual short int isLogarithm() {
	 return 1;
      }
      virtual double normalize(double value);

   private:

      void logarithm();
};

#endif
