//-*-c++-*------------------------------------------------------------
// FILE: ToolKit.h -- Some basic utilities
// AUTH: Qi Yang
//--------------------------------------------------------------------

#ifndef TOOLKIT_HEADER
#define TOOLKIT_HEADER

#include <cstdlib>
#include <iostream>


#include <UTL/Misc.h>

const int MAX_MSG_BUFFER_SIZE = 512;

const char OPEN_TOKEN = '{';
const char CLOSE_TOKEN = '}';

extern const char endc;
extern const char null;
extern const char *indent;
extern const char *hextag;

class ToolKit
{
  friend class CmdArgsParser;

public:

  enum {
	VERBOSE_QUITE = 0,
	VERBOSE_NORMAL	= 1,
	VERBOSE_DEBUG = 2
  };
	  
  enum {
	NICE_DEDICATED = 0,
	NICE_PRIMARY = 1,
	NICE_SHARED = 2,
	NICE_AUTOMATIC = 3
  };

  ToolKit() { }
  virtual ~ToolKit() { }

protected:

  // Optional data files are searched in order from indir, then
  // paradir.

  static char *paradir_;	// parameter directory
  static char *indir_;		// input directory
  static char *outdir_;		// output directory
  static char *workdir_;	// working directory

  static int verbose_;		// 0=quiet 1=verbose 2+=debug
  static int nice_;			// 0=dedicated 1=primary 2=shared

public:

  static void initialize();

  // These are used by parsers

  static inline const char* paradir() { return paradir_; }
  static inline const char* indir() { return indir_; }
  static inline const char* outdir() { return outdir_; }
  static inline const char* workdir() { return workdir_; }
	  
  static void paradir(const char* d);
  static void indir(const char* d);
  static void outdir(const char* d);
  static void workdir(const char* wd);

  static inline const char* paraDir() {
	return expand_dir(paradir_);
  }
  static inline const char* inDir() {
	return expand_dir(indir_);
  }
  static inline const char* outDir() {
	return expand_dir(outdir_);
  }

  static const char* expand_dir(const char *);
  static const char* currentDir();

  static inline int verbose() { return verbose_; }
  static inline void verbose(int v) { verbose_ = v; }
  static inline int isQuiet() { return verbose_ == 0; }
  static inline int isVerbose() { return verbose_ == 1; }
  static inline int debug() { return (verbose_ < 2) ? 0 : verbose_; }
  static inline int nice() { return nice_; }
  static inline void nice(int i) { nice_ = i; }

  static const char* optionalInputFile(
									   const char *name,
									   const char *i = NULL,
									   const char *d = NULL);
  static const char* file(
						  const char *name,
						  const char *d = NULL);
  static const char* parafile(
							  const char *name,
							  const char *p = NULL);
  static const char* infile(
							const char *name,
							const char *i = NULL);
  static const char* outfile(
							 const char *name,
							 const char *o = NULL);
  static const char* wfile(
						   const char *name,
						   const char *w = NULL);
  static const char* mkdir(const char* d) ;
  static int makedirs() ;
  static int fileExists(const char *name) ;
  static int copyFile(
					  const char* src,
					  const char* des);

#ifdef SGI_CC
  static int getDirContents(
							const char * dir, 
							const char ** & files, 
							int & num_files);
#endif

  static const char *RealPath (const char *file_name);
  static const char* splitPath(const char *pathname);
  static const char* splitName(const char *pathname);

  static int isValidFilename(const char *name);
  static int isValidInputFilename(const char *name, 
								  const char* none = "None");
  static const char* errorMsgPrefix();

  // Return a file name which consists of prefix+pid+suffix.

  static char* filename(const char *prefix, const char *suffix);
  static char* filename(const char *prefix, int suffix);

  static inline char* msgBuffer() { return msgBuffer_; }
  static char* cleanMsgBuffer();

private:

  static char* filename_buffer;
  static char* msgBuffer_;
};

extern char *theExec;

extern void PrintVersionInfo(std::ostream &os = std::cout);
extern const char* theVersionInfo[2];

#endif
