//-*-c++-*------------------------------------------------------------
// NAME: Simulation State 
// AUTH: Qi Yang
// FILE: SimulationEngine.cc
// DATE: Mon Nov 20 12:29:38 1995
//--------------------------------------------------------------------

#include "SimulationEngine.h"

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <sys/types.h>
#include <ctime>

#include <IO/Exception.h>
#include <IO/Communicator.h>

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>
#include <Tools/Random.h>
#include <Tools/GenericVariable.h>
using namespace std;

SimulationEngine * theSimulationEngine = NULL; 

SimulationEngine::SimulationEngine()
   : master_(NULL),
     state_(STATE_NOT_STARTED),
     chosenOutput_(0),
	 mode_(0),
	 nBreakPoints_(0),
	 breakPoints_(NULL),
	 nextBreakPoint_(0)
{
   theSimulationEngine = this;
}


SimulationEngine::~SimulationEngine()
{
}


// Create a filename tagged with an optional iteration counter

char *
SimulationEngine::createFilename(const char *tag, int iter)
{
   const char *name;

   int id;
   if (theBaseCommunicator->isRunningAlone()) {
	  id = getpid();
   } else {
	  id = theBaseCommunicator->simlab().tid();
   }
   
   if (iter >= 0) {
	 name = Str("%x-%d.%s", id, iter, tag);
   } else {
	 name = Str("%x.%s", id, tag);
   }

   const char *realname = ToolKit::wfile(name);

   if (realname) {
	  return Copy(realname);
   } else {
	  cerr << "Error:: Failed resolving filename <"
		   << name << ">" << endl;
	  theException->exit(0);
   }
   return NULL;
}


// Set the name of the master file

void SimulationEngine::master(const char *name)
{
  static char *clone = NULL;
  if (master_ == name) return;
  else if (clone) free(clone);
  if (name) clone = strdup(name);
  else clone = 0;
  master_ = clone;
}


// Resolve the name of the master file. Expand the path if necessary.
// It returns 0 if not error, 1 if warning error or -1 if fatal error.

int
SimulationEngine::loadMasterFile()
{
   const char *realname;
   int warning;
   if (master_) {				// find real name
	  realname = ExpandEnvVars(master_);
	  if (! HasStrFromRight(realname, ext())) {
		realname = Str("%s%s", realname, ext());
	  }
	  warning = 1;				// show warning message if error
   } else {						// no specified, use default
	  realname = Str("master%s", ext());
	  warning = 0;
   }

   master(realname);

   if (ToolKit::fileExists(master_)) {
	  return 0;
   } else if (warning) {
	  return -1;
   } else {
	  return 1;
   }
}

int SimulationEngine::canStart()
{
   if (master_) return 1;
   const char *name = Str("master%s", ext());
   return ToolKit::fileExists(name);
}


int SimulationEngine::start()	// virtual
{
   cout << "Start the simulation engine ..." << endl;

   theSimulationClock->init();
   theSimulationClock->start();
   return 0;
}


int
SimulationEngine::isRunning()
{
   if (!theSimulationClock->isPaused() &&
       theSimulationClock->isStarted()) {
      return 1;
   } else {
      return 0;
   }
}


int
SimulationEngine::isWaiting()
{
   return theSimulationClock->isWaiting();
}


void
SimulationEngine::run()
{
   // In batch mode, the simulation is started automatically. When the
   // simulation is done.  We call quit() explicility.

   while (simulationLoop() >= 0);
}


// This is a virtual function and should be (1) overload in the
// derived class (e.g. batch mode), or (2) called by a recursive
// timeout callback.

int 
SimulationEngine::simulationLoop()
{
   if (theSimulationClock->currentTime() < theSimulationClock->stopTime()) {
      theSimulationClock->advance();
      return (state_ = STATE_OK);
   } else {
      return (state_ = STATE_DONE);
   }
}


// This function is called when application is done, closed or error
// occurs. It gives a last change to do the clean up tasks.

void
SimulationEngine::quit(int state)
{
   if (state_ == STATE_QUIT_CALLED) return;

   cout << "Simulation ";
   switch (state) {
      case STATE_ERROR_QUIT:
      {
		 cout << "cancelled because of error." << endl;
		 break;
      }
      case STATE_QUIT:
      {
		 cout << "cancelled." << endl;
		 break;
      }
      case STATE_DONE:
      {
		 cout << "completed." << endl;
		 break;
      }
      default:
      {
		 cout << "aborted abnormally." << endl;
		 break;
      }
   }
   
   state_ = STATE_QUIT_CALLED;
}


int
SimulationEngine::isTimeToBreak(double now, double epsilon)
{
   if (!breakPoints_ ||			// no break points set
	   nextBreakPoint_ >= nBreakPoints_) { // all break points passed
	  return 0;
   } else if (breakPoints_[nextBreakPoint_] <= now - epsilon) {
	  return 1;
   }
   return 0;
}

double
SimulationEngine::nextBreakPoint(double now)
{
   if (!breakPoints_) return 68400.0;
   else {
	  while (nextBreakPoint_ < nBreakPoints_ &&
			 breakPoints_[nextBreakPoint_] <= now) {
		 nextBreakPoint_ ++;
	  }
	  return breakPoints_[nextBreakPoint_];
   }
}


void
SimulationEngine::parseBreakPoints(int n, double *bps)
{
   if (breakPoints_) {
	  delete [] breakPoints_;
   }

   nBreakPoints_ = n;
   nextBreakPoint_ = 0;

   if (n < 1) {
	  breakPoints_ = NULL;
   } else {
	  breakPoints_ = new double [n + 1];
	  memcpy(breakPoints_, bps, sizeof(double) * n);
	  breakPoints_[n] = 68400.0;
   }
}

int
SimulationEngine::parseBreakPoints(GenericVariable &gv)
{
   if (breakPoints_) {
	  delete [] breakPoints_;
   }

   nBreakPoints_ = gv.nElements();
   nextBreakPoint_ = 0;

   if (nBreakPoints_ < 1) {
	  breakPoints_ = NULL;
   } else {
	  breakPoints_ = new double [nBreakPoints_ + 1];
	  for (int i = 0; i < nBreakPoints_; i ++) {
		 breakPoints_[i] = gv.element(i);
	  }
	  breakPoints_[nBreakPoints_] = 86400.0;
   }
   return 0;
}


void SimulationEngine::save(ostream &os)
{
   os << "SimulationEngine::save() called." << endl;
}
