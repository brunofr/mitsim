//-*-c++-*------------------------------------------------------------
// FILE: ToolKit.C -- Some basic utilities
// AUTH: Qi Yang
//--------------------------------------------------------------------

#include "ToolKit.h"
#include "Reader.h"

// #include <signal.h>

#include <cstdlib>
#include <sys/file.h>	
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/param.h>
#include <unistd.h>
#include <fcntl.h>
#include <cstdio>
#include <cstring>
#include <cctype>
#include <ctime>
#include <cerrno>

#include <iostream>
#include <iomanip>
using namespace std;

const char endc		= ' ';		// end of a data item
const char null		= '\0';		// end of string
const char *indent	= "  ";		// indent data line
const char *hextag	= "0x";

char* ToolKit::msgBuffer_ = new char[MAX_MSG_BUFFER_SIZE+1];
char* ToolKit::filename_buffer = new char[MAXPATHLEN+1];

// This is the default

char* ToolKit::paradir_ = strdup("../Common");
char* ToolKit::indir_   = strdup("./");
char* ToolKit::outdir_  = strdup("./Output");
char* ToolKit::workdir_ = strdup("/tmp");

int ToolKit::nice_     = ToolKit::NICE_AUTOMATIC;
int ToolKit::verbose_  = ToolKit::VERBOSE_NORMAL;

int theUserId		= 0;
char *theUserName	= NULL;
char *theFullUserName	= NULL;
char *theExec		= NULL;


void
ToolKit::initialize()
{
  umask(2);
}

const char *
ToolKit::RealPath (const char *file_name)
{
  const char *path = ::realpath((char*)file_name, (char*)filename_buffer);
  if (path) {					// Found by following path or symbolic links
	  return path;
  } else {						// Not found
	  return filename_buffer;
  }
}

int
ToolKit::isValidInputFilename(const char *name, const char* none)
{
  return (name && strlen(Trim((char *)name)) > 0 && CmpCi(name, none));
}

int
ToolKit::isValidFilename(const char *name)
{
  return (name && strlen(Trim((char *)name)) > 0);
}

const char *
ToolKit::errorMsgPrefix()
{
  const char* msg;
  if (UserName())
	  msg = Str("Oops, %s. %s is in trouble!\n",
				UserName(), theExec);
  else
	  msg = Str("Oops, %s is in trouble!\n",
				theExec);
  return msg;
}

void ToolKit::paradir(const char* d)
{
  ::Copy(&paradir_, d);
}

void ToolKit::indir(const char* d)
{
  ::Copy(&indir_, d);
}

void ToolKit::outdir(const char* d)
{
  ::Copy(&outdir_, d);
}

void ToolKit::workdir(const char* wd)
{
  ::Copy(&workdir_, wd);
}

const char* ToolKit::mkdir(const char* d)
{
  const char* path = ExpandEnvVars(d) ;
  if (!path) return NULL ;
  else if (fileExists(path)) return path ;
  else if (::mkdir(path, 0775)) return NULL ;
  else return path ;
}

int ToolKit::makedirs()
{
  int err = 0 ;
  if (outdir_) {
	if (!mkdir(outdir_)) {
	  cerr << "Failed creating output directory \""
		   << outdir_ << "\"." << endl;
	  err += 0x1 ;
	}
  }
  if (workdir_) {
	if (!mkdir(workdir_)) {
	  cerr << "Failed creating working directory \""
		   << workdir_ << "\"." << endl;
	  err += 0x2 ;
	}
  }
  return err ;
}

const char *
ToolKit::optionalInputFile(const char *name, const char *i,
						   const char *d)
{
  if (!name) return NULL;		// no file specified

  const char * filename;

  if (!i) i = indir_;
  if (!d) d = paradir_;

  if (fileExists(filename = infile(name, i))) {
	return filename;
  } else if (fileExists(filename = parafile(name, d))) {
	return filename;
  } else {
	cerr << endl << endl << errorMsgPrefix()
		 << "File <" << name << "> not found" << endl;
	if (i) {
	  cerr << "in <"
		   << ExpandEnvVars(i)
		   << ">" << endl;
	}
	if (i && d && Cmp(i, d)) {
	  cerr << "and <"
		   << ExpandEnvVars(d) << ">" << endl;
	}
	cerr << "on <" << HostName() << ">." << endl;

	return NULL;
  }
}

const char*
ToolKit::parafile(const char *name, const char *p)
{
  if (!name) return NULL;

  if (p) {
	if (*name != '/' && strlen(p) > 0) {
	  sprintf(filename_buffer, "%s/%s", p, name);
	} else {
	  strcpy(filename_buffer, name);
	}
  } else if (paradir_) {
	if (*name != '/' && strlen(paradir_) > 0) {
	  sprintf(filename_buffer, "%s/%s", paradir_, name);
	} else {
	  strcpy(filename_buffer, name);
	}
  } else{
	strcpy(filename_buffer, name);
  }
  return ExpandEnvVars(filename_buffer);
}

const char*
ToolKit::infile(const char *name, const char *i)
{
  if (!name) return NULL;

  if (i) {
	if (*name != '/' && strlen(i) > 0) {
	  sprintf(filename_buffer, "%s/%s", i, name);
	} else {
	  strcpy(filename_buffer, name);
	}
  } else if (indir_) {
	if (*name != '/' && strlen(indir_) > 0) {
	  sprintf(filename_buffer, "%s/%s", indir_, name);
	} else {
	  strcpy(filename_buffer, name);
	}
  } else {
	strcpy(filename_buffer, name);
  }
  return ExpandEnvVars(filename_buffer);
}

const char*
ToolKit::outfile(const char *name, const char *o)
{
  if (!name) return NULL;

  if (o) {
	if (*name != '/' && strlen(o) > 0) {
	  sprintf(filename_buffer, "%s/%s", o, name);
	} else {
	  strcpy(filename_buffer, name);
	}
  } else if (outdir_) {
	if (*name != '/' && strlen(outdir_) > 0) {
	  sprintf(filename_buffer, "%s/%s", outdir_, name);
	} else {
	  strcpy(filename_buffer, name);
	}
  } else {
	strcpy(filename_buffer, name);
  }
  return ExpandEnvVars(filename_buffer);
}

const char*
ToolKit::wfile(const char *name, const char *w)
{
  if (!name) return NULL;

  if (w) {
	if (*name != '/' && strlen(w) > 0) {
	  sprintf(filename_buffer, "%s/%s", w, name);
	} else {
	  strcpy(filename_buffer, name);
	}
  } else if (workdir_) {
	if (*name != '/' && strlen(workdir_) > 0) {
	  sprintf(filename_buffer, "%s/%s", workdir_, name);
	} else {
	  strcpy(filename_buffer, name);
	}
  } else {
	strcpy(filename_buffer, name);
  }
  return ExpandEnvVars(filename_buffer);
}

const char*
ToolKit::file(const char *name, const char *d)
{ 
  if (!name) return NULL;

  if (d) {
	if (*name != '/' && strlen(d) > 0) {
	  sprintf(filename_buffer, "%s/%s", d, name);
	} else {
	  strcpy(filename_buffer, name);
	}
  } else {
	strcpy(filename_buffer, name);
  }
  return ExpandEnvVars(filename_buffer);
}


// Returns the path part of the pathname or NULL if no path

const char*
ToolKit::splitPath(const char *pathname)
{
  static char buffer[MAXPATHLEN + 1];
  strcpy(buffer, RealPath(pathname));
  char *c = strrchr(buffer, '/');
  if (c) *c = '\0';
  else strcpy(buffer, ".");
  return buffer;
}

// Returns the file part of the pathname

const char*
ToolKit::splitName(const char *pathname)
{
  static char buffer[MAXPATHLEN + 1];
  strcpy(buffer, RealPath(pathname));
  char *c = strrchr(buffer, '/');
  if (c) return (c + 1);
  return buffer;
}


int
ToolKit::fileExists(const char* name)
{
  if (!name) return 0 ;
  struct stat st ;
  int err = stat(name, &st) ;
  if (err) return 0;

  if ((st.st_mode & S_IFMT) == S_IFREG) {
	return 1;					// regular file
//   } else if ((st.st_mode & S_IFMT) == S_IFCHR) {
// 	return 1;
  } else if (S_ISDIR(st.st_mode)) {
	return 2 ;
  } else {
	return 0;
  }
}


const char*
ToolKit::expand_dir(const char *dir)
{
  if (!dir ||
	  Cmp(dir, "") == 0 ||
	  Cmp(dir, ".") == 0) {
	return currentDir();
  } else {
	return ExpandEnvVars(dir);
  }
}


const char*
ToolKit::currentDir()
{
  static char *dir = NULL;
  if (dir) return dir;
  dir = Copy(ExpandEnvVars("$PWD"));
  return dir;
}


// Copy file src to file des

int ToolKit::copyFile(const char *src, const char *des)
{
  const char *cmd;
  cmd = Str("\\cp -f %s %s", src, des);
  return system(cmd);
}



// Return a file name which consists of prefix + pid + suffix.

char* ToolKit::filename(const char *prefix, const char *suffix)
{
  char *name;
  name = StrCopy("%s%x.%s", prefix, getpid(), suffix);
  return name;
}


char* ToolKit::filename(const char *prefix, int suffix)
{
  char *name;
  name = StrCopy("%s%x.%x", prefix, getpid(), suffix);
  return name;
}


char* ToolKit::cleanMsgBuffer()
{
  memset(msgBuffer_, 0, MAX_MSG_BUFFER_SIZE);
  return msgBuffer_;
}

// This function is used to log the user and version data

void PrintVersionInfo(ostream &os)
{
  os << "# User Info:    " << UserName()
	 << "@" << HostName()
	 << " (" << TimeTag() << ")" << endl;
  os << "# Version Info: " << theVersionInfo[0]
	 << " (" << theVersionInfo[1] << ")" << endl << endl;
}
