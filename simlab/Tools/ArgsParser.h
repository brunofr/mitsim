//-*-c++-*------------------------------------------------------------
// FILE: ArgsParser.h -- Parsing command line arguments
// DATE: Mon Oct 16 18:24:19 1995
//--------------------------------------------------------------------

#ifndef ARGSPARSER_HEADER
#define ARGSPARSER_HEADER

#include <iostream>

#include "ToolKit.h"

class ArgsParser;

class ArgsSwitch
{
      friend class ArgsParser;

   protected:

      const char *token_;	// token string
      const char *doc_;		// documentation string
      ArgsSwitch *next_;	// next switch
      short int	used_;		// number of times used
      short int required_;	// 1=required 0=not

   public:

      ArgsSwitch(const char *t, const char *d = NULL, short int r = 0)
		 : token_(t), doc_(d), required_(r),
		   next_(NULL), used_(0) { }
      virtual ~ArgsSwitch() { }
      virtual int isBool() const { return 0; }
      virtual int setValue(const char *) { return 0; } // should be overloaded
      virtual void markAsUsed(const char *);
      virtual void print(std::ostream &os = std::cout) {
		 os << indent << token_ << endc << doc_ << '\n';
      }
      inline short int isRequired() const { return required_; }
      inline void required(short int r) { required_ = r; }
};

class BoolArgsSwitch : public ArgsSwitch
{
   protected:

      int	*varptr_;	// which variable to parse into
      int	mask_;		// >0 to set a bit or 0 to overwrite

   public:

      BoolArgsSwitch(const char *t, int *pv, const char *d,
					 short int r = 0, int bitmask = 1)
		 : ArgsSwitch(t, d, r), varptr_(pv), mask_(bitmask) { }
      ~BoolArgsSwitch() { }
      int isBool() const { return 1; }
      int setValue(const char *);
      void print(std::ostream &os = std::cout) {
		 os << indent << "[+|-]" << token_;
		 os << " <" << *varptr_ << "> " << doc_ << '\n';
      }
};

class IntArgsSwitch : public ArgsSwitch
{
   protected:

      int	*varptr_;		// which variable to parse into

   public:

      IntArgsSwitch(const char *t, int *pv, const char *d,
					short int r)
		 : ArgsSwitch(t, d, r), varptr_(pv) { }
      ~IntArgsSwitch() { }
      int setValue(const char *);
      void print(std::ostream &os = std::cout) {
		 os << indent << token_;
		 os << "=<" << *varptr_ << "> " << doc_ << '\n';
      }
};

class UintArgsSwitch : public ArgsSwitch
{
   protected:

      unsigned int	*varptr_;		// which variable to parse into

   public:

      UintArgsSwitch(const char *t, unsigned int *pv, const char *d,
					 short int r)
		 : ArgsSwitch(t, d, r), varptr_(pv) { }
      ~UintArgsSwitch() { }
      int setValue(const char *);
      void print(std::ostream &os = std::cout) {
		 os << indent << token_;
		 os << "=<" << *varptr_ << "> " << doc_ << '\n';
      }
};

class ShortArgsSwitch : public ArgsSwitch
{
   protected:

      short int	*varptr_;	// which variable to parse into

   public:

      ShortArgsSwitch(const char *t, short int *pv, const char *d,
					  short int r)
		 : ArgsSwitch(t, d, r), varptr_(pv) { }
      ~ShortArgsSwitch() { }
      int setValue(const char *);
      void print(std::ostream &os = std::cout) {
		 os << indent << token_;
		 os << "=<" << *varptr_ << "> " << doc_ << '\n';
      }
};

class LongArgsSwitch : public ArgsSwitch
{
   protected:

      long int	*varptr_;	// which variable to parse into

   public:

      LongArgsSwitch(const char *t, long int *pv, const char *d,
					 short int r)
		 : ArgsSwitch(t, d, r), varptr_(pv) { }
      ~LongArgsSwitch() { }
      int setValue(const char *);
      void print(std::ostream &os = std::cout) {
		 os << indent << token_;
		 os << "=<" << *varptr_ << "> " << doc_ << '\n';
      }
};

class FloatArgsSwitch : public ArgsSwitch
{
   protected:

      float	*varptr_;		// which variable to parse into

   public:

      FloatArgsSwitch(const char *t, float *pv, const char *d,
					  short int r)
		 : ArgsSwitch(t, d, r), varptr_(pv) { }
      ~FloatArgsSwitch() { }
      int setValue(const char *);
      void print(std::ostream &os = std::cout) {
		 os << indent << token_;
		 os << "=<" << *varptr_ << "> " << doc_ << '\n';
      }
};

class DoubleArgsSwitch : public ArgsSwitch
{
   protected:

      double	*varptr_;	// which variable to parse into

   public:

      DoubleArgsSwitch(const char *t, double *pv, const char *d,
					   short int r)
		 : ArgsSwitch(t, d, r), varptr_(pv) { }
      ~DoubleArgsSwitch() { }
      int setValue(const char *);
      void print(std::ostream &os = std::cout) {
		 os << indent << token_;
		 os << "=<" << *varptr_ << "> " << doc_ << '\n';
      }
};

class StringArgsSwitch : public ArgsSwitch
{
   protected:

      char	**varptr_;		// which variable to parse into

   public:

      StringArgsSwitch(const char *t, char **pv, const char *d,
					   short int r)
		 : ArgsSwitch(t, d, r), varptr_(pv) { }
      ~StringArgsSwitch() { }
      int setValue(const char *);
      void print(std::ostream &os = std::cout) {
		 os << indent << token_ << "=<";
		 if (*varptr_)
			os << *varptr_ << "> " << doc_<< '\n';
		 else
			os << doc_ << ">" << '\n';
      }
};

class ConstStringArgsSwitch : public ArgsSwitch
{
   protected:

      const char **varptr_;	// which variable to parse into

   public:

      ConstStringArgsSwitch(const char *t, const char **pv,
							const char *d, short int r)
		 : ArgsSwitch(t, d, r), varptr_(pv) { }
      ~ConstStringArgsSwitch() { }
      int setValue(const char *);
      void print(std::ostream &os = std::cout) {
		 os << indent << token_ << "=<";
		 if (*varptr_)
			os << *varptr_ << "> " << doc_ << '\n';
		 else
			os << doc_ << ">" << '\n';
      }
};


class ArgsParser
{
   protected:

      ArgsSwitch*	head_;	// pointer to the first switch
      ArgsSwitch*	tail_;	// pointer to the last switch
      int help_;		// 1=help is switched on

   public:

      ArgsParser() : head_(NULL), tail_(NULL), help_(0) {
		 addSwitch("help", &help_, "Show usage");
		 addSwitch("h", &help_, "Same as <help>");
      }

      ~ArgsParser();

      ArgsSwitch* find(const char *name);

      void addSwitch(const char *t, int *pv, const char *d = NULL,
					 short int r = 0, int m = 1);
      void addOption(const char *t, int *pv, const char *d = NULL,
					 short int r = 0);
      void addOption(const char *t, unsigned int *pv, const char *d = NULL,
					 short int r = 0);
      void addOption(const char *t, short int *pv, const char *d = NULL,
					 short int r = 0);
      void addOption(const char *t, long int *pv, const char *d = NULL,
					 short int r = 0);
      void addOption(const char *t, float *pv, const char *d = NULL,
					 short int r = 0);
      void addOption(const char *t, double *pv, const char *d = NULL,
					 short int r = 0);
      void addOption(const char *t, char **pv, const char *d = NULL,
					 short int r = 0);
//       void addOption(const char *t, const char **pv, const char *d = NULL,
// 		     short int r = 0);
      void add(ArgsSwitch *);

      // Check all arguments. Failure of finding required arguments
      // results in exit

      void checkRequired();

      int parse(char **argv, int is_last);

      void parse(int argc, char *argv[]);

      void showUsage(std::ostream& os = std::cout) {
		 os << "Switches and options <default value> :\n";
		 print(os);
      }

      void showSettings(std::ostream& os = std::cout) {
		 os << "Current switches and options <value> :\n";
		 print(os);
      }
      void print(std::ostream& os = std::cout);
};

#endif
