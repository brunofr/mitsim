/*-*-c++-*------------------------------------------------------------
 *  VariableScanner.lex -- Variable lexical analyzer generator
 *--------------------------------------------------------------------
 */

%{
#include "VariableParser.h"
#define YY_BREAK
%}

%option yyclass="VariableFlexLexer"
%option noyywrap
%option yylineno

C	(#.*)|(%.*)|("//".*)
WS	[\t ,;\n]+

D	[0-9]
H       [0-9A-Fa-f]
E	[eE][-+]?{D}+
N	{D}+
X       {H}+
F1	{D}+\.{D}*({E})?
F2	{D}*\.{D}+({E})?
F3	{D}+({E})?

HEXTAG  ("0x")|("0X")|("x")|("X")

I	[-+]?({N}|({HEXTAG}{X}))
R	[-+]?({F1}|{F2}|{F3})

STR	["]([^"]*)["]
NAME	[[]([A-Za-z \t]*)[]]

HH	(([01]?[0-9])|([2][0-3]))
MM	([0-5]?[0-9])
SS	([0-5]?[0-9])
TIME	{HH}[:]{MM}[:]{SS}

PAIR	{N}-{N}

OB	{WS}?[{]{WS}?
CB	{WS}?[}]{WS}?

%%

"/*"	{	/* skip comments */
  int c;
  while ((c = yyinput()) != 0) {
    if (c == '*') {
      if ((c = yyinput()) == '/') break;
      else unput(c);
    }
  }
  break;
}

"=" 	{ return VariableParser::VariableEQUAL; }
{WS}	{ break; }
{C}	{ break; }

{OB}	{ return VariableParser::VariableOB; }
{CB}	{ return VariableParser::VariableCB; }
		            
{I}	{ return VariableParser::VariableINT; }
{R}	{ return VariableParser::VariableREAL; }
{TIME}  { return VariableParser::VariableTIME; }

{NAME}  { return VariableParser::VariableNAME; }
{STR}	{ return VariableParser::VariableSTRING; }

{PAIR}  { return VariableParser::VariablePAIR; }

%%
