//-*-c++-*------------------------------------------------------------
// FILE: ArgsParser.C -- command line argument parser
// DATE: Mon Oct 16 19:17:53 1995
//--------------------------------------------------------------------

#include <iostream>
#include <cstdlib>
#include <string>
#include <new>

#include <IO/Exception.h>
#include "ArgsParser.h"
using namespace std;

void
ArgsSwitch::markAsUsed(const char *msg)
{
   if (used_ ++) {
      cerr << "Warning:: " << msg << " <" << token_ << "> was used "
		   << used_ << " times.  Last one wins!" << endl; 
   }
}

int
BoolArgsSwitch::setValue(const char *value)
{
   if (*value == '+') {
      *varptr_ |= mask_;
   } else if (*value == '-') {
      *varptr_ &= ~mask_;
   } else {
      cerr << "Error:: Problem with field <" << value
		   << ">.  Switch <" << token_
		   << "> accepts no value." << endl;
      return 0;
   }
   markAsUsed("Switch");
   return 1;
}

int
IntArgsSwitch::setValue(const char *value)
{
   char *endptr;
   int i = (int)strtol(value, &endptr, 0);
   if (*endptr != '\0') {
      cerr << "Error:: Problem with field <" << endptr
		   << ">.  Option <" << token_
		   << "> expects an integer value." << endl;
      return 0;
   }
   *varptr_ = i;
   markAsUsed("Option");
   return 1;
}

int
UintArgsSwitch::setValue(const char *value)
{
   char *endptr;
   unsigned int i = (unsigned int)strtol(value, &endptr, 0);
   if (*endptr != '\0') {
      cerr << "Error:: Problem with field <" << endptr
		   << ">.  Option <" << token_
		   << "> expects an integer value." << endl;
      return 0;
   }
   *varptr_ = i;
   markAsUsed("Option");
   return 1;
}

int
ShortArgsSwitch::setValue(const char *value)
{
   char *endptr;
   short int s = (short int)strtol(value, &endptr, 0);
   if (*endptr != '\0') {
      cerr << "Error:: Problem with field <" << endptr
		   << ">.  Option <" << token_
		   << "> expects an short integer value." << endl;
      return 0;
   }
   *varptr_ = s;
   markAsUsed("Option");
   return 1;
}

int
LongArgsSwitch::setValue(const char *value)
{
   char *endptr;
   long int l = strtol(value, &endptr, 0);
   if (*endptr != '\0') {
      cerr << "Error:: Problem with field <" << endptr
		   << ">.  Option <" << token_
		   << "> expects an short integer value." << endl;
      return 0;
   }
   *varptr_ = l;
   markAsUsed("Option");
   return 1;
}

int
FloatArgsSwitch::setValue(const char *value)
{
   char *endptr;
   float f = strtod(value, &endptr);
   if (*endptr != '\0') {
      cerr << "Error:: Problem with field <" << endptr << ">.  "
		   << "Option <" << token_
		   << "> expects a float value." << endl;
      return 0;
   }
   *varptr_ = f;
   markAsUsed("Option");
   return 1;
}

int
DoubleArgsSwitch::setValue(const char *value)
{
   char *endptr;
   double d = strtod(value, &endptr);
   if (*endptr != '\0') {
      cerr << "Error:: Problem with field <" << endptr << ">.  "
		   << "Option <" << token_
		   << "> expects a double value." << endl;
      return 0;
   }
   *varptr_ = d;
   markAsUsed("Option");
   return 1;
}

int
StringArgsSwitch::setValue(const char *value)
{
   if (*value == '\0') {
      cerr << "Error:: Option <" << token_
		   << "> requires a string." << endl;
      return 0;
   }
   *varptr_ = Copy(value);
   markAsUsed("Option");
   return 1;
}

int
ConstStringArgsSwitch::setValue(const char *value)
{
   if (*value == '\0') {
      cerr << "Error:: Option <" << token_
		   << "> requires a string." << endl;
      return 0;
   }
   *varptr_ = value;
   markAsUsed("Option");
   return 1;
}

ArgsParser::~ArgsParser()
{
   while (head_) {
      tail_ = head_->next_;
      delete head_;
      head_ = tail_;
   }
}

void
ArgsParser::addSwitch(const char *t, int *pv, const char *d,
					  short int r, int m)
{
   ArgsSwitch* sw = new BoolArgsSwitch(t, pv, d, r, m);
   add(sw);
}

void 
ArgsParser::addOption(const char *t, int *pv, const char *d,
					  short int r)
{
   ArgsSwitch* sw = new IntArgsSwitch(t, pv, d, r);
   add(sw);
}

void 
ArgsParser::addOption(const char *t, unsigned int *pv, const char *d,
					  short int r)
{
   ArgsSwitch* sw = new UintArgsSwitch(t, pv, d, r);
   add(sw);
}

void 
ArgsParser::addOption(const char *t, short int *pv, const char *d,
					  short int r)
{
   ArgsSwitch* sw = new ShortArgsSwitch(t, pv, d, r);
   add(sw);
}

void 
ArgsParser::addOption(const char *t, long int *pv, const char *d,
					  short int r)
{
   ArgsSwitch* sw = new LongArgsSwitch(t, pv, d, r);
   add(sw);
}

void
ArgsParser::addOption(const char *t, float *pv, const char *d,
					  short int r)
{
   ArgsSwitch* sw = new FloatArgsSwitch(t, pv, d, r);
   add(sw);
}

void
ArgsParser::addOption(const char *t, double *pv, const char *d,
					  short int r)
{
   ArgsSwitch* sw = new DoubleArgsSwitch(t, pv, d, r);
   add(sw);
}

void
ArgsParser::addOption(const char *t, char **pv, const char *d,
					  short int r)
{
   ArgsSwitch* sw = new StringArgsSwitch(t, pv, d, r);
   add(sw);
}

void
ArgsParser::add(ArgsSwitch* sw)
{
   ArgsSwitch* old = find(sw->token_);

   if (old) {
      cerr << "Warning:: Cannot add <" << sw->token_ << "> "
		   << "because it already exists." << endl;
      delete sw;
   } else {
      if (tail_) tail_->next_ = sw;
      else head_ = sw;
      tail_ = sw;
   }
}


ArgsSwitch*
ArgsParser::find(const char *name)
{
   ArgsSwitch* sw = head_;
   while (sw) {
      if (strcmp(sw->token_, name) == 0) break;
      sw = sw->next_;
   }
   return sw;
}


// Returns number of args parsed in this turn.  The value is negative
// if it fails to parse an argument.

int
ArgsParser::parse(char **argv, int is_last)
{
   static char buffer[2] = " ";
   char *name, *value = buffer;
   int is_bool = 1, num = 1;
   if (*argv[0] == '-') {
      name = ++argv[0];
      *value = '-'; 
   } else if (*argv[0] == '+') {
      name = ++argv[0];
      *value = '+';
   } else if (name = strchr(*argv, '=')) {
      *name = '\0';
      value = ++name;
      name = *argv;
      is_bool = 0;
      if (*value == '\0' && !is_last) {
		 value = argv[1];
		 num = 2;
      }
   } else {
      cerr << "Error:: Parse error. "
		   << "No operator ('+' '-' or '=') found in <"
		   << argv[0] << ">" << endl;
      return -1;
   }
   ArgsSwitch* sw = find(name);
   if (!sw) {
      cerr << "Error:: Unknown option or switch <"
		   << argv[0] << ">" << endl;
      return -num;
   } else if (is_bool && !sw->isBool()) {
      cerr << "Error:: <" << sw->token_ 
		   << "> is not a switch" << endl;
      return -num;
   } else if (sw->setValue(value)) {
      return num;
   }
   return 0;
}


// Parse the command line arguments. The first argument is assumed to
// be the procedure name and skipped by the parser.

void 
ArgsParser::parse(int argc, char *argv[])
{
   char *s;
   int num = 1;
   for (int i = 1; i < argc; i += num) {
      s = Copy(argv[i]);
      num = parse(argv + i, (i >= argc - 1)); 
      if (num < 0) {
		 cout << "Warning:: <" << s << "> skipped" << endl;
		 num = -num;
      } else if (num == 0) {
		 num = 1;
      }
      delete [] s;
   }
   if (head_->used_ || head_->next_->used_) { // help
      showUsage();
      theException->done();
   }
   else checkRequired();
}


// Check all arguments. Failure of finding required arguments results
// in exit

void
ArgsParser::checkRequired()
{
   ArgsSwitch *arg;
   int num;
   for (num = 0, arg = head_; arg; arg = arg->next_) {
      if (arg->required_ && !arg->used_) num ++;
   }
   if (num) {
      cerr << "Following options/switches are required but not found." << endl;
      for (arg = head_; arg; arg = arg->next_) {
		 if (arg->required_ && !arg->used_) arg->print(cerr);
      }
      theException->exit(1);
   }
}


void
ArgsParser::print(ostream& os)
{
   ArgsSwitch* sw = head_;
   while (sw) {
      sw->print(os);
      sw = sw->next_;
   }
}

//#define TEST_ARGS_PARSER
#ifdef TEST_ARGS_PARSER
void main(int argc, char* argv[])
{
   int j = 0, i = 0;
   float f = 0.0;
   char *id = NULL, *od = NULL;

   ArgsParser ps;
   ps.addSwitch("debug", &j, "Debug mode can be switched on or off");
   ps.addOption("in", &id, "Input directory is a string");
   ps.addOption("out", &od, "Output directory is another string");
   ps.addOption("runs", &i, "Number of runs is an integer number");
   ps.addSwitch("debug", &j, "This can not be added");
   ps.addOption("speed", &f, "Speed is a float number");
   ps.showUsage();
   ps.parse(argc, argv);
   ps.showSettings();
}
#endif
