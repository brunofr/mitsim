//-*-c++-*------------------------------------------------------------
// NAME: Normalize a value based on predefined max and min value.
// AUTH: Qi Yang
// FILE: Normalizer.C
// DATE: Sat Feb 10 18:48:44 1996
//--------------------------------------------------------------------

#include <math.h>
#include "Normalizer.h"

Normalizer::Normalizer(double minvalue, double maxvalue,
		       short int reverse, short int dec)
   : minimum_(minvalue), maximum_(maxvalue),
     lower_(minvalue), upper_(maxvalue),
     reverse_(reverse), decimals_(dec)
{
   range_ = maximum_ - minimum_;
}

void
Normalizer::set(double minvalue, double maxvalue,
		     short int reverse, short int dec)
{
  lower_ = minimum_ = minvalue;
  upper_ = maximum_ = maxvalue;
  range_ = maximum_ - minimum_;
  reverse_ = reverse;
  decimals_ = dec;
}

double
Normalizer::normalize(double value)
{
   if (isReverse()) {
      if (value <= minimum_) {
	 return 1.0;
      } else if (value >= maximum_) {
	 return 0.0;
      } else {
	 return (maximum_ - value) / range_;
      }
   } else {
      if (value <= minimum_) {
	 return 0.0;
      } else if (value >= maximum_) {
	 return 1.0;
      } else {
	 return (value - minimum_) / range_;
      }
   }
}

void
Normalizer::minimum(double minvalue) {
   minimum_ = minvalue;
   range_ = maximum_ - minimum_;
}

void
Normalizer::maximum(double maxvalue) {
   maximum_ = maxvalue;
   range_ = maximum_ - minimum_;
}


LogNormalizer::LogNormalizer(double minvalue, double maxvalue,
			     short int reverse, short int dec)
   : Normalizer(minvalue, maxvalue, reverse, dec)
{
  logarithm();
}

void
LogNormalizer::logarithm()
{
   logminimum_ = (minimum_ < 1.0) ? 0.0 : log(minimum_);
   logmaximum_ = (maximum_ < 1.0) ? 0.0 : log(maximum_);
   logrange_ = logmaximum_ - logminimum_;
}

void
LogNormalizer::set(double minvalue, double maxvalue,
		short int reverse, short int dec)
{
  Normalizer::set(minvalue, maxvalue, reverse, dec);
  logarithm();
}

double
LogNormalizer::normalize(double value)
{
   double logvalue = (value < 1.0) ? 0.0 : log(value);

   if (isReverse()) {
      if (logvalue <= logminimum_) {
	 return 1.0;
      } else if (logvalue >= logmaximum_) {
	 return 0.0;
      } else {
	 return (logmaximum_ - logvalue) / logrange_;
      }
   } else {
      if (logvalue <= logminimum_) {
	 return 0.0;
      } else if (logvalue >= logmaximum_) {
	 return 1.0;
      } else {
	 return (logvalue - logminimum_) / logrange_;
      }
   }
}

void
LogNormalizer::minimum(double minvalue) {
   minimum_ = minvalue;
   range_ = maximum_ - minimum_;
   logminimum_ = (minvalue < 1) ? 0 : log(minvalue);
   logrange_ = logmaximum_ - logminimum_;
}

void
LogNormalizer::maximum(double maxvalue) {
   maximum_ = maxvalue;
   range_ = maximum_ - minimum_;
   logmaximum_ = (maxvalue < 1) ? 0 : log(maxvalue);
   logrange_ = logmaximum_ - logminimum_;
}
