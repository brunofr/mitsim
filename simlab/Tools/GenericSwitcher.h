//*-*-c++-*------------------------------------------------------------
// File: GenericSwitcher.h
//---------------------------------------------------------------------

#ifndef GENERIC_SWITCHER
#define GENERIC_SWITCHER

class GenericVariable;

class GenericSwitcher
{
   public:

      GenericSwitcher() : compactName_(0) { }
      virtual ~GenericSwitcher() { delete [] compactName_; }

      char * compact(char *);

      // This functions is called by the parser and should be
      // overloaded in the derived class. It returns a value 0 if gv
      // is accepted, and a non-zero value if it is rejected.  A
      // negative return value indicates a fatal error and will cause
      // the parse bail-out.

      virtual int parseVariable(GenericVariable &gv);

   private:

      char * compactName_;

};

#endif /* GENERIC_SWITCHER */
