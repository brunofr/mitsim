//
// VariableParser.y -- Variable parser generator
// By Qi Yang
//

%name   VariableParser
%define ERROR_BODY           DEF_ERROR_BODY
%define LEX_BODY             DEF_LEX_BODY

// These macros are expanded when creating the definition for named
// BisonParser class.  The functions defined as member here are
// designed for use in the grammar rules.

%define CONSTRUCTOR_PARAM  GenericSwitcher* pGS_
%define CONSTRUCTOR_INIT   : _pGS(pGS_)
%define MEMBERS            DEF_MEMBERS  \
   GenericSwitcher* _pGS ;              \
   GenericVariable _gv ;                \

%union {
  long        type_i ;
  double      type_f ;
  const char* type_s ;
}

%token <type_s> LEX_EQUAL
%token <type_s> LEX_COMMA
%token <type_s> LEX_COLON
%token <type_s> LEX_SEMICOLON
%token <type_s> LEX_OAB
%token <type_s> LEX_CAB
%token <type_s> LEX_OBB
%token <type_s> LEX_CBB
%token <type_s> LEX_OCB
%token <type_s> LEX_CCB
%token <type_i> LEX_INT
%token <type_f> LEX_REAL
%token <type_s> LEX_TIME
%token <type_s> LEX_PAIR
%token <type_s> LEX_TAG
%token <type_s> LEX_STRING
%token <type_s> LEX_DUP
%token <type_s> LEX_END

%header{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <IO/Exception.h>

#include <Tools/Scanner.h>
#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>
#include <Tools/GenericSwitcher.h>
#include <Tools/GenericVariable.h>
%}

//--------------------------------------------------------------------
// Symbols used varible grammer rules.
//--------------------------------------------------------------------

%type <type_s> variable_name string time
%type <type_s> inum fnum pair
             
%type <type_s> everything
%type <type_s> multi_variables variable 
%type <type_s> multi_values values value 

%start everything

%%

//--------------------------------------------------------------------
// Variable grammer rules.
//--------------------------------------------------------------------

everything: multi_variables
{
  if (ToolKit::verbose()) {
    cout << "Finished parsing <";
    cout << filename() << ">." << endl;
  }
}
;

multi_variables: variable
| multi_variables variable
;

variable: variable_name LEX_EQUAL values
{
  if (ToolKit::debug()) cout << _gv << endl;
  int err = _pGS->parseVariable(_gv);
  check(err);
}
;

variable_name: LEX_TAG
{
  _gv.clean() ;
  _gv.name(value("[]"));
}
;

values: LEX_OCB multi_values LEX_CCB
| value | LEX_OCB LEX_CCB
;

multi_values: value
| multi_values value
;


value: time | fnum | inum | string | pair
;

//--------------------------------------------------------------------
// Basic grammer rules.
//--------------------------------------------------------------------

inum: LEX_INT
{
  _gv.addElement((int)strtol(text(), NULL, 0));
}
;

fnum: LEX_REAL
{
  _gv.addElement(atof(value()));
}
;

time: LEX_TIME
{
  _gv.addElement(SimulationClock::convertTime(text()));
}
;

string: LEX_STRING
{
  _gv.addElement(value("\"\""));
}
;

pair: LEX_PAIR
{
  int o, d;
  if (sscanf(text(), "%d-%d", &o, &d) == 2) {
	_gv.addElement(o, d);
  } else {
	check(-1);
  }
}
;

%%

// Following pieces of code will be verbosely copied into the parser.


%header{
  // empty
%}
