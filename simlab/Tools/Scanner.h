//
// Scanner.h
//

#ifndef SCANNER_HEADER
#define SCANNER_HEADER

#include <iostream>
using namespace std;

#undef yyFlexLexer
#define yyFlexLexer SFlexLexer
#include <FlexLexer.h>

#include <UTL/bool.h>

// Tokens linking scanner to the parser

enum {
  LEX_EQUAL     = 258,
  LEX_COMMA     = 259,
  LEX_COLON     = 260,
  LEX_SEMICOLON = 261,
  LEX_OAB       = 262,
  LEX_CAB       = 263,
  LEX_OBB       = 264,
  LEX_CBB       = 265,
  LEX_OCB       = 266,
  LEX_CCB       = 267,
  LEX_INT       = 268,
  LEX_REAL      = 269,
  LEX_TIME      = 270,
  LEX_PAIR      = 271,
  LEX_TAG       = 272,
  LEX_STRING    = 273,
  LEX_DUP       = 274,
  LEX_END       = 275
} ;

class Scanner : public SFlexLexer
{
 public:

  Scanner(istream* yyin_ = 0, ostream* yyout_ = 0)
      : SFlexLexer(yyin_, yyout_), _szFilename(NULL), _pis(yyin_) { }

  virtual ~Scanner() {  close() ; }

  int yylex() { return SFlexLexer::yylex() ; }

  void check(int err_no, const char* szMsg_ = NULL) ;
  const char* text() ;
  const char* value(const char* delis_ = 0) ;
  const char* filename() { return _szFilename ; }
  bool open(const char* szFilename_) ;
  bool close() ;
  bool good() ;

 protected:

  // Redefine the virtual function in yyFlexLexer

  void LexerError(const char* szMsg_) { check(-1, szMsg_) ; }

 private:

  const char* _szFilename ;
  istream* _pis ;
} ;


// Skip C Style comment. Called in Scanner.lex after a "/*" is parsed.

#define SKIP_C_COMMENT                                        \
  int c ;                                                     \
  while ((c = yyinput()) != 0) {                              \
	  if (c == '*') {                                           \
	    if ((c = yyinput()) == '/') break ;                     \
	    else unput(c) ;                                         \
	  }                                                         \
  }                                                           \
  break ;                                                     \


// This macro should goes into the %define MEMBER macro in bison++
// parser class definition.  It defines a Scanner for the parser and a
// few mirror functions for accssing the Scanner's members in the
// yyparse().  It also defines member function:
// 
//   void parse(const char szFilename)
//
// which actually invokes the parsing.
//

#define DEF_MEMBERS                                           \
private:                                                      \
Scanner _scanner ;                                            \
public:                                                       \
Scanner& scanner() { return _scanner ; }                      \
int lineno() { return _scanner.lineno() ; }                   \
void check(int err_no, const char* szMsg_ = 0) {              \
  _scanner.check(err_no, szMsg_) ;                            \
}                                                             \
const char* text() { return _scanner.text() ; }               \
const char* value(const char* delis_ = 0) {                   \
  return _scanner.value(delis_) ;                             \
}                                                             \
const char* filename() { return _scanner.filename() ; }       \
void parse(const char* szFilename_) {                         \
  if (szFilename_ && _scanner.open(szFilename_)) {            \
    yyparse() ;                                               \
  } else if (_scanner.good()) {                               \
    yyparse() ;                                               \
  }                                                           \
}                                                             \

#define DEF_LEX_BODY   { return _scanner.yylex() ; }
#define DEF_ERROR_BODY { _scanner.check(-1, "Parser error") ; }

#endif
