/*
 * Scanner.lex -- A generic scanner that supports all my parsers.
 */

%{
#include "Scanner.h"            /* For tokens */
#define YY_BREAK
%}

/* 
 * This will name the generated scanner as Scanner rather than default
 * name yyFlexLaxer.  In addition, it allows use of Scanner class
 * members inside yylex().
 */
%option yyclass="SFlexLexer"
%option yylineno
%option noyywrap

/*
  %option debug
*/

C	(#.*)|(%.*)|("//".*)|(!.*)
W	[\t \n]+

D	[0-9]
H   [0-9A-Fa-f]
E	[eE][-+]?{D}+
N	{D}+
X   {H}+
F1	{D}+\.{D}*({E})?
F2	{D}*\.{D}+({E})?
F3	{D}+({E})?

HEX ("0x")|("0X")

I	[-+]?({N}|({HEX}{X}))
R	[-+]?({F1}|{F2}|{F3})

S	[\"](([^\"]|("\\\""))*)[\"]

TAG	[\[]([^\]]*)[\]]

HH	(([01]?[0-9])|([2][0-3]))
MM	([0-5]?[0-9])
SS	([0-5]?[0-9])
T	  ({HH}[:]{MM}[:]{SS})

P   ({N}-{N})|({N},{N})|({N}[/]{N})
DUP [\*xX]+

OAB	[\<]
CAB	[\>]
OBB	[\[]
CBB	[\]]
OCB	[\{]
CCB	[\}]

END ([<]"END"[>])|([[]"END"[]])

%%

"/*"  { SKIP_C_COMMENT ; }

"="   { return LEX_EQUAL ; }
","   { return LEX_COMMA ; }
":"   { return LEX_COLON ; }
";"   { return LEX_SEMICOLON ; }

{W}   { break ; }
{C}   { break ; }

{OAB} { return LEX_OAB ; }
{CAB} { return LEX_CAB ; }
{OBB} { return LEX_OBB ; }
{CBB} { return LEX_CBB ; }
{OCB} { return LEX_OCB ; }
{CCB} { return LEX_CCB ; }
		           
{I}   { return LEX_INT ; }
{R}	  { return LEX_REAL ; }
{T}   { return LEX_TIME ; }
               
{P}   { return LEX_PAIR; }
{DUP} { return LEX_DUP; }
               
{TAG} { return LEX_TAG ; }
{S}	  { return LEX_STRING ; }
{END} { return LEX_END ; }

%%
