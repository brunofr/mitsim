//-*-c++-*------------------------------------------------------------
// FILE: SimulationClock.C
// AUTH: Qi Yang
// DATE: Thu Oct 19 23:05:05 1995
//--------------------------------------------------------------------


#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cstring>

#include <IO/Exception.h>

#include "SimulationClock.h"
#include "ToolKit.h"
#include "Reader.h"
using namespace std;


time_t  SimulationClock::baseTime_ = 0;
struct tm * SimulationClock::localTime_ = NULL;


SimulationClock * theSimulationClock = NULL;

SimulationClock::SimulationClock()
  : actualTimeFactor_(0.0),
	desiredTimeFactor_(0.0),
	desiredClkTcksPerStep_(0L),
	started_(0),
	paused_(1),
	masterTime_(86400.0), startTime_(0.0), stopTime_(3600.0),

	startStringTime_(NULL),
	stopStringTime_(NULL),
    
	baseClkTcks_(0),
	currentClkTcks_(0),
	lastStepCpuClkTcks_(1),
	lastStepRealClkTcks_(1),
	lastStepSize_(1),

	latestStepRealClkTcks_(new clock_t[NUM_STEPS]),
	latestStepSimTime_(new double[NUM_STEPS]),
	nSteps_(0),
	front_(0),
	rear_(-1),
	sumClkTcks_(0),
	sumSimTime_(0.0)
{
  if (theSimulationClock) {
	cerr << "Error:: Clock created twice." << endl; 
	theException->exit(1);
  }
  theSimulationClock = this;
  init(0.0, 3600.0, 1.0);
}


// Setup the simulation times

int
SimulationClock::init (double start, double stop, double step)
{
  setBaseTime();

  if (start > stop || step < 0.0) {
	return 1;
  }

  lastStepSize_ = stepSize_ = step;
  currentTime_ = start;

  startTime(start);
  stopTime(stop);

  double n = stepSize_ * desiredTimeFactor_ * CLK_TCK;
  desiredClkTcksPerStep_ = (long)n;

  // These deals with the real time and cpu times

  currentClkTcks_ = baseClkTcks_ = times(&cputimes_); // in CLK_TCKs

  return 0;
}

void SimulationClock::startTime(double t)
{
  startTime_ = t ;
  currentTime_ = t ;
  simulationTime_ = stopTime_ - startTime_;
  ::Copy(&startStringTime_, convertTime(startTime_));
}

void SimulationClock::stopTime(double t)
{
  stopTime_ = t;
  simulationTime_ = stopTime_ - startTime_;
  ::Copy(&stopStringTime_, convertTime(stopTime_));
}

void SimulationClock::startTime(const char *st)
{
  startTime_ = convertTime(st);
  simulationTime_ = stopTime_ - startTime_;
  ::Copy(&startStringTime_, st);
}

void SimulationClock::stopTime(const char *st)
{
  stopTime_ = convertTime(st);
  simulationTime_ = stopTime_ - startTime_;
  ::Copy(&stopStringTime_, st);
}

void SimulationClock::read(Reader &is)
{
  double start, stop, step;
  start = is.gettime();
  stop = is.gettime();
  step = is.gettime();
  if (init(start, stop, step)) {
	is.reference();
  }
}

//returns base time of simulation (ie. seconds since last midnight)
time_t SimulationClock::getBaseTime()
{
  return baseTime_; //in seconds
}

void SimulationClock::setBaseTime()
{  
  // These deal with simulated time

  baseTime_ = time(NULL);	// in seconds
  localTime_ = localtime(&baseTime_);

  // Reference time of 00:00:00AM today

  baseTime_ -= currentClockTime();
}


// Return current clock time with 00:00:00AM TODAY as reference point
// (Not 00:00:00AM on January 1st, 1970 returned by function time())

long int
SimulationClock::currentClockTime()
{
  long seconds =
	localTime_->tm_sec +
	60L * localTime_->tm_min +
	3600L * localTime_->tm_hour;

  return seconds;
}


int
SimulationClock::advance(double step)
{
  static clock_t cycle_clks = 0;

  // This deals with the simulated time

  currentTime_ += step;
  long int sec10 = (long int)(10.0 * currentTime_ + 0.5);
  currentTime_ = sec10 / 10.0;
  lastStepSize_ = step;

  // These deal with the real time and cpu times

  clock_t prev_clktcks = currentClkTcks_;

  struct tms now;
  currentClkTcks_ = times(&now);
  time_t sec = time(NULL);		// in seconds
  localTime_ = localtime(&sec);


  // We assume that the flap point is prev_clktcks (actual flap
  // time could be larger than prev_clktcks).

  if (currentClkTcks_ < prev_clktcks) {
	cycle_clks += prev_clktcks - baseClkTcks_;
	baseClkTcks_ = 0;
  }
  currentClkTcks_ += cycle_clks; 

  lastStepRealClkTcks_ = currentClkTcks_ - prev_clktcks;
  actualTimeFactor_ = (double)lastStepRealClkTcks_ / (double)CLK_TCK / step;
  
  lastStepCpuClkTcks_ = (now.tms_utime + now.tms_stime) -
	(cputimes_.tms_utime + cputimes_.tms_stime);

  memcpy(&cputimes_, &now, sizeof(struct tms));

  // Record the real time of the last step and calculate the sum of
  // real time of the lastest several steps

  if (nSteps_ < NUM_STEPS) {
	nSteps_ ++ ;
	rear_ ++ ;
  } else {
	sumClkTcks_ -= latestStepRealClkTcks_[front_];
	sumSimTime_ -= latestStepSimTime_[front_];
	front_ = (front_ + 1) % NUM_STEPS;
	rear_ = (rear_ + 1) % NUM_STEPS;
  }
  sumClkTcks_ += lastStepCpuClkTcks_;
  sumSimTime_ += step;
  latestStepRealClkTcks_[rear_] = lastStepCpuClkTcks_;
  latestStepSimTime_[rear_] = step;

  return take_a_nice_nap();
}

double SimulationClock::lastStepCpuUsage()
{
  if (lastStepRealClkTcks_) {
	return (double)lastStepCpuClkTcks_ / (double)lastStepRealClkTcks_;
  } else {
	return 0.0;
  }
}

double SimulationClock::recentCpuUsage()
{
  if (sumSimTime_ > 0.01) {
	return (double)sumClkTcks_ / (double)CLK_TCK / sumSimTime_;
  } else {
	return 0;
  }
}


double
SimulationClock::cpuTimeSinceStart()
{
  tms tmp;
  times(&tmp);
  return (double)(tmp.tms_utime + tmp.tms_stime) / (double)CLK_TCK; 
}

double SimulationClock::expectedTimeToGo()
{
  double done = currentTime_ - startTime_;
  double togo = stopTime_ - currentTime_;
  if (done > 60.0) {
	return togo * timeSinceStart() / done;
  } else {
	return 0;
  }
}

// The returned value of this function is used to register
// callbacks.

long
SimulationClock::desiredCallbackTimeIntervals()
{
  const double regulator = 0.8;
  static long int num = 0;

  if (desiredClkTcksPerStep_ <= 0) num = 1;
  else {
     
	// The 10 is convert CLT_TCK to milliseconds

	double inc = num + 10.0 * regulator *
	  (desiredClkTcksPerStep_ - lastStepRealClkTcks_);
	num = (inc > 1.0) ? (long int)(inc) : (1);
  }

  return num;
}

int SimulationClock::take_a_nice_nap()
{
#ifdef __sgi

  // nap time (1/100 sec) in each iteration for 3 nice level

  static int scale[] = {
	0,						// dedicated (night and weekend)
	10 * CLK_TCK / 1000,		// primary
	20 * CLK_TCK / 1000		// shared
  };

  // Decide if run the program in nice mode

  static int nice = ToolKit::NICE_PRIMARY;
  static clock_t last = 0;

  // Only adjust the parameter every 10 seconds

  if (ToolKit::nice() >= ToolKit::NICE_AUTOMATIC) {	// automatic
	if (currentClkTcks_ > last + CLK_TCK * 10) {
	  if (localTime_->tm_hour < 9 ||	// 00:00 -- 09:00AM Everyday
		  localTime_->tm_hour > 19 ||	// 07:00 -- 12:00PM Everyday
		  localTime_->tm_wday == 0 ||	// Sunday
		  localTime_->tm_wday == 6) {	// Saturday
		nice = ToolKit::NICE_DEDICATED; // Set to dedicated mode
	  } else {
		nice = ToolKit::NICE_PRIMARY; // Set to primary mode
	  }
	}
  } else {
	nice = ToolKit::nice();
  }

  if (nice) {
	sginap(scale[nice]);
	return 1;
  }

#endif
  return 0;
}

// Force the simulation sleep for nSeconds.  Do not depend on this function.
// I may want delete it later.

void
SimulationClock::delay(double nSeconds)
{

  double num = nSeconds * CLK_TCK;

#ifdef __sgi
  sginap((long int)(num));
#else

  // This is really a stupid way to slow down the simulation.

  struct tms tmp;
  for (clock_t start = times(&tmp),
		 now = start,
		 stop = start + (long int)num;
	   now < stop;
	   now = times(&tmp))
	{
      ; // Loop until nSeconds expires
	}
#endif
}


// Force the program to sleep for a certain period of time if
// necessary to achieve desired speed factor.  This function should be
// called after advance() is called.  It returns 1 if it has imposed
// an artificial delay (simulation is run too faster) or 0 if no
// artificial delay has been imposed.

int
SimulationClock::wait()
{
  const double epsilon = 0.01;
  if (desiredTimeFactor_ < epsilon) return 0;
  double dt = waitTime();
  if (dt < epsilon) return 0;
  else delay(dt);
  return 1;
}

double
SimulationClock::waitTime()
{
  return desiredTimeFactor_ * lastStepSize_ - lastStepRealTime();
}


/*
 *---------------------------------------------------------------
 * Convert a string time into float format
 *---------------------------------------------------------------
 */

double 
SimulationClock::convertTime(const char* t)
{
  double ss = 0.0;
  char *s, buf[12];
  strncpy(buf, t, 12);
  if (s = strrchr(buf, ':')) {
	ss = atof(s+1);
	*s = '\0';
  } else {
	return atof(buf);
  }
  if (s = strrchr(buf, ':')) {
	ss += atof(s+1) * 60;
	*s = '\0';
  } else {
	return ss + atof(buf) * 60;
  }
  ss += atof(buf) * 3600;
  return ss;
}


/*
 *---------------------------------------------------------------
 * Convert a float time into string format
 *---------------------------------------------------------------
 */

const char*
SimulationClock::convertTimeLong(double t)
{
  static char buffer[12];
  int hh, mm;
  long int ss = (long int)t;
  t -= ss;
  hh = ss / 3600;
  ss %= 3600;
  mm = ss / 60;
  ss %= 60;
  t += ss;
  sprintf(buffer, "%2.2d:%2.2d:%04.1lf", hh, mm, t);
  return buffer;
}

const char*
SimulationClock::convertTime(double t)
{
  static char buffer[12];
  int hh, mm;
  long int ss = (long int)t;
  hh = ss / 3600;
  ss %= 3600;
  mm = ss / 60;
  ss %= 60;
  sprintf(buffer, "%2.2d:%2.2d:%2.2ld", hh, mm, ss);
  return buffer;
}
