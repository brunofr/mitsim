//-*-c++-*------------------------------------------------------------
// NAME: File Reader
// FILE: Reader.h
// DATE: Mon Oct 30 13:47:59 1995
//--------------------------------------------------------------------

#ifndef READER_HEADER
#define READER_HEADER

#include <iostream>
//using namespace std;


const int FILE_READER_LINESIZE = 1024;

class Reader
{
      friend Reader& ws(Reader&);

   public:

      Reader();			// will attach to named file later
      Reader(std::istream &);	// attach to an existing stream

      // Attach to named file in default input directory. Look for
      // alternative directory if the file does not exists in the
      // default input directory.

      Reader(const char* fname, const char* altpath = 0);
	  void reset_counters();
      void open(const char* name, const char* altpath = 0);
      void close();

      virtual ~Reader();
	
      int line_number() { return current_line;}

      // failure of read operator results in exit

      void set_fatal(int i = 1) { failure_is_fatal = i; }
		 
      long flags() { return (isp->flags()); }
      long flags(std::_Ios_Fmtflags f) { return (isp->flags(f)); }
		
      long setf(std::_Ios_Fmtflags setbits, std::_Ios_Fmtflags field) {
	 return (isp->setf(setbits, field));
      }
      long setf(std::_Ios_Fmtflags l) { return (isp->setf(l)); }
      void unsetf(std::_Ios_Fmtflags l) { isp->unsetf(l); }
		
      int width() { return (isp->width()); }
      int width(int w) { return (isp->width(w)); }
		
      int rdstate() { return (isp->rdstate()); }
      operator void * () {
	 if (isp->fail()) return (0); 
	 else return (this);
      }
		
      int operator!() { return (!(*isp));}
		
      int eof()	{ return (isp->eof()); }
      int fail() { return (isp->fail()); }
      int bad()	{ return (isp->bad()); }
      int good() { return (isp->good()); }
      void clear(std::_Ios_Iostate i =0) { isp->clear(i); }
	
      Reader& operator >> (Reader& (*f)(Reader&)) {
	 return ((*f)(*this));
      }
      Reader& operator>>(std::istream& (*f)(std::istream&)) {
	 *isp >> f; return (*this);
      }
      Reader& operator>>(std::ios& (*f)(std::ios&)) {
	 *isp >> f; return (*this);
      }
      Reader& operator >>(char*);
      Reader& operator >>(unsigned char* c) {
	 return (*this >> (char *) c);
      }
      Reader& operator >>(unsigned char& c);
      Reader& operator >>(char& c);
      Reader& operator >>(short&);
      Reader& operator >>(int&);
      Reader& operator >>(long&);
      Reader& operator >>(unsigned short&);
      Reader& operator >>(unsigned int&);
      Reader& operator >>(unsigned long&);
      Reader& operator >>(float&);
      Reader& operator >>(double&);

      Reader& get(char* , int lim, char delim='\n');
      Reader& get(unsigned char* b, int lim, char delim='\n') {
	 return (get((char *) b, lim, delim));
      }

      Reader& getline(char* b, int lim, char delim='\n');
      Reader& getline(unsigned char* b, int lim, char delim='\n') {
	 return (getline((char *) b, lim, delim));
      }

      Reader& get(unsigned char& c);
      Reader& get(char& c);

      int get();
      unsigned int gethex();
      double gettime();
	
      Reader& ignore(int n=1,int delim=EOF) {
	isp->ignore(n, delim);
	return (*this);
      }

      int gcount() { return last_count; }
      Reader& putback(char c) {
	 isp->putback(c);
	 if (c == '\n')	current_line--;
	 return (*this);
      }

      // skip white spaces and comments and returns next non-space
      // char

      int skip_comments();

      int eatwhite();
      int peek() { return skip_comments(); }

      int findToken(char);

      char* name() const { return name_; }

      // Error reporting routines

      void check_status(const char *);
      void error_quit(const char *msg = 0, int line_info = 1);
      void reference();

   private:

      char *name_;		// file name if available
      int  isp_was_allocated;
      int  last_count;
      int  current_line;
      int  failure_is_fatal;
      std::istream* isp;
      char beginning_field;
};

#endif
