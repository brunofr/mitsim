//-*-c++-*------------------------------------------------------------
// FILE: SimulationClock.h
// DATE: Thu Oct 19 23:04:06 1995
//--------------------------------------------------------------------

#ifndef SIMULATIONCLOCK_HEADER
#define SIMULATIONCLOCK_HEADER

#include <ctime>
#include <unistd.h>
#include <sys/types.h>
#include <sys/times.h>
#ifndef CLK_TCK
#define CLK_TCK CLOCKS_PER_SEC
#endif


const long ONE_DAY = 86400;		// in second

// For averaging actual speed factor, we keeps track of simulated and
// actual time for lastest several steps.  The NUM_STEPS defined below
// must be no less than 2.

#define NUM_STEPS	5

class Reader;

class SimulationClock
{
   protected:
	
      static time_t baseTime_;	// time of 00:00:00AM today 
	  static struct tm *localTime_;	// current local clock time

      double startTime_;	// simulation start time 
      double stopTime_;		// simulation stop time 
      double simulationTime_;	// total simultion time
      double stepSize_;		// simulation step size 
      double currentTime_;	// current simulation clock time 
      double masterTime_;	// current time of master clock

      short int started_;	// 1 if already started
      short int paused_;	// 1 if paused

      char* startStringTime_;
      char* stopStringTime_;

      // structure tms is defined in <sys/times.h> and filled by
      // calling times(&cputms_)

      struct tms cputimes_; // time-accounting information

      clock_t baseClkTcks_;	// returned by first call of times();

      // These three are filled in advance(double), which is assumed
      // to be called in every simulation cycle once and only once.

      clock_t currentClkTcks_; // returns by last call of times();
      clock_t lastStepCpuClkTcks_; // in CLK_TCKs
      clock_t lastStepRealClkTcks_; // in CLK_TCKs

      double lastStepSize_; // last step size

      // These variables implements a circular array for calculate
      // average real time per step

      clock_t *latestStepRealClkTcks_;
      double *latestStepSimTime_;
      short int nSteps_;
      short int front_;
      short int rear_;
      clock_t sumClkTcks_;
      double sumSimTime_;

   private:

      double actualTimeFactor_; // running/real time 
      long int desiredClkTcksPerStep_; // num of clk_tcks
      double desiredTimeFactor_; // running/real time 

   public:

      SimulationClock();
      SimulationClock(double start, double stop, double step = 1.0) {
		 init(start, stop, step);
      }

      virtual ~SimulationClock() { }

      // Conversion of time strings

      static double convertTime(const char *);
      static const char* convertTimeLong(double t);
      static const char* convertTime(double t);
	  static const char* convert(double t) {
		 return convertTime(t);
	  }

      virtual void init() {
		 init(startTime_, stopTime_, stepSize_);
      }

      virtual void read(Reader &);
      
      virtual int init(double startTime, double stopTime,
					   double stepTime = 1.0);

      void stepSize(double step) {
		 stepSize_ = step;
		 double n = stepSize_ * desiredTimeFactor_ * CLK_TCK;
		 desiredClkTcksPerStep_ = (long)n;
      }

      inline double masterTime() { return masterTime_; }
      inline void masterTime(double m) { masterTime_ = m; }
      inline int isWaiting() {
		 return (currentTime_ >= masterTime_);
      }

      inline const char* startStringTime() {
		 return startStringTime_;
      }

      inline const char* stopStringTime() {
		 return stopStringTime_;
      }

      inline const char *currentStringTime() {
		 return convertTimeLong(currentTime());
      }

	  void startTime(double t);
	  void stopTime(double t);
	  void startTime(const char *t);
	  void stopTime(const char *t);

      inline double startTime() { return startTime_; }
      inline double stopTime() { return stopTime_; }
      inline double stepSize() { return stepSize_; }

      inline double desiredRealTimePerStep() {
		 return (stepSize_ * desiredTimeFactor_);
      }
      inline long desiredClkTcksPerStep() {
		 return desiredClkTcksPerStep_;
      }
	
	  // returns 1 if it took a nice nap

      inline int advance() { return advance(stepSize_); }
      int advance(double t);

      inline void desiredTimeFactor(double x) { 
		 double n = stepSize_ * x * CLK_TCK;
		 desiredTimeFactor_ = x;
		 desiredClkTcksPerStep_ = (long)n;
      }
      inline double desiredTimeFactor() {
		 return desiredTimeFactor_; 
      }
      inline double actualTimeFactor() {
		 return actualTimeFactor_;
      }
      inline double currentTime() { 
		 return currentTime_; 
      }
      inline void currentTime(double x) {
		 currentTime_ = x;
      }
  
      //Daniel Florian, 02-26-2004
      time_t getBaseTime();
  
      double cpuTimeSinceStart();

      // These two functions return time information for last interval
      // and are measured in ClkTcks.

      inline clock_t lastStepCpuClkTcks() {
		 return lastStepCpuClkTcks_;
      }
      inline clock_t lastStepRealClkTcks() {
		 return lastStepRealClkTcks_;
      }

      // These two returns time in seconds

      inline double lastStepCpuTime() {
		 return (double)lastStepCpuClkTcks_ / (double)CLK_TCK;
      }

      inline double lastStepRealTime() {
		 return (double)lastStepRealClkTcks_ / (double)CLK_TCK;
      }

	  double lastStepCpuUsage();
	  double recentCpuUsage();

      // The returned value of this function is used to register
      // callbacks.

      virtual long desiredCallbackTimeIntervals();

      // Force the program to sleep for nSeconds

      virtual void delay(double nSeconds);

      // Force the program to sleep for a certain period of time if
      // necessary to achieve desired speed factor.  This function
      // should be called after advance() is called.  It returns 1 if
      // it has imposed an artificial delay (simulation is run too
      // faster) or 0 if no artificial delay has been imposed.
	 
      virtual int wait();

      virtual double waitTime();

      inline void start() {
		 started_ = 1;
		 paused_ = 0;
      }
      inline short int isStarted() {
		 return started_;
      }
      inline void pause() {
		 paused_ = 1;
      }
      inline void resume() {
		 paused_ = 0;
      }
      inline short int isPaused() {
		 return paused_;
      }

      // Returns time in seconds of the SIMULATED time

      inline double timeSimulated() {
		 return (currentTime_ - startTime_);
      }

      inline double timeToBeSimulated() {
		 return (stopTime_ - currentTime_);
      }

      // Return time in seconds of the REAL TIME
	 
      inline double timeSinceStart() {
		 return (double)(currentClkTcks_ - baseClkTcks_) / (double)CLK_TCK;
      }

	  double expectedTimeToGo();

      // Return current clock time with 00:00:00AM TODAY as reference
      // point (Not 00:00:00AM on January 1st, 1970 returned by
      // function time())

      long int currentClockTime();

      inline double expectedClockTimeToStop() {
		 return (currentClockTime() + expectedTimeToGo());
      }

      inline const char* clockTimeSinceStart() {
		 return convertTime(timeSinceStart());
      }

      inline const char* clockTimeNow() {
		 return convertTime(currentClockTime());
      }

      inline const char* clockTimeStop() {
		 return convertTime(currentClockTime() + expectedTimeToGo());
      }

      inline double percentLeft() {
		 return 100.0 * (stopTime_ - currentTime_) / simulationTime_;
      }

      inline double percentDone() {
		 return 100.0 * (currentTime_ - startTime_) / simulationTime_;
      }

      inline double progress() {
		 return (currentTime_ - startTime_) / simulationTime_;
      }

	  int take_a_nice_nap();

   private:

      void setBaseTime();
};

extern SimulationClock * theSimulationClock;

#endif
