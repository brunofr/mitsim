//-*-c++-*------------------------------------------------------------
// FILE: Scaler.h
// AUTH: Qi Yang
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#ifndef SCALER_HEADER
#define SCALER_HEADER

class GenericVariable;

class Scaler
{
   private:
      
      int nIntervals_;
      double start_;
      double stop_;
      double step_;

   public:

      Scaler() : nIntervals_(0), start_(0.), stop_(0.), step_(0.) { }
      Scaler(int num, double start, double step) {
	init(num, start, step);
      }
      ~Scaler() { }

      int init(int num, double start, double step);
      int init(GenericVariable &gv);
      int init(GenericVariable &gv, double scaling);

      inline int nIntervals() { return nIntervals_; }
      inline double start() { return start_; }
      inline double stop() { return stop_; }
      inline double step() { return step_; }

      int index(double value);

      double startValue(int index);
      double endValue(int index);
      double medianValue(int index);
      double value(double);
};

#endif
