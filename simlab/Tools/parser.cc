//
// parser.cc -- Testing the Scanner and generic variable parser
//

#include <iostream.h>

#include "GenericVariable.h"
#include "VariableParser.h"

int main(int argc, char* argv[])
{
  if (argc < 2) {
	cerr << "Usage: " << argv[0] << " file1 file2 ..." << endl ;
  }
  GenericSwitcher gs ;
  VariableParser vp(&gs) ;
  for (int i = 1; i < argc; i ++) {
	cout << "Parsing " << argv[i] << endl ;
	vp.parse(argv[i]) ;
  }
}
