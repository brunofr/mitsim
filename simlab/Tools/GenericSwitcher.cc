//-*-c++-*------------------------------------------------------------
// File: GenericSwitcher.C
//--------------------------------------------------------------------

#include <iostream>
#include <cstring>
#include <cstdlib>
#include <cctype>

#include "GenericSwitcher.h"
#include "GenericVariable.h"
using namespace std;

// Return a compact name by remove the space between words.

char *
GenericSwitcher::compact(char *name)
{
   if (compactName_) delete [] compactName_;
   compactName_ = new char [strlen(name) + 1];
   int i = 0;
   while (*name != '\0') {
      if (!isspace(*name)) {
	 compactName_[i] = *name;
	 i ++;
      }
      name ++;
   }
   compactName_[i] = '\0';
   return compactName_;
}


// This function is called by the parser and should be overload in the
// derived class.  It returns a value 0 if gv is accepted, and a
// non-zero value if it is rejected.  A negative return value
// indicates a fatal error and will cause the parse bail-out.

int
GenericSwitcher::parseVariable(GenericVariable &gv)
{
   cout << gv << endl;
   return 0;
}

