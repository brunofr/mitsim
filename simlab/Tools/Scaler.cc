//-*-c++-*------------------------------------------------------------
// FILE: Scaler.cc
// AUTH: Qi Yang
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#include <iostream>

#include "Math.h"
#include "Scaler.h"
#include "GenericVariable.h"
using namespace std;

// --------------------------------------------------------------------
// Requires: num >= 2 and step >= 0
//
// e.g. (a scaler with 5 intervals, indices will range from 0 to 4)
//
//   0    |     1    |     2    |    3     |    4
//        +----------+----------+----------+
//        ^                                ^
//      Start        |<- Step ->|         Stop
//
// --------------------------------------------------------------------

int
Scaler::init(int num, double start, double step)
{
   if (num < 2) {
      cerr << "Error:: a scaler must have at lease 2 intervals." << endl;
      return -1;
   }
   start_ = start;
   step_ = step;
   stop_ = start + step * (num - 2);
   nIntervals_ = num;
   return 0;
}

int
Scaler::init(GenericVariable &gv)
{
   return init((int)(gv.number(0)), gv.number(1), gv.number(2)); 
}

int
Scaler::init(GenericVariable &gv, double scaling)
{
   return init((int)(gv.number(0)), gv.number(1) * scaling,
	       gv.number(2) * scaling); 
}


/*
 *---------------------------------------------------------------
 * Return an index for the given value
 *     0                value <  start
 *     1       start <= value <  start+step
 *     2  start+step <= value <  start+2step
 *     3 start+2step <= value <  start+3step
 *   ...         ...     ...        ...
 * num-1                value >= start+(num-2)*step
 *---------------------------------------------------------------
 */

int
Scaler::index(double value)
{
   if (value < start_)
      return 0;
   else if (value >= stop_)
      return (nIntervals_ - 1);
   else
      return ((int)((value - start_) / step_ + 1));
}


// Beginning value of an interval ( 0 < index < nIntervals )

double
Scaler::startValue(int index)
{
   if (index <= 0) return (-DBL_INF);
   else return (start_ + (index - 1) * step_);
}


// End value of an interval ( 0 <= index < nIntervals-1 )

double
Scaler::endValue(int index)
{
   if (index >= nIntervals_ - 1) return DBL_INF;
   else return (start_ + index * step_);
}


// Median value of an interval ( 0 < index < nIntervals-1 )

double
Scaler::medianValue(int index)
{
   if (index <= 0)
      return start_;
   else if (index >= nIntervals_ - 1)
      return stop_;
   else
      return (start_ + (index + 0.5) * step_);
}


// Value in an interval ( 1.0 <= pos <=  nIntervals )

double
Scaler::value(double pos)
{
   return (start_ + pos * step_);
}
