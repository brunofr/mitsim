#ifndef YY_VariableParser_h_included
#define YY_VariableParser_h_included

#line 1 "/usr/lib/bison.h"
/* before anything */
#ifdef c_plusplus
#ifndef __cplusplus
#define __cplusplus
#endif
#endif
#ifdef __cplusplus
#ifndef YY_USE_CLASS
#define YY_USE_CLASS
#endif
#else
#endif
#include <stdio.h>

/* #line 14 "/usr/lib/bison.h" */
#line 21 "VariableParser.h"
#define YY_VariableParser_ERROR_BODY            DEF_ERROR_BODY
#define YY_VariableParser_LEX_BODY              DEF_LEX_BODY
#define YY_VariableParser_CONSTRUCTOR_PARAM   GenericSwitcher* pGS_
#define YY_VariableParser_CONSTRUCTOR_INIT    : _pGS(pGS_)
#define YY_VariableParser_MEMBERS             DEF_MEMBERS  \
   GenericSwitcher* _pGS ;              \
   GenericVariable _gv ;                \


#line 20 "VariableParser.y"
typedef union {
  long        type_i ;
  double      type_f ;
  const char* type_s ;
} yy_VariableParser_stype;
#define YY_VariableParser_STYPE yy_VariableParser_stype
#line 45 "VariableParser.y"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <IO/Exception.h>

#include <Tools/Scanner.h>
#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>
#include <Tools/GenericSwitcher.h>
#include <Tools/GenericVariable.h>

#line 14 "/usr/lib/bison.h"
 /* %{ and %header{ and %union, during decl */
#ifndef YY_VariableParser_COMPATIBILITY
#ifndef YY_USE_CLASS
#define  YY_VariableParser_COMPATIBILITY 1
#else
#define  YY_VariableParser_COMPATIBILITY 0
#endif
#endif

#if YY_VariableParser_COMPATIBILITY != 0
/* backward compatibility */
#ifdef YYLTYPE
#ifndef YY_VariableParser_LTYPE
#define YY_VariableParser_LTYPE YYLTYPE
/* WARNING obsolete !!! user defined YYLTYPE not reported into generated header */
/* use %define LTYPE */
#endif
#endif
#ifdef YYSTYPE
#ifndef YY_VariableParser_STYPE 
#define YY_VariableParser_STYPE YYSTYPE
/* WARNING obsolete !!! user defined YYSTYPE not reported into generated header */
/* use %define STYPE */
#endif
#endif
#ifdef YYDEBUG
#ifndef YY_VariableParser_DEBUG
#define  YY_VariableParser_DEBUG YYDEBUG
/* WARNING obsolete !!! user defined YYDEBUG not reported into generated header */
/* use %define DEBUG */
#endif
#endif
#ifdef YY_VariableParser_STYPE
#ifndef yystype
#define yystype YY_VariableParser_STYPE
#endif
#endif
/* use goto to be compatible */
#ifndef YY_VariableParser_USE_GOTO
#define YY_VariableParser_USE_GOTO 1
#endif
#endif

/* use no goto to be clean in C++ */
#ifndef YY_VariableParser_USE_GOTO
#define YY_VariableParser_USE_GOTO 0
#endif

#ifndef YY_VariableParser_PURE

/* #line 63 "/usr/lib/bison.h" */
#line 104 "VariableParser.h"

#line 63 "/usr/lib/bison.h"
/* YY_VariableParser_PURE */
#endif

/* #line 65 "/usr/lib/bison.h" */
#line 111 "VariableParser.h"

#line 65 "/usr/lib/bison.h"
/* prefix */
#ifndef YY_VariableParser_DEBUG

/* #line 67 "/usr/lib/bison.h" */
#line 118 "VariableParser.h"

#line 67 "/usr/lib/bison.h"
/* YY_VariableParser_DEBUG */
#endif
#ifndef YY_VariableParser_LSP_NEEDED

/* #line 70 "/usr/lib/bison.h" */
#line 126 "VariableParser.h"

#line 70 "/usr/lib/bison.h"
 /* YY_VariableParser_LSP_NEEDED*/
#endif
/* DEFAULT LTYPE*/
#ifdef YY_VariableParser_LSP_NEEDED
#ifndef YY_VariableParser_LTYPE
typedef
  struct yyltype
    {
      int timestamp;
      int first_line;
      int first_column;
      int last_line;
      int last_column;
      char *text;
   }
  yyltype;

#define YY_VariableParser_LTYPE yyltype
#endif
#endif
/* DEFAULT STYPE*/
#ifndef YY_VariableParser_STYPE
#define YY_VariableParser_STYPE int
#endif
/* DEFAULT MISCELANEOUS */
#ifndef YY_VariableParser_PARSE
#define YY_VariableParser_PARSE yyparse
#endif
#ifndef YY_VariableParser_LEX
#define YY_VariableParser_LEX yylex
#endif
#ifndef YY_VariableParser_LVAL
#define YY_VariableParser_LVAL yylval
#endif
#ifndef YY_VariableParser_LLOC
#define YY_VariableParser_LLOC yylloc
#endif
#ifndef YY_VariableParser_CHAR
#define YY_VariableParser_CHAR yychar
#endif
#ifndef YY_VariableParser_NERRS
#define YY_VariableParser_NERRS yynerrs
#endif
#ifndef YY_VariableParser_DEBUG_FLAG
#define YY_VariableParser_DEBUG_FLAG yydebug
#endif
#ifndef YY_VariableParser_ERROR
#define YY_VariableParser_ERROR yyerror
#endif

#ifndef YY_VariableParser_PARSE_PARAM
#ifndef __STDC__
#ifndef __cplusplus
#ifndef YY_USE_CLASS
#define YY_VariableParser_PARSE_PARAM
#ifndef YY_VariableParser_PARSE_PARAM_DEF
#define YY_VariableParser_PARSE_PARAM_DEF
#endif
#endif
#endif
#endif
#ifndef YY_VariableParser_PARSE_PARAM
#define YY_VariableParser_PARSE_PARAM void
#endif
#endif

/* TOKEN C */
#ifndef YY_USE_CLASS

#ifndef YY_VariableParser_PURE
extern YY_VariableParser_STYPE YY_VariableParser_LVAL;
#endif


/* #line 143 "/usr/lib/bison.h" */
#line 204 "VariableParser.h"
#define	LEX_EQUAL	258
#define	LEX_COMMA	259
#define	LEX_COLON	260
#define	LEX_SEMICOLON	261
#define	LEX_OAB	262
#define	LEX_CAB	263
#define	LEX_OBB	264
#define	LEX_CBB	265
#define	LEX_OCB	266
#define	LEX_CCB	267
#define	LEX_INT	268
#define	LEX_REAL	269
#define	LEX_TIME	270
#define	LEX_PAIR	271
#define	LEX_TAG	272
#define	LEX_STRING	273
#define	LEX_DUP	274
#define	LEX_END	275


#line 143 "/usr/lib/bison.h"
 /* #defines token */
/* after #define tokens, before const tokens S5*/
#else
#ifndef YY_VariableParser_CLASS
#define YY_VariableParser_CLASS VariableParser
#endif

#ifndef YY_VariableParser_INHERIT
#define YY_VariableParser_INHERIT
#endif
#ifndef YY_VariableParser_MEMBERS
#define YY_VariableParser_MEMBERS 
#endif
#ifndef YY_VariableParser_LEX_BODY
#define YY_VariableParser_LEX_BODY  
#endif
#ifndef YY_VariableParser_ERROR_BODY
#define YY_VariableParser_ERROR_BODY  
#endif
#ifndef YY_VariableParser_CONSTRUCTOR_PARAM
#define YY_VariableParser_CONSTRUCTOR_PARAM
#endif
/* choose between enum and const */
#ifndef YY_VariableParser_USE_CONST_TOKEN
#define YY_VariableParser_USE_CONST_TOKEN 0
/* yes enum is more compatible with flex,  */
/* so by default we use it */ 
#endif
#if YY_VariableParser_USE_CONST_TOKEN != 0
#ifndef YY_VariableParser_ENUM_TOKEN
#define YY_VariableParser_ENUM_TOKEN yy_VariableParser_enum_token
#endif
#endif

class YY_VariableParser_CLASS YY_VariableParser_INHERIT
{
public: 
#if YY_VariableParser_USE_CONST_TOKEN != 0
/* static const int token ... */

/* #line 182 "/usr/lib/bison.h" */
#line 267 "VariableParser.h"
static const int LEX_EQUAL;
static const int LEX_COMMA;
static const int LEX_COLON;
static const int LEX_SEMICOLON;
static const int LEX_OAB;
static const int LEX_CAB;
static const int LEX_OBB;
static const int LEX_CBB;
static const int LEX_OCB;
static const int LEX_CCB;
static const int LEX_INT;
static const int LEX_REAL;
static const int LEX_TIME;
static const int LEX_PAIR;
static const int LEX_TAG;
static const int LEX_STRING;
static const int LEX_DUP;
static const int LEX_END;


#line 182 "/usr/lib/bison.h"
 /* decl const */
#else
enum YY_VariableParser_ENUM_TOKEN { YY_VariableParser_NULL_TOKEN=0

/* #line 185 "/usr/lib/bison.h" */
#line 294 "VariableParser.h"
	,LEX_EQUAL=258
	,LEX_COMMA=259
	,LEX_COLON=260
	,LEX_SEMICOLON=261
	,LEX_OAB=262
	,LEX_CAB=263
	,LEX_OBB=264
	,LEX_CBB=265
	,LEX_OCB=266
	,LEX_CCB=267
	,LEX_INT=268
	,LEX_REAL=269
	,LEX_TIME=270
	,LEX_PAIR=271
	,LEX_TAG=272
	,LEX_STRING=273
	,LEX_DUP=274
	,LEX_END=275


#line 185 "/usr/lib/bison.h"
 /* enum token */
     }; /* end of enum declaration */
#endif
public:
 int YY_VariableParser_PARSE(YY_VariableParser_PARSE_PARAM);
 virtual void YY_VariableParser_ERROR(char *msg) YY_VariableParser_ERROR_BODY;
#ifdef YY_VariableParser_PURE
#ifdef YY_VariableParser_LSP_NEEDED
 virtual int  YY_VariableParser_LEX(YY_VariableParser_STYPE *YY_VariableParser_LVAL,YY_VariableParser_LTYPE *YY_VariableParser_LLOC) YY_VariableParser_LEX_BODY;
#else
 virtual int  YY_VariableParser_LEX(YY_VariableParser_STYPE *YY_VariableParser_LVAL) YY_VariableParser_LEX_BODY;
#endif
#else
 virtual int YY_VariableParser_LEX() YY_VariableParser_LEX_BODY;
 YY_VariableParser_STYPE YY_VariableParser_LVAL;
#ifdef YY_VariableParser_LSP_NEEDED
 YY_VariableParser_LTYPE YY_VariableParser_LLOC;
#endif
 int YY_VariableParser_NERRS;
 int YY_VariableParser_CHAR;
#endif
#if YY_VariableParser_DEBUG != 0
public:
 int YY_VariableParser_DEBUG_FLAG;	/*  nonzero means print parse trace	*/
#endif
public:
 YY_VariableParser_CLASS(YY_VariableParser_CONSTRUCTOR_PARAM);
public:
 YY_VariableParser_MEMBERS 
};
/* other declare folow */
#endif


#if YY_VariableParser_COMPATIBILITY != 0
/* backward compatibility */
#ifndef YYSTYPE
#define YYSTYPE YY_VariableParser_STYPE
#endif

#ifndef YYLTYPE
#define YYLTYPE YY_VariableParser_LTYPE
#endif
#ifndef YYDEBUG
#ifdef YY_VariableParser_DEBUG 
#define YYDEBUG YY_VariableParser_DEBUG
#endif
#endif

#endif
/* END */

/* #line 236 "/usr/lib/bison.h" */
#line 369 "VariableParser.h"

#line 162 "VariableParser.y"

  // empty
#endif
