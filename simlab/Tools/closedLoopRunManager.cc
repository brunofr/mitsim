//-*-c++-*------------------------------------------------------------
//
// File name : closedLoopRunManager.cc
// Author :   Daniel Florian
// Date :     Nov. 24th, 2003
//
// Modification history:
//
// Date                     Author            Description
// ======                   ======            ============
//
//--------------------------------------------------------------------
//
//#include <fstream>
#include <iostream>
#include <sstream>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstdlib>
#include <cstring>
#include "closedLoopRunManager.h"
#include <cmath>
#include <cstdio>
#include <fcntl.h>
using namespace std;


char * closedLoopRunManager::name_ = NULL;

/// YW 4/20/2006: for compatability of old compilers, not inline this
/// function New C++ standard says it can be inlined, though

/// return reference to prevent un-wanted deletion
closedLoopRunManager& closedLoopRunManager::theOne()
{
    static closedLoopRunManager instance; /// won't create until first called
    return instance;
}


closedLoopRunManager::closedLoopRunManager(){
  runFileTimeStamp=0;
  last_guidance_interval=0; //represents the last od interval for which guidance has been received.  Note that DynaMIT will never generate guidance twice for the same od interval.

  // YW 4/11/2006: these initial values could be overridden by master files
  mDynamitOdIntervalLength = 900; // assuming 15 minutes OD interval
  mTimeToUnblock = 28800; // stop blocking for guidance at 8am
  mMaxAllowedDynamitDelay = 300;  // mitsim can continue to run 5 minutes without guidance
  
}

closedLoopRunManager::~closedLoopRunManager(){
  if (name_) {
    delete [] name_;
    name_ = NULL;
  }
}

/*checkRunStatus checks the timestamp on the DynaMIT guidance file to 
 * see if it has changed since the last run.
 * Returns 0 if guidance is ready, 1 otherwise
 */
int closedLoopRunManager::checkRunStatus(string fileName) {
  //check timestamp on file to see if it has changed  
  struct stat fileStatus; //data structure used to return values from fstat() call
  time_t newTimeStamp;
  
  if (!stat(fileName.c_str(),&fileStatus)) {
  } 
  else {
    //cout <<"closedlooprunmanager: guidance file not present... " <<endl;
    return 1;  //sensor file not found
  }
  newTimeStamp = fileStatus.st_ctime;
  
  //--- YW 11/7/2005: Define KeepGuidance version for NY project, not showing screen output --
#ifndef MITSIM_KEEP_GUIDANCE
  cout << "closedLoopRunManager: last file update:"<< runFileTimeStamp <<endl;
  cout << "closedLoopRunManager: current file timestamp:"<< newTimeStamp <<endl;
#endif  
  //--------------------------------------------------------------------
      
  if ((newTimeStamp != runFileTimeStamp) && (fileStatus.st_size != 0)) {
    runFileTimeStamp = newTimeStamp;
    return 0;  //ready for new run
  }
  else
    return 1;
}

/*checkRunStatusBlocking() will block if the computational delay for DynaMIT guidance has been 
 * exceeded. MITSIM will run for one DynaMIT OD Interval without guidance, but will then
 * require guidance to be provided within 5 minutes of each OD Interval.  If the guidance
 * is not ready, MITSIM will block and poll until guidance is ready.
 *
 * Returns 0 if guidance is ready, 1 otherwise
 */
int closedLoopRunManager::checkRunStatusBlocking(string fileName, double startTime, double nowTime) {
  
  struct stat fileStatus; //data structure used to return values from fstat() call
  //  time_t newTimeStamp=0;
  
  // YW 4/11/2006: change float to double.
  double dynamit_odinterval = 0;  //starts at 0
  double dynamit_lastodinterval = 0;
  double dynamit_intervaltime = 0; //time within the last(current) dynamit od interval
  int notready_flag = 1; //default is not ready

  // YW 4/11/2006: remove the hard-coded 15 minutes.
  //--- from ---
  // //WARNING: DynaMIT OD Interval is hard-coded at 15 minutes for now
  // dynamit_odinterval = (nowTime - startTime)/900; //900 seconds represents the DynaMIT 15 minute OD Interval.  If the DynaMIT OD Interval is 5 minutes, change this to 300 seconds. dynamit_od interval represents the # of OD increments simulated. Eg. halfway through 2nd OD interval is 1.5
  //--- changed to ---
  dynamit_odinterval = (nowTime - startTime)/mDynamitOdIntervalLength;
  //--- end of change ---
  
  dynamit_lastodinterval = floor(dynamit_odinterval);
  // YW 4/20/2006: remove hard-coded 900 seconds
  // dynamit_intervaltime = (nowTime - startTime) - 900 * floor(dynamit_odinterval); //amount of time simulated since last od interval
  //--- changed to ---
  dynamit_intervaltime = (nowTime - startTime) - mDynamitOdIntervalLength * dynamit_lastodinterval; //amount of time simulated since last od interval
  //--- end of change ---
  
  

  //  cout << "closedLoopRunManager: dynamit_odinterval:"<<dynamit_odinterval<<" dynamit_intervaltime="<<dynamit_intervaltime <<endl;
  //  cout << "closedLoopRunManager: runFileTimeStamp is " << runFileTimeStamp << endl;
  //  cout << "closedLoopRunManager: starttime is " << startTime << endl;
  //  cout << "closedLoopRunManager: nowtime is " << nowTime << endl;
  //  cout << "closedLoopRunManager: last_guidance_interval is " << last_guidance_interval << endl;

  
  if ((dynamit_odinterval < 1)){
    //don't block for guidance during 1st od interval
    notready_flag = checkRunStatus(fileName);
  }
  //if we are more than 5 min past the od interval mark,
  // and if we haven't yet had guidance in this interval...
  // ...then block until guidance is received
  
  else 
  {
      // YW 11/7/2005: For KeepGuidance version, remove the check of
      // guidance file, so that mitsim can move on even if old guidance is
      // not available.
#ifndef MITSIM_KEEP_GUIDANCE
      // YW 4/20/2006: change hard-coded 28800 to mTimeToUnblock,
      // and this parameter can be read from master.mitsim

      //--- 
      // if (nowTime<28800) //don't block for guidance after 8am - this is just for dan's runs! remove this!
      //--- changed to the following line ---
      if ( nowTime < mTimeToUnblock ) 
      {
          // YW 4/20/2006: change hard-coded 300 to mMaxAllowedDynamitDelay
          //---
          // if ((dynamit_intervaltime>300) && (last_guidance_interval < dynamit_lastodinterval)) //was 100
          //--- changed to ---
          if ( (dynamit_intervaltime > mMaxAllowedDynamitDelay) &&
               (last_guidance_interval < dynamit_lastodinterval) )
          {
              cout << "\nMaximal computational delay of DynaMIT (" << mMaxAllowedDynamitDelay
                   << ") exceeded: DynaMIT has not provided guidance to MITSIM during the first "
                   << dynamit_intervaltime << " seconds of the current interval.\n"
                   << "Current interval is ["
                   << mDynamitOdIntervalLength * dynamit_lastodinterval << "s - " 
                   << mDynamitOdIntervalLength * (dynamit_lastodinterval+1)
                   << "s] (in seconds, counting from the beginning of simulation).\n"
                   << "\nStart to block MITSIM and wait for guidance ... " << endl;
              
              while (notready_flag) {
                  sleep(10);
                  notready_flag = checkRunStatus(fileName);
                  //  last_guidance_interval = last_guidance_interval + (1 - notready_flag); 
                  cout << "closedlooprunmanager: still no guidance... last_guidance_interval was "
                       << last_guidance_interval << " dynamit_lastodinterval was "
                       << dynamit_lastodinterval << endl;
              }
          }
          else {  // don't block but still need to check for guidance
              notready_flag = checkRunStatus(fileName);
          }
      }
      else  //otherwise don't block for guidance
          notready_flag = checkRunStatus(fileName);
#else
      // YW 4/6/2006: this is only for KeepGuidance version
       notready_flag = checkRunStatus(fileName);
#endif
  }

  if (!notready_flag) {
    last_guidance_interval = last_guidance_interval + (1 - notready_flag); 
  }
  return notready_flag; 
}

/*
 * Move old file to timestamped archive
 */ 
void closedLoopRunManager::rotateOldGuidanceFile(string fileName) {  
  time_t curtime; 
  char timestr[30]; 
  ostringstream newfileNameStr; 
  ostringstream mvCommandStr; 
 
  sprintf(timestr, "%ld", time (&curtime)); 
  struct stat fileStatus; //data structure used to return values from fstat() call
  if (!stat(fileName.c_str(),&fileStatus)) { 
    newfileNameStr << fileName.c_str() << "." << timestr; 
    mvCommandStr << "mv " << fileName.c_str() << " " << newfileNameStr.str(); 
    //    cout<<"mvcommandstr is " << mvCommandStr.str()<<endl; 
    if (system(mvCommandStr.str().c_str())) 
      cout<<"ClosedLoopRunmanager: Warning: Could not rotate old guidance file, it will be overwritten"<<endl;
  } 
  else 
  cout << "ClosedLoopRunManager: Old Guidance file not found... no guidance to archive"<<endl; 
} 

/* Acquire file lock to avoid race conditions
 * between MITSIM and DynaMIT read/write operations
 * Returns -1 on failure, file descriptor>0 on success
 */
int closedLoopRunManager::getFileLock(string fileName) {
  int fd=-1;
  char fname[100];
  strcpy(fname,fileName.c_str());
  string lockfilename = strcat(fname,".lck");
  
  fd=open(lockfilename.c_str(), O_RDONLY | O_CREAT | O_EXCL);
  while (fd<0) {
    cout<<"ClosedLoopRunManager:Waiting for file lock "<<lockfilename << " to clear"<<endl;
    sleep(10); //wait for process to finish using file
    fd=open(fname, O_RDONLY | O_CREAT | O_EXCL);
  }
  
  cout <<"ClosedLoopRunManager: fd is "<< fd <<endl;
  return fd;
}
  
int closedLoopRunManager::removeFileLock(string fileName) {
  char fname[100];
  strcpy(fname,fileName.c_str());
  string lockfilename = strcat(fname,".lck");

  cout<<"ClosedLoopRunManager:Removing file lock "<<lockfilename<<endl;
  return remove(fname);
}
 
 

// YW 4/20/2006: the following functions enable the corresponding parameters
// to be set from outside
    
void
closedLoopRunManager::setDynamitOdIntervalLength(double seconds)
{
    cout << "\nclosedLoopRunManager::mDynamitOdIntervalLength changed from "
         << mDynamitOdIntervalLength << " to " << seconds << '\n';
    
    mDynamitOdIntervalLength = seconds;
}

void closedLoopRunManager::setTimeToUnblock(double seconds) 
{
    cout << "\nclosedLoopRunManager::mTimeToUnblock changed from "
         << mTimeToUnblock << " to " << seconds << '\n';

    mTimeToUnblock = seconds;
}

void closedLoopRunManager::setMaxAllowedDynamitDelay(double seconds) 
{
    cout << "\nclosedLoopRunManager::mMaxAllowedDynamitDelay changed from "
         << mMaxAllowedDynamitDelay << " to " << seconds << '\n';

    mMaxAllowedDynamitDelay = seconds;
}

