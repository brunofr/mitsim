//-*-c++-*------------------------------------------------------------
// CmdArgsParser.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef CMDARGSPARSER_HEADER
#define CMDARGSPARSER_HEADER

#include <UTL/CloParser.h>

// This class register some common used command line arguments.  You
// may inherit class from CmdArgsParser and register the arguments you
// needs.

class CmdArgsParser : public CloParser
{
public:

  CmdArgsParser();
  virtual ~CmdArgsParser() { }
  virtual bool parse(int *argc, char *argv[]);
};

#endif
