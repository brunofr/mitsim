//-*-c++-*------------------------------------------------------------
// CmdArgsParser.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------


#include <Tools/ToolKit.h>
#include <IO/MessageTags.h>
#include <IO/Communicator.h>
#include <IO/Exception.h>

#include "CmdArgsParser.h"
#include "Random.h"
#include "SimulationEngine.h"
#include "SimulationClock.h"
using namespace std;

// This class register some common used command line arguments

CmdArgsParser::CmdArgsParser()
  : CloParser()
{
  add_help();

  add(new Clo("-R", &Random::flags_, 0,
			  "Random seeds"));
  
  add(new Clo("-v", &ToolKit::verbose_, 1,
			  "Verbose mode:\n\t0 = Quiet\n\t1 = Verbose\n\t2 = Debug"));
  
  add(new Clo("-nice", &ToolKit::nice_, 1,
			  "CPU usage:\n\t0 = Dedicated\n\t1 = Primary\n\t2 = Shared"));
  
  add(new Clo("-mode", &theSimulationEngine->mode_, 0,
			  "Running mode:\n\t0 = Silent\n\t1 = Manual start\n\t2 = Demo\n\t4 = Demo with pause)"));
  
  add(new Clo("-m", &theSimulationEngine->master_, NULL,
			  "Master file"));
  
  add(new Clo("-t", &NO_MSG_WAITING_TIME, 0.1,
			  "Timeout for waiting message"));
}

bool CmdArgsParser::parse(int *argc, char **argv)
{
  if (CloParser::parse(argc, argv)) {
	cout << "Warning:: Unrecognized command line options:" << endl;
	print(*argc-1, argv+1, cout);
	cout << "Type \"" << argv[0] << " -help\" for available options."
		 << endl; 
	// theException->exit();
  }
  return false;
}
