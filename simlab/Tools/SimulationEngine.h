//-*-c++-*------------------------------------------------------------
// NAME: Simulation State
// AUTH: Qi Yang
// FILE: SimulationEngine.h
// DATE: Mon Nov 20 12:03:14 1995
//--------------------------------------------------------------------

#ifndef SIMULATIONENGINE_HEADER
#define SIMULATIONENGINE_HEADER

#include <iostream>
//using namespace std;

#include <IO/MessageTags.h>

// State of the simulation. This is the returned value by
// SimulationEngine::simulationLoop() or the inheritation of this
// function.

enum {
   STATE_QUIT_CALLED   = -8, // already quit
   STATE_ERROR_QUIT    = -3, // quit because of error
   STATE_QUIT          = -2, // simulation cancelled by user
   STATE_DONE          = -1, // simulation is done
   STATE_OK            =  0, // a step is done (clock advanced)
   STATE_WAITING       =  1, // waiting for other process,
   STATE_COMMUNICATING =  2, // send or receiving data
   STATE_NOT_STARTED   =  9  // initial value
};


//const short int MASTER_ENV_EXPANDED = 1;
//const short int MASTER_EXT_EXPANDED = 2;

const unsigned int OUTPUT_VEHICLE_LOG		= 0x00000001;

//const unsigned int OUTPUT_SENSOR		= 0x0000000E; // sum (removed by Angus)
const unsigned int OUTPUT_SENSOR_READINGS	= 0x00000002;
const unsigned int OUTPUT_VRC_READINGS		= 0x00000004;
const unsigned int OUTPUT_ASSIGNMENT_MATRIX    	= 0x00000008; // (Angus)

const unsigned int OUTPUT_LINK_TRAVEL_TIMES	= 0x00000010; // 16
const unsigned int OUTPUT_SEGMENT_TRAVEL_TIMES	= 0x00000020; // 32
const unsigned int OUTPUT_SEGMENT_STATISTICS	= 0x00000040; // 64
const unsigned int OUTPUT_QUEUE_STATISTICS	= 0x00000080; // 128
const unsigned int OUTPUT_TRAVEL_TIMES_TABLE	= 0x00000100; // 256
const unsigned int OUTPUT_VEHICLE_PATH_RECORDS	= 0x00000200; // 512
const unsigned int OUTPUT_VEHICLE_DEP_RECORDS	= 0x00000400; // 1024
const unsigned int OUTPUT_VEHICLE_TRAJECTORY	= 0x00000800; // 2048
const unsigned int OUTPUT_RECT_TEXT		= 0x00001000; // 4096
const unsigned int OUTPUT_SKIP_COMMENT		= 0x00002000; // 8192
const unsigned int OUTPUT_TRANSIT_TRAJECTORY	= 0x00004000; // 16384
const unsigned int OUTPUT_STOP_ARRIVAL  	= 0x00008000; // 32768
const unsigned int OUTPUT_STATE_3D              = 0x00010000; // 
const unsigned int OUTPUT_SIGNAL_PRIORITY  	= 0x00020000; // 
//  Tomer to have the lane changing locations

const unsigned int OUTPUT_LANE_CHANGING		= 0x00040000; // 
const unsigned int OUTPUT_LANE_UTILITIES        = 0x00080000; // gunwoo -define the constant for this file

const unsigned int OUTPUT_SYSTEM_MOE		= 0x10000000;
class GenericVariable;

class SimulationEngine
{
	  friend class CmdArgsParser;

   protected:

      char *master_;
      unsigned int chosenOutput_;
      int state_;

	  unsigned int mode_;

	  int nBreakPoints_;		// number of break points
	  double *breakPoints_;		// preset break points
	  int nextBreakPoint_;

   public:

      SimulationEngine();
      virtual ~SimulationEngine();

      inline void state(int s) { state_ = s; }
      inline int state() { return state_; }

	  virtual char *createFilename(const char *tag, int iter = -1);

      inline unsigned int isRectangleFormat() {
		 return chosenOutput_ & OUTPUT_RECT_TEXT;
      }
	  inline unsigned int skipComment() {
		 return chosenOutput_ & OUTPUT_SKIP_COMMENT;
	  }

	  inline int isDemoMode() { return mode_ & (MODE_DEMO|MODE_DEMO_PAUSE); }
	  inline unsigned int mode(unsigned int mask = 0xFFFFFFFF) {
		 return (mode_ & mask);
	  }
	  inline void unsetMode(unsigned int s) {
		 mode_ &= ~s;
	  }
	  inline void setMode(unsigned int s) {
		 mode_ |= s;
	  }

      inline unsigned int chosenOutput(unsigned int mask = 0xFFFFFFFF) {
		 return (chosenOutput_ & mask);
      }
      inline void chooseOutput(unsigned int mask) {
		 chosenOutput_ |= mask;
      }
      inline void removeOutput(unsigned int mask = 0) {
		 chosenOutput_ &= ~mask;
      }

	  const char *title() { return master_; }
      void master(const char* name);
	  const char * master() { return master_; }
	  virtual const char* ext() { return ""; }

      // Returns 0 if no error, negative if fatal error and positive
      // if warning error

      virtual int loadMasterFile();
      virtual int loadSimulationFiles() { return 0; }

	  virtual int canStart();
      virtual int isRunning();
      virtual int isWaiting();

      virtual int start();

      // This call simulationLoop in a loop. You have to overload this
      // function in graphical mode.  In batch mode, this function
      // would be good enough. This function will NOT return until the
      // simulation is done.

      virtual void run();

      // One step of the simulation. This function needs to be
      // overloaded in derived class to do the real things.  The dummy
      // function just prints the current time in the console window.

      virtual int simulationLoop();

      // No blocking communication to other processes. It returns a
      // positive integer if any message has been received and
      // processed; 0 if no message is received; or negative integer
      // if fatal errors occurs.  This function should be overloaded
      // in the derived class if interprocess communication is needed.

      virtual int receiveMessages(double sec = 0.0) = 0;

      // Called after simulation loop is done (or cancelled by user in
      // Graphical mode). Gives the last chance to do the clean up
      // tasks.

      virtual void quit(int s);

	  inline int nBreakPoints() { return nBreakPoints_; }
	  inline double breakPoint(int i) { return breakPoints_[i]; }

	  int isTimeToBreak(double now, double epsilon = 0.1);
	  double nextBreakPoint(double now);
	  inline void resetBreakPoint(double now) {
		 nextBreakPoint_ = 0;
		 nextBreakPoint(now);
	  }
	  void parseBreakPoints(int n, double *);
	  int parseBreakPoints(GenericVariable &gv);

	  virtual void save(std::ostream &os = std::cout);
};

extern SimulationEngine * theSimulationEngine;

#endif
