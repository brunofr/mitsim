/*
 * Create a source file contains the following information:
 *   who    : the persion who compiled the code
 *   when   : the time at compilation
 *   version: debug or fast version
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

const char* user()
{
    const char * usr;
    if (usr = getenv("NAME")) return usr;
    else if (usr = getenv("USER")) return usr;
    else if (usr = getenv("LOGNAME")) return usr;
    else return "";
}

int main(int argc, char *argv[])
{
    const char *tag = "const char* theVersionInfo[2]";
    FILE *fpo;
    time_t t = time(NULL);
    struct tm *c = localtime (&t);

    if (argc != 3) {
        printf("Usage: %s Flag VersionInfo.C\n", argv[0]);
        return 1;
    }

    if ((fpo = fopen(argv[2], "w")) == NULL) {
        printf("Cannot open output file %s.\n", argv[1]);
        return 1;
    }
  
    fprintf(fpo, "/* Generated code. Do NOT edit */\n\n");
    fprintf(fpo, "%s = {\n"
            "\t\"%s\",\n"
            "\t\"%2.2d:%2.2d:%2.2d %c %2.2d/%2.2d/%2.2d\"\n"
            "};\n",
            tag, user(),
            c->tm_hour, c->tm_min, c->tm_sec,
            *argv[1],
            //(c->tm_mon+1), c->tm_mday, c->tm_year);
            (c->tm_mon+1), c->tm_mday, c->tm_year + 1900); // YW 10/3/2005: c->tm_year will return 105 for year 2005...

    return 0;
}
