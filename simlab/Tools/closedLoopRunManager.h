//-*-c++-*------------------------------------------------------------
//
// File name : closedLoopRunManager.h
// Author :   Daniel Florian
// Date :     Nov. 24th, 2003
//
// Modification history:
//
// Date                     Author            Description
// ======                   ======            ============
//
//--------------------------------------------------------------------
//

#include "unistd.h"
//#include <fstream>
//#include <sstream>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstdlib>
#include <string>

class closedLoopRunManager {
  
public:

    /// YW 4/20/2006: change interface to use theOne so that we don't need
    /// objects like closedLoopRunManager runManager; and that we can
    /// eventually have more freedom to use it elsewhere other than merely
    /// in the main function, for improvement of efficiency
    /// 
    /// return reference to prevent un-wanted deletion
    static closedLoopRunManager& theOne();

private:
    /** Default constructor
     */

    /// YW 4/20/2005: make ctor non-public to prevent clients from creating a new closedLoopRunManager
    closedLoopRunManager();
    
    /// Prevent clients from creating a copy of the closedLoopRunManager,
    /// declare copy-ctor and operator=as private, and
    /// NEVER implement them!
    closedLoopRunManager( const closedLoopRunManager& );
    closedLoopRunManager& operator=(const closedLoopRunManager& );


    /** Destructor
     */
    virtual ~closedLoopRunManager();

public:
    
  int checkRunStatus(std::string fileName);

  int checkRunStatusBlocking(std::string fileName, double startTime, double nowTime);
  
  //Handler for parsing of master.mitsim - called by TS_FileManager
  static char * name_;	// file name
  static char * name() { return name_; }
  static char ** nameptr() { return &name_; }
  
  /*
   * Move old file to timestamped archive
   */ 
  void rotateOldGuidanceFile(std::string fileName);

  /* Acquire file lock to avoid race conditions
   * between MITSIM and DynaMIT read/write operations
   * Returns -1 on failure, file descriptor>0 on success
   */
  int getFileLock(std::string fileName);
  int removeFileLock(std::string fileName);
  
 private:
  
  /** The timestamp on the 'run file' from MITSIM
   */
  time_t runFileTimeStamp;  //time in seconds
  int last_guidance_interval; //represents the last od interval for which guidance has been received.  Note that DynaMIT will never generate guidance twice for the same od interval.

    // YW 4/11/2006: add functions for customizing closed-loop parameters
    double mDynamitOdIntervalLength;   ///< length of DynaMIT OD interval (in seconds)

    // YW 4/20/2006: the second to stop blocking for guidance (was hard-coded 28800)
    double mTimeToUnblock;
    
    // time before blocking (``computational delay'' in Dan's thesis, section 3.3 ATIS Evaluation Framework)
    double mMaxAllowedDynamitDelay;
    
    
public:

    // YW 4/20/2006: the following functions enable the corresponding
    // parameters to be set from outside
    
    void setDynamitOdIntervalLength(double seconds);

    void setTimeToUnblock(double seconds); 

    void setMaxAllowedDynamitDelay(double seconds);
    
  
};
