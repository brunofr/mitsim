/*
 * This is a program to update the license data
 */

#include <stdio.h>

int main(int argc, char *argv[])
{
   const int size = 256;
   long int sum = 0;
   long int pos;
   int uid;
   int lines = 0;
   char username[size];
   FILE *fpi, *fpo = stdout;

   if (argc != 3) {
      fprintf(stdout, "Usage: %s license.txt license.dat\n", argv[0]);
      return 1;
   }

   if ((fpi = fopen(argv[1], "r")) == NULL) {
      fprintf(stderr, "Cannot open input file %s\n", argv[1]);
      return 1;
   }
  
   if ((fpo = fopen(argv[2], "w")) == NULL) {
      fprintf(stderr, "Cannot open output file %s\n", argv[2]);
      return 1;
   }

   if (fgets(username, size, fpi) == NULL) goto error;
   fprintf(fpo, "%% License data. Do NOT change\n");
   pos = ftell(fpo);
   fprintf(fpo, "%5x %s\n", 0, "% License Number");

   if (fgets(username, size, fpi) == NULL) goto error;

   while (fscanf(fpi, "%d", &uid) == 1 &&
		  fgets(username, size, fpi) != NULL) {
	 fprintf(fpo, "%5.5d%s", uid, username); 
	 sum += uid;
	 lines ++;
   }
   if (fseek(fpo, pos, SEEK_SET)) goto error;

   fprintf(fpo, "%2.2x%3.3x", lines, sum%lines);

   fclose(fpi);
   fclose(fpo);

   return 0;

  error:

   fprintf(stderr, "Failed convert license data\n");
   return 1;
}

