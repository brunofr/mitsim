//-*-c++-*------------------------------------------------------------
// SMC_Options.h
//
// qiyang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef OPTIONS_HEADER
#define OPTIONS_HEADER

#include <iostream>

typedef struct {
  int verbose;
  int nice;
  int points;
  double *breaks;
} OptionInfo;

class Options
{
public:

  Options();
  ~Options();

  void allocBreakPoints(int n);	// create space

  void verbose(int p) { info_.verbose = p; }
  void nice(int p) { info_.nice = p; }
  void setBreakPoints(int n, double *points);
  void setBreakPoint(int i, double p) {
	info_.breaks[i] = p;
  }

  int verbose() { return info_.verbose; }
  int nice() { return info_.nice; }

  int nBreakPoints() { return info_.points; }
  double breakPoint(int i) { return info_.breaks[i]; }
  double* breakPoints() { return info_.breaks; }

  void print(std::ostream &os = std::cout) {
	printBreakPoints(os);
	printOptions(os);
  }
  void printOptions(std::ostream &os = std::cout);
  void printBreakPoints(std::ostream &os = std::cout);

private:
	
  OptionInfo info_;
};

extern Options *theOptions;

#endif
