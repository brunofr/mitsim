//-*-c++-*------------------------------------------------------------
// NAME: GenericVariable.h
// AUTH: Qi Yang
// DATE: Wed Dec  6 18:05:09 EST 1995
//--------------------------------------------------------------------

#ifndef GENERICVARIABLE_HEADER
#define GENERICVARIABLE_HEADER

#include <iostream>
#include <cstring>
#include <cstdlib>
#include <UTL/bool.h>

const int GROW_SIZE = 16;

class PairElement
{
  friend class ArrayElement;

public:

  PairElement(int o, int d) : o_(o), d_(d) { }
  PairElement(PairElement* p) : o_(p->o_), d_(p->d_) { }
  int o() { return o_; }
  int d() { return d_; }
  PairElement& operator = (PairElement& p) {
	o_ = p.o_;
	d_ = p.d_;
	return *this;
  }

private:

  int o_;
  int d_;
};


class ArrayElement
{
public:

  enum ArrayElementType {
	UNSET,
	BOOLEAN,
	SHORT_INT,
	INTEGER,
	UNSIGNED_INT,
	FLOAT,
	DOUBLE,
	STRING,
	PAIR
  };

  ArrayElement() : type_(UNSET), d_element_(0) { }

#ifndef INTBOOL
  ArrayElement(bool element) { 
	b_element_ = element;
	type_ = BOOLEAN;
  }
#endif

  ArrayElement(short int element) { 
	h_element_ = element;
	type_ = SHORT_INT;
  }

  ArrayElement(int element) { 
	i_element_ = element;
	type_ = INTEGER;
  }

  ArrayElement(unsigned int element) { 
	u_element_ = element;
	type_ = UNSIGNED_INT;
  }

  ArrayElement(float element) { 
	f_element_ = element;
	type_ = FLOAT;
  }

  ArrayElement(double element) { 
	d_element_ = element;
	type_ = DOUBLE;
  }

  ArrayElement(const char * element) {
	if (element) {
	  s_element_ = strdup(element);
	} else {
	  s_element_ = NULL;
	}
	type_ = STRING;
  }
      
  ArrayElement(int o, int d) {
	p_element_ = new PairElement(o, d);
	type_ = PAIR;
  }

  ArrayElement(const ArrayElement &other);

  ~ArrayElement() {
	clean();
  }

  void clean() ;

  static char* name(ArrayElementType);

  void deepCopy(const ArrayElement &other);

  ArrayElement& operator = (const ArrayElement &other); 

#ifndef INTBOOL
  bool operator = (bool b) {
	clean();
	b_element_ = b;
	type_ = BOOLEAN;
	return b_element_;
  }
#endif

  short int operator = (short int h) {
	clean();
	h_element_ = h;
	type_ = SHORT_INT;
	return h_element_;
  }

  int operator = (int i) {
	clean();
	i_element_ = i;
	type_ = INTEGER;
	return i_element_;
  }

  unsigned int operator = (unsigned int u) {
	clean();
	u_element_ = u;
	type_ = INTEGER;
	return u_element_;
  }

  float operator = (float f) {
	clean();
	f_element_ = f;
	type_ = FLOAT;
	return f_element_;
  }

  double operator = (double d) {
	clean();
	d_element_ = d;
	type_ = DOUBLE;
	return d_element_;
  }

  char* operator = (char * s_element) { 
	clean();
	if (s_element) {
	  s_element_ = strdup(s_element);
	} else {
	  s_element_ = NULL;
	}
	type_ = STRING;
	return s_element_;
  }

  PairElement* operator = (PairElement* p) {
	clean();
	p_element_ = new PairElement(p);
	type_ = PAIR;
	return p_element_;
  }

  ArrayElementType type() { return type_; }

  // The original types

#ifndef INTBOOL
  operator bool() { return b_element() ; }
#endif
  operator short int() { return (short int) i_element(); }
  operator int() { return i_element(); }
  operator unsigned int() { return (unsigned int) i_element(); }
  operator float () { return d_element(); }
  operator double () { return d_element(); }
  operator char *() { return s_element(); }

  short int h_element() { return (short int) i_element(); }
  int i_element();

#ifndef INTBOOL
  bool b_element();
#endif

  unsigned int u_element() { return (unsigned int) i_element(); }
  float f_element() { return d_element(); }
  double d_element();
  char * s_element();
  PairElement* p_element();

private:
      
  friend std::ostream & operator << (std::ostream &os, ArrayElement &ae);

  ArrayElementType type_;

  union {
	bool b_element_;
	short int h_element_;
	int i_element_;
	unsigned int u_element_;
	float f_element_;
	double d_element_;
	char* s_element_;
	PairElement *p_element_;
  };
};


class GenericVariable
{
  friend std::ostream & operator << (std::ostream &os, GenericVariable &gv); 

protected:
      
  char *name_;		// variable name

  // each element could be double, int, or string

  ArrayElement* elements_;
  int nElements_;
  int nCapacity_;

public:

  GenericVariable();
  ~GenericVariable();

  char * name() { return name_; }
  void name(const char *n);
  void clean();

  void addElement(const ArrayElement &e);

  // Convenience interface for mutable functions

#ifndef INTBOOL
  void addElement(bool e) {
 	addElement(ArrayElement(e));
  }
#endif

  void addElement(int e) {
 	addElement(ArrayElement(e));
  }
  void addElement(short int e) {
	addElement(ArrayElement(e));
  }
  void addElement(unsigned int e) {
	addElement(ArrayElement(e));
  }
  void addElement(double e) {
	addElement(ArrayElement(e));
  }
  void addElement(float e) { 
	addElement(ArrayElement(e));
  }
  void addElement(const char *e) {
	addElement(ArrayElement(e));
  }
  void addElement(int o, int d) {
	addElement(ArrayElement(o, d));
  }

  inline int nElements() const { return nElements_; }

  // These are the access functions

  char* string(int i = 0) { return element(i); }
  double number(int i = 0) { return element(i); }

  PairElement* pair(int i = 0);
  double getTime(int i = 0);

  ArrayElement& element(int i = 0);
  ArrayElement* elements(int i = 0);
};

std::ostream & operator << (std::ostream &os, GenericVariable &gv); 
std::ostream & operator << (std::ostream &os, ArrayElement &ae);

#endif

