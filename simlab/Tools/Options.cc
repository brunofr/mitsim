//-*-c++-*------------------------------------------------------------
// Options.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include "ToolKit.h"
#include "Options.h"
using namespace std;

Options *theOptions = new Options;

Options::Options()
{
   info_.verbose   = ToolKit::VERBOSE_NORMAL;
   info_.nice      = ToolKit::NICE_PRIMARY;
   info_.points    = 0;
   info_.breaks    = NULL;
}


Options::~Options()
{
   delete [] info_.breaks;
}

void Options::allocBreakPoints(int n)
{
   if (info_.breaks) {
	  delete [] info_.breaks;
   }
   info_.points = n;
   info_.breaks = new double[n];
}

void Options::setBreakPoints(int n, double *points)
{
   allocBreakPoints(n);
   memcpy(info_.breaks, points, sizeof(double) * n);
}

void Options::printOptions(ostream &os)
{
   os << endl
	  << "# [Verbose]   = " << info_.verbose << endl
	  << "# [Nice]      = " << info_.nice << endl;
   if (info_.points) printBreakPoints(os);
}

void Options::printBreakPoints(ostream &os)
{
   os << endl << "[Break Points] = {" << endl;
   for (int i = 0; i < info_.points; i ++) {
	  os << "\t" << info_.breaks[i] << endl;
   }
   os << "}" << endl;
}
