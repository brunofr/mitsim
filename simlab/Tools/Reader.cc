//-*-c++-*------------------------------------------------------------
// NAME: File Reader
// FILE: Reader.C
// DATE: Mon Oct 30 13:52:56 1995
//--------------------------------------------------------------------

#include <fstream>
#include <cstring>
#include <cctype>
#include <cstdio>
#include <cstdlib>

#include <IO/Exception.h>

#include "Reader.h"
#include "ToolKit.h"
using namespace std;

const char newline = '\n';

// Create an empty file reader

Reader::Reader()
   : name_(NULL),
     isp(new ifstream ()),
     isp_was_allocated(1)
{
   reset_counters();
}


// Create a file reader and attach it to a existing file stream

Reader::Reader(istream& is)
   : name_(NULL),
     isp_was_allocated(0)
{
   reset_counters();
   isp = &is;
}


// Create a file reader for the file 'fname' in default input
// directory.  It tries to open the file in directory 'altpath' if the
// file does not exist the default input directory and 'altpath' is
// specified (not NULL).

Reader::Reader(const char* fname, const char* altpath)
   : isp_was_allocated(1)
{
   reset_counters();
   const char *fn;
   if (ToolKit::fileExists(fn = ToolKit::file(fname)) ||
       altpath &&
       ToolKit::fileExists(fn = ToolKit::file(fname, altpath))) {
      isp = new ifstream (fn);
   } else goto error;

   if (!(*isp)) goto error;

   name_ = Copy(fn);

   return;

  error:

   cerr << endl << ToolKit::errorMsgPrefix()
		<< "I couldn't open <" << fn << ">"
		<< " on <" << HostName() << ">." << endl << endl;
   theException->exit(1);
}


void Reader::reset_counters()
{
   last_count = 0;
   current_line = 1;
   beginning_field = 0;
   failure_is_fatal = 1;
}

// The destructor

Reader::~Reader()
{
   if (isp_was_allocated) {
      delete isp;
   }
   if (name_) {
      free(name_);
   }
}


// Attach this file reader to the file in the default input directory

void
Reader::open(const char* fname, const char* altpath)
{
   reset_counters();

   const char *fn;

   if (ToolKit::fileExists(fn  = ToolKit::file(fname)) ||
       altpath &&
       ToolKit::fileExists(fn = ToolKit::file(fname, altpath))) {
      ((ifstream *)isp)->open(fn);
   } else goto error;

   if (!(*isp)) goto error;

   name_ = Copy(fname);

   return;

  error:

   cerr << endl << ToolKit::errorMsgPrefix()
		<< "I couldn't open <" << fn << "> on <"
		<< HostName() << ">." << endl << endl;
   theException->exit(1);
}


void
Reader::close()
{
   ((ifstream *)isp)->close();
}


int
Reader::skip_comments()
{
   int c;
  
   // Skip white space

   while (isspace(c = isp->get()) ||
	  c == ',' || c == ';') {
      if (c == newline) current_line ++;
   }

   if (c == '#' ||
       c == '%' ||
       c == '/' && isp->peek() == '/') {

      // Skip the rest of the line

      isp->ignore(FILE_READER_LINESIZE, newline);
      current_line ++;

      return skip_comments();

   } else if (c == '/' &&  isp->peek() == '*') {

      // Skip everything in the comment block

      c = isp->get();		// skip the 1st "*"
      int last = 0;
      while (!(eof()) && good()) {
	 c = isp->get();
	 if (c == '/' && last == '*') break;
	 else last = c;
	 if (last == newline) current_line ++;
      }

      return skip_comments();

   } else if (c != EOF) {
      isp->putback((char) c);
   }
   return c;
}


int
Reader::eatwhite()
{
   beginning_field = skip_comments();
   return beginning_field;
}

/* Read a string of characters.  The leading spaces is ignored.
 * The reading will stop when it hit a white space, close token
 * or end-of-file.
 */

Reader&
Reader::operator >>(char* b)
{
   char c;
   eatwhite();
   while (!(eof()) && good() && 
	  (c = isp->peek()) != CLOSE_TOKEN &&
	  !isspace(c)) {
      *isp >> c;
      *b = c;
      b ++;
   }
   *b = '\0';
   check_status("extracting a string");
   return (*this);
}


Reader&
Reader::operator >>(unsigned char& c)
{
   eatwhite();
   *isp >> c;
   check_status("extracting an unsigned char");
   return (*this);
}

Reader&
Reader::operator >>(char& c)
{
   eatwhite();
   *isp >> c;
   check_status("extracting a char");
   return (*this);
}

Reader&
Reader::operator >>(unsigned short& i)
{
   eatwhite();
   *isp >> i;
   check_status("extracting a unsigned short");
   return (*this);
}

Reader&
Reader::operator >>(short& i)
{
   eatwhite();
   *isp >> i;
   check_status("extracting a short");
   return (*this);
}

Reader&
Reader::operator >>(unsigned int& i)
{
   eatwhite();
   *isp >> i;
   check_status("extracting a unsigned int");
   return (*this);
}

Reader&
Reader::operator >>(int& i)
{
   eatwhite();
   *isp >> i;
   check_status("extracting an int");
   return (*this);
}

Reader&
Reader::operator >>(unsigned long& l)
{
   eatwhite();
   *isp >> l;
   check_status("extracting a unsigned long");
   return (*this);
}

Reader&
Reader::operator >>(long& l)
{
   eatwhite();
   *isp >> l;
   check_status("extracting a long");
   return (*this);
}

Reader&
Reader::operator >>(float& f)
{
   eatwhite();
   *isp >> f;
   check_status("extracting a float");
   return (*this);
}

Reader&
Reader::operator >>(double& d)
{
   eatwhite();
   *isp >> d;
   check_status("extracting a double");
   return (*this);
}

Reader&
Reader::get(unsigned char& c)
{
   c = '\0';
   skip_comments();
   *isp >> c;
   check_status("extracting an unsigned char");
   return (*this);
}

Reader&
Reader::get(char& c)
{
   c = '\0';
   skip_comments();
   *isp >> c;
   check_status("extracting a char");
   return (*this);
}

int
Reader::get()
{
   skip_comments();
   int c = isp->get();
   return (c);
}

unsigned int
Reader::gethex()
{
   char buf[16];
   unsigned int h;

   skip_comments();
   *this >> buf;
   
   if (sscanf(buf, "%X", &h) != 1) {
      error_quit("extracting a hex integer");
   }

   return (h);
}


double
Reader::gettime()
{
   char buf[16];
   double t = 0.0;

   skip_comments();
   *this >> buf;

   if (strchr(buf, ':')) {
      int hh, mm;
      double ss;
      if (sscanf(buf, "%d:%d:%lf", &hh, &mm, &ss) != 3) goto data_error;
      if (hh < 0 || hh > 23) goto data_error;
      t += hh * 3600.0;
      if (mm < 0 || mm > 59) goto data_error;
      t += mm * 60.0;
      if (ss < 0.0 || ss >= 60.0) goto data_error;
      t += ss;
   } else {
      if (sscanf(buf, "%lf", &t) != 1) goto data_error;
   }

   return t;

  data_error:

   error_quit("extracting a time");

   return 0.0;
}


Reader&
Reader::getline(char* b, int lim, char delim)
{
   char *s = b;
   int c;

   skip_comments();

   for (last_count = 0, c = isp->get();
	last_count < lim && c != delim && isp->good();
	c = isp->get()) {
      if (c == newline) current_line++;
      *s++ = c;
      last_count ++;
   }

   if (last_count == 0 && isp->eof()) {
      isp->clear(ios::failbit | isp->rdstate());
   } else if (last_count >= lim && c != EOF) {
      isp->putback((char) c);
   } else if (isp->good())	{ /* must have reached delimiter */
      if (delim == newline) current_line ++;
      last_count ++;
   } else if (isp->fail()) {
      check_status("using getline or get");
   }

   *s = '\0';
   return (*this);
}

Reader&
Reader::get(char* b, int lim, char delim)
{
   getline(b, lim, delim);
   if (last_count > 0 && last_count < lim) {
      isp->putback(b[--last_count]);
      if (b[last_count] == newline) current_line --;
      b[last_count] = '\0';
   }
   return (*this);
}


int
Reader::findToken(char token)
{
   skip_comments();
   if (isp->get() == token) return (1);
   const char *msg;
   msg = Str("extracting token <%c>", token);
   error_quit(msg);
   return 0;
}


void
Reader::error_quit(const char *msg, int line_info)
{
   cerr << endl << "Error:: operation failed";

   if (msg) cerr << " " << msg;
   cerr << ". ";

   if (line_info) reference();
   else cerr << endl;

   theException->exit(1);
}


void
Reader::check_status(const char* operation)
{
   if (isp->eof()) {
      cerr << "Error:: End of file encountered while "
	   << operation << ". ";
      reference();
   } else if (isp->fail()) {
      cerr << "Error:: Operation failed "
	   << operation << ". ";
      reference();
   }
   if (failure_is_fatal && !(*isp)) {
      theException->exit(1);
   }
}


void
Reader::reference()
{
   cerr << "Problem in (" << name() << ":"
	<< current_line << ")." << endl;
}
