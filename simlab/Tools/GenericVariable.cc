//-*-c++-*------------------------------------------------------------
// NAME: GenericVariable.C
// AUTH: Qi Yang
// DATE: Wed Dec  6 18:05:09 EST 1995
//--------------------------------------------------------------------

#include <IO/Exception.h>

#include "GenericVariable.h"
#include "ToolKit.h"
#include "SimulationClock.h"
using namespace std;

ArrayElement::ArrayElement(const ArrayElement &other)
{
  deepCopy(other);
}

void ArrayElement::clean()
{
  if (type_ == STRING && s_element_) {
	free (s_element_);
  } else if (type_ == PAIR && p_element_) {
	delete p_element_;
  }
  type_ = UNSET;
  s_element_ = NULL;
}

char *
ArrayElement::name(ArrayElementType t)
{
  switch (t) {
  case BOOLEAN:
	{
	  return "bool";
	}
  case SHORT_INT:
	{
	  return "short interger";
	}
  case INTEGER:
	{
	  return "interger";
	}
  case UNSIGNED_INT:
	{
	  return "unsigned interger";
	}
  case FLOAT:
	{
	  return "float";
	}
  case DOUBLE:
	{
	  return "double";
	}
  case STRING:
	{
	  return "string";
	}
  case PAIR:
	{
	  return "pair";
	}
  default:
	{
	  return "unset element";
	}
  }
}


ArrayElement& 
ArrayElement::operator = (const ArrayElement &other)
{
  deepCopy(other);
  return *this;
}


void
ArrayElement::deepCopy(const ArrayElement &other) 
{
  type_ = other.type_;
  switch (type_) {
  case BOOLEAN:
  {
	  b_element_ = other.b_element_ ;
	  break;
  }
  case SHORT_INT:
	{
	  h_element_ = other.h_element_;
	  break;
	}
  case INTEGER:
	{
	  i_element_ = other.i_element_;
	  break;
	}
  case UNSIGNED_INT:
	{
	  u_element_ = other.u_element_;
	  break;
	}
  case FLOAT: 
	{
	  f_element_ = other.f_element_;
	  break;
	}
  case DOUBLE:
	{
	  d_element_ = other.d_element_;
	  break;
	}
  case STRING:
	{
	  if (other.s_element_) { 
		s_element_ = Copy(other.s_element_);
	  } else {
		s_element_ = NULL;
	  }
	  break;
	}
  case PAIR:
	{
	  if (other.s_element_) {
		p_element_ = new PairElement(other.p_element_);
	  } else {
		p_element_ = NULL;
	  }
	  break;
	}
  default:
	{
	  b_element_ = false;
	  h_element_ = 0;
	  i_element_ = 0;
	  u_element_ = 0;
	  f_element_ = 0.0;
	  d_element_ = 0.0;
	  s_element_ = NULL;
	  p_element_ = NULL;
	}
  }
}


int
ArrayElement::i_element() 
{
  switch (type_) {
  case SHORT_INT:
	{
	  return (int) h_element_;
	}
  case INTEGER:
	{
	  return i_element_;
	}
  case UNSIGNED_INT:
	{
	  return (int) u_element_;
	}
  default:
	{
	  cerr << "Error:: Cannot convert a " << name(type_)
		   << " to " << name(INTEGER) << "." << endl;
	  theException->exit(1);
	}
  }
  return 0;
}

#ifndef INTBOOL

bool
ArrayElement::b_element() 
{
  switch (type_) {
  case BOOLEAN:
  {
	  return b_element_ ;
  }
  case SHORT_INT:
  {
	  return (bool) h_element_;
  }
  case INTEGER:
	{
	  return (bool) i_element_;
	}
  case UNSIGNED_INT:
	{
	  return (bool) u_element_;
	}
  default:
	{
	  cerr << "Error:: Cannot convert a " << name(type_)
		   << " to " << name(BOOLEAN) << "." << endl;
	  theException->exit(1);
	}
  }
  return 0;
}
#endif

double 
ArrayElement::d_element() 
{
  switch (type_) {
  case SHORT_INT:
	{
	  return h_element_;
	}
  case INTEGER:
	{
	  return i_element_;
	}
  case UNSIGNED_INT:
	{
	  return u_element_;
	}
  case FLOAT:
	{
	  return f_element_;
	}
  case DOUBLE:
	{
	  return d_element_;
	}
  default:
	{
	  cerr << "Error:: Cannot convert a " << name(type_)
		   << " to " << name(DOUBLE) << "." << endl;
	  theException->exit(1);
	}
  }
  return 0.0;
}


char *
ArrayElement::s_element() 
{ 
  if (type_ == STRING) return s_element_; 
  else {
	cerr << "Error:: Cannot convert a " << name(type_)
		 << " to " << name(STRING) << "." << endl;
	theException->exit(1);
  }
  return NULL;
}

PairElement*
ArrayElement::p_element() 
{ 
  if (type_ == PAIR) return p_element_; 
  else {
	cerr << "Error:: Cannot convert a " << name(type_)
		 << " to " << name(PAIR) << "." << endl;
	theException->exit(1);
  }
  return NULL;
}


GenericVariable::GenericVariable()
  : name_(NULL),
	nElements_(0),
	nCapacity_(128)
{
	elements_ = new ArrayElement[nCapacity_] ;
}

GenericVariable::~GenericVariable()
{
  clean() ;
  if (elements_) {
	delete [] elements_ ;
  }
}

void GenericVariable::clean()
{
  if (name_) {
	free(name_) ;
	name_ = NULL ;
  }
  for (int i = 0; i < nElements(); i ++) {
	elements_[i].clean() ;
  }
  nElements_ = 0 ;
}

void
GenericVariable::addElement(const ArrayElement &elem)
{
  if (nElements_ >= nCapacity_) {
	ArrayElement* es = elements_ ;
	nCapacity_ *= 2 ;
	elements_ = new ArrayElement[nCapacity_] ;
	for (int i = 0; i < nElements_; i ++) {
	  elements_[i].deepCopy(es[i]) ;
	}
  }
  elements_[nElements_].deepCopy(elem) ;
  nElements_ ++ ;
}


ArrayElement &
GenericVariable::element(int i)
{
  if (i >= 0 && i < nElements()) {
	return elements_[i];
  } else if (nElements() > 0) {
	cerr << "Error:: Index <" << i << "> is outside the boundary "
		 << "[0," << nElements() << ") of variable <" << name_ << ">."
		 << endl;
  } else {
	cerr << "Error:: Variable " << name_
		 << "> is used before its value are set." << endl;
  }
  theException->exit(1);
  return elements_[0];		// this will never be called
}


ArrayElement*
GenericVariable::elements(int i)
{
  if (i >= 0 && i < nElements()) {
	return (elements_ + i);
  }
  return NULL;
}


PairElement*
GenericVariable::pair(int i)
{
  return element(i).p_element();
}

double GenericVariable::getTime(int i)
{
  ArrayElement &e = element(i);
  if (e.type() == ArrayElement::STRING) {
	return theSimulationClock->convertTime(e.s_element());
  } else {
	return (double) e;
  }
}

void 
GenericVariable::name(const char *n) 
{
  if (n == name_) return ;
  if (name_) free(name_) ;
  name_ = Copy(n); 
}


ostream & operator << (ostream &os, GenericVariable &gv)
{
  int n = gv.nElements() ;
  const char* str = gv.name() ;
  os << str << " = ";
  if (n > 1) os << "{ ";
  for (int i = 0; i < n; i++) {
	os << gv.element(i) << " ";
  }
  if (n > 1) os << "}";
  return os;
}

ostream & operator << (ostream &os, ArrayElement &ae)
{
  switch (ae.type()) {
  case ArrayElement::SHORT_INT:
	{
	  os << ae.h_element();
	  break;
	}
  case ArrayElement::INTEGER:
	{
	  os << ae.i_element();
	  break;
	}
  case ArrayElement::UNSIGNED_INT:
	{
	  os << ae.u_element();
	  break;
	}
  case ArrayElement::FLOAT: 
	{
	  os << ae.f_element();
	  break;
	}
  case ArrayElement::DOUBLE:
	{
	  os << ae.d_element();
	  break;
	}
  case ArrayElement::STRING:
	{
	  os << ae.s_element();
	  break;
	}
  case ArrayElement::PAIR:
	{
	  os << ae.p_element()->o() << '-' << ae.p_element()->d();
	  break;
	}
  default:
	{
	  os << "<null>";
	  break;
	}
  }
  return os;
}
