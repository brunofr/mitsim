#ifndef _XfwTabBook_h
#define _XfwTabBook_h

/***********************************************************************
 *
 * TabBook Widget
 *
 ***********************************************************************/

/*
  New resource names/classes
*/

#define XfwNtabShadowThickness "tabShadowThickness"
#define XfwCTabShadowThickness "TabShadowThickness"

#define XfwNautoManage	"autoManage"
#define XfwCAutoManage	"AutoManage"

#define XfwNactivePage	"activePage"
#define XfwCActivePage	"ActivePage"

#define XfwNnewPageCallback "newPageCallback,"
#define XfwCNewPageCallback "NewPageCallback,"

/*
  Constraint resource names/classes
*/

#define XfwNresizeChild	 "resizeChild"
#define XfwCResizeChild	"ResizeChild"

#define XfwRESIZE_NONE 0
#define XfwRESIZE_VERTICAL 1
#define XfwRESIZE_HORIZONTAL 2
#define XfwRESIZE_BOTH (XfwRESIZE_VERTICAL | XfwRESIZE_HORIZONTAL)

#define XfwNanchorChild	 "anchorChild"
#define XfwCAnchorChild	 "AnchorChild"


#define XfwANCHOR_CENTER	0
#define XfwANCHOR_EAST		1
#define XfwANCHOR_NORTH		2
#define XfwANCHOR_NORTHEAST	3
#define XfwANCHOR_NORTHWEST	4
#define XfwANCHOR_SOUTH		5
#define XfwANCHOR_SOUTHEAST	6
#define XfwANCHOR_SOUTHWEST	7
#define XfwANCHOR_WEST		8


#if defined(__cplusplus) || defined(c_plusplus)
extern "C" { 
#endif

typedef struct _TabBookClassRec	*XfwTabBookWidgetClass;
typedef struct _TabBookRec	*XfwTabBookWidget;

extern WidgetClass xfwTabBookWidgetClass;

#define XfwTabBook_OPT_NO_CB 1
Boolean XfwTabBookSetActivePage(Widget w, int page, int option );
int XfwTabBookGetActivePage(Widget w );
Widget XfwTabBookGetActivePageWidget(Widget w);


#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

/*
 * Callback reasons.  Try to stay out of range of the Motif XmCR_* reasons.
 */
typedef enum _XfwTabBookgReasonType {
    XfwNewPage = 202		/* New page is active */
} XfwTabBookReasonType;


/*
** Struct passed to application when called back
*/
typedef struct _XfwTabBookCallbackData
{
	XfwTabBookReasonType reason;	/* reason for callback */
	XEvent *event;			/* button event, NULL if emulated  */
	int prev_active_page;		/* 0 when initially called */
	int active_page;		/* new active page */
	Widget button;			/* the button widget which was
					   pressed (emulated or actual*/
	Boolean ret_veto;		/* caller may set to True to stop page change */
	int future1;
	void * future2;
} XfwTabBookCallbackData;

 
#endif /* _XfwTabBook_h */
