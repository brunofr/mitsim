//-*-c++-*------------------------------------------------------------
// NAME: GDS Arc -- Element for GDS_GraphicBlock
// AUTH: Qi Yang
// FILE: GDS_Arc.h
// DATE: Thu Apr  4 05:31:35 1996
//--------------------------------------------------------------------

#ifndef GDS_ARC_HEADER
#define GDS_ARC_HEADER

#include <iostream>
#include <Tools/Math.h>
#include <GRN/WcsPoint.h>

class Reader;

class GDS_Arc
{
   public:

      GDS_Arc() { }
      ~GDS_Arc() { }

      void read(Reader& is);
      void init(double bulge, double x, double y);
	  void print(std::ostream &os = std::cout);

      inline int operator == (GDS_Arc &other);
      
      inline float bulge() { return bulge_; }
      inline float x() { return xposition_; }
      inline float y() { return yposition_; }

	  WcsPoint point() {
		 return WcsPoint(xposition_, yposition_);
	  }

   protected:

      float bulge_;
      float xposition_, yposition_;
};


int GDS_Arc::operator == (GDS_Arc &other) {
   return (AproxEqual(xposition_, other.xposition_) &&
	   AproxEqual(yposition_, other.yposition_));
}

#endif
