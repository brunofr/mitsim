//-*-c++-*------------------------------------------------------------
// NAME: GDS Objects
// AUTH: Qi Yang
// FILE: GDS_Object.h
// DATE: Wed Apr  3 23:26:59 1996
//--------------------------------------------------------------------

#ifndef GDS_OBJECT_HEADER
#define GDS_OBJECT_HEADER

#include <vector>
#include <iostream>
#include <Xmw/XmwColor.h>

class GDS_Block;
class DRN_DrawingArea;
class Reader;
class WcsPoint;

class GDS_Object
{
   public:

      GDS_Object();
      GDS_Object(const char *name);
      ~GDS_Object();

	  void add(GDS_Block * block);
	  void add(const WcsPoint &p, const char *text);

	  void color(Pixel c) { color_ = c; }
	  void fill(short int f) { fill_ = f; }
      void read(Reader& is);
	  void print(std::ostream &os = std::cout);
      int nBlocks() { return blocks_.size(); }
	  GDS_Block *block(int i) { return blocks_[i]; }
      int calcStyles(const char *style);
      Pixel color(int);

      int isVisible(DRN_DrawingArea *);
      void draw(DRN_DrawingArea *);

      float min_x() { return min_x_; }
      float min_y() { return min_y_; }
      float max_x() { return max_x_; }
      float max_y() { return max_y_; }
      
   private:

      char * name_;

      std::vector<GDS_Block *> blocks_;

      short int fill_;
      Pixel color_;

      float min_x_, min_y_, max_x_, max_y_;
};

#endif
