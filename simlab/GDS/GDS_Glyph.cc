//-*-c++-*------------------------------------------------------------
// NAME: GDS Glyph
// AUTH: Qi Yang
// FILE: GDS_Glyph.C
// DATE: Thu Apr  4 05:55:08 1996
//--------------------------------------------------------------------

#include <fstream>

#include "GDS_Glyph.h"
#include "GDS_Object.h"

#include <Tools/Reader.h>
#include <Tools/ToolKit.h>
#include <IO/Exception.h>
#include <DRN/DRN_DrawingArea.h>
#include <DRN/DRN_Interface.h>
using namespace std;

const char begin_token = '{';
const char end_token = '}';
const char * name_token = "feature-name";
const char * gb_token = "graphic-block";
const char * tb_token = "text-block";


GDS_Glyph::GDS_Glyph(const char* filename, float minscale, float maxscale)
   : name_(Copy(filename)),
     min_scale_(minscale),
     max_scale_(maxscale),
     min_x_(FLT_INF),
     min_y_(FLT_INF),
     max_x_(-FLT_INF),
     max_y_(-FLT_INF),
     state_(0)
{
}


GDS_Glyph::GDS_Glyph()
   : name_(NULL),
     min_scale_(0.0),
     max_scale_(FLT_INF),
     min_x_(FLT_INF),
     min_y_(FLT_INF),
     max_x_(-FLT_INF),
     max_y_(-FLT_INF),
     state_(0)
{
}


GDS_Glyph::~GDS_Glyph()
{
   if (name_) delete [] name_;
}


void GDS_Glyph::name(char *name)
{
   name_ = Copy(name);
}


void GDS_Glyph::load()
{
   const int len = 256;
   char buffer[len];
   setState(STATE_LOADED);

   if (!ToolKit::isValidInputFilename(name_)) return;

   const char *filename = ToolKit::infile(name_);

   if (!ToolKit::fileExists(filename)) {
      cerr << "Error:: Cannot find GDS file <"
		   << filename << ">." << endl; 
      return;
   }

   Reader is(filename);
   if (ToolKit::verbose()) {
      cout << "Loading GDS file <"
		   << is.name() << "> ..." << endl;
   }
   int nblks = 0;
   for (is.eatwhite(); !is.eof(); is.eatwhite()) {
      is >> buffer;
      if (Cmp(buffer, name_token) == 0) {
		 GDS_Object* obj = new GDS_Object;
		 obj->read(is);
		 nblks += obj->nBlocks();
		 add(obj);
      }
   }

   is.close();
   if (objects_.size() <= 0) {
      cerr << "Error:: No objects loaded in GDS file <"
		   << is.name() << ">." << endl;
      return;
   }
   if (ToolKit::verbose()) {
      cerr << objects_.size()
		   << " objects and " << nblks
		   << " blocks loaded from GDS file <"
		   << is.name() << ">." << endl;
   }
}

void GDS_Glyph::print(ostream &os)
{
   for (int i = 0; i < objects_.size(); i ++) {
	  objects_[i]->print(os);
   }
}

void GDS_Glyph::save()
{
   if (!state(STATE_MODIFIED)) return;
   const char *filename = ToolKit::infile(name_);
   ofstream os(filename);
   if (os.good()) {
	  print(os);
	  os.close();
   } else {
	  theDrnInterface->outputFileError(filename);
   }
}

void GDS_Glyph::add(GDS_Object *obj)
{
   objects_.push_back(obj);
   min_x_ = Min(min_x_, obj->min_x());
   max_x_ = Max(max_x_, obj->max_x());
   min_y_ = Min(min_y_, obj->min_y());
   max_y_ = Max(max_y_, obj->max_y());
}

void GDS_Glyph::add(const WcsPoint& p, const char *text)
{
   GDS_Object* obj;
   if (objects_.size() > 0) {
	  obj = objects_[objects_.size()-1];
	  obj->add(p, text);
   } else {
	  obj = new GDS_Object("WKLAYER:LABEL:TEXT/14");
	  obj->add(p, text);
	  add(obj);
   }
}

int
GDS_Glyph::isVisible(DRN_DrawingArea *area)
{
   return (min_x() <= area->right() &&
		   max_x() >= area->left() &&
		   min_y() <= area->top() &&
		   max_y() >= area->bottom());
}


void
GDS_Glyph::draw(DRN_DrawingArea *area)
{
   if (area->meter2Pixels() < min_scale_ ||
       area->meter2Pixels() > max_scale_) return;

   if (!state(STATE_LOADED)) {
      load();
   }
   if (state(STATE_LOADED) && isVisible(area)) {
      for (int i = 0; i < objects_.size(); i ++) {
		 objects_[i]->draw(area);
      }
   }
}
