//-*-c++-*------------------------------------------------------------
// NAME: GDS Block -- Elements for GDS_Object
// AUTH: Qi Yang
// FILE: GDS_Block.C
// DATE: Thu Apr  4 06:00:05 1996
//--------------------------------------------------------------------

#include "GDS_Block.h"
#include "GDS_Glyph.h"

#include <Tools/Reader.h>
#include <Tools/ToolKit.h>
#include <IO/Exception.h>
#include <GRN/RN_WorldSpace.h>
#include <GRN/RN_Arc.h>
#include <DRN/DRN_DrawingArea.h>

const int NUM_POINTS_PER_ARC = 10;
const double POLYGON_ACCURACY = 1.0; // in pixels

using namespace std;

// Check the boundary to find if the block is visible in the area

int
GDS_Block::isVisible(DRN_DrawingArea *area)
{
   return (min_x() <= area->right() &&
		   max_x() >= area->left() &&
		   min_y() <= area->top() &&
		   max_y() >= area->bottom());
}


//--------------------------------------------------------------------
// CLASS NAME: GDS_TextBlock
//--------------------------------------------------------------------

void GDS_TextBlock::read(Reader& is)
{
   const int len = 256;
   char	buffer[len];
   double x, y;
   is >> x >> y;
   is.eatwhite();
   is.getline(buffer, len);
   init(x, y, buffer);
}

void GDS_TextBlock::print(ostream& os)
{
   WcsPoint p = theWorldSpace->databasePoint(xposition_, yposition_);
   os << "\t" << tb_token
	  << "\t" << p.x()
	  << "\t" << p.y()
	  << "\t" << text_
	  << endl;
}

void GDS_TextBlock::init(double x, double y, const char *text)
{
   WcsPoint p = theWorldSpace->worldSpacePoint(x, y);
   xposition_ = p.x();
   yposition_ = p.y();
   text_ = Copy(text);
}


void
GDS_TextBlock::draw(DRN_DrawingArea *area)
{
   static XmwPoint point;

   if (!text_ || !isVisible(area)) return;

   if (area->world2Window(WcsPoint(xposition_, yposition_),
						  point)) {
      area->drawCString(text_, point);
   }
}



//--------------------------------------------------------------------
// CLASS NAME: GDS_GraphicBlock
//--------------------------------------------------------------------

GDS_GraphicBlock::GDS_GraphicBlock()
   : GDS_Block(),
     min_x_(FLT_INF), min_y_(FLT_INF),
     max_x_(-FLT_INF), max_y_(-FLT_INF)
{
}


void GDS_GraphicBlock::read(Reader& is)
{
   if (!is.findToken(begin_token)) theException->exit(0);

   GDS_Arc *arc_prev = NULL;
   while (!is.eof() && is.peek() != end_token) {
      GDS_Arc *arc = new GDS_Arc;
      arc->read(is);
      if (arc_prev == NULL) {
		 arcs_.push_back(arc);
		 min_x_ = arc->x();
		 max_x_ = arc->x();
		 min_y_ = arc->y();
		 max_y_ = arc->y();
		 arc_prev = arc;
      } else if (*arc == *arc_prev) { // skip if two points are very close
		 delete arc;
      } else {
		 arcs_.push_back(arc);
		 min_x_ = Min(min_x_, arc->x());
		 max_x_ = Max(max_x_, arc->x());
		 min_y_ = Min(min_y_, arc->y());
		 max_y_ = Max(max_y_, arc->y());
		 arc_prev = arc;
      }
   }

   if (!is.findToken(end_token)) theException->exit(0);
}

void GDS_GraphicBlock::print(ostream &os)
{
   os << "\t" << gb_token << endl
	  << "\t" << begin_token << endl;
   for (int i = 0; i < arcs_.size(); i ++) {
	  arcs_[i]->print(os);
   }
   os << "\t" << end_token << endl;
}

void GDS_GraphicBlock::draw(DRN_DrawingArea *area)
{
   if (!isVisible(area)) return;
   GDS_Arc *p1, *p2;
   for (int i = 1; i < arcs_.size(); i ++) {
	  p1 = arcs_[i-1];
	  p2 = arcs_[i];
	  RN_Arc rn_arc(WcsPoint(p1->x(), p1->y()),
					WcsPoint(p2->x(), p2->y()),
					p1->bulge());
      area->draw(rn_arc);
   }
}


//--------------------------------------------------------------------
// CLASS NAME: GDS_GraphicSolidBlock
//--------------------------------------------------------------------

GDS_GraphicSolidBlock::GDS_GraphicSolidBlock()
   : GDS_GraphicBlock(),
     xpts_(NULL),
     nMaxPoints_(0),
     nPoints_(0)
{
}


GDS_GraphicSolidBlock::~GDS_GraphicSolidBlock()
{
   deletePoints();
}


void
GDS_GraphicSolidBlock::draw(DRN_DrawingArea *area)
{
   if (!isVisible(area)) return;

   if (arcs_.size() < 3) {
      GDS_GraphicBlock::draw(area);
      return;
   }

   createPoints();

   GDS_Arc *p1, *p2;
   for (int i = 1; i < arcs_.size(); i ++) {
	  p1 = arcs_[i-1];
	  p2 = arcs_[i];
	  RN_Arc rn_arc(WcsPoint(p1->x(), p1->y()),
					WcsPoint(p2->x(), p2->y()),
					p1->bulge());
	  addPoints(area, rn_arc);
   }
   checkSpace(nPoints_ + 1);
   setPoint(area, arcs_[0]->point());

   if (nPoints_) {
      area->XmwDrawingArea::drawFilledPolygon(xpts_, nPoints_);
   }

   deletePoints();
}


void
GDS_GraphicSolidBlock::createPoints()
{
   deletePoints();

   // Predicted number of points in this polygon
   
   nMaxPoints_ = arcs_.size() * NUM_POINTS_PER_ARC;
   xpts_ = new XPoint [nMaxPoints_];
   nPoints_ = 0;
}


void
GDS_GraphicSolidBlock::deletePoints()
{
   if (xpts_) delete [] xpts_;
   xpts_ = NULL;
   nMaxPoints_ = nPoints_ = 0;
}


void
GDS_GraphicSolidBlock::addPoints(DRN_DrawingArea *area, RN_Arc &arc)
{
   if (arc.isArc()) {		// curve
      WcsPoint p;
      double shell = POLYGON_ACCURACY / area->meter2Pixels();
      double step = acos(1.0 - shell / arc.radius());
      int n = (int) ceil(Abs(arc.arcAngle() / step));
      int m = (int) ceil(arc.length() / shell);
      if (n < 1) {
		 n = 1;
      } else if (n > m) {
		 n = m;
      }
      checkSpace(n);
      step = 1.0 / n;
      for (int i = n; i > 0; i --) {
		 setPoint(area, arc.intermediatePoint(i * step));
      }
   } else {			// straight line
      checkSpace(2);
      setPoint(area, arc.startPnt());
      setPoint(area, arc.endPnt());
   }
}



// Translate and set the next point on the polygon

void
GDS_GraphicSolidBlock::setPoint(DRN_DrawingArea *area, const WcsPoint &pt)
{
   WcsPoint p = area->world2Window(pt);
   XPoint &xp = xpts_[nPoints_];
   xp.x = (int)p.x();
   xp.y = (int)p.y();
   nPoints_ ++;
}


// Make sure the remaining space is enough for additional n points.

void
GDS_GraphicSolidBlock::checkSpace(int n)
{
   if (nPoints_ + n <= nMaxPoints_) return;
   
   // No enough space
   
   XPoint *old = xpts_;
   nMaxPoints_ = nPoints_ + n;
   xpts_ = new XPoint[nMaxPoints_];

   memcpy(xpts_, old, nPoints_*sizeof(XPoint));
   delete [] old;
}
