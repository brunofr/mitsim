//-*-c++-*------------------------------------------------------------
// NAME: GDS Object -- elements for GDS_Glyph
// AUTH: Qi Yang
// FILE: GDS_Object.C
// DATE: Thu Apr  4 06:02:59 1996
//--------------------------------------------------------------------

#include <cstring>
#include <cstdlib>

#include "GDS_Block.h"
#include "GDS_Object.h"
#include "GDS_Glyph.h"

#include <Tools/Reader.h>
#include <Tools/ToolKit.h>
#include <Tools/Math.h>
#include <IO/Exception.h>
#include <DRN/DRN_DrawingArea.h>
using namespace std;

GDS_Object::GDS_Object() :
   name_(0),
   min_x_(FLT_INF), min_y_(FLT_INF),
   max_x_(-FLT_INF), max_y_(-FLT_INF),
   fill_(0), color_(0)
{
}

GDS_Object::GDS_Object(const char *n) :
   name_(::Copy(n)),
   min_x_(FLT_INF), min_y_(FLT_INF),
   max_x_(-FLT_INF), max_y_(-FLT_INF),
   fill_(0), color_(1)
{
   if (calcStyles(name_) < 0) {
	  theException->exit();
   }
}

GDS_Object::~GDS_Object()
{
   if (name_) free(name_);
   for (int i = 0; i < nBlocks(); i ++) {
	  delete blocks_[i];
   }
}

void GDS_Object::add(GDS_Block *block)
{
   min_x_ = Min(min_x_, block->min_x());
   max_x_ = Max(max_x_, block->max_x());
   min_y_ = Min(min_y_, block->min_y());
   max_y_ = Max(max_y_, block->max_y());
   blocks_.push_back(block);
}

void GDS_Object::read(Reader& is)
{
   const int len = 256;
   char buffer[len];

   is >> buffer;
   name_ = Copy(buffer);

   if (calcStyles(name_) < 0) {
	  is.reference();
	  theException->exit();
   }

   if (!is.findToken(begin_token)) theException->exit(0);

   GDS_Block * block;

   while (!is.eof() && is.peek() != end_token) {
      is >> buffer;
      if (Cmp(buffer, tb_token) == 0) {
		 block = new GDS_TextBlock;
      } else if (Cmp(buffer, gb_token) == 0) {
		 if (fill_) {
			block = new GDS_GraphicSolidBlock;
		 } else {
			block = new GDS_GraphicBlock;
		 }
      } else {
		 cerr << "Error:: Unknown GDS object. ";
		 is.reference();
		 theException->exit(0);
      }
      block->read(is);
	  add(block);
   }

   if (!is.findToken(end_token)) theException->exit(0);

   return;
}


void GDS_Object::print(ostream &os)
{
   os << name_token << " " << name_ << '\n'
	  << begin_token << '\n';
   for(int i = 0; i < nBlocks(); i ++) {
	  block(i)->print(os);
   }
   os << end_token << endl;
}

void GDS_Object::add(const WcsPoint& p, const char *text)
{
   GDS_Block* blk = new GDS_TextBlock(p.x(), p.y(), text);
   add(blk);
}

int GDS_Object::calcStyles(const char *style)
{
   char *buf = strdup(style);
 
   char *namestr, *stylestr, *colorstr;
   namestr = strchr(buf, ':');
   if (namestr == NULL) {
      cerr << "Error:: GDS object no name. ";
	  goto error;
   }
   *namestr = '\0'; namestr ++;

   stylestr = strchr(namestr, ':');
   if (stylestr == NULL) {
      cerr << "Error:: GDS object no style. ";
      goto error;
   }
   *stylestr = '\0'; stylestr ++;

   colorstr = strchr(stylestr, '/');
   if (colorstr == NULL) {
      cerr << "Error:: GDS object no color. ";
      goto error;
   }
   *colorstr = '\0'; colorstr ++;

   if (Cmp(stylestr, "WATER") == 0) {
      fill_ = 1;
      color_ = theColorTable->water();
   } else if (Cmp(stylestr, "BUILDING") == 0) {
      fill_ = 1;
	  color_ = color(atoi(colorstr));
   } else if (Cmp(stylestr, "ROAD") == 0) {
      fill_ = 1;
	  color_ = color(atoi(colorstr));
   } else {
      fill_ = 0;
	  color_ = color(atoi(colorstr));
   }

   free(buf);
   return 0;

 error:

   free(buf);
   return -1;
}


Pixel GDS_Object::color(int c)
{
   c %= theColorTable->nCols();
   return theColorTable->color(c);
}


int GDS_Object::isVisible(DRN_DrawingArea *area)
{
   return (min_x() <= area->right() &&
		   max_x() >= area->left() &&
		   min_y() <= area->top() &&
		   max_y() >= area->bottom());
}

void GDS_Object::draw(DRN_DrawingArea * area)
{
   if (!isVisible(area)) return;
   area->foreground(color_);
   area->setCopyMode();
   for (int i = 0; i < blocks_.size(); i ++) {
      blocks_[i]->draw(area);
   }
}
