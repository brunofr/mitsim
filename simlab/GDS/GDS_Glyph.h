//-*-c++-*------------------------------------------------------------
// NAME: GDS Data
// AUTH: Qi Yang
// FILE: GDS_Glyph.h
// DATE: Wed Apr  3 23:05:52 1996
//--------------------------------------------------------------------

#ifndef GDS_GLYPH_HEADER
#define GDS_GLYPH_HEADER

#include <vector>
#include <iostream>
#include <Tools/Math.h>

extern const char begin_token;
extern const char end_token;
extern const char * name_token;
extern const char * gb_token;
extern const char * tb_token;

class GDS_Object;
class DRN_DrawingArea;
class WcsPoint;

class GDS_Glyph
{
   protected:

      char *name_;
      float min_scale_, max_scale_;
      unsigned int state_;
      std::vector<GDS_Object *> objects_;
      float min_x_, min_y_, max_x_, max_y_;

   public:

      GDS_Glyph(const char* filename, float min_scale, float max_scale);
      GDS_Glyph();

      ~GDS_Glyph();
      
      // state manipulators

	  enum {
		 STATE_LOADED	= 0x1,
		 STATE_MODIFIED	= 0x2
	  };

      inline unsigned int state(unsigned int mask = 0xFFFF) {
		 return (state_ & mask);
      }
      inline void setState(unsigned int s) {
		 state_ |= s;
      }
      inline void unsetState(unsigned int s) {
		 state_ &= ~s;
      }

	  char* name() { return name_; }
	  float minScale() { return min_scale_; }
	  float maxScale() { return max_scale_; }

      void name(char *name);
      void setScale(float minscale, float maxscale = FLT_INF) {
		 min_scale_ = minscale;
		 max_scale_ = maxscale;
      }

      void load();
	  unsigned int isLoaded() { return state_ & STATE_LOADED; }
	  void print(std::ostream &os = std::cout);
	  void save();

	  void add(const WcsPoint& p, const char *text);
	  void add(GDS_Object *obj);

      int isVisible(DRN_DrawingArea *);
      void draw(DRN_DrawingArea *);

      float min_x() { return min_x_; }
      float min_y() { return min_y_; }
      float max_x() { return max_x_; }
      float max_y() { return max_y_; }
};

#endif

