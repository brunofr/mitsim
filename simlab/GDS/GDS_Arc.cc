//-*-c++-*------------------------------------------------------------
// NAME: GDS Arc -- Elements for GDS_GraphicsBlock
// AUTH: Qi Yang
// FILE: GDS_Arc.C
// DATE: Thu Apr  4 06:06:06 1996
//--------------------------------------------------------------------

#include "GDS_Arc.h"

#include <Tools/Reader.h>
#include <GRN/RN_WorldSpace.h>

using namespace std;

void
GDS_Arc::read(Reader& is)
{
   double b, x, y;
   is >> b >> x >> y;
   init(b, x, y);
}

void GDS_Arc::init(double b, double x, double y)
{
   bulge_ = b;
   WcsPoint p = theWorldSpace->worldSpacePoint(x, y);
   xposition_ = p.x();
   yposition_ = p.y();
}


void GDS_Arc::print(ostream &os)
{
   WcsPoint p = theWorldSpace->databasePoint(xposition_, yposition_);
   os << "\t"
	  << "\t" << bulge_
	  << "\t" << p.x()
	  << "\t" << p.y()
	  << endl;
}


