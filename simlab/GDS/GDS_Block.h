//-*-c++-*------------------------------------------------------------
// NAME: GDS Blocks -- Elements for GDS_Object
// AUTH: Qi Yang
// FILE: GDS_Block.h
// DATE: Wed Apr  3 23:38:09 1996
//--------------------------------------------------------------------

#ifndef GDS_BLOCK_HEADER
#define GDS_BLOCK_HEADER

#include <X11/Intrinsic.h>

#include <iostream>
#include <vector>

#include "GDS_Arc.h"

class Reader;
class WcsPoint;
class RN_Arc;
class DRN_DrawingArea;

//--------------------------------------------------------------------
// CLASS NAME: GDS_Block
//--------------------------------------------------------------------

class GDS_Block
{
   public:

      GDS_Block() { }
      virtual ~GDS_Block() { }

      // All these functions need to be overloaded

      virtual void read(Reader &) = 0;
	  virtual void print(std::ostream &os = std::cout) = 0;
      virtual void draw(DRN_DrawingArea *) = 0;

      virtual float min_x() = 0;
      virtual float min_y() = 0;
      virtual float max_x() = 0;
      virtual float max_y() = 0;
      
      virtual int isVisible(DRN_DrawingArea *);
};


//--------------------------------------------------------------------
// CLASS NAME: GDS_TextBlock
//--------------------------------------------------------------------

class GDS_TextBlock : public GDS_Block
{
   public:

      GDS_TextBlock() : GDS_Block(), text_(NULL) { }
	  GDS_TextBlock(double x, double y, const char *text) {
		 init(x, y, text);
	  }
      virtual ~GDS_TextBlock() {
		 if (text_) delete [] text_;
      }

      void read(Reader& is);
	  void print(std::ostream &os = std::cout);
      void init(double x, double y, const char *text);

      void draw(DRN_DrawingArea *);

      inline float min_x() { return xposition_; }
      inline float min_y() { return yposition_; }
      inline float max_x() { return xposition_; }
      inline float max_y() { return yposition_; }

   protected:

      char	*text_;
      float	xposition_;
      float	yposition_;
};


//--------------------------------------------------------------------
// CLASS NAME: GDS_GraphicBlock
//--------------------------------------------------------------------

class GDS_GraphicBlock : public GDS_Block
{
   public:

      GDS_GraphicBlock();

      virtual ~GDS_GraphicBlock() { }

      void read(Reader& is);
	  void print(std::ostream &os = std::cout);

      void draw(DRN_DrawingArea *);

      float min_x() { return min_x_; }
      float min_y() { return min_y_; }
      float max_x() { return max_x_; }
      float max_y() { return max_y_; }

   protected:

      std::vector<GDS_Arc *> arcs_;
      float min_x_, min_y_, max_x_, max_y_;
};

//--------------------------------------------------------------------
// CLASS NAME: GDS_GraphicBlock
//--------------------------------------------------------------------

class GDS_GraphicSolidBlock : public GDS_GraphicBlock
{
   public:

      GDS_GraphicSolidBlock();
      ~GDS_GraphicSolidBlock();

   protected:

      XPoint *xpts_;			// vertices in window
      int nMaxPoints_;			// space in xpts_
      int nPoints_;				// used space in xpts_

   private:

      void draw(DRN_DrawingArea *);

      void createPoints();
      void deletePoints();
      void addPoints(DRN_DrawingArea *, RN_Arc&);
      void setPoint(DRN_DrawingArea *, const WcsPoint&);
      void checkSpace(int n);
};

#endif
