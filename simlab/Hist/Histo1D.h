// $Id: Histo1D.h,v 1.1.1.1 1997/10/15 00:58:16 qiyang Exp $
//
// Copyright SAMTECH - LTAS ......................... BOSS VERSION 1.4-00
// Routine manager: "Burton"                               Date: 19-04-96
//
// Routine history (creation,modification,correction)
// +--------------------------------------------------------------------+
// !Programmer ! Comments                              ! Date   !Version!
// +-----------!---------------------------------------!--------!-------+
// ! Burton    ! Creation                              !19-04-96! 1.4-00!
// +--------------------------------------------------------------------+
//----------------------------------------------------------------------+
//                                                                      !
// - CLASS NAME  : Histo1D                                              !
//                                                                      !
// - FILE        : [X] Interface      (.h)                              !
//                 [ ] Implementation (.cc)                             !
//                                                                      !
// - CLASS TYPE  : [X] Abstract      [ ] Representation                 !
//                 [X] Base          [ ] Derived                        !
//                 [ ] Template                                         !
//                                                                      !
// - DERIVATION  : [ ] Public    : .................................... !
//                 [ ] Protected : .................................... !
//                 [ ] Private   : .................................... !
//                 [ ] Virtual   : [ ] All                              !
//                                 [ ] : .............................. !
//                                                                      !
// - LAYERED     : [ ] with class : ................................... !
//                                                                      !
// - FRIENDS     :                                                      !
//                                                                      !
// - EXTERNALS   :                                                      !
//                                                                      !
// - C++         : [X] 2.0                                              !
//                 [ ] 2.1  (embedded types)                            !
//                 [ ] 3.0  (templates & exceptions)                    !
//                                                                      !
// - CALLS TO    : [ ] I/O streams    [ ] X11                           !
//                 [ ] strings        [ ] X11 Extensions                !
//                 [ ] math           [ ] X11 Toolkit                   !
//                 [ ] system         [X] OSF/MOTIF (Xm library)        !
//                 [ ] GNU library    [X] Xmt - Xpm - other X libraries !
//                                                                      !
// - LIBRARY     : [X][ ] Util        [ ][X] MotifUI                    !
//   [use][in]     [ ][ ] Param       [ ][ ] ParamUI                    !
//                 [ ][ ] Cmd         [ ][ ] CmdUI                      !
//                 [ ][ ] Ctrl        [ ][ ] CtrlUI                     !
//                 [ ][ ] Stat        [ ][ ] StatUI                     !
//                 [ ][ ] Opti        [ ][ ] OptiUI                     !
//                 [ ][ ] Iter        [ ][ ] IterUI                     !
//                 [ ][ ] Exper       [ ][ ] ExperUI                    !
//                 [ ][ ] Graph       [ ][ ] GraphUI                    !
//                                                                      !
// - DESCRIPTION : provide a pulldown menu giving a set of available    !
//                 options.                                             !
//                                                                      !
//----------------------------------------------------------------------+

#ifndef Histo1D_h_
#define Histo1D_h_
                                    // includes from 'MotifUI' library
#include "UIWidget.h"
#include "Custom.h"
                                    // includes from 'Util' library
#include "ValueDef.h"
#include "BaseClass.h"

class Histo1D : public UIWidget {

protected:

  BaseClass_data_w(int,barSep)
  BaseClass_data_w(value_t,min)
  BaseClass_data_w(value_t,max)
  BaseClass_data_w(value_t,ave)
  BaseClass_dataPtr_w(ValueDef,vdef)

public:

  Histo1D( Widget, const String,
	   ArgList  args = NULL,
	   Cardinal numArgs = 0 );

  virtual ~Histo1D();

  void setBarSep( int );
  void setXAxisLabel( const char* );
  void setYAxisLabel( const char* );
  void display( int numBars_ = 7,
		bool automatic_ = true,
                ValueDef *vdef_ = 0  );

  virtual const char *const className() const { return "Histo1D"; }
};
                                    // declare a creator for automatic
                                    // widget creation with Xmt resources.
Customdeclare(Histo1D)

#endif

// **********************************************************************
// **----------------- Do not modify below this line ------------------**
// **********************************************************************
// $Log: Histo1D.h,v $
// Revision 1.1.1.1  1997/10/15 00:58:16  qiyang
// Creation
//
// Revision 1.1  1996/04/23 13:53:47  burton
// Creation.
//
