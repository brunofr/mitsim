// $Id: Histo1D.cc,v 1.1.1.1 1997/10/15 00:58:16 qiyang Exp $
//
// Copyright SAMTECH - LTAS ......................... BOSS VERSION 1.4-00
// Routine manager: "Burton"                               Date: 19-04-96
//
// Routine history (creation,modification,correction)
// +--------------------------------------------------------------------+
// !Programmer ! Comments                              ! Date   !Version!
// +-----------!---------------------------------------!--------!-------+
// ! Burton    ! Creation                              !19-04-96! 1.4-00!
// +--------------------------------------------------------------------+
//----------------------------------------------------------------------+
//                                                                      !
// - CLASS NAME  : Histo1D                                              !
//                                                                      !
// - DESCRIPTION : see file "Histo1D.h"                                 !
//                                                                      !
// - FILE        : [ ] Interface      (.h)                              !
//                 [X] Implementation (.cc)                             !
//                                                                      !
// - CLASS TYPE  : [X] Abstract      [ ] Representation                 !
//                 [X] Base          [ ] Derived                        !
//                 [ ] Template                                         !
//                                                                      !
// - DERIVATION  : [ ] Public    : .................................... !
//                 [ ] Protected : .................................... !
//                 [ ] Private   : .................................... !
//                 [ ] Virtual   : [ ] All                              !
//                                 [ ] : .............................. !
//                                                                      !
// - LAYERED     : [ ] with class : ................................... !
//                                                                      !
// - FRIENDS     :                                                      !
//                                                                      !
// - EXTERNALS   :                                                      !
//                                                                      !
// - C++         : [X] 2.0                                              !
//                 [ ] 2.1  (embedded types)                            !
//                 [ ] 3.0  (templates & exceptions)                    !
//                                                                      !
// - CALLS TO    : [ ] I/O streams    [ ] X11                           !
//                 [ ] strings        [ ] X11 Extensions                !
//                 [ ] math           [ ] X11 Toolkit                   !
//                 [ ] system         [X] OSF/MOTIF (Xm library)        !
//                 [ ] GNU library    [X] Xmt - Xpm - other X libraries !
//                                                                      !
// - LIBRARY     : [ ][ ] Util        [X][X] MotifUI                    !
//   [use][in]     [ ][ ] Param       [ ][ ] ParamUI                    !
//                 [ ][ ] Cmd         [ ][ ] CmdUI                      !
//                 [ ][ ] Ctrl        [ ][ ] CtrlUI                     !
//                 [ ][ ] Study       [ ][ ] StudyUI                    !
//                 [ ][ ] Stat        [ ][ ] StatUI                     !
//                 [ ][ ] Opti        [ ][ ] OptiUI                     !
//                 [ ][ ] Iter        [ ][ ] IterUI                     !
//                 [ ][ ] Exper       [ ][ ] ExperUI                    !
//                 [ ][ ] Graph       [ ][ ] GraphUI                    !
//                 [ ][ ] Drive       [ ][ ] OpSys                      !
//                 [ ][ ] Fct                                           !
//                                                                      !
//----------------------------------------------------------------------+

#include "Histo1D.h"
                                    // includes from 'MotifUI' library
                                    // includes from Xmt - Xpm libraries
#include <Xmt/Xmt.h>
                                    // includes from third party widgets
extern "C" {
#include <PlotW/H1D.h>
}
                                    // includes from C library
#include <math.h>
#include <assert.h>                //   compile with -DNDEBUG to cancel
                                    //   assertation calls.

//----------------------------------------------------------------------+

                                    // define the ListItem automatic
                                    // creator functions.
Customdefine(Histo1D)

//----------------------------------------------------------------------+

Histo1D::Histo1D( Widget       parent_,
		  const String name_,
		  ArgList      args_,
		  Cardinal     numArgs_ )
  : UIWidget( name_ ) {


  _w = XtVaCreateWidget( name_,
			 h1DWidgetClass, parent_,
			 args_, numArgs_ );

  installDestroyHandler();

  vdefSet( 0 );
}

//----------------------------------------------------------------------+

Histo1D::~Histo1D() {

}

//----------------------------------------------------------------------+

void
Histo1D::setBarSep( int pcent_ ) {

  barSepSet( pcent_ );
  XtVaSetValues( _w,
		 XmNbarSeparation, pcent_,
		 NULL );
}

//----------------------------------------------------------------------+

void
Histo1D::setXAxisLabel( const char* s_ ) {

  XmString s = XmtCreateXmString( s_ );
  XtVaSetValues( _w,
		 XmNxAxisLabel, s,
		 NULL );
  XmStringFree( s );
}

//----------------------------------------------------------------------+

void
Histo1D::setYAxisLabel( const char* s_ ) {

  XmString s = XmtCreateXmString( s_ );
  XtVaSetValues( _w,
		 XmNyAxisLabel, s,
		 NULL );
  XmStringFree( s );
}

//----------------------------------------------------------------------+

void
Histo1D::display( int numBars_,
		  bool automatic_,
		  ValueDef *vdef_ ) {

  if ( vdef_ ) vdefSet( vdef_ );
  if ( !_vdef || numBars_ <= 0 || !_w ) return;
  
  int i, n = _vdef->valuesNum();
                                    // computes automatically min, max and ave
  if ( automatic_ ) {
    _min = _max = ( n > 0 ) ? _vdef->values(0) : 0.0;
    _ave = 0;
    for ( i = 0; i < n; i++ ) {
      value_t val = _vdef->values( i );
      _ave += val;
      if ( val > _max ) _max = val;
      else if ( val < _min ) _min = val;
    }
    _ave /= ( n > 0 ) ? (value_t) n : 1.0;
  }

  float *bins = (float*) XtMalloc( sizeof(float) * numBars_ );

                                    // count the number of values for each
                                    // slice, that is, build the histogram bins

  for ( i = 0; i < numBars_; ++i ) bins[i] = 0.0;
  value_t slice = ( _max - _min ) / numBars_;
  for ( i = 0; i < n; ++i ) {
    value_t val = _vdef->values( i );
    value_t colval = (slice > 0.0) ? (val - _min)/slice : 0.0;
    int col = (int) floor( colval );
    if ( col >= numBars_ ) col = numBars_ - 1;
    bins[ col ] += 1.0;
  }
  value_t dist = ( _ave-_min > _max-_ave ) ? _ave-_min : _max-_ave;

                                    // display the histogram.

  H1DSetContents( _w, _ave-dist, _ave+dist, numBars_,
		  bins, NULL, NULL,
		  H1D_RESCALE);

  XtFree( (char*) bins );
}

//----------------------------------------------------------------------+

// **********************************************************************
// **----------------- Do not modify below this line ------------------**
// **********************************************************************
// $Log: Histo1D.cc,v $
// Revision 1.1.1.1  1997/10/15 00:58:16  qiyang
// Creation
//
// Revision 1.2  1997/02/18 16:56:55  jmc
// "Review integer declaration when used at several levels."
//
// Revision 1.1  1996/04/23 13:53:29  burton
// Creation.
//
