// $Id: Histo2D.h,v 1.1.1.1 1997/10/15 00:58:16 qiyang Exp $
//
// Copyright SAMTECH - LTAS ......................... BOSS VERSION 1.4-00
// Routine manager: "Burton"                               Date: 19-04-96
//
// Routine history (creation,modification,correction)
// +--------------------------------------------------------------------+
// !Programmer ! Comments                              ! Date   !Version!
// +-----------!---------------------------------------!--------!-------+
// ! Burton    ! Creation                              !19-04-96! 1.4-00!
// +--------------------------------------------------------------------+
//----------------------------------------------------------------------+
//                                                                      !
// - CLASS NAME  : Histo2D                                              !
//                                                                      !
// - FILE        : [X] Interface      (.h)                              !
//                 [ ] Implementation (.cc)                             !
//                                                                      !
// - CLASS TYPE  : [X] Abstract      [ ] Representation                 !
//                 [X] Base          [ ] Derived                        !
//                 [ ] Template                                         !
//                                                                      !
// - DERIVATION  : [ ] Public    : .................................... !
//                 [ ] Protected : .................................... !
//                 [ ] Private   : .................................... !
//                 [ ] Virtual   : [ ] All                              !
//                                 [ ] : .............................. !
//                                                                      !
// - LAYERED     : [ ] with class : ................................... !
//                                                                      !
// - FRIENDS     :                                                      !
//                                                                      !
// - EXTERNALS   :                                                      !
//                                                                      !
// - C++         : [X] 2.0                                              !
//                 [ ] 2.1  (embedded types)                            !
//                 [ ] 3.0  (templates & exceptions)                    !
//                                                                      !
// - CALLS TO    : [ ] I/O streams    [ ] X11                           !
//                 [ ] strings        [ ] X11 Extensions                !
//                 [ ] math           [ ] X11 Toolkit                   !
//                 [ ] system         [X] OSF/MOTIF (Xm library)        !
//                 [ ] GNU library    [X] Xmt - Xpm - other X libraries !
//                                                                      !
// - LIBRARY     : [X][ ] Util        [ ][X] MotifUI                    !
//   [use][in]     [ ][ ] Param       [ ][ ] ParamUI                    !
//                 [ ][ ] Cmd         [ ][ ] CmdUI                      !
//                 [ ][ ] Ctrl        [ ][ ] CtrlUI                     !
//                 [ ][ ] Stat        [ ][ ] StatUI                     !
//                 [ ][ ] Opti        [ ][ ] OptiUI                     !
//                 [ ][ ] Iter        [ ][ ] IterUI                     !
//                 [ ][ ] Exper       [ ][ ] ExperUI                    !
//                 [ ][ ] Graph       [ ][ ] GraphUI                    !
//                                                                      !
// - DESCRIPTION : provide a pulldown menu giving a set of available    !
//                 options.                                             !
//                                                                      !
//----------------------------------------------------------------------+

#ifndef Histo2D_h_
#define Histo2D_h_
                                    // includes from 'MotifUI' library
#include "UIWidget.h"
#include "Custom.h"
                                    // includes from 'Util' library
#include "ValueDef.h"
#include "BaseClass.h"
                                    // includes from third party widgets
extern "C" {
#include <PlotW/2DHist.h>
}

class Histo2D : public UIWidget {

protected:

  bool processData( int numBars_ = 7,
		    bool automatic_ = true,
		    ValueDef *xvdef_ = 0,
		    ValueDef *yvdef_ = 0  );
  float *_bins;
  h2DHistSetup _setup;
  double _fi, _psi;
  int _nxr, _nyr;

  BaseClass_data_w(int,barSep)
  BaseClass_data_w(value_t,xmin)
  BaseClass_data_w(value_t,xmax)
  BaseClass_data_w(value_t,xave)
  BaseClass_data_w(value_t,ymin)
  BaseClass_data_w(value_t,ymax)
  BaseClass_data_w(value_t,yave)
  BaseClass_data_w(value_t,xdist)
  BaseClass_data_w(value_t,ydist)
  BaseClass_dataPtr_w(ValueDef,xvdef)
  BaseClass_dataPtr_w(ValueDef,yvdef)

public:

  Histo2D( Widget, const String,
	   ArgList  args = NULL,
	   Cardinal numArgs = 0 );

  virtual ~Histo2D();

  void setXAxisLabel( const char* );
  void setYAxisLabel( const char* );
  void setZAxisLabel( const char* );
  void display( int numBars_ = 7,
		bool automatic_ = true,
                ValueDef *xvdef_ = 0,
                ValueDef *yvdef_ = 0  );
  void update( int numBars_ = 7,
		bool automatic_ = false,
                ValueDef *xvdef_ = 0,
                ValueDef *yvdef_ = 0  );
  void rebin( int numBars_ = 7,
	      bool automatic_ = false,
	      ValueDef *xvdef_ = 0,
	      ValueDef *yvdef_ = 0  );

  virtual const char *const className() const { return "Histo2D"; }
};
                                    // declare a creator for automatic
                                    // widget creation with Xmt resources.
Customdeclare(Histo2D)

#endif

// **********************************************************************
// **----------------- Do not modify below this line ------------------**
// **********************************************************************
// $Log: Histo2D.h,v $
// Revision 1.1.1.1  1997/10/15 00:58:16  qiyang
// Creation
//
// Revision 1.1  1996/04/23 13:52:54  burton
// Creation.
//
