// $Id: Histo2D.cc,v 1.1.1.1 1997/10/15 00:58:16 qiyang Exp $
//
// Copyright SAMTECH - LTAS ......................... BOSS VERSION 1.4-00
// Routine manager: "Burton"                               Date: 19-04-96
//
// Routine history (creation,modification,correction)
// +--------------------------------------------------------------------+
// !Programmer ! Comments                              ! Date   !Version!
// +-----------!---------------------------------------!--------!-------+
// ! Burton    ! Creation                              !19-04-96! 1.4-00!
// +--------------------------------------------------------------------+
//----------------------------------------------------------------------+
//                                                                      !
// - CLASS NAME  : Histo2D                                              !
//                                                                      !
// - DESCRIPTION : see file "Histo2D.h"                                 !
//                                                                      !
// - FILE        : [ ] Interface      (.h)                              !
//                 [X] Implementation (.cc)                             !
//                                                                      !
// - CLASS TYPE  : [X] Abstract      [ ] Representation                 !
//                 [X] Base          [ ] Derived                        !
//                 [ ] Template                                         !
//                                                                      !
// - DERIVATION  : [ ] Public    : .................................... !
//                 [ ] Protected : .................................... !
//                 [ ] Private   : .................................... !
//                 [ ] Virtual   : [ ] All                              !
//                                 [ ] : .............................. !
//                                                                      !
// - LAYERED     : [ ] with class : ................................... !
//                                                                      !
// - FRIENDS     :                                                      !
//                                                                      !
// - EXTERNALS   :                                                      !
//                                                                      !
// - C++         : [X] 2.0                                              !
//                 [ ] 2.1  (embedded types)                            !
//                 [ ] 3.0  (templates & exceptions)                    !
//                                                                      !
// - CALLS TO    : [ ] I/O streams    [ ] X11                           !
//                 [ ] strings        [ ] X11 Extensions                !
//                 [ ] math           [ ] X11 Toolkit                   !
//                 [ ] system         [X] OSF/MOTIF (Xm library)        !
//                 [ ] GNU library    [X] Xmt - Xpm - other X libraries !
//                                                                      !
// - LIBRARY     : [ ][ ] Util        [X][X] MotifUI                    !
//   [use][in]     [ ][ ] Param       [ ][ ] ParamUI                    !
//                 [ ][ ] Cmd         [ ][ ] CmdUI                      !
//                 [ ][ ] Ctrl        [ ][ ] CtrlUI                     !
//                 [ ][ ] Study       [ ][ ] StudyUI                    !
//                 [ ][ ] Stat        [ ][ ] StatUI                     !
//                 [ ][ ] Opti        [ ][ ] OptiUI                     !
//                 [ ][ ] Iter        [ ][ ] IterUI                     !
//                 [ ][ ] Exper       [ ][ ] ExperUI                    !
//                 [ ][ ] Graph       [ ][ ] GraphUI                    !
//                 [ ][ ] Drive       [ ][ ] OpSys                      !
//                 [ ][ ] Fct                                           !
//                                                                      !
//----------------------------------------------------------------------+

#include "Histo2D.h"
                                    // includes from 'MotifUI' library
                                    // includes from Xmt - Xpm libraries
#include <Xmt/Xmt.h>

#include <iostream.h>
                                    // includes from C library
#include <math.h>
#include <assert.h>                //   compile with -DNDEBUG to cancel
                                    //   assertation calls.

//----------------------------------------------------------------------+

                                    // define the ListItem automatic
                                    // creator functions.
Customdefine(Histo2D)

//----------------------------------------------------------------------+

Histo2D::Histo2D( Widget       parent_,
		  const String name_,
		  ArgList      args_,
		  Cardinal     numArgs_ )
  : UIWidget( name_ ) {


  _w = XtVaCreateWidget( name_,
			 hist2DWidgetClass, parent_,
			 args_, numArgs_ );

  installDestroyHandler();

  _setup.nXBins = 0;
  _setup.nYBins = 0;
  _setup.xMin = 0.;
  _setup.xMax = 100.;
  _setup.yMin = 0.;
  _setup.yMax = 100.;
  _setup.xScaleType = H2D_LINEAR;
  _setup.xScaleBase = 0.;
  _setup.yScaleType = H2D_LINEAR;
  _setup.yScaleBase = 0.;
  _setup.xLabel = "x";
  _setup.yLabel = "y";
  _setup.zLabel = "counts";
  _setup.bins = NULL;

  _fi = 4.8;
  _psi = 0.08;

  _nxr = _nyr = 0;
  _bins = (float*) NULL;

  xvdefSet( 0 );
  yvdefSet( 0 );
}

//----------------------------------------------------------------------+

Histo2D::~Histo2D() {

}

//----------------------------------------------------------------------+

void
Histo2D::setXAxisLabel( const char* s_ ) {

  _setup.xLabel = (char*) s_;
}

//----------------------------------------------------------------------+

void
Histo2D::setYAxisLabel( const char* s_ ) {

  _setup.yLabel = (char*) s_;
}

//----------------------------------------------------------------------+

void
Histo2D::setZAxisLabel( const char* s_ ) {

  _setup.zLabel = (char*) s_;
}

//----------------------------------------------------------------------+

bool
Histo2D::processData( int numBars_,
		      bool automatic_,
		      ValueDef *xvdef_,
		      ValueDef *yvdef_ ) {

  if ( xvdef_ ) xvdefSet( xvdef_ );
  if ( yvdef_ ) yvdefSet( yvdef_ );
  if ( !(_xvdef || _yvdef) || numBars_ <= 0 || !_w ) return false;
  
  int i, nx = _xvdef ? _xvdef->valuesNum() : 0;
  int j, ny = _yvdef ? _yvdef->valuesNum() : 0;

                                    // computes automatically min, max and ave
  if ( automatic_ ) {
                                    // first for x values.

    _xmin = _xmax = ( nx > 0 ) ? _xvdef->values(0) : 0.0;
    _xave = 0;
    for ( i = 0; i < nx; i++ ) {
      value_t val = _xvdef->values( i );
      _xave += val;
      if ( val > _xmax ) _xmax = val;
      else if ( val < _xmin ) _xmin = val;
    }
    _xave /= ( nx > 0 ) ? (value_t) nx : 1.0;

                                    // idem for y values.

    _ymin = _ymax = ( ny > 0 ) ? _yvdef->values(0) : 0.0;
    _yave = 0;
    for ( j = 0; j < ny; j++ ) {
      value_t val = _yvdef->values( j );
      _yave += val;
      if ( val > _ymax ) _ymax = val;
      else if ( val < _ymin ) _ymin = val;
    }
    _yave /= ( ny > 0 ) ? (value_t) ny : 1.0;
  }

  _nxr = nx ? numBars_ : 1;
  _nyr = ny ? numBars_ : 1;
  int nn =  _nxr * _nyr;
  _bins = (float*) XtMalloc( sizeof(float) * nn );
  if ( !_bins ) return false;
                                    // count the number of values for each
                                    // slice, that is, build the histogram bins

  for ( i = 0; i < nn; ++i ) _bins[i] = 0.0;

                                    // prepare x bins

  value_t xslice = ( _xmax - _xmin ) / numBars_;
  for ( i = 0; i < nx; ++i ) {
    value_t val = _xvdef->values( i );
    value_t colval = (xslice > 0.0) ? (val - _xmin)/xslice : 0.0;
    int col = (int) floor( colval );
    if ( col >= numBars_ ) col = numBars_ - 1;
    _bins[ col * _nyr ] += 1.0;
  }
  _xdist = ( _xave-_xmin > _xmax-_xave ) ? _xave-_xmin : _xmax-_xave;

                                    // prepare y bins

  value_t yslice = ( _ymax - _ymin ) / numBars_;
  for ( j = 0; j < ny; ++j ) {
    value_t val = _yvdef->values( j );
    value_t colval = (yslice > 0.0) ? (val - _ymin)/yslice : 0.0;
    int col = (int) floor( colval );
    if ( col >= numBars_ ) col = numBars_ - 1;
    _bins[ col ] += 1.0;
  }
  _ydist = ( _yave-_ymin > _ymax-_yave ) ? _yave-_ymin : _ymax-_yave;

  return true;
}

//----------------------------------------------------------------------+

void
Histo2D::display( int numBars_,
		  bool automatic_,
		  ValueDef *xvdef_,
		  ValueDef *yvdef_ ) {

  if ( !processData( numBars_, automatic_, xvdef_, yvdef_ ) ) return;

                                    // display the histogram.
  _setup.nXBins = _nxr;
  _setup.xMin = _xave - _xdist;
  _setup.xMax = _xave + _xdist;
  _setup.nYBins = _nyr;
  _setup.yMin = _yave - _ydist;
  _setup.yMax = _yave + _ydist;
  _setup.bins = _bins;

  hist2DSetHistogram( _w, &_setup );
  hist2DSetViewAngles( _w, _fi, _psi );

  XtFree( (char*) _bins );
  _bins = (float*) NULL;
}

//----------------------------------------------------------------------+

void
Histo2D::update( int numBars_,
		 bool automatic_,
		 ValueDef *xvdef_,
		 ValueDef *yvdef_ ) {

  if ( !processData( numBars_, automatic_, xvdef_, yvdef_ ) ) return;

                                    // display the histogram.

  hist2DUpdateHistogramData( _w, _bins, NULL, NULL,
			     HIST2D_SCALING );

  hist2DGetViewAngles( _w, &_fi, &_psi );

  XtFree( (char*) _bins );
  _bins = (float*) NULL;
}

//----------------------------------------------------------------------+

void
Histo2D::rebin( int numBars_,
		bool automatic_,
		ValueDef *xvdef_,
		ValueDef *yvdef_ ) {

  if ( !processData( numBars_, automatic_, xvdef_, yvdef_ ) ) return;

                                    // display the histogram.

  hist2DSetRebinnedData( _w, _bins, NULL, NULL,
                         _nxr, _nyr,
                         _xave - _xdist,
                         _xave + _xdist,
			 _yave - _ydist,
                         _yave + _ydist,
                         HIST2D_SCALING );

  hist2DGetViewAngles( _w, &_fi, &_psi );

  XtFree( (char*) _bins );
  _bins = (float*) NULL;
}

//----------------------------------------------------------------------+

// **********************************************************************
// **----------------- Do not modify below this line ------------------**
// **********************************************************************
// $Log: Histo2D.cc,v $
// Revision 1.1.1.1  1997/10/15 00:58:16  qiyang
// Creation
//
// Revision 1.2  1997/02/18 16:57:05  jmc
// "Review integer declaration when used at several levels."
//
// Revision 1.1  1996/04/23 13:53:09  burton
// Creation.
//
