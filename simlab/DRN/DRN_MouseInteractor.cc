//-*-c++-*------------------------------------------------------------
// DRN_MouseInteractor.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <Xmt/Xmt.h>
#include <Xmw/XmwFakeKey.h>

#include "DRN_Interface.h"
#include "DRN_Symbols.h"
#include "DRN_MouseInteractor.h"
#include "DRN_KeyInteractor.h"
#include "DRN_Menu.h"
#include "DRN_DrawingArea.h"
#include "DRN_Legend.h"
#include "DRN_Segment.h"
#include "DRN_Node.h"
#include "DRN_ToolBox.h"
#include "DrawableRoadNetwork.h"

DRN_MouseInteractor::DRN_MouseInteractor(DRN_DrawingArea * area)
  : XmwMouseInteractor((XmwDrawingArea *) area)
{
}


// Operation on the drawing area

void 
DRN_MouseInteractor::leftButtonDown(XmwPoint& point)
{
  if (!theNetwork) return;

  drawingArea()->setState(DRN_DrawingArea::MOUSE_DOWN);

  int tool = theDrnSymbols()->tools().value();

  switch (tool) {

  case DRN_Symbols::TOOL_PAN:
	// set the anchor point for pan
	anchor_pt_ = prev_pt_ = point;
	drawingArea()->pan(prev_pt_, point);
	drawingArea()->foreground(theColorTable->yellow());
	drawingArea()->XmwDrawingArea::drawLine(anchor_pt_, point);
	drawingArea()->XmwDrawingArea::drawPie(anchor_pt_, 3);
	break;

  case DRN_Symbols::TOOL_ZOOMIN:
	// Set the anchor point for the zoom in box
	prev_pt_ = anchor_pt_ = point;
	break;

  case DRN_Symbols::TOOL_ZOOMOUT:
	drawingArea()->zoomOut(point);
	break;

  case DRN_Symbols::TOOL_CENTER:
	drawingArea()->reCenter(point);
	break;

  case DRN_Symbols::TOOL_ROTATE:
	// set the anchor point for rotation
	prev_pt_ = point;
	anchor_pt_ = XmwPoint(drawingArea()->width()/2,
						  drawingArea()->height()/2);
	drawingArea()->rotate(prev_pt_, point);
	drawingArea()->foreground(theColorTable->lightGreen());
	drawingArea()->XmwDrawingArea::drawLine(anchor_pt_, point);
	drawingArea()->XmwDrawingArea::drawPie(anchor_pt_, 5);
	break;

  case DRN_Symbols::TOOL_QUERY:
	if (theDrnSymbols()->viewType().is(DRN_Symbols::VIEW_MICRO)) {
	  theDrawableNetwork->queryVehicle(point);
	} else {
	  // DRN_Node::query(point);

	  // I have problem with invoke a popup menu from mouse
	  // interactor.  Hear is a work around: Generate a key press
	  // event.
	  static XmwFakeKey fke = { drawingArea()->window(), (KeySym)XK_Z };
	  theDrnInterface->makeKeyPressEvent((XtPointer) &fke);
	}
	break;

  case DRN_Symbols::TOOL_LEGEND:
	drawingArea()->unsetState(DRN_DrawingArea::MOUSE_DOWN);
	drawingArea()->setXorMode();
	if (theDrnSymbols()->isLegendOn().value()) {
	  drawingArea()->legend()->draw();
	}
	drawingArea()->legend()->setPosition(point );
	drawingArea()->legend()->draw();
	theDrnSymbols()->isLegendOn().set(true);
	break;

  case DRN_Symbols::TOOL_DRAW:
	// set anchor point for line drawing
	prev_pt_ = anchor_pt_ = point;
	break;

  case DRN_Symbols::TOOL_TRACE:
	if (theDrnSymbols()->viewType().is(DRN_Symbols::VIEW_MICRO)) {
	  theDrawableNetwork->traceVehicle(point);
	}
	break;

  default:
	break;
  }
}

void 
DRN_MouseInteractor::leftButtonMotion(XmwPoint& point)
{
  if (!theNetwork) return;
  int tool = theDrnSymbols()->tools().value();
  switch (tool) {
  case DRN_Symbols::TOOL_PAN:
	drawingArea()->pan(prev_pt_, point);
	drawingArea()->foreground(theColorTable->yellow());
	drawingArea()->XmwDrawingArea::drawLine(anchor_pt_, point);
	drawingArea()->XmwDrawingArea::drawPie(anchor_pt_, 3);
	prev_pt_ = point;
	break;

  case DRN_Symbols::TOOL_ROTATE:
	drawingArea()->rotate(prev_pt_, point);
	drawingArea()->foreground(theColorTable->lightGreen());
	drawingArea()->XmwDrawingArea::drawLine(anchor_pt_, point);
	drawingArea()->XmwDrawingArea::drawPie(anchor_pt_, 5);
	prev_pt_ = point;
	break;

  case DRN_Symbols::TOOL_ZOOMIN:
	drawingArea()->setXorMode();
	drawingArea()->foreground(theColorTable->lightRed());
	// erase old box
	drawingArea()->XmwDrawingArea::drawRect(anchor_pt_, prev_pt_);
	// draw new box
	prev_pt_ = point;
	drawingArea()->XmwDrawingArea::drawRect(anchor_pt_, prev_pt_);
	break;

  case DRN_Symbols::TOOL_DRAW:
	drawingArea()->setXorMode();
	drawingArea()->foreground(theColorTable->yellow());

	// erase old line

	drawingArea()->XmwDrawingArea::drawLine(anchor_pt_, prev_pt_);

	// draw new line

	prev_pt_ = point;
	drawingArea()->XmwDrawingArea::drawLine(anchor_pt_, prev_pt_);
	break;

  case DRN_Symbols::TOOL_LEGEND:
	drawingArea()->setXorMode();
	drawingArea()->legend()->draw();
	drawingArea()->legend()->setPosition(point );
	drawingArea()->legend()->draw();
	prev_pt_ = point;
	break;

  default:
	break;
  }	 
}

void 
DRN_MouseInteractor::leftButtonUp(XmwPoint& point)
{
  if (!theNetwork) return;

  drawingArea()->unsetState(DRN_DrawingArea::MOUSE_DOWN);

  int tool = theDrnSymbols()->tools().value();

  switch (tool) {

  case DRN_Symbols::TOOL_PAN:
	drawingArea()->redraw();
	break;

  case DRN_Symbols::TOOL_ROTATE:
	drawingArea()->redraw();
	break;

  case DRN_Symbols::TOOL_ZOOMIN:
	drawingArea()->setXorMode();
	// erase old box
	drawingArea()->foreground(theColorTable->lightRed());
	drawingArea()->XmwDrawingArea::drawRect(anchor_pt_, prev_pt_);
	drawingArea()->zoomIn(anchor_pt_, prev_pt_);
	break;

  case DRN_Symbols::TOOL_DRAW:
	drawingArea()->setXorMode();
	drawingArea()->foreground(theColorTable->yellow());
	// erase old line
	drawingArea()->XmwDrawingArea::drawLine(anchor_pt_, prev_pt_);
	// draw new line
	prev_pt_ = point;
	drawingArea()->XmwDrawingArea::drawLine(anchor_pt_, prev_pt_);
	break;

  case DRN_Symbols::TOOL_LEGEND:
    drawingArea()->legend()->setPosition( point );
    // drawingArea()->redraw();
    break;

  default:
	break;
  }	 
}


// This functions is reserved for MITSIM and MesoTS to overload

void
DRN_MouseInteractor::leftButtonControlDown(XmwPoint& point)
{
  if (!theNetwork) return;

  int tool = theDrnSymbols()->tools().value();

  switch (tool) {

  case DRN_Symbols::TOOL_QUERY:
	drawingArea()->showSegment(point, 2);
	break;

  case DRN_Symbols::TOOL_DRAW:
	// Set an incident at the cursor
	break;

  default:
	break;
  }
}


void
DRN_MouseInteractor::leftButtonShiftDown(XmwPoint& point)
{
  if (!theNetwork) return;

  int tool = theDrnSymbols()->tools().value();

  switch (tool) {

  case DRN_Symbols::TOOL_QUERY:
	drawingArea()->showSegment(point, 1);
	break;

  case DRN_Symbols::TOOL_DRAW:
	// not used yet
	break;

  default:
	break;
  }
}


void
DRN_MouseInteractor::middleButtonDown(XmwPoint& point)
{
  if (!theNetwork) return;

  int tool = theDrnSymbols()->tools().value();

  switch (tool) {

  case DRN_Symbols::TOOL_QUERY:
	drawingArea()->showCoordinates(point);
	break;

  case DRN_Symbols::TOOL_DRAW:
	// not used yet
	break;

  default:
	break;
  }
}


void 
DRN_MouseInteractor::rightButtonDown(XmwPoint& point)
{
  if (!theNetwork) return;

  drawingArea()->setState(DRN_DrawingArea::MOUSE_DOWN);

  // set anchor point for zooming
  prev_pt_ = anchor_pt_ = point;
}

void 
DRN_MouseInteractor::rightButtonMotion(XmwPoint& point) {

  int tool = theDrnSymbols()->tools().value();

  KeySym keysym;

  switch (tool) {
  default:
    if ( point.x() > prev_pt_.x() + 5 &&
		 point.y() + 5 < prev_pt_.y() ) {
	  keysym = XK_i;
	  drawingArea()->zoom(keysym);
	  prev_pt_ = point;
    } else if ( point.x() + 5 < prev_pt_.x() &&
				point.y() > prev_pt_.y() + 5 ) {
	  keysym = XK_o;
	  drawingArea()->zoom(keysym);
	  prev_pt_ = point;
    }
    theDrnInterface->handlePendingEvents();
    break;
  }	 
}


void 
DRN_MouseInteractor::rightButtonUp(XmwPoint& point)
{
  int tool = theDrnSymbols()->tools().value();

  drawingArea()->unsetState(DRN_DrawingArea::MOUSE_DOWN);

  switch (tool) {
  default:
    prev_pt_ = point;
    XmtDiscardButtonEvents( drawingArea()->widget() );
    break;
  }	 
}
