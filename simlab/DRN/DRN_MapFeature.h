//-*-c++-*------------------------------------------------------------
// NAME: GDS Data Blocks
// AUTH: Qi Yang
// FILE: DRN_MapFeature.h
// DATE: Thu Mar  7 22:05:30 1996
//--------------------------------------------------------------------

#ifndef DRN_MAP_FEATURE_HEADER
#define DRN_MAP_FEATURE_HEADER

//#include <iostream>
#include <vector>

class DRN_DrawingArea;
class GenericVariable;
class XmwPoint;
class GDS_Glyph;


class DRN_MapFeature
{
   protected:
	  
      std::vector<GDS_Glyph *> glyphs_;
	  GDS_Glyph* wklayer_;

   public:

      DRN_MapFeature();
      virtual ~DRN_MapFeature();

      void draw(DRN_DrawingArea *);
      void load(GenericVariable &);
	  inline int nGlyphs() { return glyphs_.size(); }
	  inline GDS_Glyph* glyph(int i) {
		 return glyphs_[i];
	  }
	  void add(GDS_Glyph *g) {
		 glyphs_.push_back(g);
	  }
	  void addLabel(const XmwPoint &p, const char *label);
	  void save();				// save change glyphs
};

extern DRN_MapFeature *theMapFeatures;

#endif
