//-*-c++-*------------------------------------------------------------
// DRN_SegmentCCDialog.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef DRN_SEGMENTCCDIALOG_HEADER
#define DRN_SEGMENTCCDIALOG_HEADER

#include <Xmw/XmwDialogManager.h>

class DRN_SegmentCCDialog: public XmwDialogManager
{
   public:

	  DRN_SegmentCCDialog(Widget parent);
	  ~DRN_SegmentCCDialog() { }
	  void post();				// virtual

   private:

	  // overload the virtual functions in base class

	  void okay(Widget w, XtPointer tag, XtPointer data);
	  void cancel(Widget w, XtPointer tag, XtPointer data);
	  void help(Widget w, XtPointer tag, XtPointer data);

	  // new callbacks

	  static void clickCodeCB(Widget w, XtPointer tag, XtPointer data);

   private:
	  
	  Widget codeFld_;			// the chooser widget
	  int code_;				// saved for cancel
};

#endif
