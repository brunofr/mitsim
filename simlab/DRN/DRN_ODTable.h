//-*-c++-*------------------------------------------------------------
// NAME: Drawable Road Network
// AUTH: Qi Yang
// FILE: DRN_ODTable.h
// DATE: Sun Feb 11 15:19:19 1996
//--------------------------------------------------------------------

#ifndef DRN_ODTABLE_HEADER
#define DRN_ODTABLE_HEADER

#include <GRN/OD_Table.h>

class DRN_DrawingArea;

class DRN_ODTable : public OD_Table
{
   public:
      
      DRN_ODTable() : OD_Table() { }
      virtual ~DRN_ODTable() { }

	  // derived class should overload this virtual function

      virtual OD_Cell* newOD_Cell() = 0;

      virtual void draw(DRN_DrawingArea *);
};

#endif
