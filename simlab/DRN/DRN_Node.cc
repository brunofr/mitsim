//-*-c++-*------------------------------------------------------------
// DRN_Node.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <sstream>
using namespace std;

#include <Xmt/Xmt.h>
#include <Xmt/Dialogs.h>

#include <Tools/SimulationClock.h>
#include <Tools/ToolKit.h>

#include "DRN_ODTable.h"
#include "DRN_ODCell.h"
#include "DRN_Node.h"
#include "DRN_Segment.h"
#include "DRN_Lane.h"
#include "DRN_DrawingArea.h"
#include "DRN_Interface.h"
#include "DRN_Symbols.h"

#include "DrawableRoadNetwork.h"

int DRN_Node::radius_ = DEFAULT_NODE_RADIUS;
DRN_Node * DRN_Node::selectedNode_ = NULL; 

void
DRN_Node::draw(DRN_DrawingArea * area)
{
  XmwPoint p;

  if (area->world2Window(loc_, p)) {

	if (isSelected())
	  area->foreground(theColorTable->lightGray());
	else
	  area->foreground(theColorTable->darkGray());

	area->drawPie(p, radius_);
	 
	area->foreground(theColorTable->yellow());
	area->drawCircle(p, radius_);

	area->foreground(theColorTable->lightRed());

	const char *os;
	os = Str("%d", code_);
	area->drawString(os, p);

	setState(STATE_VISIBLE);
  } else {
	unsetState(STATE_VISIBLE);
  }
}


void
DRN_Node::flowDiaglog()
{
  if (odType() == 0) return;

  SimulationClock *clk = theSimulationClock;
  double dt = clk->currentTime() - clk->startTime();
  if (dt < 60.0) return;
  else dt /= 3600.0;

  ostringstream os;

  const char *tos = Str("Node %d", code_);

  if (name()) os << name() << "\n\n";

  os << "During Period " << clk->startStringTime()
	 << " - " << clk->currentStringTime();

  if (type_ & NODE_TYPE_ORI || nOriCounts_ > 0) {
	os << "\n  " << nOriCounts_ << " vehicle(s) departed";
  }
  if (type_ & NODE_TYPE_DES || nDesCounts_ > 0) {
	os << "\n  " << nDesCounts_ << " vehicle(s) arrived";
  }

  os << "\n\nFlow Rates (vph):";
  if (type_ & NODE_TYPE_ORI || nOriCounts_ > 0) {
	os << "\n  Dep = " << Round(nOriCounts_ / dt);
  }
  if (type_ & NODE_TYPE_DES || nDesCounts_ > 0) {
	os << "\n  Arr = " << Round(nDesCounts_ / dt);
  }


  XmtDisplayInformation(
						theDrnInterface->widget(),
						NULL,					// msg_name,
						os.str().c_str(),				// msg_default
						tos);


}


void DRN_Node::query( const XmwPoint& point)
{
  // Clear previous selection

  if (selectedNode_) {
	selectedNode_->unsetState(STATE_SELECTED);
	theDrawingArea->setCopyMode();
	selectedNode_->draw(theDrawingArea);
	selectedNode_ = NULL;
  }

   // Select a node and popup a menu.  queryCB() below will be called
   // when a node is chosen.

  theDrawableNetwork->selectNode(point);
}

void DRN_Node::queryCB(Widget, XtPointer client_data, XtPointer)
{
  selectedNode_ = (DRN_Node *) client_data;
  if (!selectedNode_) return;

  // Draw the node

  if (theDrnSymbols()->viewType().is(DRN_Symbols::VIEW_ODFLOW) &&
	  theODTable != NULL) {
	selectedNode_->setState(STATE_SELECTED);
	selectedNode_->flowDiaglog();
	theDrawingArea->redraw();
  } else {
	selectedNode_->setState(STATE_SELECTED);
	selectedNode_->draw(theDrawingArea);
  }
}
