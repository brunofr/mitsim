//-*-c++-*------------------------------------------------------------
// FILE:    DRN_Signal.h
// AUTHOR:  Qi Yang 
// DATE:    Sat Jun 3 1995
//--------------------------------------------------------------------

#ifndef DRN_SIGNAL_HEADER
#define DRN_SIGNAL_HEADER

#include <Xmw/XmwColor.h>

#include <GRN/RN_Signal.h>
#include <GRN/WcsPoint.h>

class DRN_DrawingArea;

// A signal may be represented by an arrow, cross, boxed cross, bar,
// or filled pie.  The constants below defines the size of these
// symbols.

const double SIGNAL_SIZE  = 2.0;
const double SIGNAL_ARROW_SIZE = 0.8; // proportion
const double SIGNAL_ARROW_ANGLE  = ONE_PI / 8.0;


class DRN_Signal : public virtual RN_Signal
{
   public:
	
      // A signal may be represented by an arrow, cross, boxed cross,
      // bar, or filled pie.

      enum {
		 NONE        = 0,	// not visible
		 ARROW       = 1,	// eg LUS
		 CROSS       = 2,	// eg LUS
		 BOXED_CROSS = 3,	// eg incident
		 BAR         = 4,	// eg TS, PS, TollPlaza, BusStop (margaret)
		 PIE         = 5,	// eg TS
		 NUMBER      = 6		// eg VSLS and VMS
      };

   protected:

      WcsPoint center_;		// position
      double angle_;		// direction

      unsigned int shownState_;	// current shown state
      short int onScreen_;	// 1 if visible

   public:

      DRN_Signal();
      virtual ~DRN_Signal() { }

      void calcGeometricData();
      WcsPoint& center() { return center_; }

      virtual Pixel* calcColor(unsigned int);

      virtual int isChosen();
      virtual int draw(DRN_DrawingArea *);

   protected:
      
      virtual int drawShape(DRN_DrawingArea *);
	      
      virtual int drawPie(DRN_DrawingArea *, Pixel *);
      virtual int drawPie(DRN_DrawingArea *, Pixel *, double);
      virtual int drawArrow(DRN_DrawingArea *, Pixel *, double beta = 0.0);
      virtual int drawCross(DRN_DrawingArea *, Pixel *);
      virtual int drawBoxedCross(DRN_DrawingArea *, Pixel *);
	  virtual int drawFilledRectangleCross(DRN_DrawingArea *, Pixel *);
	  virtual int drawRubberNeck(DRN_DrawingArea *, Pixel *);
      virtual int drawBar(DRN_DrawingArea *, Pixel *);
      virtual int drawBooth(DRN_DrawingArea *, Pixel *);
      virtual int drawBusStop(DRN_DrawingArea *, Pixel *);  //margaret

      virtual int drawNumber(DRN_DrawingArea *, Pixel,
							 const char* fmt = "%d");
      virtual int drawLine(DRN_DrawingArea *, Pixel);

      virtual int drawTS(DRN_DrawingArea *);
      virtual int drawSingleTS(DRN_DrawingArea *,
							   double, short int,
							   short int bits = 0);
};

#endif
