//-*-c++-*------------------------------------------------------------
// DRN_ViewMarker.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <cstdio>

#include <Tools/ToolKit.h>
#include <Tools/GenericVariable.h>

#include <GRN/WcsPoint.h>
#include <GRN/RN_WorldSpace.h>

#include "DRN_ViewMarker.h"
#include "DRN_Symbols.h"
#include "DRN_Interface.h"
#include "DRN_DrawingArea.h"
#include "DRN_Menu.h"
using namespace std;

DRN_ViewMarker::StoreType DRN_ViewMarker::ViewMarkers_ ;

void DRN_ViewMarker::LoadViewMarkers(GenericVariable& gv)
{
	DRN_ViewMarker* marker ;
	int n = gv.nElements() / 14 ;
	for(int i = 0; i < n; i ++) {
		marker = new DRN_ViewMarker ;
		marker->init(gv.elements(i * 14)) ;
		AddMarker(marker) ;
	}
}


int DRN_ViewMarker::AddMarker(DRN_ViewMarker* marker)
{
    theMenu->needSave(XmwMenu::STATE_SAVE_MASTER) ;
	ViewMarkers_.push_back(marker) ;
	return nMarkers() ;
}

bool DRN_ViewMarker::DeleteMarker(DRN_ViewMarker* marker)
{
	for (int i = 0; i < nMarkers(); i ++) {
		if (Marker(i) == marker) {
			ViewMarkers_.erase(ViewMarkers_.begin() + i) ;
			return true ;
		}
	}
	return false ;
}

void DRN_ViewMarker::DeleteMarkers()
{
	ViewMarkers_.erase(ViewMarkers_.begin(), ViewMarkers_.end()) ;
}

void DRN_ViewMarker::SaveMarkers(ostream& os)
{
	os << endl << "[View Markers] = {" << endl ;
	os << indent
	   << "# Label Position Scale Angle Tool ViewType Segment"
	   << endl
	   << indent
	   << "# SensorType/Label/Color SignalType Map Legend"
	   << endl ;
	for (int i = 0; i < nMarkers() ; i ++) {
		Marker(i)->save(os) ;
	}
	os << "}" << endl ;
}

void DRN_ViewMarker::GotoMarker(int i)
{
	char buf[256] ;
	if (i < 0 || i >= nMarkers()) {
		sprintf(buf, "View marker %d not found.", i) ;
	} else {
		DRN_ViewMarker *marker = Marker(i) ;
		DRN_DrawingArea *area = theDrawingArea ;

		theDrnSymbols()->tools().set(marker->tools_);
		theDrnSymbols()->viewType().set(marker->viewType_);
		theDrnSymbols()->segmentColorCode().set(marker->segmentColorCode_);
		theDrnSymbols()->sensorTypes().set(marker->sensorTypes_);
		theDrnSymbols()->sensorLabel().set(marker->sensorLabel_);
		theDrnSymbols()->sensorColorCode().set(marker->sensorColorCode_);
		theDrnSymbols()->signalTypes().set(marker->signalTypes_);
		theDrnSymbols()->isMapFeaturesOn().set(marker->isMapFeaturesOn_);
		theDrnSymbols()->isLegendOn().set(marker->isLegendOn_) ;
		
		area->GotoMarker(marker->xpos_, marker->ypos_,
						 marker->scale_, marker->angle_) ;
		
		sprintf(buf, "%d: %s", i, marker->label_.c_str()) ;
	}
	theDrnInterface->msgSet(buf) ;
}

DRN_ViewMarker::DRN_ViewMarker()
	: label_(""),				// label of the marker
	  xpos_(0.5),				// center of the scope
	  ypos_(0.5),
	  scale_(1.0),				// 1 pixel per meter
	  angle_(0.0),				// north up
	  tools_(DRN_Symbols::TOOL_PAN),
	  viewType_(DRN_Symbols::VIEW_MICRO),
	  segmentColorCode_(DRN_Symbols::SEGMENT_LINKTYPE),
	  sensorTypes_(0),
	  sensorLabel_(DRN_Symbols::SENSOR_LABEL_NONE),
	  sensorColorCode_(DRN_Symbols::SENSOR_OCCUPANCY),
	  signalTypes_(0),
	  isMapFeaturesOn_(false),
	  isLegendOn_(false)
{
}

void DRN_ViewMarker::init(DRN_DrawingArea *area, const char* label)
{
	label_ = label ;
	WcsPoint c = area->center() ;
	xpos_ = c.x() / theWorldSpace->width() ;
	ypos_ = c.y() / theWorldSpace->height() ;
	scale_ = area->meter2Pixels();
	angle_ = area->angle() ;
	tools_  = theDrnSymbols()->tools().value();
	viewType_  = theDrnSymbols()->viewType().value();
	segmentColorCode_  = theDrnSymbols()->segmentColorCode().value();
	sensorTypes_  = theDrnSymbols()->sensorTypes().value();
	sensorLabel_  = theDrnSymbols()->sensorLabel().value();
	sensorColorCode_  = theDrnSymbols()->sensorColorCode().value();
	signalTypes_ = theDrnSymbols()->signalTypes().value();
	isMapFeaturesOn_ = theDrnSymbols()->isMapFeaturesOn().value();
	isLegendOn_ = theDrnSymbols()->isLegendOn().value();
}

void DRN_ViewMarker::init(ArrayElement* e)
{
	label_ = (const char*) e[0] ;
	xpos_ = e[1] ;
	ypos_ = e[2] ;
	scale_ = e[3] ;
	angle_ = e[4] ;
	tools_ = e[5] ;
	viewType_ = e[6] ;
	segmentColorCode_ = e[7] ;
	sensorTypes_ = e[8] ;
	sensorLabel_ = e[9] ;
	sensorColorCode_ = e[10] ;
	signalTypes_ = e[11] ;
	isMapFeaturesOn_ = e[12] ;
	isLegendOn_ = e[13] ;
}

void DRN_ViewMarker::save(ostream& os)
{
	os << indent << '"' << label_.c_str() << '"' << endc
	   << xpos_ << endc << ypos_ << endc
	   << scale_ << endc
	   << angle_ << endc
	   << tools_ << endc
	   << viewType_ << endc
	   << segmentColorCode_<< endc
	   << sensorTypes_<< endc
	   << sensorLabel_<< endc
	   << sensorColorCode_<< endc
	   << signalTypes_<< endc
	   << isMapFeaturesOn_ << endc
	   << isLegendOn_ << endl ;
}
