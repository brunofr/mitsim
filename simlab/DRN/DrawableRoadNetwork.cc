//-*-c++-*------------------------------------------------------------
// DrawableRoadNetwork.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//--------------------------------------------------------------------

#include <cstdio>
#include <Tools/ToolKit.h>

#include <Xmw/XmwPopupChooser.h>
#include <GRN/NameTable.h>
#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>

#include "DrawableRoadNetwork.h"

#include "DRN_Sensor.h"
#include "DRN_Signal.h"
#include "DRN_Segment.h"
#include "DRN_Lane.h"
#include "DRN_Node.h"
#include "DRN_ODTable.h"
#include "DRN_ODCell.h"

#include "DRN_Symbols.h"
#include "DRN_Interface.h"
#include "DRN_Menu.h"
#include "DRN_DrawingArea.h"
#include "DRN_SignalEditor.h"
#include "DRN_MouseInteractor.h"
using namespace std;

DrawableRoadNetwork * theDrawableNetwork = NULL;

DrawableRoadNetwork::DrawableRoadNetwork() 
  : RoadNetwork()
{
  theDrawableNetwork = this;
}


DrawableRoadNetwork::~DrawableRoadNetwork()
{
  theDrawableNetwork = NULL;
  theDrawingArea->unsetState(DRN_DrawingArea::NETWORK_LOADED);
}


// These functions should be overloaded in the inherited class.

RN_Segment* DrawableRoadNetwork::newSegment()
{
  return new DRN_Segment;
}

RN_Lane* DrawableRoadNetwork::newLane()
{
  return new DRN_Lane;
}

RN_Sensor* DrawableRoadNetwork::newSensor()
{
  return new DRN_Sensor;
}

RN_Signal* DrawableRoadNetwork::newSignal()
{
  return new DRN_Signal;
}

RN_Node* DrawableRoadNetwork::newNode()
{
  return new DRN_Node;
}


// This overload the function in base class. It asks X server to
// process the pending events.

void DrawableRoadNetwork::updateProgress(const char *msg)
{
  RoadNetwork::updateProgress(msg);
  if (msg) {
	theDrnInterface->msgSet(msg);
  } else {
	theDrnInterface->msgClear();
  }
  // theDrnInterface->handlePendingEvents();
}

void DrawableRoadNetwork::updateProgress(float pct)
{
  if (!ToolKit::debug() && ToolKit::verbose()) {
	theModeline()->updateProgress(0, pct);
  }
}


// Adjust arc coordinates to make the end points of each connected
// segment to snapped at the same point.  Then it calculates the
// widths and offsets from the base line.  Finally, it calculates the
// intersections between lines.

void
DrawableRoadNetwork::calcGeometricData()
{
  updateProgress("Calculating geometric data for graphics");

  register int i;

  // Find the width and offset from the base line for each segment
  // and the lanes

  for (i = 0; i < nSegments(); i ++) {
	drnSegment(i)->calcWidthAndOffset();
  }

  // Calculate the intersection of the curves (lane mark lines or
  // curb lines).

  for (i = 0; i < nSegments(); i ++) {
	drnSegment(i)->calcLaneCurves();
  }

  // Calculate the position of sensors

  for (i = 0; i < nSensors(); i ++) {
	sensor(i)->calcGeometricData();
  }
  
  // Calculate the position of signals

  for (i = 0; i < nSignals(); i ++) {
	signal(i)->calcGeometricData();
  }

  // Set variables in nodes

  for (i = 0; i < nNodes(); i ++) {
	node(i)->calcGeometricData();
  }

  theDrawingArea->setState(DRN_DrawingArea::NETWORK_LOADED);
}


void
DrawableRoadNetwork::unsetDrawingStates(DRN_DrawingArea *area)
{
   area->unsetState(DRN_DrawingArea::VEHICLE_ON);
   area->unsetState(DRN_DrawingArea::SENSOR_ON);
   area->unsetState(DRN_DrawingArea::SIGNAL_ON);
   area->unsetState(DRN_DrawingArea::INCIDENT_ON);
}

// -------------------------------------------------------------------
//  This draws all network objects
// -------------------------------------------------------------------

void
DrawableRoadNetwork::drawNetwork(DRN_DrawingArea * area)
{
  if (area->state(DRN_DrawingArea::VIEW_NONE)) return;

  drawRoads(area);

  if (theDrnSymbols()->viewType().is(DRN_Symbols::VIEW_MICRO)) {
	drawSensors(area);
	drawSignals(area);
  } else {
	if (theDrnSymbols()->viewType().is(DRN_Symbols::VIEW_ODFLOW) &&
		theODTable != NULL) {
	  ((DRN_ODTable *)theODTable)->draw(area);
	}
	drawNodes(area);
  }
}


void
DrawableRoadNetwork::drawNodes(DRN_DrawingArea * area)
{
  area->setCopyMode();
  for (int i = 0; i < nNodes(); i ++) {
	((DRN_Node *) node(i))->draw(area);
  }

  // Put the selected node to front

  if (DRN_Node::selectedNode()) {
	DRN_Node::selectedNode()->draw(area);
  }
}


// -------------------------------------------------------------------
//  This draws segments and lanes
// -------------------------------------------------------------------

void
DrawableRoadNetwork::drawRoads(DRN_DrawingArea * area)
{
  for (register int i = 0; i < nSegments(); i ++) {
	((DRN_Segment *) segment(i))->draw(area);
  }
}


void
DrawableRoadNetwork::updateRoads(DRN_DrawingArea * area)
{
  int view = theDrnSymbols()->segmentColorCode().value();
  if (view == DRN_Symbols::SEGMENT_LINKTYPE) {
	return;
  }
  if (area->state(DRN_DrawingArea::REDRAW)) {
	area->redraw();
  } else {
	drawRoads(area);
  }
  theDrnInterface->handlePendingEvents();
}


// -------------------------------------------------------------------
//  This draw all sensors in the network
// -------------------------------------------------------------------

void
DrawableRoadNetwork::drawSensors(DRN_DrawingArea * area)
{
  if (!area->isSensorDrawable()) return;
  for (register int i = 0; i < nSensors(); i ++) {
	sensor(i)->draw(area);
  }
  area->setState(DRN_DrawingArea::SENSOR_ON);
}


// -------------------------------------------------------------------
//  This draw all signals in the network
// -------------------------------------------------------------------

void
DrawableRoadNetwork::drawSignals(DRN_DrawingArea * area)
{
  register int i;

  // Only the chosen types of Standard signals are shown

  if (area->isSignalDrawable()) {
	for (i = 0; i < nStdSignals(); i ++) {
	  RN_Signal *s = signal(i);
	  s->draw(area);
	}
  }

  // Toll booths and incidents are always shown

  if (area->resolution() > DRN_DrawingArea::LOW_RESOLUTION) {
	for (i = nStdSignals_; i < nSignals(); i ++) {
	  RN_Signal *s = signal(i);
	  s->draw(area);
	}
  }

  area->setState(DRN_DrawingArea::SIGNAL_ON);
}


// -------------------------------------------------------------------
// Find the closest segment at the point where left mouse button and
// <shift> pressed.
// -------------------------------------------------------------------

DRN_Segment *
DrawableRoadNetwork::nearestSegment(const WcsPoint& pressed)
{
  double epsilon = 0.5 * LANE_WIDTH;
  DRN_Segment *ps, *nearest = NULL;
  double mindis = DBL_INF;
  double dis;
  WcsPoint p;

  for (register int i = 0; i < nSegments(); i ++) {
	ps = drnSegment(i);
	if (ps->isVisible()) {
	  RN_Arc central_arc(ps->centerPoint(1.0), // start point
						 ps->centerPoint(0.0), // end point
						 ps->bulge());
	  p = central_arc.nearestPoint(pressed, epsilon * ps->nLanes());
	  dis = p.distance_squared(pressed);
	  if (dis < mindis) {
		mindis = dis;
		nearest = ps;
	  }
	}
  }
   
  if (nearest &&
	  sqrt(mindis) <= 2.0 * epsilon * nearest->nLanes()) {
	return (nearest);
  } else {
	return (NULL);
  }
}


void DrawableRoadNetwork::selectNode(const XmwPoint &point)
{
  WcsPoint p = theDrawingArea->window2World(point);
  float epsilon = 3.0 * DRN_Node::radius() / theDrawingArea->meter2Pixels();

  vector<string> labels;
  vector<XtPointer> candidates;

  for (register int i = 0; i < nNodes(); i ++) {
	DRN_Node* pn = drnNode(i);
	if (pn->isVisible()) {
	  if (pn->loc().distance(p) < epsilon) {
		char label[128];
		if (pn->name()) {
		  sprintf(label, "%d: %s", pn->code(), pn->name());
		} else {
		  sprintf(label, "Node %d", pn->code());
		}
		labels.push_back(label);
		candidates.push_back((XtPointer) pn);
	  }
	}
  }

  if (candidates.size() > 1) {	// multiple nodes
	static XmwPopupChooser *chooser = NULL;
	if (chooser) delete chooser;
	chooser = new XmwPopupChooser(
		       theDrawingArea->widget(),
		       labels.size(),
		       labels,
		       &DRN_Node::queryCB,
		       &candidates);
	chooser->popup(&point);
	theDrnInterface->msgClear();
  } else if (candidates.size() > 0) { // only one
	DRN_Node::queryCB(NULL, candidates[0], NULL);
	theDrnInterface->msgClear();
  } else {
	const char *msg = XmtLocalize2(theDrnInterface->widget(),
								   "No node found.", "prompt", "noNodeFound");
	theDrnInterface->msgSet(msg);
  }
}

void DrawableRoadNetwork::editSignalStates(const XmwPoint& point)
{
  WcsPoint p = theDrawingArea->window2World(point);
  float epsilon = LANE_WIDTH * 5.0;

  vector<string> labels;
  vector<XtPointer> stations;

  for (register int i = 0; i < nLinks(); i ++) {
	RN_Link *pl = link(i);
	CtrlList ctrl = pl->ctrlList();
	while (ctrl) {
	  RN_CtrlStation *s = ctrl->data();
	  DRN_Segment *ps = (DRN_Segment *) s->segment();
	  if (ps->isVisible()) {
		float dis = p.distance(ps->centerPoint(s->position()));
		if (dis < epsilon) {
		  RN_Signal *sn = NULL;
		  for (int j = 0; sn == NULL && j < s->nLanes(); j ++) {
			sn = s->signal(j);
		  }
		  if (sn) {
			char label[64];
			sprintf(label, "%s: %d", NameTable::signal(s->type()),
					sn->code());
			labels.push_back(label);
			stations.push_back((XtPointer) s);
		  }
		}
	  }
	  ctrl = ctrl->next();
	}
  }

  // Create and post the dialog box for signal state editing

  if (stations.size()) {
	static XmwPopupChooser *chooser = NULL;
	if (chooser) delete chooser;
	chooser = new XmwPopupChooser(
			theDrawingArea->widget(),
			labels.size(),
			labels,
			&DrawableRoadNetwork::editSignalStatesCB,
			&stations);
	chooser->popup(&point);
	theDrnInterface->msgClear();
  } else {
	const char *msg = XmtLocalize2(theDrnInterface->widget(),
								   "No signal found.", "prompt", "noSignalFound");
	theDrnInterface->msgSet(msg);
  }
}

void DrawableRoadNetwork::editSignalStatesCB(Widget,
											 XtPointer client_data, XtPointer)
{
  RN_CtrlStation *station = (RN_CtrlStation *) client_data;
  if (station) {
	DRN_SignalEditor::createAndPost(station);
  }
}
