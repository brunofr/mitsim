//-*-c++-*------------------------------------------------------------
// FILE:    DRN_Sensor.C
// AUTHOR:  Qi Yang 
// DATE:    Nov 27, 1995
//--------------------------------------------------------------------

#include <stdio.h>
#include <Xmt/Xmt.h>

#include "DrawableRoadNetwork.h"
#include "DRN_Interface.h"
#include "DRN_Menu.h"
#include "DRN_DrawingArea.h"
#include "DRN_Sensor.h"
#include "DRN_Lane.h"
#include "DRN_Segment.h"
#include "DRN_Symbols.h"

#include <Tools/Math.h>
#include <Tools/ToolKit.h>
#include <GRN/Constants.h>
#include <GRN/Parameter.h>
#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>

DRN_Sensor::DRN_Sensor() 
   : RN_Sensor(),
     label_(NULL),
     onScreen_(0)
{
}


// ------------------------------------------------------------------
//  Find the position of the sensor
// ------------------------------------------------------------------

void DRN_Sensor::calcGeometricData()
{
   RN_DrawableLane *pl = (RN_DrawableLane *)lane();
   float pos = position() + zoneLength()/pl->length();
   angle_  = pl->tangentAngle(pos);
   if (isLinkWide()) {
      center_ = ((RN_DrawableSegment *)pl->segment())->centerPoint(pos);
   } else {
      center_ = pl->centerPoint(pos);
   }
}


// ------------------------------------------------------------------
//  Returns a color for drawing a sensor
// ------------------------------------------------------------------

Pixel
DRN_Sensor::calcColor(DRN_DrawingArea * area)
{
   if (state_ & SENSOR_BROKEN) {
      return theColorTable->darkGray();

   } else if (atask() & SENSOR_AGGREGATE) {

	  switch (theDrnSymbols()->sensorColorCode().value()) {

		 case DRN_Symbols::SENSOR_OCCUPANCY:
			return area->occupancyColor(occupancy());

		 case DRN_Symbols::SENSOR_SPEED:
			return area->speedColor(speed());

		 case DRN_Symbols::SENSOR_FLOW:
			return area->linkFlowColor(flow());

		 case DRN_Symbols::SENSOR_COUNT:
			return area->linkCountColor(count());

		 default:
			return theColorTable->white();
	  }
   } else {
	  if (state_ & SENSOR_ACTIVATED) {
		 return theColorTable->cyan();
	  } else {
		 return theColorTable->lightGray();
      }
   }
}


// -----------------------------------------------------------------
// Draw a sensor in the view window. It returns 1 if the sensor is
// visible or 0 otherwise
// -----------------------------------------------------------------

int
DRN_Sensor::draw(DRN_DrawingArea * area)
{
   Pixel oldc = color_;
   char *oldm = label_;

   color_ = calcColor(area);
   label_ = calcLabel();

   int changed = color_ != oldc || Cmp(oldm, label_) != 0;
   int on = onScreen_ && area->state(DRN_DrawingArea::SENSOR_ON);
   
   if (on && !changed) {
      return onScreen_;		// nothing changed
   }

   // erase previous image

   if (on) draw(area, oldc, oldm);
   else on = calcPoints(area);

   onScreen_ = 0;
   if (oldm) delete [] oldm;

   if (on) {		// draw
      draw(area, color_, label_);
   }

   return onScreen_ = on;
}


void
DRN_Sensor::draw(DRN_DrawingArea * area, Pixel clr, char *label)
{
   area->setXorMode();
   area->foreground(clr);
   
   if (label) {
      RN_DrawableSegment *ps = (RN_DrawableSegment *) segment();
      double size = area->fontHeight() /	area->meter2Pixels();
      double pos = position() - size / ps->length();
      WcsPoint p(ps->centerPoint(pos));
      area->draw(p, label);
   }

   area->drawLines(points_, 5);
}


// ----------------------------------------------------------------
//  Erase a sensor from the view window
// ----------------------------------------------------------------

void
DRN_Sensor::erase(DRN_DrawingArea * area)
{
   if (!onScreen_) return;
   draw(area, color_, label_);
   onScreen_ = 0;
   if (label_) delete [] label_;
}


// ----------------------------------------------------------------
//  Create the label string. The caller is responsible for
//  releasing the used memory.
// ----------------------------------------------------------------

char *
DRN_Sensor::calcLabel()
{
   if (station()->sensor(0) != this) { // one label per station
	  return NULL;
   }

   char buffer[32];

   switch (theDrnSymbols()->sensorLabel().value()) {

	  case DRN_Symbols::SENSOR_LABEL_CODE:
		 {
			sprintf(buffer, "%d", code());
			return strdup(buffer);
		 }
	  case DRN_Symbols::SENSOR_LABEL_VALUE:
		 {
			if (!(atask() & SENSOR_AGGREGATE)) {
			   return NULL;
			}
			switch (theDrnSymbols()->sensorColorCode().value()) {
			   case DRN_Symbols::SENSOR_OCCUPANCY:
				  {
					 sprintf(buffer, "%.1f", stationOccupancy());
					 break;
				  }
			   case DRN_Symbols::SENSOR_SPEED:
				  {
					 float x = stationSpeed()/theBaseParameter->speedFactor();
					 sprintf(buffer, "%.0f", x);
					 break;
				  }
			   case DRN_Symbols::SENSOR_FLOW:
				  {
					 sprintf(buffer, "%d", stationFlow());
					 break;
				  }
			   case DRN_Symbols::SENSOR_COUNT:
				  {
					 sprintf(buffer, "%d", stationCount());
					 break;
				  }
			   default:
				  {
					 return NULL;
				  }
			}
			return strdup(buffer);
		 }
	  default:
		 {
			return NULL;
		 }
   }
   return NULL;
}


// ----------------------------------------------------------------
//  Calculate the 4 corner points of the sensor. It returns 1 if
//  the sensor is visible or 0 otherwise.
// ----------------------------------------------------------------

int
DRN_Sensor::calcPoints(DRN_DrawingArea * area)
{
   float w = 0.5 * width() - 0.2 * LANE_WIDTH;
   float h = 0.5 * SENSOR_SIZE;
   float d = sqrt(w * w + h * h);
   float a = acos(h / d);

   if (!area->world2Window(center_.bearing(d, angle_ + a),
						   points_[0])) return 0;

   if (!area->world2Window(center_.bearing(d, angle_ - a),
						   points_[1])) return 0;
   
   if (!area->world2Window(center_.bearing(d, angle_ + a - ONE_PI),
						   points_[2])) return 0;

   if (!area->world2Window(center_.bearing(d, angle_ - a + ONE_PI),
						   points_[3])) return 0;

   points_[4] = points_[0];

   return 1;
}


// Average of all sensors in the station

int 
DRN_Sensor::stationCount()
{
   if (station_->isLinkWide()) {
      return count();
   }
   
   RN_Segment *ps = station_->segment();
   int num = ps->nLanes();
   int total = 0;
   for (register int i = 0; i < num; i ++) {
      if (station_->sensor(i)) {
		 total += station_->sensor(i)->count();
      }
   }
   return total;
}

int 
DRN_Sensor::stationFlow()
{
   if (station_->isLinkWide()) {
      return flow();
   }
   
   RN_Segment *ps = station_->segment(); 
   int num = ps->nLanes();
   int total = 0;
   int cnt = 0;
   for (register int i = 0; i < num; i ++) {
      if (station_->sensor(i)) {
		 total += station_->sensor(i)->flow();
		 cnt ++;
      }
   }
   if (cnt) return total / cnt;
   else return 0;
}

float 
DRN_Sensor::stationOccupancy()
{
   if (station_->isLinkWide()) {
      return occupancy();
   }
   
   RN_Segment *ps = station_->segment(); 
   int num = ps->nLanes();
   float total = 0;
   int cnt = 0;
   for (register int i = 0; i < num; i ++) {
      if (station_->sensor(i)) {
		 total += station_->sensor(i)->occupancy();
		 cnt ++;
      }
   }
   if (cnt) return total / cnt;
   else return 0.0;
}

float 
DRN_Sensor::stationSpeed()
{
   if (station_->isLinkWide()) {
      return speed();
   }
   RN_Segment *ps = station_->segment(); 
   int num = ps->nLanes();
   float total = 0;
   int cnt = 0;
   for (register int i = 0; i < num; i ++) {
      if (station_->sensor(i)) {
		 total += station_->sensor(i)->speed();
		 cnt ++;
      }
   }
   if (cnt) return total / cnt;
   else return ps->freeSpeed();
}
