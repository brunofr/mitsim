//-*-c++-*------------------------------------------------------------
// DRN_FindDialog.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the implementation for the class DRN_FindDialog.
//--------------------------------------------------------------------

#include <iostream>
#include <cstring>
using namespace std;

#include <Xmt/Xmt.h>
#include <Xmt/Dialog.h>

#include <Tools/ToolKit.h>

#include "DRN_Interface.h"
#include "DRN_DrawingArea.h"
#include "DRN_FindDialog.h"
#include "DRN_DrawingArea.h"
#include "DrawableRoadNetwork.h"

XtResource DRN_FindDialog::resources[] =
{
   { "type", "type", XtRInt,
	 sizeof(int), XtOffsetOf(DRN_FindDialog::dataType, type),
	 XtRImmediate, (XtPointer)DRN_FindDialog::FIND_NODE
   },
   { "object", "object", XmtRBuffer,
	 max_code_columns, XtOffsetOf(DRN_FindDialog::dataType, object),
	 XtRImmediate, (XtPointer)NULL
   },
   { "annotation", "annotation", XtRInt,
	 sizeof(int), XtOffsetOf(DRN_FindDialog::dataType, annotation),
	 XtRImmediate, (XtPointer)DRN_FindDialog::ANNOTATION_ALL
   }
};

DRN_FindDialog::DRN_FindDialog ( Widget parent )
   : XmwDialogManager(parent, "finderDialog",
					  DRN_FindDialog::resources,
					  XtNumber(DRN_FindDialog::resources),
					  "search", "close")
{
   data_.type = FIND_NODE;
   data_.annotation = ANNOTATION_ALL;
}

// These overload the functions in base class

void DRN_FindDialog::cancel(Widget w, XtPointer tag, XtPointer data)
{
   XmtDialogCancelCallback(w, tag, data);
   theDrawingArea->redraw();
}


void DRN_FindDialog::okay(Widget w, XtPointer tag, XtPointer data)
{
   if (theNetwork) {
	  Widget shell = XmtGetShell(w);
	  XmtDialogGetDialogValues(shell, &data_);
	  if (!IsEmpty(data_.object)) {
		 theDrawingArea->locate(data_.type, data_.object,
			data_.annotation);
	  }
   } else {
	  theDrnInterface->msgShow(XmtLocalize2(widget_,
		 "Network not ready.", "prompt", "noNetwork"));
   }
}


void DRN_FindDialog::help(Widget, XtPointer, XtPointer)
{
   theDrnInterface->openUrl("finder", "drn.html");
}


void DRN_FindDialog::post()
{
   XmtDialogDo(widget(), &data_);
   XmwDialogManager::ready();
}
