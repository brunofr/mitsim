//-*-c++-*------------------------------------------------------------
// NAME: Drawable Raod Network
// AUTH: Qi Yang
// FILE: DRN_RangeForm.h
// DATE: Tue Feb 13 10:09:19 1996
//--------------------------------------------------------------------

#ifndef DRN_RANGEFORM_HEADER
#define DRN_RANGEFORM_HEADER

class Normalizer;

class DRN_RangeDialog
{
   public:

      DRN_RangeDialog(Widget, Normalizer *, double scaling,
		      char * title);

      ~DRN_RangeDialog();

      void create(char * title);
      void read();

      static void getRange(Widget, Normalizer *, double scaling = 1.0,
			   char * title = "Range");

   protected:

      Widget _parent;
      Widget _baseWidget;
	     
      Widget _maxScale;
      Widget _minScale;
      Widget _okButton;

      double scaling_;		// convert units
      double dec_;		// decimal to integer value

      Normalizer *range_;

   protected:

      virtual void changeMinValue ( Widget, XtPointer );
      virtual void changeMaxValue ( Widget, XtPointer );
      virtual void rangeOk ( Widget, XtPointer );

  private: 

    // Callbacks to interface with Motif

    static void maxChangedCallback ( Widget, XtPointer, XtPointer );
    static void maxDragCallback ( Widget, XtPointer, XtPointer );
    static void minChangedCallback ( Widget, XtPointer, XtPointer );
    static void minDragCallback ( Widget, XtPointer, XtPointer );
    static void rangeOkCallback ( Widget, XtPointer, XtPointer );
};

#endif

