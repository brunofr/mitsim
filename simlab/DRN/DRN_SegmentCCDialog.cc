//-*-c++-*------------------------------------------------------------
// DRN_SegmentCCDialog.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <assert.h>

#include <Xmt/Xmt.h>
#include <Xmt/Chooser.h>

#include "DRN_SegmentCCDialog.h"
#include "DRN_Interface.h"
#include "DRN_Symbols.h"
#include "DRN_DrawingArea.h"
#include "DRN_Menu.h"

DRN_SegmentCCDialog::DRN_SegmentCCDialog ( Widget parent )
   : XmwDialogManager(parent, "segmentDialog", NULL, 0)
{
   codeFld_ = theDrnSymbols()->associate(widget_, "segmentColorCode");
   assert(codeFld_);
   XtAddCallback(codeFld_, XmtNvalueChangedCallback,
				 DRN_SegmentCCDialog::clickCodeCB, NULL);
}

// These overload the functions in base class

void DRN_SegmentCCDialog::cancel(Widget, XtPointer, XtPointer)
{
   theDrnSymbols()->segmentColorCode().set(code_);
   theDrawingArea->setState(DRN_DrawingArea::REDRAW);
   unmanage();
}


void DRN_SegmentCCDialog::okay(Widget, XtPointer, XtPointer)
{
   theDrawingArea->setState(DRN_DrawingArea::REDRAW);
   unmanage();
   theMenu->needSave();
}


void DRN_SegmentCCDialog::help(Widget, XtPointer, XtPointer)
{
   theDrnInterface->openUrl("segmentColorCode", "drn.html");
}


void DRN_SegmentCCDialog::post()
{
   code_ = theDrnSymbols()->segmentColorCode().value();

   // This is a work around of a problem with XmtSymbol which does not
   // update the chooser state sometimes.  This line should be removed
   // after we figure out and fix the problem.
   
   XmtChooserSetState(codeFld_, code_, False);

   XmwDialogManager::post();
}


void DRN_SegmentCCDialog::clickCodeCB(Widget w, XtPointer, XtPointer)
{
   theDrawingArea->redraw();
}
