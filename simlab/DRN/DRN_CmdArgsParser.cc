//-*-c++-*------------------------------------------------------------
// NAME: Command line arguments parser for DRN
// AUTH: Qi Yang
// FILE: DRN_CmdArgsParser.C
// DATE: Fri Nov 17 17:28:06 1995
//--------------------------------------------------------------------

#include <Xmw/XmwColor.h>
#include "DRN_CmdArgsParser.h"
#include "DRN_Interface.h"
#include "DrawableRoadNetwork.h"


DRN_CmdArgsParser::DRN_CmdArgsParser() : CmdArgsParser()
{
   addOption("display", &(DRN_Interface::dsp_),
	     "Display for graphical windows");
}


// This is a virtula function and will be overloaded in inherited
// class

void DRN_CmdArgsParser::init()
{
   // Empty
}
