//-*-c++-*------------------------------------------------------------
// NAME: Show help info for the keys and mouse buttons
// AUTH: Qi Yang
// FILE: DRN_Help.h
// DATE: Tue May  7 15:00:23 1996
//--------------------------------------------------------------------

#ifndef DRN_HELP_HEADER
#define DRN_HELP_HEADER

// These two variables are initialized as NULL and can be used to
// add additional help information

extern char *aux_help_msg;

extern void HelpKeys();

#endif
