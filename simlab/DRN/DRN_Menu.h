//-*-c++-*------------------------------------------------------------
// DRN_Menu.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class definition for the class DRN_Menu used
// by all simulators.
//--------------------------------------------------------------------

#ifndef DRN_MENU_HEADER
#define DRN_MENU_HEADER

#include <Xmw/XmwMenu.h>
#include <Xmw/XmwCallback.h>

#include <GRN/Constants.h>

class DRN_DrawingArea;

class DRN_Menu : public XmwMenu
{ 
  friend class DRN_Interface;
  friend class DRN_KeyInteractor;
  friend class DRN_MouseInteractor;
  friend class DRN_ToolBox;

  CallbackDeclare(DRN_Menu);

public:

  DRN_Menu(Widget parent);

  virtual ~DRN_Menu() { }
  virtual void updateButtons();

  void updateRunningState(int running);
  virtual int save();		// save master file

protected:
	
  // Callbacks inherited from base class

  virtual void open ( Widget, XtPointer, XtPointer );
  virtual void save ( Widget, XtPointer, XtPointer );
  virtual void saveAs ( Widget, XtPointer, XtPointer );
  virtual void print ( Widget, XtPointer, XtPointer );
  virtual void quit ( Widget, XtPointer, XtPointer );

  // New callbacks

  virtual void run ( Widget, XtPointer, XtPointer );
  virtual void pause ( Widget, XtPointer, XtPointer );

  virtual void option ( Widget, XtPointer, XtPointer );
  virtual void parameter ( Widget, XtPointer, XtPointer );
  virtual void view ( Widget, XtPointer, XtPointer );

  virtual void mapFeature ( Widget, XtPointer, XtPointer );
  virtual void roadSegment ( Widget, XtPointer, XtPointer );
  virtual void sensor ( Widget, XtPointer, XtPointer );
  virtual void signal ( Widget, XtPointer, XtPointer );

  virtual void linearColor ( Widget, XtPointer, XtPointer );
  virtual void symbolColor ( Widget, XtPointer, XtPointer );

  virtual void viewMarker ( Widget, XtPointer, XtPointer );
  virtual void eXport ( Widget, XtPointer, XtPointer );

private:

  void findObject ( Widget, XtPointer, XtPointer );
  void segmentAttribute ( Widget, XtPointer, XtPointer );

protected:

  // Simulation pane

  XmtMenuItem *run_;
  XmtMenuItem *pause_;

  // View pane

  XmtMenuItem *view_;
};

extern DRN_Menu * theMenu;

#endif
