//-*-c++-*------------------------------------------------------------
// DRN_SegmentAttributeDialog.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef DRN_SEGMENTATTRIBUTEDIALOG_HEADER
#define DRN_SEGMENTATTRIBUTEDIALOG_HEADER

#include <Xmw/XmwCallback.h>

class DRN_SegmentAttributeDialog : public XmwWidget
{
	  CallbackDeclare(DRN_SegmentAttributeDialog);

   public:

	  DRN_SegmentAttributeDialog(Widget parent);
	  ~DRN_SegmentAttributeDialog() { }

	  void post();

   private:

	  void enter ( Widget, XtPointer, XtPointer );
	  void leave ( Widget, XtPointer, XtPointer );

   private:

	  Widget table_;			// XbaeMatrix
};

#endif
