//-*-c++-*------------------------------------------------------------
// DRN_Lane.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef DRN_LANE_HEADER
#define DRN_LANE_HEADER

#include <GRN/RN_DrawableLane.h>
#include <Xmw/XmwColor.h>

extern const double LANE_MARK_SPACE; // in meters

class DRN_DrawingArea;

class DRN_Lane : public RN_DrawableLane
{
  friend class DRN_Segment;

public:

  DRN_Lane();
  virtual ~DRN_Lane() { }

  inline DRN_Segment * drnSegment() {
	return (DRN_Segment *)segment();
  }

  virtual Pixel calcColor(DRN_DrawingArea *);
  
  virtual int draw(DRN_DrawingArea *);

  virtual int drawCurb(DRN_DrawingArea *);
  virtual int drawMark(DRN_DrawingArea *);

  virtual int drawSpecialMarks(DRN_DrawingArea *);
  virtual int highlight(DRN_DrawingArea * area, Pixel clr);
};


#endif
