//-*-c++-*------------------------------------------------------------
// DRN_Symbols.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//--------------------------------------------------------------------

#include "DRN_Symbols.h"
#include "DRN_Interface.h"

void DRN_Symbols::registerAll()
{
   XmwSymbols::registerAll();

   tools_.create("tools", XtRInt, TOOL_PAN);
   viewType_.create("viewType", XtRInt, VIEW_MICRO);

   segmentColorCode_.create("segmentColorCode", XtRInt, SEGMENT_LINKTYPE);

   sensorTypes_.create("sensorTypes", XtRInt, 0);
   sensorLabel_.create("sensorLabel", XtRInt, SENSOR_LABEL_NONE);
   sensorColorCode_.create("sensorColorCode", XtRInt, SENSOR_OCCUPANCY);

   signalTypes_.create("signalTypes", XtRInt, 0);
   isSignalLabelOn_.create("isSignalLabelOn", XtRBoolean, 0);

   isMapFeaturesOn_.create("isMapFeaturesOn", XtRBoolean, 0);

   isLegendOn_.create("isLegendOn", XtRBoolean, 0);
   isGridOn_.create("isGridOn", XtRBoolean, 0);
   isVersionLabelOn_.create("isVersionLabelOn", XtRBoolean, 0);
}
