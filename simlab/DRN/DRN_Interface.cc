//-*-c++-*------------------------------------------------------------
// DRN_Interface.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class implementation for the class
// DRN_Interface
//--------------------------------------------------------------------

#include <iostream>
#include <cstdlib>
#include <cassert>
using namespace std;

#include <Xmt/Xmt.h>
#include <Xmt/Create.h>
#include <Xmt/Chooser.h>

#include <Xmw/XmwImage.h>
#include <Xmw/XmwFont.h>
#include <Xmw/XmwColor.h>

#include <IO/Communicator.h>
#include <Tools/SimulationEngine.h>
#include <Tools/SimulationClock.h>

#include "DRN_Interface.h"
#include "DRN_Symbols.h"
#include "DRN_Menu.h"
#include "DRN_Modeline.h"
#include "DRN_DrawingArea.h"
#include "DRN_ToolBox.h"
#include "DRN_Logo.h"

DRN_Interface* theDrnInterface = NULL;

DRN_Interface::DRN_Interface(int *argc, char **argv, XmwImage *icon)
   : XmwInterface(argc, argv, icon),
	 drawingArea_(NULL), logo_(NULL)
{
   theDrnInterface = this;
}

DRN_Interface::~DRN_Interface()
{
   delete drawingArea_;
}

// This function is called by the class constructor (MW_Interface)

void DRN_Interface::create()
{
   XmwInterface::create();

   workingArea_ = XmtNameToWidget(widget_, "*workingArea");
   assert(workingArea_);

   // This is the drawing window

   ::theDrawingArea = drawingArea_ = drawingArea(workingArea_);

   // A toolbox contains operation tools (zoom, pan, etc.) and chooser
   // of view types

   ::theToolBox = toolBox_ = toolBox(workingArea_);
}

void DRN_Interface::manage()	// virtual
{
   XmwInterface::manage();
   handlePendingEvents();
}

XmwSymbols* DRN_Interface::createSymbols()
{
   return new DRN_Symbols;
}

XmwMenu* DRN_Interface::menu(Widget parent)
{
   return new DRN_Menu(parent);
}

DRN_DrawingArea* DRN_Interface::drawingArea(Widget w)
{
   return new DRN_DrawingArea(w);
}

DRN_ToolBox* DRN_Interface::toolBox(Widget w)
{
   return new DRN_ToolBox(w);
}

XmwModeline* DRN_Interface::modeline(Widget parent)
{
   return new DRN_Modeline(parent);
}

void DRN_Interface::drawLogo(DRN_DrawingArea *area)
{
   if (!logo_) {
      logo_ = new DRN_Logo(theDrnInterface->widget());
   }
   logo_->draw(area);
}

void DRN_Interface::view(Boolean show)
{
   if (show) {
	  XmwWidget::manage(workingArea_);
	  drawingArea_->setState(DRN_DrawingArea::VIEW_NONE);
   } else {
	  XmwWidget::unmanage(workingArea_);
	  drawingArea_->unsetState(DRN_DrawingArea::VIEW_NONE);
   }
}

void DRN_Interface::tellLoadingIsDone()
{
   const char *msg = XmtLocalize2(widget_,
	  "Click Run button to start.", "prompt", "start");
   msgSet(msg);
   updateButtons();
}


void DRN_Interface::updateButtons()
{
   menu()->updateButtons();
   modeline()->updateButtons();
}


void DRN_Interface::pause(int others)
{
   if (!theSimulationEngine->isRunning()) return;

   // Tell the master controller about the pause

   if (others && theBaseCommunicator &&
	   theBaseCommunicator->isSimlabConnected()) {
	  theBaseCommunicator->simlab() << MSG_PAUSE;
	  theBaseCommunicator->simlab().flush(SEND_IMMEDIATELY);
   }

   toggle_zzz(true);
   theSimulationClock->pause();
   menu()->updateRunningState(False);
   modeline_->setKeyState(XmwModeline::KEY_STATE_PAUSE);
   const char *msg = XmtLocalize2(widget_,
	  "Simulation is paused.", "prompt", "paused");
   msgSet(msg);

   handlePendingEvents();
}

void DRN_Interface::run(int others)
{
   if (theSimulationEngine->isRunning()) return;
 
   if (!theSimulationClock->isStarted()) {
	  theSimulationEngine->loadSimulationFiles();
   }

   if (others && theBaseCommunicator &&
	   theBaseCommunicator->isSimlabConnected()) {
	  theBaseCommunicator->simlab() << MSG_RESUME;
	  theBaseCommunicator->simlab().flush(SEND_IMMEDIATELY);
   }

   toggle_zzz(false);
   theSimulationClock->resume();
   menu()->updateRunningState(True);
   modeline()->setKeyState(XmwModeline::KEY_STATE_RUN);
   const char *msg = XmtLocalize2(widget_,
	  "Simulation is running.", "prompt", "running");
   msgSet(msg);					// no block

   handlePendingEvents();
}

void
DRN_Interface::changeDesiredTimeFactor(double change)
{
   float s = theSimulationClock->desiredTimeFactor() + change;
   if (s < 0.0) s = 0.0;
   else if (s > 10) s = 10.0;
   theSimulationClock->desiredTimeFactor(s);
   theModeline()->updateMeter(0, s);
}

// This procedure starts the simulation loops.

int DRN_Interface::mainloop()
{
   SimulationEngine *engine = theSimulationEngine;
   SimulationClock *clock = theSimulationClock;

   destroyWorkingBox ();
   showDefaultCursor ();

   // In graphical mode, we always wait the user to click start.  This
   // give a chance to change the simulation setups.

   clock->pause();

   if (!engine->canStart()) {	// No master file available
	  const char *msg = XmtLocalize2(widget_,
		 "Choose a master file.", "prompt", "master");
	  msgShow(msg);
   } else if (!theBaseCommunicator->isSimlabConnected()) { // running alone
	  const char *msg = XmtLocalize2(widget_,
		 "Setup or click start to go.", "prompt", "start");
	  msgShow(msg);
   } else if (!clock->isStarted() &&
			  !theSimulationEngine->mode(MODE_HOLD)) {
	  // Automatic start the simulation
	  theSimulationEngine->loadSimulationFiles();
   }

   updateButtons();

   while (engine->state() >= 0) {

	  if (theDrawingArea->state(DRN_DrawingArea::REDRAW)) {
		 theDrawingArea->redraw();
	  }

      if (handlePendingEvents()) {
		 ;						// XEvents have the highest priority

      } else if (engine->receiveMessages() > 0) {
		 ;						// IPC events have the second priority

      } else if (engine->isWaiting() &&
				 engine->receiveMessages(NO_MSG_WAITING_TIME) >= 0) {
		 ;						// Wait for IPC events

	  } else if (clock->isPaused()) {

		 // If simulation is paused, we wait for XEvents or IPC events
		 // to occurs.  Here we will partially block the process by
		 // waiting for XEvent when the simulation is paused.  A dummy
		 // event is generated to break the waiting for XEvents in
		 // order to process IPC events periodically (e.g. update the
		 // network state, start, quit, and so on)

		 waitNextEvent(2000);

      } else if (engine->isRunning()) {
		 eventLoop();			// Real simulation loop
      }
   }
   return 0;
}

// This function calls the simulationLoop() of the SimulationEngine
// and then updates the graphics

void DRN_Interface::eventLoop()
{
   static double sleep = 0.0;
   const double regulator = 0.75;
   const double epsilon = 1.0E-3;

   SimulationClock *clock = theSimulationClock;
   SimulationEngine *engine = theSimulationEngine;

   // This calls the actual simulation loop. It also updates the clock
   // and set simulation state.
   
   if (engine->simulationLoop() < 0) {
	  engine->quit(engine->state());
   }

   // Update modeline

   double dec = clock->currentTime() - (long int)clock->currentTime();
   if (dec > -epsilon && dec < epsilon) {
	  modeline()->update();
   }

   // Checks if the simulation should be slowed down.

   sleep += clock->waitTime() * regulator;
   
   if (sleep < 1.0E-3) {
	  sleep = 0.0;				// no sleep
   } else {
	  // Wait for next XEvent which will be terminated by a timeout
	  // dummy events
	  unsigned long int ms = (int) (1000.0 * sleep);
	  waitNextEvent(ms);
   }
}

void DRN_Interface::NoNetwork()
{
  msgSet(XmtLocalize2(widget_, "No network",
		      "prompt", "noNetwork"));
}
