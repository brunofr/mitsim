//-*-c++-*------------------------------------------------------------
// DRN_ExportDialog.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef DRN_EXPORT_DIALOG_HEADER
#define DRN_EXPORT_DIALOG_HEADER

#include <Xmw/XmwDialogManager.h>

struct ExportSpec {		// add more members later
  int type ;
} ;

class DRN_ExportDialog : public XmwDialogManager
{
  friend class XmwInterface;

public:

  DRN_ExportDialog(Widget parent);
	
  ~DRN_ExportDialog() { }

  void post();				// virtual

protected:

  // overload the virtual functions

  void okay(Widget w, XtPointer tag, XtPointer data);
  void cancel(Widget w, XtPointer tag, XtPointer data);
  void help(Widget w, XtPointer tag, XtPointer data);

private:

  static XtResource resources[];
  static ExportSpec exportSpec ;
};

#endif
