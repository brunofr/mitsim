//-*-c++-*------------------------------------------------------------
// DrawableRoadNetwork.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class definition for the class
// DrawableRoadNetwork
//--------------------------------------------------------------------

#ifndef DRAWABLEROADNETWORK_HEADER
#define DRAWABLEROADNETWORK_HEADER

#include <Xm/Xm.h>
#include <GRN/RoadNetwork.h>

class XmwPoint;
class DRN_Sensor;
class DRN_Signal;
class DRN_Segment;
class DRN_Lane;
class DRN_DrawingArea;
class DRN_Node;

class DrawableRoadNetwork : public RoadNetwork
{
   public:

      DrawableRoadNetwork();

      virtual ~DrawableRoadNetwork();

      static DrawableRoadNetwork* network() {
		 return (DrawableRoadNetwork *)theNetwork;
      }

      // These functions should be overloaded in the inherited class.

      virtual RN_Segment* newSegment();
      virtual RN_Lane* newLane();
      virtual RN_Sensor* newSensor();
      virtual RN_Signal* newSignal();
      virtual RN_Node* newNode();
  
	  inline DRN_Node* drnNode(int i) {
		 return (DRN_Node*) RoadNetwork::node(i);
	  }
      inline DRN_Segment* drnSegment(int i) {
		 return (DRN_Segment*) RoadNetwork::segment(i);
      }
      inline DRN_Lane* drnLane(int i) {
		 return (DRN_Lane*) RoadNetwork::lane(i);
      }

      virtual void calcGeometricData();

      virtual void unsetDrawingStates(DRN_DrawingArea *);

      // This draws all network objects such as link/lanes, sensors,
      // and signals

      virtual void drawNetwork(DRN_DrawingArea * area);
      
      // These draw the subsets of network objects and should be
      // called by drawNetwork() or after drawRoads() is called.

      virtual void drawRoads(DRN_DrawingArea * area);
      virtual void drawNodes(DRN_DrawingArea * area);
      virtual void drawSensors(DRN_DrawingArea * area);
      virtual void drawSignals(DRN_DrawingArea * area);
      virtual void updateRoads(DRN_DrawingArea * area);
      
      virtual DRN_Segment * nearestSegment(const WcsPoint& pressed);

      virtual void updateProgress(const char *msg = NULL);
	  virtual void updateProgress(float pct);

	  virtual void queryVehicle(const XmwPoint&) { }
	  virtual void traceVehicle(const XmwPoint&) { }
	  virtual void traceVehicle() { }
	  virtual void setIncident(const XmwPoint&) { }

      void selectNode(const XmwPoint&);
	  void editSignalStates(const XmwPoint&);

   private:

	  static void editSignalStatesCB(Widget, XtPointer station, XtPointer);
};

extern DrawableRoadNetwork * theDrawableNetwork;

#endif
