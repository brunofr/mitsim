//-*-c++-*------------------------------------------------------------
// DRN_ToolBox.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <cassert>

#include <Tools/ToolKit.h>

#include <Xmt/Xmt.h>
#include <Xmt/Create.h>
#include <Xmt/Chooser.h>

#include "DRN_ToolBox.h"
#include "DRN_Interface.h"
#include "DRN_Symbols.h"
#include "DRN_DrawingArea.h"
#include "DRN_KeyInteractor.h"
#include "DRN_Menu.h"
#include "DRN_Legend.h"
#include "DrawableRoadNetwork.h"
using namespace std;

DRN_ToolBox *theToolBox = NULL;

DRN_ToolBox::DRN_ToolBox(Widget parent) : XmwWidget("*toolBox")
{
   theToolBox = this;

   widget_ = XmtNameToWidget(parent, name_);
   assert(widget_);

   actions_ = XmtNameToWidget(widget_, "actions");
   assert(actions_);

   // Link the choosers to the internal symbols

   tools_ = theDrnSymbols()->associate(widget_, "tools");
   viewType_ = theDrnSymbols()->associate(widget_, "viewType");

   // REGISTER CALLBACKS

   // Redraw the window when view type changes

   XtAddCallback(tools_, XmNvalueChangedCallback,
				 &DRN_ToolBox::toolsCB, NULL);

   XtAddCallback(viewType_, XmNvalueChangedCallback,
				 &DRN_ToolBox::viewTypeCB, NULL);

   // Register tips

   WidgetList children;
   Cardinal num;

   // Fetch children, register callbacks and tips

   // Actions

   static String ActionTips[] = {
	  "home","find",NULL
   };
   XtVaGetValues(actions_, XmtNnumItems, &num,
				 XmtNitemWidgets, &children, NULL);
   assert(children[0]);
   assert(children[1]);
   XtAddCallback(children[0],
				 XmNactivateCallback, &DRN_ToolBox::homePositionCB,
				 NULL );
   XtAddCallback(children[1],
				 XmNactivateCallback, &DRN_ToolBox::findObjectCB,
				 NULL );
   XmwInterface::addTips(children, ActionTips, num);

   // Tools: Should be consistent with definitions in DRN_Symbols.h
   // and drnlayout.ad files.

   static String ToolTips[] = {
	  "pan","zoomin","zoomout","center",
	  "rotate","query","legend","draw",
	  "text","trace",NULL
   };
   XtVaGetValues(tools_, XmtNnumItems, &num,
				 XmtNitemWidgets, &children, NULL);
   XmwInterface::addTips(children, ToolTips, num);

   // View types

   static String ViewTypeTips[] = {
	  "micro","macro","odflow",NULL
   };
   XtVaGetValues(viewType_, XmtNnumItems, &num,
				 XmtNitemWidgets, &children, NULL);
   XmwInterface::addTips(children, ViewTypeTips, num);
}

DRN_ToolBox::~DRN_ToolBox()
{
   destroy(actions_);
   destroy(tools_);
   destroy(viewType_);
}

void DRN_ToolBox::toolsCB(Widget w, XtPointer, XtPointer)
{
   static int prev_tool = -1;
   int tool = theDrnSymbols()->tools().value();

   if (ToolKit::debug()) {
	  cout << "Tools: " << tool << endl;
   }
   if (!theNetwork) {
	  theDrnInterface->msgShow(XmtLocalize2(w,
			"No network loaded.", "prompt", "noNetwork"),
			MSG_STAY_TIME);
	  return;
   }
   switch (tool) {
	  case DRN_Symbols::TOOL_PAN:
		 theDrnInterface->msgShow(XmtLocalize2(w,
			"Drag left button to pan.", "prompt", "helpPan"),
			MSG_STAY_TIME);
		 break;
	  case DRN_Symbols::TOOL_ZOOMIN:
		 if (tool == prev_tool) {
			theDrawingArea->zoom((KeySym)XK_i);
		 }
		 theDrnInterface->msgShow(XmtLocalize2(w,
			"Drag right button to zoom in.", "prompt", "helpZoomIn"),
			MSG_STAY_TIME);
		 break;
	  case DRN_Symbols::TOOL_ZOOMOUT:
		 theDrnInterface->msgShow(XmtLocalize2(w,
			"Click left button to zoom out.", "prompt", "helpZoomOut"),
			MSG_STAY_TIME);
		 theDrawingArea->zoom((KeySym)XK_o);
		 break;
	  case DRN_Symbols::TOOL_CENTER:
		 theDrnInterface->msgShow(XmtLocalize2(w,
			"Click left button to set center.", "prompt", "helpCenter"),
			MSG_STAY_TIME);
		 break;
	  case DRN_Symbols::TOOL_ROTATE:
		 theDrnInterface->msgShow(XmtLocalize2(w,
			"Drag left button to rotate.", "prompt", "helpRotate"),
			MSG_STAY_TIME);
		 break;
	  case DRN_Symbols::TOOL_QUERY:
		 theDrnInterface->msgShow(XmtLocalize2(w,
			"Click left button to query.", "prompt", "helpQuery"),
			MSG_STAY_TIME);
		 break;
	  case DRN_Symbols::TOOL_LEGEND:
		 theDrnInterface->msgShow(XmtLocalize2(w,
			"Click left button to place legend.", "prompt", "helpLegend"),
			MSG_STAY_TIME);
		 if (theDrnSymbols()->isLegendOn().value()) {
		   theDrnSymbols()->isLegendOn().set(false);
		 } else {
		   theDrnSymbols()->isLegendOn().set(true);
		 }
		 theDrawingArea->setXorMode();
		 theDrawingArea->legend()->draw();
		 break;
	  case DRN_Symbols::TOOL_DRAW:
		 theDrnInterface->msgShow(XmtLocalize2(w,
			"Use left button to draw.", "prompt", "helpDraw"),
			MSG_STAY_TIME);
		 break;
	  case DRN_Symbols::TOOL_TEXT:
		 theDrnInterface->msgShow(XmtLocalize2(w,
			"Type text and press <Enter>.", "prompt", "helpText"),
			MSG_STAY_TIME);
		 break;
	  case DRN_Symbols::TOOL_TRACE:
		 theDrnInterface->msgShow(XmtLocalize2(w,
		   "Click left mouse button to trace a vehicle.", "prompt",
		   "helpTrace"),
		   MSG_STAY_TIME);
		 if (theDrnSymbols()->viewType().is(DRN_Symbols::VIEW_MICRO)) {
		   theDrawableNetwork->traceVehicle();
		 }
		 break;
	  default:
		 break;
   }
   prev_tool = tool;
}

void DRN_ToolBox::viewTypeCB(Widget, XtPointer, XtPointer)
{
   if (ToolKit::debug()) {
	  int tool = theDrnSymbols()->viewType().value();
	  cout << "View: " << tool << endl;
   }
   theDrawingArea->setState(DRN_DrawingArea::REDRAW);
}


void DRN_ToolBox::homePositionCB(Widget, XtPointer, XtPointer)
{
   theDrawingArea->setHomePosition();
   theDrawingArea->redraw();
}

void DRN_ToolBox::findObjectCB(Widget, XtPointer, XtPointer)
{
   theMenu->findObject(NULL, NULL, NULL);
}
