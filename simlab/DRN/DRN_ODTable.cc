//-*-c++-*------------------------------------------------------------
// NAME: Drawable Road Network
// AUTH: Qi Yang
// FILE: DRN_ODTable.C
// DATE: Sun Feb 11 15:24:17 1996
//--------------------------------------------------------------------

#include "DRN_ODTable.h"
#include "DRN_ODCell.h"
#include "DRN_Node.h"
#include "DRN_DrawingArea.h"

// Loop throught each od pairs and draw its value with color coded
// lines

void
DRN_ODTable::draw(DRN_DrawingArea * area)
{
   od_list_link_type cell = cells_.tail();
   RN_Node* selection = DRN_Node::selectedNode();

   if (selection) {
      while (cell) {
		 DRN_ODCell *i = (DRN_ODCell *) cell->data();
		 if (i->oriNode() == selection ||
			 i->desNode() == selection) {
			 i->draw(area);
		 }
		 cell = cell->prev();
      }
   } else {
	 
      while (cell) {
		 DRN_ODCell *i = (DRN_ODCell *) cell->data();
		 i->draw(area);
		 cell = cell->prev();
      }
   }
}
