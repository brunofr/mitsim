//-*-c++-*------------------------------------------------------------
// DRN_Node.h
//
// Peter Welch and Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef DRN_NODE_HEADER
#define DRN_NODE_HEADER


#include <Xm/Xm.h>

#include <GRN/RN_DrawableNode.h>
#include <GRN/Constants.h>

const int DEFAULT_NODE_RADIUS = 10;  // pixels

class XmwPoint;
class DRN_Node;
class DRN_DrawingArea;

class DRN_Node : public RN_DrawableNode
{
  friend class DRN_MouseInteractor;

public:

  DRN_Node() : RN_DrawableNode() { }

  virtual ~DRN_Node() { }

  static void radius(int r) { radius_ = r; }
  static double radius() { return radius_; }
  static DRN_Node * selectedNode() { return selectedNode_; }
  static void query(const XmwPoint& point);
  static void queryCB(Widget, XtPointer, XtPointer);

  virtual void draw(DRN_DrawingArea * area);
  virtual void flowDiaglog();

  int isVisible() { return state_ & STATE_VISIBLE; }
  int isSelected() { return state_ & STATE_SELECTED; }

protected:

  static int radius_;
  static DRN_Node * selectedNode_;
};

#endif
