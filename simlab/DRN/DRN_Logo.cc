//-*-c++-*------------------------------------------------------------
// DRN_Logo.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//--------------------------------------------------------------------

#include <cassert>

#include "DRN_Logo.h"
#include "DRN_DrawingArea.h"
#include "DRN_Interface.h"
#include "DRN_Symbols.h"
using namespace std;

// This shows a logo when the application is loading data.

DRN_Logo::DRN_Logo(Widget parent)
{
  if (!(pixmap_ = XmtGetPixmap(parent, NULL, "mylogo")) &&
	  !(pixmap_ = XmtGetPixmap(parent, NULL, "logo"))) {
	cerr << "No logo or no enough colors." << endl;
	return;
  }

  Window win;
  int x, y;
  unsigned int border;

  XGetGeometry(XtDisplay(parent),
			   pixmap_,		// the drawable
			   &win,			// window id returned
			   &x, &y,			// window position (always zero)
			   &width_, &height_, // size of the drawable
			   &border,		// border width (always zero)
			   &depth_			// bits per pixel
			   );
}

void DRN_Logo::draw(DRN_DrawingArea *area, int dx, int dy)
{
  if (!pixmap_ || !XtIsRealized(area->widget()))  {
	return;
  } else if (!area->isInitialized()) {
	area->initialize();
  }

  dx += (area->width() - width_) / 2;
  dy += (area->height() - height_) / 2;

  XCopyArea(area->display(), pixmap_, area->window(),
			area->gc(),
			0, 0, width_, height_, dx, dy);

  if (theDrnSymbols()->isVersionLabelOn().value()) {
	area->drawVersion();
  }

  theDrnInterface->handlePendingEvents();
}

