//-*-c++-*------------------------------------------------------------
// DRN_MapFeature.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//--------------------------------------------------------------------

#include <new>
#include <cstdlib>

#include <Xmt/Xmt.h>

#include <Tools/ToolKit.h>
#include <Tools/VariableParser.h>

#include <IO/Exception.h>
#include <GDS/GDS_Glyph.h>

#include "DRN_Menu.h"
#include "DRN_DrawingArea.h"
#include "DRN_MapFeature.h"
using namespace std;

DRN_MapFeature * theMapFeatures = NULL;


DRN_MapFeature::DRN_MapFeature()
   :wklayer_(0)
{
}


DRN_MapFeature::~DRN_MapFeature()
{
   for (int i = 0; i < glyphs_.size(); i ++) {
      delete glyphs_[i];
   }
}

void DRN_MapFeature::draw(DRN_DrawingArea *area)
{
   for (int i = 0; i < nGlyphs(); i ++) {
      glyph(i)->draw(area);
   }
}


void
DRN_MapFeature::load(GenericVariable &gv)
{
   GDS_Glyph *glyph;
   if (gv.nElements() == 1) {

      // Only the file name. No scaling control

	 if (ToolKit::isValidInputFilename(gv.element())) {
	   glyph = new GDS_Glyph(gv.element(), MIN_SCALE, MAX_SCALE);
	   glyphs_.push_back(glyph);
	 }

   } else {

      // Filename with scaling controls

      int k, num = gv.nElements() / 3;
      for (int i = 0; i < num; i ++) {
		 k = 3 * i;
		 if (ToolKit::isValidInputFilename(gv.element(k))) {
		   glyph = new GDS_Glyph(gv.element(k),
								 gv.element(k+1),
								 gv.element(k+2));
		   glyphs_.push_back(glyph);
		 }
      }
   }
}

void DRN_MapFeature::addLabel(const XmwPoint &p, const char *label)
{
   const char *name = "wklayer.gds";
   for (int i = 0; !wklayer_ && i < glyphs_.size(); i ++) {
	  if (strcmp(glyph(i)->name(), name) == 0) {
		 wklayer_ = glyph(i);
	  }
   }
   if (wklayer_) {
	  if (!wklayer_->isLoaded()) {
		 wklayer_->load();		// read the file now
	  }
   } else {
	  wklayer_ = new GDS_Glyph(name, MIN_SCALE, MAX_SCALE);
	  glyphs_.push_back(wklayer_);
	  wklayer_->setState(GDS_Glyph::STATE_LOADED);	// direct drawing
   }
   WcsPoint dp = theDrawingArea->window2Database(p);
   wklayer_->add(dp, label);
   wklayer_->setState(GDS_Glyph::STATE_MODIFIED);
   theMenu->needSave(XmwMenu::STATE_SAVE_GDS);
}

void DRN_MapFeature::save()
{
   for (int i = 0; i < nGlyphs(); i ++) {
      glyph(i)->save();
   }   
}
