//-*-c++-*------------------------------------------------------------
// FILE:    DRN_Signal.C
// AUTHOR:  Qi Yang 
// DATE:    Sat Jun 3 1995
//--------------------------------------------------------------------

#include <cstdio>
#include <cstring>

#include <Xmt/Xmt.h>

#include <Tools/ToolKit.h>
#include <Tools/Math.h>
#include <GRN/Constants.h>
#include <GRN/RN_Link.h>
#include <GRN/RN_CtrlStation.h>
#include <GRN/Parameter.h> //IEM(Aug13)

#include "DrawableRoadNetwork.h"
#include "DRN_Interface.h"
#include "DRN_Menu.h"
#include "DRN_DrawingArea.h"
#include "DRN_Segment.h"
#include "DRN_Lane.h"
#include "DRN_Signal.h"
#include "DRN_Symbols.h"
using namespace std;

DRN_Signal::DRN_Signal()
   : RN_Signal(),
	 shownState_(0),
	 onScreen_(0)
{
}


void DRN_Signal::calcGeometricData()
{
   RN_DrawableSegment * ps = (RN_DrawableSegment *)segment();
   RN_DrawableLane * pl = (RN_DrawableLane *)lane();
   angle_  = pl->tangentAngle(position());
   if (isLinkWide()) {
      center_ = ps->centerPoint(position());
   } else {
      center_ = pl->centerPoint(position());
   }
}


// ------------------------------------------------------------------
//  Find a color to show the signal state.
// ------------------------------------------------------------------

Pixel *
DRN_Signal::calcColor(unsigned int s)
{
   static Pixel clr[2];

   // FIND THE FILL OR PRIMER COLOR

   switch (s & SIGNAL_COLOR) {
      case SIGNAL_RED:
		 {
			clr[0] = clr[1] = theColorTable->lightRed();
			break;
		 }
      case SIGNAL_YELLOW:
		 {
			clr[0] = clr[1] = theColorTable->yellow();
			break;
		 }
      case SIGNAL_GREEN:
		 {
			clr[0] = clr[1] = theColorTable->lightGreen();
			break;
		 }
      case SIGNAL_BLANK:
		 {
			clr[0] = clr[1] = theColorTable->darkGray();
			break;
		 }
      default:
		 {
			clr[0] = clr[1] = theColorTable->cyan();
			break;
		 }
   }

   // COLOR FOR BORDER OR ARMS (show if the movement is protected)

   switch (s & SIGNAL_USAGE) {
      case SIGNAL_USAGE:	// Arrow + Flashing:
		 {
			clr[1] = theColorTable->green();
			break;
		 }
      case SIGNAL_ARROW:	// Arrow w/o flashing
		 {
			clr[1] = theColorTable->cyan();
			break;
		 }
      case SIGNAL_FLASHING:	// Flashing w/o arrow
		 {
			clr[1] = theColorTable->darkBlue();
			break;
		 }
   }
   return clr;
}


int
DRN_Signal::isChosen()
{
   XmwSymbol<int>& c = theDrnSymbols()->signalTypes();
   switch (type())
   {
      case CTRL_PS:
		 {
			return c.isOn(DRN_Symbols::SIGNAL_PS);
		 }
      case CTRL_TS:
		 {
			return c.isOn(DRN_Symbols::SIGNAL_TS);
		 }
      case CTRL_VSLS:
		 {
			return c.isOn(DRN_Symbols::SIGNAL_VSLS);
		 }
      case CTRL_VMS:
		 {
			return c.isOn(DRN_Symbols::SIGNAL_VMS);
		 }
      case CTRL_LUS:
		 {
			return c.isOn(DRN_Symbols::SIGNAL_LUS);
		 }
      case CTRL_RAMPMETER:
		 {
			return c.isOn(DRN_Symbols::SIGNAL_RAMPMETER);
		 }
      case CTRL_TOLLBOOTH:	// These are always shown
      case CTRL_BUSSTOP:
      case CTRL_INCIDENT:
      default:
		 {
			return 1;
		 }
   }
}


// -----------------------------------------------------------------
// Draw a signal with given shape. It returns 1 if the signal
// is visible or 0 otherwise.
// -----------------------------------------------------------------

int
DRN_Signal::draw(DRN_DrawingArea * area)
{
   if (!isChosen()) return 0;

   area->setXorMode();

   if (area->state(DRN_DrawingArea::SIGNAL_ON) && onScreen_) {
      if (state_ == shownState_) {
		 // Nothing changes
		 return onScreen_;
      } else {			// erase the old image
		 drawShape(area);
      }
   }

   shownState_ = state_;
   onScreen_ = drawShape(area);
   return onScreen_;
}


int
DRN_Signal::drawShape(DRN_DrawingArea * area)
{
   int visible = 0;

   switch (type()) {
      case CTRL_TS:		// color will be defined later
		 {
			visible = drawTS(area);
			break;
		 }
      case CTRL_PS:
		 {
			visible = drawBar(area, calcColor(shownState_));
			break;
		 }
      case CTRL_VSLS:
		 {
			Pixel clr;
			unsigned int lmt = segment()->speedLimit(); 
			if (shownState_ < lmt / 2) {
			   clr = theColorTable->red();
			} else if (shownState_ < lmt) {
			   clr = theColorTable->yellow();
			} else {
			   clr = theColorTable->green();
			}
			visible = drawNumber(area, clr);
			break;
		 }
      case CTRL_VMS:
		 {
			visible = drawNumber(area, theColorTable->cyan(), "%8.8x");
			break;
		 }
      case CTRL_LUS:
		 {
			Pixel *clr = calcColor(shownState_);
			if (state_ == SIGNAL_GREEN) {
			   visible = drawArrow(area, clr);
			} else if (state_ & SIGNAL_RIGHT) {
			   visible = drawArrow(area, clr, -0.25 * M_PI);
			} else if (state_ & SIGNAL_LEFT) {
			   visible = drawArrow(area, clr, 0.25 * M_PI);
			} else {
			   visible = drawCross(area, clr);
			}
			break;
		 }
      case CTRL_RAMPMETER:
		 {
			visible = drawPie(area, calcColor(shownState_));
			break;
		 }
      case CTRL_TOLLBOOTH:
		 {
			if (isEtc()) {
			   visible = drawLine(area, *calcColor(shownState_));
			} else {
			   visible = drawBooth(area, calcColor(shownState_));
			}
			break;
		 }
      case CTRL_BUSSTOP:  //margaret
		 {
			visible = drawBusStop(area, calcColor(shownState_));
			break;
		 }      
      case CTRL_INCIDENT:
		 {
			if (shownState_ & INCI_STOP) {
			   visible = drawFilledRectangleCross(area, calcColor(shownState_));
			} else {
			   visible = drawRubberNeck(area, calcColor(shownState_));
			}
			break;
		 }
      default:
		 {
			cerr << "Error: Unknown shape for signal <"
				 << code_ << ">." << endl;
			break;
		 }
   }

   // Label the signal

   if (visible && theDrnSymbols()->isSignalLabelOn().value() &&
	   station_->signal(0) == this) {
	  char label[12];
	  sprintf(label, "%d", code());
	  RN_DrawableSegment *ps = (RN_DrawableSegment *) segment();
      double size = area->fontHeight() / area->meter2Pixels();
      double pos = position() - size / ps->length();
      WcsPoint p(ps->centerPoint(pos));
	  area->foreground(theColorTable->yellow());
      area->draw(p, label);
   }

   return visible;
}



// ----------------------------------------------------------------
// Draw an arrow with color showing current state
// ----------------------------------------------------------------

int
DRN_Signal::drawArrow(DRN_DrawingArea * area, Pixel *clr, double beta)
{
   static XmwPoint points[4];
   const double size = 0.9 * SIGNAL_SIZE;
   const double arrow_size = size * SIGNAL_ARROW_SIZE;
   beta += angle_;

   WcsPoint p0(center_.bearing(size, beta));
   if (!area->world2Window(p0, points[0])) return 0;
	
   WcsPoint p1(center_.bearing(size, beta + ONE_PI));
   if (!area->world2Window(p1, points[1])) return 0;

   WcsPoint p2(p0.bearing(arrow_size,
						  beta + SIGNAL_ARROW_ANGLE + ONE_PI));
   if (!area->world2Window(p2, points[2])) return 0;

   WcsPoint p3(p0.bearing(arrow_size,
						  beta - SIGNAL_ARROW_ANGLE + ONE_PI));
   if (!area->world2Window(p3, points[3])) return 0;

   area->lineWidth(area->thickLine());

   area->foreground(clr[1]);
   area->XmwDrawingArea::drawLine(points[0], points[1]);

   area->foreground(clr[0]);
   area->XmwDrawingArea::drawLine(points[0], points[2]);
   area->XmwDrawingArea::drawLine(points[0], points[3]);

   area->lineWidth(0);
   
   return 1;
}


// -----------------------------------------------------------------
//  Draw a cross with color showing current state
// -----------------------------------------------------------------

int
DRN_Signal::drawCross(DRN_DrawingArea * area, Pixel *clr)
{
   static XmwPoint points[4];
   const double size = 0.8 * SIGNAL_SIZE;

   WcsPoint p0(center_.bearing(size, angle_ + 0.75 * ONE_PI));
   if (!area->world2Window(p0, points[0])) return 0;
      
   WcsPoint p1(center_.bearing(size, angle_ - 0.75 * ONE_PI));
   if (!area->world2Window(p1, points[1])) return 0;

   WcsPoint p2(center_.bearing(size, angle_ - 0.25 * ONE_PI));
   if (!area->world2Window(p2, points[2])) return 0;

   WcsPoint p3(center_.bearing(size, angle_ + 0.25 * ONE_PI));
   if (!area->world2Window(p3, points[3])) return 0;
   
   area->lineWidth(area->thickLine());

   area->foreground(clr[0]);
   area->XmwDrawingArea::drawLine(points[0], points[2]);
   
   area->foreground(clr[1]);
   area->XmwDrawingArea::drawLine(points[1], points[3]);

   area->lineWidth(0);

   return 1;
}


// ----------------------------------------------------------------
// Draw a cross and a box with color showing current current state
// ----------------------------------------------------------------

int
DRN_Signal::drawBoxedCross(DRN_DrawingArea * area, Pixel *clr)
{
   static XPoint points[5];
   const double size = 0.75 * SIGNAL_SIZE;

   WcsPoint p0(center_.bearing(size, angle_ + 0.75 * ONE_PI));
   if (!area->world2Window(p0, points[0])) return 0;
      
   WcsPoint p1(center_.bearing(size, angle_ - 0.75 * ONE_PI));
   if (!area->world2Window(p1, points[1])) return 0;
   
   WcsPoint p2(center_.bearing(size, angle_ - 0.25 * ONE_PI));
   if (!area->world2Window(p2, points[2])) return 0;

   WcsPoint p3(center_.bearing(size, angle_ + 0.25 * ONE_PI));
   if (!area->world2Window(p3, points[3])) return 0;

   points[4] = points[0];
	
   area->foreground(clr[0]);
   area->XmwDrawingArea::drawLine(points[0], points[2]);
   area->XmwDrawingArea::drawLine(points[1], points[3]);

   area->foreground(clr[1]);
   area->XmwDrawingArea::drawLines(points, 5);
   
   return 1;
}

int
DRN_Signal::drawFilledRectangleCross(DRN_DrawingArea * area, Pixel *clr)
{
   static XPoint points[5];
   const double size = 0.75 * SIGNAL_SIZE;

   WcsPoint p0(center_.bearing(size, angle_ + 0.75 * ONE_PI));
   if (!area->world2Window(p0, points[0])) return 0;
      
   WcsPoint p1(center_.bearing(size, angle_ - 0.75 * ONE_PI));
   if (!area->world2Window(p1, points[1])) return 0;
   
   WcsPoint p2(center_.bearing(size, angle_ - 0.25 * ONE_PI));
   if (!area->world2Window(p2, points[2])) return 0;

   WcsPoint p3(center_.bearing(size, angle_ + 0.25 * ONE_PI));
   if (!area->world2Window(p3, points[3])) return 0;

   points[4] = points[0];
	
   area->foreground(clr[1]);
   area->XmwDrawingArea::drawFilledPolygon(points, 5, Convex);
   
   area->foreground(clr[0]);
   area->XmwDrawingArea::drawLine(points[0], points[2]);
   area->XmwDrawingArea::drawLine(points[1], points[3]);

   return 1;
}

int
DRN_Signal::drawRubberNeck(DRN_DrawingArea * area, Pixel *clr)
{
   static XPoint points[5];
   const double size = 0.75 * SIGNAL_SIZE;

   WcsPoint p0(center_.bearing(size, angle_ + 0.75 * ONE_PI));
   if (!area->world2Window(p0, points[0])) return 0;
      
   WcsPoint p1(center_.bearing(size, angle_ - 0.75 * ONE_PI));
   if (!area->world2Window(p1, points[1])) return 0;
   
   WcsPoint p2(center_.bearing(size, angle_ - 0.25 * ONE_PI));
   if (!area->world2Window(p2, points[2])) return 0;

   WcsPoint p3(center_.bearing(size, angle_ + 0.25 * ONE_PI));
   if (!area->world2Window(p3, points[3])) return 0;

   points[4] = points[0];
	
   area->foreground(clr[0]);
   area->XmwDrawingArea::drawLine(points[0], points[2]);
   area->XmwDrawingArea::drawLine(points[1], points[3]);

   area->foreground(clr[1]);
   area->XmwDrawingArea::drawLine(points[0], points[3]);
   area->XmwDrawingArea::drawLine(points[1], points[2]);
   
   return 1;
}

// -----------------------------------------------------------------
//  Draw an filled circle with color showing current signal state
// -----------------------------------------------------------------

int
DRN_Signal::drawPie(DRN_DrawingArea * area, Pixel *clr,
					double alpha)
{
   static XmwPoint point;

   const double size = 0.5 * SIGNAL_SIZE;
   const double offset = 0.8 * SIGNAL_SIZE;

   int radius = Round(size * area->meter2Pixels());
   if (radius < 1) return 0;

   WcsPoint p(center_.bearing(offset, alpha));

   if (!area->world2Window(p, point)) return 0;

   area->foreground(clr[0]);
   area->XmwDrawingArea::drawPie(point, radius);

   area->foreground(clr[1]);
   area->XmwDrawingArea::drawCircle(point, radius);

   return 1;
}

int
DRN_Signal::drawPie(DRN_DrawingArea * area, Pixel *clr)
{
   static XmwPoint point;
   const double size = 0.5 * SIGNAL_SIZE;

   int radius = Round(size * area->meter2Pixels());
   if (radius < 1) return 0;

   if (!area->world2Window(center_, point)) return 0;

   area->foreground(clr[0]);
   area->XmwDrawingArea::drawPie(point, radius);

   area->foreground(clr[1]);
   area->XmwDrawingArea::drawCircle(point, radius);

   return 1;
}


// ----------------------------------------------------------------
// Draw an string (message or speed limit)
// ----------------------------------------------------------------


int
DRN_Signal::drawNumber(DRN_DrawingArea * area, Pixel clr,
					   const char *fmt)
{
   static XmwPoint point;
   
   if (!area->world2Window(center_, point)) return 0;

   char sign[12];
   sprintf(sign, fmt, state_);
   area->foreground(clr);
   area->XmwDrawingArea::drawCString(sign, point);

   return 0;
}


// ------------------------------------------------------------------
// Draw a toll booth (a large L)
// ------------------------------------------------------------------

int
DRN_Signal::drawBooth(DRN_DrawingArea * area, Pixel *clr)
{
   XmwPoint points[3];
   double size = 0.5 * ((DRN_Lane *)lane())->width(position());

   WcsPoint p0(center_.bearing(size, angle_ + HALF_PI));
   if (!area->world2Window(p0, points[0])) return 0;

   WcsPoint p1(center_.bearing(size, angle_ - HALF_PI));
   if (!area->world2Window(p1, points[1])) return 0;

   WcsPoint p2(p0.bearing(LANE_WIDTH, angle_ + ONE_PI));
   if (!area->world2Window(p2, points[2])) return 0;

   area->foreground(clr[0]);
   area->XmwDrawingArea::drawLine(points[0], points[1]);

   area->lineWidth(area->thickLine());
   area->foreground(theColorTable->darkGray());
   area->XmwDrawingArea::drawLine(points[0], points[2]);
   area->lineWidth(0);

   return 1;
}

// ------------------------------------------------------------------
// Draw a Bus Stop --margaret
// ------------------------------------------------------------------

int
DRN_Signal::drawBusStop(DRN_DrawingArea * area, Pixel *clr)
{
   XmwPoint points[3];
   double size = 0.5 * ((DRN_Lane *)lane())->width(position());

   WcsPoint p0(center_.bearing(size, angle_ + HALF_PI));
   if (!area->world2Window(p0, points[0])) return 0;

   WcsPoint p1(center_.bearing(size, angle_ - HALF_PI));
   if (!area->world2Window(p1, points[1])) return 0;

   //IEM(Aug13) Make the yellow line span the bus stop's length.
   float length = stopLength() * theBaseParameter->lengthFactor();
   WcsPoint p2(p0.bearing(length, angle_ + ONE_PI));
   if (!area->world2Window(p2, points[2])) return 0;

   // WcsPoint p2(p0.bearing(LANE_WIDTH, angle_ + ONE_PI));
   // if (!area->world2Window(p2, points[2])) return 0;

   area->foreground(clr[0]);
   area->XmwDrawingArea::drawLine(points[0], points[1]);

   area->lineWidth(area->thickLine());
   area->foreground(theColorTable->yellow()); //can we designate bus stops a different color -- margaret
   area->XmwDrawingArea::drawLine(points[0], points[2]);
   area->lineWidth(0);

   return 1;
}

// ------------------------------------------------------------------
// Draw a thick line cross lanes (portal signal)
// ------------------------------------------------------------------

int
DRN_Signal::drawBar(DRN_DrawingArea * area, Pixel *clr)
{
   XmwPoint points[2];
   double size;

   if (isLinkWide()) {
	  size = 0.5 * ((DRN_Segment *)segment())->width(position());
   } else {
	  size = 0.5 * ((DRN_Lane *)lane())->width(position());
   }

   WcsPoint p0(center_.bearing(size, angle_ + HALF_PI));
   if (!area->world2Window(p0, points[0])) return 0;

   WcsPoint p1(center_.bearing(size, angle_ - HALF_PI));
   if (!area->world2Window(p1, points[1])) return 0;

   area->foreground(clr[0]);

   area->lineWidth(area->thickLine());
   area->XmwDrawingArea::drawLine(points[0], points[1]);
   area->lineWidth(0);

   return 1;
}


// ------------------------------------------------------------------
// Draw a thick line cross lanes (portal signal)
// ------------------------------------------------------------------

int
DRN_Signal::drawLine(DRN_DrawingArea * area, Pixel clr)
{
   XmwPoint points[2];
   double size;

   if (isLinkWide()) {
	  size = 0.5 * ((DRN_Segment *)segment())->width(position());
   } else {
	  size = 0.5 * ((DRN_Lane *)lane())->width(position());
   }

   WcsPoint p0(center_.bearing(size, angle_ + HALF_PI));
   if (!area->world2Window(p0, points[0])) return 0;

   WcsPoint p1(center_.bearing(size, angle_ - HALF_PI));
   if (!area->world2Window(p1, points[1])) return 0;

   area->foreground(clr);

   area->XmwDrawingArea::drawLine(points[0], points[1]);

   return 1;
}



// -------------------------------------------------------------------
// Draw a traffic signal. A signal may consists of several pies, each
// indicating the signal state corresponding to the movement to a
// particular turning movement. It returns 1 if the signal is visible
// or 0 otherwise.
// -----------------------------------------------------------------

int
DRN_Signal::drawTS(DRN_DrawingArea * area)
{
   DRN_Segment * ps = (DRN_Segment *)segment();
   short int num = link()->nDnLinks();
   short int i;
   unsigned char first = link()->rightDnIndex();
   double r = position();
   double alpha = ps->tangentAngle(r);

   int vis = 0;
   if (first != 0xFF) {		// dn link exists
      for (i = 0; i < num; i ++) {
		 vis |= drawSingleTS(area, alpha, (i + first) % num, i);
      }
   } else {			// no dn links (eg 77 Mass Ave)
      vis |= drawSingleTS(area, alpha, 0);
   }

   return vis;
}


// Draw a signal corresponding to a turning movement

int
DRN_Signal::drawSingleTS(
						 DRN_DrawingArea * area,
						 double alpha, short int olink, short int bits)
{
   const double U_ANGLE = ONE_PI * 0.125;
   RN_Link * dnlink = link()->dnLink(olink);
   unsigned int s = stateForExit(shownState_, bits);

   Pixel * clr = calcColor(s); // find a color to represent state

   if (dnlink) {

      double gamma = (alpha >= ONE_PI) ?
		 (alpha - ONE_PI) :
		 (alpha + ONE_PI);

      double beta = dnlink->outboundAngle() - gamma;

      if (fabs(beta) < U_ANGLE) return 0; // skip the U turn signal 

      alpha = dnlink->startSegment()->startTangentAngle();

      return drawPie(area, clr, alpha);

   } else {
      return drawPie(area, clr);
   }
}

