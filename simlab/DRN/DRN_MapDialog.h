//-*-c++-*------------------------------------------------------------
// XmwMapDialog.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef DRN_MAPDIALOG_HEADER
#define DRN_MAPDIALOG_HEADER

#include <Xmw/XmwDialogManager.h>

class DRN_MapDialog : public XmwDialogManager
{
   public:

	  DRN_MapDialog(Widget parent);
	
	  ~DRN_MapDialog() { }

	  void post();				// virtual

   protected:

	  // overload the virtual functions

	  void okay(Widget w, XtPointer tag, XtPointer data);
	  void cancel(Widget w, XtPointer tag, XtPointer data);
	  void help(Widget w, XtPointer tag, XtPointer data);

   private:

	  Widget gdsFiles_;			// XmwXbaeList
	  Widget gridSize_;			// XmtInputField
	  Widget isGridOn_;			// XmToggleButton
	  Widget isMapFeaturesOn_;	// XmToggleButton
	  Widget isLegendOn_;		// XmToggleButton

   private:
  
	  void buildGdsFileLists();
};

#endif
