//-*-c++-*------------------------------------------------------------
// NAME: Drawable Road Network
// AUTH: Qi Yang
// FILE: DRN_ODCell.h
// DATE: Sun Feb 11 14:36:03 1996
//--------------------------------------------------------------------

#ifndef DRN_ODCELL_HEADER
#define DRN_ODCELL_HEADER

#include <Xmw/XmwColor.h>
#include <GRN/OD_Cell.h>

class DRN_DrawingArea;

class DRN_ODCell : public OD_Cell
{
   public:

      DRN_ODCell() : OD_Cell() { }
      virtual ~DRN_ODCell() { }

      virtual Pixel calcColor(DRN_DrawingArea *, int *width = NULL);

      virtual int draw(DRN_DrawingArea *);
};

#endif
