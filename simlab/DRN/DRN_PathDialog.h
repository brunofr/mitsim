//-*-c++-*------------------------------------------------------------
// DRN_PathDialog.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//--------------------------------------------------------------------

#ifndef DRN_PATHDIALOG_HEADER
#define DRN_PATHDIALOG_HEADER

#include <Xmw/XmwDialogManager.h>
#include <Xmw/XmwCallback.h>

class RN_Route;
class XmwSciPlot;

class DRN_PathDialog : public XmwDialogManager
{
	  CallbackDeclare(DRN_PathDialog);

   public:

      DRN_PathDialog (Widget parent);
      ~DRN_PathDialog() { }

	  void post();

   protected:

	  // New callback functions
	  
	  void firstCB(Widget w, XtPointer tag, XtPointer data);
	  void prevCB (Widget w, XtPointer tag, XtPointer data);
	  void nextCB (Widget w, XtPointer tag, XtPointer data);
	  void lastCB (Widget w, XtPointer tag, XtPointer data);
	  void plotPathTimeCB(Widget w, XtPointer tag, XtPointer data);

	  // Input callback

	  void odcodeCB(Widget w, XtPointer tag, XtPointer data);
	  void pathcodeCB(Widget w, XtPointer tag, XtPointer data);

   private:

	  void plotPathTimes(int ori, int des, int pathindex);
	  void plotPathTime(XmwSciPlot *plot,
						const char *label, RN_Route *r,
						int o, int d, int pindex);
   private:

	  void activate();
	  void deactivate();
	  void setPath();
	  void setPair();
	  void updateSensitivities();
	  void draw();

	  // Inherited member

	  void okay(Widget, XtPointer, XtPointer);
	  void cancel(Widget, XtPointer, XtPointer);
	  void help(Widget, XtPointer, XtPointer);

   private:

	  int index_;				// path index

	  Widget odcode_;			// XmtInputField
      Widget plotPathTime_;		// XmPushButton
	  Widget first_;			// XmPushButton
	  Widget prev_;				// XmPushButton
	  Widget pathcode_;			// XmtInputField
	  Widget next_;				// XmPushButton
	  Widget last_;				// XmPushButton
};

#endif
