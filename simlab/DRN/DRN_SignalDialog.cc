//-*-c++-*------------------------------------------------------------
// DRN_SignalDialog.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <assert.h>

#include <Xmt/Xmt.h>
#include <Xmt/Chooser.h>
#include <Xm/ToggleB.h>

#include "DRN_SignalDialog.h"
#include "DRN_Interface.h"
#include "DRN_Symbols.h"
#include "DRN_DrawingArea.h"
#include "DRN_Menu.h"

DRN_SignalDialog::DRN_SignalDialog ( Widget parent )
   : XmwDialogManager(parent, "signalDialog", NULL, 0)
{
   typesFld_ = theDrnSymbols()->associate(widget_, "signalTypes");
   assert(typesFld_);
   addCallback(typesFld_, XmtNvalueChangedCallback,
			   &DRN_SignalDialog::clickTypesCB, this);

   isLabelOnFld_ = theDrnSymbols()->associate(widget_, "isSignalLabelOn");
   assert(isLabelOnFld_);
   addCallback(isLabelOnFld_, XmtNvalueChangedCallback,
			   &DRN_SignalDialog::clickLabelCB, this);
}

// These overload the functions in base class

void DRN_SignalDialog::cancel(Widget, XtPointer, XtPointer)
{
   // Restore the original code before close the box
   
   theDrnSymbols()->signalTypes().set(types_);
   theDrnSymbols()->isSignalLabelOn().set(isLabelOn_);

   theDrawingArea->setState(DRN_DrawingArea::REDRAW);
   unmanage();
}


void DRN_SignalDialog::okay(Widget, XtPointer, XtPointer)
{
   theDrawingArea->setState(DRN_DrawingArea::REDRAW);
   theMenu->needSave();
   unmanage();
}


void DRN_SignalDialog::help(Widget, XtPointer, XtPointer)
{
   theDrnInterface->openUrl("signalColorCode", "drn.html");
}


void DRN_SignalDialog::post()
{
   // Record the value when the box is posted.

   types_ = theDrnSymbols()->signalTypes().value();
   isLabelOn_ = theDrnSymbols()->isSignalLabelOn().value();

   // BUG COMMENTS BEGIN: The state of choosers do not replect the
   // latest value of the linked symbols when after the cancel button
   // is pressed and the dialog box is posed again.  This should be a
   // bug somewhere, but I could not figure out where.  Following is a
   // work around which force to update the chooser state based on
   // latest symbol value.  This is not necessary if the Xmt symbols
   // work as supported.

   XmtChooserSetState(typesFld_, types_, False);
   XmToggleButtonSetState(isLabelOnFld_, isLabelOn_, False);

   // BUG COMMENTS END

   if (theDrnSymbols()->signalTypes().value()) {
	  XmwDialogManager::activate(isLabelOnFld_);
   } else {
	  XmwDialogManager::deactivate(isLabelOnFld_);
   }

   XmwDialogManager::post();
}

void DRN_SignalDialog::clickTypesCB(Widget, XtPointer, XtPointer)
{
   if (theDrnSymbols()->signalTypes().value()) {
	  XmwDialogManager::activate(isLabelOnFld_);
   } else {
	  XmwDialogManager::deactivate(isLabelOnFld_);
   }
   theDrawingArea->redraw();
}

void DRN_SignalDialog::clickLabelCB(Widget, XtPointer, XtPointer)
{
   // This is not necessary but it seems the value associated to
   // XmToggleButton is not updated when the button is clicked.

   Boolean on = XmToggleButtonGetState(isLabelOnFld_);
   theDrnSymbols()->isSignalLabelOn().set(on);

   theDrawingArea->redraw();
}
