//-*-c++-*------------------------------------------------------------
// DRN_SensorDialog.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <assert.h>

#include <Xmt/Xmt.h>
#include <Xmt/Chooser.h>

#include "DRN_SensorDialog.h"
#include "DRN_Interface.h"
#include "DRN_Symbols.h"
#include "DRN_DrawingArea.h"
#include "DRN_Menu.h"

DRN_SensorDialog::DRN_SensorDialog ( Widget parent )
   : XmwDialogManager(parent, "sensorDialog", NULL, 0)
{
   typesFld_ = theDrnSymbols()->associate(widget_, "sensorTypes");
   assert(typesFld_);
   addCallback(typesFld_, XmtNvalueChangedCallback,
			   &DRN_SensorDialog::clickTypesCB, this);

   labelFld_ = theDrnSymbols()->associate(widget_, "sensorLabel");
   assert(labelFld_);
   addCallback(labelFld_, XmtNvalueChangedCallback,
			   &DRN_SensorDialog::clickCB, this);

   colorCodeFld_ = theDrnSymbols()->associate(widget_, "sensorColorCode");
   assert(colorCodeFld_);
   addCallback(colorCodeFld_, XmtNvalueChangedCallback,
			   &DRN_SensorDialog::clickCB, this);
}

// These overload the functions in base class

void DRN_SensorDialog::cancel(Widget, XtPointer, XtPointer)
{
   // Restore the original code before close the box
   
   theDrnSymbols()->sensorTypes().set(types_);
   theDrnSymbols()->sensorLabel().set(label_);
   theDrnSymbols()->sensorColorCode().set(colorCode_);

   theDrawingArea->setState(DRN_DrawingArea::REDRAW);
   unmanage();
}


void DRN_SensorDialog::okay(Widget, XtPointer, XtPointer)
{
   theDrawingArea->setState(DRN_DrawingArea::REDRAW);
   unmanage();
   theMenu->needSave();
}


void DRN_SensorDialog::help(Widget, XtPointer, XtPointer)
{
   theDrnInterface->openUrl("sensorColorCode", "drn.html");
}


void DRN_SensorDialog::post()
{
   // Record the value when the box is posted.

   types_ = theDrnSymbols()->sensorTypes().value();
   label_ = theDrnSymbols()->sensorLabel().value();
   colorCode_ = theDrnSymbols()->sensorColorCode().value();

   // BUG COMMENTS BEGIN: The state of choosers do not replect the
   // latest value of the linked symbols when after the cancel button
   // is pressed and the dialog box is posed again.  This should be a
   // bug somewhere, but I could not figure out where.  Following is a
   // work around which force to update the chooser state based on
   // latest symbol value.  This is not necessary if the Xmt symbols
   // work as supported.

   XmtChooserSetState(typesFld_, types_, False);
   XmtChooserSetState(labelFld_, label_, False);
   XmtChooserSetState(colorCodeFld_, colorCode_, False);

   // BUG COMMENTS END

   if (types_) {
	  activate(labelFld_);
   } else {
	  deactivate(labelFld_);
   }
   if (types_ & DRN_Symbols::SENSOR_AGGREGATE) {
	  activate(colorCodeFld_);
   } else {
	  deactivate(colorCodeFld_);
   }
   XmwDialogManager::post();
}


void DRN_SensorDialog::clickTypesCB(Widget, XtPointer, XtPointer)
{
   int types = theDrnSymbols()->sensorTypes().value();
   if (types) {
	  activate(labelFld_);
   } else {
	  deactivate(labelFld_);
   }
   if (types & DRN_Symbols::SENSOR_AGGREGATE) {
	  activate(colorCodeFld_);
   } else {
	  deactivate(colorCodeFld_);
   }
   theDrawingArea->redraw();
}

void DRN_SensorDialog::clickCB(Widget, XtPointer, XtPointer)
{
   theDrawingArea->redraw();
}
