//-*-c++-*------------------------------------------------------------
// DRN_SegmentAttributeDialog.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//--------------------------------------------------------------------

#include <cstring>
#include <cstdio>
#include <cassert>
#include <iostream>

#include <Xmt/Xmt.h>
#include <Xmt/Dialogs.h>
#include <Xmt/Create.h>
#include <Xbae/Matrix.h>

#include <Tools/ToolKit.h>

#include <Xmw/XmwColor.h>

#include "DRN_Interface.h"
#include "DRN_SegmentAttributeDialog.h"
#include "DRN_DrawingArea.h"
#include "DRN_Segment.h"
#include "DRN_Lane.h"
#include "DRN_Menu.h"

#include <GRN/RoadNetwork.h>
#include <GRN/Parameter.h>
#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>
using namespace std;

DRN_SegmentAttributeDialog::DRN_SegmentAttributeDialog ( Widget parent )
   : XmwWidget("saDialog")
{
   if (theBaseInterface) {
	  theBaseInterface->showBusyCursor();
   }

   widget_ = XmtBuildQueryDialog(parent, name_, NULL, 0,
								 "table", &table_,
								 NULL);
   assert (widget_);
   assert (table_);

   addCallback(table_, XmNenterCellCallback,
			   &DRN_SegmentAttributeDialog::enter, NULL);

   addCallback(table_, XmNleaveCellCallback,
			   &DRN_SegmentAttributeDialog::leave, NULL);

   int i, j, nrows, ncols = 3;

   XtVaGetValues(table_, XmNrows, &nrows, NULL);
   XbaeMatrixDeleteRows(table_, 0, nrows);

   nrows = theNetwork->nSegments();
   RN_Segment *ps;

   String *row_labels = new String [nrows];
   String *attrs = new String [nrows * ncols];

   for (i = 0; i < nrows; i ++) {
	  ps = theNetwork->segment(i);
	  row_labels[i]  = ::Copy(ps->code());
	  attrs[i*3 + 0] = ::Copy(ps->grade(), 1);
	  attrs[i*3 + 1] = ::Copy(ps->freeSpeed()/theBaseParameter->speedFactor(), 0);
	  attrs[i*3 + 2] = ::Copy(ps->sdIndex());
   }

   XbaeMatrixAddRows(table_, 0, attrs, row_labels, NULL, nrows);

   XmwWidget::manage(widget_);

   for (i = 0; i < nrows; i ++) {
	  delete [] row_labels[i];
	  for (j = 0; j < ncols; j ++) {
		 free(attrs[i*ncols + j]);
	  }
   }
   delete [] attrs;
   delete [] row_labels;

   if (!theBaseInterface) return;
   theBaseInterface->showDefaultCursor();
}


void DRN_SegmentAttributeDialog::enter( Widget w, XtPointer, XtPointer data )
{
   XbaeMatrixEnterCellCallbackStruct *p;
   p = (XbaeMatrixEnterCellCallbackStruct *) data;
   DRN_Segment *dps = (DRN_Segment *) theNetwork->segment(p->row);

   WcsPoint point = dps->centerPoint();
   if (!theDrawingArea->isInScope(point)) {
	  theDrawingArea->reCenter(point);
   } else {
	  theDrawingArea->redraw();
   }

   float wd = LANE_WIDTH * theDrawingArea->meter2Pixels();
   if (wd < 4.0) wd = 4.0;
   else if (wd > 10.0) wd = 10.0;
   theDrawingArea->lineWidth((int) wd);
   dps->highlight(theDrawingArea, theColorTable->red());
   theDrawingArea->lineWidth(0);

   // select the cell

   p->select_text = True;
}

void DRN_SegmentAttributeDialog::leave( Widget w, XtPointer, XtPointer data )
{
   XbaeMatrixLeaveCellCallbackStruct *p;
   p = (XbaeMatrixLeaveCellCallbackStruct *) data;

   if (!Cmp(p->value, XbaeMatrixGetCell(w, p->row, p->column))) {
	  return;
   }
   
   theMenu->needSave(XmwMenu::STATE_SAVE_NETWORK);
   
   RN_Segment *ps = theNetwork->segment(p->row);
   switch (p->column) {
	  case 0:					// grade
		 ps->grade(atof(p->value));
		 break;
	  case 1:					// speed
		{
		  float spd = atof(p->value) * theBaseParameter->speedFactor();
		  if (spd > 0) {
			ps->freeSpeed(spd);
		  } else {
			p->doit = False;
			theDrnInterface->msgShow(XmtLocalize2(w, "Error",
												  "input", "freespd"));
		  }
		  break;
		}
	  case 2:					// sd index
		{
		  int i = atoi(p->value);
		  if (i >= 0 && i < theNetwork->nSdFns()) {
			ps->sdIndex(i);
		  } else {
			theDrnInterface->msgShow(XmtLocalize2(w, "Error",
												  "input", "sdindex"));
			p->doit = False;
		  }
		  break;
		}
	  default:					// imposible
		 break;
   }
}

void DRN_SegmentAttributeDialog::post()
{
   XmwWidget::manage(widget_);
}
