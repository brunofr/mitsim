//-*-c++-*------------------------------------------------------------
// DRN_Interface.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class definition for the class DRN_Interface
//--------------------------------------------------------------------

#ifndef DRN_INTERFACE_HEADER
#define DRN_INTERFACE_HEADER

#include <Xmw/XmwInterface.h>
#include <Xmw/XmwCallback.h>

class DRN_Menu;
class DRN_DrawingArea;
class DRN_Modeline;
class DRN_ToolBox;
class DRN_Logo;
class DRN_Symbols;

class DRN_Interface : public XmwInterface
{
  friend class DRN_Menu;
  friend class DRN_Symbols;

public:

  DRN_Interface(int *argc, char **argv, XmwImage *icon);
  virtual ~DRN_Interface();

  virtual void create();
  virtual void manage();

  // overload the virtual function in base class

  void updateButtons();
  void pause (int others);
  void run (int others);

  virtual XmwSymbols *createSymbols(); // virtual
  DRN_Symbols* symbols() { return (DRN_Symbols*) symbols_; }

  DRN_Menu *menu() { return (DRN_Menu *) menu_; }
  DRN_Modeline *modeline() { return (DRN_Modeline *) modeline_; }

  DRN_DrawingArea *drawingArea() { return drawingArea_; }
  DRN_ToolBox *toolBox() { return toolBox_; }

  void drawLogo(DRN_DrawingArea *);
  void changeDesiredTimeFactor(double changes);
  void tellLoadingIsDone();
  void NoNetwork() ;

  int mainloop();			// virtual
  void eventLoop();

protected:

  virtual void view(Boolean);

protected:

  DRN_Logo *logo_;
  Widget workingArea_;
  DRN_DrawingArea *drawingArea_;
  DRN_ToolBox *toolBox_;

  virtual XmwMenu* menu(Widget);	// virtual
  virtual XmwModeline* modeline(Widget);	// virtual
  virtual DRN_DrawingArea* drawingArea(Widget);
  virtual DRN_ToolBox* toolBox(Widget);
};

extern DRN_Interface *theDrnInterface;

inline DRN_Symbols* theDrnSymbols() {
  return theDrnInterface->symbols();
}

#endif
