//-*-c++-*------------------------------------------------------------
// DRN_Symbols.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// Manage all the Xmt symbols appliable to DRN classes
//--------------------------------------------------------------------

#ifndef DRN_SYMBOLS_HEADER
#define DRN_SYMBOLS_HEADER

#include <Xmw/XmwSymbols.h>

class DRN_Symbols : public XmwSymbols
{
   public:

	  enum {
		 TOOL_PAN		= 0,
		 TOOL_ZOOMIN	= 1,
		 TOOL_ZOOMOUT	= 2,
		 TOOL_CENTER	= 3,
		 TOOL_ROTATE	= 4,
		 TOOL_QUERY		= 5,
		 TOOL_LEGEND	= 6,
		 TOOL_DRAW		= 7,
		 TOOL_TEXT		= 8,
		 TOOL_TRACE		= 9
	  };

	  enum {
		 VIEW_MICRO         = 0,
		 VIEW_MACRO         = 1,
		 VIEW_ODFLOW        = 2
	  };

	  enum {
		 SEGMENT_LINKTYPE  = 0,
		 SEGMENT_DENSITY   = 1,
		 SEGMENT_SPEED     = 2,
		 SEGMENT_FLOW      = 3
	  };

	  enum {
		 SENSOR_AGGREGATE = 0x1,
		 SENSOR_INDIVIDUAL= 0x2,
		 SENSOR_SNAPSHOT  = 0x4
	  };

	  enum {
		 SENSOR_LABEL_NONE  = 0,
		 SENSOR_LABEL_CODE  = 1,
		 SENSOR_LABEL_VALUE = 2
	  };

	  enum {
		 SENSOR_COUNT       = 0,
		 SENSOR_FLOW        = 1,
		 SENSOR_SPEED       = 2,
		 SENSOR_OCCUPANCY   = 3
	  };

	  enum {
		 SIGNAL_TS          = 0x01,
		 SIGNAL_PS          = 0x02,
		 SIGNAL_VSLS        = 0x04,
		 SIGNAL_VMS         = 0x08,
		 SIGNAL_LUS         = 0x10, // 16
		 SIGNAL_RAMPMETER   = 0x20 // 32
	  };

   public:

	  DRN_Symbols() : XmwSymbols() { }
	  virtual ~DRN_Symbols() { }
	  virtual void registerAll();

	  // Access functions
	  
	  XmwSymbol<int>& tools() { return tools_; }
	  XmwSymbol<int>& viewType() { return viewType_; }
	  XmwSymbol<int>& segmentColorCode() { return  segmentColorCode_; }

	  XmwSymbol<int>& sensorTypes() { return sensorTypes_; }
	  XmwSymbol<int>& sensorLabel() { return sensorLabel_; }
	  XmwSymbol<int>& sensorColorCode() { return sensorColorCode_; }

	  XmwSymbol<int>& signalTypes() { return signalTypes_; }
	  XmwSymbol<Boolean>& isSignalLabelOn() { return isSignalLabelOn_; }

	  XmwSymbol<Boolean>& isMapFeaturesOn() { return isMapFeaturesOn_; }
	  XmwSymbol<Boolean>& isLegendOn() { return isLegendOn_; }
	  XmwSymbol<Boolean>& isGridOn() { return isGridOn_; }
	  XmwSymbol<Boolean>& isVersionLabelOn() { return isVersionLabelOn_; }

   protected:

	  // New symbols

	  XmwSymbol<int> tools_;
	  XmwSymbol<int> viewType_;

	  XmwSymbol<int> segmentColorCode_;

	  XmwSymbol<int> sensorTypes_;
	  XmwSymbol<int> sensorLabel_;
	  XmwSymbol<int> sensorColorCode_;

	  XmwSymbol<int> signalTypes_;
	  XmwSymbol<Boolean> isSignalLabelOn_;

	  XmwSymbol<Boolean> isMapFeaturesOn_;
	  XmwSymbol<Boolean> isLegendOn_;
	  XmwSymbol<Boolean> isGridOn_;
	  XmwSymbol<Boolean> isVersionLabelOn_;
};

#endif // DRN_SYMBOLS_HEADER
