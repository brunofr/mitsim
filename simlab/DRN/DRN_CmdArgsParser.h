//-*-c++-*------------------------------------------------------------
// NAME: Command line arguments parser for DRN
// AUTH: Qi Yang
// FILE: DRN_CmdArgsParser.h
// DATE: Tue Nov 21 17:58:27 1995
//--------------------------------------------------------------------

#ifndef DRNCMDARGSPARSER_HEADER
#define DRNCMDARGSPARSER_HEADER

#include <Tools/CmdArgsParser.h>

// This class register some common used command line arguments for DRN
// based application.  You may inherit class from DRN_CmdArgsParser
// and register more arguments you needs.  You cannot remove the
// arguments already registered in the constructor of
// DRN_CmdArgsParser. However, you can use this class as an example
// and create your parser from CmdArgsParser.

class DRN_CmdArgsParser : public CmdArgsParser
{
   public:
      
      DRN_CmdArgsParser();
      virtual ~DRN_CmdArgsParser() { }

      virtual void init();
};

#endif
