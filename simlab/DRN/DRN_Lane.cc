//-*-c++-*------------------------------------------------------------
// DRN_Lane.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------


#include <GRN/Constants.h>
#include <GRN/RN_SurvStation.h>
#include <GRN/RN_CtrlStation.h>

#include "DRN_Lane.h"
#include "DRN_Segment.h"
#include "DRN_DrawingArea.h"

const double LANE_MARK_SPACE = 10.0; // in meters

DRN_Lane::DRN_Lane() 
  : RN_DrawableLane()
{
}


// Find the color for drawing the lane mark (or curb for the right
// most lane)

Pixel DRN_Lane::calcColor(DRN_DrawingArea * area)
{
  Pixel clr;

  // Color is determined based on lane change regulations

  int mode = 0;
  if (rules_ & LANE_CHANGE_RIGHT) mode |= 1;
  if (right()->rules() & LANE_CHANGE_LEFT) mode |= 2;

  switch (mode) {
  case 0:			// no change
	{
	  clr = theColorTable->lightRed();
	  break;
	}
  case 1:			// change to right allowed
	{
	  clr = theColorTable->yellow();
	  break;
	}
  case 2:			// change to left allowed
	{
	  clr = theColorTable->cyan();
	  break;
	}
  case 3:			// both change allowed
	{
	  clr = theColorTable->lightGreen();
	  break;
	}
  }
  return clr;
}

 
int DRN_Lane::draw(DRN_DrawingArea * area)
{
  int visible = 0;

  if (right()) {				// draw lane mark
	if (!area->isLaneMarkDrawable()) {
	  return visible;			// skip lane marks
	} else {
	  Pixel c = calcColor(area);
	  area->foreground(c);
   	  visible = drawMark(area);
	}
  } else {						// draw segment curb
	visible = drawCurb(area);
  }

  if (!area->isLaneTypeDrawable())
	return visible;				// skip type marks

  if (isEtcLane()) {			// ETC lane
	area->foreground(theColorTable->color(0.25, 0.25));
	visible |= drawSpecialMarks(area);
  } else if (isHovLane()) {
	area->foreground(theColorTable->color(0.75, 0.25));
	visible |= drawSpecialMarks(area);
  } else if (isBusLane()) {
	area->foreground(theColorTable->yellow());
	visible |= drawSpecialMarks(area);
  }

  return visible;
}


int DRN_Lane::drawCurb(DRN_DrawingArea * area)
{
  int visible = 0;
  WcsPoint prev = rightPoint(1.0);
  WcsPoint p;
  int n, i;
  double step;
  DRN_Segment *ps = (DRN_Segment *) segment();
  if (isCurve()) {

	double shell = ARC_ACCURACY / area->meter2Pixels();
	int m = (int) ceil(length() / shell);
	step = acos(1.0 - shell / radius());
	n = (int) ceil(Abs(arcAngle() / step));
	if (n < 1) {
	  n = 1;
	} else if (n > m) {
	  n = m;
	}
	step = 1.0 / n;
  } else {
	n = 5;
	step = 1.0 / n;
  }
  for (i = n-1; i >= 0; i --) {
	p = rightPoint(i * step);
	Pixel c = ps->calcColor(area, (i + 0.5) * step);
	area->foreground(c);
	visible |= area->drawLine(prev, p);
	prev = p;
  }
  return visible;
}


int DRN_Lane::drawMark(DRN_DrawingArea * area)
{
  int n = (int) (length() / LANE_MARK_SPACE);
  n = Max(1, n);
  double step = 1.0 / n;

  int visible = 0;
  WcsPoint p;

  for (int i = n; i >= 0; i --) {
	p = rightPoint(i * step);
	visible |= area->drawPoint(p);
  }
  
  return visible;
}


int DRN_Lane::drawSpecialMarks(DRN_DrawingArea * area)
{
  int n = (int) (length() / LANE_MARK_SPACE);
  n = Max(1, n);
  double step = 1.0 / n;

  WcsPoint p;
  int visible = 0;
  double pos;
  for (int i = n; i >= 0; i --) {
	pos = i * step;
	p = centerPoint(pos);
	visible |= area->drawDiamond(p, tangentAngle(pos));
  }
  
  return visible;
}

int DRN_Lane::highlight(DRN_DrawingArea * area, Pixel clr)
{
  int vis = 0;

  area->foreground(clr);

  // draw a line at the center

  if (segment()->isArc()) { // curve

	WcsPoint prev = centerPoint(1.0);
	WcsPoint p;

	double shell = ARC_ACCURACY / area->meter2Pixels();
	double step = acos(1.0 - shell / radius());
    
	int n = (int) ceil(Abs(arcAngle() / step));
	int m = (int) ceil(length() / shell);
	if (n < 1) {
	  n = 1;
	} else if (n > m) {
	  n = m;
	}
	step = 1.0 / n;
	for (int i = n; i >= 0; i --) {
	  p = centerPoint(i * step);
	  vis |= area->drawLine(prev, p);
	  prev = p;
	}
  } else {			// straint line
	vis = area->drawLine(centerPoint(1.0), centerPoint(0.0));
  }

  return vis;
}
