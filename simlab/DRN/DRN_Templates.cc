//-*-c++-*------------------------------------------------------------
// NAME: Explicitly instantiate all the template instances we use
// AUTH: Qi Yang
// FILE: fiTemplates.C
// DATE: Thu Apr 18 23:17:31 1996
//--------------------------------------------------------------------

#ifdef FORCE_INSTANTIATE

#include <GDS/GDS_Glyph.h>
#include <Xmw/XmwSymbols.h>
#include <Templates/Pointer.h>
#include <vector>

#ifdef __sgi

#pragma instantiate Pointer <GDS_Glyph>
#pragma instantiate std::vector <Pointer <GDS_Glyph> >

#pragma instantiate XmwSymbol <char>
#pragma instantiate XmwSymbol <int>

#endif

#ifdef __GNUC__

template class Pointer <GDS_Glyph>;
template class std::vector <Pointer <GDS_Glyph> >;

template class XmwSymbol <char>;
template class XmwSymbol <int>;

#endif  // __GNUC__

#endif
