//-*-c++-*------------------------------------------------------------
// DRN_KeyInteractor.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <iostream>
#include <cctype>
#include <cstring>
using namespace std;

#include <Xmt/Xmt.h>
#include <X11/Xlib.h>
#include <Xmw/XmwFont.h>
#include <Xmw/XmwColor.h>

#include "DrawableRoadNetwork.h"
#include "DRN_KeyInteractor.h"
#include "DRN_MouseInteractor.h"
#include "DRN_DrawingArea.h"
#include "DRN_Menu.h"
#include "DRN_Interface.h"
#include "DRN_Help.h"
#include "DRN_PathDialog.h"
#include "DRN_FindDialog.h"
#include "DRN_Symbols.h"
#include "DRN_MapFeature.h"
#include "DRN_Node.h"
#include "DRN_ViewMarker.h"

#include <Tools/SimulationClock.h>
#include <Tools/SimulationEngine.h>
#include <Tools/ToolKit.h>

// This function is called whenever a key is pressed in the
// area

Boolean DRN_KeyInteractor::keyPress(KeySym keysym)
{
  if (XmwKeyInteractor::keyPress(keysym)) {
	return True;
  }

  if (theDrnSymbols()->tools().value() == DRN_Symbols::TOOL_TEXT &&
	  textInput(keysym)) {
	return True;
  }

  switch (keysym)
	{
	case XK_q:			// close and quit
	case XK_Q:
	  {
		theSimulationEngine->quit(STATE_QUIT);
		break;
	  }
	case XK_h:				// show help info
	case XK_H:
	case XK_question:
	  {
		drawingArea()->mouse_interactor()->unsetModifier();
		::HelpKeys();
		break;
	  }
	case XK_m:				// turn on/off map features
	case XK_M:
	  { 
		Boolean on = theDrnSymbols()->isMapFeaturesOn().value();
		theDrnSymbols()->isMapFeaturesOn().set(!on);
		drawingArea()->setState(DRN_DrawingArea::REDRAW);
		break;
	  }
	case XK_F4:
	  {
		Boolean on = theDrnSymbols()->isLegendOn().value();
		theDrnSymbols()->isLegendOn().set(!on);
		drawingArea()->setState(DRN_DrawingArea::REDRAW);
		break;
	  }
	case XK_w:
	case XK_W:
	  {
		theMenu->findObject(NULL, NULL, NULL);
		break;
	  }
	case XK_a:		// returns to home position
	case XK_A:
	case XK_KP_5:
	case XK_Home:
	case XK_KP_Begin:
	  {
		drawingArea()->setHomePosition();
		drawingArea()->redraw();
		break;
	  }
	case XK_KP_Enter:		// toggle between pause/run
	case XK_Return:
	case XK_Pause:
	  {
		if (theSimulationClock->isPaused() ||
			!theSimulationClock->isStarted()) {
		  theDrnInterface->run(True);
		} else {
		  theDrnInterface->pause(True);
		}
		break;
	  }
	
	case XK_l:		// panning
	case XK_L:
	case XK_Left:
	case XK_KP_4:
	case XK_KP_Left:

	case XK_r:
	case XK_R:
	case XK_Right:
	case XK_KP_6:
	case XK_KP_Right:

	case XK_u:
	case XK_U:
	case XK_Up:
	case XK_KP_8:
	case XK_KP_Up:

	case XK_d:
	case XK_D:
	case XK_Down:
	case XK_KP_2:
	case XK_KP_Down:

	case XK_KP_1:
	case XK_KP_End:

	case XK_KP_3:
	case XK_KP_Page_Down:

	case XK_KP_7:
	case XK_KP_Home:     

	case XK_KP_9:
	case XK_KP_Page_Up:
	  {
		drawingArea()->pan(keysym);
		break;
	  }
	case XK_i:		// zooming
	case XK_I:
	case XK_o:
	case XK_O:
	  {
		drawingArea()->zoom(keysym);
		break;
	  }

	case XK_j:
	case XK_J:
	case XK_backslash:

	case XK_k:
	case XK_K:
	case XK_KP_Divide:
	case XK_slash:
	  {
			
		drawingArea()->rotate(keysym);
		break;
	  }

	  // Center to the mouse pointer

	case XK_c:
	case XK_C:
	  {
		XmwPoint p;
		if (theNetwork && drawingArea()->queryCursorPoint(p)) {
		  drawingArea()->reCenter(p);
		}
		break;
	  }

	case XK_x:
	case XK_X:
	  {
		XmwPoint p;
		if (theNetwork && drawingArea()->queryCursorPoint(p)) {
		  theDrawableNetwork->setIncident(p);
		}
		break;
	  }

	case XK_e:
	case XK_E:
	  {
		XmwPoint p;
		if (theNetwork && drawingArea()->queryCursorPoint(p)) {
		  theDrawableNetwork->editSignalStates(p);
		}
		break;
	  }

	case XK_z:
	case XK_Z:
	  {
		XmwPoint p;
		if (theNetwork && drawingArea()->queryCursorPoint(p)) {
		  DRN_Node::query(p);
		}
		break;
	  }

	case XK_f:		// change running speed
	case XK_F:
	case XK_plus:
	case XK_KP_Add:
	  {
		double change = scale(0.05, 0.10, 0.25);
		theDrnInterface->changeDesiredTimeFactor(-change);
		break;
	  }
	case XK_s:
	case XK_S:
	case XK_minus:
	case XK_KP_Subtract:
	  {
		double change = scale(0.05, 0.10, 0.25);
		theDrnInterface->changeDesiredTimeFactor(change);
		break;
	  }
	case XK_g:		// turn on/off grid
	case XK_G:
	  {
		Boolean on = theDrnSymbols()->isGridOn().value();
		theDrnSymbols()->isGridOn().set(!on);
		drawingArea()->setState(DRN_DrawingArea::REDRAW);
		break;
	  }

	case XK_asterisk:		// toggle version label
	case XK_KP_Multiply:	// using in developing version only
	case XK_period:
	case XK_KP_Decimal:
	  {
		int on = theDrnSymbols()->isVersionLabelOn().value();
		theDrnSymbols()->isVersionLabelOn().set(!on);
		drawingArea()->setState(DRN_DrawingArea::REDRAW);
		break;
	  }

	case XK_0:
	case XK_1:
	case XK_2:
	case XK_3:
	case XK_4:
	case XK_5:
	case XK_6:
	case XK_7:
	case XK_8:
	case XK_9:
	{
		GotoViewMarker(keysym) ;
		break ;
	}

	default:
	  {
		// 			if (ToolKit::debug()) {
		// 			   cout << "Key <" << XKeysymToString(keysym)
		// 					<< "> ignored." << endl;
		// 			}
		return False;
	  }
	}
  return True;
}

void DRN_KeyInteractor::GotoViewMarker(KeySym keysym)
{
	int base ;
	switch (modifier_) {
	case XK_Control_L:
	case XK_Control_R:
		base = 10 ;
		break ;
	case XK_Alt_L:
	case XK_Alt_R:
		base = 20 ;
		break ;
	default:
		base = 0 ;
		break ;
	}
	DRN_ViewMarker::GotoMarker(base + keysym - XK_0) ;
}


Boolean DRN_KeyInteractor::textInput(KeySym keysym)
{
  static string label;

  switch (keysym) {
  case XK_KP_Enter:
  case XK_Return:
	{
	  XmwPoint p;
	  if (drawingArea()->queryCursorPoint(p)) {
		drawingArea()->setFont(1); // select font 1
		drawingArea()->foreground(theColorTable->yellow());
		drawingArea()->setXorMode();
		drawingArea()->drawCString(label.c_str(), p);
		if (!theMapFeatures) {
		  theMapFeatures = new DRN_MapFeature;
		}
		theMapFeatures->addLabel(p, label.c_str());
	  }
#ifdef __GNUC__
	  label.erase();
#else
# ifdef SUNOS
	  label.erase();
# else  // __sgi
	  label.remove();
# endif
#endif
	  theDrnInterface->msgClear();
	  return True;
	}

  case XK_Delete:
  case XK_KP_Delete:
  case XK_BackSpace:
	{
	  if (label.length() > 0) {
#ifdef __GNUC__
		label.erase(label.length()-1, 1);
#else
# ifdef SUNOS
	  label.erase(label.length()-1, 1);
# else  // __sgi
	  label.remove(label.length()-1, 1);
# endif
#endif
		theDrnInterface->msgSet(label.c_str());
	  }
	  return True;
	}

  default:
	{
	  if (IsCursorKey(keysym) ||
		  IsFunctionKey(keysym) ||
		  IsKeypadKey(keysym) ||
		  IsMiscFunctionKey(keysym) ||
		  IsPFKey(keysym)) {
		return false;
	  } else if (isprint(keysym)) {
		char s[2];
		s[0] = keysym;
		s[1] = '\0';
		theDrnInterface->msgAppend(s);
		label += s[0];
		return True;
	  }
	}
	return False;
  }
}


// Return the scale based on the modifer

double
DRN_KeyInteractor::scale(double a_little, double regular, double a_lot)
{
  switch (modifier_)
	{
	case XK_Control_L:
	case XK_Control_R:
	  {
		return a_little;
	  }
	case XK_Shift_L:
	case XK_Shift_R:
	  {
		return a_lot;
	  }
	}
  return regular;
}
