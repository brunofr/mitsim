//-*-c++-*------------------------------------------------------------
// DRN_Logo.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// A class that draws the initializing logo, which can be redefined in
// the resource file.
//
//--------------------------------------------------------------------

#ifndef DRN_LOGO_HEADER
#define DRN_LOGO_HEADER

#include <Xmt/Xmt.h>
#include <Xmt/Pixmap.h>

class DRN_DrawingArea;

class DRN_Logo
{
   public:

      DRN_Logo(Widget w);
      virtual ~DRN_Logo() { }

 	  void draw(DRN_DrawingArea* area, int dx = 0, int dy = 0);
	  
   private:

	  Pixmap pixmap_;
	  unsigned int width_;
	  unsigned int height_;
	  unsigned int depth_;
};


#endif
