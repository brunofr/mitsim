//-*-c++-*------------------------------------------------------------
// DRN_Segment.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//--------------------------------------------------------------------

#include <stdio.h>

#include <Tools/ToolKit.h>
#include <GRN/RN_Link.h>

#include "DRN_Segment.h"
#include "DRN_Lane.h"
#include "DRN_Interface.h"
#include "DRN_DrawingArea.h"
#include "DRN_Menu.h"
#include "DRN_Symbols.h"
#include "DrawableRoadNetwork.h"

DRN_Segment::DRN_Segment()
  : RN_DrawableSegment(),
	min_x_(FLT_INF), min_y_(FLT_INF),
	max_x_(-FLT_INF), max_y_(-FLT_INF)
{
}

void
DRN_Segment::calcScope()
{
  DRN_Lane *pl = (DRN_Lane *)rightLane();
  min_x_ = Min(RN_Arc::min_x(), pl->min_x());
  max_x_ = Max(RN_Arc::max_x(), pl->max_x());
  min_y_ = Min(RN_Arc::min_y(), pl->min_y());
  max_y_ = Max(RN_Arc::max_y(), pl->max_y());
}


Pixel
DRN_Segment::calcColor(DRN_DrawingArea * area, float r)
{
  Pixel clr;

  if (theDrnSymbols()->viewType().is(DRN_Symbols::VIEW_ODFLOW)) {
	return theColorTable->darkGray();
  }

  switch (theDrnSymbols()->segmentColorCode().value()) {
  case DRN_Symbols::SEGMENT_DENSITY:
	{
	  clr = area->densityColor(smdDensity(r));
	  break;
	}
  case DRN_Symbols::SEGMENT_SPEED:  
	{
	  clr = area->speedColor(smdSpeed(r));
	  break;
	}
  case DRN_Symbols::SEGMENT_FLOW:
	{
	  clr = area->linkFlowColor(smdFlow(r));
	  break;
	}
  case DRN_Symbols::SEGMENT_LINKTYPE:
	{
	  int tunnel = link()->isInTunnel();
	  switch (link()->linkType()) {
	  case LINK_TYPE_FREEWAY:
		{
		  if (tunnel) {
			clr = theColorTable->cyan();
		  } else {
			clr = theColorTable->yellow();
		  }
		  break;
		}
	  case LINK_TYPE_RAMP:
		{
		  if (tunnel) {
			clr = theColorTable->lightGreen();
		  } else {
			clr = theColorTable->red();
		  }
		  break;
		}
	  case LINK_TYPE_URBAN:
		{
		  if (tunnel) {
			clr = theColorTable->green();
		  } else {
			clr = theColorTable->lightRed();
		  }
		  break;
		}
	  default:
		{
		  if (tunnel) {
			clr = theColorTable->blue();
		  } else {
			clr = theColorTable->lightGray();
		  }
		  break;
		}
	  }
	  break;
	}
  default:			// error
	{
	  clr = theColorTable->magenta();
	  break;
	}
  }
  return clr;
}

float DRN_Segment::smdDensity(float r)
{
  float ux, dx;					// values
  float up, dp;					// positions
  DRN_Segment *ps;
  if (r > 0.5) {				// upstream half section
	ps = (DRN_Segment *)upstream();
	dx = density();
	if (!ps) return dx;
	ux = ps->density();
	up = 0.5 * ps->length() + (1.0 - r) * length();
	dp = (r - 0.5) * length();
  } else {						// downstream half section
	ps = (DRN_Segment *)downstream();
	ux = density();
	if (!ps) return ux;
	dx = ps->density();
	up = (0.5 - r) * length();
	dp = r * length() + 0.5 * ps->length();
  }
  r = up / (up + dp);
  return dx + r * (ux - dx);
}

float DRN_Segment::smdSpeed(float r)
{
  float ux, dx;					// values
  float up, dp;					// positions
  DRN_Segment *ps;
  if (r > 0.5) {				// upstream half section
	ps = (DRN_Segment *)upstream();
	dx = speed();
	if (!ps) return dx;
	ux = ps->speed();
	up = 0.5 * ps->length() + (1.0 - r) * length();
	dp = (r - 0.5) * length();
  } else {						// downstream half section
	ps = (DRN_Segment *)downstream();
	ux = speed();
	if (!ps) return ux;
	dx = ps->speed();
	up = (0.5 - r) * length();
	dp = r * length() + 0.5 * ps->length();
  }
  r = up / (up + dp);
  return dx + r * (ux - dx);
}

float DRN_Segment::smdFlow(float r)
{
  float ux, dx;					// values
  float up, dp;					// positions
  DRN_Segment *ps;
  if (r > 0.5) {				// upstream half section
	ps = (DRN_Segment *)upstream();
	dx = flow();
	if (!ps) return dx;
	ux = ps->flow();
	up = 0.5 * ps->length() + (1.0 - r) * length();
	dp = (r - 0.5) * length();
  } else {						// downstream half section
	ps = (DRN_Segment *)downstream();
	ux = flow();
	if (!ps) return ux;
	dx = ps->flow();
	up = (0.5 - r) * length();
	dp = r * length() + 0.5 * ps->length();
  }
  r = up / (up + dp);
  return dx + r * (ux - dx);
}

int
DRN_Segment::isInScope(DRN_DrawingArea *area)
{
  return (min_x() <= area->right() &&
		  max_x() >= area->left() &&
		  min_y() <= area->top() &&
		  max_y() >= area->bottom());
}


int
DRN_Segment::draw(DRN_DrawingArea * area)
{
  int vis = 0;

  if (!isInScope(area)) {
	unsetState(STATE_VISIBLE);
	return vis;
  }

  // Draw left curb

  if (theDrnSymbols()->viewType().is(DRN_Symbols::VIEW_MACRO)) {
	double w = 0.85 * area->meter2Pixels() * nLanes() * LANE_WIDTH;
	int thinkness = (int) w;
	area->lineWidth(thinkness);
  }

  area->setCopyMode();

  WcsPoint prev = leftPoint(1.0);
  WcsPoint p;
  int n, i;
  double step;

  if (isArc()) {				// curve
	double shell = ARC_ACCURACY / area->meter2Pixels();
	int m = (int) ceil(length() / shell);
	step = acos(1.0 - shell / radius());
	n = (int) ceil(Abs(arcAngle() / step));
	if (n < 1) {
	  n = 1;
	} else if (n > m) {
	  n = m;
	}
	step = 1.0 / n;
  } else {						// straint line
	n = 5;
	step = 1.0 / n;
  }

  for (i = n-1; i >= 0; i --) {
	p = leftPoint(i * step);
	Pixel c = calcColor(area, (i + 0.5) * step);
	area->foreground(c);
	vis |= area->drawLine(prev, p);
	prev = p;
  }

  // Draws lane marks and lane type for each lane in this segment.
  // The lane marks of right most lane, which is the same as the
  // right curb, is shown in solid line.

  if (theDrnSymbols()->viewType().is(DRN_Symbols::VIEW_MICRO)) {
	for (i = 0; i < nLanes_; i ++) {
	  vis |= ((DRN_Lane *) lane(i))->draw(area);
	}
  } else if (theDrnSymbols()->viewType().is(DRN_Symbols::VIEW_MACRO)) {
	area->lineWidth( 0 );
  }

  if (vis) setState(STATE_VISIBLE);
  else unsetState(STATE_VISIBLE);

  return vis;
}

int
DRN_Segment::highlight(DRN_DrawingArea * area, Pixel clr)
{
  int vis = 0;

  if (!isInScope(area)) {
	return vis;
  }

  area->foreground(clr);

  // draw a line at the center

  if (isArc()) { // curve

	WcsPoint prev = centerPoint(1.0);
	WcsPoint p;

	double shell = ARC_ACCURACY / area->meter2Pixels();
	double step = acos(1.0 - shell / radius());
    
	int n = (int) ceil(Abs(arcAngle() / step));
	int m = (int) ceil(length() / shell);
	if (n < 1) {
	  n = 1;
	} else if (n > m) {
	  n = m;
	}
	step = 1.0 / n;
	for (int i = n; i >= 0; i --) {
	  p = centerPoint(i * step);
	  vis |= area->drawLine(prev, p);
	  prev = p;
	}
  } else {			// straint line
	vis = area->drawLine(centerPoint(1.0), centerPoint(0.0));
  }

  return vis;
}
