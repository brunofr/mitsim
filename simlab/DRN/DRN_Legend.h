//-*-c++-*------------------------------------------------------------
// NAME: Drawable Road Network
// AUTH: Qi Yang
// FILE: DRN_Legend.h
// DATE: Tue Feb  6 10:13:06 1996
//--------------------------------------------------------------------

#ifndef DRN_LEGEND_HEADER
#define DRN_LEGEND_HEADER

#include <Xmw/XmwPoint.h>
#include <Xmw/XmwColor.h>

class DRN_DrawingArea;
class Normalizer;

class DRN_Legend
{
   protected:

      XmwPoint pos_;			// upper left corner
      int offset_;

   public:

      DRN_Legend(DRN_DrawingArea * p) { create(p); }
      virtual ~DRN_Legend() { }

      virtual void create(DRN_DrawingArea *p);
      
      // Set the legend at a given position

      virtual void setPosition(XmwPoint& p);
      
      // Draw the legend at its current position

      virtual void draw(int mode = 3);
      virtual void drawSegmentSymbols();
      virtual void drawSensorSymbols();
      
      virtual void drawLineSymbols(char *title, Normalizer *,
				   double scaling,
				   int num_labels = 3);

      virtual void drawLineSymbol(Pixel clr, char *label,
				  int offset = 0, int width = 20);

      virtual void drawShortLineSymbol(Pixel clr, char *label,
				       int offset = 0);

      virtual void drawBoxSymbol(Pixel clr, char *label,
				 int w, int h,
				 int offset = 0);

      virtual void drawSquareBoxSymbol(Pixel clr, char *label,
				       int offset = 0);

      virtual void drawLongBoxSymbol(Pixel clr, char *label,
				     int offset = 0);

      // This function is called in draw()

      virtual void draw_more() { }
};

#endif
