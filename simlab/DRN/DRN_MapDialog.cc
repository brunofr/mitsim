//-*-c++-*------------------------------------------------------------
// DRN_MapDialog.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <cassert>
#include <iostream>
#include <cstdlib>
#include <cstdio>

#include <Xmt/Xmt.h>
#include <Xmt/Dialog.h>
#include <Xmt/Chooser.h>
#include <Xmt/Create.h>
#include <Xmt/InputField.h>
#include <Xm/ToggleB.h>

#include <Xmw/XmwXbaeList.h>

#include <IO/Exception.h>
#include <Tools/ToolKit.h>
#include <GRN/Parameter.h>

#include <GDS/GDS_Glyph.h>

#include "DRN_MapDialog.h"
#include "DRN_MapFeature.h"
#include "DRN_Interface.h"
#include "DRN_Symbols.h"
#include "DRN_DrawingArea.h"
#include "DRN_Legend.h"
#include "DRN_Menu.h"
using namespace std;

DRN_MapDialog::DRN_MapDialog(Widget parent)
   : XmwDialogManager(parent, "mapDialog", NULL, 0)
{
   XtVaSetValues(widget_,
				 XmNdialogStyle, XmDIALOG_PRIMARY_APPLICATION_MODAL,
				 NULL);

   static short int widths[] = { 12, 6, 6 };
   static String col_labels[] = { "File\nName", "Min\nScale", "Max\nScale" };
   gdsFiles_ = XmtNameToWidget(widget_, "gdsFiles");
   assert(gdsFiles_);
   XmwXbaeList *list = getXmwXbaeList(gdsFiles_);
   list->setColAttrs(3, widths, col_labels);
   XtVaSetValues(list->xmw(),
				 XmNhorizontalScrollBarDisplayPolicy, XmDISPLAY_NONE,
				 NULL );


   gridSize_ = XmtNameToWidget(widget_, "gridSize");
   assert(gridSize_);

   isGridOn_ = theDrnSymbols()->associate(widget_, "isGridOn");
   assert(isGridOn_);

   isMapFeaturesOn_ = theDrnSymbols()->associate(widget_, "isMapFeaturesOn");
   assert(isMapFeaturesOn_);

   isLegendOn_ = theDrnSymbols()->associate(widget_, "isLegendOn");
   assert(isLegendOn_);
}


// Build list of the gds files

void DRN_MapDialog::buildGdsFileLists()
{
   char buf[12];
   XmwXbaeList *list = getXmwXbaeList(gdsFiles_);
   list->deleteAll();
   if (theMapFeatures) {
	  int i, n = theMapFeatures->nGlyphs();
	  String *files = new String [3 * n];
	  for (i = 0; i < n; i ++) {
		 files[i*3 + 0] = strdup(theMapFeatures->glyph(i)->name());
		 sprintf(buf, "%.1f", theMapFeatures->glyph(i)->minScale());
		 files[i*3 + 1] = strdup(buf);
		 sprintf(buf, "%.1f", theMapFeatures->glyph(i)->maxScale());
		 files[i*3 + 2] = strdup(buf);
	  }
	  list->appendRows(files, NULL, n);
	  for (i = 0; i < 3 * n; i ++) {
		 delete [] files[i];
	  }
	  delete [] files;
   } else {
	  String file[3];
	  file[0] = strdup("");
	  sprintf(buf, "%.1f", theDrawingArea->min_scale());
	  file[1] = strdup(buf);
	  sprintf(buf, "%.1f", theDrawingArea->max_scale());
	  file[2] = strdup(buf);
	  list->appendRows(file, NULL, 1);		// one row
	  delete [] file[0];
	  delete [] file[1];
	  delete [] file[2];
   }
}


// These overload the virtual functions defined in the based class

void DRN_MapDialog::okay(Widget w, XtPointer tag, XtPointer data)
{
   XmwXbaeList *list = getXmwXbaeList(gdsFiles_);
   list->commitEdit();
   int num = list->nRows();

   if (theMapFeatures) {
	  delete theMapFeatures;
   }
   if (num) {
	  theMapFeatures = new DRN_MapFeature;

	  const char *file, *minscale, *maxscale;
	  float mins, maxs;
	  GDS_Glyph *glyph;
	  int count = 0;
	  for (int i = 0; i < num; i ++) {
		 file = list->getCell(i, 0);
		 if (!IsEmpty(file)) {
			minscale = list->getCell(i, 1);
			maxscale = list->getCell(i, 2);
			if (IsEmpty(minscale)) {
			   mins = theDrawingArea->min_scale();
			} else {
			   mins = atof(minscale);
			}
			if (IsEmpty(maxscale)) {
			   maxs = theDrawingArea->max_scale();
			} else {
			   maxs = atof(maxscale);
			}
			glyph = new GDS_Glyph(file, mins, maxs);
			theMapFeatures->add(glyph);
			count ++;
		 }
	  }
	  if (!count) {
		 delete theMapFeatures;
		 theMapFeatures = NULL;
	  }
   } else {
	  theMapFeatures = NULL;
   }

   double size = atof(XmtInputFieldGetString(gridSize_));
   size *= theBaseParameter->lengthFactor();
   theDrawingArea->gridSize(size);

   // START: Get around of a symbol bug (symbol value not updated
   // automatically)

   if (XmToggleButtonGetState(isGridOn_)) {
	  theDrnSymbols()->isGridOn().set(True);
   } else {
	  theDrnSymbols()->isGridOn().set(False);
   }

   if (XmToggleButtonGetState(isMapFeaturesOn_)) {
	  theDrnSymbols()->isMapFeaturesOn().set(True);
   } else {
	  theDrnSymbols()->isMapFeaturesOn().set(False);
   }

   if (XmToggleButtonGetState(isLegendOn_)) {
	  theDrnSymbols()->isLegendOn().set(True);
   } else {
	  theDrnSymbols()->isLegendOn().set(False);
   }

   // END

   unmanage();
   theDrawingArea->setState(DRN_DrawingArea::REDRAW);
   theMenu->needSave();
}

void DRN_MapDialog::cancel(Widget w, XtPointer tag, XtPointer data)
{
   unmanage(widget_);
}

void DRN_MapDialog::help(Widget, XtPointer, XtPointer)
{
   theBaseInterface->openUrl("mapFeatures", "drn.html");
}


void DRN_MapDialog::post()
{
   buildGdsFileLists();

   char buf[12];
   double size = theDrawingArea->gridSize() / theBaseParameter->lengthFactor();
   sprintf(buf, "%.1lf", size);
   XmtInputFieldSetString(gridSize_, buf);

   XmToggleButtonSetState(isGridOn_, theDrnSymbols()->isGridOn().value(),
						  False);

   XmToggleButtonSetState(isMapFeaturesOn_,
						  theDrnSymbols()->isMapFeaturesOn().value(),
						  False);

   XmToggleButtonSetState(isLegendOn_,
						  theDrnSymbols()->isLegendOn().value(),
						  False);

   XmwDialogManager::post();
}
