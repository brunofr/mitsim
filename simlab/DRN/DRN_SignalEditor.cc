//-*-c++-*------------------------------------------------------------
// DRN_SignalEditor.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//--------------------------------------------------------------------

#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <iostream>

#include <Xmt/Xmt.h>
#include <Xmt/Dialogs.h>
#include <Xmt/Create.h>
#include <Xmt/InputField.h>
#include <Xbae/Matrix.h>

#include <Tools/ToolKit.h>
#include <Tools/Math.h>

#include <Xmw/XmwColor.h>

#include <GRN/Parameter.h>
#include <GRN/NameTable.h>
#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>

#include "DRN_SignalEditor.h"
#include "DRN_Interface.h"
#include "DRN_DrawingArea.h"
#include "DRN_Segment.h"
#include "DRN_Lane.h"
#include "DRN_Signal.h"
#include "DrawableRoadNetwork.h"
using namespace std;

DRN_SignalEditor::DRN_SignalEditor ( Widget parent )
   : XmwDialogManager(parent, "signalEditor", NULL, 0,
					  "okay", "cancel")
{
   if (theBaseInterface) {
	  theBaseInterface->showBusyCursor();
   }

   XtVaSetValues(widget_,
				 XmNdialogStyle, XmDIALOG_APPLICATION_MODAL,
				 NULL);
  
   position_ = XmtNameToWidget(widget_, "position");
   assert(position_);

   type_ = XmtNameToWidget(widget_, "type");
   assert(type_);

   visibility_ = XmtNameToWidget(widget_, "visibility");
   assert(visibility_);
   
   lanes_ = XmtNameToWidget(widget_, "lanes");
   assert(lanes_);

   addCallback(lanes_, XmNenterCellCallback,
			   &DRN_SignalEditor::enter, NULL);

//    addCallback(lanes_, XmNleaveCellCallback,
// 			   &DRN_SignalEditor::leave, NULL);

}


DRN_SignalEditor::~DRN_SignalEditor()
{
}


void DRN_SignalEditor::enter( Widget /* w */, XtPointer,
							  XtPointer data )
{
   XbaeMatrixEnterCellCallbackStruct *p;
   p = (XbaeMatrixEnterCellCallbackStruct *) data;

   p->select_text = True;

   //    cout << "Enter Row" << p->row << endl;
}

// void DRN_SignalEditor::leave( Widget w, XtPointer, XtPointer data )
// {
//    XbaeMatrixLeaveCellCallbackStruct *p;
//    p = (XbaeMatrixLeaveCellCallbackStruct *) data;

//    if (!ToolKit::comp(p->value, XbaeMatrixGetCell(w, p->row, p->column))) {
// 	  return;
//    }
// }

// These overload the functions in base class

void DRN_SignalEditor::cancel(Widget, XtPointer, XtPointer)
{
   unmanage();
}


void DRN_SignalEditor::okay(Widget, XtPointer, XtPointer)
{
   RN_Segment *ps = station_->segment();
   const char* str = XmtInputFieldGetString(visibility_);
   station_->visibility(atof(str) * theBaseParameter->lengthFactor());

   XbaeMatrixCommitEdit(lanes_, False);

   int n, k = 0;
   if (station_->isLinkWide()) {
	 n = 1;
   } else {
	 n = ps->nLanes();
   }

   for (int i = 0; i < n; i ++) {
	  RN_Signal *sn = station_->signal(i);
	  if (!sn) continue;
	  str = XbaeMatrixGetCell(lanes_, i, 0);
	  unsigned int state;
	  if (station_->type() != CTRL_VSLS) {
		 state = strtoul(str, NULL, 16);
	  } else {
		 state = strtoul(str, NULL, 10);
	  }
	  state = sn->stateIoToInternal(state);
	  sn->state(state);
	  k ++;
   }
   unmanage();
}

void DRN_SignalEditor::help(Widget, XtPointer, XtPointer)
{
   theDrnInterface->openUrl("incident", "mitsim.html");
}


void DRN_SignalEditor::postAndWait(RN_CtrlStation *s)
{
   char buffer[128];
   station_ = s;

   RN_Segment *ps = s->segment();

   sprintf(buffer, "%d / %.2f", ps->code(), s->position());
   XtVaSetValues(position_,
				 RES_CONVERT(XmNlabelString, buffer), NULL);

   sprintf(buffer, "%s", NameTable::signal(s->type()));
   XtVaSetValues(type_, RES_CONVERT(XmNlabelString, buffer), NULL);

   float vis = s->visibility() / theBaseParameter->lengthFactor();
   sprintf(buffer, "%.0f", vis);
   XmtInputFieldSetString(visibility_, buffer);

   int i, nrows, nlanes;

   XtVaGetValues(lanes_, XmNrows, &nrows, NULL);
   XbaeMatrixDeleteRows(lanes_, 0, nrows);

   if (s->isLinkWide()) {
	 nlanes = 1;
   } else {
	 nlanes = ps->nLanes();
   }

   String *labels = new String [nlanes];
   String *states = new String [nlanes];

   RN_Signal *sn;
   nrows = 0;
   for (i = 0; i < nlanes; i ++) {
	  sn = station_->signal(i);
	  if (!sn) continue;
	  labels[i] = Copy(sn->code());
	  unsigned int state = sn->stateInternalToIo(sn->state());
	  if (s->type() == CTRL_VSLS) {
		sprintf(buffer, "%d", state);
	  } else {
		sprintf(buffer, "%x", state);
	  }
	  states[i] = Copy(buffer);
	  nrows ++;
   }

   XbaeMatrixAddRows(lanes_, 0, states, labels, NULL, nrows);

   XmwWidget::manage(widget_);

   for (i = 0; i < nrows; i ++) {
	  free (labels[i]);
	  free (states[i]);
   }
   delete [] states;
   delete [] labels;

   ready();
}


void DRN_SignalEditor::createAndPost(RN_CtrlStation *s)
{   
   static DRN_SignalEditor *dialog = NULL;
   if (!dialog) {
	 dialog = new DRN_SignalEditor(theDrnInterface->widget());
   }
   dialog->postAndWait(s);
}

