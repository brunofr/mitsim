//-*-c++-*------------------------------------------------------------
// DRN_Menu.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This is the implementation for class DRN_Menu
//--------------------------------------------------------------------

#include <cstdio>
#include <cstdlib>
#include <sstream>
#include <fstream>
#include <cassert>
#include <sys/param.h>
using namespace std;

#include <Xmt/Xmt.h>
#include <Xmt/Create.h>
#include <Xmt/Menu.h>
#include <Xmt/Dialogs.h>
#include <Xmt/Dialog.h>

#include <IO/Exception.h>

#include <Tools/ToolKit.h>
#include <Tools/Options.h>
#include <Tools/SimulationClock.h>
#include <Tools/SimulationEngine.h>

#include <Xmw/XmwModeline.h>
#include <Xmw/XmwOptionDialog.h>

#include "DRN_Menu.h"
#include "DRN_Symbols.h"
#include "DRN_Interface.h"
#include "DRN_DrawingArea.h"
#include "DRN_Segment.h"
#include "DRN_Node.h"
#include "DRN_FindDialog.h"
#include "DRN_SegmentCCDialog.h"
#include "DRN_SensorDialog.h"
#include "DRN_SignalDialog.h"
#include "DRN_MapDialog.h"
#include "DRN_MapFeature.h"
#include "DRN_SegmentAttributeDialog.h"
#include "DRN_ViewMarker.h"
#include "DRN_ExportDialog.h"
#include "DrawableRoadNetwork.h"

DRN_Menu * theMenu = NULL;

// If you change these defaults, please make corresponding changes for
// the menu items in the constructor of Menu

DRN_Menu::DRN_Menu(Widget parent) : XmwMenu(parent)
{
  Widget popup = XmtNameToWidget(parent, "*drawingArea*popup");

  Widget w;

  run_ = getItem("Run");
  assert (run_);
  addCallback(getItemWidget(run_),
	      XmNactivateCallback, &DRN_Menu::run,
	      NULL );
  deactivate(run_);


  pause_ = getItem("Pause");
  assert (pause_);
  addCallback(getItemWidget(pause_),
	      XmNactivateCallback, &DRN_Menu::pause,
	      NULL );
  deactivate(pause_);

  w = getItemWidget("Export");
  assert (w);
  addCallback(w,
	      XmNactivateCallback, &DRN_Menu::eXport,
	      NULL );

  w = getItemWidget("Option");
  assert (w);
  addCallback(w,
	      XmNactivateCallback, &DRN_Menu::option,
	      NULL );
  
  w = getItemWidget("Parameter");
  assert (w);
  addCallback(w,
	      XmNactivateCallback, &DRN_Menu::parameter,
	      NULL );

  view_ = getItem("View");
  assert (view_);
  addCallback(getItemWidget(view_),
	      XmNvalueChangedCallback, &DRN_Menu::view,
	      NULL);

  w = getItemWidget("MapFeature");
  assert (w);
  addCallback(w,
	      XmNactivateCallback, &DRN_Menu::mapFeature,
	      NULL );
  if (popup && (w = getItemWidget(popup, "MapFeature"))) {
    addCallback(w,
		XmNactivateCallback, &DRN_Menu::mapFeature,
		NULL );
  }

  w = getItemWidget("RoadSegment");
  assert (w);
  addCallback(w,
	      XmNactivateCallback, &DRN_Menu::roadSegment,
	      NULL );
  if (popup && (w = getItemWidget(popup, "RoadSegment"))) {
    addCallback(w,
		XmNactivateCallback, &DRN_Menu::roadSegment,
		NULL );
  }

  w = getItemWidget("Sensor");
  assert (w);
  addCallback(w,
	      XmNactivateCallback, &DRN_Menu::sensor,
	      NULL );
  if (popup && (w = getItemWidget(popup, "Sensor"))) {
    addCallback(w,
		XmNactivateCallback, &DRN_Menu::sensor,
		NULL );
  }

  w = getItemWidget("Signal");
  assert (w);
  addCallback(w,
	      XmNactivateCallback, &DRN_Menu::signal,
	      NULL );
  if (popup && (w = getItemWidget(popup, "Signal"))) {
    addCallback(w,
		XmNactivateCallback, &DRN_Menu::signal,
		NULL );
  }

  w = getItemWidget("LinearColor");
  assert (w);
  addCallback(w,
	      XmNactivateCallback, &DRN_Menu::linearColor,
	      NULL );

  w = getItemWidget("Bookmark");
  assert (w);
  addCallback(w,
	      XmNactivateCallback, &DRN_Menu::viewMarker,
	      NULL );

  w = getItemWidget("SymbolColor");
  assert (w);
  addCallback(w,
	      XmNactivateCallback, &DRN_Menu::symbolColor,
	      NULL );

  w = getItemWidget("SegmentAttribute");
  assert (w);
  addCallback(w,
	      XmNactivateCallback, &DRN_Menu::segmentAttribute,
	      NULL );
  if (popup && (w = getItemWidget(popup, "SegmentAttribute"))) {
    addCallback(w,
		XmNactivateCallback, &DRN_Menu::segmentAttribute,
		NULL );
  }

  w = getItemWidget("FindObject");
  assert (w);
  addCallback(w,
	      XmNactivateCallback, &DRN_Menu::findObject,
	      NULL );
  if (popup && (w = getItemWidget(popup, "FindObject"))) {
    addCallback(w,
		XmNactivateCallback, &DRN_Menu::findObject,
		NULL );
  }

  theMenu = this;
}

// Set initial state

void DRN_Menu::updateButtons()
{
  if (theSimulationClock->isStarted()) {
    deactivate(open_);
  } else {
    activate(open_);
  }

  if (!theSimulationEngine->canStart() ||
      theSimulationEngine->isRunning()) {
    deactivate(run_);
  } else {
    activate(run_);
  }

  if (!theSimulationEngine->master() ||
      !theSimulationEngine->isRunning()) {
    deactivate(pause_);
  } else {
    activate(pause_);
  }
}

void DRN_Menu::updateRunningState(int running)
{
  if (running) {
    activate(pause_);
    deactivate(run_);
  } else {
    activate(run_);
    deactivate(pause_);
  }
}

// Callbacks for File pane

void DRN_Menu::open ( Widget w, XtPointer, XtPointer)
{
  char filename[MAXPATHLEN + 1];
  char directory[MAXPATHLEN + 1];
  char pattern[MAXPATHLEN + 1];

  filename[0] = '\0';
  directory[0] = '\0';
  strcpy(pattern, "*");
  strcat(pattern, theSimulationEngine->ext());

  while (XmtAskForFilename(w,
			   "openFileDialog", 
			   "Please type or select a master file:",
			   ToolKit::inDir(), NULL,
			   filename, sizeof(filename),
			   directory, sizeof(directory),
			   pattern, 0,
			   NULL)) {
    theSimulationEngine->master(filename);
    if (theSimulationEngine->loadMasterFile() == 0) {
      break;
    }
  }
}

// Returns 1 if error

int DRN_Menu::save ()
{
  ofstream os(theSimulationEngine->master());
  if (os.good()) {
    theDrnInterface->msgSet(XmtLocalize2(widget_, "Saving",
					 "prompt", "saving"));
    theDrnInterface->msgAppend(" ");
    theDrnInterface->msgAppend(theSimulationEngine->master());

    theSimulationEngine->save(os);
    os.close();

    if (isSaveNeeded(STATE_SAVE_NETWORK)) {
      theDrnInterface->msgAppend(" + ");
      theDrnInterface->msgAppend(theNetwork->name());
      theNetwork->save();
    }

    if (isSaveNeeded(STATE_SAVE_GDS)) {
      theDrnInterface->msgAppend(" + ");
      theDrnInterface->msgAppend("Map Features");
      theMapFeatures->save();
    }

    theDrnInterface->msgAppend(" ");
    theDrnInterface->msgAppend(XmtLocalize2(widget_, "done.",
					    "prompt", "done"));
    theDrnInterface->msgClear(MSG_STAY_TIME);
    deactivate(save_);
    unsetState(STATE_SAVE);

    return 0;
  } else {
    theDrnInterface->outputFileError(theSimulationEngine->master());
    return 1;
  }
}

void DRN_Menu::save ( Widget w, XtPointer tag, XtPointer data )
{
  if (!theSimulationEngine->master() || save() != 0) {
    saveAs (w, tag, data);
  }
}

void DRN_Menu::saveAs ( Widget w, XtPointer, XtPointer)
{
  char filename[MAXPATHLEN + 1];
  char directory[MAXPATHLEN + 1];
  char pattern[MAXPATHLEN + 1];

  filename[0] = '\0';
  directory[0] = '\0';
  strcpy(pattern, "*");
  strcat(pattern, theSimulationEngine->ext());

  Boolean done = 0;

  while (!done &&
	 XmtAskForFilename(w, "askOutputFile", 
			   "Enter an output filename:",
			   ToolKit::inDir(), NULL,
			   filename, sizeof(filename),
			   directory, sizeof(directory),
			   pattern, 0,
			   NULL)) {
    if (ToolKit::fileExists(filename)) {
      XmtAskForBoolean(w, "confirmFileOverwrite", // reource querey name
		       "File already exists!", // defaul prompt
		       "Overwrite",	// yes default
		       "No",	// no default
		       "Cancel",	// cancel default
		       XmtNoButton, // default button
		       0,	// default icon type
		       False, // show cancel button
		       &done,
		       NULL);
    } else {
      done = True;
    }
    if (done) {
      theSimulationEngine->master(filename);
      if (save() == 0) done = True;
      else done = False;
    }
  }
}

void DRN_Menu::print ( Widget w, XtPointer, XtPointer)
{
  const int len = 64;
  static char lpcmd[len] = "lpr -Plute";

  if (!XmtAskForString(w, "printCmd", // query name
		       "Enter the print command",	// default prompt
		       lpcmd,	// buf_in_out
		       len,		// buf_len
		       "Example: lpr -Ptempo")) {
    return;
  }

  theDrawingArea->redraw();
  theDrawingArea->foreground(theColorTable->red());
  theDrawingArea->drawBoundingBox();
  theDrawingArea->makePrintable();

  char filename[32];
  sprintf(filename, "xwd%d.ps", getpid());
  const char *psfile = ToolKit::outfile(filename);
  ostringstream cmd;
  cmd << "xwd -id " << theDrawingArea->window() << " | "
      << "xpr -output " << psfile << " ; ";
   
  if (!IsEmpty(lpcmd)) {
    cmd << lpcmd << " " << psfile;
  }
  system (cmd.str().c_str());


  if (!IsEmpty(lpcmd)) {
    remove(psfile);
  } else {
    theDrnInterface->msgSet("Image dumped in ");
    theDrnInterface->msgAppend(psfile);
  }

  theDrawingArea->setState(DRN_DrawingArea::REDRAW);
}

void DRN_Menu::quit ( Widget, XtPointer, XtPointer )
{
  theSimulationEngine->quit(STATE_QUIT);
}

// Callbacks for Simulation pane

void DRN_Menu::run ( Widget, XtPointer, XtPointer )
{
  theDrnInterface->run(True);
}

void DRN_Menu::pause ( Widget, XtPointer, XtPointer )
{
  theDrnInterface->pause(True);
}

void DRN_Menu::option ( Widget, XtPointer, XtPointer )
{
  static XmwOptionDialog dialog(theDrnInterface->widget());
  dialog.post();
}


void DRN_Menu::parameter ( Widget w, XtPointer, XtPointer )
{
  static XmwDialogManager dialog(w, "parameterDialog");
  dialog.post();
}


void DRN_Menu::view ( Widget, XtPointer, XtPointer)
{
  theDrnInterface->view(XmtMenuItemGetState(view_));
}

void DRN_Menu::mapFeature ( Widget, XtPointer, XtPointer )
{
  static DRN_MapDialog dialog(theDrnInterface->widget());
  dialog.post();
}


void DRN_Menu::roadSegment ( Widget, XtPointer, XtPointer )
{
  static DRN_SegmentCCDialog dialog(theDrnInterface->widget());
  dialog.post();
}


void DRN_Menu::sensor ( Widget, XtPointer, XtPointer )
{
  static DRN_SensorDialog dialog(theDrnInterface->widget());
  dialog.post();
}


void DRN_Menu::signal ( Widget w, XtPointer, XtPointer )
{
  static DRN_SignalDialog dialog(theDrnInterface->widget());
  dialog.post();
}

void DRN_Menu::symbolColor ( Widget, XtPointer, XtPointer )
{
  theDrawingArea->pointObjectsColorRange();
}

void DRN_Menu::linearColor ( Widget, XtPointer, XtPointer )
{
  theDrawingArea->lineObjectsColorRange();
}

void DRN_Menu::viewMarker ( Widget w, XtPointer, XtPointer )
{
  if (!theNetwork) {
    theDrnInterface->NoNetwork() ;
    return ;
  }
  char buffer[64] ;
  sprintf(buffer, "View %d", DRN_ViewMarker::nMarkers()) ;
  Boolean ret = XmtAskForString(w, "View Marker", 
				"View label",
				buffer, 64,
				"Set a marker for current view") ;
  if (ret) {
    DRN_ViewMarker *marker ;
    marker = new DRN_ViewMarker;
    marker->init(theDrawingArea, buffer) ;
    DRN_ViewMarker::AddMarker(marker) ;
  }
}

void DRN_Menu::eXport ( Widget, XtPointer, XtPointer )
{
  if (theNetwork) {
    static DRN_ExportDialog dialog(theDrnInterface->widget());
    dialog.post();
  } else {
    theDrnInterface->NoNetwork();
  }
}

void DRN_Menu::findObject ( Widget, XtPointer, XtPointer )
{
  if (theNetwork) {
    static DRN_FindDialog dialog(theDrnInterface->widget());
    dialog.post();
  } else {
    theDrnInterface->NoNetwork() ;
  }
}

void DRN_Menu::segmentAttribute ( Widget, XtPointer, XtPointer )
{
  if (!theNetwork) {
    theDrnInterface->NoNetwork() ;
    return ;
  }

  static DRN_SegmentAttributeDialog *dialog = NULL;
  static RoadNetwork *network = NULL;

  // This dialog box is cached.  However, it will create a new one if
  // the network has been changed.

  if (theNetwork != network) {
    if (dialog) delete dialog;
    dialog = new DRN_SegmentAttributeDialog(theDrnInterface->widget());
    network = theNetwork;
  }
  dialog->post();
}
