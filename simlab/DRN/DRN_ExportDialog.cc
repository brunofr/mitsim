//-*-c++-*------------------------------------------------------------
// DRN_ExportDialog.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <cassert>
#include <iostream>
#include <cstdlib>

#include <Xmt/Xmt.h>
#include <Xmt/Dialog.h>
#include <Xmt/Chooser.h>
#include <Xmt/Create.h>

#include <Xmw/XmwInterface.h>
#include <Xmw/XmwMenu.h>

#include "DRN_ExportDialog.h"
#include "DRN_Interface.h"
#include "DrawableRoadNetwork.h"
using namespace std;

XtResource DRN_ExportDialog::resources[] =
{
  { "type", "type", XtRInt,
    sizeof(int), XtOffsetOf(ExportSpec, type),
    XtRImmediate, (XtPointer)0
  },
} ;

ExportSpec DRN_ExportDialog::exportSpec = {
  0				// type
} ;

DRN_ExportDialog::DRN_ExportDialog(Widget parent)
  : XmwDialogManager(parent, "exportDialog",
		     DRN_ExportDialog::resources,
		     XtNumber(DRN_ExportDialog::resources))
{
  XtVaSetValues(widget_,
		XmNdialogStyle, XmDIALOG_PRIMARY_APPLICATION_MODAL,
		NULL);
}


// These overload the virtual functions defined in the based class

void DRN_ExportDialog::okay(Widget w, XtPointer tag, XtPointer data)
{
  XmtDialogOkayCallback(w, tag, data) ;
  theDrnInterface->msgSet("Exporting ... ") ;
  int status = theNetwork->Export(exportSpec.type) ;
  if (status < 0) {
    theDrnInterface->msgAppend("error.") ;
  } else if (status > 0) {
    theDrnInterface->msgAppend("not supported.") ;
  } else {
    theDrnInterface->msgAppend("done.") ;
  }
}

void DRN_ExportDialog::cancel(Widget w, XtPointer tag, XtPointer data)
{
  XmtDialogCancelCallback(w, tag, data);
}

void DRN_ExportDialog::help(Widget, XtPointer, XtPointer)
{
  theBaseInterface->openUrl("export", "drn.html");
}

void DRN_ExportDialog::post()
{
  XmtDialogDo(widget(), &exportSpec);
  XmwDialogManager::ready();
}
