//-*-c++-*------------------------------------------------------------
// DRN_PathDialog.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//--------------------------------------------------------------------

#include <string.h>
#include <stdio.h>
#include <assert.h>

#include <Xmt/Xmt.h>
#include <Xmt/Create.h>
#include <Xmt/Dialogs.h>
#include <Xmt/InputField.h>

#include <Tools/ToolKit.h>

#include <Xmw/XmwSciPlot.h>

#include "DRN_PathDialog.h"
#include "DRN_DrawingArea.h"
#include "DRN_FindDialog.h"
#include "DRN_Interface.h"

#include <GRN/RN_PathTable.h>
#include <GRN/RN_Link.h>
#include <GRN/RN_Path.h>
#include <GRN/RN_DynamicRoute.h>

DRN_PathDialog::DRN_PathDialog ( Widget parent )
   : XmwDialogManager(parent, "pathDialog", NULL, 0, "show", "close"),
	 index_(0)
{
   odcode_ = XmtNameToWidget(widget_, "*odcode");
   assert (odcode_);
   addCallback(odcode_, XmtNinputCallback, &DRN_PathDialog::odcodeCB, NULL);

   plotPathTime_ = XmtNameToWidget(widget_, "*plotPathTime");
   assert (plotPathTime_);
   addCallback(plotPathTime_, XmNactivateCallback,
			   &DRN_PathDialog::plotPathTimeCB, NULL);

   first_ = XmtNameToWidget(widget_, "*first");
   assert (first_);
   addCallback(first_, XmNactivateCallback, &DRN_PathDialog::firstCB, NULL);

   prev_ = XmtNameToWidget(widget_, "*prev");
   assert (prev_);
   addCallback(prev_, XmNactivateCallback, &DRN_PathDialog::prevCB, NULL);

   pathcode_ = XmtNameToWidget(widget_, "*pathcode");
   assert (pathcode_);
   addCallback(pathcode_, XmtNinputCallback, &DRN_PathDialog::pathcodeCB, NULL);

   next_ = XmtNameToWidget(widget_, "*next");
   assert (next_);
   addCallback(next_, XmNactivateCallback, &DRN_PathDialog::nextCB, NULL);

   last_ = XmtNameToWidget(widget_, "*last");
   assert (last_);
   addCallback(last_, XmNactivateCallback, &DRN_PathDialog::lastCB, NULL);
}

void DRN_PathDialog::odcodeCB(Widget w, XtPointer tag, XtPointer data)
{
   if (!data) return;

   const char *od = (const char*) data;
   int o, d;
   if (GetPair(od, &o, &d)) {
	  XmtMsgLinePrintf(theDrnInterface->msgline(),
		 "No OD pair %d-%d found.", o, d);
	  return;
   }
   for (int i = 0; i < thePathTable->nPaths(); i ++) {
	  RN_Path *p = thePathTable->path(i);
	  if (p->oriCode() == o && p->desCode() == d) {
		 index_ = i;
		 setPath(); updateSensitivities(); draw();
		 return;
	  }
   }
   XmtMsgLinePrintf(theDrnInterface->msgline(),
	  "No path found for OD pair %d-%d.", o, d);
}

void DRN_PathDialog::plotPathTimeCB(Widget, XtPointer, XtPointer)
{
   const char *od = XmtInputFieldGetString(odcode_);
   if (!od) return ;
   int o, d;
   if (GetPair(od, &o, &d)) {
	  XmtMsgLinePrintf(theDrnInterface->msgline(),
		 "No OD pair %d-%d found.", o, d);
	  return;
   }
   for (int i = 0; i < thePathTable->nPaths(); i ++) {
	  RN_Path *p = thePathTable->path(i);
	  if (p->oriCode() == o && p->desCode() == d) {
		 index_ = i;
		 setPath(); updateSensitivities(); draw();
		 plotPathTimes(o, d, i);
		 return;
	  }
   }
   XmtMsgLinePrintf(theDrnInterface->msgline(),
	  "No path found for OD pair %d-%d.", o, d);
}

void DRN_PathDialog::plotPathTimes(int o, int d, int pindex)
{
   static XmwSciPlot *plot = NULL;
 
   if (!plot) {
	  plot = new XmwSciPlot(theDrnInterface->widget(), "sciplotShell");
	  plot->xlabel("Dep. Time");
	  plot->ylabel("Travel Time (minutes)");
   } else {
	  plot->deleteAllSeries();
   }

   char str[64];
   sprintf(str, "Travel times on the routes connecting OD pair %d-%d", o, d);
   plot->title(str);

   if (theUnGuidedRoute != theGuidedRoute) {
	  plotPathTime(plot, "Historical", theUnGuidedRoute, o, d, pindex);
	  plotPathTime(plot, "ATIS",  theGuidedRoute, o, d, pindex);
   } else {
	  plotPathTime(plot, NULL, theGuidedRoute, o, d, pindex);
   }

   plot->post(&plot);
   theDrnInterface->showDefaultCursor();
}

void DRN_PathDialog::plotPathTime(XmwSciPlot *plot,
								  const char *label, RN_Route *r,
								  int o, int d, int pindex)
{
   int i;
   int n = r->infoPeriods();
   float *x = new float [n];
   float *y = new float [n];
   char str[32];

   for (i = 0; i < n; i ++) {
	  x[i] = r->whatTime(i) / 3600;
   }

   RN_Path *p = thePathTable->path(pindex);

   while (p && p->oriCode() == o && p->desCode() == d) {

	  if (label) {
		 sprintf(str, "%d %s", p->code(), label);
	  } else {
		 sprintf(str, "%d", p->code());
	  }
	  for (i = 0; i < n; i ++) {
		 y[i] = p->travelTime(r, 0, x[i] * 3600.0) / 60.0;
	  }
	  plot->createSeries(n, x, y, str);

	  pindex ++;
	  if (pindex < thePathTable->nPaths()) {
		 p = thePathTable->path(pindex);
	  } else {
		 p = NULL;
	  } 
   }

   delete [] x;
   delete [] y;
}

void DRN_PathDialog::pathcodeCB(Widget w, XtPointer tag, XtPointer data)
{
   const char *p = (const char*) data;
   RN_Path *i = thePathTable->findPath(atoi(p));
   if (i) {
	  index_ = i->index();
	  setPair(); updateSensitivities(); draw();
   } else {
	  XmtMsgLinePrintf(theDrnInterface->msgline(),
		 "No path %s found.", p);
   }
}


void DRN_PathDialog::firstCB(Widget, XtPointer, XtPointer)
{
   index_ = 0;
   setPath(); setPair(); updateSensitivities(); draw();
}

void DRN_PathDialog::prevCB(Widget, XtPointer, XtPointer)
{
   if (index_ > 0) {
	  index_ --;
   }
   setPath(); setPair(); updateSensitivities(); draw();
}


void DRN_PathDialog::nextCB(Widget, XtPointer, XtPointer)
{
   if (index_ < thePathTable->nPaths() - 1) {
	  index_ ++;
   }
   setPath(); setPair(); updateSensitivities(); draw();
}


void DRN_PathDialog::lastCB(Widget, XtPointer, XtPointer)
{
   index_ = thePathTable->nPaths() - 1;
   setPath(); setPair(); updateSensitivities(); draw();
}

void DRN_PathDialog::setPath()
{
   char buf[7];
   RN_Path *p = thePathTable->path(index_);

   sprintf(buf, "%d", p->code());
   XmtInputFieldSetString(pathcode_, buf);
}

void DRN_PathDialog::setPair()
{
   char buf[13];
   RN_Path *p = thePathTable->path(index_);
   sprintf(buf, "%d-%d", p->oriCode(), p->desCode());
   XmtInputFieldSetString(odcode_, buf);
}

void DRN_PathDialog::post()
{
   if (thePathTable) {
	  activate();
   } else {
	  deactivate();
	  theDrnInterface->msgShow(XmtLocalize2(widget_,
		 "No path table", "prompt", "noPathTable"));
   }
   XmwWidget::manage(widget_);
   theBaseInterface->showDefaultCursor();
}


void DRN_PathDialog::updateSensitivities()
{
   if (index_ <= 0) {
	  XmwDialogManager::deactivate(first_);
	  XmwDialogManager::deactivate(prev_);
   } else if (thePathTable->nPaths() > 0) {
	  XmwDialogManager::activate(first_);
	  XmwDialogManager::activate(prev_);
   }
   if (index_ >= thePathTable->nPaths() - 1) {
	  XmwDialogManager::deactivate(next_);
	  XmwDialogManager::deactivate(last_);
   } else if (thePathTable->nPaths() > 0) {
	  XmwDialogManager::activate(next_);
	  XmwDialogManager::activate(last_);
   }
}

void DRN_PathDialog::activate()
{
   XmwDialogManager::activate(odcode_);
   if (theGuidedRoute && theGuidedRoute->infoPeriods() > 1 ||
	   theUnGuidedRoute && theUnGuidedRoute->infoPeriods() > 1) {
	  XmwDialogManager::activate(plotPathTime_);
   } else {
	  XmwDialogManager::deactivate(plotPathTime_);
   }
   XmwDialogManager::activate(first_);
   XmwDialogManager::activate(prev_);
   XmwDialogManager::activate(pathcode_);
   XmwDialogManager::activate(next_);
   XmwDialogManager::activate(last_);
   XmwDialogManager::activate(okay_);
}

void DRN_PathDialog::deactivate()
{
   XmwDialogManager::deactivate(odcode_);
   XmwDialogManager::deactivate(plotPathTime_);
   XmwDialogManager::deactivate(first_);
   XmwDialogManager::deactivate(prev_);
   XmwDialogManager::deactivate(pathcode_);
   XmwDialogManager::deactivate(next_);
   XmwDialogManager::deactivate(last_);
   XmwDialogManager::deactivate(okay_);
}

void DRN_PathDialog::draw()
{
   theDrawingArea->redraw();

   char *p = XmtInputFieldGetString(pathcode_);
   if (IsEmpty(p)) return;

   theDrawingArea->locate(DRN_FindDialog::FIND_PATH, p,
						  DRN_FindDialog::ANNOTATION_SYMBOL |
						  DRN_FindDialog::ANNOTATION_LABEL);   
}

void DRN_PathDialog::okay(Widget, XtPointer, XtPointer)
{
   draw();
}

void DRN_PathDialog::cancel(Widget, XtPointer, XtPointer)
{
   unmanage();
}

void DRN_PathDialog::help(Widget, XtPointer, XtPointer)
{
   theDrnInterface->openUrl("path", "drn.html");
}
