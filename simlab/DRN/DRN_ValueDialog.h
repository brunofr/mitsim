//-*-c++-*------------------------------------------------------------
// NAME: Value dialog
// AUTH: Qi Yang
// FILE: DRN_ValueDialog.h
// DATE: Tue Feb 13 10:09:19 1996
//--------------------------------------------------------------------

#ifndef DRN_VALUE_DIALOG_HEADER
#define DRN_VALUE_DIALOG_HEADER

#include <X11/Xlib.h>

class DRN_ValueDialog
{
   public:

      DRN_ValueDialog(Widget parent,
		      double &value,
		      double minValue, double  maxValue,
		      double scaling, short int decimals,
		      char *title, char *label);

      ~DRN_ValueDialog();

      static void getValue(
	 Widget parent,
	 double &value,
	 double minValue, double  maxValue,
	 double scaling = 1.0, short int decimals = 1,
	 char * title = "", char *label = "");

   protected:

      Widget _parent;
      Widget _baseWidget;
	     
      Widget _valueScale;
      Widget _okButton;

      double scaling_;		// convert units
      double& value_;		// value

   protected:

      virtual void changeValue ( Widget, XtPointer );
      virtual void valueOk ( Widget, XtPointer );

   private: 

      virtual void read();

      // Callbacks to interface with Motif

      static void valueChangedCallback ( Widget, XtPointer, XtPointer );
      static void valueDragCallback ( Widget, XtPointer, XtPointer );
      static void valueOkCallback ( Widget, XtPointer, XtPointer );
};

#endif
