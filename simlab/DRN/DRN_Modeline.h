//-*-c++-*------------------------------------------------------------
// DRN_Modeline.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef DRN_MODELINE_HEADER
#define DRN_MODELINE_HEADER

#include <Xmw/XmwModeline.h>

class DRN_Modeline : public XmwModeline
{
   public:

	  DRN_Modeline(Widget parent);
	  ~DRN_Modeline() { }

	  void keyStateChanged(enum KeyState state); // virtual

	  void updateButtons();
	  void update();				// virtual
};

#endif
