//-*-c++-*------------------------------------------------------------
// DRN_Modeline.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <Tools/SimulationEngine.h>
#include <Tools/SimulationClock.h>

#include "DRN_Modeline.h"
#include "DRN_Interface.h"

DRN_Modeline::DRN_Modeline(Widget parent)
   : XmwModeline(parent)
{
   static String help_meters[2];
   help_meters[0] = XmtLocalize2(meters_,
								 "Desired time factor",
								 "meters", "desired");
   help_meters[1] = XmtLocalize2(meters_,
								 "Actual time factor",
								 "meters", "actual");
   XtAddCallback(meters_,
				 XmNenterCellCallback, &XmwModeline::cellEnterCB,
				 help_meters);
}


void DRN_Modeline::keyStateChanged(enum KeyState state)
{
   switch (state) {
	  case KEY_STATE_PAUSE:
	  {
		 theDrnInterface->pause(True);
		 break;
	  }
	  case KEY_STATE_RUN:
	  {
		 theDrnInterface->run(True);
		 break;
	  }
   }
}

void DRN_Modeline::updateButtons()
{
   if (!theSimulationEngine->canStart()) {
	  setKeyState(KEY_STATE_DISABLED);
   } else if (theSimulationEngine->isRunning()) {
	  setKeyState(KEY_STATE_RUN);
   } else {
	  setKeyState(KEY_STATE_PAUSE);
   }
}


void DRN_Modeline::update()
{
   updateMeter(1, theSimulationClock->actualTimeFactor());
   XmwModeline::update();
}
