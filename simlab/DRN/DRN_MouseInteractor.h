//-*-c++-*------------------------------------------------------------
// DRN_MouseInteractor.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef DRN_MOUSEINTERACTOR_HEADER
#define DRN_MOUSEINTERACTOR_HEADER

#include <Xmw/XmwMouseInteractor.h>
#include <Xmw/XmwPoint.h>

class DRN_DrawingArea;
class DrawableRoadNetwork;

class DRN_MouseInteractor : public XmwMouseInteractor
{
      friend class DrawableRoadNetwork;
      friend class DRN_DrawingArea;

   public:
      
      DRN_MouseInteractor(DRN_DrawingArea * area);
      ~DRN_MouseInteractor() { }

      // Functionality depends on the tool chosen

      virtual void leftButtonDown       ( XmwPoint& );
      virtual void leftButtonMotion     ( XmwPoint& );
      virtual void leftButtonUp         ( XmwPoint& );

      virtual void rightButtonDown       ( XmwPoint& );
      virtual void rightButtonMotion     ( XmwPoint& );
      virtual void rightButtonUp         ( XmwPoint& );

      virtual void leftButtonControlDown( XmwPoint& );
      virtual void leftButtonShiftDown  ( XmwPoint& );

      virtual void middleButtonDown     ( XmwPoint& );

      inline DRN_DrawingArea * drawingArea() {
         return (DRN_DrawingArea *) drawing_area_;
      }

   private:

      /* Drawing stuff: */

      XmwPoint anchor_pt_;
      XmwPoint prev_pt_;
};

#endif
