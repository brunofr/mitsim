//-*-c++-*------------------------------------------------------------
// DRN_SensorDialog.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef DRN_SENSORDIALOG_HEADER
#define DRN_SENSORDIALOG_HEADER

#include <Xmw/XmwDialogManager.h>
#include <Xmw/XmwCallback.h>

class DRN_SensorDialog: public XmwDialogManager
{
  	  CallbackDeclare(DRN_SensorDialog);

   public:

	  DRN_SensorDialog(Widget parent);
	  ~DRN_SensorDialog() { }
	  void post();				// virtual

   private:

	  // overload the virtual functions in base class

	  void okay(Widget, XtPointer, XtPointer);
	  void cancel(Widget, XtPointer, XtPointer);
	  void help(Widget, XtPointer, XtPointer);

	  // new callbacks

	  void clickTypesCB(Widget, XtPointer, XtPointer);
	  void clickCB(Widget, XtPointer, XtPointer);

   private:
	  
	  Widget typesFld_;			// check box
	  int types_;
	  Widget labelFld_;			// radio box
	  int label_;
	  Widget colorCodeFld_;		// radio box
	  int colorCode_;
};

#endif
