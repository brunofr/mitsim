//-*-c++-*------------------------------------------------------------
// DRN_ValueDialog.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//--------------------------------------------------------------------

#include <Xm/Form.h> 
#include <Xm/PushB.h> 
#include <Xm/Scale.h> 

#include <Tools/Math.h>
#include <Tools/Normalizer.h>

#include "DRN_ValueDialog.h"
#include "DRN_DrawingArea.h"

// This is a static function

void
DRN_ValueDialog::getValue(
  Widget w, double &value, double minValue, double  maxValue, 
  double scaling, short int dec, char * title, char *label)
{
   static DRN_ValueDialog * dialog = NULL;
   if (dialog != NULL) delete dialog;

   dialog = new DRN_ValueDialog(
     XtParent(w),
     value, minValue, maxValue, scaling, dec, title, label);

   dialog->read();
}


void
DRN_ValueDialog::read()
{
   XtManageChild(_baseWidget);
}


DRN_ValueDialog::DRN_ValueDialog(
  Widget parent, double &value,
  double minValue, double  maxValue,
  double scaling, short int decimals,
  char *title, char *label)
  : _parent(parent),
    value_(value)
{
   // Create widgets used in this component.

   XmString str = XmStringCreateSimple(title);

   Arg al[2];

   XtSetArg(al[0], XmNdialogStyle, XmDIALOG_FULL_APPLICATION_MODAL);
   XtSetArg(al[1], XmNdialogTitle, str);

   _baseWidget = XmCreateFormDialog(_parent, "ValueDialog", al, 2);

   XmStringFree(str);
   
   scaling_ = scaling * pow(10.0, decimals);

   int lower = Round(minValue * scaling_);
   int upper = Round(maxValue * scaling_);

   str = XmStringCreateSimple(label);

   _valueScale = XtVaCreateManagedWidget (
      "value",  xmScaleWidgetClass,
      _baseWidget, 
      XmNtitleString, str,
      XmNminimum, lower,
      XmNmaximum, upper,
      XmNvalue, Round(scaling_ * value_),
      XmNdecimalPoints, decimals,
      XmNorientation, XmHORIZONTAL, 
      XmNshowValue, True, 
      XmNscaleWidth, 300, 
      XmNscaleHeight, 20, 
      XmNtopAttachment, XmATTACH_FORM,
      XmNleftAttachment, XmATTACH_FORM, 
      XmNrightAttachment, XmATTACH_FORM, 
      XmNtopOffset, 10, 
      XmNleftOffset, 10, 
      XmNrightOffset, 10,
      (XtPointer) NULL ); 

   XmStringFree(str);

   XtAddCallback ( _valueScale,
		   XmNvalueChangedCallback,
		   &DRN_ValueDialog::valueChangedCallback,
		   (XtPointer) this ); 

   
   XtAddCallback ( _valueScale,
		   XmNdragCallback,
		   &DRN_ValueDialog::valueDragCallback,
		   (XtPointer) this );

   _okButton = XtVaCreateManagedWidget (
      "Ok",  xmPushButtonWidgetClass,
      _baseWidget, 
      XmNtopAttachment, XmATTACH_WIDGET,
      XmNtopWidget, _valueScale,
      XmNleftAttachment, XmATTACH_FORM, 
      XmNrightAttachment, XmATTACH_FORM, 
      XmNbottomAttachment, XmATTACH_FORM,
      XmNtopOffset, 10, 
      XmNbottomOffset, 10,
      XmNleftOffset, 130,
      XmNrightOffset, 130,
      XmNheight, 30, 
      (XtPointer) NULL ); 

   XtAddCallback ( _okButton,
		   XmNactivateCallback,
		   &DRN_ValueDialog::valueOkCallback,
		   (XtPointer) this ); 
}


DRN_ValueDialog::~DRN_ValueDialog()
{
   XtDestroyWidget(XtParent(_baseWidget));
}


// The following functions are static member functions used to
// interface with Motif.

void DRN_ValueDialog::valueChangedCallback (
  Widget    w,
  XtPointer clientData,
  XtPointer callData ) 
{ 
   DRN_ValueDialog* obj = ( DRN_ValueDialog * ) clientData;
   obj->changeValue ( w, callData );
}

void DRN_ValueDialog::valueDragCallback (
  Widget    w,
  XtPointer clientData,
  XtPointer callData ) 
{ 
   DRN_ValueDialog* obj = ( DRN_ValueDialog * ) clientData;
   obj->changeValue ( w, callData );
}


void DRN_ValueDialog::valueOkCallback (
  Widget    w,
  XtPointer clientData,
  XtPointer callData ) 
{ 
   DRN_ValueDialog* obj = ( DRN_ValueDialog * ) clientData;
   obj->valueOk ( w, callData );
}


// Set the value

void DRN_ValueDialog::changeValue ( Widget, XtPointer callData )
{
   XmScaleCallbackStruct *cbs = (XmScaleCallbackStruct*) callData;
   double x = cbs->value / scaling_;
   value_ = x;
}


void DRN_ValueDialog::valueOk ( Widget, XtPointer )
{
   theDrawingArea->setState(DRN_DrawingArea::REDRAW);
}
