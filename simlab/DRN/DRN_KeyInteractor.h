//-*-c++-*------------------------------------------------------------
// DRN_KeyInteractor.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef DRN_KEYINTERACTOR_HEADER
#define DRN_KEYINTERACTOR_HEADER

#include <Xmw/XmwKeyInteractor.h>

class DRN_DrawingArea;

class DRN_KeyInteractor : public XmwKeyInteractor
{
   public:

      DRN_KeyInteractor(XmwDrawingArea* drawing_area) :
		 XmwKeyInteractor(drawing_area) {
	  }

      // allows you to set a keyboard input area (drawing_area) and a
      // view window on which the tranformation functions will be
      // called

      virtual ~DRN_KeyInteractor() { }

      virtual Boolean keyPress(KeySym keysym);
	  virtual Boolean textInput(KeySym keysym);

      // Returns a scale corresponse to the modifier

      virtual double scale(double a_little, double regular, double a_lot);

	  inline DRN_DrawingArea * drawingArea() {
		 return (DRN_DrawingArea *) drawing_area_;
	  }

	  void GotoViewMarker(KeySym keysym) ;
};

#endif

