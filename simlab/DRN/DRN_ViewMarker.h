//-*-c++-*------------------------------------------------------------
// DRN_ViewMarker.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef DRN_VIEWMARKER_HEADER
#define DRN_VIEWMARKER_HEADER

#include <string>
#include <vector>
#include <iostream>
//using namespace std;

class DRN_DrawingArea ;
class GenericVariable ;
class ArrayElement ;

class DRN_ViewMarker {
public:
	static void LoadViewMarkers(GenericVariable& gv) ;
	static int nMarkers() { return ViewMarkers_.size() ; }
	static DRN_ViewMarker* Marker(int i) { return ViewMarkers_[i] ; }
	static int AddMarker(DRN_ViewMarker* marker) ;
	static bool DeleteMarker(DRN_ViewMarker* marker) ;
	static void DeleteMarkers() ;
	static void SaveMarkers(std::ostream& os) ;
	static void GotoMarker(int i) ;

	DRN_ViewMarker() ;
	~DRN_ViewMarker() { }
	void init(DRN_DrawingArea *area, const char *label) ;
	void init(ArrayElement* gv) ;

	void save(std::ostream& os) ;
	
protected:

	std::string label_ ;

	// Drawing area

	double xpos_ ;
	double ypos_ ;
	double scale_ ;
	double angle_ ;

	// Symbols

	int tools_ ;
	int viewType_ ;

	int segmentColorCode_;
	   
	int sensorTypes_;
	int sensorLabel_;
	int sensorColorCode_;
	   
	int signalTypes_;

	// Gargets

	bool isMapFeaturesOn_;
	bool isLegendOn_;

private:
	typedef std::vector<DRN_ViewMarker*> StoreType ;
	static StoreType ViewMarkers_ ;

} ;

#endif
