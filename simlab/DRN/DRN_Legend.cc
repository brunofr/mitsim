//-*-c++-*------------------------------------------------------------
// NAME: Drawable Road Network
// AUTH: Qi Yang
// FILE: DRN_Legend.C
// DATE: Tue Feb  6 10:25:33 1996
//--------------------------------------------------------------------

#include <sstream>
#include <cstdio>

#include <Tools/Normalizer.h>
#include <Tools/ToolKit.h>

#include <GRN/Parameter.h>
#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>

#include <Xmt/Xmt.h>

#include "DRN_Interface.h"
#include "DRN_Legend.h"
#include "DRN_Menu.h"
#include "DRN_DrawingArea.h"
#include "DRN_Segment.h"
#include "DRN_Symbols.h"
using namespace std;

// Create a legent in the given drawing area

void DRN_Legend::create(DRN_DrawingArea * parent)
{
  pos_.set(parent->width() * 5 / 6, parent->height() * 2 / 3); 
}

      
// Set the legend at a given position

void DRN_Legend::setPosition(XmwPoint& p)
{
  pos_ = p;
}


// Draw the legend at a given position

void DRN_Legend::draw(int mode)
{
  XFontStruct * current_font = theDrawingArea->fontAttr();
  XFontStruct * font = theDrawingArea->setFont(1);

  // Drawing the context of the legend.
   
  offset_ = 0;

  if (mode & 1) drawSegmentSymbols();
  if (mode & 2) drawSensorSymbols();

  draw_more();

  // Restore the default font

  if (current_font != font) {
	theDrawingArea->setFont(current_font);
  }
}


void DRN_Legend::drawLineSymbol(Pixel clr, char *label, int offset,
								int width)
{
  const int symbol_size = 20;
  const int col_space = 15;

  int sx = pos_.x();
  int ex = sx + width + (label ? 5 : 0);
  int y = pos_.y() + offset_;

  XmwPoint sp(sx, y), ep(ex, y);

  theDrawingArea->foreground(clr);
  theDrawingArea->XmwDrawingArea::drawLine(sp, ep);

  if (label != NULL) {
	int wx = sx + symbol_size + col_space;
	XmwPoint wp(wx, y);
	theDrawingArea->foreground(theColorTable->white());
	theDrawingArea->drawLString(label, wp);
  }

  if (offset <= 0) {		// not specified
	offset_ += theDrawingArea->fontHeight();
  } else {
	offset_ += offset;
  }
}


void DRN_Legend::drawShortLineSymbol(Pixel clr, char *label, int offset)
{
  drawLineSymbol(clr, label, offset, 10);
}


void DRN_Legend::drawSquareBoxSymbol(
									 Pixel clr, char *label,
									 int offset)
{
  int h = Round(theDrawingArea->meter2Pixels() * 1.8);
  h = Min(h, 6); 
  drawBoxSymbol(clr, label, h, h, offset);
}

void DRN_Legend::drawLongBoxSymbol(
								   Pixel clr, char *label,
								   int offset)
{
  int h = Round(theDrawingArea->meter2Pixels() * 1.8);
  h = Min(h, 5); 
  int w = h * 3;
  drawBoxSymbol(clr, label, w, h, offset);
}


void DRN_Legend::drawBoxSymbol(Pixel clr, char *label,
							   int w, int h,
							   int offset)
{
  const int symbol_size = 20;
  const int col_space = 15;

  if (offset <= 0) {		// not specified
	offset_ += theDrawingArea->fontHeight();
  } else {
	offset_ = offset;
  }

  int sx = pos_.x();
  int wx = sx + symbol_size + col_space;
  int y = pos_.y() + offset_;

  XmwPoint sp(sx, y - h/2);
  theDrawingArea->foreground(clr);
  theDrawingArea->XmwDrawingArea::drawRect(sp, w, h);

  XmwPoint wp(wx, y);
  theDrawingArea->foreground(theColorTable->white());
  theDrawingArea->drawLString(label, wp);
}


void DRN_Legend::drawLineSymbols(
								 char *title,
								 Normalizer *normalizer,
								 double scaling,
								 int num_labels)
{
  XmwPoint p(pos_.x(), pos_.y() + offset_);
  theDrawingArea->drawLString(title, p);
  int tick_space = 3 * theDrawingArea->fontHeight() / 2;

  offset_ += tick_space;
  tick_space = (tick_space > 5) ? (tick_space / 5) : 1;

  double start = normalizer->minimum();
  double end = normalizer->maximum();
  double step, value;

  int num_ticks = num_labels * 5;

  if (normalizer->isLogarithm()) {
	start = Max(start, 1.0);
	step = pow(end / start, 1.0 / num_ticks);
  } else {
	step = (end - start) / num_ticks;
  }

  Pixel clr;

  for (int i = 0; i <= num_ticks; i ++) {

	if (normalizer->isLogarithm()) {
	  value = start * pow(step, i);
	} else {
	  value = start + i * step;
	}

	clr = theColorTable->color(
							   normalizer->normalize(value), theDrawingArea->intensity());
      
	if (i % 5 == 0) {
	  char label[64];
	  sprintf(label, "%.*lf", normalizer->decimals(),
			  value / scaling);
	  drawLineSymbol(clr, label, tick_space);
	} else {
	  drawLineSymbol(clr, NULL, tick_space);
	}
  }
}


void DRN_Legend::drawSegmentSymbols()
{ 
  if (theDrnSymbols()->viewType().value() == DRN_Symbols::VIEW_ODFLOW) {
	return;
  }

  theDrawingArea->foreground(theColorTable->yellow());

  switch (theDrnSymbols()->segmentColorCode().value()) {
  case DRN_Symbols::SEGMENT_LINKTYPE:
	{
	  theDrawingArea->drawLString("Road Type", pos_);
	  offset_ += theDrawingArea->fontHeight();
	  drawLineSymbol(theColorTable->black(),      "Tunnel");
	  drawLineSymbol(theColorTable->cyan(),       "  Freeway");
	  drawLineSymbol(theColorTable->lightGreen(), "  Ramp");
	  drawLineSymbol(theColorTable->green(),      "  Urban");
	  drawLineSymbol(theColorTable->black(),      "Normal");
	  drawLineSymbol(theColorTable->yellow(),     "  Freeway");
	  drawLineSymbol(theColorTable->red(),        "  Ramp");
	  drawLineSymbol(theColorTable->lightRed(),   "  Urban");
	  break;
	}
  case DRN_Symbols::SEGMENT_DENSITY:
	{
	  drawLineSymbols(Parameter::densityLabel(),
					  theDrawingArea->density(),
					  Parameter::densityFactor());
	  break;
	}
  case DRN_Symbols::SEGMENT_SPEED:  
	{
	  drawLineSymbols(Parameter::speedLabel(),
					  theDrawingArea->speed(),
					  Parameter::speedFactor());
	  break;
	}
  case DRN_Symbols::SEGMENT_FLOW:
	{
	  drawLineSymbols(Parameter::flowLabel(),
					  theDrawingArea->linkFlow(),
					  Parameter::flowFactor());
	  break;
	}
  }
}


void DRN_Legend::drawSensorSymbols()
{
  if (!theDrawingArea->isSensorDrawable()) return;

  theDrawingArea->foreground(theColorTable->yellow());
  offset_ += theDrawingArea->fontHeight();

  ostringstream os;
  os << "Sensor ";
  
  switch (theDrnSymbols()->sensorColorCode().value()) {
  case DRN_Symbols::SENSOR_OCCUPANCY:
	{
	  os << Parameter::occupancyLabel();
      
      {
          const string& temp_str = os.str();
          const char* cchar_temp_str = temp_str.c_str();
          // Undefined behavior if drawLineSymbols changes hacked_char_temp_str!
          char* hacked_char_temp_str = const_cast<char*>(cchar_temp_str);
          drawLineSymbols(hacked_char_temp_str,
                          theDrawingArea->occupancy(),
                          1.0);
      }
      
	  break;
	}
  case DRN_Symbols::SENSOR_SPEED:  
	{
	  os << Parameter::speedLabel();
      {
          const string& temp_str = os.str();
          const char* cchar_temp_str = temp_str.c_str();
          // Undefined behavior if drawLineSymbols changes hacked_char_temp_str!
          char* hacked_char_temp_str = const_cast<char*>(cchar_temp_str);
          drawLineSymbols(hacked_char_temp_str,
                          theDrawingArea->speed(),
                          Parameter::speedFactor());
      }
      
	  break;
	}
  case DRN_Symbols::SENSOR_FLOW:
	{
	  os << Parameter::flowLabel();
      {
          const string& temp_str = os.str();
          const char* cchar_temp_str = temp_str.c_str();
          // Undefined behavior if drawLineSymbols changes hacked_char_temp_str!
          char* hacked_char_temp_str = const_cast<char*>(cchar_temp_str);

          drawLineSymbols(hacked_char_temp_str,
					  theDrawingArea->linkFlow(),
                          Parameter::flowFactor());
      }
      
	  break;
	}
  case DRN_Symbols::SENSOR_COUNT:
	{
	  os << "Counts";
      {
          const string& temp_str = os.str();
          const char* cchar_temp_str = temp_str.c_str();
          // Undefined behavior if drawLineSymbols changes hacked_char_temp_str!
          char* hacked_char_temp_str = const_cast<char*>(cchar_temp_str);

          drawLineSymbols(hacked_char_temp_str,
                          theDrawingArea->linkCount(),
                          1.0);
      }
      
	  break;
	}
  default:
	{
	  os << "State";
      {
          const string& temp_str = os.str();
          const char* cchar_temp_str = temp_str.c_str();
          // Undefined behavior if drawLString changes hacked_char_temp_str!
          char* hacked_char_temp_str = const_cast<char*>(cchar_temp_str);
          
          XmwPoint p(pos_.x(), pos_.y() + offset_);
          theDrawingArea->drawLString(hacked_char_temp_str, p);
      
          drawLineSymbol(theColorTable->red(), "Activated");
          drawLineSymbol(theColorTable->cyan(), "Idle");
          drawLineSymbol(theColorTable->lightGray(), "Failed");
      }

	  break;
	}
  }

}


