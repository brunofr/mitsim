//-*-c++-*------------------------------------------------------------
// NAME: Drawable Road Network
// AUTH: Qi Yang
// FILE: DRN_ODCell.C
// DATE: Sun Feb 11 14:43:58 1996
//--------------------------------------------------------------------

#include <Tools/Normalizer.h>
#include <Tools/Math.h>

#include "DRN_ODCell.h"
#include "DRN_DrawingArea.h"
#include "DRN_Node.h"

const double MIN_WIDTH = 0.0;
const double MAX_WIDTH = 10.0;

// Represent the flow by color using logarithm scale.

Pixel
DRN_ODCell::calcColor(DRN_DrawingArea * area, int *width)
{
   double r = area->odFlow()->normalize(rate());
   if (width) {
      *width = Round(r * (MAX_WIDTH - MIN_WIDTH));
   }
   return theColorTable->color(r);
}


// Draw the flow for a OD pair with a line

int
DRN_ODCell::draw(DRN_DrawingArea * area)
{
   int width;
   Pixel clr = calcColor(area, &width);

   area->foreground(clr);
   area->lineWidth(width);

   int vis = area->drawLine(
      ((DRN_Node *)oriNode())->loc(),
      ((DRN_Node *)desNode())->loc());

   area->lineWidth(0);
   return vis;
}
