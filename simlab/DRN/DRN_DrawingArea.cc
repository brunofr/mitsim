//-*-c++-*------------------------------------------------------------
// DRN_DrawingArea.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class implementation for the class
// XmwDrawingArea
//--------------------------------------------------------------------

#include <iostream>
#include <sstream>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cassert>
using namespace std;

#include <Tools/Math.h>
#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>
#include <Tools/GenericVariable.h>
#include <Tools/Normalizer.h>

#include <X11/cursorfont.h>

#include <Xmw/XmwFont.h>
#include <Xmw/XmwColor.h>

#include <GRN/WcsPoint.h>
#include <GRN/Parameter.h>
#include <GRN/RN_PathTable.h>
#include <GRN/RN_Path.h>
#include <GRN/RN_Link.h>
#include <GRN/RN_DynamicRoute.h>
 
#include "DrawableRoadNetwork.h"
#include "DRN_Interface.h"
#include "DRN_Node.h"
#include "DRN_Segment.h"
#include "DRN_Lane.h"
#include "DRN_Menu.h"
#include "DRN_DrawingArea.h"
#include "DRN_Modeline.h"
#include "DRN_KeyInteractor.h"
#include "DRN_MouseInteractor.h"
#include "DRN_Legend.h"
#include "DRN_Logo.h"
#include "DRN_RangeDialog.h"
#include "DRN_ODTable.h"
#include "DRN_ODCell.h"
#include "DRN_MapFeature.h"
#include "DRN_Symbols.h"

#include "DRN_Sensor.h"
#include "DRN_Signal.h"
#include "DRN_FindDialog.h"
#include "DRN_PathDialog.h"

// Default min and max scale in pixels per meter

const double MIN_SCALE =  0.02;
const double MAX_SCALE = 10.00;

// These are initial values for calculating color indices

const double MIN_DENSITY   =  6.214; // vehicles/km = 10/lane/mile
const double MAX_DENSITY   =  43.51; // vehicles/km = 70/lane/mile
const double MIN_SPEED     =  2.235; // meter/sec = ~5 mph
const double MAX_SPEED     =  35.76; // meter/sec = ~80 mph
const double MIN_ODFLOW    =    0.0; // vehicles/hour
const double MAX_ODFLOW    = 7200.0; // vehicles/hour
const double MIN_LINKFLOW  =    0.0; // vehicles/hour/lane
const double MAX_LINKFLOW  = 1800.0; // vehicles/hour/lane
const double MIN_LINKCOUNT =    0.0; // vehicles
const double MAX_LINKCOUNT = 1800.0; // vehicles
const double MIN_OCCUPANCY =    0.0; // in percent of time 
const double MAX_OCCUPANCY =   30.0; // in percent of time 

Normalizer * DRN_DrawingArea::density_   = new Normalizer;
Normalizer * DRN_DrawingArea::speed_     = new Normalizer;
Normalizer * DRN_DrawingArea::odFlow_    = new LogNormalizer;
Normalizer * DRN_DrawingArea::linkFlow_  = new Normalizer;
Normalizer * DRN_DrawingArea::linkCount_ = new Normalizer;
Normalizer * DRN_DrawingArea::occupancy_ = new Normalizer;

DRN_DrawingArea * theDrawingArea = NULL;

int
DRN_DrawingArea::loadScalableFontSizes(GenericVariable& gv)
{
  theNumFonts = gv.nElements();
  theFontSizes = new int [theNumFonts];
  for (int i = 0; i < theNumFonts; i ++) {
	theFontSizes[i] = gv.element(i);
  }
  return 0;
}


DRN_DrawingArea::DRN_DrawingArea(Widget w)
  : XmwDrawingArea("*drawingArea", w),
	gridSize_(100.0),
	state_(NOT_READY | REDRAW)
{
  theDrawingArea = this;

  XtAddEventHandler(widget_,
					EnterWindowMask | LeaveWindowMask,
					False,
					&DRN_DrawingArea::showCursorEH,
					(XtPointer) NULL );
}


void DRN_DrawingArea::initialize()
{
  XmwDrawingArea::initialize();
  key_interactor_ = newKeyInteractor();
  mouse_interactor_ = newMouseInteractor();
  legend_ = newLegend();
  reset();
}


DRN_KeyInteractor *
DRN_DrawingArea::newKeyInteractor()
{
  return new DRN_KeyInteractor( this );
}


DRN_MouseInteractor *
DRN_DrawingArea::newMouseInteractor()
{
  return new DRN_MouseInteractor( this );
}


DRN_Legend *
DRN_DrawingArea::newLegend()
{
  return new DRN_Legend(this);
}


void DRN_DrawingArea::GotoMarker(double xpos, double ypos,
								 double scale, double angle)
{
	double x = theWorldSpace->width()  * xpos;
	double y = theWorldSpace->height() * ypos;
	center_.set(x, y);
	meter2Pixels_ = scale;
	rotate(angle);
}

// Callbacks: The following functions will be called by keyboard and
// mouse input, as well as by the menu bar.

void DRN_DrawingArea::zoom(KeySym keysym)
{
  double scale = key_interactor_->scale(1.05, 1.25, 1.50);

  switch (keysym)
	{
	case XK_i:
	case XK_I:
	  {
		scale = meter2Pixels_ * scale;
		break;
	  }
	case XK_o:
	case XK_O:
	  {
		scale = meter2Pixels_ / scale;
		break;
	  }
	default:
	  {
		return;
	  }
	}

  DRN_DrawingArea::zoom(scale);
}


void DRN_DrawingArea::rotate(KeySym keysym)
{
  double delta = key_interactor_->scale(ONE_PI / 32.0, // a little
										ONE_PI / 8.0, // regular
										ONE_PI / 4.0); // a lot

  double alpha;

  switch (keysym)
	{
	case XK_j:
	case XK_J:
	case XK_backslash:
	  {
		alpha = angle_ + delta;
		break;
	  }
	case XK_k:
	case XK_K:
	case XK_KP_Divide:
	case XK_slash:
	  {
		alpha = angle_ - delta;
		break;
	  }
	default:
	  {
		return;
	  }
	}

  if (alpha < 0) alpha += TWO_PI;
  else if (alpha >= TWO_PI) alpha -= TWO_PI;
  rotate(alpha);
}


//   We must look at the x and y pan individually and then call
//   reCenter()

void DRN_DrawingArea::pan(KeySym keysym)
{
  int mode = 0;
  switch (keysym)
	{
	case XK_l:
	case XK_L:
	case XK_Left:
	case XK_KP_4:
	case XK_KP_Left:
	  {
		mode = 4;
		break;
	  }
	case XK_r:
	case XK_R:
	case XK_Right:
	case XK_KP_6:
	case XK_KP_Right:
	  {
		mode = 6;
		break;
	  }
	case XK_u:
	case XK_U:
	case XK_Up:
	case XK_KP_8:
	case XK_KP_Up:
	  {
		mode = 8;
		break;
	  }
	case XK_d:
	case XK_D:
	case XK_Down:
	case XK_KP_2:
	case XK_KP_Down:
	  {
		mode = 2;
		break;
	  }
	case XK_KP_1:
	case XK_KP_End:
	  {
		mode = 1;
		break;
	  }
	case XK_KP_3:
	case XK_KP_Page_Down:
	  {
		mode = 3;
		break;
	  }
	case XK_KP_7:
	case XK_KP_Home:     
	  {
		mode = 7;
		break;
	  }
	case XK_KP_9:
	case XK_KP_Page_Up:
	  {
		mode = 9;
		break;
	  }
	default:
	  {
		return;
	  }
	}
  if (mode) {
	double s = key_interactor_->scale(0.02, 0.10, 0.25);
	DRN_DrawingArea::pan(mode, s);
  }
}


void DRN_DrawingArea::resize()
{
  if (state(NOT_READY) || !isDrawable()) {
	clear();
	theDrnInterface->drawLogo(this);
  } else {
	redraw();
  }
}


void DRN_DrawingArea::expose()
{
  if (state(NOT_READY) || !isDrawable()) {
	theDrnInterface->drawLogo(this);
  } else {
	setState(REDRAW);
  }
}


void DRN_DrawingArea::redraw()
{
  if (!state(NETWORK_LOADED) || state(VIEW_NONE)) {
	unsetState(REDRAW);
	return;
  }

  if (meter2Pixels_ < min_scale_) {
	// First time this function is called
	if (!theFontTable) {
	  theFontTable = new XmwFont();
	  setFont(theFontTable->fixed());
	}
	setHomePosition();
  }

  // Clean these bits so that an erase operation is not needed when
  // drawing vehicles, sensors, and signals
   
  theDrawableNetwork->unsetDrawingStates(this);

  // XSync(display_, True);
      
  clear();
   
  if (theMapFeatures && theDrnSymbols()->isMapFeaturesOn().value()) {
	theMapFeatures->draw(this);
  }

  if (theDrnSymbols()->isGridOn().value()) drawGrid();

  theDrawableNetwork->drawNetwork(this);

  if (theDrnSymbols()->isLegendOn().value()) legend_->draw();

  // draw the version info at the right corner to track who and when

  if (theDrnSymbols()->isVersionLabelOn().value()) drawVersion();

  theDrnInterface->handlePendingEvents();

  unsetState(REDRAW);
}

void DRN_DrawingArea::draw_misc()
{
  // Clean these bits so that an erase operation is not needed when
  // drawing vehicles, sensors, and signals
   
  theDrawableNetwork->unsetDrawingStates(this);

  clear();
   
  if (theMapFeatures && theDrnSymbols()->isMapFeaturesOn().value()) {
	theMapFeatures->draw(this);
  }

  if (theDrnSymbols()->isGridOn().value()) drawGrid();

  theDrawableNetwork->DrawableRoadNetwork::drawNetwork(this);

  if (theDrnSymbols()->isLegendOn().value()) legend_->draw();

  // draw the version info at the right corner to track who and when

  if (theDrnSymbols()->isVersionLabelOn().value()) drawVersion();

  theDrnInterface->handlePendingEvents();
}


void 
DRN_DrawingArea::drawVersion()
{
  XFontStruct * font = fontAttr(); // save current font
  setFont(1);				// select font 1
   
  XPoint p;

  p.x = width_ - fontHeight_;
  foreground(theColorTable->lightGray());
  p.y = height_ - fontHeight_ - 3;
   
  drawRString(theVersionInfo[1], p);
  p.y -= fontHeight_;
  drawRString(theVersionInfo[0], p);

  setFont(font);			// restore font
}

void 
DRN_DrawingArea::drawGrid()
{
  const double epsilon = 1.e-5;
  double x, y;
  double xstart, ystart, xend, yend;

  x = worldSpace().lowerLeftPoint().x();
  y = worldSpace().lowerLeftPoint().y();

  xstart = Floor (x, gridSize_) - x;
  ystart = Floor (y, gridSize_) - y;

  xend = Ceil (worldSpace().upperRightPoint().x(), gridSize_) - x;
  yend = Ceil (worldSpace().upperRightPoint().y(), gridSize_) - y;

  foreground(theColorTable->darkGray());
  setXorMode();
      
  // Draw vertical lines:

  for (x = xstart; x <= xend + epsilon; x += gridSize_) {
	drawLine(WcsPoint(x, ystart), WcsPoint(x, yend));
  }
      
  // Draw horizontal lines:
   
  for (y = ystart; y <= yend + epsilon; y += gridSize_) {
	drawLine(WcsPoint(xstart, y), WcsPoint(xend, y));	
  }

}


void
DRN_DrawingArea::lineObjectsColorRange()
{
  if (!isDrawable()) return;

  if (theDrnSymbols()->viewType().is(DRN_Symbols::VIEW_ODFLOW)) {
	if (theODTable) {
	  DRN_RangeDialog::getRange(
								widget(), odFlow_, Parameter::flowFactor(),
								Parameter::flowLabel());
	  redraw();
	}
  } else {
	switch (theDrnSymbols()->segmentColorCode().value()) {
	case DRN_Symbols::SEGMENT_DENSITY:
	  {
		DRN_RangeDialog::getRange(widget(), density_,
								  1.0 / Parameter::densityFactor(),
								  Parameter::densityLabel());
		break;
	  }
	case DRN_Symbols::SEGMENT_SPEED:
	  {
		DRN_RangeDialog::getRange(widget(), speed_,
								  1.0 / Parameter::speedFactor(),
								  Parameter::speedLabel());
		break;
	  }
	case DRN_Symbols::SEGMENT_FLOW:
	  {
		DRN_RangeDialog::getRange(widget(), linkFlow_,
								  1.0 / Parameter::flowFactor(),
								  Parameter::flowLabel());
		break;
	  }
	}
	theDrawableNetwork->drawRoads(this);
  }
}


void
DRN_DrawingArea::pointObjectsColorRange()
{
  if (!isDrawable() || !isSensorDrawable()) {
	return;
  }

  switch (theDrnSymbols()->sensorTypes().isOn(DRN_Symbols::SENSOR_AGGREGATE)) {
  case DRN_Symbols::SENSOR_OCCUPANCY:
	{
	  DRN_RangeDialog::getRange(widget(), occupancy_, 1.0,
								Parameter::occupancyLabel());
	  theDrawableNetwork->drawSensors(this);
	  break;
	}
  case DRN_Symbols::SENSOR_SPEED:
	{
	  DRN_RangeDialog::getRange(widget(), speed_,
								1.0 / Parameter::speedFactor(),
								Parameter::speedLabel());
	  theDrawableNetwork->drawSensors(this);	  
	  break;
	}
  case DRN_Symbols::SENSOR_FLOW:
	{
	  DRN_RangeDialog::getRange(widget(), linkFlow_,
								1.0 / Parameter::flowFactor(),
								Parameter::flowLabel());
	  theDrawableNetwork->drawSensors(this);
	  break;
	}
  case DRN_Symbols::SENSOR_COUNT:
	{
	  DRN_RangeDialog::getRange(widget(), linkCount_, 1.0, "Vehicles");
	  theDrawableNetwork->drawSensors(this);
	  break;
	}
  }
}


void
DRN_DrawingArea::reset()
{
  angle_ = 0; 
  meter2Pixels_= 0.0;

  left_ = -DBL_INF;
  top_ = DBL_INF;
  right_ = DBL_INF;
  bottom_ = -DBL_INF;

  min_scale_= MIN_SCALE;
  max_scale_= MAX_SCALE;
  state_ = REDRAW;
  density_->set(MIN_DENSITY, MAX_DENSITY);
  speed_->set(MIN_SPEED, MAX_SPEED, 1);
  odFlow_->set(MIN_ODFLOW, MAX_ODFLOW);
  linkFlow_->set(MIN_LINKFLOW, MAX_LINKFLOW);
  linkCount_->set(MIN_LINKCOUNT, MAX_LINKCOUNT);
  occupancy_->set(MIN_OCCUPANCY, MAX_OCCUPANCY);

  intensity_ = 1.0;
}


// These functions requires XmwColor is in increasing order
// (cold->hot)

Pixel
DRN_DrawingArea::densityColor(double x)
{
  return theColorTable->color(density_->normalize(x), intensity_);
}


Pixel
DRN_DrawingArea::speedColor(double x)
{
  return theColorTable->color(speed_->normalize(x), intensity_);
}


Pixel
DRN_DrawingArea::occupancyColor(double x)
{
  return theColorTable->color(occupancy_->normalize(x));
}


Pixel
DRN_DrawingArea::odFlowColor(double x)
{
  return theColorTable->color(odFlow_->normalize(x));
}

Pixel
DRN_DrawingArea::linkFlowColor(double x)
{
  return theColorTable->color(linkFlow_->normalize(x), intensity_);
}

Pixel
DRN_DrawingArea::linkCountColor(double x)
{
  return theColorTable->color(linkCount_->normalize(x));
}

// Returns the world space

RN_WorldSpace&
DRN_DrawingArea::worldSpace()
{
  return theDrawableNetwork->worldSpace();
}


void DRN_DrawingArea::zoom(double scale)
{
  meter2Pixels(scale);
  redraw();
}

void DRN_DrawingArea::zoomIn(XmwPoint& from, XmwPoint &to)
{
  XmwPoint c((from.x() + to.x()) / 2, (from.y() + to.y()) / 2);
  center_ = window2World(c);
  if (to.x() != from.x() && to.y() != from.y()) {
	double w = fabs(width_  / (to.x() - from.x()) * meter2Pixels_);
	double h = fabs(height_ / (to.y() - from.y()) * meter2Pixels_);
	double scale = Min(w, h);
	meter2Pixels(scale);
  }
  calcScope();
  redraw();
}

void DRN_DrawingArea::zoomIn(XmwPoint& p)
{
  center_ = window2World(p);
  meter2Pixels(1.25 * meter2Pixels_);
  calcScope();
  redraw();
}

void DRN_DrawingArea::zoomOut(XmwPoint& p)
{
  center_ = window2World(p);
  meter2Pixels(0.75 * meter2Pixels_);
  calcScope();
  redraw();
}

void DRN_DrawingArea::rotate(double zero_angle)
{
  angle_ = zero_angle;
  sin_ = sin(angle_);
  cos_ = cos(angle_);
  calcScope();

  redraw();
}

void DRN_DrawingArea::rotate(XmwPoint &from, XmwPoint &to)
{
  WcsPoint p1 = window2World(from);
  WcsPoint p2 = window2World(to);
  double a1 = center_.angle(p1);
  double a2 = center_.angle(p2);
  double alpha = angle_ + a2 - a1;
  if (alpha < 0) alpha += TWO_PI;
  else if (alpha >= TWO_PI) alpha -= TWO_PI;

  angle_ = alpha;
  sin_ = sin(angle_);
  cos_ = cos(angle_);
  calcScope();

  clear();
  if (theDrnSymbols()->isGridOn().value()) drawGrid();
  theDrawableNetwork->drawNetwork(this);
}


void DRN_DrawingArea::pan(int mode, double amount)
{
  static float direction[] = {
	1.25, 1.5, 1.75, 1.0, 0.0, 0.0, 0.75, 0.5, 0.25 
  };
  double beta = direction[mode-1] * ONE_PI;
  double x = width_  * (0.5 + amount * cos(beta));
  double y = height_ * (0.5 - amount * sin(beta));

  reCenter(XmwPoint(Round(x), Round(y)));
}

void DRN_DrawingArea::pan(XmwPoint &from, XmwPoint &to)
{
  double dx = (to.x() - from.x()) / meter2Pixels_;
  double dy = (to.y() - from.y()) / meter2Pixels_;
  double x = center_.x() - (dx * cos_ - dy * sin_);
  double y = center_.y() + (dy * cos_ + dx * sin_);
  center_.set(x,y);
  calcScope();

  theDrawableNetwork->unsetDrawingStates(this);
  clear();
  if (theDrnSymbols()->isGridOn().value()) drawGrid();
  theDrawableNetwork->drawRoads(this);
}

void DRN_DrawingArea::set_center(const WcsPoint & p)
{
  center_ = p;
  calcScope();
}  

void DRN_DrawingArea::center(const WcsPoint & p)
{
  set_center(p);
  redraw();
}


void DRN_DrawingArea::reCenter(const XmwPoint& pressed) 
{
  WcsPoint p(window2World(pressed));
  set_center(p);
  redraw();
}

void DRN_DrawingArea::reCenter(const WcsPoint& p) 
{
  set_center(p);
  redraw();
}

Boolean DRN_DrawingArea::queryCursorPoint(XmwPoint &point)
{
  Window root_window, child_window;
  int root_x, root_y;
  int window_x, window_y;
  unsigned int mask;
  
  if (XQueryPointer(display_, window_,
					&root_window, &child_window,
					&root_x, &root_y,
					&window_x, &window_y, &mask) != False ) {
	point.set(window_x, window_y);
	return True;
  } else {
	return False;
  }
}


void DRN_DrawingArea::annotate(const WcsPoint & point, const char *label,
							   int annotation, int required)
{
  if (required && !isInScope(point)) {

	// the annotation has to be shown and it is not in current view

	center_ = point;
	calcScope();
	redraw();
  }

  if (annotation & DRN_FindDialog::ANNOTATION_SYMBOL) {
	foreground(theColorTable->cyan());

	double size = fontHeight_/meter2Pixels_;
	size = Max(size, 0.5 * LANE_WIDTH);

	drawDiamond(point, HALF_PI, size);
  }
  if (label != NULL &&
	  (annotation & DRN_FindDialog::ANNOTATION_LABEL)) {
	foreground(theColorTable->yellow());
	draw(point, label);
  }
}

// Center and draw an OD pair

void DRN_DrawingArea::annotateODPair(DRN_Node *ori, DRN_Node *des,
									 const char *label, int annotation)
{
  int required;
  if (isInScope(ori->loc()) || isInScope(des->loc())) {
	required = 0;
  } else {
	required = 1;
  }

  // Center point between ori and des

  WcsPoint p(ori->loc(), des->loc(), 0.5);
  annotate(p, label, annotation, required);

  // Draw a line

  if (annotation & DRN_FindDialog::ANNOTATION_SYMBOL) {
	foreground(theColorTable->lightRed());
	drawLine(ori->loc(), des->loc());

	double size = fontHeight_/meter2Pixels_;
	size = Max(size, 0.5 * LANE_WIDTH);

	foreground(theColorTable->green());
	drawDiamond(ori->loc(), HALF_PI, size);

	foreground(theColorTable->red());
	drawDiamond(des->loc(), 0, size);
  }

  if (annotation & DRN_FindDialog::ANNOTATION_LABEL) {
	char o[12], d[12];
	sprintf(o, "%d", ori->code());
	sprintf(d, "%d", des->code());
	foreground(theColorTable->cyan());
	draw(ori->loc(), o);

	foreground(theColorTable->magenta());
	draw(des->loc(), d);
  }
}


// Highlight a path by drawing each segment with thick line

void DRN_DrawingArea::annotationPath(RN_Path *path, int annotation)
{
  int register i, n = path->nLinks();
  RN_Link *pl;
  RN_Segment *ps;
  DRN_Segment *dps;
   
  float w = LANE_WIDTH * meter2Pixels_;
  if (w < 4.0) w = 4.0;
  else if (w > 10.0) w = 10.0;
  lineWidth((int) w);
  setCopyMode();

  for (i = 0; i < n; i ++) {
	pl = path->link(i);
	ps = pl->startSegment();
	while (ps) {
	  dps = (DRN_Segment *) ps;
	  dps->highlight(this, theColorTable->yellow());
	  ps = ps->downstream();
	}
  }
  lineWidth(0);

  pl = path->link(0);
  ps = pl->startSegment();
  dps = (DRN_Segment *) ps;
  WcsPoint p(dps->centerPoint(0.75));

  if (annotation && isInScope(p)) {
	ostringstream os;
	if (annotation & DRN_FindDialog::ANNOTATION_SYMBOL) {
	  os << path->code();
	}
	if (annotation & DRN_FindDialog::ANNOTATION_LABEL) {
	  double ut = path->travelTime(theUnGuidedRoute, 0) / 60.0;
	  os << " (" << Fix(ut, 0.1);
	  if (theGuidedRoute != theUnGuidedRoute) {
		double gt = path->travelTime(theGuidedRoute, 0) / 60.0;
		if (Abs(ut - gt) > 0.1) {
		  os << " / " << Fix(gt, 0.1);
		}
	  }
	  os << ")";
	}
	os << null;
	foreground(theColorTable->lightRed());
	draw(p, os.str().c_str());

  }
}


void DRN_DrawingArea::locate(int type, const char *code, int annotation)
{
  static const char *labels[] = {
	"node",
	"link",
	"segment",
	"lane",
	"sensor",
	"signal",
	"od",
	"path",
	"object"
  };
  short int i = 8;

  switch (type) {
  case DRN_FindDialog::FIND_NODE:
	{
	  // Center to the node

	  DRN_Node *obj = (DRN_Node *)
		theDrawableNetwork->findNode(atoi(code));
	  if (obj) {
		annotate(obj->loc(), code, annotation);
		return;
	  }
	  i = 0;
	  break;
	}
  case DRN_FindDialog::FIND_LINK:
	{
	  // Center to the beginning of the link

	  RN_Link *obj = (RN_Link *) theDrawableNetwork->findLink(atoi(code));
	  if (obj) {
		DRN_Segment *dps = (DRN_Segment *) obj->startSegment();
		annotate(dps->centerPoint(), code, annotation);
		setXorMode();
		float wd = LANE_WIDTH * meter2Pixels();
		if (wd < 4.0) wd = 4.0;
		else if (wd > 10.0) wd = 10.0;
		lineWidth((int) wd);
		while (dps) {
		  dps->highlight(this, theColorTable->yellow());
		  dps = (DRN_Segment *) dps->downstream();
		}
		lineWidth(0);

		return;
	  }
	  i = 1;
	  break;
	}
  case DRN_FindDialog::FIND_SEGMENT:
	{
	  // Center to the beginning of the segment

	  DRN_Segment *obj = (DRN_Segment *)
		theDrawableNetwork->findSegment(atoi(code));
	  if (obj) {

		annotate(obj->centerPoint(0.5), code, annotation);

		setXorMode();
		float wd = LANE_WIDTH * meter2Pixels();
		if (wd < 4.0) wd = 4.0;
		else if (wd > 10.0) wd = 10.0;
		lineWidth((int) wd);
		obj->highlight(this, theColorTable->lightRed());
		lineWidth(0);

		return;
	  }
	  i = 2;
	  break;
	}
  case DRN_FindDialog::FIND_LANE:
	{
	  DRN_Lane *obj = (DRN_Lane *)
		theDrawableNetwork->findLane(atoi(code));
	  if (obj) {
		annotate(obj->centerPoint(0.5), code, annotation);
		setXorMode();
		float wd = 0.5 * LANE_WIDTH * meter2Pixels();
		if (wd < 2.0) wd = 2.0;
		else if (wd > 5.0) wd = 5.0;
		lineWidth((int) wd);
		obj->highlight(this, theColorTable->orange());
		lineWidth(0);

		return;
	  }
	  i= 3;
	  break;
	}
  case DRN_FindDialog::FIND_SENSOR:
	{
	  RN_Sensor *obj = theDrawableNetwork->findSensor(atoi(code));
	  if (obj) {
		annotate(obj->center(), code, annotation);
		return;
	  }
	  i = 4;
	  break;
	}
  case DRN_FindDialog::FIND_SIGNAL:
	{
	  RN_Signal *obj = theDrawableNetwork->findSignal(atoi(code));
	  if (obj) {
		annotate(obj->center(), code, annotation);
		return;
	  }
	  i = 5;
	  break;
	}
  case DRN_FindDialog::FIND_OD:
	{
	  // Center to the origin and draw a line connecting to
	  // destination
 
	  int ori, des;
	  if (GetPair(code, &ori, &des) == 0) {
		DRN_Node *obj_o = (DRN_Node *) theDrawableNetwork->findNode(ori);
		DRN_Node *obj_d = (DRN_Node *) theDrawableNetwork->findNode(des);
		if (obj_o != NULL && obj_d != NULL) {
		  annotateODPair(obj_o, obj_d, code, annotation);
		  return;
		}
	  }
	  i = 6;
	  break;
	}
  case DRN_FindDialog::FIND_PATH:
	{
	  // Center to the start node and draw a line connecting to the
	  // end node

	  if (thePathTable) {
		RN_Path *obj = thePathTable->findPath(atoi(code));
		if (obj) {
		  annotationPath(obj, annotation);
		  return;
		}
	  }
	  i = 7;
	  break;
	}
  default:
	{
	  return;
	}
  }

  const char *msg;
  msg = Str("No %s with code %d exists.", labels[i],  code);
  theDrnInterface->msgShow(msg, MSG_STAY_TIME);
}


void DRN_DrawingArea::reCenterNoRedraw(const XmwPoint& pressed) 
{
  WcsPoint p(window2World(pressed));
  set_center(p);
}

//----------------------------------------------------------------
// Ultimately this ought to be made a function of the size of the
// world space being represented.
//----------------------------------------------------------------


double DRN_DrawingArea::meter2Pixels(double scale)
{
  if (scale < min_scale_) {
	meter2Pixels_ = min_scale_;
	const char *msg = XmtLocalize2(widget_, "No more zoom in.",
								   "prompt", "noZoomIn");
	theDrnInterface->msgShow(msg, MSG_STAY_TIME);
  } else if (scale > max_scale_) {
	meter2Pixels_ = max_scale_;
	const char *msg = XmtLocalize2(widget_, "No more zoom out.",
								   "prompt", "noZoomOut");
	theDrnInterface->msgShow(msg, MSG_STAY_TIME);
  } else {
	meter2Pixels_ = scale;
  }

  // SCALE THE FONT

  // Find relation between meter2Pixels_ and font size code

  float step = (theFontTable->nFonts() - 1.0) / (max_scale_ - min_scale_);

  float r = (meter2Pixels_ - min_scale_) * step + 0.5;
  XFontStruct *current_font = fontAttr();
  XFontStruct *font = setFont((int) r);
  if (current_font != font) {	// font changed
	DRN_Node::radius(XTextWidth(font, "M", 1)+2);
  }
   
  calcResolution();
  calcScope();

  return meter2Pixels_;
}


void DRN_DrawingArea::calcScope()
{
  if (meter2Pixels_ > 1.0E-8) {
 	double r = 0.5 * sqrt(width_ * width_ + height_ * height_) / meter2Pixels_;
	double epsilon = 30.0 / meter2Pixels_;
	left_   = center_.x() - r;
	top_    = center_.y() + r;
	right_  = center_.x() + r;
	bottom_ = center_.y() - r;
	scopeEpsilon_ = r - 0.5 * Min(width_, height_) / meter2Pixels_;
	scopeEpsilon_ = Max(epsilon, scopeEpsilon_);
  } else {
	left_   = -DBL_INF;
	top_    =  DBL_INF;
	right_  =  DBL_INF;
	bottom_ = -DBL_INF;
  }
}


int DRN_DrawingArea::isInScope(const WcsPoint &p)
{
  if (p.x() < left_ || p.x() > right_ ||
	  p.y() < bottom_ || p.y() > top_) {
	return 0;
  } else {
	return 1;
  }
}

void DRN_DrawingArea::keepInScope(const WcsPoint &p)
{
  bool out = false;
  double x, y;
  if (p.x() < left_ + scopeEpsilon_) {
	x = 2.0 * (left_ + scopeEpsilon_) - center_.x();
	out = true;
  } else if (p.x() + scopeEpsilon_ > right_) {
	x = 2.0 * (right_ - scopeEpsilon_) - center_.x();
	out = true;
  } else {
	x = center_.x();
  }
  if (p.y() < bottom_ + scopeEpsilon_) {
	y = 2.0 * (bottom_ + scopeEpsilon_) - center_.y();
	out = true;
  } else if (p.y() + scopeEpsilon_ > top_) {
	y = 2.0 * (top_ - scopeEpsilon_) - center_.y();
	out = true;
  } else {
	y = center_.y();
  }
  if (out) {
	WcsPoint cp(x, y);
	set_center(cp);
	draw_misc();
  }
}

// Translate a world_point to window_point

WcsPoint
DRN_DrawingArea::world2Window(const WcsPoint & world_point) 
{
  // Translation of origin

  double x = world_point.x() - center_.x();
  double y = world_point.y() - center_.y();

  // Rotation

  double e = x * cos_ - y * sin_;
  double n = x * sin_ + y * cos_;

   // Scaling and translation to window coordinates

  x = 0.5 * width_  + e * meter2Pixels_;
  y = 0.5 * height_ - n * meter2Pixels_;

  return WcsPoint(x, y);
}

int
DRN_DrawingArea::world2Window(const WcsPoint & world_point, 
							  XmwPoint & window_point)
{
  WcsPoint p = world2Window(world_point);
  window_point.set(Round(p.x()), Round(p.y()));
  return isInScope(world_point);
}


int
DRN_DrawingArea::world2Window(const WcsPoint & world_point, 
							  XPoint & xpoint)
{
  WcsPoint p = world2Window(world_point);
  xpoint.x = Round(p.x());
  xpoint.y = Round(p.y());
  return isInScope(world_point);
}


WcsPoint
DRN_DrawingArea::window2World(const XmwPoint & window_point)
{
  // Translations

  double e = window_point.x() - 0.5 * width_;
  double n = 0.5 * height_ - window_point.y();

  // Rotatioing

   double x =  e * cos_ + n * sin_;
   double y = -e * sin_ + n * cos_;

   // Scaling

  x /= meter2Pixels_;
  y /= meter2Pixels_;

   // Translation

  WcsPoint p(center_.x() + x, center_.y() + y);

  return p;
}


// Computes the end points of the visible portion of a line from wcsp1
// to wcsp2.  The input points wcsp1 and wcsp2 are world points and
// the output points s1 and s2 are integral points in the current view
// window.  The line extended outside the wiew window is cut off to
// assure safe operation in X.  This function returns 1 if the line is
// visible or 0 otherwise.  If the line is invisible, s1 and s2 are
// not defined.

int
DRN_DrawingArea::cutLine(const WcsPoint& wcsp1, const WcsPoint& wcsp2, 
						 XmwPoint& s1, XmwPoint& s2)
{
  static WcsPoint p1, p2;

  double xalpha, xbeta;
  double yalpha, ybeta;
  double d, alpha, beta;

  p1 = world2Window(wcsp1);
  p2 = world2Window(wcsp2);

  d = p1.x() - p2.x();
  if (d > POINT_EPSILON) {
	xalpha = - p2.x() / d;
	xbeta = (width_ - p2.x()) / d;
  } else if (d < - POINT_EPSILON) {
	xalpha = (width_ - p2.x()) / d;
	xbeta = - p2.x() / d;
  } else {
	if (p1.x() > 0.0 && p1.x() < width_) {
	  xalpha = 0.0;
	  xbeta = 1.0;
	} else {
	  xalpha = 1.0;
	  xbeta = 0.0;
	}
  }
	
  d = p1.y() - p2.y();
  if (d > POINT_EPSILON) {
	yalpha = - p2.y() / d;
	ybeta = (height_ - p2.y()) / d;
  } else if (d < - POINT_EPSILON) {
	yalpha = (height_ - p2.y()) / d;
	ybeta = - p2.y() / d;
  } else {
	if (p1.y() > 0.0 && p1.y() < height_) {
	  yalpha = 0.0;
	  ybeta = 1.0;
	} else {
	  yalpha = 1.0;
	  ybeta = 0.0;
	}
  }

  alpha = Max(xalpha, yalpha);
  beta = Min(xbeta, ybeta);
  alpha = Max(alpha, 0.0);
  beta = Min(beta, 1.0);
	
  if (alpha < beta) {
	s1.set ( Round(beta * p1.x() + (1.0 - beta) * p2.x()), 
			 Round(beta * p1.y() + (1.0 - beta) * p2.y()) );
		
	s2.set ( Round(alpha * p1.x() + (1.0 - alpha) * p2.x()),
			 Round(alpha * p1.y() + (1.0 - alpha) * p2.y()) );
		
	return 1;
  } else {
	return 0;
  }
}


// This function takes into account the current size of the drawing
// area and the world space boundaries of the network and displays the
// full network in the drawing area.
//
// POINT_EPSILON is defined in WcsPoint.h.

// This function takes into account the current size of the drawing
// area and the world space boundaries of the network and displays the
// full network in the drawing area.
//
// POINT_EPSILON is defined in WcsPoint.h.

void DRN_DrawingArea::setHomePosition()
{
  if (!state(NETWORK_LOADED)) return;

  // Calc the scales for width and height of the world space

  double xscale = width_ / worldSpace().width();
  double yscale = height_ / worldSpace().height();
  double scale = Min(xscale, yscale);

  center_ = worldSpace().center();
  angle_  = worldSpace().angle();

  sin_ = sin(angle_);
  cos_ = cos(angle_);

  scale = meter2Pixels(scale);

  // I am not sure if it is the best position to put this line which
  // needs to be executed only once AFTER THIS functions is called.

  unsetState(NOT_READY);
}


int
DRN_DrawingArea::draw(const WcsPoint& p)
{
  static XmwPoint point;
  if (world2Window(p, point)) {
	XmwDrawingArea::drawPoint(point);
	return 1;
  }
  return 0;
}


int
DRN_DrawingArea::draw(const WcsPoint& p1, const WcsPoint& p2)
{
  static XmwPoint win_start, win_end;
  if (cutLine(p1, p2, win_start, win_end)) {
	XmwDrawingArea::drawLine(win_start, win_end);
	return 1;
  }
  return 0;
}


int
DRN_DrawingArea::draw(RN_Arc& arc)
{
  int vis = 0;
  if (arc.isArc()) {		// curve
	WcsPoint prev = arc.startPnt();
	WcsPoint p;

	double shell = ARC_ACCURACY / meter2Pixels_;
	double step = acos(1.0 - shell / arc.radius());
    
	int n = (int) ceil(Abs(arc.arcAngle() / step));
	int m = (int) ceil(arc.length() / shell);
	if (n < 1) {
	  n = 1;
	} else if (n > m) {
	  n = m;
	}
	step = 1.0 / n;
	for (register int i = n; i >= 0; i --) {
	  p = arc.intermediatePoint(i * step);
	  vis |= draw(prev, p);
	  prev = p;
	}
  } else {			// straint line
	vis = draw(arc.startPnt(), arc.endPnt());
  }
  return vis;
}


int
DRN_DrawingArea::draw(const WcsPoint& p, const char *str, int mode)
{
  XPoint xp;
  if (world2Window(p, xp)) {
	if (mode < 0) {
	  drawLString(str, xp);
	} else if (mode > 0) {
	  drawRString(str, xp);
	} else {
	  drawCString(str, xp);
	}
	return 1;
  }
  return 0;
}


int
DRN_DrawingArea::draw(const WcsPoint& p, int code, int mode)
{
  char buffer[256];
  sprintf(buffer, "%d", code);
  return draw(p, buffer, mode);
}


int
DRN_DrawingArea::draw(const WcsPoint& p, float value, int dec, int mode)
{
  char buffer[256];
  sprintf(buffer, "%.*f", dec, value);
  return draw(p, buffer, mode);
}

int
DRN_DrawingArea::drawDiamond(const WcsPoint& p0, double angle)
{
  const double size = 0.25 * LANE_WIDTH;
  return drawDiamond(p0, angle, size);
}

int
DRN_DrawingArea::drawDiamond(const WcsPoint& p0, double angle, double size)
{
  static XPoint p[5];

  WcsPoint p1(p0.bearing(size, angle));
  WcsPoint p2(p0.bearing(size, angle + HALF_PI));
  WcsPoint p3(p0.bearing(size, angle + ONE_PI));
  WcsPoint p4(p0.bearing(size, angle - HALF_PI));

  if (world2Window(p1, p[1]) &&
	  world2Window(p2, p[2]) &&
	  world2Window(p3, p[3]) &&
	  world2Window(p4, p[4])) {
	p[0].x = p[4].x;
	p[0].y = p[4].y;

	drawLines(p, 5);
	return 1;
  }
  return 0;
}


// This function is called whenever the view is scaled (i.e. do we see
// the cars, sensors, and signals at all and at what level of detail
// if we do)

void 
DRN_DrawingArea::calcResolution()
{
  int size = Round(LANE_WIDTH * meter2Pixels_);
   
  if (size > Parameter::resolution(3)) {
	// labels
	resolution_ = HIGHEST_RESOLUTION;
	intensity_ = 0.0;
  } else if (size > Parameter::resolution(2)) {
	// detailed vehicles
	resolution_ = HIGH_RESOLUTION;
	intensity_ = 0.0;
  } else if (size > Parameter::resolution(1)) {
	// lane type, vehicles, sensors and signals
	resolution_ = MEDIUM_RESOLUTION;
	intensity_ = 0.0;
  } else if (size > Parameter::resolution(0)) {
	// lanes
	resolution_ = LOW_RESOLUTION;
	intensity_ = 1.0;
  } else {
	// only links
	resolution_ = LOWEST_RESOLUTION;
	intensity_ = 1.0;
  }

  thickLine_ = Max(1, Round(0.1 * size));
}

WcsPoint DRN_DrawingArea::window2Database(const XmwPoint & point)
{
  WcsPoint wp = window2World(point);
  return worldSpace().databasePoint(wp);
}

void
DRN_DrawingArea::showCoordinates(const XmwPoint &p)
{
  WcsPoint w = window2World(p);
  WcsPoint d = worldSpace().databasePoint(w);
  const char *msg;
  msg = Str("X(%d,%d) World(%lf,%lf) Database(%lf,%lf)",
			p.x(), p.y(),
			w.x(), w.y(),
			d.x(), d.y());
  theDrnInterface->msgSet(msg);
}


void
DRN_DrawingArea::showSegment(const XmwPoint & point, int mode)
{
  static Pixel LABEL_COLOR = theColorTable->color(0.25);
  WcsPoint pressed = window2World(point);
  DRN_Segment *ps = theDrawableNetwork->nearestSegment(pressed);

  if (ps) {
	ostringstream os;
	switch (mode) {
	case 1:
	  {
		const char* label = ps->link()->name();
		if (label) os << label << ends;
		else os << ps->link()->code() << ":" << ps->code() << ends;
		break;
	  }

	case 2:
	default: 
	  {
		os << ps->link()->code() << ":" << ps->code();
		int n = ps->link()->nSegments();
		if (n > 1) {
		  os << " [" << 1 + ps->localIndex()
			 << "/" << n << "]";
		}
		os << " (";
		for (int i = 0; i < ps->nLanes(); i ++) {
		  if (i != 0) os << '|';
		  os << ps->lane(i)->code();  
		}
		os << ")" << ends;
		break;
	  }
	
	}
	foreground(LABEL_COLOR);
	drawCString(os.str().c_str(), point);

  }
}

// These functions check if the object need to be drawn (selected and
// reslution is high enough)

int
DRN_DrawingArea::isSensorDrawable()
{
  return (theDrnSymbols()->viewType().is(DRN_Symbols::VIEW_MICRO) &&
		  theDrnSymbols()->sensorTypes().value() &&
		  resolution_ > LOW_RESOLUTION);
}

int
DRN_DrawingArea::isSignalDrawable()
{
  return (theDrnSymbols()->viewType().is(DRN_Symbols::VIEW_MICRO) &&
		  theDrnSymbols()->signalTypes().value() &&
		  resolution_ > LOW_RESOLUTION);
}

int
DRN_DrawingArea::isTollBoothDrawable()
{
  return (theDrnSymbols()->viewType().is(DRN_Symbols::VIEW_MICRO) &&
		  resolution_ > LOW_RESOLUTION);
}

int
DRN_DrawingArea::isBusStopDrawable()   //margaret
{
  return (theDrnSymbols()->viewType().is(DRN_Symbols::VIEW_MICRO) &&
		  resolution_ > LOW_RESOLUTION);
}

int
DRN_DrawingArea::isLaneMarkDrawable() 
{
  return (theDrnSymbols()->viewType().is(DRN_Symbols::VIEW_MICRO) &&
		  resolution_ > LOWEST_RESOLUTION);
}

int
DRN_DrawingArea::isLaneTypeDrawable()
{
  return (theDrnSymbols()->viewType().is(DRN_Symbols::VIEW_MICRO) &&
		  resolution_ > LOW_RESOLUTION);
}

int
DRN_DrawingArea::isVehicleDrawable()
{
  return (!state(MOUSE_DOWN) &&
		  theDrnSymbols()->viewType().is(DRN_Symbols::VIEW_MICRO) &&
		  resolution_ > LOW_RESOLUTION);
}

int
DRN_DrawingArea::isVehicleStateDrawable()
{
  return (theDrnSymbols()->viewType().is(DRN_Symbols::VIEW_MICRO) &&
		  resolution_ > MEDIUM_RESOLUTION);
}

int
DRN_DrawingArea::drawFilledPolygon(WcsPoint *wpts, int npts, int shape)
{
  int visible = 0 ;
  XPoint* xpts = new XPoint[npts] ;
  for (int i = 0 ; i < npts ; ++ i) {
	visible |= world2Window(wpts[i], xpts[i]) ;
  }
  XmwDrawingArea::drawFilledPolygon(xpts,npts,shape) ;
  delete [] xpts ;
  return visible ; 
}

Pixmap DRN_DrawingArea::draw(const XmwPoint &p, const char *name)
{
  Pixmap pixmap = XmtGetPixmap(theDrnInterface->widget(), NULL, name);
  if (pixmap) {
	XmwDrawingArea::draw(p, pixmap);
  } else {
	cerr << "Error: failed creating pixmap " << name << endl;
  }
  return pixmap;
}


// static

void DRN_DrawingArea::showCursorEH( Widget w,
									XtPointer clientData,
									XEvent *event,
									Boolean *b )
{
  assert(XtIsObject( w ));

  if ( event == NULL ) return;

  Cursor c;

  switch ( event->type ) {
  case EnterNotify:
	{
	  int tool = theDrnSymbols()->tools().value();
	  switch ( tool ) {
	  case DRN_Symbols::TOOL_PAN:
		c = theDrnInterface->getCursor( XC_hand2 );
		break;
	  case DRN_Symbols::TOOL_ZOOMIN:
		c = theDrnInterface->getCursor( XC_sizing );
		break;
	  case DRN_Symbols::TOOL_ZOOMOUT:
		c = theDrnInterface->getCursor( XC_dotbox );
		break;
	  case DRN_Symbols::TOOL_CENTER:
		c = theDrnInterface->getCursor( XC_cross );
		break;
	  case DRN_Symbols::TOOL_ROTATE:
		c = theDrnInterface->getCursor( XC_exchange );
		break;
	  case DRN_Symbols::TOOL_QUERY:
		c = theDrnInterface->getCursor( XC_question_arrow );
		break;
	  case DRN_Symbols::TOOL_LEGEND:
		c = theDrnInterface->getCursor( XC_tcross );
		break;
	  case DRN_Symbols::TOOL_DRAW:
		c = theDrnInterface->getCursor( XC_pencil );
		break;
	  case DRN_Symbols::TOOL_TEXT:
		c = theDrnInterface->getCursor( XC_xterm );
		break;
	  default:
		c = theDrnInterface->getCursor( XC_hand1 );
		break;
	  }
	  XmtDisplayCursor( w, c );
	  break;
	}
  case LeaveNotify:
	{
	  XmtDisplayDefaultCursor( w );
	  break;
	}
  default:
    break;
  }
}
