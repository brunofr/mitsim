//-*-c++-*------------------------------------------------------------
// NAME: Show help info for the keys and mouse buttons
// AUTH: Qi Yang
// FILE: DRN_Help.C
// DATE: Tue May  7 15:04:53 1996
//--------------------------------------------------------------------

#include <sstream>
#include <Tools/ToolKit.h>

#include <Xmt/Xmt.h>
#include <Xmt/Dialogs.h>

#include "DRN_Help.h"
#include "DRN_Interface.h"
using namespace std;

char *aux_help_msg = NULL;

void HelpKeys()
{
   static char *general =
      "Keys:\n"
      "  H/? -> Show this message box\n"
      "  Q -> Quit\n"
      "  F/+/S/- -> Run faster or slower";

   static char *modifier =
      "Modifier:\n"
      "  <Ctrl+> -> Small\n"
      "  <Ctrl+> -> Large";

   static char *navigation =
      "Navigation\n"
      "  <Home>/A -> Set home position\n"
      "  <Enter> -> Toggle run/pause\n"
      "  L/R/U/D/Arrows -> Pan\n"
      "  J/K -> Rotate\n"
      "  C -> Set center";
 
   static char *others =
      "Others\n"
      "  g -> Toggle on/off grid\n"
      "  m -> Toggle on/off map features\n"
      "  <F4> -> Toggle on/off legend\n"
      "  w -> Locate an object by code";

   ostringstream os;

   os << general << '\n'
      << modifier << '\n'
      << navigation << '\n'
      << others << '\n';
  
   if (aux_help_msg != NULL) {
      os << aux_help_msg << '\n';
   }


   XmtDisplayInformation(
	   theDrnInterface->widget(),
	   NULL, // msg_name,
	   os.str().c_str(), // msg_default
	   "Help on Commands");

}
