//-*-c++-*------------------------------------------------------------
// FILE: DRN_Segment.h
// AUTH: Peter J. Welch and Qi Yang
// DATE: Fri Oct 27 11:30:59 1995
//--------------------------------------------------------------------

#ifndef DRN_SEGMENT_HEADER
#define DRN_SEGMENT_HEADER

#include <GRN/RN_DrawableSegment.h>
#include <Tools/Math.h>
#include <Xmw/XmwColor.h>

class DRN_DrawingArea;
class DrawableRoadNetwork;

enum LineType { SOLID = 1, DASHED = 0, THICK = 2 }; 

const double ARC_ACCURACY = 1.0; // in pixels

class DRN_Segment : public RN_DrawableSegment
{
  friend class DrawableRoadNetwork;

public:

  DRN_Segment();
  virtual ~DRN_Segment() { }

  // Find color for the segment at position r.

  virtual Pixel calcColor(DRN_DrawingArea *, float r);

  int isInScope(DRN_DrawingArea *);

  int draw(DRN_DrawingArea * area);

  int highlight(DRN_DrawingArea * area, Pixel color);

  int isVisible() { return state_ & STATE_VISIBLE; }
      
  void calcScope();
  virtual void calcStaticInfo() {
	RN_DrawableSegment::calcStaticInfo();
	calcScope();
  }

  // These overload the method defined in RN_Arc

  float min_x() { return min_x_; }
  float min_y() { return min_y_; }
  float max_x() { return max_x_; }
  float max_y() { return max_y_; }

  float smdDensity(float r);
  float smdSpeed(float r);
  float smdFlow(float r);
  
private:

  float min_x_, min_y_, max_x_, max_y_;
};

#endif
