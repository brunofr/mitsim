//-*-c++-*------------------------------------------------------------
// FILE:    DRN_Sensor.h
// AUTHOR:  Qi Yang 
// DATE:    Nov 27, 1995
//--------------------------------------------------------------------

#ifndef DRN_SENSOR_HEADER
#define DRN_SENSOR_HEADER

#include <Xmw/XmwColor.h>

#include <GRN/RN_Sensor.h>
#include <GRN/WcsPoint.h>

class DRN_DrawingArea;

// Each sensor is represented by a square.

const float SENSOR_SIZE  = 1.8; // in meters

class DRN_Sensor : public RN_Sensor
{
   protected:
      
      // These two variable are static information to be filled by
      // calcGeometricData()

      WcsPoint center_;		// position
      float angle_;		// direction

      Pixel color_;		// indicates state
      XPoint points_[5];	// 4 vertices
      short int onScreen_;	// visible in the window
      char *label_;		// current label

   public:
	
      DRN_Sensor();
      virtual ~DRN_Sensor() { }

      void calcGeometricData();
      WcsPoint& center() { return center_; }

      virtual int draw(DRN_DrawingArea *);
      virtual void erase(DRN_DrawingArea *);

   private:

      virtual Pixel calcColor(DRN_DrawingArea *);
      virtual int calcPoints(DRN_DrawingArea *);
      virtual char* calcLabel();

      void draw(DRN_DrawingArea *, Pixel, char *);

      int stationCount();
      int stationFlow();
      float stationOccupancy();
      float stationSpeed();
};

#endif
