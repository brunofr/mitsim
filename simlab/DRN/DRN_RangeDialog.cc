//-*-c++-*------------------------------------------------------------
// DRN_RangeDialog.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//--------------------------------------------------------------------

#include <Xm/Form.h> 
#include <Xm/PushB.h> 
#include <Xm/Scale.h> 

#include <Tools/Math.h>
#include <Tools/Normalizer.h>

#include "DRN_RangeDialog.h"
#include "DRN_DrawingArea.h"

void
DRN_RangeDialog::getRange(Widget w, Normalizer * range,
			  double scaling, char * title)
{
   static DRN_RangeDialog * dialog = NULL;
   if (dialog != NULL) delete dialog;
   dialog = new DRN_RangeDialog(XtParent(w), range, scaling, title);
   dialog->read();
}


DRN_RangeDialog::DRN_RangeDialog(Widget parent, Normalizer *range,
				 double scaling, char * title)
   : _parent(parent), range_(range) 
{
   // Create widgets used in this component.

   XmString str = XmStringCreateSimple(title);

   Arg al[2];

   XtSetArg(al[0], XmNdialogStyle, XmDIALOG_FULL_APPLICATION_MODAL);
   XtSetArg(al[1], XmNdialogTitle, str);

   _baseWidget = XmCreateFormDialog(_parent, "Range", al, 2);

   XmStringFree(str);
   
   dec_ = pow(10.0, range_->decimals());
   scaling_ = scaling * dec_;

   int lower = Round(range_->lower() * scaling_);
   int upper = Round(range_->upper() * scaling_);

   str = XmStringCreateSimple("Minimum");

   _minScale = XtVaCreateManagedWidget (
      "Minimum",  xmScaleWidgetClass,
      _baseWidget, 
      XmNtitleString, str,
      XmNminimum, lower,
      XmNmaximum, upper,
      XmNvalue, Round(scaling_ * range_->minimum()),
      XmNdecimalPoints, range_->decimals(),
      XmNorientation, XmHORIZONTAL, 
      XmNshowValue, True, 
      XmNscaleWidth, 300, 
      XmNscaleHeight, 20, 
      XmNtopAttachment, XmATTACH_FORM,
      XmNleftAttachment, XmATTACH_FORM, 
      XmNrightAttachment, XmATTACH_FORM, 
      XmNtopOffset, 10, 
      XmNleftOffset, 10, 
      XmNrightOffset, 10,
      (XtPointer) NULL ); 

   XmStringFree(str);

   XtAddCallback ( _minScale,
		   XmNvalueChangedCallback,
		   &DRN_RangeDialog::minChangedCallback,
		   (XtPointer) this ); 

   XtAddCallback ( _minScale,
		   XmNdragCallback,
		   &DRN_RangeDialog::minDragCallback,
		   (XtPointer) this );   

   str = XmStringCreateSimple("Maximum");

   _maxScale = XtVaCreateManagedWidget (
      "Maximum", xmScaleWidgetClass,
      _baseWidget,
      XmNtitleString, str,
      XmNminimum, lower,
      XmNmaximum, upper,
      XmNvalue, Round(scaling_ * range_->maximum()),
      XmNdecimalPoints, range_->decimals(),
      XmNorientation, XmHORIZONTAL, 
      XmNshowValue, True, 
      XmNscaleWidth, 300, 
      XmNscaleHeight, 20, 
      XmNtopAttachment, XmATTACH_WIDGET, 
      XmNtopWidget, _minScale,
      XmNleftAttachment, XmATTACH_FORM, 
      XmNrightAttachment, XmATTACH_FORM, 
      XmNtopOffset, 10,
      XmNleftOffset, 10, 
      XmNrightOffset, 10, 
      (XtPointer) NULL );

   XmStringFree(str);

   XtAddCallback ( _maxScale,
		   XmNvalueChangedCallback,
		   &DRN_RangeDialog::maxChangedCallback,
		   (XtPointer) this ); 

   XtAddCallback ( _maxScale,
		   XmNdragCallback,
		   &DRN_RangeDialog::maxDragCallback,
		   (XtPointer) this );

   
   _okButton = XtVaCreateManagedWidget (
      "Ok",  xmPushButtonWidgetClass,
      _baseWidget, 
      XmNtopAttachment, XmATTACH_WIDGET,
      XmNtopWidget, _maxScale,
      XmNleftAttachment, XmATTACH_FORM, 
      XmNrightAttachment, XmATTACH_FORM, 
      XmNbottomAttachment, XmATTACH_FORM,
      XmNtopOffset, 10, 
      XmNbottomOffset, 10,
      XmNleftOffset, 130,
      XmNrightOffset, 130,
      XmNheight, 30, 
      (XtPointer) NULL ); 

   XtAddCallback ( _okButton,
		   XmNactivateCallback,
		   &DRN_RangeDialog::rangeOkCallback,
		   (XtPointer) this ); 
}


DRN_RangeDialog::~DRN_RangeDialog()
{
   XtDestroyWidget(XtParent(_baseWidget));
}


void
DRN_RangeDialog::read()
{
   XtManageChild(_baseWidget);
}


// The following functions are static member functions used to
// interface with Motif.

void DRN_RangeDialog::minChangedCallback ( Widget    w,
					   XtPointer clientData,
					   XtPointer callData ) 
{ 
   DRN_RangeDialog* obj = ( DRN_RangeDialog * ) clientData;
   obj->changeMinValue ( w, callData );
}

void DRN_RangeDialog::minDragCallback ( Widget    w,
					XtPointer clientData,
					XtPointer callData ) 
{ 
   DRN_RangeDialog* obj = ( DRN_RangeDialog * ) clientData;
   obj->changeMinValue ( w, callData );
}

void DRN_RangeDialog::maxChangedCallback ( Widget    w,
					   XtPointer clientData,
					   XtPointer callData ) 
{ 
   DRN_RangeDialog* obj = ( DRN_RangeDialog * ) clientData;
   obj->changeMaxValue ( w, callData );
}

void DRN_RangeDialog::maxDragCallback ( Widget    w,
					XtPointer clientData,
					XtPointer callData ) 
{ 
   DRN_RangeDialog* obj = ( DRN_RangeDialog * ) clientData;
   obj->changeMaxValue ( w, callData );
}

void DRN_RangeDialog::rangeOkCallback ( Widget    w,
					XtPointer clientData,
					XtPointer callData ) 
{ 
   DRN_RangeDialog* obj = ( DRN_RangeDialog * ) clientData;
   obj->rangeOk ( w, callData );
}


void DRN_RangeDialog::changeMinValue ( Widget w, XtPointer callData )
{
   XmScaleCallbackStruct *cbs = (XmScaleCallbackStruct*) callData;
   double x = cbs->value / scaling_;
   if (x > range_->maximum()) {
      XtVaSetValues(w, XmNvalue,
		    Round(scaling_ * range_->maximum()),
		    (XtPointer) NULL);
   } else {
      range_->minimum(x);
      theDrawingArea->redraw();
   }
}


void DRN_RangeDialog::changeMaxValue ( Widget w, XtPointer callData )
{
   XmScaleCallbackStruct *cbs = (XmScaleCallbackStruct*) callData;
   double x = cbs->value / scaling_;
   if (x < range_->minimum()) {
      XtVaSetValues(w, XmNvalue,
		    Round(scaling_ * range_->minimum()),
		    (XtPointer) NULL);
   } else {
      range_->maximum(x);
      theDrawingArea->redraw();
   }
}


void DRN_RangeDialog::rangeOk ( Widget, XtPointer )
{
}
