//-*-c++-*------------------------------------------------------------
// DRN_SignalDialog.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef DRN_SIGNALDIALOG_HEADER
#define DRN_SIGNALDIALOG_HEADER

#include <Xmw/XmwDialogManager.h>
#include <Xmw/XmwCallback.h>

class DRN_SignalDialog: public XmwDialogManager
{
  CallbackDeclare(DRN_SignalDialog);

public:

  DRN_SignalDialog(Widget parent);
  ~DRN_SignalDialog() { }
  void post();				// virtual

private:

  // overload the virtual functions in base class

  void okay(Widget, XtPointer, XtPointer);
  void cancel(Widget, XtPointer, XtPointer);
  void help(Widget, XtPointer, XtPointer);

  // new callbacks

  void clickTypesCB(Widget, XtPointer, XtPointer);
  void clickLabelCB(Widget, XtPointer, XtPointer);

private:
	  
  Widget typesFld_;			// check box
  int types_;
  Widget isLabelOnFld_;		// toggle button
  Boolean isLabelOn_;
};

#endif
