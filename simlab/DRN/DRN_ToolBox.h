//-*-c++-*------------------------------------------------------------
// DRN_ToolBox.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef DRN_TOOLBOX_HEADER
#define DRN_TOOLBOX_HEADER

#include <Xmw/XmwWidget.h>

class DRN_ToolBox : public XmwWidget
{
	  friend class DRN_Interface;

   public:

	  DRN_ToolBox(Widget w);
	  virtual ~DRN_ToolBox();

   private:
	  
	  static void toolsCB(Widget, XtPointer, XtPointer);
	  static void viewTypeCB(Widget, XtPointer, XtPointer);
	  static void homePositionCB(Widget, XtPointer, XtPointer);
	  static void findObjectCB(Widget, XtPointer, XtPointer);

   protected:

	  Widget actions_;
	  Widget tools_;
	  Widget viewType_;
};

extern DRN_ToolBox *theToolBox;

#endif
