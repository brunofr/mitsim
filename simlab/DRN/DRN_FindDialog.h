//-*-c++-*------------------------------------------------------------
// DRN_FindDialog.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the definition for the class DRN_FindDialog.
// This dialog box is designed to locate any network object by code.
//--------------------------------------------------------------------

#ifndef DRN_FINDDIALOG_HEADER
#define DRN_FINDDIALOG_HEADER

#include <Xmw/XmwDialogManager.h>

class DRN_FindDialog: public XmwDialogManager
{
   public:

      enum {
		 FIND_NODE    = 0x0000,
		 FIND_LINK    = 0x0001,
		 FIND_SEGMENT = 0x0002,
		 FIND_LANE    = 0x0003,
		 FIND_SENSOR  = 0x0004,
		 FIND_SIGNAL  = 0x0005,
		 FIND_OD      = 0x0006,
		 FIND_PATH    = 0x0007
      };

     enum {
       ANNOTATION_ALL    = 0x0003,
       ANNOTATION_LABEL  = 0x0001,
       ANNOTATION_SYMBOL = 0x0002
     };  


    enum {
       max_code_columns = 10
    };
	  struct dataType {
			int type;			// current type
			char object[max_code_columns+1]; // current value
			int annotation;		// current annotation
	  };

   public:

      DRN_FindDialog (Widget parent);
	  ~DRN_FindDialog() { }

	  void post();				// virtual

   protected:
	  
	 // overload the virtual functions

	  void okay(Widget w, XtPointer tag, XtPointer data);
	  void cancel(Widget w, XtPointer tag, XtPointer data);
	  void help(Widget w, XtPointer tag, XtPointer data);

   protected:

	  static XtResource resources[];
	  dataType data_;
};

#endif
