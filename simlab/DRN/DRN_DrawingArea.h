//-*-c++-*------------------------------------------------------------
// DRN_DrawingArea.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//--------------------------------------------------------------------

#ifndef DRN_DRAWINGAREA_HEADER
#define DRN_DRAWINGAREA_HEADER

#include <Tools/Math.h>
#include <GRN/WcsPoint.h>
#include <Xmw/XmwPoint.h>
#include <Xmw/XmwDrawingArea.h>

// Default min and max scale in pixels per meter. These value are used
// in any calculation if the derived class has assigned min_scale and
// max_scale.

extern const double MIN_SCALE;
extern const double MAX_SCALE;

class RN_WorldSpace;
class RN_Arc;
class RN_Path;
class DRN_Node;
class DRN_Menu;
class DRN_KeyInteractor;
class DRN_MouseInteractor;
class DRN_Legend;
class Normalizer;
class LogNormalizer;
class GenericVariable;
class DrawableRoadNetwork;
class XmwFont;

class DRN_DrawingArea : public XmwDrawingArea
{
public:

  friend class DrawableRoadNetwork;

  enum {
	LOWEST_RESOLUTION  = 0, // only links
	LOW_RESOLUTION     = 1, // lanes
	MEDIUM_RESOLUTION  = 2, // lane type, vehicles, sensors and signals
	HIGH_RESOLUTION    = 3, // detailed vehicles
	HIGHEST_RESOLUTION = 4	 // labels
  };


  enum {
   SIGNAL_ON          = 0x00000001,
   SENSOR_ON          = 0x00000002,
   INCIDENT_ON        = 0x00000004,
   VEHICLE_ON         = 0x00000008,
   NETWORK_LOADED     = 0x01000000,
   MOUSE_DOWN         = 0x04000000
  };

  // Window initial states. Those bits should be 0 when the window
  // has been initialized properly and ready for the normal use.
  // If window is CREATED but NOT_READY, it can be used to show
  // the pixmap of the program logo.  If the state is set to
  // WINDOW_REDRAW, the entire window will be redraw in the next
  // event loop.

  enum {
   VIEW_NONE   = 0x10000000,
   REDRAW      = 0x40000000,
   NOT_READY   = 0x80000000
  };

public:

  DRN_DrawingArea(Widget w);

  virtual ~DRN_DrawingArea() { }
  virtual void reset();

  static int loadScalableFontSizes(GenericVariable&);
  static RN_WorldSpace& worldSpace();

  virtual void initialize();
  virtual DRN_MouseInteractor * newMouseInteractor();
  virtual DRN_KeyInteractor * newKeyInteractor();
  virtual DRN_Legend * newLegend();

  DRN_KeyInteractor * key_interactor() {
	return key_interactor_;
  }

  DRN_MouseInteractor * mouse_interactor() {
	return mouse_interactor_;
  }

  DRN_Legend * legend() { return legend_; }

  // ZOOMING IN AND OUT

  // Callback invoked by zooming keys. This function will call
  // zoom(double)

  void zoom(KeySym keysym);

  // Zooms the view window. It also calls redraw to expose.

  // ROTATION

  // Callback invoked by rotation keys. It calls rotate(double).

  void rotate(KeySym keysym);

  // Rotates the view window. East is given by zero_angle. For
  // regular map oritation. Input argument should be PI/2.  This
  // function calls redraw() to expose.

  // PANNING

  // Callback invoked by pressing the panning key (numeric pad or
  // arrows).  This function calls pan(int mode)

  void pan(KeySym keysym);

  // Pan(int mode) is called either by pan(KeySym).  The input
  // argument mode defines which direction to go and should have a
  // value of [1-9] but 5.  This function also calls redraw to
  // update the drawing and expose the view window

  void setHomePosition();
  virtual void expose();	// virtual
  virtual void resize();	// virtual

  inline int resolution() { return resolution_; }

  // redraw every thing and then expose
      
  void redraw();
  void draw_misc();

  void drawGrid();
  void drawVersion();

  void lineObjectsColorRange();
  void pointObjectsColorRange();

  // current scope in WorldSpace. These are updated when either
  // the window is recentered or zooming scale are changed.

  double left() { return left_; }
  double top() { return top_; }
  double right() { return right_; }
  double bottom() { return bottom_; }

  int isInScope(const WcsPoint &p);

  inline double min_scale() { return min_scale_; }
  inline double max_scale() { return max_scale_; }
 
  inline Normalizer* density() { return density_; }
  inline Normalizer* speed() { return speed_; }
  inline Normalizer* occupancy() { return occupancy_; }
  inline Normalizer* odFlow() { return odFlow_; }
  inline Normalizer* linkFlow() { return linkFlow_; }
  inline Normalizer* linkCount() { return linkCount_; }

  // This control the brightness of link colors

  void intensity(float x) { intensity_ = x; }
  float intensity() { return intensity_; }

  virtual Pixel densityColor(double);
  virtual Pixel speedColor(double);
  virtual Pixel occupancyColor(double);
  virtual Pixel odFlowColor(double);
  virtual Pixel linkFlowColor(double);
  virtual Pixel linkCountColor(double);

  // Set view to based on view marker

  void GotoMarker(double xpos, double ypos,
				  double scale, double angle) ;

  // ZOOMING IN AND OUT

  // Zooms the view window. It also calls redraw to expose.

  virtual void zoom(double scale = 1); // 1=max_scale 0=min_scale
  virtual void zoomIn(XmwPoint& from, XmwPoint &to);
  virtual void zoomIn(XmwPoint& p);
  virtual void zoomOut(XmwPoint& p);

  // ROTATION

  // Rotates the view window. East is given by zero_angle. For
  // regular map oritation. Input argument should be PI/2.  This
  // function calls redraw() to expose.

  virtual void rotate(double zero_angle = HALF_PI);
  // Use mouse to rotate
  virtual void rotate(XmwPoint& from, XmwPoint &to);

  // PANNING

  // Pan(int mode) is called either by pan(KeySym) or callbacks of
  // buttons in ToolBoxWindow.  The input argument mode defines
  // which direction to go and should have a value of [1-9] but 5.
  // This function also calls redraw to update the drawing and
  // expose the view window

  virtual void pan(int mode, double amount);
  // Use mouse to rotate
  virtual void pan(XmwPoint& from, XmwPoint &to);

  // Set the center of window to the point where left mouse button
  // is pressed

  Boolean queryCursorPoint(XmwPoint &p);
  void set_center(const WcsPoint &p);
  void keepInScope(const WcsPoint &p);
  virtual void reCenter(const WcsPoint& p);
  virtual void reCenter(const XmwPoint& pressed);
  virtual void reCenterNoRedraw(const XmwPoint& pressed);

  virtual void annotate(const WcsPoint &, const char *code = NULL,
						int annotation = 0, int requires = 1);

  void annotateODPair(DRN_Node *ori, DRN_Node *des,
					  const char *label, int annotation);
  void annotationPath(RN_Path *path, int annotation);

  virtual void locate(int type, const char *code, int annotation);

  // Tranlates a line represented by two world points to a line in
  // the current view window.  The line extended outside the wiew
  // window is cut off to assure safe operation in X.

  int cutLine(const WcsPoint&, const WcsPoint&,
			  XmwPoint&, XmwPoint&);

  // draw a line represented by two world points.
      
  virtual int draw(RN_Arc& arc);
  virtual int draw(const WcsPoint& p1, const WcsPoint& p2);
  virtual int draw(const WcsPoint& p);
  virtual int draw(const WcsPoint& p, const char *str, int mode = 0);
  virtual int draw(const WcsPoint& p, int code, int mode = 0);
  virtual int draw(const WcsPoint& p, float value, int dec, int mode = 0);

  virtual int drawArc(RN_Arc& arc) {
	return draw(arc);
  }
  virtual int drawLine(const WcsPoint& p1, const WcsPoint& p2) {
	return draw(p1, p2);
  }
  virtual int drawPoint(const WcsPoint& p) {
	return draw(p);
  }

  int drawDiamond(const WcsPoint& p, double angle);
  int drawDiamond(const WcsPoint& p, double angle, double size);

  int drawFilledPolygon(WcsPoint *pts, int npts,
						int shape = Nonconvex) ;

  Pixmap draw(const XmwPoint &p, const char *name);
  void draw(const XmwPoint &p, const Pixmap& pixmap) {
	XmwDrawingArea::draw(p, pixmap);
  }
  double meter2Pixels(double scale);
  double meter2Pixels() { return meter2Pixels_; }

  // This function checks if the view area is ready for drawing
  // (network has been read and world space is calculated)

  inline int isDrawable() {
	return (meter2Pixels_ > 1.0E-5);
  }

  short int thickLine() { return thickLine_; }

  inline WcsPoint& center() { return center_; }
  void center(const WcsPoint & center);
  inline double angle() { return angle_; }

  // Transformation wrappers

  WcsPoint world2Window(const WcsPoint & world_point);

  int world2Window(const WcsPoint & world_point,
				   XmwPoint & window_point);

  int world2Window(const WcsPoint & world_point,
				   XPoint & xpoint);

  WcsPoint window2World(const XmwPoint & window_point);
  WcsPoint window2Database(const XmwPoint & window_point);

  // Diagnostic functions:

  // This will write out the world and window point clicked

  virtual void showCoordinates(const XmwPoint& point);
  void showSegment(const XmwPoint & point, int mode);
      
  // state manipulators

  inline unsigned int state(unsigned int mask = 0xFFFF) {
	return (state_ & mask);
  }
  inline void setState(unsigned int s) {
	state_ |= s;
  }
  inline void unsetState(unsigned int s) {
	state_ &= ~s;
  }

  // These functions check if the object need to be drawn
  // (selected and reslution is high enough)

  int isSensorDrawable();
  int isSignalDrawable();
  int isTollBoothDrawable();
  int isBusStopDrawable();  //margaret
  int isLaneMarkDrawable();
  int isLaneTypeDrawable();
  int isVehicleDrawable();
  int isVehicleStateDrawable();

  void gridSize(double size) { gridSize_ = size; }
  double gridSize() { return gridSize_; }

private:

  void calcResolution();
  void calcScope();

  static void showCursorEH( Widget w,
							XtPointer clientData,
							XEvent *event,
							Boolean *b );
protected:

  static DrawableRoadNetwork * theNetwork_;
      
  unsigned int state_;
  float intensity_;

  double min_scale_, max_scale_;

  double meter2Pixels_;	// Pixels per meter - defines scale_
  short int thickLine_;	// line width in pixels, min=1

  // These two values are updated when rotatation occurs

  double sin_;				// sin(angle)
  double cos_;				// cos(angle)

  int resolution_;

  WcsPoint center_;			// current center

  double scopeEpsilon_;		// buffer space from bounding circle
  double left_;				// current scope in WorldSpace
  double top_;
  double right_;
  double bottom_;

  double angle_;			// angle in radians - defines rotation
  double gridSize_;

  static Normalizer * density_;
  static Normalizer * speed_;
  static Normalizer * odFlow_;
  static Normalizer * linkFlow_;
  static Normalizer * linkCount_;
  static Normalizer * occupancy_;

private:

  DRN_Legend * legend_;
  DRN_KeyInteractor   * key_interactor_;
  DRN_MouseInteractor * mouse_interactor_;
};

extern DRN_DrawingArea * theDrawingArea;

#endif
