//-*-c++-*------------------------------------------------------------
// DRN_SignalEditor.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef DRN_SIGNAL_EDITOR_HEADER
#define DRN_SIGNAL_EDITOR_HEADER

#include <Xmw/XmwDialogManager.h>
#include <Xmw/XmwCallback.h>

class DRN_Signal;
class RN_CtrlStation;

class DRN_SignalEditor : public XmwDialogManager
{
	  CallbackDeclare(DRN_SignalEditor);

   public:

	  DRN_SignalEditor(Widget parent);
	  ~DRN_SignalEditor();

	  static void createAndPost(RN_CtrlStation *s);

   private:

	  void postAndWait(RN_CtrlStation *s);

	  // existing callbacks

	  void okay(Widget, XtPointer, XtPointer);
	  void cancel(Widget, XtPointer, XtPointer);
	  void help(Widget, XtPointer, XtPointer);

	  // new callbacks

	  void enter ( Widget, XtPointer, XtPointer );
// 	  void leave ( Widget, XtPointer, XtPointer );

   private:

	  RN_CtrlStation *station_;	// signal station
	  Widget position_;			// XmLabel
	  Widget type_;				// XmLabel
	  Widget visibility_;		// XmtInputField
	  Widget lanes_;			// XbaeMatrix
};

#endif
