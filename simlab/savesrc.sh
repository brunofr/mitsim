# !/bin/csh
# This is csh script for backup the simlab programs.

set FromDir = `pwd`
set ToDir = $HOME/backup
set TargetFile = $ToDir/simlab-src.tar

set SrcList = (\
	include \
	NewXmt \
    	Xmw \
	Xfw \
	IO \
	UTL \
	Tools \
	Templates \
	GRN \
	DRN \
	GDS \
	TC \
	TMS \
	TS \
	MESO \
	SMC \
	PS \
	Q \
    MDI \
    Rob \
	Test \
	ad \
	)

echo "Backup files now so you do not have to cry later:"
echo "  from: $FromDir"
echo "    in: $SrcList"
echo "    to: $TargetFile.gz"
echo "Please confirm (y/n) ?"

set a=$<
if ($a != 'y') exit

if ! -d $ToDir mkdir $ToDir 

echo "\nBackup top level files  ..."
tar cvf $TargetFile *.sh *.pl *.tmpl *.mk Makefile readme* *.cfg

echo "\nBackup source files ..."
foreach SrcDir ($SrcList)
	echo "\nPacking files $FromDir/$SrcDir ..."
	tar rvf $TargetFile \
		$SrcDir/Make* \
		$SrcDir/*.[chy] $SrcDir/*.cc \
		$SrcDir/*.lex $SrcDir/*.xbm \
		$SrcDir/*.xpm $SrcDir/*.sh \
		$SrcDir/*.pl $SrcDir/*.ad
end

echo "\nCompressing $TargetFile ..."
gzip $TargetFile
echo "$TargetFile.gz done."
