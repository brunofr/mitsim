//-*-c++-*------------------------------------------------------------
// SMC_ModuleInfo.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef SMC_MODULEINFO_HEADER
#define SMC_MODULEINFO_HEADER

#include <iostream>

#ifdef INTERNAL_GUI
#include <Xmt/Xmt.h>
#endif

class SMC_ModuleInfo
{
  friend class SMC_SetupDialog;
  friend class SMC_SimlabModule;

public:

  SMC_ModuleInfo(const char *name, char *master=NULL,
				 char *host=NULL, char *display=NULL,
				 unsigned int mode = 0);
  ~SMC_ModuleInfo();
	  
  void print(std::ostream &os = std::cout);

private:
  const char *name_;
  char *master_;
  char *host_;
  char *display_;
  unsigned int mode_;

#ifdef INTERNAL_GUI
private:

  Widget masterUI_;				// XmtInputField
  Widget hostUI_;				// Combo Box
  Widget displayUI_;			// Combo Box
  Widget modeUI_;				// XmtChooser

  static Widget createHostUI(Widget parent, const char *name,
							 struct pvmhostinfo *info, int nhost);
  void createUI(Widget parent, struct pvmhostinfo *info, int nhost);
  void transferData();
  void activate();
  void deactivate();
  
#endif
};

#endif
