//-*-c++-*------------------------------------------------------------
// Main.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains implementation of main()
//--------------------------------------------------------------------

#include <Tools/ToolKit.h>
using namespace std;

#ifdef INTERNAL_GUI
#include "SMC_Interface.h"
#endif

#include "SMC_Engine.h"
#include "SMC_Version.h"
#include "SMC_Exception.h"
#include "SMC_CmdArgsParser.h"

//void main(int argc, char **argv)
int main(int argc, char **argv)
{
  ToolKit::initialize();

  ::Welcome(argv[0]);

  // Create the interface

#ifdef INTERNAL_GUI
  theInterface = new SMC_Interface(&argc, argv);
#endif

  SMC_Engine engine(&argc, argv);

  SMC_CmdArgsParser cp;
  cp.parse(&argc, argv);

  if (engine.canStart()) {
	engine.loadMasterFile();
  }
#ifndef INTERNAL_GUI
  else {
	cerr << "No master file." << endl;
	theException->exit(0);
  }
#endif

  engine.run();
}
