//-*-c++-*------------------------------------------------------------
// SMC_Modeline.cc
//
// qiyang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <Xbae/Matrix.h>

#include "SMC_Modeline.h"
#include "SMC_Interface.h"
#include "SMC_Engine.h"

SMC_Modeline::SMC_Modeline(Widget parent)
   : XmwModeline(parent)
{
   static String help_timers[2];
   help_timers[0] = XmtLocalize2(timers_,
								 "Current time", "timers", "current");
   help_timers[1] = XmtLocalize2(timers_,
								 "Real time left", "timers", "left");
   XtAddCallback(timers_,
			   XmNenterCellCallback, &XmwModeline::cellEnterCB,
			   help_timers);

   static String help_meters[4];
   help_meters[0] = XmtLocalize2(meters_,
								 "Desired", "meters", "desired");
   help_meters[1] = XmtLocalize2(meters_,
								 "MITSIM", "meters", "MITSIM");
   help_meters[2] = XmtLocalize2(meters_,
								 "TMS", "meters", "TMS");
   help_meters[3] = XmtLocalize2(meters_,
								 "MesoTS", "meters", "MesoTS");
   XtAddCallback(meters_,
			   XmNenterCellCallback, &XmwModeline::cellEnterCB,
			   help_meters);
}


void SMC_Modeline::keyStateChanged(enum KeyState state)
{
   switch (state) {
	  case KEY_STATE_PAUSE:
	  {
		 theInterface->pause(FLAG_ALL);
		 break;
	  }
	  case KEY_STATE_RUN:
	  {
		 theInterface->run(FLAG_ALL);
		 break;
	  }
   }
}

void SMC_Modeline::updateButtons()
{
   if (!theEngine->canStart()) {
	  setKeyState(KEY_STATE_DISABLED);
   } else if (theEngine->isRunning()) {
	  setKeyState(KEY_STATE_RUN);
   } else {
	  setKeyState(KEY_STATE_PAUSE);
   }
}

#endif
