//-*-c++-*------------------------------------------------------------
// SMC_Engine.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef SMC_ENGINE_HEADER
#define SMC_ENGINE_HEADER

//#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/times.h>

#include <Tools/SimulationClock.h>
#include <Tools/SimulationEngine.h>

#include "SMC_SimlabModule.h"

class SMC_Engine : virtual public SimulationEngine, 
				   virtual public SimulationClock
{
  friend class SMC_CmdArgsParser;

protected:

  char **argv_;				// cmd arguments
  int pid_;					// process
  unsigned int busy_;		// state of the modules

  SMC_SimlabModule* modules_[5];
      
  double batchStepSize_;	// batch step size
  double batchTime_;		// next time to update console

  static int debugger_;

public:

  SMC_Engine(int *argc, char **argv);
  ~SMC_Engine() { }

  static int debugger() { return debugger_; }

  const char* ext();		// virtual

  int isOnHold();

  inline unsigned int busy(unsigned int mask = 0xFFFFFFFF) {
	return (busy_ & mask);
  }
  inline void unsetBusy(int s = 0) {
	busy_ &= ~s;
  }
  inline void setBusy(int s) {
	busy_ |= s;
  }
  inline int isWaiting(unsigned int mask) {
	return (busy_ & mask);
  }
  inline int isWaiting() {
	return (busy_ & FLAG_ALL);
  }

  void setupModules(SMC_SysInfo *);

  inline SMC_SimlabModule& mitsim() { return *modules_[0]; }
  inline SMC_SimlabModule& tms() { return *modules_[1]; }
  inline SMC_SimlabModule& meso() { return *modules_[2]; }
  inline SMC_SimlabModule& mdi() { return *modules_[3]; }
  inline SMC_SimlabModule& tmca() { return *modules_[4]; }

  int loadMasterFile();		// virtual

  int start();		// virtual

  int simulationLoop();

  void run();
  void quit(int s);

  int receiveMessages(double sec = 0.0);

  void pauseSimulation(int others);
  void resumeSimulation(int others);

private:

  void startChildren();

  // Check the integrity of the timing data.

  int checkConnection(SMC_SimlabModule &);
  int checkTimingData(SMC_SimlabModule &);
};

extern SMC_Engine * theEngine; 

#endif
