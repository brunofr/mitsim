//-*-c++-*------------------------------------------------------------
// SMC_ModuleInfo.cc
//
// qiyang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <string.h>
#include <assert.h>

#include <pvm3.h>

#include <Xmt/Xmt.h>
#include <Xmt/Chooser.h>
#include <Xmt/InputField.h>
#include <Xmt/Dialogs.h>
#include <Combo/ComboBox.h>

#include <Tools/ToolKit.h>

#include "SMC_ModuleInfo.h"
#include "SMC_SetupDialog.h"
using namespace std;

SMC_ModuleInfo::SMC_ModuleInfo(const char *name, char *master,
							   char *host, char *display,
							   unsigned int mode) :
  name_(name), mode_(mode)
{
  if (master) {
	master_ = strdup(master);
  } else {
	master_ = NULL;
  }
  if (host) {
	host_ = strdup(host);
  } else {
	host_ = strdup(HostName());
  }
  if (display) {
	display_ = strdup(display);
  } else {
	display_ = NULL;
  }
}

SMC_ModuleInfo::~SMC_ModuleInfo()
{
  if (master_) free(master_);
  if (host_) free(host_);
  if (display_) free(display_);
}

void SMC_ModuleInfo::print(ostream &os)
{
  if (!ToolKit::isValidFilename(master_)) return;

  os << "[" << name_ << "] = {" << endl;
  os << "\t\"" << NoNull(master_) << "\"\t# master file" << endl
	 << "\t\"" << NoNull(host_) << "\"\t# host" << endl
	 << "\t\"" << NoNull(display_) << "\"\t# display" << endl;
  if (mode_) {
	os << "0x" << hex << mode_ << dec << "\t# mode" << endl;
	os << "\t% 0x1 = Hold on" << endl;
  }
  os << "}" << endl;
  os << endl;
}


#ifdef INTERNAL_GUI

// Create a list of hosts

Widget SMC_ModuleInfo::createHostUI(Widget parent, const char *name,
									struct pvmhostinfo *info, int nhost)
{
  Widget w = XtNameToWidget(parent, name);
  if (w) {
	XmString str;
	for (int i = 0; i < nhost; i ++) {
	  str = XmStringCreateLtoR(info[i].hi_name, XmSTRING_DEFAULT_CHARSET);
	  XmComboBoxAddItem(w, str, 0);
	  XmStringFree(str);
	}
  }
  return w;
}

void SMC_ModuleInfo::createUI(Widget parent, struct pvmhostinfo *info,
								int nhost)
{
  char pagename[64];
  strcpy(pagename, "*");
  strcat(pagename, name_);
  strcat(pagename, "Group");

  Widget page = XtNameToWidget(parent, pagename);
  assert (page);

  // Master file

  masterUI_ = XtNameToWidget (page, "*master");
  assert(masterUI_);
  XmtInputFieldSetString(masterUI_, master_);

  // Mode
  modeUI_ = XtNameToWidget(page, "*mode");
  assert(modeUI_);
  XmtChooserSetState(modeUI_, mode_, False);

  // Create two ComboBox field with a list of machines for host and
  // display

  XmString str;

  hostUI_ = createHostUI(page, "*host", info, nhost);
  assert(hostUI_);
  str = XmStringCreateLtoR(host_, XmSTRING_DEFAULT_CHARSET);
  if (XmComboBoxItemExists(hostUI_, str)) {
	XmComboBoxSelectItem(hostUI_, str, False);
  } else if(host_) {
	XmtDisplayErrorMsg(parent,
					   NULL,	// msg_name: for customizing by res
					   "%s not found in the virtual machine.",
					   "Error",				// title
					   "PVM was not stated properly for specified host.",
					   host_);
  }
  XmStringFree(str);

  displayUI_ = createHostUI(page, "*display", info, nhost);
  assert(displayUI_);
  str = XmStringCreateLtoR(display_, XmSTRING_DEFAULT_CHARSET);
  if (XmComboBoxItemExists(displayUI_, str)) {
	XmComboBoxSelectItem(displayUI_, str, False);
  } else {
	XmComboBoxSetString(displayUI_, display_);
  }
  XmStringFree(str);
}

void SMC_ModuleInfo::transferData()
{
  ::Copy(&master_, XmtInputFieldGetString(masterUI_));
  ::Alias(&host_, XmComboBoxGetString(hostUI_));
  ::Alias(&display_, XmComboBoxGetString(displayUI_));
  mode_ = XmtChooserGetState(modeUI_); 
}

void SMC_ModuleInfo::activate()
{
  XmwDialogManager::activate(masterUI_);
  XmwDialogManager::activate(hostUI_);
  XmwDialogManager::activate(displayUI_);
  XmwDialogManager::activate(modeUI_);
}

void SMC_ModuleInfo::deactivate()
{
  XmwDialogManager::deactivate(masterUI_);
  XmwDialogManager::deactivate(hostUI_);
  XmwDialogManager::deactivate(displayUI_);
  XmwDialogManager::deactivate(modeUI_);
}

#endif // INTERNAL_GUI
