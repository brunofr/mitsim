//-*-c++-*------------------------------------------------------------
// NAME: Main loop of the smc
// AUTH: Qi Yang
// FILE: SMC_EngineMainLoop.C
// DATE: Fri Nov 17 10:12:59 1995
//--------------------------------------------------------------------

#include <iostream>
using namespace std;

#include <Tools/ToolKit.h>
#include <Tools/Math.h>

#include "SMC_Engine.h"
#include "SMC_Exception.h"

#ifdef INTERNAL_GUI
#include <Xmt/Xmt.h>
#include <Xmt/Dialogs.h>
#include "SMC_Interface.h"
#include "SMC_Modeline.h"
#endif // !INTERNA_GUI


// This function is called by TimeOutSimulationLoop() in graphical
// mode or a while loop of main() in batch mode.  Graphics related
// functions should be placed inside INTERNAL_GUI block.

int
SMC_Engine::simulationLoop()
{
   const double epsilon = 1.E-5;

#ifdef INTERNAL_GUI
   if (isTimeToBreak(currentTime_)) {
	  const char *msg;
	  msg = Str("Reach break point at %s.",
				theSimulationClock->currentStringTime());
	  XmtDisplayWarningAndAsk(
		theInterface->widget(), "bpsDialog",
		msg,
		"Continue", "Quit", XmtNoButton,
		"\nClick @fBContinue@fR to proceed or @fBQuit@fB to\n"
		"Terminate the simulation");
	  nextBreakPoint(currentTime_ + theSimulationClock->stepSize());
   }
#endif
   
   if (currentTime_ < stopTime_ + epsilon) {
      advance();
      state_ = STATE_OK;
   } else {
      state_ = STATE_DONE;
   }
  
   mitsim().advanceClock();
   tms().advanceClock();

   if (mdi().isConnected()) {
	 mdi() << MSG_CURRENT_TIME << theEngine->currentTime();
	 mdi().flush(SEND_IMMEDIATELY);
   }
   if (tmca().isConnected()) {
	 tmca() << MSG_CURRENT_TIME << theEngine->currentTime();
	 tmca().flush(SEND_IMMEDIATELY);
   }


#ifdef INTERNAL_GUI
   theInterface->modeline()->update();
#else
   if (ToolKit::verbose()) {
      if (batchTime_ < currentTime_ + epsilon) { 
		 cout << currentStringTime() << endl;
		 batchTime_ = currentTime_ + batchStepSize_;
      }
   }
#endif
   
   return state_;
}


int
SMC_Engine::receiveMessages(double sec)
{
   int busy = 0, flag;
   sec = sec / 3.0;

   flag = mitsim().receiveMessages(sec);
   if (flag < 0) {
      theException->exit(FLAG_TMS + FLAG_MESO + FLAG_MDI + FLAG_TMCA );
   } else if (flag > 0) {
      busy |= FLAG_MITSIM;
   }

   flag = tms().receiveMessages(sec);
   if (flag < 0) {
      theException->exit(FLAG_MITSIM + FLAG_MESO + FLAG_MDI + FLAG_TMCA );
   } else if (flag > 0) {
      busy |= FLAG_TMS;
   }

   flag = meso().receiveMessages(sec);
   if (flag < 0) {
      theException->exit(FLAG_MITSIM + FLAG_TMS + FLAG_MDI + FLAG_TMCA );
   } else if (flag > 0) {
      busy |= FLAG_MESO;
   }

   flag = mdi().receiveMessages(sec);
   if (flag < 0) {
      theException->exit(FLAG_MITSIM + FLAG_TMS + FLAG_MESO + FLAG_TMCA );
   } else if (flag > 0) {
      busy |= FLAG_MDI;
   }

   flag = tmca().receiveMessages(sec);
   if (flag < 0) {
      theException->exit(FLAG_MITSIM + FLAG_TMS + FLAG_MESO + FLAG_MDI );
   } else if (flag > 0) {
      busy |= FLAG_TMCA;
   }

   return busy;
}
