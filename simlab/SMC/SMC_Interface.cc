//-*-c++-*------------------------------------------------------------
// SMC_Interface.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class implementation for the class
// SMC_Interface
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <iostream>
#include <cstdlib>
using namespace std;

#include <Xmt/Xmt.h>
#include <Xmt/Create.h>

#include <Xmw/XmwImage.h>
#include <Xmw/XmwSymbols.h>

#include "SMC_Interface.h"
#include "SMC_Menu.h"
#include "SMC_Engine.h"
#include "SMC_Modeline.h"
#include "SMC_SysInfo.h"
#include "SMC_Icon.cc"

SMC_Interface* theInterface = NULL;

SMC_Interface::SMC_Interface(int *argc, char **argv)
  : XmwInterface(
				 argc, argv,
				 new XmwImage("smc", icon_bits, NULL, icon_width, icon_height))
{
  theInterface = this;
  create();
}

SMC_Interface::~SMC_Interface()
{
  destroy(widget_);
}

// This function is called by the class constructor (XmwInterface)

void SMC_Interface::create()
{
  XmwInterface::create();
  manage();
}

XmwMenu* SMC_Interface::menu(Widget parent)
{
  return new SMC_Menu(widget_);
}

XmwModeline* SMC_Interface::modeline(Widget parent)
{
  return new SMC_Modeline(parent);
}


void SMC_Interface::updateButtons()
{
  menu()->updateButtons();
  modeline()->updateButtons();
}


void SMC_Interface::pause(int others)
{
  toggle_zzz(true);
  menu()->updateRunningState(False);
  modeline()->setKeyState(XmwModeline::KEY_STATE_PAUSE);
  theEngine->pauseSimulation(others);
  msgSet("Simulation is paused");

  handlePendingEvents();
}

void SMC_Interface::run(int others)
{
  if (!theEngine->isStarted()) {
	theEngine->start();
	theInterface->setTitle(theSysInfo->title_);
	theInterface->updateButtons();
  }
  toggle_zzz(false);
  menu()->updateRunningState(True);
  modeline()->setKeyState(XmwModeline::KEY_STATE_RUN);
  theEngine->resumeSimulation(others);
  msgSet("Simulation is in progress");

  handlePendingEvents();
}

int SMC_Interface::mainloop()
{
  destroyWorkingBox ();
  showDefaultCursor ();

  // Simulation will be started manually from the menu.
   
  theEngine->pause();
  updateButtons();

  while (theEngine->state() >= 0) {

	if (handlePendingEvents()) {
	  ;						// XEvents have the highest priority

	} else if (theEngine->receiveMessages() > 0) {
	  ;						// IPC events have the 2nd priority

	} else if ((theEngine->isWaiting() || theEngine->isOnHold()) &&
			   theEngine->receiveMessages(NO_MSG_WAITING_TIME) >= 0) {
	  ;						// Wait for IPC events

	} else if (theEngine->isPaused()) {

	  // If simulation is paused, we wait for XEvents or IPC events
	  // to occurs.  Here we will partially block the process by
	  // waiting for XEvent when the simulation is paused.  A dummy
	  // event is generated to break the waiting for XEvents in
	  // order to process IPC events periodically (e.g. update the
	  // network state, start, quit, and so on)

	  waitNextEvent(2000);

	} else if (theEngine->isRunning()) {
	  theEngine->simulationLoop(); // real simulation loop
	}
  }

  return theEngine->state();
}

#endif // INTERNAL_GUI
