//-*-c++-*------------------------------------------------------------
// SMC_SetupDialog.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef SMC_SETUPDIALOG_HEADER
#define SMC_SETUPDIALOG_HEADER

#include <Xmw/XmwDialogManager.h>

class SMC_SetupDialog : public XmwDialogManager
{
public:

  SMC_SetupDialog(Widget parent);
	
  ~SMC_SetupDialog() { }

  void post_and_wait(SMC_SetupDialog **ptr); // virtual
	  
  void activate();
  void deactivate();

protected:

  // overload the virtual functions

  void okay(Widget w, XtPointer tag, XtPointer data);
  void cancel(Widget w, XtPointer tag, XtPointer data);
  void help(Widget w, XtPointer tag, XtPointer data);

private:
	  
  SMC_SetupDialog **ptr_to_handler_;

  Widget title_;			// XmtInputField
  Widget indir_;			// XmtInputField
  Widget outdir_;			// XmtInputField

  // Data for individual modules are in SMC_SysInfo class
};

#endif
