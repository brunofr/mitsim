//-*-c++-*------------------------------------------------------------
// NAME: Parser for the Simlab master file
// AUTH: Qi Yang
// FILE: SMC_FileManager.h
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#ifndef SMC_FILEMANAGER_HEADER
#define SMC_FILEMANAGER_HEADER

//#include <fstream>
#include <Tools/GenericSwitcher.h>

class GenericVariable;

class SMC_FileManager : public GenericSwitcher
{
   public:

      SMC_FileManager() : GenericSwitcher() { }
      ~SMC_FileManager() { }

      void read(const char *);

      int parseVariable(GenericVariable &gv);

   private:

      int parseVariable(char ** varptr, GenericVariable &gv);

      int isEqual(const char *s1, const char *s2);
};

#endif
