//-*-c++-*------------------------------------------------------------
// SMC_Menu.h
//
// qiyang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef SMC_MENU_HEADER
#define SMC_MENU_HEADER

#include <Xmw/XmwMenu.h>
#include <Xmw/XmwCallback.h>

class SMC_Menu : public XmwMenu
{
	  friend class SMC_Interface;
	  friend class SMC_SetupDialog;

	  CallbackDeclare(SMC_Menu);

   public:

  enum {
	 SIM_WINDOW_MITSIM = 0x00010000,
	 SIM_WINDOW_TMS    = 0x00020000,
	 SIM_WINDOW_MESO   = 0x00040000
  };
	  SMC_Menu(Widget parent);
	  ~SMC_Menu() { }

	  void updateRunningState(int running);
	  int save();

   protected:
	
      void updateButtons();

	  // Callbacks inherited from base class

	  void open ( Widget, XtPointer, XtPointer );
 	  void save ( Widget, XtPointer, XtPointer );
	  void saveAs ( Widget, XtPointer, XtPointer );
	  void print ( Widget, XtPointer, XtPointer );
	  void quit ( Widget, XtPointer, XtPointer );

	  // New callbacks

	  void randomizer ( Widget, XtPointer, XtPointer );

	  void run ( Widget, XtPointer, XtPointer );
	  void pause ( Widget, XtPointer, XtPointer );
	  void setup ( Widget, XtPointer, XtPointer );
	  void option ( Widget, XtPointer, XtPointer );

	  void mitsim ( Widget, XtPointer, XtPointer );
	  void tms ( Widget, XtPointer, XtPointer );
	  void meso ( Widget, XtPointer, XtPointer );

   private:

	  XmtMenuItem *run_;
	  XmtMenuItem *pause_;
	  XmtMenuItem *mitsim_;
	  XmtMenuItem *tms_;
	  XmtMenuItem *meso_;
};

#endif
