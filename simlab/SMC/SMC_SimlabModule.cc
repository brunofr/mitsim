//-*-c++-*------------------------------------------------------------
// NAME: Source file for SMC_SimlabModule
// AUTH: Qi Yang
// FILE: SMC_SimlabModule.C
// DATE: Sun Feb 25 11:40:44 1996
//--------------------------------------------------------------------

#include <string.h>
#include <sstream>
using namespace std;

#include <Tools/ToolKit.h>
#include <Tools/Random.h>
#include <Tools/GenericVariable.h>
#include <Tools/Math.h>

#include <IO/Exception.h>
#include <IO/MessageTags.h>
#include <IO/Communicator.h>

#ifdef INTERNAL_GUI
#include <Xmw/XmwModeline.h>
#include "SMC_Interface.h"
#include "SMC_Menu.h"
#endif

#include "SMC_SimlabModule.h"
#include "SMC_ModuleInfo.h"
#include "SMC_Engine.h"
#include "SMC_SysInfo.h"

int theSpFlag = 0;

SMC_SimlabModule::SMC_SimlabModule(int id)
  : IOService(),
	id_(id),
	flag_(1 << (id - 1)),
	mode_(0),
	master_(NULL),
	host_(NULL),
	display_(NULL),
	cpuUsage_(0.0)
{
}


SMC_SimlabModule::~SMC_SimlabModule()
{
  if (master_) {
	free(master_);
	master_ = NULL;
  }
}


int SMC_SimlabModule::parse(GenericVariable &gv)
{
  SMC_ModuleInfo *p = theSysInfo->module(id_-1);
  int n = gv.nElements();

  if (n > 0 && !IsEmpty((const char *)gv.element(0))) {
	::Copy(&master_, gv.element(0));
	::Copy(&p->master_, master_);
  } else {
	::Copy(&master_, NULL);
	::Copy(&p->master_, NULL);
  }
  if (n > 1 && !IsEmpty((const char *)gv.element(1))) {
	::Copy(&host_, gv.element(1));
	::Copy(&p->host_, host_);
  } else {
	::Copy(&host_, NULL);
	::Copy(&p->host_, NULL);
  }
  if (n > 2 && !IsEmpty((const char *)gv.element(2))) {
	::Copy(&display_, gv.element(2));
	::Copy(&p->display_, display_);
  } else {
	::Copy(&display_, NULL);
	::Copy(&p->display_, NULL);
  }
  if (n > 3) {
	mode_ = gv.element(3);
	p->mode_ = mode_;
  }
  return 0;
}

void SMC_SimlabModule::parse(SMC_SysInfo *sysinfo)
{
  SMC_ModuleInfo *p = sysinfo->module(id_-1);
  ::Copy(&master_, p->master_);
  ::Copy(&host_, p->host_);
  ::Copy(&display_, p->display_);
  mode_ = p->mode_;
}


// Start the process at the given host. Pass command line args and
// options too.  Returns the process's task id.

int SMC_SimlabModule::lauch()
{
  if (!master_) {
	if (ToolKit::verbose()) {
	  cout << "No master file. " << name_ << " skipped." << endl;
	}
	unsetMode(MODE_HOLD);
	return 0;
  }

  int argc = 0;
  char *argv[8];

  // NAME OF THE MASTER FILE

  if (ToolKit::indir()) {
	argv[argc++] = StrCopy("-m=%s/%s",
						   ExpandEnvVars(ToolKit::indir()),
						   master_);
  } else {
	argv[argc++] = StrCopy("-m=%s", master_);
  }

  // DISPLAY ENVIRONMENT VARIABLE

  if (!IsEmpty(display())) {
	  argv[argc++] = StrCopy("-d:%s", ExpandEnvVars(display()));
  }

#ifdef INTERNAL_GUI
  if ( (flag() & FLAG_MDI) || (flag() & FLAG_TMCA) ) {
	unsetFlag(FLAG_HOLD);
  } else {
	if (mode(MODE_HOLD)) {
	  setFlag(FLAG_HOLD);
	} else {
	  unsetFlag(FLAG_HOLD);
	}
  }
#else
  unsetFlag(FLAG_HOLD);
#endif

  if (mode_) {
	argv[argc++] = StrCopy("-mode=%d", mode_);
  }

  if (theSpFlag && (flag() & (FLAG_MITSIM|FLAG_MESO))) {
	argv[argc++] = StrCopy("-sp=%d", theSpFlag);
  }

  if (Random::flags()) {
	argv[argc++] = StrCopy("-R=%d", Random::flags());
  }

  if (ToolKit::verbose() != ToolKit::VERBOSE_NORMAL) {
	argv[argc++] = StrCopy("-v=%d", ToolKit::verbose());
  }

  if (ToolKit::nice() != ToolKit::NICE_AUTOMATIC) {
	argv[argc++] = StrCopy("-nice=%d", ToolKit::nice());
  }

  if (!AproxEqual(NO_MSG_WAITING_TIME, DEFAULT_NO_MSG_WAITING_TIME)) {
	argv[argc++] = StrCopy("-t=%.2f", NO_MSG_WAITING_TIME);
  }

  argv[argc] = NULL;
   
  if (ToolKit::verbose()) {
	std::ostringstream os;
	for (int i = 0; i < argc; i ++) {
	  os << argv[i] << " ";
	}
	//os << ends;
	cout << "\nStarting <" << name_ << "> on "
		 << host() << " ...\n";
	cout << indent << name_ << " " << os.str() << endl;
//	delete [] os.str();
  }

  int f = 0;
  if (theEngine->debugger() & flag()) {
	f |= IOService::taskDebugFlag();
  }

  if (host()) {
	f |= IOService::taskHostFlag();
  }

  return IOService::spawn(argv, f, host());
}


// Receives the messages or wait for the message for a given period of
// time if no message in the queue (timeout receiving).  It returns
// msg id if any message has been received or 0 if no message has
// arrived.

int
SMC_SimlabModule::receiveMessages(double sec)
{

#ifdef INTERNAL_GUI
  theInterface->handlePendingEvents();
#endif
   
  int f = receive(sec);

  if (f <= 0) return f;

  unsigned int msg;
  *this >> msg;

  switch (msg) {

  case MSG_END_CYCLE:
	*this >> nextTime_ >> cpuUsage_;
	theEngine->unsetBusy(flag());
#ifdef INTERNAL_GUI
	theModeline()->updateMeter(id_, cpuUsage_);
#endif
	break;
#ifdef INTERNAL_GUI
  case MSG_PAUSE:
	theInterface->pause(others());
	break;

  case MSG_RESUME:
	unsetFlag(FLAG_HOLD);
	theInterface->run(others());
	break;
#else
  case MSG_PAUSE:
	break;

  case MSG_RESUME:
	unsetFlag(FLAG_HOLD);
	break;
#endif
  case MSG_QUIT:
	theException->exit(others());
	break;

  case MSG_DONE:
	theException->done(others());
	break;

  default:
	Communicator::skipUnknownMsg(name_, msg);
	break;
  }

  return msg;
}


// This function send the current time

void
SMC_SimlabModule::advanceClock()
{
  if (!isConnected() ||
	  nextTime_ >= theEngine->currentTime()) {
	return;
  }

  *this << MSG_CURRENT_TIME << theEngine->currentTime();
  flush(SEND_IMMEDIATELY);
  theEngine->setBusy(flag());
}


void
SMC_SimlabModule::sendCmd(int cmd)
{
  if (isConnected()) {
	*this << cmd;
	flush(SEND_IMMEDIATELY);
  }
}
