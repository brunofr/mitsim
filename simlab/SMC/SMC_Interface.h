//-*-c++-*------------------------------------------------------------
// SMC_Interface.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class definition for the class TS_Interface
//--------------------------------------------------------------------

#ifndef SMC_INTERFACE_HEADER
#define SMC_INTERFACE_HEADER

#include <Xmw/XmwInterface.h>
#include <Xmw/XmwCallback.h>

class SMC_Menu;
class SMC_Modeline;

class SMC_Interface : public XmwInterface
{
   public:

	  SMC_Interface(int *argc, char **argv);
	  ~SMC_Interface();

	  void create();
	  int mainloop();			// virtual

	  // overload the virtual function in base class

	  void updateButtons();
	  void pause (int others);
	  void run (int others);

	  SMC_Menu* menu() { return (SMC_Menu *) menu_; }
	  SMC_Modeline* modeline() { return (SMC_Modeline *) modeline_; }

   private:

	  XmwModeline* modeline(Widget parent);	// virtual
	  XmwMenu* menu(Widget parent);	// virtual
};

extern SMC_Interface *theInterface;

#endif
