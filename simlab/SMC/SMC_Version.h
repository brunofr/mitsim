//-*-c++-*------------------------------------------------------------
// NAME: Master controller for SIMLAB programs
// AUTH: Qi Yang
// FILE: SMC_Version.h
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#ifndef SMC_VERSION_HEADER
#define SMC_VERSION_HEADER

extern char*  g_majorVersionNumber;
extern char*  g_minorVersionNumber;
extern char*  g_codeDate;

extern void Welcome(char *);

#endif
