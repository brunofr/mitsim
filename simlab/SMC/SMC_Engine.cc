//-*-c++-*------------------------------------------------------------
// NAME: Source file for SMC_Engine
// AUTH: Qi Yang
// FILE: SMC_Engine.C
// DATE: Mon Nov  6 05:51:29 1995
//--------------------------------------------------------------------

#include <IO/IOService.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/param.h>

#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <Tools/Random.h>
#include <Tools/Math.h>

#ifdef INTERNAL_GUI
#include <Tools/Options.h>
#include <Xmt/Xmt.h>
#include <Xmt/Dialogs.h>
#include "SMC_Interface.h"
#include "SMC_Menu.h"
#endif

#include "SMC_Engine.h"
#include "SMC_FileManager.h"
#include "SMC_Exception.h"
#include "SMC_ModuleInfo.h"
#include "SMC_SysInfo.h"
#include <iostream>
using namespace std;

SMC_Engine * theEngine = NULL;

int SMC_Engine::debugger_ = 0;

SMC_Engine::SMC_Engine(int *argc, char **argv) :
   SimulationEngine(),
   SimulationClock(),
   argv_ (argv),
   busy_(FLAG_HOLD),
   batchTime_(DBL_INF),
   batchStepSize_(DBL_INF)
{
   theException = new SMC_Exception(*argv);
   theEngine = this;

   modules_[0] = new SMC_SimlabModule(1);
   modules_[1] = new SMC_SimlabModule(2);
   modules_[2] = new SMC_SimlabModule(3);
   modules_[3] = new SMC_SimlabModule(4);
   modules_[4] = new SMC_SimlabModule(5);

   if ((pid_ = IOService::pid()) < 0) {
#ifdef INTERNAL_GUI
	  theInterface->openUrl("daemon", "pvm.html");
#else
	  cerr << endl << "Error: SMC needs PVM support" << endl;
#endif
      quit(STATE_ERROR_QUIT);
   } else if (ToolKit::verbose()) {
	  cout << endl << "Service <" << hex << pid_ << dec
		   << "> assigned." << endl;
   }

   startTime_ = 86400.0;
   stopTime_ = -86400.0;
   stepSize_ = 1.0;

   mitsim().init("MITSIM", MSG_TAG_MC, MSG_TAG_MITSIM);

   tms().init("TMS", MSG_TAG_MC, MSG_TAG_TMS);

   meso().init("MesoTS", MSG_TAG_MC, MSG_TAG_MESO);

   mdi().init("MDI", MSG_TAG_MC, MSG_TAG_MDI);

   tmca().init("TMCA", MSG_TAG_MC, MSG_TAG_TMCA);
}


const char * SMC_Engine::ext()
{
   return ".smc";
}


// This function is called when application is done, closed or error
// occurs. It gives a last change to do the clean up tasks.

void
SMC_Engine::quit(int s)
{
#ifdef INTERNAL_GUI
   if (theInterface->menu()->isSaveNeeded()) {
	  Boolean yes;
	  XmtAskForBoolean(theInterface->widget(),
					   "isSaveNeeded", // reource querey name
					   "Save changes?", // defaul prompt
					   "Save",	// yes default
					   "Discard",	// no default
					   "Cancel",	// cancel default
					   XmtYesButton, // default button
					   0,	// default icon type
					   False, // show cancel button
					   &yes,
					   NULL);
	  if (yes) {
		 theInterface->menu()->save();
	  }
   }
   // next line seems cause problem on linux system
   // delete theInterface;
#endif

   // Called base class quit

   SimulationEngine::quit(s);
   if (state_ != STATE_DONE) {
      theException->exit(0xF);
   } else {
      theException->done(0xF);
   }
}

int SMC_Engine::isOnHold()
{
   if (busy(FLAG_HOLD)) {
	  if (mitsim().flag(FLAG_HOLD)) return 1;
	  if (tms().flag(FLAG_HOLD)) return 1;
	  if (meso().flag(FLAG_HOLD)) return 1;
	  if (mdi().flag(FLAG_HOLD)) return 1;
	  unsetBusy(FLAG_HOLD);
   }
   return 0;
}

void
SMC_Engine::run()
{
#ifdef INTERNAL_GUI

   theInterface->mainloop();

#else  // batch mode

   paused_ = 0;

   if (start() != 0) quit(STATE_ERROR_QUIT);

   while (state_ >= 0 && (receiveMessages() > 0 ||
						  (isWaiting() || isOnHold()) &&
						  receiveMessages(NO_MSG_WAITING_TIME) >= 0 ||
						  simulationLoop() >= 0)) {
      ;
   }

#endif // !INTERNAL_GUI

   quit(STATE_DONE);
}


int
SMC_Engine::loadMasterFile()
{
   // In batch mode the master file has to be loaded in order to
   // start the engine.

   int warning = SimulationEngine::loadMasterFile();
   if (warning) {
#ifdef INTERNAL_GUI
	  if (warning < 0) {
		 theInterface->inputFileNotFound(master());
	  }
#else
      cerr << "Error:: Cannot find the master file <"
		   << master() << ">." << endl;
      quit(STATE_ERROR_QUIT);
#endif
      return 1;
   }

   // Read master file here

   SMC_FileManager manager;
   manager.read(master());

   ToolKit::indir(theSysInfo->indir_);
   ToolKit::outdir(theSysInfo->outdir_);

   // if (::GetUserName()) theException->exit(1);
   // ::GetUserName();

#ifdef INTERNAL_GUI
   theOptions->setBreakPoints(nBreakPoints_, breakPoints_);
   theInterface->updateButtons();
#endif

   return 0;
}


int
SMC_Engine::start()
{
   SimulationEngine::start();

   // Open logfile

   IOService::logfile(stdout);

   startChildren();

   SimulationClock::init();		// reset the simulation clock

   return 0;
}


void
SMC_Engine::startChildren()
{
   // Initialize the child processes

   if (mitsim().display()) mitsim().name("xmitsim");
   else mitsim().name("mitsim");

   if (tms().display()) tms().name("xtms");
   else tms().name("tms");

   if (meso().display()) meso().name("xmeso");
   else meso().name("meso");

   mdi().name("mdi");
   tmca().name("tmca");

   // Start the child processes

   if (mitsim().lauch() < 0 || checkTimingData(mitsim()) < 0) {
      quit(STATE_ERROR_QUIT);
   }

   if (tms().lauch() < 0 || checkTimingData(tms()) < 0) {
      quit(STATE_ERROR_QUIT);
   }

   if (meso().lauch() < 0 || checkConnection(meso()) < 0) {
      quit(STATE_ERROR_QUIT);
   }

   if (mdi().lauch() < 0 || checkConnection(mdi()) < 0) {
      quit(STATE_ERROR_QUIT);
   }

   if (tmca().lauch() < 0 || checkConnection(tmca()) < 0) {
      quit(STATE_ERROR_QUIT);
   }


   if (stepSize_ < 1.0) {
	  stepSize_ = 1.0;
   }
   if (batchStepSize_ < 1.0) {
	  batchStepSize_ = 1.0;
   }
   currentTime_ = startTime_;

   // Announce the task IDs of the processes

   if (mitsim().isConnected()) {
      mitsim() << MSG_PROCESS_IDS << tms().tid();
      mitsim().flush(SEND_IMMEDIATELY);
   }

   if (tms().isConnected()) {
      tms() << MSG_PROCESS_IDS
	    << mitsim().tid() << meso().tid() << mdi().tid() << tmca().tid();
      tms().flush(SEND_IMMEDIATELY);
   }

   if (meso().isConnected()) {
      meso() << MSG_PROCESS_IDS << tms().tid();
      meso().flush(SEND_IMMEDIATELY);
   }

   if (mdi().isConnected()) {
      mdi() << MSG_PROCESS_IDS << tms().tid();
      mdi().flush(SEND_IMMEDIATELY);
   }

   if (tmca().isConnected()) {
      tmca() << MSG_PROCESS_IDS << tms().tid();
      tmca().flush(SEND_IMMEDIATELY);
   }


   batchTime_ = startTime_;
};


// Check the integrity of the timing data for the given module. If
// returns 1 if data is ok; negative int if error; or 0 if no data
// arrived.

int
SMC_Engine::checkTimingData(SMC_SimlabModule &module)
{
   static char first_time = 1;

   if (!module.isConnected()) return 1;	// no need to check

   if (ToolKit::verbose()) {
      cout << "Waiting timing data from "
		   << module.name() << endl;
   }

   int flag;

#ifdef INTERNAL_GUI

   // A blocking receive, but allows GUI events

   while ((flag = module.receive(NO_MSG_WAITING_TIME)) == 0) {
	 theInterface->handlePendingEvents();
	 theInterface->makeSnoozeEvent();
   }

#else

   // A blocking receive

   flag = module.receive(WAIT_MSG);

#endif

   if (flag <= 0) return flag;

   unsigned int head;
   
   module >> head;
   
   switch (head) {

      case MSG_TIMING_DATA:
      {
		 double start, stop, step, batch_step;
		 char wd[MAXPATHLEN+1];

		 module >> start >> stop >> step >> batch_step
				>> wd;
		 
		 if (first_time) {
			startTime_ = start;
			stopTime_ = stop;
			stepSize_ = step;
			batchStepSize_ = batch_step;
			ToolKit::workdir(wd);
			first_time = 0;
		 } else if (!AproxEqual(start, startTime_) ||
			    !AproxEqual(stop, stopTime_)) {
			cerr << "Error:: Start or stop time of "
				 << module.name()
				 << " does not match previous process." << endl;
			quit(STATE_ERROR_QUIT);
			return -1;
		 } else if (Cmp(ToolKit::workdir(), wd)) {
		   cerr << "Error:: Working directory <"
				<< wd << "> of "
				<< module.name()
				<< " does not match <"
				<< ToolKit::workdir() << ">." << endl;
		   quit(STATE_ERROR_QUIT);
		   return -1;
		 } else {
			stepSize_ = Min(stepSize_, step);
			batchStepSize_ = Min(batchStepSize_, batch_step);			
		 }

		 if (ToolKit::verbose()) {
			cout << "Received timing data from "
				 << module.name() << "." << endl;
		 }
		 return 1;
      }
      case MSG_QUIT:
      {
		 quit(STATE_QUIT);
		 return -1;
      }
      default:
      {
		 cout << "Warning:: Unknown message <"
			  << head << "> received from "
			  << module.name() << "." << endl;
		 break;
      }
   }
   return 0;
}


int
SMC_Engine::checkConnection(SMC_SimlabModule &module)
{
   if (!module.isConnected()) return 1;	// no need to check

   if (ToolKit::verbose()) {
      cout << "Check connection with "
		   << module.name() << endl;
   }

   int flag;

#ifdef INTERNAL_GUI

   // A blocking receive, but allows GUI events

   while ((flag = module.receive(NO_MSG_WAITING_TIME)) == 0) {
	 theInterface->handlePendingEvents();
	 theInterface->makeSnoozeEvent();
   }

#else

   // A blocking receive

   flag = module.receive(WAIT_MSG);

#endif

   if (flag <= 0) return flag;

   unsigned int head;

   module >> head;
   
   switch (head) {

      case MSG_TIMING_DATA:
      {
		char wd[MAXPATHLEN+1];
		module >> wd;
		if (Cmp(ToolKit::workdir(), wd)) {
		  cerr << "Error:: Working directory <"
			   << wd << "> of "
			   << module.name()
			   << " does not match <"
			   << ToolKit::workdir() << ">." << endl;
		  quit(STATE_ERROR_QUIT);
		  return -1;
		}
		if (ToolKit::verbose()) {
		  cout << "Connection with "
			   << module.name() << " is good." << endl;
		}
		return 1;
      }
      case MSG_QUIT:
      {
		 quit(STATE_QUIT);
		 return -1;
      }
      default:
      {
		 cout << "Warning:: Unknown message <"
			  << head << "> received from "
			  << module.name() << "." << endl;
		 break;
      }
   }
   return 0;
}


void
SMC_Engine::pauseSimulation(int others)
{
   if (!isPaused()) {
	  SimulationClock::pause();
   }
   if (others & FLAG_MITSIM) mitsim().pause();
   if (others & FLAG_TMS) tms().pause();
   if (others & FLAG_MESO) meso().pause();
   if (others & FLAG_MDI) mdi().pause();
   if (others & FLAG_TMCA) tmca().pause();
}


void
SMC_Engine::resumeSimulation(int others)
{
   if (isPaused()) {
	  SimulationClock::resume();
   }
   if (others & FLAG_MITSIM) mitsim().resume();
   if (others & FLAG_TMS) tms().resume();
   if (others & FLAG_MESO) meso().resume();
   if (others & FLAG_MDI) mdi().resume();
   if (others & FLAG_TMCA) tmca().resume();
}

void SMC_Engine::setupModules(SMC_SysInfo *info)
{
   mitsim().parse(info);
   tms().parse(info);
   meso().parse(info);
   mdi().parse(info);
   tmca().parse(info);
}
