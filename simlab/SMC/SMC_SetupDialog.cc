//-*-c++-*------------------------------------------------------------
// SMC_SetupDialog.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved`
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <cassert>
#include <iostream>
using namespace std;

#include <Xmt/Xmt.h>
#include <Xmt/InputField.h>
#include <Xmt/Dialogs.h>

#include <pvm3.h>

#include <Tools/ToolKit.h>
#include <IO/Exception.h>

#include "SMC_SetupDialog.h"
#include "SMC_SysInfo.h"
#include "SMC_Interface.h"
#include "SMC_Engine.h"
#include "SMC_Menu.h"


SMC_SetupDialog::SMC_SetupDialog(Widget parent)
   : XmwDialogManager(parent, "setupDialog", NULL, 0)
{
   XtVaSetValues(widget_,
				 XmNdialogStyle, XmDIALOG_PRIMARY_APPLICATION_MODAL,
				 NULL);

   // Find the widget for the title
   
   title_ = XmtNameToWidget (widget_, "title");
   assert(title_);
   XmtInputFieldSetString(title_, theSysInfo->title_);

   // Input directory

   indir_ = XmtNameToWidget (widget_, "*indir");
   assert(indir_);
   XmtInputFieldSetString(indir_, theSysInfo->indir_);

   // Output directory

   outdir_ = XmtNameToWidget (widget_, "*outdir");
   assert (outdir_);
   XmtInputFieldSetString(outdir_, theSysInfo->outdir_);

   // Find the tab widget

   Widget tabs = XmtNameToWidget (widget_, "*tabs");
   assert(tabs);

  // Get the hosts in the virtual machine
   
  int nhost, narch;
  struct pvmhostinfo *info;

  if (pvm_config(&nhost, &narch,  &info) < 0) {
	XmtDisplayErrorMsg(widget_, NULL,
					   "Cannot get host info of the virtual machine.",
					   "Error",
					   "Start PVM daemon and try again."
					   );
  }

  // Create all pages

  for (int i = 0; i < theSysInfo->nModules(); i ++) {
	theSysInfo->modules_[i]->createUI(tabs, info, nhost);
  }
}


// These overload the virtual functions defined in the based class

void SMC_SetupDialog::okay(Widget, XtPointer, XtPointer)
{
  if (theSysInfo->title_) delete [] theSysInfo->title_;
  theSysInfo->title_ = ::Copy(XmtInputFieldGetString(title_));
  
  if (theSysInfo->indir_) delete [] theSysInfo->indir_;
  theSysInfo->indir_ = ::Copy(XmtInputFieldGetString(indir_));

  if (theSysInfo->outdir_) delete [] theSysInfo->outdir_;
  theSysInfo->outdir_ = ::Copy(XmtInputFieldGetString(outdir_));

  // Save the data for all module
   
  for (int i = 0; i < theSysInfo->nModules(); i ++) {
	theSysInfo->modules_[i]->transferData();
  }
  theEngine->setupModules(theSysInfo);

  XtVaSetValues(theInterface->widget(),
				XmNtitle, theSysInfo->title_,
				NULL);
  theInterface->menu()->activate(theInterface->menu()->save_);
  theInterface->menu()->needSave();

  // Had problem on linux and IRIX 5.3
  // unmanage( widget_ );
  
  // This works but with cost.
  if (ptr_to_handler_) *ptr_to_handler_ = NULL;
  delete this;
}

void SMC_SetupDialog::cancel(Widget, XtPointer, XtPointer)
{
   // Had problem on linux and IRIX 5.3
   // unmanage( widget_ );

   // This works but with cost.
   if (ptr_to_handler_) *ptr_to_handler_ = NULL;
   delete this;
}

void SMC_SetupDialog::help(Widget, XtPointer, XtPointer)
{
   theInterface->openUrl("setup", "smc.html");
}

void SMC_SetupDialog::activate()
{
   XmwDialogManager::activate(okay_);
   XmwDialogManager::activate(title_);
   XmwDialogManager::activate(indir_);
   XmwDialogManager::activate(outdir_);

   for (int i = 0; i < theSysInfo->nModules(); i ++) {
	 theSysInfo->modules_[i]->activate();
   }
}

void SMC_SetupDialog::deactivate()
{
   XmwDialogManager::deactivate(okay_);

   XmwDialogManager::deactivate(title_);
   XmwDialogManager::deactivate(indir_);
   XmwDialogManager::deactivate(outdir_);

   for (int i = 0; i < theSysInfo->nModules(); i ++) {
	 theSysInfo->modules_[i]->activate();
   }
}

void SMC_SetupDialog::post_and_wait(SMC_SetupDialog **ptr)
{
   ptr_to_handler_ = ptr;

   if (theEngine->isStarted()) {
	  deactivate();
   } else {
	  activate();
   }
   XmwDialogManager::post();
}

#endif // INTERNAL_GUI
