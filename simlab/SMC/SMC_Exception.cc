//-*-c++-*------------------------------------------------------------
// NAME: Exception handler
// AUTH: Qi Yang
// FILE: SMC_Exception.C
// DATE: Sun Mar 17 14:25:46 1996
//--------------------------------------------------------------------


#include "SMC_Exception.h"
#include "SMC_Engine.h"


void
SMC_Exception::exit(int code)
{
   // Terminate all child processes.

   if (theEngine) {
	  if (code & 0x1) {
		 theEngine->mitsim().sendCmd(MSG_QUIT);
	  }
	  if (code & 0x2) {
		 theEngine->tms().sendCmd(MSG_QUIT);
	  }
	  if (code & 0x4) {
		 theEngine->meso().sendCmd(MSG_QUIT);
	  }
   }
   Exception::exit(code);
}


void
SMC_Exception::done(int code)
{
   // Terminate all child processes.

   if (theEngine) {
	  if (code & 0x1) {
		 theEngine->mitsim().sendCmd(MSG_DONE);
	  }
	  if (code & 0x2) {
		 theEngine->tms().sendCmd(MSG_DONE);
	  }
	  if (code & 0x4) {
		 theEngine->meso().sendCmd(MSG_DONE);
	  }
   }
   Exception::done(code);
}
