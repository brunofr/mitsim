//-*-c++-*------------------------------------------------------------
// SMC_SysInfo.h
//
// qiyang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef SMC_SYSINFO_HEADER
#define SMC_SYSINFO_HEADER

#include <iostream>
#include <vector>

#include "SMC_ModuleInfo.h"

class SMC_SysInfo
{
	  friend class SMC_SetupDialog;
	  friend class SMC_FileManager;
	  friend class SMC_Engine;
	  friend class SMC_Interface;

   public:

	  SMC_SysInfo(char *title=NULL, char *indir=NULL, char *outdir=NULL);
	  ~SMC_SysInfo();

	  inline int nModules() { return modules_.size(); }
	  SMC_ModuleInfo *module(int i) { return modules_[i]; }
	  void print(std::ostream &os = std::cout);

	  void title(const char *value);
	  void indir(const char *value);
	  void outdir(const char *value);

   private:

	  char *title_;
	  char *indir_;
	  char *outdir_;
	  std::vector<SMC_ModuleInfo*> modules_;
};

extern SMC_SysInfo *theSysInfo;

#endif
