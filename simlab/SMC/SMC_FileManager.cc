//-*-c++-*------------------------------------------------------------
// NAME: Parser for Simlab master file
// AUTH: Qi Yang
// FILE: SMC_FileManager.C
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#include <string>

#include <Tools/ToolKit.h>

// This file is generated from VariableParser.y

#include <Tools/VariableParser.h>

#include <IO/MessageTags.h>

#include "SMC_Engine.h"
#include "SMC_SimlabModule.h"
#include "SMC_FileManager.h"
#include "SMC_SysInfo.h"
#include <iostream>
using namespace std;

// Read variables from master file.

void
SMC_FileManager::read(const char *filename)
{
   VariableParser vp(this);
   vp.parse(filename);
}


// Check if two token are equal.

int SMC_FileManager::isEqual(const char *s1, const char *s2)
{
   return IsEqual(s1, s2, 1);
}


// This function returns 0 if the variable is parsed or 1 if it is
// skipped (in case command line provides the value), or -1 if error
// occurs.

int SMC_FileManager::parseVariable(char ** varptr, GenericVariable &gv)
{
   int error;
   if (*varptr == NULL || strlen(*varptr) <= 0) {
      *varptr = Copy(gv.string());
      error = 0;
   } else if (ToolKit::verbose()) {
      cout << "Msg:: " << gv.name() << " = <"
	   << gv.string() << "> replaced by <"
	   << *varptr << ">." << endl;
      error = 1;
   }
   return error;
}


// All the variables provided in the master file should be parsed in
// this function.

int
SMC_FileManager::parseVariable(GenericVariable &gv) 
{
   char * token = compact(gv.name());

   if (isEqual(token, "Title")) {
	  return parseVariable(&theSysInfo->title_, gv);
   } else if (isEqual(token, "InputDirectory")) {
      return parseVariable(&theSysInfo->indir_, gv);
   } else if (isEqual(token, "OutputDirectory")) {
      return parseVariable(&theSysInfo->outdir_, gv);

   } else if (isEqual(token, "MITSIM")) {
      return theEngine->mitsim().parse(gv);
   } else if (isEqual(token, "TMS")) {
      return theEngine->tms().parse(gv);
   } else if (isEqual(token, "MesoTS")) {
      return theEngine->meso().parse(gv);
   } else if (isEqual(token, "MDI")) {
      return theEngine->mdi().parse(gv);
   } else if (isEqual(token, "TMCA")) {
      return theEngine->tmca().parse(gv);

   } else if (isEqual(token, "Misc")) {
      return theEngine->mdi().parse(gv);

   } else if (isEqual(token, "BreakPoints")) {
	 return theEngine->parseBreakPoints(gv);

   } else if (isEqual(token, "Timeout")) {
      NO_MSG_WAITING_TIME = gv.element();

   } else if (ToolKit::verbose()) {
      return 1;
   }
   return 0;
}

