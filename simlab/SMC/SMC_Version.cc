//-*-c++-*------------------------------------------------------------
// NAME: Master controller for SIMLAB programs
// AUTH: Qi Yang
// FILE: SMC_Version.C
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#include <new>
#include <iostream>
using namespace std;

#include <Tools/ToolKit.h>

#include "SMC_Exception.h"
#include "SMC_Version.h"

char*  g_majorVersionNumber = "1";
char*  g_minorVersionNumber = "0 Beta";
char*  g_codeDate           = "1996";

void Welcome(char *exec)
{
   theExec = Copy(ToolKit::RealPath(exec));

   cout << "SMC Release "
	<< g_majorVersionNumber << "." << g_minorVersionNumber << endl
	<< "Copyright (c) " << g_codeDate << endl
	<< "Massachusetts Institute of Technology" << endl
	<< "All Right Reserved" << endl << endl;

#ifndef FINAL_VERSION
   PrintVersionInfo();
#endif

   cout.flush();

   set_new_handler(::FreeRamException);
}
