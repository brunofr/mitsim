//-*-c++-*------------------------------------------------------------
// SMC_SysInfo.cc
//
// qiyang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <string>

#include <Tools/ToolKit.h>
#include <Tools/Options.h>

#include "SMC_SysInfo.h"
using namespace std;

SMC_SysInfo *theSysInfo = new SMC_SysInfo;

SMC_SysInfo::SMC_SysInfo(char *title, char *indir, char *outdir)
{
   if (title) {
	  title_ = strdup(title);
   } else {
	  title_ = NULL;
   }
   if (indir) {
	  indir_ = strdup(indir);
   } else {
	  indir_ = NULL;
   }
   if (outdir) {
	  outdir_ = strdup(outdir);
   } else {
	  outdir_ = NULL;
   }
   modules_.push_back(
	  new SMC_ModuleInfo(
		 "MITSIM",
		 "master.mitsim")
	  );
   modules_.push_back(
	  new SMC_ModuleInfo(
		 "TMS",
		 "master.tms")
	  );
   modules_.push_back(
	  new SMC_ModuleInfo(
		 "MesoTS")
	  );
   modules_.push_back(
	  new SMC_ModuleInfo(
		 "Misc")
	  );
}

SMC_SysInfo::~SMC_SysInfo()
{
   if (title_) free(title_);
   if (indir_) free(indir_);
   if( outdir_) free(outdir_);
   for (int i = 0; i < nModules(); i ++) {
	  delete modules_[i];
   }
}

void SMC_SysInfo::title(const char *value)
{
   ::Copy(&title_, value);
}

void SMC_SysInfo::indir(const char *value)
{
   ::Copy(&indir_, value);
}

void SMC_SysInfo::outdir(const char *value)
{
   ::Copy(&outdir_, value);
}

void SMC_SysInfo::print(ostream &os)
{
   os << "# Simlab master file\n\n";
   os << "[Title] = \"" << NoNull(title_) << "\"" << endl;
   os << "[Input Directory] = \"" << NoNull(indir_) << "\"" << endl;
   os << "[Output Directory] = \"" << NoNull(outdir_) << "\"" << endl;
   os << endl;
   for (int i = 0; i < nModules(); i ++) {
	  modules_[i]->print(os);
   }
   theOptions->print(os);
   os << endl;
   PrintVersionInfo(os);
}
