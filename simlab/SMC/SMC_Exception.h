//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// NOTE: 
// AUTH: Qi Yang
// FILE: SMC_Exception.h
// DATE: Sun Mar 17 14:30:41 1996
//--------------------------------------------------------------------

#ifndef SMC_EXCEPTION_HEADER
#define SMC_EXCEPTION_HEADER

#include <IO/Exception.h>

class SMC_Exception : public Exception
{
   public:

      SMC_Exception(const char *name) : Exception(name) {
	  }
      ~SMC_Exception() { }

      void exit(int code = 7);	// virtual
      void done(int code = 7);	// virtual
};

#endif
