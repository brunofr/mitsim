//-*-c++-*------------------------------------------------------------
// NAME: Parse command line arguments for SIMLAB
// AUTH: Qi Yang
// FILE: CmdArgsParser.C
// DATE: Fri Nov 17 17:28:06 1995
//--------------------------------------------------------------------

#include <Tools/ToolKit.h>

#include <IO/MessageTags.h>

#include "SMC_CmdArgsParser.h"
#include "SMC_Engine.h"
#include "SMC_SimlabModule.h"

SMC_CmdArgsParser::SMC_CmdArgsParser()
  : CmdArgsParser()
{
  char *doc;
  doc = StrCopy("Routing options for guided drivers:\n"
				"%6X = Time variant\n"
				"%6X = Update shortest path tree\n"
				"%6X = Update path table info\n"
				"%6X = Use existing tables if available\n"
				"%6X = Updated travel time available for pretrip planning",
				INFO_FLAG_DYNAMIC,
				INFO_FLAG_UPDATE_TREES,
				INFO_FLAG_UPDATE_PATHS,
				INFO_FLAG_USE_EXISTING_TABLES,
				INFO_FLAG_AVAILABILITY);

   add(new Clo("-sp", &theSpFlag, 0, doc));
   theSpFlag = INFO_FLAG_DYNAMIC | INFO_FLAG_UPDATE_PATHS;

   add(new Clo("-debugger", &(SMC_Engine::debugger_), 0,
			   "debugging (1=MITSIM 2=TMS 4=MesoTS 8=Other)"));
}
