//-*-c++-*------------------------------------------------------------
// SMC_Modeline.h
//
// qiyang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef SMC_MODELINE_HEADER
#define SMC_MODELINE_HEADER

#include <Xmw/XmwModeline.h>

class SMC_Modeline : public XmwModeline
{
  public:

	SMC_Modeline(Widget parent);
	~SMC_Modeline() { }

	void keyStateChanged(enum KeyState state); // virtual

	void updateButtons();
};

#endif
