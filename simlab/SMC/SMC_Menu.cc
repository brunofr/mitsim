//-*-c++-*------------------------------------------------------------
// SMC_Menu.cc
//
// qiyang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <fstream>
#include <sys/param.h>
using namespace std;

#include <Xmt/Xmt.h>
#include <Xmt/Create.h>
#include <Xmt/Menu.h>
#include <Xmt/Dialogs.h>
#include <Xmt/Dialog.h>

#include <IO/Exception.h>
#include <Tools/ToolKit.h>
#include <Tools/Options.h>

#include <Xmw/XmwModeline.h>
#include <Xmw/XmwOptionDialog.h>
#include <Xmw/XmwRandomizer.h>

#include "SMC_Menu.h"
#include "SMC_SetupDialog.h"
#include "SMC_Interface.h"
#include "SMC_Engine.h"
#include "SMC_SysInfo.h"

SMC_Menu::SMC_Menu(Widget parent)
  : XmwMenu(parent)
{
  Widget w;
   
  // Simulation pane

  if (run_ = getItem("Run")) {
	addCallback(getItemWidget(run_),
				XmNactivateCallback, &SMC_Menu::run,
				NULL );
	deactivate(run_);
  }

  if (pause_ = getItem("Pause")) {
	addCallback(getItemWidget(pause_),
				XmNactivateCallback, &SMC_Menu::pause,
				NULL );
	deactivate(pause_);
  }

  if (w = getItemWidget("Setup")) {
	addCallback(w,
				XmNactivateCallback, &SMC_Menu::setup,
				NULL );
  }

  if (w = getItemWidget("Option")) {
	addCallback(w,
				XmNactivateCallback, &SMC_Menu::option,
				NULL );
  }

  // View pane

  if (mitsim_ = getItem("MITSIM")) {
	addCallback(getItemWidget(mitsim_),
				XmNvalueChangedCallback, &SMC_Menu::mitsim,
				NULL );
	XtVaSetValues(getItemWidget(mitsim_), XmNindicatorOn, True, NULL);
	setToggleState(mitsim_, False);
	deactivate(mitsim_);
  }   

  if (tms_ = getItem("TMS")) {
	addCallback(getItemWidget(tms_),
				XmNvalueChangedCallback, &SMC_Menu::tms,
				NULL );
	XtVaSetValues(getItemWidget(tms_), XmNindicatorOn, True, NULL);
	setToggleState(tms_, False);
	deactivate(tms_);
  }

  if (meso_ = getItem("MesoTS")) {
	addCallback(getItemWidget(meso_),
				XmNvalueChangedCallback, &SMC_Menu::meso,
				NULL );
	XtVaSetValues(getItemWidget(meso_), XmNindicatorOn, True, NULL);
	setToggleState(meso_, False);
	deactivate(meso_);
  }   
}

// Set initial state

void SMC_Menu::updateButtons()
{
  if (theEngine->isStarted()) {
	deactivate(open_);
  } else {
	activate(open_);
  }

  if (theEngine->mitsim().display() && theEngine->isStarted()) {
	activate(mitsim_);
	setToggleState(mitsim_, True);
	setState(SIM_WINDOW_MITSIM);
  } else {
	deactivate(mitsim_);
	setToggleState(mitsim_, False);
	unsetState(SIM_WINDOW_MITSIM);
  }
  if (theEngine->tms().display() && theEngine->isStarted()) {
	activate(tms_);
	setToggleState(tms_, True);
	setState(SIM_WINDOW_TMS);
  } else {
	deactivate(tms_);
	setToggleState(tms_, False);
	unsetState(SIM_WINDOW_TMS);
  }
  if (theEngine->meso().display() && theEngine->isStarted()) {
	activate(meso_);
	setToggleState(meso_, True);
	setState(SIM_WINDOW_MESO);
  } else {
	deactivate(meso_);
	setToggleState(meso_, False);
	unsetState(SIM_WINDOW_MESO);
  }

  updateRunningState(theEngine->isRunning());
}

void SMC_Menu::updateRunningState(int running)
{
  if (running) {
	activate(pause_);
	deactivate(run_);
  } else {
	activate(run_);
	deactivate(pause_);
  }
}

void SMC_Menu::open ( Widget w, XtPointer, XtPointer)
{
  char filename[MAXPATHLEN + 1];
  char directory[MAXPATHLEN + 1];

  filename[0] = '\0';
  directory[0] = '\0';

  while (XmtAskForFilename(w,
						   "openFileDialog", 
						   "Please type or select a master file:",
						   ToolKit::inDir(), NULL,
						   filename, sizeof(filename),
						   directory, sizeof(directory),
						   "*.smc", 0,
						   NULL) == True) {
	theEngine->master(filename);
	if (theEngine->loadMasterFile() == 0) {
	  break;
	}
  }
  theInterface->updateButtons();
}

// Return error code

int SMC_Menu::save ()
{
  ofstream os(theEngine->master());
  if (os.good()) {
	theInterface->msgSet("Save ");
	theInterface->msgAppend(theEngine->master());
	theSysInfo->print(os);
	theInterface->msgAppend(" done.");
	deactivate(save_);
	unsetState(STATE_SAVE);
	os.close();
	theInterface->updateButtons();
	return 0;
  } else {
	theInterface->outputFileError(theEngine->master());
	return 1;
  }
}

void SMC_Menu::save ( Widget w, XtPointer tag, XtPointer data)
{
  if (!theEngine->master() || save() != 0) {
	saveAs (w, tag, data);
  }
}

void SMC_Menu::saveAs ( Widget w, XtPointer, XtPointer)
{
  char filename[MAXPATHLEN + 1];
  char directory[MAXPATHLEN + 1];
  char pattern[MAXPATHLEN + 1];

  filename[0] = '\0';
  directory[0] = '\0';
  strcpy(pattern, "*");
  strcat(pattern, theSimulationEngine->ext());

  Boolean done = 0;

  while (!done &&
		 XmtAskForFilename(w, "openFileDialog", 
						   "Please type or select a master file name:",
						   ToolKit::inDir(), NULL,
						   filename, sizeof(filename),
						   directory, sizeof(directory),
						   pattern, 0,
						   NULL)) {
	if (ToolKit::fileExists(filename)) {
	  XmtAskForBoolean(w, "confirmFileOverwrite", // reource querey name
					   "File already exists!", // defaul prompt
					   "Overwrite",	// yes default
					   "No",	// no default
					   "Cancel",	// cancel default
					   XmtNoButton, // default button
					   0,	// default icon type
					   False, // show cancel button
					   &done,
					   NULL);
	} else {
	  done = True;
	}
	if (done) {
	  theEngine->master(filename);
	  if (save() == 0) done = True;
	  else done = False;
	}
  }
}

void SMC_Menu::print ( Widget, XtPointer, XtPointer)
{
  theSysInfo->print();
}

void SMC_Menu::quit ( Widget, XtPointer, XtPointer )
{
  theEngine->quit(STATE_QUIT);
}

void SMC_Menu::run ( Widget, XtPointer, XtPointer )
{
  theInterface->run(FLAG_ALL);
}

void SMC_Menu::pause ( Widget, XtPointer, XtPointer )
{
  theInterface->pause(FLAG_ALL);
}


void SMC_Menu::randomizer ( Widget, XtPointer, XtPointer )
{
  static XmwRandomizer dialog(theInterface->widget(), 4);
  dialog.post();
}

void SMC_Menu::setup ( Widget, XtPointer, XtPointer )
{
  // Bacause of a problem in ComboBox widget on some machine.  We
  // create this dialog box each time it is posted and destroy it
  // when Ok or Cancel button is pressed.

  static SMC_SetupDialog *dialog = NULL;
  dialog = new SMC_SetupDialog(theInterface->widget());
  dialog->post_and_wait(&dialog);
}

void SMC_Menu::option ( Widget, XtPointer, XtPointer )
{
  static XmwOptionDialog dialog(theInterface->widget());
  dialog.post();
}

void SMC_Menu::mitsim ( Widget, XtPointer, XtPointer )
{
  if (!theEngine->mitsim().isConnected()) return;

  theInterface->msgSet("MITSIM ");
  if (state(SIM_WINDOW_MITSIM)) {
	theEngine->mitsim() << MSG_WINDOW_HIDE;
	theEngine->mitsim().flush(SEND_IMMEDIATELY);
	unsetState(SIM_WINDOW_MITSIM);
	theInterface->msgAppend("hiden");
  } else {
	theEngine->mitsim() << MSG_WINDOW_SHOW;
	theEngine->mitsim().flush(SEND_IMMEDIATELY);
	setState(SIM_WINDOW_MITSIM);
	theInterface->msgAppend("shown");
  }
}

void SMC_Menu::tms ( Widget, XtPointer, XtPointer )
{
  if (!theEngine->tms().isConnected()) return;

  theInterface->msgSet("TMS ");
  if (state(SIM_WINDOW_TMS)) {
	theEngine->tms() << MSG_WINDOW_HIDE;
	theEngine->tms().flush(SEND_IMMEDIATELY);
	unsetState(SIM_WINDOW_TMS);
	theInterface->msgAppend("hiden");
  } else {
	theEngine->tms() << MSG_WINDOW_SHOW;
	theEngine->tms().flush(SEND_IMMEDIATELY);
	setState(SIM_WINDOW_TMS);
	theInterface->msgAppend("shown");
  }
}

void SMC_Menu::meso ( Widget, XtPointer, XtPointer )
{
  if (!theEngine->meso().isConnected()) return;

  theInterface->msgSet("MesoTS ");
  if (state(SIM_WINDOW_MESO)) {
	theEngine->meso() << MSG_WINDOW_HIDE;
	theEngine->meso().flush(SEND_IMMEDIATELY);
	unsetState(SIM_WINDOW_MESO);
	theInterface->msgAppend("hiden");
  } else {
	theEngine->meso() << MSG_WINDOW_SHOW;
	theEngine->meso().flush(SEND_IMMEDIATELY);
	setState(SIM_WINDOW_MESO);
	theInterface->msgAppend("shown");
  }
}

#endif // INTERNAL_GUI
