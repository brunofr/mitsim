//-*-c++-*------------------------------------------------------------
// NAME: Header file for SMC_SimlabModule
// AUTH: Qi Yang
// FILE: SMC_SimlabModule.h
// DATE: Sun Feb 25 11:39:59 1996
//--------------------------------------------------------------------

#ifndef SMC_SIMLABMODULE_HEADER
#define SMC_SIMLABMODULE_HEADER

#include <IO/IOService.h>
#include <IO/MessageTags.h>

const unsigned int FLAG_ALL    = 0x001F; // sum
const unsigned int FLAG_MITSIM = 0x0001;
const unsigned int FLAG_TMS    = 0x0002;
const unsigned int FLAG_MESO   = 0x0004;
const unsigned int FLAG_MDI    = 0x0008;
const unsigned int FLAG_TMCA   = 0x0010;
;
const unsigned int FLAG_HOLD   = 0x1000;

//--------------------------------------------------------------------
// CLASS NAME: SMC_SimlabModule
// After creating an object of this class, the base class function:
//
//   int init(char *module, int stag, int rtag,
//            int msg_size = DEFAULT_MSG_SIZE);
//
// must be called to initialize the object and function.  Then we
// start the module on a particular host (or type of host) by calling
// function (see PVM manual for flag definition):
//
//   int spawn(char **argv, int flag = PvmTaskDefault,
//             char *where = "");
//
// or attach the module to an process already running on the virtual
// machine.
//--------------------------------------------------------------------

class GenericVariable;
class SMC_SysInfo;

class SMC_SimlabModule : public IOService
{
      friend void ParseCmdArgs(int, char **);
      friend class SMC_FileManager;
      friend class SMC_Engine;

   protected:
       
      double nextTime_;			// schedule time for next run

	  int id_;					// id of the module
	  unsigned int flag_;
	  unsigned int mode_;
      double cpuUsage_;			// cpu usage for last step
      
      char* master_;			// name of master file
	  char* host_;
	  char* display_;

   public:

      SMC_SimlabModule(int id);
      ~SMC_SimlabModule();

	  inline unsigned int others() { return ~flag_ & FLAG_ALL; }
	  inline unsigned int flag(unsigned int mask = 0xFFFFFFFF) {
		 return (flag_ & mask);
	  }
	  inline void unsetFlag(unsigned int s) {
		 flag_ &= ~s;
	  }
	  inline void setFlag(unsigned int s) {
		 flag_ |= s;
	  }
      
	  inline unsigned int mode(unsigned int mask = 0xFFFFFFFF) {
		 return (mode_ & mask);
	  }
	  inline void unsetMode(unsigned int s) {
		 mode_ &= ~s;
	  }
	  inline void setMode(unsigned int s) {
		 mode_ |= s;
	  }

      inline char * master() { return master_; }

	  char * host() { return host_; }
	  char * display() { return display_; }

	  int parse(GenericVariable &);
	  void parse(SMC_SysInfo *sysinfo);

	  int lauch();

	  // Next scheduled time for activating this module.

	  inline double nextTime()  { return nextTime_; }
      
	  // Returns 0 if no message

	  int receiveMessages(double sec = 0.0);

	  void advanceClock();

	  void sendCmd(int cmd);

	  void pause() { sendCmd(MSG_PAUSE); }
	  void resume() { sendCmd(MSG_RESUME); }
};

extern int theSpFlag;

#endif
