//-*-c++-*------------------------------------------------------------
// NAME: Explicitly instantiate all the template instances we use
// AUTH: Qi Yang
// FILE: fiTemplates.C
// DATE: Thu Apr 18 23:17:31 1996
//--------------------------------------------------------------------

#ifdef FORCE_INSTANTIATE

#include "SMC_ModuleInfo.h"

#ifdef INTERNAL_GUI
#include <Xmw/XmwSymbols.h>
#endif // INTERNAL_GUI

#include <vector>

#ifdef __sgi

#pragma instantiate vector <SMC_ModuleInfo*>

#ifdef INTERNAL_GUI
#pragma instantiate XmwSymbol <char>
#endif // INTERNAL_GUI

#endif

#ifdef __GNUC__

template class std::vector <SMC_ModuleInfo*>;

#ifdef INTERNAL_GUI
template class XmwSymbol <char>;
#endif // INTERNAL_GUI

#endif  // __GNUC__

#endif
