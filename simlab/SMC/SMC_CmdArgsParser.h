//-*-c++-*------------------------------------------------------------
// SMC_CmdArgsParser.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef SMC_CMDARGSPARSER_HEADER
#define SMC_CMDARGSPARSER_HEADER

#include <Tools/CmdArgsParser.h>

class SMC_CmdArgsParser : public CmdArgsParser
{
   public:
      
      SMC_CmdArgsParser();
      ~SMC_CmdArgsParser() { }
};

#endif
