#include <Xmt/Xmt.h>
#include <Xmt/Layout.h>
#include <Xmt/Menu.h>    
#include <Xm/DrawingA.h>

/* dummy procedures to call */
static void NewFile(/*Widget w, XtPointer tag, XtPointer data*/) {}
static void OpenFile(/*Widget w, XtPointer tag, XtPointer data*/) {}
static void SaveFile(/*Widget w, XtPointer tag, XtPointer data*/) {}
static void SaveAs(/*Widget w, XtPointer tag, XtPointer data*/) {}
static void Quit(/*Widget w, XtPointer tag, XtPointer data*/) {}
static void About(/*Widget w, XtPointer tag, XtPointer data*/) {}
static void ContextHelp(/*Widget w, XtPointer tag, XtPointer data*/) {}
static void move(/*Widget w, XtPointer tag, XtPointer data*/) {}
static void rotate(/*Widget w, XtPointer tag, XtPointer data*/) {}
static void scale(/*Widget w, XtPointer tag, XtPointer data*/) {}
static void toggle_grid(/*Widget w, XtPointer tag, XtPointer data*/) {}

static XmtMenuItem file_menu_items[] = {
    {XmtMenuItemPushButton, "New", 'N', "Meta<Key>N", "Meta+N", NewFile },
    {XmtMenuItemPushButton, "Open...", 'O', "Meta<Key>O", "Meta+O", OpenFile},
    {XmtMenuItemPushButton, "Save", 'S', "Meta<Key>S", "Meta+S", SaveFile},
    {XmtMenuItemPushButton, "Save As...", 'A', "Meta<Key>A", "Meta+A", SaveAs},
    {XmtMenuItemSeparator},
    {XmtMenuItemPushButton, "Exit", 'x', "Ctrl<Key>C", "Ctrl-C", Quit},
    {XmtMenuItemEnd}
};

static XmtMenuItem view_menu_items[] = {
    {XmtMenuItemToggleButton, "By Icon", 'B', NULL, NULL, NULL, NULL, NULL,
        "view_by_name", "By Name", 'B'},
    {XmtMenuItemToggleButton, "Sort by Size", 'S', "Meta<Key>Z", "Meta+Z", 
        NULL, NULL, NULL, "sort_by_size"},
    {XmtMenuItemEnd}
};

static XmtMenuItem help_menu_items[] = {
    {XmtMenuItemPushButton, "About", 'A', NULL, NULL, About},
    {XmtMenuItemPushButton, "On Context", 'C', "Meta<Key>H", "Meta+H", 
        ContextHelp},
    {XmtMenuItemEnd}
};

static XmtMenuItem menubar_items[] = {
    {XmtMenuItemCascadeButton + XmtMenuItemTearoff,
        "File", 'F', NULL, NULL, NULL, NULL, file_menu_items},
    {XmtMenuItemCascadeButton + XmtMenuItemTearoff,
        "View", 'V', NULL, NULL, NULL, NULL, view_menu_items},
    {XmtMenuItemCascadeButton+XmtMenuItemHelp+XmtMenuItemTearoff, 
        "Help", 'H',  NULL, NULL, NULL, NULL, help_menu_items},
};

static XmtMenuItem popup_items[] = {
    {XmtMenuItemLabel, "@fBOperations"},
    {XmtMenuItemSeparator},
    {XmtMenuItemPushButton, "Move", 'M', "Ctrl<Key>M", "Ctrl-M", move},
    {XmtMenuItemPushButton, "Rotate", 'R', "Ctrl<Key>R", "Ctrl-R", rotate},
    {XmtMenuItemPushButton, "Scale", 'S', "Ctrl<Key>S", "Ctrl-S", scale},
    {XmtMenuItemSeparator},
    {XmtMenuItemLabel, "@fBOptions"},
    {XmtMenuItemSeparator},
    {XmtMenuItemToggleButton+XmtMenuItemOn, "Show Grid", 'G',
     "Ctrl<Key>G", "Ctrl-G", toggle_grid, NULL, NULL, NULL, "Hide Grid", 'G'} 
};

void main(argc,argv)
int argc;
char **argv;
{
    XtAppContext app;
    Widget toplevel;
    Widget layout, menubar, workarea,  popup;
    static Boolean name, sort;
    Arg args[5];
    int ac;

    toplevel = XtAppInitialize (&app, "Test", NULL, 0,
#ifndef X11R5				
				(Cardinal *)&argc,
#else
				&argc,
#endif				
				argv, NULL, NULL, 0);

    XmtVaRegisterSymbols("view_by_name", XtRBoolean, sizeof(Boolean), &name,
			 "sort_by_size", XtRBoolean, sizeof(Boolean), &sort,
			 NULL);

    layout = XmtCreateLayout(toplevel, "layout", NULL, 0);
    XtManageChild(layout);

    ac = 0;
    XtSetArg(args[ac], XmtNitems, menubar_items); ac++;
    XtSetArg(args[ac], XmtNnumItems, XtNumber(menubar_items)); ac++;
    menubar = XmtCreateMenubar(layout, "menubar", args, ac);
    XtManageChild(menubar);
    
    workarea = XmCreateDrawingArea(layout, "workarea", NULL, 0);
    XtVaSetValues(workarea,
		  XmtNlayoutWidth, 400,
		  XmtNlayoutHeight, 300,
		  NULL);
    XtManageChild(workarea);

    ac = 0;
    XtSetArg(args[ac], XmtNitems, popup_items); ac++;
    XtSetArg(args[ac], XmtNnumItems, XtNumber(popup_items)); ac++;
    popup = XmtCreatePopupMenu(workarea, "popup", args, ac);

    XtRealizeWidget(toplevel);
    XtAppMainLoop(app);
}
