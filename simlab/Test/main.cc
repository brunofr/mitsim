// FILE: main.cc
//
// An example of C source code for using Xmt
//
// Qi Yang
// Wed Jun 18 11:21:58 EDT 1997
//
// The code to be tested should be called by functions registered in
// HelloCB() in hello.cc.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <Xmt/All.h>
#include <Xmw/XmwList.h>
#include <Xmw/XmwXbae.h>
#include <Xmw/XmwXbaeList.h>
#include <Xmw/XmwSciPlot.h>
#include <Xmw/XmwOthers.h>
#include <Combo/ComboBox.h>

extern void HelloCB(Widget, XtPointer, XtPointer);
extern void DoneCB(Widget, XtPointer, XtPointer);

Widget		toplevel;

void main(int argc, char **argv)
{
   XtAppContext	app;
   char buffer[256];

   // (0) Set the XENVIRONMENT variable to the resource file.

   sprintf(buffer, "XENVIRONMENT=%s.ad", argv[0]);
   putenv(buffer);

   // (1) Initialize Xmt.

   toplevel = XmtInitialize (&app, "hello", NULL, 0,
							 &argc, argv, NULL, NULL, 0);

   // (2.0) Register all XMT widgets.

   XmtRegisterAll();

   // (2.1) Register other widgets.

   XmtRegisterXbaeWidgets();
   XmtRegisterWidgetConstructor( "XgTabs", CreateXgTabs );
   XmtRegisterWidgetConstructor( "SciPlot", CreateSciPlot );
   XmtRegisterWidgetConstructor( "ListTree", CreateListTree );
   XmtRegisterWidgetConstructor( "ComboBox", XmCreateComboBox );

   XmtRegisterWidgetConstructor( "XmwList", createXmwList );
   XmtRegisterWidgetConstructor( "XmwXbaeList", createXmwXbaeList );

   // EditRes Support

   // XtAddEventHandler(toplevel, 0, True,
   // (XtEventHandler)_XEditResCheckMessages, NULL);

   // (3) Register callback procedures

   XmtRegisterCallbackProcedure("HelloCB", HelloCB, XmtRCallbackUnused);
   XmtRegisterCallbackProcedure("DoneCB", DoneCB, XmtRCallbackUnused);

   // (4) Automatical create widgets from resource.

   XmtCreateChildren(toplevel);

   // (5) Create addition callbacks, etc.

   // ...

   // (7) Make the application visible.

   XtRealizeWidget(toplevel);

   // (8) Turn the control to Xt event loop.

   XtAppMainLoop(app);
}
