// FILE: hello.cc
//
// Qi Yang
// Wed Jun 18 11:21:58 EDT 1997
//
// The code to be tested should be called by functions registered in
// HelloCB().

#include <assert.h>
#include <iostream.h>
#include <stdlib.h>
#include <Xmt/All.h>

extern Widget toplevel;

void HelloCB(Widget, XtPointer, XtPointer)
{
   cout << "Hello world." << endl;
}

void DoneCB(Widget, XtPointer, XtPointer)
{
   cout << "Done." << endl;
   exit(0);
}
