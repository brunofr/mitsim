// FILE: hello.cc
//
// Qi Yang
// Wed Jun 18 11:21:58 EDT 1997
//
// The code to be tested should be called by functions registered in
// HelloCB().

#include <assert.h>
#include <iostream.h>
#include <stdlib.h>
#include <Xmt/All.h>

#include <Xmw/XmwSciPlot.h>

extern Widget toplevel;

void HelloCB(Widget, XtPointer, XtPointer)
{
   cout << "Hello world." << endl;

   // The plot is create here but can be close (destroyed) by itself.

   XmwSciPlot *plot = new XmwSciPlot(toplevel, "sciplotShell");
   static double x[]={14.,18.,22.,26.,SCIPLOT_SKIP_VAL,30.,34.,38.,42.}; 
   static double y[]={30.,60.,90.,120.,SCIPLOT_SKIP_VAL,150.,180.,270.,355.}; 
   int i = plot->createSeries(9, x, y, "Line 1");
   plot->post(&plot);
}

void DoneCB(Widget, XtPointer, XtPointer)
{
   cout << "Done." << endl;
   exit(0);
}
