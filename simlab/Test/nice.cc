// Test the scheduling of a process

#include <sys/resource.h>
#include <iostream.h>

void main(int argc, char *argv[])
{
  int base = getpriority(PRIO_PROCESS, 0);
  int dx = 1;
  int ivalue, ovalue;
  int error;
  cout << "Base = " << base << endl;
  while (dx) {
	cout << "Enter = "; cout.flush();
	cin >> dx;
	ivalue = base + dx;
	error = setpriority(PRIO_PROCESS, 0, ivalue);
	ovalue = getpriority(PRIO_PROCESS, 0);
	cout << "I=" << ivalue << " O=" << ovalue << " E=" << error << endl;
  }
}
