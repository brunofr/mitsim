# -*- Makefile -*-
#
# plot.mk -- The hello world small example
#

all: plot

AUX.CCFLAGS = $(GUI_CCFLAGS) $(STL_CCFLAGS)
AUX.LDFLAGS = -lXmw -lTools -lIO $(GUI_LDFLAGS) $(STL_LDFLAGS) -lm

TARGET = plot
EXE.OBJS = \
	VersionInfo.o \
	main.o \
	plot.o

include ../general.tmpl

$(TARGET): Version $(BIN_DIR) $(EXE.OBJS) \
	$(ARCHIVE) $(LIB_DIR)/libXmw.a
	$(CC) -o $@ $(EXE.OBJS) $(LD_FLAGS)

clean: clean_objects
	$(RM) $(TARGET)