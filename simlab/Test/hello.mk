# -*- Makefile -*-
#
# hello.mk -- The hello world small example
#

all: hello

AUX.CCFLAGS = $(GUI_CCFLAGS)
AUX.LDFLAGS = -lXmw $(GUI_LDFLAGS) -lm

TARGET = hello
EXE.OBJS = \
	main.o \
	hello.o

include ../general.tmpl

$(TARGET): $(BIN_DIR) $(EXE.OBJS) $(ARCHIVE)
	$(CC) -o $@ $(EXE.OBJS) $(LD_FLAGS)
