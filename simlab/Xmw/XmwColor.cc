//-*-c++-*------------------------------------------------------------
// XmwColor.cc
//
// This file contains the class implementation for the class XmwColor,
// which allocates some named colors and a list of colors for a color
// rain bow.
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//--------------------------------------------------------------------

#include <iostream>
#include <cstdio>

#include <Xmt/Xmt.h>
#include <Xmt/Color.h>

#include <Tools/ToolKit.h>

#include "XmwInterface.h"
#include "XmwColorMap.h"
#include "XmwColor.h"
using namespace std;

XmwColor *theColorTable = NULL;

XmwColor::XmwColor()
  : nColors_(0)
{
  Widget w = theBaseInterface->widget();

  if (ToolKit::verbose()) {
	cout << XmtLocalize2(w, "Initializing colors ", "colors",
						 "promptStart");
	cout.flush();
  }

  Screen *screen = XtScreen(w);
  display_ = XtDisplay(w);

  // Find the colormap

  if (theColorMap) {
	cmap_ = theColorMap->colormap();
	black_      = color("#000000");
	white_      = color("#FFFFFF");
  } else {
	cmap_ = DefaultColormapOfScreen(screen);
	black_ = BlackPixelOfScreen(screen);
	white_ = WhitePixelOfScreen(screen);
  }

  darkBlue_   = color("#000099", "darkBlue");
  blue_       = color("#0077FF", "blue");
  green_      = color("#00AA00", "green");
  lightGreen_ = color("#00FF00", "lightGreen");
  yellow_     = color("#FFFF00", "yellow");
  orange_     = color("#FF7700", "orange");
  red_        = color("#BB0000", "red");
  lightRed_   = color("#FF0000", "lightRed");
  magenta_    = color("#9F009F", "magenta");
		  				       
  lightGray_  = color("#9F9F9F", "lightGray");
  darkGray_   = color("#5F5F5F", "darkGray");
  cyan_	      = color("#00F0F0", "cyan");
  water_      = color("#000030", "water");

  inciBefore_ = color("#303030", "inciBefore");
  inciAfter_  = color("#101010", "inciAfter");

  gray_       = color("gray", "gray");

  createRainBow();

  if (ToolKit::verbose()) {
	cout << " " << nColors_ << " "
		 << XmtLocalize2(w, "colors done.", "colors", "promptEnd")
		 << endl;
  }
}

XmwColor::~XmwColor()
{
  delete [] pixels_;
}


Pixel
XmwColor::color(const char *defa, const char *symbol)
{
  Widget w = theBaseInterface->widget();
  const char *name = XmtLocalize2(w, defa, "colors", symbol);
  return color(name);
}

// Returns the pixel value of the given color. If the color does not
// exits or colormap is already full, it shows an error message and
// returns white.

Pixel
XmwColor::color(const char *name)
{
  Pixel p = white_;
  Widget w = theBaseInterface->widget();
  int err = XmtAllocWidgetColor(w, name, &p);
  switch (err) {
  case 1:
	cerr << "Unknown or malformed color name (" << name << ")."
		 << endl;
	break;
  case 2:
	cerr << "Colormap full when allocating color (" << name << ")."
		 << endl;
	break;
  default:
	if (ToolKit::verbose() && !(nColors_ % 10)) {
	  cout << "."; cout.flush();
	}
	nColors_ ++;
	break;
  }
  return p;
}

// Returns a color based on RGB value. All input value are between 0.0
// and 1.0.

Pixel XmwColor::rgb01(float r, float g, float b)
{
  return rgb((int)(r * 256.0), (int)(g * 256.0), (int)(b * 256.0));
}


// Returns a color based on HSL value. All input value are between 0.0
// and 1.0.

Pixel XmwColor::hsl01(float h, float s, float l)
{
  return hsl((int)(h * 360.0), (int)(s * 100.0), (int)(l * 100.0));
}


Pixel XmwColor::rgb(unsigned int r, unsigned int g, unsigned int b)
{
  XColor c;
  c.red = r;
  c.green = g;
  c.blue = b;
  c.flags = DoRed | DoGreen | DoBlue;
  if (XAllocColor(display_, cmap_, &c) == 0) {
	cerr << "Failed allocating RGB color ("
		 << r << '/' << g << '/' << b << ")."
		 << endl;
  } else {
	nColors_ ++;
  }

  if (ToolKit::verbose() && !(nColors_ % 10)) {
	cout << "."; cout.flush();
  }
  return c.pixel;
}


Pixel XmwColor::hsl(unsigned int h, unsigned int s, unsigned int l)
{
  unsigned int r, g, b;
  if (h > 360) h = h % 360;
  if (s > 100) s = 100;
  if (l > 100) l = 100;
  XmtHSLToRGB(h, s, l, &r, &g, &b);
  return rgb(r, g, b);
}

void XmwColor::createRainBow()
{
  Widget w = theBaseInterface->widget();

  int hue   = atoi(XmtLocalize2(w, "240", "colors", "hueStart"));
  int step  = atoi(XmtLocalize2(w, "-10", "colors", "hueStep"));
  int ncols = atoi(XmtLocalize2(w, "30", "colors", "hueCols"));
  int nrows = atoi(XmtLocalize2(w, "2", "colors", "hueRows"));
  const char *minv = XmtLocalize2(w, "30", "colors", "minLightness");
  const char *maxv = XmtLocalize2(w, "50", "colors", "maxLightness");
  float minvalue = atof(minv);
  float maxvalue = atof(maxv);

  int i, j, h, v;
  float alpha;					// interpolate
  float value;					// lightness
  nCells_ = ncols * nrows;
  nRows_ = nrows;
  nCols_ = ncols;
  pixels_ = new Pixel [nCells_];
  for (i = 0; i < nrows; i ++) {
	h = hue;
	if (nrows > 1) {
	  alpha = (float)i / (float)(nrows - 1);
	} else {
	  alpha = 1.0;
	}
	value = minvalue + (maxvalue - minvalue) * alpha;
	v = (int) (value + 0.5);
	for (j = 0; j < ncols; j ++) {
	  if (h < 0) h += 360;
	  else if (h > 360) h -= 360;
	  pixels_[i * ncols + j] = hsl(h, 100, v);
	  h += step;
	}
  }
}


// Small set of colors

void XmwColor::organizeRainBow()
{
  int &i = nCells_;
  nRows_ = 1;
  nCols_ = 9;
  pixels_ = new Pixel[nCells_];
  i = 0;
  pixels_[i++] = darkBlue_;
  pixels_[i++] = blue_;
  pixels_[i++] = green_;
  pixels_[i++] = lightGreen_;
  pixels_[i++] = yellow_;
  pixels_[i++] = orange_;
  pixels_[i++] = red_;
  pixels_[i++] = lightRed_;
  pixels_[i++] = magenta_;
}


// Returns a color in last row (the one with max density).

Pixel
XmwColor::color(float clr)
{
  clr *= (nCols_ - 1) + 0.5;
  int col;

  if (clr < 0) col = 0;
  else if (clr > nCols_ - 1) col = nCols_ - 1;
  else col = (int) clr;

  col += (nRows_ - 1) * nCols_;

  return pixels_[col];
}


// Returns a color with a given color and intensity.

Pixel
XmwColor::color(float clr, float intensity)
{
  clr *= (nCols_ - 1) + 0.5;
  intensity *= (nRows_ - 1) + 0.5;
  int row, col;

  if (clr < 0) col = 0;
  else if (clr > nCols_ - 1) col = nCols_ - 1;
  else col = (int) clr;

  if (intensity < 0) row = 0;
  else if (intensity > nRows_ - 1) row = nRows_ - 1;
  else row = (int) intensity;

  col += row * nCols_;

  return pixels_[col];
}


Pixel
XmwColor::pixel(int c, int i)
{
  if (i < 0) i = 0;
  else if (i >= nRows_) i = nRows_ - 1;
  if (c < 0) c = 0;
  else if (c >= nCols_) c = nCols_ - 1;
  return pixels_[i * nCols_ + c];
}


Pixel
XmwColor::pixel(int c)
{
  if (c < 0) c = 0;
  else if (c >= nCols_) c = nCols_ - 1;
  return pixels_[(nRows_ - 1) * nCols_ + c];
}
