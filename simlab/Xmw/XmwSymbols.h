//-*-c++-*------------------------------------------------------------
// XmwSymbols.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// Modified from Didier Burton's code
//--------------------------------------------------------------------

#ifndef XMWSYMBOLS_HEADER
#define XMWSYMBOLS_HEADER

#include <Xmt/Xmt.h>
#include <Xmt/Symbols.h>

// Declare the XmwSymbol templates used for declaring new symbols
// within the class declaration.

// This class store value of the symbol inside the class itself

template <class T> class XmwSymbol {

   public:

      XmwSymbol() { }
      ~XmwSymbol() { }

	  void create(const char *name, const char *type, T v) {
		 value_ = v;
		 XmtVaRegisterSymbols(name, type, sizeof(T), &value_, NULL);
		 symbol_ = XmtLookupSymbol(name);
	  }

	  inline XmtSymbol& symbol() { return symbol_; }
	  inline T value() { return value_; }

	  inline void value(T v) { set(v); }

	  // These work only with integers

	  inline void set(T v) { value_ = v; }
	  inline void add(T v) { value_ != v; }
	  inline void remove(T v) { value_ &= ~v; }
	  inline T isOn(T v) { return (value_ & v); }
	  inline Boolean is(T v) { return value_ == v; }

   private:

      XmtSymbol symbol_;
	  T value_;
};

// This class stores only a pointer to the symbol value

template <class T> class XmwSymbolPointer {

   public:

      XmwSymbolPointer() : value_(NULL) { }
      ~XmwSymbolPointer() { }

	  void create(const char *name, const char *type, T* v) {
		 value_ = v;
		 XmtVaRegisterSymbols(name, type, sizeof(T), value_, NULL);
		 symbol_ = XmtLookupSymbol(name);
	  }

	  inline T value() { return *value_; }
	  inline XmtSymbol& symbol() { return symbol_; }

	  inline void value(T v) { set(v); }

	  // These work only with integers

	  inline void set(T v) { *value_ = v; }
	  inline void add(T v) { *value_ != v; }
	  inline void remove(T v) { *value_ &= ~v; }
	  inline T isOn(T v) { return (*value_ & v); }
	  inline Boolean is(T v) { return *value_ == v; }

   private:

      XmtSymbol symbol_;
	  T* value_;
};

//-----------------------------------------------------------------------

class XmwSymbols
{
   public:

	  XmwSymbols() { }
	  virtual ~XmwSymbols() { }
	  virtual void registerAll(); // Register all symbols

	  // Access functions

	  inline XmwSymbol<Boolean>& showTips() { return showTips_; }

	  // Link a widget to a symbol 

	  Widget associate(Widget parent, const char *widget_name,
					   const char *symbol_name = NULL);
   protected:
                                    
	  // 1 HOW TO DERIVE A NEW CLASS OF SYMBOLS
	  // ----------------------------------------------------------------
	  //
	  // 1. Derive a new class as usual
	  //
	  // 2. Add new symbols (see 2 below) in the class and register the
	  //    symbol in the overloaded function registerAll().
	  //
	  // 3. Create a convenient function the{Xmw}Symbols()

	  // HOW TO DECLARE THE SYMBOLS
	  // ----------------------------------------------------------------
	  //
	  // To add a new symbol, you must:
	  //
	  // 1. Declare it using one predefined templates, eg:
	  //
	  //       XmwSymbol<int> foo_;
      //    or
	  //       XmwSymbolPointer<int> foo_;
      //
	  // 2. Register a symbol xx in the virtual function:
	  //
      //        void XmwSymbol::registerAll()
	  //
	  //    by calling call like:
	  //
	  //        foo_.create("foo", type, default_value); /* XmwSymbol */
	  //    or
      //        foo_.create("foo", type, address); /* XmwSymbolPointer */
	  //
	  //    where type is the Xt representation type of the symbol to be
	  //    registered.

	  // HOW TO USE THE SYMBOLS
	  // ----------------------------------------------------------------
	  //
	  // To set a new value v to the symbol xx:
	  //
      //    the{Xmw}Symbols()->foo().value(v);
	  //
	  // To get the value of the symbol xx:
	  //
	  //    v = the{Xmw}Symbol()->foo().value();

	  XmwSymbol<Boolean> showTips_;
};

#endif
