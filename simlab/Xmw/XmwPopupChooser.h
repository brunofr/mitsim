//-*-c++-*------------------------------------------------------------
// XmwPopupChooser.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef XMWPOPUPCHOOSER_HEADER
#define XMWPOPUPCHOOSER_HEADER

#include <Xm/RowColumn.h>
#include <string>
#include <Templates/STL_Configure.h>
#include <vector>

class XmwPoint;

class XmwPopupChooser
{
public:

  XmwPopupChooser(Widget parent, // a drawing area
		  int num,
		  const std::vector<std::string>& labels,	 // labels for the choices
		  XtCallbackProc cb = 0,      // callback procedure
		  const std::vector<XtPointer> *client_data = 0);// cb client data

  XmwPopupChooser(Widget parent, // a drawing area
		  XtCallbackProc cb = 0);     // callback procedure

  virtual ~XmwPopupChooser();

  virtual void add(const std::string &label, const XtPointer client_data);
  virtual void popup(const XmwPoint *p = 0);

private:

  void create(XtCallbackProc cb);
  static void choose_cb(Widget, XtPointer, XtPointer);

private:

  Widget parent_;
  Widget popup_;
  XtCallbackProc cb_;
  int nChoices_;
};

#endif
