//-*-c++-*------------------------------------------------------------
// XmwSymbols.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <Xmt/Xmt.h>
#include <Xmt/Create.h>
#include <Xmt/Chooser.h>

#include "XmwSymbols.h"
#include "XmwInterface.h"

void XmwSymbols::registerAll() {
   showTips_.create("showTips", XtRBoolean, True);
   // fontsize.create("fontsize", XtRInt, 120);
   // scale.create("scale", XtRFloat, 1.0);
}


// Connect a widget to a symbal

Widget XmwSymbols::associate(Widget parent,
							 const char *widget_name,
							 const char *symbol_name)
{
   if (symbol_name == NULL) {
	  symbol_name = widget_name;
   }

   Widget w = XmtNameToWidget(parent, widget_name); 
   if (w) {
	  XtVaSetValues(w, XmtNsymbolName, symbol_name, NULL);
   } else {
	  theBaseInterface->widgetMissingBoilout(widget_name);
   }
   return w;
}
