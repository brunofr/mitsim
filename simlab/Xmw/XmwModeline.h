//-*-c++-*------------------------------------------------------------
// XmwModeline.h
//
// qiyang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef XmwMODELINE_HEADER
#define XmwMODELINE_HEADER

#include <Xm/Xm.h>
#include <Xbae/Matrix.h>

class XmwModeline
{
  friend class XmwInterface;

public:

  enum KeyState {
	KEY_STATE_DISABLED = 0,
	KEY_STATE_PAUSE = 1,
	KEY_STATE_RUN = 2
  };

  XmwModeline(Widget parent);
  virtual ~XmwModeline() { }

  virtual void update();
  void updateTimer(int i, double t);
  void updateMeter(int i, double r);
  void updateProgress(int i, float p);
  void setKeyState(enum KeyState state);
  void playKey();
  virtual void keyStateChanged(enum KeyState state) { }
  virtual void updateButtons() { }

protected:
	
  Widget timers_;
  Widget meters_;
  Widget progress_;
  Widget key_;
  char keyTipTag_[5];

protected:

  static void keyClickCB(Widget, XtPointer, XtPointer);
  static void cellEnterCB(Widget, XtPointer, XtPointer);
};

#endif
