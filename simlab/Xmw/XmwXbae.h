/*
 * XmwXbae.h -- Add matrix widget to Xmt
 *
 * Qi Yang modified from:
 * http://sauron.et.byu.edu/Xmt/XmtData/XmtXbae.html
 * By Karl G. Merkley
 * Mechanical Engineering Dept.
 * 242 Clyde Building
 * Brigham Young University
 * Provo, UT 84602 USA
 */

#ifndef XMWXBAE_HEADER
#define XMWXBAE_HEADER

#if defined(__cplusplus) || defined(c_plusplus)
  extern "C" {
#endif
    void XmtRegisterXbaeWidgets(void);
#if defined(__cplusplus) || defined(c_plusplus)
  }
#endif
#endif
