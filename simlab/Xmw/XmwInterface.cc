//-*-c++-*------------------------------------------------------------
// XmwInterface.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the implementation for the class XmwInterface
//--------------------------------------------------------------------

#include <cassert>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <sys/param.h>

#include <X11/Xlib.h>
#include <X11/cursorfont.h>
#include <X11/keysym.h>

#include <Xmt/Xmt.h>
#include <Xmt/WidgetType.h>
#include <Xmt/Create.h>
#include <Xmt/Converters.h>
#include <Xmt/Layout.h>
#include <Xmt/Menu.h>
#include <Xmt/WorkingBox.h>
#include <Xmt/Dialogs.h>
#include <Xmt/InputField.h>
#include <Xmt/Procedures.h>
#include <Xmt/Include.h>
#include <Xmt/Pixmap.h>

#include <IO/Exception.h>
#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>
#include <Tools/SimulationEngine.h>

#include "XmwInterface.h"
#include "XmwColor.h"
#include "XmwSymbols.h"
#include "XmwMenu.h"
#include "XmwImage.h"
#include "XmwModeline.h"
#include "XmwList.h"
#include "XmwXbaeList.h"
#include "XmwXbae.h"
#include "XmwFakeKey.h"
#include "XmwOthers.h"
#include "XmwKeyInteractor.h"
#include "XmwTab.h"
using namespace std;

#include <Combo/ComboBox.h>

Widget XmwInterface::toplevel_ = NULL;
Widget XmwInterface::workingShell_ = NULL;
Display* XmwInterface::display_ = NULL;
Widget XmwInterface::tipShell_ = NULL;
String XmwInterface::tipTag_ = NULL;
XmwImage* XmwInterface::icon_ = NULL;

XmwInterface* theBaseInterface = NULL;
int MSG_STAY_TIME = 5000;		// in milliseconds


void XmwInterface::msgPush()
{
  if (!msgline_) return;
  XmtMsgLinePush(msgline_);
  msg_count_ ++;
}

void XmwInterface::msgPop()
{
  if (!msgline_) return;
  while (msg_count_ > 0) {
	XmtMsgLinePop(msgline_, XmtMsgLineNow);
	msg_count_ --;
  }
}

void XmwInterface::msgSet(const char *msg)
{
  if (!msgline_) return;
  XmtMsgLineSet(msgline_, msg);
}

void XmwInterface::msgClear(int when)
{
  if (!msgline_) return;
  XmtMsgLineClear(msgline_, when);
//   if (when > 0) {
// 	scheduleOneDummyEvent(when + 1);
//   }
}

void XmwInterface::msgPrintf(const char* fmt, ...)
{
  if (!msgline_) return;
  va_list ap;
  va_start(ap, fmt);
  XmtMsgLinePrintf(msgline_, fmt, ap);
  va_end(ap);
}

void XmwInterface::msgShow(const char *msg, int when)
{
  msgSet(msg);
  msgClear(when);
}

// Constructor

XmwInterface::XmwInterface(int *argc, char **argv, XmwImage *icon)
  : XmwWidget(),
	symbols_(NULL),
	msgline_(NULL),
	msg_count_(0),
	modeline_(NULL),
	helpCmdPrefix_(NULL),
	helpCmdSuffix_(NULL),
	helpUrl_(NULL),
	helpNode_(NULL),
	is_zzz_on_(0)
{
  theBaseInterface = this;
  icon_ = icon;
  name_ = Copy(ToolKit::splitName(argv[0]));

  // Set environment variable for the name of the main resource
  // file. The searching order are: (1) the file pointed by
  // XENVIRONMENT; (2) ad file in the same directory as the
  // executable; (4) ad file in ../../lib/ad directory with respect to
  // executable; and (5) ad file in $(SIMLAB)/lib/ad directory.

  const char *ad = getenv("XENVIRONMENT");
  if (ad) {
	  if (!ToolKit::fileExists(ad)) {
		  cerr << "Error: Resource file " << ad << " not found." << endl;
		  theException->exit(STATE_ERROR_QUIT);
	  }
  } else {
	const char* path = ToolKit::splitPath(argv[0]);
	char buffer[MAXPATHLEN+1];

	sprintf(buffer, "%s/%s.ad", path, name_);
	if (ToolKit::fileExists(buffer)) goto ad_found ;

	sprintf(buffer, "%s/../..", path);
	sprintf(buffer, "%s/lib/ad/%s.ad", ExpandEnvVars(buffer), name_);
	if (ToolKit::fileExists(buffer)) goto ad_found ;

	if (!(path = getenv("SIMLAB"))) goto ad_error ;
	sprintf(buffer, "%s/lib/ad/%s.ad", path, name_);
	if (ToolKit::fileExists(buffer)) goto ad_found ;

  ad_error:
	cerr << "Error: failed locating resource file "
		 << buffer << endl;
	theException->exit(STATE_ERROR_QUIT);

  ad_found:
	PutEnv("XENVIRONMENT=%s", ToolKit::RealPath(buffer));
	ad = getenv("XENVIRONMENT");
  }

  if (ToolKit::verbose()) {
	cout << "Loading resource " << ad << " ..." << endl;
  }

  // Now we are ready to do real things

  toplevel_ = XmtInitialize (&appContext_, (char *)name_, NULL, 0,
							 argc, argv, NULL, NULL, 0);

  // Extract and save a pointer to the X display structure

  display_ = XtDisplay ( toplevel_ );

  if (ToolKit::verbose()) {
	cout << "Display is " << DisplayString(display_) << endl;
  }

  displayWorkingBox (name_, "Initializing");

  // Register all converters of the Xmt package

  XmtRegisterAll ();

  // Register customized packages

  XmtRegisterXbaeWidgets();
  XmtRegisterWidgetConstructor( "XmwTab", XmwTab::createXmwTab );
  XmtRegisterWidgetConstructor( "SciPlot", CreateSciPlot );
  XmtRegisterWidgetConstructor( "ListTree", CreateListTree );
  XmtRegisterWidgetConstructor( "ComboBox", XmCreateComboBox );

  XmtRegisterWidgetConstructor( "XmwList", createXmwList );
  XmtRegisterWidgetConstructor( "XmwXbaeList", createXmwXbaeList );
  //   XmtRegisterWidgetConstructor( "XmtHisto1D", createHisto1D );
  //   XmtRegisterWidgetConstructor( "XmtHisto2D", createHisto2D );
}

XmwInterface::~XmwInterface()
{
  delete menu_;
  delete modeline_;
}

void XmwInterface::setTitle(const char *title)
{
  const char *os = Str("%s:%s", name_, title);
  XtVaSetValues(widget_, XmNtitle, os, NULL);
}

// This function should be called at the beginning of create() in the
// derived class.

void XmwInterface::create()
{
  // Register symbols

  symbols_ = createSymbols();	// virtual
  symbols_->registerAll();		// virtual

  // Create the top level shell

  widget_ = XmtBuildToplevel(toplevel_, name_);

  // Create a menu bar

  menu_ = menu(widget_);

  // Create a modeline

  modeline_ = modeline(widget_);

  msgline_ = XmtNameToWidget(widget_, "*msgline");
  assert(msgline_);
  zzz_ = XmtNameToWidget(widget_, "*zzz");
  if (zzz_) {
	toggle_zzz(false);
  }

  // Get information about the external browser

  helpCmdPrefix_  = XmtLocalize2(widget_, "netscape -remote 'openURL(",
								 "browser", "cmdPrefix");
  helpCmdSuffix_  = XmtLocalize2(widget_, ")'",
								 "browser", "cmdSuffix");
  helpUrl_  = XmtLocalize2(widget_, "http://its.mit.edu/simlab",
						   "browser", "url");
  helpNode_ = XmtLocalize2(widget_, "simlab.html", "browser", "node");

  int t = atoi(XmtLocalize2(widget_, "4000", "prompt", "MsgStayTime"));
  if (t > 0) {
	MSG_STAY_TIME = t;
  }
}


XmwSymbols* XmwInterface::createSymbols()
{
  return new XmwSymbols;
}

void XmwInterface::manage()
{
  XmwWidget::manage ();
  XmtWaitUntilMapped ( widget_ );
  showBusyCursor();
  handlePendingEvents();
}


void XmwInterface::showBusyCursor()
{
  XmtDisplayBusyCursor(widget_);
}

void XmwInterface::showDefaultCursor()
{
  XmtDisplayDefaultCursor(widget_);
}


XmwMenu* XmwInterface::menu(Widget parent)
{
  return new XmwMenu(parent);
}

XmwModeline* XmwInterface::modeline(Widget parent)
{
  return new XmwModeline(parent);
}


// Show a waiting dialog box.  This function can not be called until
// the toplevel shell of the interface is created.  For example:
//
//   theInterface->displayWorkingBox ("Hello", "Initializing");
//
// This message will read as "Initializing Hello.  Please wait ...".
// This working dialog box should be removed by calling:
//
//   theInterface->destroyWorkingBox ();

void XmwInterface::displayWorkingBox(const char *title, const char *msg)
{
  static char font[] = "*-helvetica-bold-r-*-*-*-180-*";
  char message[200];

  sprintf(message, "%s %s\nPlease wait ...", msg, title);
	
  if (workingShell_) {
	destroyWorkingBox();
  }

  workingShell_ = XtCreatePopupShell("working",
									 overrideShellWidgetClass, toplevel_,
									 NULL, 0);

  Widget working_box = XtVaCreateManagedWidget("working_box",
											   xmtWorkingBoxWidgetClass, workingShell_,
											   XmtNmessage, message,
											   XtVaTypedArg, XmtNfontList, XtRString,
											   font, sizeof(font),
											   XtVaTypedArg, XmtNicon, XtRString,
											   icon_->name(), sizeof(icon_->name()) + 1,
											   XmtNshowScale, False,
											   XmtNshowButton, False,
											   NULL);

  // Position the box in the center of the screen

  static int dsp_wdh = DisplayWidth( display_, 0 );
  static int dsp_hgt = DisplayHeight( display_, 0 );

  Dimension wdh, hgt;			// size of the box
  Position x, y;

  XtRealizeWidget(workingShell_);

  XtVaGetValues(workingShell_,
				XmNwidth, &wdh,
				XmNheight, &hgt,
				NULL );

  x = (dsp_wdh - wdh) / 2;
  y = (dsp_hgt - hgt) / 2;
  if (x < 0) x = 0;
  if (y < 0) y = 0;

  XtVaSetValues(workingShell_,
				XmNx, x,
				XmNy, y,
				XtNsaveUnder, True,
				NULL );

  XtPopup(workingShell_, XtGrabNone);
  XmtWaitUntilMapped (workingShell_);
  XmtDisplayBusyCursor(workingShell_);
}

// Removed the working dialog box created by DisplayWorkingBox(...)

void XmwInterface::destroyWorkingBox()
{
  if (workingShell_) {
	XmtDisplayDefaultCursor(workingShell_);
	XtPopdown(workingShell_);
	XtDestroyWidget(workingShell_);
	workingShell_ = NULL;
  }
}

// Dispatching all pending events.  Return immediately if no event is
// pending.

int XmwInterface::handlePendingEvents()
{
  int n = 0;
  while (XtAppPending(appContext_)) {
	XEvent event;
	XtAppNextEvent(appContext_, &event);
	XtDispatchEvent(&event);
	n ++;
  }
  return n;
}

// Used for generate a dummy xevent

void XmwInterface::toggle_zzz(bool is_on)
{
  if (!zzz_ || !theColorTable) return;
  Pixel clr;
  if (is_on) {
	clr = theColorTable->red();
  } else {
	clr = theColorTable->gray();
  }
  XtVaSetValues(zzz_, XmNforeground, clr, NULL);
  is_zzz_on_ = is_on;
}

// Dispatching all pending events.  Wait for n/100 seconds if no event
// if pending.

void XmwInterface::waitNextEvent(int n)
{
  // register a timeout event which will generate a X event to
  // break the waiting for next event.

  XtAppAddTimeOut(appContext_, n,
 				  &XmwInterface::makeSnoozeEvent,
 				  (XtPointer) NULL);

  // block and wait for next event

  do {
	XEvent event;
	XtAppNextEvent(appContext(), &event);
	XtDispatchEvent(&event);
  } while (XtAppPending(appContext_));
}

void XmwInterface::scheduleOneDummyEvent(int n)
{
  XtAppAddTimeOut(appContext_, n, &XmwInterface::makeFakeXEvent,
				  (XtPointer) NULL);
}

void XmwInterface::makeSnoozeEvent(XtPointer, XtIntervalId *)
{
  if (theBaseInterface) {
	if (theBaseInterface->is_zzz_on()) {
	  theBaseInterface->toggle_zzz(false);
	} else {
	  theBaseInterface->toggle_zzz(true);
	}
  }
}

void XmwInterface::makeFakeXEvent(XtPointer, XtIntervalId *)
{
  theBaseInterface->toggle_zzz(false);
}

// This is to fool an application believe a key was pressed on the
// keyboard.  This needs to link with -lXtst -lXext.

void XmwInterface::makeKeyPressEvent(XtPointer client, XtIntervalId *)
{
  XmwFakeKey *fke = (XmwFakeKey *)client;
  if (fke) {
	int revert;
	Window win;

	// Save the key focus
	XGetInputFocus( display_, &win, &revert );

	// Set the input focus to the target window and send a key press
	// event.

	XSetInputFocus( display_, fke->window, RevertToNone, CurrentTime );
	KeyCode keycode = XKeysymToKeycode( display_, fke->keysym );
	XTestFakeKeyEvent(display_, keycode, True, 10L);

	// Restore the key focus
	XSetInputFocus( display_, win, revert, CurrentTime );
  }
}

// Main loop.  It never returns.

int XmwInterface::mainloop()
{
  destroyWorkingBox ();
  XmtDisplayDefaultCursor (widget_);

  XtAppMainLoop ( appContext_ );
  return 0;
}


double 
XmwInterface::convertTime(String t)
{
  return theSimulationClock->convertTime(t);
}

String
XmwInterface::convertTime(double t)
{
  return (String) theSimulationClock->convertTime(t);
}

void XmwInterface::widgetMissingBoilout(const char *item)
{
  XmtDisplayErrorMsgAndWait(XmwInterface::toplevel_,
							"widgetMissingBoilout",	// msg_name: for customizing by res
							"@fB%s@fR not found.",	// message
							"Error",					// title
							NULL,						// help
							item);
  theException->exit(0);
}

void XmwInterface::inputFileNotFound(const char *file)
{
  XmtDisplayErrorMsgAndWait(XmwInterface::toplevel_,
							"inputFileNotFound",		// msg_name: for customizing by res
							"@fB%s@fR not found.",	// message
							"Error",					// title
							NULL,						// help
							file);
}

void XmwInterface::inputFileError(const char *file, int line,
								  const char *item)
{
  XmtDisplayErrorMsgAndWait(XmwInterface::toplevel_,
							"inputFileError",		// msg_name: for customizing by res
							"Filename: @fB%s@fR"
							"Line: @fB%d@fR"
							"Problem in parsing @fB%s@fR",
							"Error",					// title
							NULL,						// help
							file, line, item);
}

void XmwInterface::outputFileError(const char *file)
{
  XmtDisplayErrorMsgAndWait(XmwInterface::toplevel_,
							"outputFileError",		// msg_name: for customizing by res
							"Can not write to @fB%s@fR.",	// message
							"Error",					// title
							NULL,						// help
							file);
}

void XmwInterface::openUrl(const char *marker, const char *node,
						   const char *url)
{
  static char *link = NULL;
  if (link) {
	delete [] link;
  }

  if (!url) url = helpUrl_;
  if (!node) node = helpNode_;

  char *fs = StrCopy("%s/%s", url, node);
  char *os;
  if (marker) {
	os = StrCopy("%s%s#%s%s", helpCmdPrefix_, fs, marker, helpCmdSuffix_);
  } else {
	os = StrCopy("%s%s%s", helpCmdPrefix_, fs, helpCmdSuffix_);
  }

  msgSet("Opening ");
  msgAppend(fs);
  msgAppend(" ... ");

  if (system(link)) {
	msgAppend("canceled.");
	XmtDisplayErrorMsgAndWait(XmwInterface::toplevel_,
							  "noBrowser",		// msg_name: for customizing by res
							  "Browser not installed on %s or not running on %s.",
							  "Error",					// title
							  NULL,						// help
							  HostName(),
							  DisplayString(display_));
  } else {
	msgAppend("done.");
  }
  msgClear(MSG_STAY_TIME);
  StrFree(fs);
  StrFree(os);
}

// Show a tip for command widget at mouse pointer.  Use following line
// to set the label for the tip in the resource file:
//
//     _Messages_*tips.tag: Tip label
//
// This function register the callback function showTipCB(), which is
// called whenever mouse pointer enter or leave the widget.

void XmwInterface::addTip(Widget w, String tag)
{
  // assert that widget w is an object

  assert( XtIsObject(w) );

  XtAddEventHandler(w,
					EnterWindowMask | LeaveWindowMask,
					False,
					&XmwInterface::showTipCB,
					(XtPointer) tag);
}

void XmwInterface::addTips(WidgetList pw, String tags[], int n)
{
  for (int i = 0; i < n; i ++) {
	if (pw[i] && tags[i]) {
	  assert( XtIsObject(pw[i]) );
	  addTip(pw[i], tags[i]);
	}
  }
}


// register a timeout event which will generate a X event to
// break the waiting for next event.

void XmwInterface::showTipCB(Widget w,
							 XtPointer clientData,
							 XEvent *event,
							 Boolean *)
{
  static XtIntervalId id = 0;

  if (!theBaseInterface->symbols()->showTips().value()) {
	return;					// tip feature is not activated
  }

  switch ( event->type ) {
  case EnterNotify:
	tipTag_ = (String) clientData; // name of the tip
	id = XtAppAddTimeOut(theBaseInterface->appContext_, 200,
						 &XmwInterface::showTip, (XtPointer) w);
	break;

  case LeaveNotify:
	if (id && tipTag_) {	// Not yet popup, remove the callback
	  XtRemoveTimeOut(id);
	} else if (tipShell_) { // Unmap the tip information
	  XtPopdown( tipShell_ );
	  XtUnrealizeWidget( tipShell_ );
	}
	break;

  default:

	break;
  }
}


// This function is registered as a timeout callback

void XmwInterface::showTip(XtPointer clientData, XtIntervalId *)
{
  static Widget tipLabel_ = NULL;

  // Create the shell for tips.

  if ( !tipShell_ ) {

	tipShell_ = XtCreatePopupShell("tipShell",
								   overrideShellWidgetClass,
								   theBaseInterface->widget(),
								   NULL, 0);
	assert(tipShell_);

	XmtCreateQueryChildren(tipShell_, "tipLabel", &tipLabel_, NULL);
	assert(tipLabel_);

	XtVaSetValues(tipShell_,
				  XtNsaveUnder, True,
				  XtNborderColor, BlackPixelOfScreen(XtScreen(tipShell_)),
				  XtNborderWidth, 1,
				  NULL );
  }
   
  Widget w = (Widget) clientData;

  static int dispWidth = DisplayWidth( display_, 0 );
  static int dispHeight = DisplayHeight( display_, 0 );

  XmFontList fontList;
  XmString labelStr;
  Dimension height, labelWidth, labelHeight;
  Position rootX, rootY;
  const char *tip;

  XtVaGetValues( tipLabel_, XmNfontList, &fontList, NULL );
		 
  // Find the tip string
   
  tip = XmtLocalize2(w, "", "tips", tipTag_);

  // Do not display tip if blank.
   
  if (*tip == '\0') return;

  // Calculate tip string position

  labelStr = XmtCreateXmString(tip);
  XtVaSetValues(tipLabel_, XmNlabelString, labelStr, NULL);
  labelWidth = XmStringWidth(fontList, labelStr);
  labelHeight = XmStringHeight(fontList, labelStr);
  XmStringFree(labelStr);
		 
  XtTranslateCoords(w, (Position)0, (Position)0, &rootX, &rootY);

  XtVaGetValues(w, XmNheight, &height, NULL);

  // Arrange for not displaying out of the display screen.

  rootY += height;
  labelWidth += 8;
  labelHeight += 4;
  if ( rootX < 0 ) rootX = 0;
  if ( rootY < 0 ) rootY = 0;
  if ( dispWidth < rootX + labelWidth ) {
	rootX = dispWidth - labelWidth;
  }
  if ( dispHeight < rootY + labelHeight ) {
	rootY = dispHeight - labelHeight;
  }

  // Set the position of the popup box

  XtVaSetValues(tipShell_,
				XmNx, rootX,
				XmNy, rootY,
				XmNwidth, labelWidth,
				XmNheight, labelHeight,
				NULL );
    
  // Display the tip information

  XtRealizeWidget(tipShell_);
  XtPopup(tipShell_, XtGrabNone);

  tipTag_ = NULL;
}


Cursor XmwInterface::getCursor( unsigned int shape )
{
  static Cursor fleur = XCreateFontCursor( display_, XC_fleur );
  static Cursor sizing = XCreateFontCursor( display_, XC_sizing );
  static Cursor dotbox = XCreateFontCursor( display_, XC_dotbox );
  static Cursor cross = XCreateFontCursor( display_, XC_cross );
  static Cursor exchange = XCreateFontCursor( display_, XC_exchange );
  static Cursor question = XCreateFontCursor( display_, XC_question_arrow );
  static Cursor tcross = XCreateFontCursor( display_, XC_tcross );
  static Cursor hand1 = XCreateFontCursor( display_, XC_hand1 );
  static Cursor hand2 = XCreateFontCursor( display_, XC_hand2 );
  static Cursor xterm = XCreateFontCursor( display_, XC_xterm );
  static Cursor pencil = XCreateFontCursor( display_, XC_pencil );

  switch ( shape ) {
  case XC_fleur:
    return fleur;
  case XC_sizing:
    return sizing;
  case XC_dotbox:
    return dotbox;
  case XC_cross:
    return cross;
  case XC_exchange:
    return exchange;
  case XC_question_arrow:
    return question;
  case XC_tcross:
    return tcross;
  case XC_hand2:
    return hand2;
  case XC_xterm:
    return xterm;
  case XC_pencil:
    return pencil;
  case XC_hand1:
  default:
    return hand1;
  };

  return 0;
}
