//-*-c++-*------------------------------------------------------------
// XmwPoint.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class definition for the class XmwPoint
//--------------------------------------------------------------------

#ifndef XmwPOINT_HEADER
#define XmwPOINT_HEADER

#include <iosfwd>
//using namespace std;

class XmwPoint
{
   protected:

      int x_;
      int y_;

   public:
      
      XmwPoint();
      XmwPoint(int x, int y);
      XmwPoint(const XmwPoint &p);

      virtual ~XmwPoint() { }

      inline int x() const { return x_; }
      inline int y() const { return y_; }

      inline void set(int x, int y) {
		 x_ = x;
		 y_ = y;
      } 
      void x(int x) { x_ = x; }
      void y(int y) { y_ = y; }

      XmwPoint& operator = (const XmwPoint& p) {
		 x_ = p.x_; y_ = p.y_;
		 return *this;
      }

      int operator == (const XmwPoint & p) {
		 return ((p.x_ == x_) && (p.y_ == y_));
      }

      int operator [] (int i) const {
		 if (i == 0) return x_;
		 else if (i == 1) return y_;
		 else return 0;
      }

	  int distance(const XmwPoint& p);
}; 

std::ostream& operator << (std::ostream &os, const XmwPoint &p); 

#endif
