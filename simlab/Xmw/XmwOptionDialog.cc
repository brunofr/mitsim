//-*-c++-*------------------------------------------------------------
// XmwOptionDialog.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <cassert>
#include <iostream>
#include <cstdlib>

#include <Xmt/Xmt.h>
#include <Xmt/Dialog.h>
#include <Xmt/Chooser.h>
#include <Xmt/Create.h>

#include <Xmw/XmwXbaeList.h>
#include <Xmw/XmwModeline.h>

#include <IO/Exception.h>
#include <Tools/ToolKit.h>
#include <Tools/Options.h>
#include <Tools/SimulationEngine.h>

#include "XmwOptionDialog.h"
#include "XmwInterface.h"
#include "XmwMenu.h"
using namespace std;

XtResource XmwOptionDialog::resources[] =
{
  { "verbose", "verbose", XtRInt,
	sizeof(int), XtOffsetOf(OptionInfo, verbose),
	XtRImmediate, (XtPointer)ToolKit::VERBOSE_NORMAL
  },
  { "nice", "nice", XtRInt,
	sizeof(int), XtOffsetOf(OptionInfo, nice),
	XtRImmediate, (XtPointer)ToolKit::NICE_PRIMARY
  }
};

XmwOptionDialog::XmwOptionDialog(Widget parent)
  : XmwDialogManager(
					 parent, "optionDialog",
					 XmwOptionDialog::resources,
					 XtNumber(XmwOptionDialog::resources))
{
  XtVaSetValues(widget_,
				XmNdialogStyle, XmDIALOG_PRIMARY_APPLICATION_MODAL,
				NULL);

  theOptions->verbose(ToolKit::verbose());
  theOptions->nice(ToolKit::nice());
  buildBreakPoints();
}


// These overload the virtual functions defined in the based class

void XmwOptionDialog::okay(Widget w, XtPointer tag, XtPointer data)
{
  XmwXbaeList *list = getXmwXbaeList(bps_);
  list->commitEdit();
  int num = list->nRows();
  theOptions->allocBreakPoints(num);
  for (int i = 0; i < num; i ++) {
	double t = theBaseInterface->convertTime(list->getCell(i, 0));
	theOptions->setBreakPoint(i, t);
  }

  XmtDialogOkayCallback(w, tag, data);

  ToolKit::verbose(theOptions->verbose());
  ToolKit::nice(theOptions->nice());

  theSimulationEngine->parseBreakPoints(num, theOptions->breakPoints());
}

void XmwOptionDialog::cancel(Widget w, XtPointer tag, XtPointer data)
{
  XmtDialogCancelCallback(w, tag, data);
}

void XmwOptionDialog::help(Widget, XtPointer, XtPointer)
{
  theBaseInterface->openUrl("options", "smc.html");
}


void XmwOptionDialog::buildBreakPoints()
{
  bps_ = XmtNameToWidget(widget_, "*breaks");
  assert(bps_);

  XmwXbaeList *list = getXmwXbaeList(bps_);

  static short int widths[] = { 8 };

  list->setColAttrs(1, widths, NULL);
  list->deleteAll();

  int i, n = theOptions->nBreakPoints();
  String *points = new String [n];
  for (i = 0; i < n; i ++) {
	double t = theOptions->breakPoint(i);
	const char *p = theBaseInterface->convertTime(t);
	points[i] = ::Copy(p);
  }
  list->appendRows(points, NULL, n);
  for (i = 0; i < n; i ++) {
	delete [] points[i];
  }
  delete [] points;
}

void XmwOptionDialog::post()
{
  XmtDialogDo(widget(), theOptions);
  XmwDialogManager::ready();
}
