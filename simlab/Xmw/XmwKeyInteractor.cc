//-*-c++-*------------------------------------------------------------
// XmwKeyInteractor.C
//
// Peter Welch and Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class implementation for the class
// XmwKeyInteractor.  See XmwKeyInteractor.h for more information.
//--------------------------------------------------------------------


#include "XmwKeyInteractor.h"
#include "XmwDrawingArea.h"

XmwKeyInteractor::XmwKeyInteractor(XmwDrawingArea * drawing_area) 
  : drawing_area_(drawing_area),
	modifier_(0), lastKeySym_(0)
{
  Widget w = drawing_area_->widget();

  XtAddEventHandler (w, KeyPressMask, FALSE, 
					 (XtEventHandler) KeyPressHandler, this);

  XtAddEventHandler (w, KeyReleaseMask, FALSE, 
					 (XtEventHandler) KeyReleaseHandler, this);
}

XmwKeyInteractor::~XmwKeyInteractor()
{
  Widget w = drawing_area_->widget();

  XtRemoveEventHandler (w, KeyPressMask, FALSE, 
						(XtEventHandler) KeyPressHandler, this);

  XtRemoveEventHandler (w, KeyReleaseMask, FALSE, 
						(XtEventHandler) KeyReleaseHandler, this);
}


void
XmwKeyInteractor::KeyPressHandler(Widget, XtPointer client, 
								  XEvent * event, Boolean*) 
{
  KeySym keysym;
  int size = 20;
  char dummy_buffer[20];
  XComposeStatus compose;
  XLookupString((XKeyEvent*)event, dummy_buffer, size, &keysym, &compose);
  ((XmwKeyInteractor*) client)->keyPress(keysym);
}

void
XmwKeyInteractor::KeyReleaseHandler(Widget, XtPointer client, 
									XEvent * event, Boolean*) 
{
  KeySym keysym;
  int size = 20;
  char dummy_buffer[20];
  XComposeStatus compose;
  XLookupString((XKeyEvent*)event, dummy_buffer, size, &keysym, &compose);
  ((XmwKeyInteractor*) client)->keyRelease(keysym);
}

Boolean XmwKeyInteractor::keyPress(KeySym keysym)
{
  lastKeySym_ = keysym ;
  if (IsModifierKey(keysym)) {
	modifier_ = keysym;
	return True;
  } else if (keysym == XK_Escape) {
	return True;
  } else {
	return False;
  }
}

// Reset the modifier_ field to 0.

Boolean XmwKeyInteractor::keyRelease(KeySym keysym)
{
  if (IsModifierKey(keysym)) {
	modifier_ = 0;	
	return True;
  } else {
	return False;
  }
}
