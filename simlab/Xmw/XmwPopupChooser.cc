//-*-c++-*------------------------------------------------------------
// XmwPopupChooser.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <iostream>
#include <cassert>

#include <Xm/PushB.h>
#include <Xm/SeparatoG.h>

#include "XmwPopupChooser.h"
#include "XmwPoint.h"
using namespace std;

XmwPopupChooser::XmwPopupChooser(Widget parent, XtCallbackProc cb)
  : parent_(parent)
{
   create(cb);
}

XmwPopupChooser::XmwPopupChooser(Widget parent,
				 int num,
				 const vector<string>& labels, 
				 XtCallbackProc cb,
				 const vector<XtPointer> *client_data)
  : parent_(parent)
{
   create(cb);

   // Add choices as push buttons

   //int nl = labels.size();
   for (int i = 0; i < num; i ++) {
	  add(labels[i], client_data ? (*client_data)[i] : 0);
   }
}

XmwPopupChooser::~XmwPopupChooser()
{
   if (popup_) XtDestroyWidget (popup_);
}


void XmwPopupChooser::create(XtCallbackProc cb)
{
   // Create a row-column popup menu

   popup_ = XmCreatePopupMenu(parent_, "popup", NULL, 0);
   assert (popup_);

   // Use the default callback if no one is supplied

   if (cb) cb_ = cb;
   else cb_ = choose_cb;

   nChoices_ = 0;
}

void XmwPopupChooser::add(const string &label, const XtPointer client_data)
{
   char *clabel = (char *) label.c_str();
   XmString str = XmStringCreateSimple(clabel);
   Widget button = XtVaCreateManagedWidget(clabel,
	  xmPushButtonWidgetClass, popup_,
	  XmNlabelString, str,
	  (XtPointer)NULL);
   XmStringFree(str);
   XtAddCallback(button, XmNactivateCallback, cb_, client_data);
   nChoices_ ++;
}

void XmwPopupChooser::popup(const XmwPoint *p)
{
   if (!nChoices_) return;		// empty

   // Before popuping the menu, add a cancel button

   Widget separator = XtVaCreateManagedWidget ( "separator",
	  xmSeparatorGadgetClass,
	  popup_, 
	  (XtPointer) NULL );

   char *cancel = "Cancel";
   XmString label = XmStringCreateSimple(cancel);
   Widget button = XtVaCreateManagedWidget(cancel,
	  xmPushButtonWidgetClass, popup_,
	  XmNlabelString, label,
	  (XtPointer)NULL);
   XmStringFree(label);
   XtAddCallback(button, XmNactivateCallback, cb_, NULL);

   // Set the position for the popup menu and manage it

   XButtonPressedEvent event;
   if (p) {						// Position specified
	 Position x, y;
	 XtTranslateCoords(parent_, p->x(), p->y(), &x, &y);
	 event.x_root = x;
	 event.y_root = y;
   } else {
	 Window root_window, child_window;
	 int root_x, root_y;
	 int window_x, window_y;
	 unsigned int mask;
	 // Query cursor
	 if (XQueryPointer(XtDisplay(popup_), XtWindow(popup_),
					   &root_window, &child_window,
					   &root_x, &root_y,
					   &window_x, &window_y, &mask) != False ) {
	   event.x_root = root_x;
	   event.y_root = root_y;
	 } else {						// Opps!
	   event.x_root = 0;
	   event.y_root = 0;
	 }
   }

   // Post the popup menu

   XmMenuPosition(popup_, &event);
   XtManageChild(popup_);
}

// This is the default callback procedure activated when user clicked
// a choice from the popup menu.  This callback can be overridden by
// the last parameter passed to the constructor.

void XmwPopupChooser::choose_cb(Widget w, XtPointer, XtPointer)
{
   cout << XtName(w) << endl;
}


#ifdef TEST_POPUP_CHOOSER

// ===================================================================
// Testing proceduces
// -------------------------------------------------------------------

#include <Xm/MainW.h>
#include <Xm/DrawingA.h>

void input_cb(Widget w, XtPointer client_data, XtPointer call_data)
{
   XmDrawingAreaCallbackStruct *cbs = (XmDrawingAreaCallbackStruct *) call_data;

   // Only the 3rd button is of interest

   if ( cbs->event->xany.type != ButtonPress ||
		cbs->event->xbutton.button != 3) {
	  return;
   }

   XButtonPressedEvent *event = (XButtonPressedEvent *) cbs->event;
   XPoint p;
   p.x = event->x_root;
   p.y = event->y_root;
   vector<string> choice;
   choice.push_back("Line");
   choice.push_back("Square");
   choice.push_back("Circle");

   XmwPopupChooser *menu = NULL;
   if (menu) delete menu;
   menu = new XmwPopupChooser(w, choice);
   menu->popup(&p);
}

int main(int argc, char *argv[])
{
   XtAppContext app;
  
   Widget toplevel = XtVaAppInitialize(&app, "demo", NULL, 0,
	  &argc, argv, NULL, NULL);

   Widget main_w = XtVaCreateManagedWidget("main_w",
	  xmMainWindowWidgetClass, toplevel,
	  NULL);

   Widget drawing_a = XtVaCreateManagedWidget("drawing_a",
	  xmDrawingAreaWidgetClass, main_w,
	  XmNwidth, 300,
	  XmNheight, 200,
	  NULL);

   XtAddCallback(drawing_a, XmNinputCallback, input_cb, NULL);

   XtRealizeWidget (toplevel);
   XtAppMainLoop(app);

   return 0;
}

#endif
