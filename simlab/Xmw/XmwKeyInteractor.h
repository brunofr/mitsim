//-*-c++-*------------------------------------------------------------
// XmwKeyInteractor.h
//
// Peter J. Welch and Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class definition for the class
// XmwKeyInteractor.  This is a base class which provides the hooks
// for key press and release events.
//
// KeyIneractor allows you to easily define reaction to key
// interactions.
//
// Creation:
//
//   To use the key interactor, you must create it specifying a
//   particular XmwDrawingArea.
//
// Usage:
//
//   Anytime a key is pressed while the mouse is in a XmwDrawingArea,
//   the function KeyPressAction(KeySym) is called.  When a key is
//   released KeyReleaseAction(KeySym) is called.
//
// These action functions are virtual and must be redefined in the
// derived class.
//--------------------------------------------------------------------

#ifndef XmwKEYINTERACTOR_HEADER
#define XmwKEYINTERACTOR_HEADER

#include <X11/keysym.h>
#include <X11/keysymdef.h>
#include <X11/Intrinsic.h>

class XmwDrawingArea;

class XmwKeyInteractor
{
public:

  XmwKeyInteractor(XmwDrawingArea * drawing_area);
  virtual ~XmwKeyInteractor();

  // These two return true if the key has been processed or false
  // otherwise

  virtual Boolean keyPress(KeySym);
  virtual Boolean keyRelease(KeySym);

  KeySym lastKeySym() { return lastKeySym_; }

protected:

  XmwDrawingArea * drawing_area_;
  KeySym modifier_;
  KeySym lastKeySym_;

private:

  static void KeyPressHandler(Widget w, XtPointer client, 
							  XEvent * event, Boolean*);

  static void KeyReleaseHandler(Widget w, XtPointer client, 
								XEvent * event, Boolean*);
};

#endif
