//-*-c++-*------------------------------------------------------------
// XmwCallback.h
//
// Didler Burton created from Douglas Young's guidelines
// Qi Yang copied from Didler Burton, Thanks to Didler.
//
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class definition/implementation for the class
//--------------------------------------------------------------------

#ifndef XmwCALLBACK_HEADER
#define XmwCALLBACK_HEADER

#include <Xm/Xm.h>				// includes from OSF/MOTIF (Xm)
#include <generic.h>			// includes from C++ library

class Callback {

   private:

	  // Generic callback function works for
	  // all callbacks
 
	  static void callbackStub(
		 Widget    w, 
		 XtPointer clientData, 
		 XtPointer callData ) {
		 Callback *obj = (Callback *) clientData;
		 obj->execute( w, obj->_clientData, callData );
	  }

   protected:

	  Widget _w;				// Widget for which callback is
                                // registered
	  String _name;				// The callback name needed to
                                // remove callback
	  XtPointer _clientData;	// Xt's idea of client data is used
								// for the this pointer,
								// so replace it with our own.

      // Tells whether the callback is removed or not.

	  int       _CBremoved; 

	  // Constructor registers callback and saves other info that
	  // might be needed later as data members

	  Callback( Widget w, String name, XtPointer clientData ) {
		 XtAddCallback( w, 
						name, 
						&Callback::callbackStub, 
						(XtPointer) this );
		 _w          = w;
		 _name       = name;
		 _clientData = clientData;
		 _CBremoved  = 0;
	  }

	  // Destructor removes the callback, if not yet removed.

	  virtual ~Callback() {
		 if ( !_CBremoved ) removeCallback();
	  }

	  // Execute must be overriden by derived classes

	  virtual void execute( Widget, XtPointer, XtPointer ) = 0;

   public:

	  // This function removes the callback, using saved data members.

	  virtual void removeCallback() {
		 XtRemoveCallback( _w, 
						   _name, 
						   &Callback::callbackStub, 
						   (XtPointer) this );
		 _CBremoved = 1;
	  }

};


// Define a macro which instantiates a Callback object and provides
// the 'this' pointer to the constructor.

#define CallbackDeclare(CLASS)	\
                                \
typedef void (CLASS::*name2(CLASS,PMF))( \
    Widget,                     \
    XtPointer,                  \
    XtPointer );                \
                                \
class name2(CLASS,Callback) : public Callback { \
                                \
  public:                       \
                                \
    name2(CLASS,Callback)(      \
       CLASS* obj,              \
       name2(CLASS,PMF) pmf,    \
       Widget w,                \
       String callbackName,     \
       XtPointer clientData )   \
    : Callback( w,              \
       callbackName,            \
       clientData ) {           \
                                \
    _obj = obj;                 \
    _pmf = pmf;                 \
  }                             \
                                \
  protected:                    \
                                \
    CLASS            *_obj;     \
    name2(CLASS,PMF)  _pmf;     \
                                \
  private:                      \
                                \
    virtual void execute(       \
       Widget w,                \
       XtPointer clientData,    \
       XtPointer callData ) {   \
                                \
    ( _obj->*_pmf )( w, clientData, callData ); \
  }                             \
};                              \
                                \
name2(CLASS,Callback) *addCallback( \
    Widget w,                   \
    String name,                \
    name2(CLASS,PMF) pmf,       \
    XtPointer clientData ) {    \
                                \
  name2(CLASS,Callback) *cb =   \
    new name2(CLASS,Callback)( this, \
    pmf,                        \
    w,                          \
    name,                       \
    clientData );               \
  return cb;                    \
}


#define Callback_declare(CLASS) \
CallbackDeclare(CLASS)

#define Callbackdeclare(CLASS)  \
CallbackDeclare(CLASS)

#define Callback_declare_protected(CLASS)  \
                                \
typedef void (CLASS::*name2(CLASS,PMF))( \
    Widget,                     \
    XtPointer,                  \
    XtPointer );                \
                                \
  protected:                    \
                                \
class name2(CLASS,Callback) : public Callback { \
                                \
  public:                       \
                                \
    name2(CLASS,Callback)(      \
       CLASS* obj,              \
       name2(CLASS,PMF) pmf,    \
       Widget w,                \
       String callbackName,     \
       XtPointer clientData )   \
    : Callback( w,              \
          callbackName,         \
          clientData ) {        \
                                \
      _obj = obj;               \
      _pmf = pmf;               \
    }                           \
                                \
  protected:                    \
                                \
    CLASS            *_obj;     \
    name2(CLASS,PMF)  _pmf;		\
                                \
  private:                      \
                                \
    virtual void execute(       \
       Widget w,                \
       XtPointer clientData,    \
       XtPointer callData ) {   \
                                \
    ( _obj->*_pmf )( w, clientData, callData ); \
  }                             \
};                              \
                                \
name2(CLASS,Callback) *addCallback( \
    Widget w,                   \
    String name,                \
    name2(CLASS,PMF) pmf,       \
    XtPointer clientData ) {    \
                                \
  name2(CLASS,Callback) *cb =   \
    new name2(CLASS,Callback)(  \
    this,                       \
    pmf,                        \
    w,                          \
    name,                       \
    clientData );               \
  return cb;                    \
}

#endif
