//-*-c++-*------------------------------------------------------------
// XmwSciPlotDialog.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <cstdio>
#include <cassert>
#include <iostream>

#include <Xm/ToggleB.h>
#include <Xmt/Xmt.h>
#include <Xmt/InputField.h>
#include <Xmt/Dialogs.h>
#include <Xmt/Chooser.h>
#include <Combo/ComboBox.h>

#include <Tools/ToolKit.h>
#include <Xmw/XmwCallback.h>

#include "XmwSciPlot.h"
#include "XmwSciPlotDialog.h"
#include "XmwInterface.h"
using namespace std;

XmwSciPlotDialog::XmwSciPlotDialog(XmwSciPlot *parent)
   : XmwDialogManager(parent->widget_, "sciplotProperties", NULL, 0,
					  "apply", "close", "help"),
	 sciplot_(parent), plot_(parent->plot_), current_(NULL), tag_(0)
{
   int i;
   XmString str;

   XtVaSetValues(widget_,
				 XmNdialogStyle, XmDIALOG_PRIMARY_APPLICATION_MODAL,
				 NULL);

   // Find the list tree widget

   tree_ = XmtNameToWidget (widget_, "*tree");
   assert (tree_);
   XtVaSetValues(tree_,
				 XtNdoIncrementalHighlightCallback, True,
				 NULL);

   ListTreeItem *item;

   ListTreeRefreshOff(tree_);

   addTreeItem(NULL, "Chart", ITEM_CHART);

   item = addTreeItem(NULL, "Axis", ITEM_NONE);
   addTreeItem(item, "X Axis", ITEM_X_AXIS);
   addTreeItem(item, "Y Axis", ITEM_Y_AXIS);

   item = addTreeItem(NULL, "Fonts", ITEM_NONE);
   addTreeItem(item, "Title", ITEM_TITLE_FONT);
   addTreeItem(item, "Axis",  ITEM_AXIS_FONT);
   addTreeItem(item, "Labels",ITEM_LABEL_FONT);

   series_root_ = addTreeItem(NULL, "Series", ITEM_NONE);

   ListTreeRefreshOn(tree_);

   addCallback(tree_,
			   XtNhighlightCallback,
			   &XmwSciPlotDialog::treeItemHighlight,
			   (XtPointer) NULL);

   addCallback(tree_,
			   XtNactivateCallback,
			   &XmwSciPlotDialog::treeItemActivate,
			   (XtPointer) NULL);

#ifdef OUT						// Not used
   addCallback(tree_,
			   XtNmenuCallback,
			   &XmwSciPlotDialog::treeItemMenu,
			   (XtPointer) NULL);
#endif

   // Find the widgets in chart group
   
   chart_ = XmtNameToWidget (widget_, "sciplotChart");
   assert(chart_);

   title_ = XmtNameToWidget (chart_, "title");
   assert(title_);

   chartOption_ = XmtNameToWidget (chart_, "chartOption");
   assert(chartOption_);

   marksize_ = XmtNameToWidget (chart_, "marksize");
   assert (marksize_);

   // Create a list of marker sizes

   static char* marker_sizes[] = { "2", "3", "4", "5", "6" };

   for (i = 0; i < XtNumber(marker_sizes); i ++) {
	  str = XmStringCreateLtoR(marker_sizes[i], XmSTRING_DEFAULT_CHARSET);
	  XmComboBoxAddItem(marksize_, str, 0);
	  XmStringFree(str);
   }

   // Find the widgets in axis group

   axis_ = XmtNameToWidget (widget_, "sciplotAxis");
   assert(axis_);

   label_ = XmtNameToWidget (axis_, "label");
   assert(label_);

   axisOption_ = XmtNameToWidget (axis_, "axisOption");
   assert(axisOption_);
   addCallback(axisOption_, XmNvalueChangedCallback,
	  &XmwSciPlotDialog::axisOptionChanged, NULL);


   autoScale_ = XmtNameToWidget (axis_, "autoScale");
   assert(autoScale_);
   addCallback(autoScale_, XmNvalueChangedCallback,
	  &XmwSciPlotDialog::autoScaleChanged, NULL);

   min_ = XmtNameToWidget (axis_, "min");
   assert(min_);

   max_ = XmtNameToWidget (axis_, "max");
   assert(max_);

   // Find the widgets in font group

   fonts_ = XmtNameToWidget (widget_, "sciplotFonts");
   assert(axis_);

   fontname_ = XmtNameToWidget (fonts_, "fontname");
   assert(fontname_);

   fontsize_ = XmtNameToWidget (fonts_, "fontsize");
   assert(fontsize_);

   fontstyle_ = XmtNameToWidget (fonts_, "fontstyle");
   assert(fontstyle_);

   // Create a list of font sizes

   static char* font_sizes[] = { " 8", "10", "12", "14", "18" };

   for (i = 0; i < XtNumber(font_sizes); i ++) {
	  str = XmStringCreateLtoR(font_sizes[i], XmSTRING_DEFAULT_CHARSET);
	  XmComboBoxAddItem(fontsize_, str, 0);
	  XmStringFree(str);
   }

   // Find the widget in series group

   series_ = XmtNameToWidget (widget_, "sciplotSeries");
   assert(series_);
   
   line_ = XmtNameToWidget (series_, "line");
   assert(line_);

   marker_ = XmtNameToWidget (series_, "marker");
   assert(marker_);

   color_ = XmtNameToWidget (series_, "color");
   assert(color_);
}


// These overload the virtual functions defined in the based class

void XmwSciPlotDialog::okay(Widget, XtPointer, XtPointer)
{
   pageDown();
   sciplot_->update();
}

void XmwSciPlotDialog::cancel(Widget, XtPointer, XtPointer)
{
   *ptr_ = NULL;
   destroy(widget_);
}

void XmwSciPlotDialog::help(Widget, XtPointer, XtPointer)
{
   if (theBaseInterface) {
	  theBaseInterface->openUrl("sciplot", "plot.html");
   }
}

// New callbacks

void XmwSciPlotDialog::autoScaleChanged(Widget w, XtPointer, XtPointer)
{
   if (XmToggleButtonGetState(autoScale_)) {
	  deactivate(min_);
	  deactivate(max_);
   } else {
	  activate(min_);
	  activate(max_);
   }
}

void XmwSciPlotDialog::axisOptionChanged(Widget, XtPointer, XtPointer)
{
   if (XmtChooserGetState(axisOption_) & 0x4) {	// logorithm
	  XmToggleButtonSetState(autoScale_, True, True);
	  deactivate(autoScale_);
   } else {
	  activate(autoScale_);
   }
}


// Highlight and menu callback are not used, left as an example

void XmwSciPlotDialog::treeItemHighlight(Widget,
										 XtPointer, XtPointer call)
{
   ListTreeMultiReturnStruct *ret = (ListTreeMultiReturnStruct *)call;

#ifdef OUT   
   cout << "Highlighted items:" << endl;

   for (int i = 0; i < ret->count; i ++) {
	  ListTreeItem *item;
	  item = ret->items[i];
	  cout << "\t" << item->text;
	  while (item = item->parent) {
		 cout << " <- " << item->text;
	  }
	  cout << endl;
   }
#endif

   if (ret->count != 1) return;	// work on one at a time

   int *pud = (int *)ret->items[0]->user_data;
   switchTo(*pud);
}

#ifdef OUT
void XmwSciPlotDialog::treeItemMenu(Widget,
									XtPointer, XtPointer call)
{
   ListTreeItemReturnStruct *ret = (ListTreeItemReturnStruct *)call;
   cout << ret->item->text << " menu" << endl;
}
#endif

void XmwSciPlotDialog::treeItemActivate(Widget,
										XtPointer, XtPointer call)
{
   ListTreeActivateStruct *ret = (ListTreeActivateStruct *)call;

#ifdef OUT
   cout << ret->item->text << endl;
   for (int i = 0; i < ret->count; i ++) {
	  cout << "\t" << ret->path[i]->text << endl;
   }
#endif

   int *pud = (int *)ret->item->user_data;
   switchTo(*pud);
}


void XmwSciPlotDialog::switchTo(int tag)
{
   if (tag == ITEM_NONE || tag == tag_) return;

   pageDown();
   tag_ = tag;
   Widget w = pageUp();

   if (w != current_) {
	  unmanage(current_);
	  current_ = w;
	  manage(current_);
   }
}

// Copy from buffer to dialog

Widget XmwSciPlotDialog::pageUp()
{
   Widget w;
   int option;
   const int n = 3;
   Boolean checks[n];

   switch (tag_) {

	  case ITEM_CHART:
		 w = chart_;
		 setChartPageInfo();
		 break;

	  case ITEM_X_AXIS:
		 w = axis_;
		 XtVaGetValues(plot_,
					   XtNshowXLabel,   &checks[0],
					   XtNxAxisNumbers, &checks[1],
					   XtNxLog,         &checks[2],
					   NULL);
		 setAxisPageInfo(0, checks, n);
		 break;

	  case ITEM_Y_AXIS:
		 w = axis_;
		 XtVaGetValues(plot_,
					   XtNshowYLabel,   &checks[0],
					   XtNyAxisNumbers, &checks[1],
					   XtNyLog,         &checks[2],
					   NULL);
		 setAxisPageInfo(1, checks, n);
		 break;

	  case ITEM_TITLE_FONT:
		 w = fonts_;
		 XtVaGetValues(plot_, XtNtitleFont, &option, NULL);
		 setFontPageInfo(option);
		 break;

	  case ITEM_AXIS_FONT:
		 w = fonts_;
		 XtVaGetValues(plot_, XtNaxisFont, &option, NULL);
		 setFontPageInfo(option);
		 break;

	  case ITEM_LABEL_FONT:
		 w = fonts_;
		 XtVaGetValues(plot_, XtNlabelFont, &option, NULL);
		 setFontPageInfo(option);
		 break;

	  default:					// data series
		 w = series_;
		 setLinePageInfo(tag_);
		 break;
   }
   return w;
}


void XmwSciPlotDialog::getChartPageInfo()
{
   static char* default_title = "SciPlot";
   const int n = 6;
   static Boolean checks[n];
   int size;

   // Get the value from the dialog

   String str = XmtInputFieldGetString(title_);
   if (!str) {
	  str = default_title;
   }
   XtVaSetValues(plot_, XtNplotTitle, str, NULL);
   sciplot_->title(str);

   str = XmComboBoxGetString(marksize_);
   size = atoi(str);
   free(str);
   if (size) {
	  XtVaSetValues(plot_, XtNdefaultMarkerSize, size, NULL);
   }

   int state = XmtChooserGetState(chartOption_);
   for (int i = 0; i < n; i ++) {
	  if (state & (1 << i)) checks[i] = True;
	  else checks[i] = False;
   }

   // Set the value to the plot

   XtVaSetValues(plot_,
				 XtNshowTitle,  checks[0],
				 XtNshowLegend, checks[1],
				 XtNdrawMajor, checks[2],
				 XtNdrawMajorTics, checks[3],
				 XtNdrawMinor, checks[4],
				 XtNdrawMinorTics, checks[5],
				 NULL);
}

void XmwSciPlotDialog::setChartPageInfo()
{
   char str[8];
   const int n = 6;
   Boolean checks[n];
   int size;

   // Get the current value from the plot

   XtVaGetValues(plot_,
				 XtNdefaultMarkerSize, &size,
				 XtNshowTitle,     &checks[0],
				 XtNshowLegend,    &checks[1],
				 XtNdrawMajor,     &checks[2],
				 XtNdrawMajorTics, &checks[3],
				 XtNdrawMinor,     &checks[4],
				 XtNdrawMinorTics, &checks[5],
				 NULL);

   // Set the value in the dialog

   XmtInputFieldSetString(title_, sciplot_->title_);
   sprintf(str, "%2d", size);
   XmComboBoxSetString(marksize_, str);
   int state = 0;
   for (int i = 0; i < n; i ++) {
	  if (checks[i]) state |= (1 << i);
   }
   XmtChooserSetState(chartOption_, state, False);
}

Boolean XmwSciPlotDialog::getAxisPageInfo(String str, Boolean checks[], int n,
   double scale[])
{
   String s = XmtInputFieldGetString(label_);
   if (s) strcpy(str, s);
   else *str = '\0';

   int state = XmtChooserGetState(axisOption_);
   for (int i = 0; i < n; i ++) {
	  if (state & (1 << i)) checks[i] = True;
	  else checks[i] = False;
   }

   scale[0] = atof(XmtInputFieldGetString(min_));
   scale[1] = atof(XmtInputFieldGetString(max_));

   Boolean automatic = XmToggleButtonGetState(autoScale_);

   // If user set scale is invalid, use auto scaling
   if (!automatic && scale[0] > scale[1] + 1.0E-7) {
	  automatic = True;
   }

   return automatic;
}

void XmwSciPlotDialog::setAxisPageInfo(int who, Boolean checks[], int n)
{
   char scale[32];
   Boolean automatic;
   String str = sciplot_->label(who);
   XmtInputFieldSetString(label_, str);
   int state = 0;
   for (int i = 0; i < n; i ++) {
	  if (checks[i]) state |= (1 << i);
   }
   XmtChooserSetState(axisOption_, state, False);

   if (checks[2]) {
	  automatic = False;
	  deactivate(autoScale_);
   } else {
	  automatic = sciplot_->autoScale(0x1 << who);
	  activate(autoScale_);
   }
   XmToggleButtonSetState(autoScale_, automatic, False);

   sprintf(scale, "%.2lf", sciplot_->scale(who*2));
   XmtInputFieldSetString(min_, scale);

   sprintf(scale, "%.2lf", sciplot_->scale(who*2+1));
   XmtInputFieldSetString(max_, scale);

   if (automatic) {
	  deactivate(min_);
	  deactivate(max_);
   } else {
	  activate(min_);
	  activate(max_);
   }
}

int XmwSciPlotDialog::getFontPageInfo()
{
   // See SciPlot.h for more information

   int option, size;

   char *str = XmComboBoxGetString(fontsize_);
   if (str && (size = atoi(str))) option = size;
   else option = XtFONT_SIZE_DEFAULT;
   free (str);

   int name = XmtChooserGetState(fontname_);
   option |= name << 8;

   int attribute = XmtChooserGetState(fontstyle_);
   option |= attribute << 12;

   return option;
}

void XmwSciPlotDialog::setFontPageInfo(int option)
{
   // See SciPlot.h for more information

   int size = option & XtFONT_SIZE_MASK;
   int name = (option & XtFONT_NAME_MASK) >> 8;
   int attribute = (option & XtFONT_ATTRIBUTE_MASK) >> 12;
   char str[8];
   sprintf(str, "%2d", size);

   XmtChooserSetState(fontname_, name, False);
   XmComboBoxSetString(fontsize_, str);
   XmtChooserSetState(fontstyle_, attribute, False);
}

void XmwSciPlotDialog::getLinePageInfo(int idnum)
{
   int pstyle = XmtChooserGetState(marker_);
   int lstyle = XmtChooserGetState(line_);
   int lcolor;
   if (sciplot_->sciColors_) {
	  lcolor = sciplot_->sciColors_[XmtChooserGetState(color_)];
   } else {
	  lcolor = 1;
   }
   SciPlotListSetStyle(plot_, idnum, lcolor, pstyle, lcolor, lstyle);
}

void XmwSciPlotDialog::setLinePageInfo(int idnum)
{
   int pstyle, lcolor, lstyle;
   SciPlotListGetStyle(plot_, idnum, &lcolor, &pstyle, &lcolor, &lstyle);
   XmtChooserSetState(marker_, pstyle, False);
   XmtChooserSetState(line_, lstyle, False);
   XmtChooserSetState(color_, lcolor, False);
}

// Copy from dialog to buffer

Widget XmwSciPlotDialog::pageDown()
{
   int option;
   char str[64];
   const int n = 3;
   Boolean checks[n];
   Boolean automatic;
   double scale[2];

   switch (tag_) {

	  case ITEM_CHART:
		 getChartPageInfo();
		 break;

	  case ITEM_X_AXIS:
		 automatic = getAxisPageInfo(str, checks, n, scale);
		 if (IsEmpty(str)) strcpy(str, "X Axis");
		 sciplot_->xlabel(str);
		 XtVaSetValues(plot_,
					   XtNxLabel,       str,
					   XtNshowXLabel,   checks[0],
					   XtNxAxisNumbers, checks[1],
					   XtNxLog,         checks[2],
					   NULL);
		 if (automatic) {
			SciPlotSetXAutoScale(plot_);
			sciplot_->setAutoScale(0x1);
		 } else {
			sciplot_->scale(0, scale[0]);
			sciplot_->scale(1, scale[1]);
			sciplot_->unsetAutoScale(0x1);
			SciPlotSetXUserScale(plot_, scale[0], scale[1]);
		 }
		 break;

	  case ITEM_Y_AXIS:
		 automatic = getAxisPageInfo(str, checks, n, scale);
		 if (IsEmpty(str)) strcpy(str, "Y Axis");
		 sciplot_->ylabel(str);
		 XtVaSetValues(plot_,
					   XtNyLabel,       str,
					   XtNshowYLabel,   checks[0],
					   XtNyAxisNumbers, checks[1],
					   XtNyLog,         checks[2],
					   NULL);
		 if (automatic) {
			SciPlotSetYAutoScale(plot_);
			sciplot_->setAutoScale(0x2);
		 } else {
			sciplot_->scale(2, scale[0]);
			sciplot_->scale(3, scale[1]);
			sciplot_->unsetAutoScale(0x2);
			SciPlotSetYUserScale(plot_, scale[0], scale[1]);
		 }
		 break;

	  case ITEM_TITLE_FONT:
		 option = getFontPageInfo();
		 XtVaSetValues(plot_, XtNtitleFont, option, NULL);
		 break;

	  case ITEM_AXIS_FONT:
		 option = getFontPageInfo();
		 XtVaSetValues(plot_, XtNaxisFont, option, NULL);
		 break;

	  case ITEM_LABEL_FONT:
		 option = getFontPageInfo();
		 XtVaSetValues(plot_, XtNlabelFont, option, NULL);
		 break;

	  default:
		 getLinePageInfo(tag_);
		 break;
   }
   return current_;
}

ListTreeItem* XmwSciPlotDialog::addTreeItem(ListTreeItem *parent,
											char *label, int tag)
{
   ListTreeItem* item = ListTreeAdd(tree_, parent, label);
   int *p = new int;
   *p = tag;
   item->user_data = (XtPointer) p;
   return item;
}


void XmwSciPlotDialog::post_property_dialog(XmwSciPlotDialog **ptr)
{
   ptr_ = ptr;
   manage(widget_);
   XmtWaitUntilMapped ( widget_ );

   if (current_) return;

   ListTreeRefreshOff(tree_);
   for (int i = 0; i < sciplot_->nSeries(); i ++) {
	  const char *name = SciPlotListName(plot_, sciplot_->series(i));
	  addTreeItem(series_root_, (char*)name, sciplot_->series(i));
   }
   ListTreeRefreshOn(tree_);

   tag_ = ITEM_CHART;
   current_ = pageUp();
   manage(current_);
   theBaseInterface->showDefaultCursor();
}
