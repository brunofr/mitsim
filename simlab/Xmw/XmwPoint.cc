//-*-c++-*------------------------------------------------------------
// XmwPoint.C
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <iostream>
#include <cmath>
#include "XmwPoint.h"
using namespace std;

XmwPoint::XmwPoint()
{
   x_ = y_ = 0;
}

XmwPoint::XmwPoint(int x, int y)
   : x_(x), y_(y)
{
}

XmwPoint::XmwPoint(const XmwPoint& p)
{
   x_ = p.x_;
   y_ = p.y_;
}

int XmwPoint::distance(const XmwPoint& p)
{
   int dx = p.x_ - x_;
   int dy = p.y_ - y_;
   return (int) sqrt(dx * dx + dy * dy);
}

ostream& operator << (ostream &os, const XmwPoint &p) 
{
   return os << p.x() << " " << p.y();
}

	
