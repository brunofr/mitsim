//-*-c++-*------------------------------------------------------------
// XmwMouseInteractor.h
//
// Peter Welch and Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class definition for the class
// XmwMouseInteractor.  This class handles all interactions with the
// mouse and provides a callback capability for mouse events (for
// combinations of mouse and key modifier events (shift+button1
// ctrl+button1 and so on).  This must be associated with a drawing
// area.
//--------------------------------------------------------------------

#ifndef XmwMOUSEINTERACTOR_HEADER
#define XmwMOUSEINTERACTOR_HEADER

#include "XmwPoint.h"

#include <Xm/Xm.h>

class XmwDrawingArea;

class XmwMouseInteractor
{
public:

  XmwMouseInteractor(XmwDrawingArea * drawing_area);
  virtual ~XmwMouseInteractor();

  // All callbacks default to these three.
      
  virtual void buttonDown  ( XmwPoint&  ) { }
  virtual void buttonUp    ( XmwPoint&  ) { }
  virtual void buttonMotion( XmwPoint&  ) { }


  // Left Button

  virtual void leftButtonDown  ( XmwPoint& pt ) {}
  virtual void leftButtonUp    ( XmwPoint& pt ) {}
  virtual void leftButtonMotion( XmwPoint& pt ) {}

  virtual void leftButtonControlDown  ( XmwPoint& pt ) {}
  virtual void leftButtonControlUp( XmwPoint& pt ) {}
  virtual void leftButtonControlMotion( XmwPoint& pt ) {}

  virtual void leftButtonShiftDown( XmwPoint& pt ) {}
  virtual void leftButtonShiftUp( XmwPoint& pt ) {}
  virtual void leftButtonShiftMotion( XmwPoint& pt ) {}

  virtual void leftButtonMetaDown( XmwPoint& pt ) {}
  virtual void leftButtonMetaUp( XmwPoint& pt ) {}
  virtual void leftButtonMetaMotion( XmwPoint& pt ) {}

  virtual void leftButtonAltDown( XmwPoint& pt ) {}
  virtual void leftButtonAltUp( XmwPoint& pt ) {}
  virtual void leftButtonAltMotion( XmwPoint& pt ) {}

  virtual void leftButtonEscapeDown( XmwPoint& pt ) {}
  virtual void leftButtonEscapeUp( XmwPoint& pt ) {}
  virtual void leftButtonEscapeMotion( XmwPoint& pt ) {}

  // Middle Button

  virtual void middleButtonDown( XmwPoint& pt ) {}
  virtual void middleButtonUp( XmwPoint& pt ) {}
  virtual void middleButtonMotion( XmwPoint& pt ) {}

  virtual void middleButtonControlDown( XmwPoint& pt ) {}
  virtual void middleButtonControlUp( XmwPoint& pt ) {}
  virtual void middleButtonControlMotion( XmwPoint& pt ) {}

  virtual void middleButtonEscapeDown( XmwPoint& pt ) {}
  virtual void middleButtonEscapeUp( XmwPoint& pt ) {}
  virtual void middleButtonEscapeMotion( XmwPoint& pt ) {}

  virtual void middleButtonShiftDown( XmwPoint& pt ) {}
  virtual void middleButtonShiftUp( XmwPoint& pt ) {}
  virtual void middleButtonShiftMotion( XmwPoint& pt ) {}

  virtual void middleButtonMetaDown( XmwPoint& pt ) {}
  virtual void middleButtonMetaUp( XmwPoint& pt ) {}
  virtual void middleButtonMetaMotion( XmwPoint& pt ) {}

  virtual void middleButtonAltDown( XmwPoint& pt ) {}
  virtual void middleButtonAltUp( XmwPoint& pt ) {}
  virtual void middleButtonAltMotion( XmwPoint& pt ) {}

  // Right Button

  virtual void rightButtonDown( XmwPoint& pt ) {}
  virtual void rightButtonUp( XmwPoint& pt ) {}
  virtual void rightButtonMotion( XmwPoint& pt ) {}

  virtual void rightButtonControlDown( XmwPoint& pt ) {}
  virtual void rightButtonControlUp( XmwPoint& pt ) {}
  virtual void rightButtonControlMotion( XmwPoint& pt ) {}

  virtual void rightButtonShiftDown( XmwPoint& pt ) {}
  virtual void rightButtonShiftUp( XmwPoint& pt ) {}
  virtual void rightButtonShiftMotion( XmwPoint& pt ) {}

  virtual void rightButtonMetaDown( XmwPoint& pt ) {}
  virtual void rightButtonMetaUp( XmwPoint& pt ) {}
  virtual void rightButtonMetaMotion( XmwPoint& pt ) {}
    
  virtual void rightButtonAltDown ( XmwPoint& pt ) {}
  virtual void rightButtonAltUp( XmwPoint& pt ) {}
  virtual void rightButtonAltMotion( XmwPoint& pt ) {}
    
  virtual void rightButtonEscapeDown( XmwPoint& pt ) {}
  virtual void rightButtonEscapeUp( XmwPoint& pt ) {}
  virtual void rightButtonEscapeMotion( XmwPoint& pt ) {}

  enum {      
   MODIFIER_ALT     = 0x01,
   MODIFIER_CONTROL = 0x02,
   MODIFIER_ESCAPE  = 0x04,
   MODIFIER_META    = 0x08,
   MODIFIER_SHIFT   = 0x10
  };

  unsigned int modifier(unsigned int flag = 0xFF) {
	return (modifier_ & flag);
  }
  void setModifier(unsigned int flag) {
	modifier_ |= flag;
  }
  void unsetModifier(unsigned int flag = 0) {
	modifier_ &= ~flag;
  }

protected:

  XmwDrawingArea* drawing_area_;
  unsigned int modifier_;

private:

  static void ButtonPressHandler(Widget w, XtPointer client, 
								 XEvent *event, Boolean*);

  static void ButtonMotionHandler(Widget w, XtPointer client, 
								  XEvent * event, Boolean*);

  static void ButtonReleaseHandler(Widget w, 
								   XtPointer client, 
								   XEvent * event, 
								   Boolean*);
 
  // Used for modifier keys

  static void KeyPressCallback(Widget w, 
							   XtPointer client,
							   XEvent * event,
							   Boolean*);

  static void KeyReleaseCallback(Widget w, 
								 XtPointer client,
								 XEvent * event,
								 Boolean*);
};

#endif
