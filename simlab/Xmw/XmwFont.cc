//-*-c++-*------------------------------------------------------------
// XmwFont.cc
//
// Qi Yang and Peter J. Welch
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class implementation for the class XmwFont.
//--------------------------------------------------------------------

#include <iostream>
#include <sstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
using namespace std;

#include <Tools/ToolKit.h>

#include "XmwFont.h"
#include "XmwInterface.h"
using namespace std;

// Default the font sizes. If the font names below is provided by
// parameter parser, they are used in replace of the defaults.

int FONT_SIZES[] = { 80, 100, 120, 140, 160 };
int *theFontSizes = FONT_SIZES;
int theNumFonts = sizeof(FONT_SIZES) / sizeof(int);
char * theFixedFont    = "fixed";
char * theScalableFont =
"-adobe-helvetica-medium-r-normal--*-*-*-*-p-*-*-1";


XmwFont *theFontTable = NULL;

XmwFont::XmwFont()
{
   display_ = theBaseInterface->display();

   int s = DefaultScreen(display_);
   nFonts_ = theNumFonts;

   if (ToolKit::verbose()) {
	  cout << "Initializing fonts ";
   }

   // calculate our screen resolution in dots per inch 25.4mm = 1 inch

   xres_ = DisplayWidth(display_, s) / DisplayWidthMM(display_, s) * 25.4;     

   yres_ = DisplayHeight(display_, s) / DisplayHeightMM(display_, s) * 25.4;   


   theFixedFont = XmtLocalize2(theBaseInterface->widget(),
							   theFixedFont, "fonts", "fixed");

   if (!(fixed_ = XLoadQueryFont(display_, theFixedFont))) {
	  cerr << "Warning:: Cannot load font <"
		   << theFixedFont << ">." << endl;
   }

   theScalableFont = XmtLocalize2(theBaseInterface->widget(),
								  theScalableFont, "fonts", "scalable");
   scalable_ = new XFontStruct * [nFonts_];

   for (register int i = 0; i < nFonts_; i ++) {
	  cout << "."; cout.flush();
      scalable_[i] = queryScalableFont(theFontSizes[i], theScalableFont);
   }

   if (ToolKit::verbose()) {
	  cout << " " << theNumFonts << " fonts done." << endl;
   }
}


XmwFont::~XmwFont()
{
   XFreeFont(display_, fixed_);
   for (int i = 0; i < nFonts_; i ++) {
	  XFreeFont(display_, scalable_[i]);
   }
   delete [] scalable_;
}

//--------------------------------------------------------------------
// returns a font structure with given name
//--------------------------------------------------------------------

XFontStruct * 
XmwFont::queryScalableFont(int size, const char * font_name)
{
   XFontStruct * font;

   char *name = ::Copy(font_name); // work on copy

   // copy the font name, changing the scalable fields as we do so

   std::ostringstream os;
   char *s = name + 1, *e;
   int field = 1;
   os << '-';
   while (e = strchr(s, '-')) {
      switch (field) {
		 case 7:				// pixel size
		 case 12:				// average width
			{
			   os << '*';			// change from "-0-" to "-*-"
			   break;        
			}
		 case 8:				// change points from "-0-" to "-size-"
			{
			   os << size;
			   break;
			}
		 case 9:				// sepcify x-resolution
			{
			   os << (int)(xres_ + 0.5);
			   break;
			}
		 case 10:				// sepcify y-resolution
			{
			   os << (int)(yres_ + 0.5);
			   break;
			}
		 default:				// other fields are copied
			{
			   *e = '\0';			// terminated the field
			   os << s;
			   break;
			}
      }
      os << '-';
      s = e + 1;				// next field
      field ++;
   }

   if (*s) os << s;				// the last field
   //os << '\0';
   const string& temp_str = os.str();
   s = const_cast<char*>(temp_str.c_str()); // safe only if XLoadQueryFont does not change s

   // If there are not 14 hyphens, it is not a well formed name.

   if (field != 14) {
      cerr << "Warning:: Invalid font name <"
		   << font_name << ">." << endl;
      font = fixed_;
   } else if ((font = XLoadQueryFont(display_, s)) == NULL) {
      cerr << "Warning:: Failed loading font <"
		   << s << ">." << endl;
      font = fixed_;
   }

   free(name);
   //delete [] s; // no longer necessary?

   return font; 
}


XFontStruct * XmwFont::font(int i)
{ 
   if (i < 0) {
      return scalable_[0];
   } else if (i > nFonts_ - 1) {
      return scalable_[nFonts_ - 1];
   } else {
      return scalable_[i];
   }
}
