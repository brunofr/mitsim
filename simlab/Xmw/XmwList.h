//-*-c++-*------------------------------------------------------------
// XmwList.h
//
// Provides a list interface, which contains selectable items includes
// from 'MotifUI' library
//
// Qi Yang -- based an example by Didier Burton
//
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef XmwLIST_HEADER
#define XmwLIST_HEADER

#include "XmwUIWidget.h"
#include "XmwCustom.h"

class XmwList : public XmwUIWidget {

   public:

	  XmwList( Widget, const String,
			   ArgList args = NULL, Cardinal argc = 0
		 );

	  int  visibleItems();
	  void setVisibleItems( int );
	  void addItem( const char *, int );
	  void addItemsAndFreeStrs( XmString *, int );
	  void deleteItem( const char * );
	  void deleteAllItems();
	  void setBottomItem( const char *, Boolean call = False );
	  void setBottomPos( int );
	  void setTopPos( int );
	  void replaceItem( int, const char *, Boolean call = False );
	  void selectItem( int, Boolean call = False  );
	  void deselectItem( int );
	  void deselectAllItems();

	  int  getSingleSelectedPos();
	  int  nItems() const { return nItems_; }

	  virtual const char * className() const;

   protected:

	  int nItems_;
};

// declare a creator for automatic widget creation with Xmt resources.

CustomDeclare(XmwList)

#endif
