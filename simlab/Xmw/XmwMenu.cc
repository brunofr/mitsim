//-*-c++-*------------------------------------------------------------
// XmwMenu.cc
//
// qiyang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <iostream>
#include <cassert>

#include <Xmt/Xmt.h>
#include <Xmt/WidgetType.h>
#include <Xmt/Create.h>
#include <Xmt/Converters.h>
#include <Xmt/Dialogs.h>
#include <Xmt/Menu.h>

#include "XmwMenu.h"
#include "XmwInterface.h"
using namespace std;

XmwMenu* theBaseMenu = NULL;

XmwMenu::XmwMenu(Widget parent)
  : XmwWidget("menubar"), state_(0)
{
  widget_ = XmtNameToWidget(parent, "*menubar");
  assert(widget_);

  if (open_ = getItem("Open")) {
	XtAddCallback(getItemWidget(open_),
				  XmNactivateCallback, &XmwMenu::openCB,
				  this );
  }
  if (save_ = getItem("Save")) {
	XtAddCallback(getItemWidget(save_),
				  XmNactivateCallback, &XmwMenu::saveCB,
				  this );
	deactivate(save_);
  }
  if (saveAs_ = getItem("SaveAs")) {
	XtAddCallback(getItemWidget(saveAs_),
				  XmNactivateCallback, &XmwMenu::saveAsCB,
				  this );
  }
  if (print_ = getItem("Print")) {
	XtAddCallback(getItemWidget(print_),
				  XmNactivateCallback, &XmwMenu::printCB,
				  this );
  }
  if (quit_ = getItem("Quit")) {
	XtAddCallback(getItemWidget(quit_),
				  XmNactivateCallback, &XmwMenu::quitCB,
				  this );
  }

  Widget w;

  if (w = getItemWidget("Randomizer")) {
	XtAddCallback(w,
				  XmNactivateCallback, &XmwMenu::randomizerCB,
				  this );
  }

  if (w = getItemWidget("About")) {
	XtAddCallback(w,
				  XmNactivateCallback, &XmwMenu::aboutCB,
				  this );
  }
  if (w = getItemWidget("ContextHelp")) {
	XtAddCallback(w,
				  XmNactivateCallback, &XmwMenu::contextHelpCB,
				  this );
  }
  if (w = getItemWidget("Index")) {
	XtAddCallback(w,
				  XmNactivateCallback, &XmwMenu::indexCB,
				  this );
  }
  if (w = getItemWidget("Credit")) {
	XtAddCallback(w,
				  XmNactivateCallback, &XmwMenu::creditCB,
				  this );
  }
}

void XmwMenu::activate(XmtMenuItem *item)
{
  XmtMenuItemSetSensitivity(item, True);
}

void XmwMenu::deactivate(XmtMenuItem *item)
{
  Widget w = XmtMenuItemGetWidget(item);
  if (XtIsSensitive(w)) {
	XmtMenuItemSetSensitivity(item, False);
  }
}

void XmwMenu::needSave(unsigned int what)
{
  activate(save_);
  setState(what);
}

void XmwMenu::setToggleState(XmtMenuItem *item, Boolean state)
{
  XmtMenuItemSetState(item, state, False);
}

Widget XmwMenu::getItemWidget(Widget parent, const char *name)
{
  XmtMenuItem *item = XmtMenuGetMenuItem(parent, name);
  if (item) return XmtMenuItemGetWidget(item);
  else return NULL;
}

Widget XmwMenu::getItemWidget(const char *name)
{
  XmtMenuItem *item = getItem(name);
  if (item) {
	return getItemWidget(item);
  } else {
	return NULL;
  }
}

XmtMenuItem *XmwMenu::getItem(const char *name)
{
  if (!widget_) return NULL;
  XmtMenuItem *item = XmtMenuGetMenuItem(widget_, name);
  if (item) return item;
  XmtDisplayErrorMsgAndWait(widget_, NULL,
							"Menu item '%s' not found.",
							"Error",					// title
							NULL,						// help msg
							name);
  return NULL;
}

Widget XmwMenu::getItemWidget(XmtMenuItem *item)
{
  if (item) return XmtMenuItemGetWidget(item);
  else return NULL;
}

// Virtual functions to be overloaded in derived classes

void XmwMenu::open ( Widget, XtPointer, XtPointer)
{
  cerr << "Open!" << endl;
}

void XmwMenu::save ( Widget, XtPointer, XtPointer)
{
  cerr << "Save!" << endl;
}

void XmwMenu::saveAs ( Widget, XtPointer, XtPointer)
{
  cerr << "Open!" << endl;
}

void XmwMenu::print ( Widget, XtPointer, XtPointer)
{
  cerr << "Print!" << endl;
}

void XmwMenu::quit ( Widget, XtPointer, XtPointer)
{
  cerr << "Quit!" << endl;
}

void XmwMenu::randomizer ( Widget, XtPointer, XtPointer)
{
  cerr << "Randomizer!" << endl;
}

void XmwMenu::about ( Widget, XtPointer, XtPointer)
{
  theBaseInterface->openUrl("about", "simlab.html");
}

void XmwMenu::contextHelp ( Widget, XtPointer, XtPointer )
{
  cerr << "Context help!" << endl;
}

void XmwMenu::index ( Widget, XtPointer, XtPointer )
{
  theBaseInterface->openUrl("index", "simlab.html");
}

void XmwMenu::credit ( Widget, XtPointer, XtPointer)
{
  theBaseInterface->openUrl("credit", "simlab.html");
}

// Static functions registered as callbacks

void XmwMenu::openCB ( Widget w, XtPointer tag, XtPointer data)
{
  XmwMenu *obj = (XmwMenu *)tag;
  obj->open(w, tag, data);
}

void XmwMenu::saveCB ( Widget w, XtPointer tag, XtPointer data)
{
  XmwMenu *obj = (XmwMenu *)tag;
  obj->save(w, tag, data);
}

void XmwMenu::saveAsCB ( Widget w, XtPointer tag, XtPointer data)
{
  XmwMenu *obj = (XmwMenu *)tag;
  obj->saveAs(w, tag, data);
}

void XmwMenu::printCB ( Widget w, XtPointer tag, XtPointer data)
{
  XmwMenu *obj = (XmwMenu *)tag;
  obj->print(w, tag, data);
}

void XmwMenu::quitCB ( Widget w, XtPointer tag, XtPointer data)
{
  XmwMenu *obj = (XmwMenu *)tag;
  obj->quit(w, tag, data);
}

void XmwMenu::randomizerCB ( Widget w, XtPointer tag, XtPointer data)
{
  XmwMenu *obj = (XmwMenu *)tag;
  obj->randomizer(w, tag, data);
}

void XmwMenu::aboutCB ( Widget w, XtPointer tag, XtPointer data)
{
  XmwMenu *obj = (XmwMenu *)tag;
  obj->about(w, tag, data);
}

void XmwMenu::contextHelpCB ( Widget w, XtPointer tag, XtPointer data)
{
  XmwMenu *obj = (XmwMenu *)tag;
  obj->contextHelp(w, tag, data);
}
void XmwMenu::indexCB ( Widget w, XtPointer tag, XtPointer data)
{
  XmwMenu *obj = (XmwMenu *)tag;
  obj->index(w, tag, data);
}
void XmwMenu::creditCB ( Widget w, XtPointer tag, XtPointer data)
{
  XmwMenu *obj = (XmwMenu *)tag;
  obj->credit(w, tag, data);
}
