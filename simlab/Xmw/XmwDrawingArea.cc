//-*-c++-*------------------------------------------------------------
// XmwDrawingArea.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class implementation for the class
// XmwDrawingArea
//--------------------------------------------------------------------

#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cassert>

#include <Xmt/Xmt.h>
#include <Xmt/WidgetType.h>

#include "XmwInterface.h"
#include "XmwDrawingArea.h"
#include "XmwFont.h"
#include "XmwColor.h"
#include "XmwFont.h"
using namespace std;


XmwDrawingArea::XmwDrawingArea(const char *name, Widget parent)
   : XmwWidget(name),
     line_style_(LineSolid),
     line_width_(0),
	 fontAttr_(NULL),
	 isInitialized_(False)
{
   widget_ = XmtNameToWidget(parent, name);
   assert(widget_);

   XtVaSetValues(widget_,
				 XmNkeyboardFocusPolicy, XmPOINTER,
				 NULL);

   // setup callbacks for the drawing area

   XtAddCallback(widget_,
				 XmNexposeCallback, (XtCallbackProc) exposeCallback, this);

   XtAddCallback(widget_,
				 XmNresizeCallback, (XtCallbackProc) resizeCallback, this);
}


XmwDrawingArea::~XmwDrawingArea()
{
   XFreeGC(display_, gc_);
}


// This should be called after the drawing area is mapped

void XmwDrawingArea::initialize()
{
   if (!XtIsRealized(widget_)) {
	  return;
   }

   window_ = XtWindow(widget_);
   display_ = XtDisplay(widget_);
   screen_ =  XtScreen(widget_);

   Window root = RootWindowOfScreen(screen_);

   if (!theColorTable) {
	  theColorTable = new XmwColor;
   }

   background_ = theColorTable->black();
   foreground_ = theColorTable->white();

   XtVaSetValues(widget_,
				 XmNbackground, background_,
				 XmNforeground, foreground_,
				 NULL);

   // Set graphics context and drawing stuff
      
   XGCValues gcv;
   gcv.foreground = foreground_;
   gcv.background = background_;
   gc_ = XCreateGC (display_, root,
					GCForeground|GCBackground, &gcv);

   XtAddEventHandler(widget_, EnterWindowMask, FALSE, 
					 (XtEventHandler) enterCallback, 
					 this);
      
   XtAddEventHandler(widget_, LeaveWindowMask, FALSE, 
					 (XtEventHandler) leaveCallback, 
					 this);

   // This allows get an expose event when the window is made smaller

   XSetWindowAttributes attrs;
   attrs.bit_gravity = ForgetGravity;
   XChangeWindowAttributes (display_, root, CWBitGravity, &attrs);

   // Find the size of the drawing area

   Dimension width, height;

   XtVaGetValues(widget_,
				 XmNwidth, &width,
				 XmNheight, &height,
				 NULL);
   width_ = width;
   height_ = height;
  
   isInitialized_ = True;
}

void
XmwDrawingArea::exposeCallback(Widget, XtPointer this_ptr, 
							   XmDrawingAreaCallbackStruct*)
{
   XmwDrawingArea *area = (XmwDrawingArea*) this_ptr;
   if (!area->isInitialized()) {
	  area->initialize();
   }
   area->expose();
}


void
XmwDrawingArea::resizeCallback(Widget w, XtPointer this_ptr, 
							   XmDrawingAreaCallbackStruct*)
{
   XmwDrawingArea *area = (XmwDrawingArea*) this_ptr;
   Dimension width, height;
   XtVaGetValues(w, XmNwidth, &width, XmNheight, &height, NULL);
   area->width_ = width;
   area->height_ = height;
   area->resize();
}


void
XmwDrawingArea::enterCallback(Widget, XtPointer this_ptr, 
							  XEvent *, Boolean*)
{
   (( XmwDrawingArea *)this_ptr)->enter();
}

void
XmwDrawingArea::leaveCallback(Widget, XtPointer this_ptr, 
							  XEvent *, Boolean*) {
   (( XmwDrawingArea *)this_ptr)->leave();
}

void XmwDrawingArea::clear()
{
   if (!XtIsRealized(widget_))  return;
   Pixel fg = foreground_;
   foreground(background_);
   XClearWindow(display_, window_);
   foreground(fg);
}

XFontStruct* XmwDrawingArea::setFont(int i)
{
   XFontStruct *font = fontAttr(i);
   setFont(font);
   return font;
}

void XmwDrawingArea::setFont(XFontStruct * font)
{
   if (fontAttr_ == font) return;
   fontAttr_ = font;
   XSetFont(display_, gc_, fontAttr_->fid);
   fontHeight_ = fontAttr_->ascent + fontAttr_->descent;
}

XFontStruct *XmwDrawingArea::fontAttr(int i)
{
   if (!theFontTable) {
	  theFontTable = new XmwFont();
	  setFont(theFontTable->fixed());
   }
   return theFontTable->font(i);
}

void XmwDrawingArea::foreground(Pixel color)
{
  if (foreground_ != color) {
	foreground_ = color; 
	XSetForeground(display_ , gc_, background_ ^ foreground_);
  }
}

void
XmwDrawingArea::foreground(float clr, float intensity)
{
   foreground(theColorTable->color(clr, intensity));
}


void
XmwDrawingArea::background(float clr, float intensity)
{
   background(theColorTable->color(clr, intensity));
}

void 
XmwDrawingArea::background(Pixel color)
{
   if (background_ == color) return;
   background_ = color;
   XSetBackground(display_, gc_, background_);	 
   XtVaSetValues(widget_,
				 XmNbackground, background_,
				 NULL);
}


void
XmwDrawingArea::drawCString (const char * string, const XmwPoint &point)
{
   int length = strlen(string);
   int w = XTextWidth(fontAttr_,  string,  length);
   int x = point.x() - w / 2;
   int y = point.y() + fontHeight_ / 2 - 2;
   XDrawString(display_, window_, gc_, x, y, string, length);
}


void
XmwDrawingArea::drawLString(const char * string, XPoint &point) 
{
   int length = strlen(string);
   int x = point.x;
   int y = point.y + fontHeight_ / 2 - 2;
   XDrawString(display_, window_, gc_, x, y, string, length);

}

void
XmwDrawingArea::drawRString(const char * string, XPoint &point) 
{
   int length = strlen(string);
   int w = XTextWidth(fontAttr_,  string,  length);
   int x = point.x - w;
   int y = point.y + fontHeight_ / 2 - 2;
   XDrawString(display_, window_, gc_, x, y, string, length);
}


void
XmwDrawingArea::drawCString (const char * string, XPoint &point)
{
   int length = strlen(string);
   int w = XTextWidth(fontAttr_,  string,  length);
   int x = point.x - w / 2;
   int y = point.y + fontHeight_ / 2 - 2;
   XDrawString(display_, window_, gc_, x, y, string, length);
}


void
XmwDrawingArea::drawLString(const char * string, const XmwPoint &point) 
{
   int length = strlen(string);
   int x = point.x();
   int y = point.y() + fontHeight_ / 2 - 2;
   XDrawString(display_, window_, gc_, x, y, string, length);

}

void
XmwDrawingArea::drawRString(const char * string, const XmwPoint &point) 
{
   int length = strlen(string);
   int w = XTextWidth(fontAttr_,  string,  length);
   int x = point.x() - w;
   int y = point.y() + fontHeight_ / 2 - 2;
   XDrawString(display_, window_, gc_, x, y, string, length);
}

void XmwDrawingArea::draw(const XmwPoint &p, const Pixmap& pixmap)
{
   if (!XtIsRealized(widget()))  {
	  return;
   }

   Window win;
   int x, y;
   unsigned int border;
   unsigned int w;
   unsigned int h;
   unsigned int depth;

   XGetGeometry(display_,
				pixmap,			// the drawable
				&win,			// window id returned
				&x, &y,			// window position (always zero)
				&w, &h,			// size of the drawable
				&border,		// border width (always zero)
				&depth			// bits per pixel
				);
   
   x = p.x() - w / 2;
   y = p.y() - h / 2;

   XCopyArea(display_, pixmap, window_, gc_,
			 0, 0, w, h, x, y);
}

void XmwDrawingArea::makePrintable()
{
   // Get image in the drawing area

   XImage *image = XGetImage(display_, window_,
							 0, 0, width_, height_,
							 ~0, ZPixmap);
   if (!image) {
	  cerr << "Failed get drawing area image" << endl;
	  return;
   }

   // Make the image black and white

   unsigned long black = theColorTable->black();
   unsigned long white = theColorTable->white();
   unsigned long pixel;

   for (int y = 0; y < image->height; y ++) {
	  for (int x = 0; x < image->width; x ++) {
		 pixel = XGetPixel(image, x, y);
		 if (pixel == black) {
			XPutPixel(image, x, y, white);
		 } else if (pixel == white) {
			XPutPixel(image, x, y, black);
		 } else {
			XPutPixel(image, x, y, black);
		 }
	  }
   }

   // Copy the image back to the drawing arae

   clear();
   XPutImage(display_, window_, gc_, image,
			 0, 0,
			 0, 0, width_, height_);

   // Free the image

   XDestroyImage(image);
}
