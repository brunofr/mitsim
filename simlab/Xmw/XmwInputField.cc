//-*-c++-*------------------------------------------------------------
// XmwInputField.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class definition/implementation for the class
//--------------------------------------------------------------------

#include <assert.h>
#include <stdio.h>

#include <Xmt/Xmt.h>
#include <Xmt/InputField.h>

#include <Tools/SimulationClock.h>

#include "XmwInputField.h"
#include "XmwInterface.h"

void XmwInputField::lookup(Widget parent, const char *name)
{
   // Layout of this XmwInputField

   Widget w = XtNameToWidget(parent, name);
   if (!w) {
	  theBaseInterface->widgetMissingBoilout(name);
   }

   input_ = XtNameToWidget(w, "*fld");
   assert(input_);
}

void XmwInputField::set(const char * value)
{
   XmtInputFieldSetString(input_, value);
}

void XmwInputField::set(double v)
{
   char buffer[12];
   sprintf(buffer, "%.1lf", v);
   set(buffer);
}

void XmwInputField::set(int v)
{
   char buffer[12];
   sprintf(buffer, "%d", v);
   set(buffer);
}

String XmwInputField::get()
{
   return XmtInputFieldGetString(input_);
}

double XmwInputField::getTime()
{
  String s = XmtInputFieldGetString(input_);
  return theSimulationClock->convertTime(s);
}
