//-*-c++-*------------------------------------------------------------
// XmwSciPlot.h
//
// Provides a list interface, which contains selectable items includes
// from 'MotifUI' library
//
// Qi Yang
//
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef XMWSCIPLOT_HEADER
#define XMWSCIPLOT_HEADER

#include <vector>

#include <Rob/SciPlot.h>
#include <Rob/ListTree.h>

#include "XmwUIWidget.h"
#include "XmwCallback.h"

class XmwSciPlot : public XmwUIWidget
{
	  CallbackDeclare(XmwSciPlot);
	  friend class XmwSciPlotDialog;

   public:

	  XmwSciPlot( Widget, const String, ArgList args = NULL,
				  Cardinal argc = 0);

	  virtual ~XmwSciPlot();

	  void title(String s) { set(&title_, s); }
	  void xlabel(String s) { set(&xlabel_, s); }
	  void ylabel(String s) { set(&ylabel_, s); }

	  virtual void post(XmwSciPlot** ptr_to_self_ptr = NULL);
	  void update();
	  Boolean quickUpdate();

	  int createSeries(int num, float *x, float *y, char *legend);
	  int createSeries(int num, double *x, double *y, char *legend);
	  void addToSeries(int series, int num, float *x, float *y);
	  void addToSeries(int series, int num, double *x, double *y);
	  void updateSeries(int series, int num, float *x, float *y);
	  void updateSeries(int series, int num, double *x, double *y);
	  void deleteSeries(int series);
	  void deleteAllSeries();

   protected:

	  virtual void save ( Widget, XtPointer, XtPointer );
	  virtual void eXport ( Widget, XtPointer, XtPointer );
	  virtual void print ( Widget, XtPointer, XtPointer );
	  virtual void close ( Widget, XtPointer, XtPointer );

	  virtual void property ( Widget, XtPointer, XtPointer );

	  virtual void about ( Widget, XtPointer, XtPointer );
	  virtual void index ( Widget, XtPointer, XtPointer );

	  const char* className() const;

   protected:

	  String title_;
	  String xlabel_;
	  String ylabel_;

	  Widget plot_;				// SciPlot widget
	  std::vector <int> series_;		// List of series

   private:

	  int series(int i) { return series_[i]; }
	  int nSeries() { return series_.size(); }

	  Widget getMenuItemWidget(Widget parent, const char *name);
	  void pickDefaultStyle(int series);
	  char* pickDefaultLegend();
	  void set(String *ptr, String value);
	  void setScales(int n, double *x, double *y);
	  void setScales(int n, float *x, float *y);

	  unsigned int autoScale(unsigned int flag) {
		 return autoScale_ & flag;
	  }
	  void setAutoScale(unsigned int flag) {
		 autoScale_ |= flag;
	  }
	  void unsetAutoScale(unsigned int flag) {
		 autoScale_ &= ~flag;
	  }
	  double scale(int i) { return scale_[i]; }
	  void scale(int i, double s) { scale_[i] = s; }

	  String label(int y) {
		 if (y) return ylabel_;
		 else return xlabel_;
	  }

	  int autoLegended_;
	  XmwSciPlot** ptr_to_self_ptr_;

	  int autoScale_;			// whether x and y are autoscaled
	  double scale_[4];			// user defined scale for x and y

	  int nSciColors_;
	  int *sciColors_;
};

#endif
