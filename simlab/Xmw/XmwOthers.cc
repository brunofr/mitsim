//-*-c++-*------------------------------------------------------------
// XmwOthers.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <stdio.h>
#include <assert.h>

#include <Xmt/Xmt.h>
#include <Xmt/WidgetType.h>
#include <Xmt/Create.h>
#include <Xmt/Converters.h>

#include <Xbae/Matrix.h>
#include <Combo/ComboBox.h>
#include <Rob/SciPlot.h>
#include <Rob/ListTree.h>

//#include "XmwHisto1D.h"
//#include "XmwHisto2D.h"

#include "XmwOthers.h"

Widget CreateSciPlot(Widget parent, String name,
					 ArgList args, Cardinal numArgs)
{
   Widget w;
   w = XtVaCreateManagedWidget("plot",
	  sciplotWidgetClass, parent,
	  XtNshowLegend, False,
	  XtNchartType, XtCARTESIAN, 
	  XtNdrawMinor, False,
	  XtNheight, 300, 
	  XtNwidth,  400,
	  XtNlabelFont, XtFONT_TIMES|10,
	  XtNtitleFont, XtFONT_TIMES|XtFONT_BOLD|12,
	  XtNaxisFont, XtFONT_TIMES|10,
	  XtNdefaultMarkerSize, 2,
	  XtNtitleMargin, 4,
	  NULL);
   assert (w);
   XtSetValues( w, args, numArgs );
   return w;
}

Widget CreateListTree(Widget parent, String name,
                      ArgList args, Cardinal numArgs)
{
   Widget w;

   w = XmCreateScrolledListTree(parent,"tree", args, numArgs);
   assert (w);

   return w;
}
