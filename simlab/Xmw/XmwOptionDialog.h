//-*-c++-*------------------------------------------------------------
// XmwOptionDialog.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef XMWOPTIONDIALOG_HEADER
#define XMWOPTIONDIALOG_HEADER

#include "XmwDialogManager.h"

class XmwOptionDialog : public XmwDialogManager
{
	  friend class XmwInterface;

   public:

	  XmwOptionDialog(Widget parent);
	
	  ~XmwOptionDialog() { }

	  void post();				// virtual

   protected:

	  // overload the virtual functions

	  void okay(Widget w, XtPointer tag, XtPointer data);
	  void cancel(Widget w, XtPointer tag, XtPointer data);
	  void help(Widget w, XtPointer tag, XtPointer data);

   private:

	  static XtResource resources[];
	  void buildBreakPoints();
	  Widget bps_;
};

#endif
