//-*-c++-*------------------------------------------------------------
// XmwUIWidget.h
//
// Base class for all C++/Motif User Interface widgets.
//
// Modified by Qi Yang based on an example provided by
// Didier Burton
//
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef XmwUIWIDGET_HEADER
#define XmwUIWIDGET_HEADER

#include "XmwWidget.h"

class XmwUIWidget : public XmwWidget
{
   public:

	  virtual ~XmwUIWidget();	// Destructor
	  virtual void manage();	// Manage the entire widget subtree

	  // set and unset the sensitivity of the widget.

	  virtual void activate();
	  virtual void deactivate();
	  virtual Boolean isSensitive();

	  virtual void setWidget( Widget );

	  virtual const char * className() const;
    
   private:
    
	  static void widgetDestroyedCallback(
		 Widget, XtPointer, XtPointer
		 );
    
	  // Interface between XmNdestroyCallback and this class

   protected:
    
	  // Protect constructor against direct instantiation

	  XmwUIWidget( const char *name = (char*) NULL );

	  // Easy hook for derived classes

	  void installDestroyHandler();

      // Called by widgetDestroyedCallback() if base widget is destroyed

	  virtual void widgetDestroyed() {
		 widget_ = NULL;
	  }

	  void setDefaultResources( const Widget,
								const String *);

	  // Loads component's default resources into database

	  void getResources( const XtResourceList,
						 const int );

	  // Retrieve resources for this class
	  // from the resource manager    
};

#endif
