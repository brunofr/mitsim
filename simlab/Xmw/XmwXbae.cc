/*
 * XmwXbae.cc -- Add matrix widget to Xmt
 *
 * Qi Yang modified from:
 * http://sauron.et.byu.edu/Xmt/XmtData/XmtXbae.html
 * By Karl G. Merkley
 * Mechanical Engineering Dept.
 * 242 Clyde Building
 * Brigham Young University
 * Provo, UT 84602 USA
 */

#include <Xmt/Xmt.h>
#include <Xmt/WidgetType.h>
#include <Xmt/Procedures.h>
#include <Xbae/Matrix.h>
#include <Xbae/Caption.h>
#include "XmwXbae.h"

Widget XbaeCreateMatrix(Widget parent, String name, ArgList args, Cardinal n)
{
  Widget w = XtCreateWidget(name, xbaeMatrixWidgetClass, parent, args, n);
  XtVaSetValues(w, XmNallowColumnResize, True, NULL);
  return w;
}

Widget XbaeCreateCaption(Widget parent, String name, ArgList args, Cardinal n) 
{
  return XtCreateWidget(name, xbaeCaptionWidgetClass, parent, args, n);
} 

XrmQuark XmtQStringTable;

static void XbaeMatrixSetValues(Widget w, XtPointer address, 
				XrmQuark type, Cardinal /* size */)
{
  if (type != XmtQStringTable) {
    XtVaSetValues(w, XmNcells, NULL, NULL);
  } else {
    XtVaSetValues(w, XmNcells, address, NULL);
  }
}

static void XbaeMatrixGetValues(Widget w, XtPointer address,
                          XrmQuark type, Cardinal /* size */)
{
  String **cells = (String **)address;
  if (type != XmtQStringTable) {
    cells = NULL;
  } else {
    XtVaGetValues(w, XmNcells, cells, NULL);
  }
}

static XmtWidgetType xbae_types[] = {
  { "XbaeMatrix", NULL, XbaeCreateMatrix,
    XbaeMatrixSetValues, XbaeMatrixGetValues, False },
  { "XbaeCaption", NULL, XbaeCreateCaption }
};

static XmtProcedureInfo xbae_procs[] = {
  { "XbaeMatrixSetCell", (XmtProcedure) XbaeMatrixSetCell,
    {XmRWidget, XmRInt, XmRInt, XmRString}},
  { "XbaeMatrixEditCell", (XmtProcedure) XbaeMatrixEditCell,
    {XmRWidget, XmRInt, XmRInt}},
  { "XbaeMatrixSelectCell", (XmtProcedure) XbaeMatrixSelectCell,
    {XmRWidget, XmRInt, XmRInt}},
  { "XbaeMatrixSelectRow", (XmtProcedure) XbaeMatrixSelectRow,
    {XmRWidget, XmRInt}},
  { "XbaeMatrixSelectColumn", (XmtProcedure) XbaeMatrixSelectColumn,
    {XmRWidget, XmRInt}},
  { "XbaeMatrixDeselectAll", (XmtProcedure) XbaeMatrixDeselectAll,
    {XmRWidget}},
  { "XbaeMatrixDeselectCell", (XmtProcedure) XbaeMatrixDeselectCell,
    {XmRWidget, XmRInt, XmRInt}},
  { "XbaeMatrixDeselectRow", (XmtProcedure) XbaeMatrixDeselectRow,
    {XmRWidget, XmRInt}},
  { "XbaeMatrixDeselectColumn", (XmtProcedure) XbaeMatrixDeselectColumn,
    {XmRWidget, XmRInt}},
  { "XbaeMatrixCancelEdit", (XmtProcedure) XbaeMatrixCancelEdit,
    {XmRWidget, XmRBoolean}},
  { "XbaeMatrixCommitEdit", (XmtProcedure) XbaeMatrixCommitEdit,
    {XmRWidget, XmRBoolean}},
  { "XbaeMatrixDeleteRows", (XmtProcedure) XbaeMatrixDeleteRows,
    {XmRWidget, XmRInt, XmRInt}},
  { "XbaeMatrixDeleteColumns", (XmtProcedure) XbaeMatrixDeleteColumns,
    {XmRWidget, XmRInt, XmRInt}},
  { "XbaeMatrixSetCellColor", (XmtProcedure) XbaeMatrixSetCellColor,
    {XmRWidget, XmRInt, XmRInt, XmRPixel}},
};


/*
 * AddRow and AddColumn not implemented due to lack of resource converters
 */

void XmtRegisterXbaeWidgets(void)
{
  XmtQStringTable = XrmStringToQuark(XmRStringTable);
  XmtRegisterWidgetTypes(xbae_types, XtNumber(xbae_types));
  XmtRegisterProcedures(xbae_procs, XtNumber(xbae_procs));
}
