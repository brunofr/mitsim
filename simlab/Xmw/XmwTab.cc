//$Id: XmwTab.cc,v 1.2 1998/02/06 21:39:02 qiyang Exp $
//----------------------------------------------------------------------+
//
// FILE NAME   : XmwTab.cc
// AUTHOR      : Didier Burton
// CREATION    : Fri Oct 17 16:35:36 1997
//
//----------------------------------------------------------------------+

#include "XmwTab.h"
                                    // includes from 'Base' library
#include <Xfw/XfwTabBook.h>
                                    // includes from 'Xmt' libraries
#include <Xmt/Xmt.h>
                                    // includes from C library
#include <assert.h>

//----------------------------------------------------------------------+
// Constructor
//----------------------------------------------------------------------+

XmwTab::XmwTab( Widget parent_,
		const String  name_,
		ArgList       args_,
		Cardinal      numArgs_ )
  : XmwWidget( name_ ) {
                                    // create the toggle button

  widget_ = XtCreateWidget( name_,
		       xfwTabBookWidgetClass, 
		       parent_,
		       args_, numArgs_ );
 
  // installDestroyHandler();
}

//----------------------------------------------------------------------+

XmwTab::~XmwTab() {

}

//----------------------------------------------------------------------+

Boolean
XmwTab::setActivePage( int page_,
		       int option_ ) {

  if (!XtIsObject(widget_)) return False;

  return XfwTabBookSetActivePage( widget_, page_, option_ );
}

//----------------------------------------------------------------------+

int
XmwTab::getActivePage() {

  if (!XtIsObject(widget_)) return -1;

  return XfwTabBookGetActivePage( widget_ );
}

//----------------------------------------------------------------------+

Widget
XmwTab::getActivePageWidget() {

  if (!XtIsObject(widget_)) return 0;

  return XfwTabBookGetActivePageWidget( widget_ );
}

//----------------------------------------------------------------------+
// static

Widget
XmwTab::createXmwTab( Widget       parent_,
                      const String name_,
		      ArgList      args_,
		      Cardinal     numArgs_ ) {

                                    // create the XmwTab with no
                                    // associated cmd.

  XmwTab *tab = new XmwTab( parent_, name_,
			    args_, numArgs_ );

                                    // make the XmwTab pointer the
                                    // userData of the pushbutton widget.

  XtVaSetValues( tab->widget(),
                 XmNuserData, (XtPointer) tab,
                 NULL );

  return tab->widget();
}

//----------------------------------------------------------------------+
// static

XmwTab *
XmwTab::getXmwTab( Widget tab_ ) {

                                    // assert that tab is a widget.
  assert(XtIsObject(tab_));
                                    // get the XtPointer from the userData
                                    // of the tab widget.
  XtPointer ptr;
  XtVaGetValues( tab_,
                 XmNuserData, &ptr,
                 NULL );
                                    // check the pointer definition.
  assert( ptr != NULL );
                                    // casts the pointer to the right type.

  XmwTab *tabObj = (XmwTab*) ptr;

  return tabObj;
}

//----------------------------------------------------------------------+
