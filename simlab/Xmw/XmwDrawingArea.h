//-*-c++-*------------------------------------------------------------
// XmwDrawingArea.h
//
// Qi Yang and Peter Welch
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class definition for the class
// XmwDrawingArea.
//
//--------------------------------------------------------------------

#ifndef XmwDRAWINGAREA_HEADER
#define XmwDRAWINGAREA_HEADER

#include <iostream>

#include "XmwUIWidget.h"
#include "XmwPoint.h"

class XmwColor;
class XmwFont;

class XmwDrawingArea : public XmwWidget
{
public:

  XmwDrawingArea(const char* name, Widget parent);
  virtual ~XmwDrawingArea();
  virtual void initialize();
  Boolean isInitialized() { return isInitialized_; }

  Display * display() { return display_; }
  Window window() { return window_; }
  Screen * screen() { return screen_; }
  const GC& gc() { return gc_; }

  // Current font

  void setFont(XFontStruct * font);
  XFontStruct * setFont(int i);
  XFontStruct * fontAttr() { return fontAttr_; }
  XFontStruct * fontAttr(int i);

  inline Pixel foreground() { return foreground_; }
  void foreground(Pixel color);
  void foreground(float color, float intensity = 1.0);

  inline Pixel background() { return background_; }
  void background(Pixel color);
  void background(float color, float intensity = 0.0);

  virtual void clear();
  virtual void expose() { }
  virtual void resize() { }
  virtual void leave() { }
  virtual void enter() { }


  // X Drawing wrappers.  These should all be inline.  

  inline void drawBoundingBox();

  inline void drawLine(const XmwPoint &from,
					   const XmwPoint &to);
	  
  inline void drawLine(XPoint &from,
					   XPoint &to);
	  
  inline void drawPoint(const XmwPoint &point);
  inline void drawPoint(XPoint &point);
	  
  inline void drawFilledRect(XPoint &point, int width, int height); 
  inline void drawFilledRect(const XmwPoint &point, 
							 int width, int height); 
	  
  inline void drawRect(const XmwPoint &from, const XmwPoint &to);
  inline void drawRect(XPoint &from, XPoint &to);
  inline void drawRect(XPoint &point, int width, int height);
  inline void drawRect(const XmwPoint &point, int width, int height);

  void drawCircle(const XmwPoint& p, int radius);
  void drawCircle(XPoint& p, int radius);
	  
  void drawPie(const XmwPoint& p, int radius);
  void drawPie(XPoint& p, int radius);
	  
  void drawString (const char * string, XPoint &point) {
	drawCString(string, point);
  }
  void drawString (const char * string,
				   const XmwPoint &point) {
	drawCString(string, point);
  }
      
  void drawLines(XPoint * points, int num_points);

  // draw strings C - center, L - left, R - right Strings will
  // always be drawn centered vertically about XmwPoint.y
      
  void  drawCString(const char * string, XPoint &point);
  void  drawCString(const char * string,
					const XmwPoint &point);
	  
  void  drawLString(const char * string, XPoint &point);
  void  drawLString(const char * string,
					const XmwPoint &point);
	  
  void  drawRString(const char * string, XPoint &point);
  void  drawRString(const char * string,
					const XmwPoint &point);
	  
  void drawFilledPolygon(XPoint *pts, int npts,
						 int shape = Nonconvex);

  void draw(const XmwPoint &p, const Pixmap& pixmap);

  void setCopyMode() {
	XSetFunction(display_, gc_, GXcopy);
  }

  void setXorMode() {
	XSetFunction(display_, gc_, GXxor);
  }

  inline void lineWidth(int width = 0) {
	line_width_ = width;
	XSetLineAttributes(display_, gc_, line_width_,
					   line_style_, CapButt, JoinMiter);
  }

  inline void lineStyle(int style = LineSolid) {
	line_style_ = style;
	XSetLineAttributes(display_, gc_, line_width_,
					   line_style_, CapButt, JoinMiter);
  }

  inline unsigned int fontHeight() { return fontHeight_; }
	      
  int width() { return width_; }
  int height() { return height_; }

  void makePrintable();

protected:

  Boolean isInitialized_;

  Window window_;
  Display * display_;
  Screen * screen_;
  GC gc_;
  Pixel background_, foreground_;

  int width_;  
  int height_;
  int line_style_;
  int line_width_;

  // String Drawing Data Members 

  XFontStruct * fontAttr_;
  unsigned int fontHeight_;

private:

  static void exposeCallback (
							  Widget, XtPointer,
							  XmDrawingAreaCallbackStruct *);

  static void resizeCallback (
							  Widget, XtPointer,
							  XmDrawingAreaCallbackStruct *);

  static void enterCallback (
							 Widget, XtPointer, 
							 XEvent *, Boolean*);

  static void leaveCallback (
							 Widget, XtPointer, 
							 XEvent *, Boolean*);
};


// XmwDrawingArea inline functions:

inline void XmwDrawingArea::drawBoundingBox() {
  XDrawRectangle(display_, window_, gc_,
				 0, 0, width_-1, height_-1);  
}

inline void XmwDrawingArea::drawLine(XPoint &from,
									 XPoint &to) 
{ 
  XDrawLine(display_, window_, gc_, from.x, from.y,
			to.x, to.y); 
}

inline void XmwDrawingArea::drawLine(const XmwPoint &from,
									 const XmwPoint &to) 
{ 
  XDrawLine(display_, window_, gc_,
			from.x(), from.y(),
			to.x(), to.y()); 
}

inline void XmwDrawingArea::drawLines(XPoint * points, int num_points) {
  XDrawLines(display_, window_, gc_,
			 points, num_points, CoordModeOrigin);
}

inline void XmwDrawingArea::drawPoint(XPoint &point) {
  XDrawPoint(display_, window_, gc_,
			 point.x, point.y); 
}

inline void XmwDrawingArea::drawPoint(const XmwPoint &point) {
  XDrawPoint(display_, window_, gc_,
			 point.x(), point.y()); 
}

inline void
XmwDrawingArea::drawFilledRect(XPoint &point, 
							   int width, int height) {
  XFillRectangle(display_, window_, gc_,
				 point.x, point.y, width, height);   
}

inline void
XmwDrawingArea::drawFilledRect(const XmwPoint &point, 
							   int width, int height) {
  XFillRectangle(display_, window_, gc_,
				 point.x(), point.y(), width, height);   
}

inline void 
XmwDrawingArea::drawRect(XPoint &point, 
						 int width, int height) {
  XDrawRectangle(display_, window_, gc_,
				 point.x, point.y, 
				 width, height);
}

inline void 
XmwDrawingArea::drawRect(const XmwPoint &point, 
						 int width, int height) {
  XDrawRectangle(display_, window_, gc_,
				 point.x(), point.y(), 
				 width, height);
}

inline void
XmwDrawingArea::drawRect(XPoint &from, XPoint &to) 
{ 
  int w, h, x, y;
  if (from.x < to.x) {
	x = from.x;
	w = to.x - from.x;
  } else {
	x = to.x;
	w = from.x - to.x;
  }
  if (from.y < to.y) {
	y = from.y;
	h = to.y - from.y;
  } else {
	y = to.y;
	h = from.y - to.y;
  }
  XDrawRectangle(display_, window_, gc_, x, y, w, h);
}

inline void
XmwDrawingArea::drawRect(const XmwPoint &from, const XmwPoint &to) 
{ 
  int w, h, x, y;
  if (from.x() < to.x()) {
	x = from.x();
	w = to.x() - from.x();
  } else {
	x = to.x();
	w = from.x() - to.x();
  }
  if (from.y() < to.y()) {
	y = from.y();
	h = to.y() - from.y();
  } else {
	y = to.y();
	h = from.y() - to.y();
  }
  XDrawRectangle(display_, window_, gc_, x, y, w, h);
}

inline void
XmwDrawingArea::drawPie(const XmwPoint& p, int radius)
{
  int size = 2 * radius;

  XFillArc(display_, window_, gc_,
		   p.x() - radius, p.y() - radius,
		   size, size, 0, 23040);
}

inline void
XmwDrawingArea::drawPie(XPoint& p, int radius)
{
  int size = 2 * radius;

  XFillArc(display_, window_, gc_,
		   p.x - radius, p.y - radius,
		   size, size, 0, 23040);
}

inline void
XmwDrawingArea::drawCircle(const XmwPoint& p, int radius)
{
  int size = 2 * radius;

  XDrawArc(display_, window_, gc_,
		   p.x() - radius, p.y() - radius,
		   size, size, 0, 23040);
}

inline void
XmwDrawingArea::drawCircle(XPoint& p, int radius)
{
  int size = 2 * radius;

  XDrawArc(display_, window_, gc_,
		   p.x - radius, p.y - radius,
		   size, size, 0, 23040);
}


inline void
XmwDrawingArea::drawFilledPolygon(XPoint *pts, int npts, int shape)
{
  XFillPolygon(display_, window_, gc_,
			   pts, npts, shape, CoordModeOrigin);
}

#endif
