//-*-c++-*------------------------------------------------------------
// XmwRandomizer.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <stdio.h>
#include <time.h>

#include <Xmt/Xmt.h>
#include <Xmt/InputField.h>
#include <Xm/ToggleB.h>

#include <Tools/Random.h>
#include <Tools/SimulationClock.h>

#include "XmwRandomizer.h"
#include "XmwCheckerField.h"
#include "XmwInterface.h"
#include "XmwMenu.h"
using namespace std;

//----------------------------------------------------------------------+

#ifdef SUNOS

static long _seed = 0;

#define MBIG 1000000000
#define MSEED 161803398
#define MZ 0

long random(void) {
  static int inext,inextp;
  static long ma[56];
  static int iff=0;
  long mj,mk;
  int i,ii,k;

  if (_seed < 0 || iff == 0) {
    iff=1;
    mj=MSEED-(_seed < 0 ? -_seed : _seed);
    mj %= MBIG;
    ma[55]=mj;
    mk=1;
    for (i=1;i<=54;i++) {
      ii=(21*i) % 55;
      ma[ii]=mk;
      mk=mj-mk;
      if (mk < MZ) mk += MBIG;
      mj=ma[ii];
    }
    for (k=1;k<=4;k++)
      for (i=1;i<=55;i++) {
        ma[i] -= ma[1+(i+30) % 55];
        if (ma[i] < MZ) ma[i] += MBIG;
      }
    inext=0;
    inextp=31;
    _seed=1;
  }
  if (++inext == 56) inext=1;
  if (++inextp == 56) inextp=1;
  mj=ma[inext]-ma[inextp];
  if (mj < MZ) mj += MBIG;
  ma[inext]=mj;
  return mj;
}

int srandom(int seed) {
  _seed = seed;
  return 1;
}

#undef MBIG
#undef MSEED
#undef MZ

#endif

//----------------------------------------------------------------------+

XmwRandomizer::XmwRandomizer ( Widget parent, int nflds )
  : XmwDialogManager(parent, "RandomizerDialog", NULL, 0)
{
  XtVaSetValues(widget_,
				XmNdialogStyle, XmDIALOG_PRIMARY_APPLICATION_MODAL,
				NULL);

  char fld_name[3];
  strcpy(fld_name, "r ");
  XmwCheckerField *fld;
  flds_.reserve(nflds);
  for (int i = 0; i < nflds; i ++) {
	fld_name[1] = '1'+ i;
	fld = new XmwCheckerField(widget_, fld_name, (1 << i) );
	flds_.push_back(fld);
	XtAddCallback(fld->checker(), XmNvalueChangedCallback,
				  &XmwRandomizer::setSeedCB, fld);
  }
}

XmwRandomizer::~XmwRandomizer()
{
  for (int i = 0; i < flds_.size(); i ++) {
	delete flds_[i];
  }
}

// These overload the functions in base class

void XmwRandomizer::cancel(Widget, XtPointer, XtPointer)
{
  unmanage();
}


void XmwRandomizer::okay(Widget, XtPointer, XtPointer)
{
  unsigned int flags = 0;
  const char * s;
  unsigned int v;
  for (int i = 0; i < flds_.size(); i ++) {
	if (s = flds_[i]->get()) {
	  v = strtol(s, NULL, 16);
	  flags |= ((v & 0xFF) << i * 8);
	}
  }
  Random::flags(flags);
  unmanage();
}


void XmwRandomizer::help(Widget, XtPointer, XtPointer)
{
  theBaseInterface->openUrl("randomizer", "misc.html");
}


void XmwRandomizer::post()
{
  // Copy data to dialog

  srandom(time(0));

  char seed[3];
  for (int i = 0; i < flds_.size(); i ++) {
	unsigned int s = (0xFF << (i * 8)) & Random::flags();
	s = (s >> (i * 8)) & 0xFF;
	if (s == 0) {				// automatic
	  flds_[i]->check(True);
	  s = random() & 0xFF;	  
	} else {					// manual
	  flds_[i]->check(False);
	}
	sprintf(seed, "%2.2X", s);
	flds_[i]->set(seed);
  }

  // Do not allow changes after simulation is started

  if (theSimulationClock->isStarted()) {
	deactivate();
  } else {
	activate();
  }
   
  XmwDialogManager::post();
}

void XmwRandomizer::activate()
{
  if (XtIsSensitive(okay_)) return;
  for (int i = 0; i < flds_.size(); i ++) {
	XmwDialogManager::activate(flds_[i]->checker());
	XmwDialogManager::activate(flds_[i]->input());
  }
  XmwDialogManager::activate(okay_);
}

void XmwRandomizer::deactivate()
{
  if (!XtIsSensitive(okay_)) return;
  for (int i = 0; i < flds_.size(); i ++) {
	XmwDialogManager::deactivate(flds_[i]->checker());
	XmwDialogManager::deactivate(flds_[i]->input());
  }
  XmwDialogManager::deactivate(okay_);
}

void XmwRandomizer::setSeedCB(Widget w, XtPointer client_data, XtPointer)
{
  XmwCheckerField *fld = (XmwCheckerField *) client_data;
  if (!fld->isChecked()) return;
  char seed[3];
  int s = random() & 0xFF;
  sprintf(seed, "%2.2X", s);
  fld->set(seed);
}
