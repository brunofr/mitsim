//-*-c++-*------------------------------------------------------------
// XmwOthers.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class definition/implementation for the class
//--------------------------------------------------------------------

#ifndef XMWOTHERS_HEADER
#define XMWOTHERS_HEADER

extern Widget CreateXgTabs(Widget parent, String name,
						   ArgList args, Cardinal numArgs);
extern Widget CreateSciPlot(Widget parent, String name,
							ArgList args, Cardinal numArgs);
extern Widget CreateListTree(Widget parent, String name,
							ArgList args, Cardinal numArgs);
#endif
