//-*-c++-*------------------------------------------------------------
// XmwUIWidget.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include "XmwUIWidget.h"

#include <stdio.h>
#include <assert.h>				// compile with -DNDEBUG to cancel

XmwUIWidget::XmwUIWidget ( const char *name )
   : XmwWidget( name )
{
}

const char * XmwUIWidget::className() const
{
   return "XmwUIWidget";
}

void
XmwUIWidget::widgetDestroyedCallback(
   Widget w, 
   XtPointer clientData, 
   XtPointer )
{
   if ( !XtIsObject( w ) ) return;

   XmwUIWidget *obj = (XmwUIWidget *) clientData;	
   obj->widgetDestroyed();
}


void
XmwUIWidget::installDestroyHandler() {

   assert ( widget_ != NULL );
   XtAddCallback(
	  widget_, 
	  XmNdestroyCallback,
	  &XmwUIWidget::widgetDestroyedCallback, 
	  (XtPointer) this
	  );
}


void
XmwUIWidget::manage()
{
   assert (
	  widget_ != NULL
	  );

   assert (
	  XtHasCallbacks( widget_, XmNdestroyCallback ) == XtCallbackHasSome 
	  );

   XtManageChild ( widget_ );
}


XmwUIWidget::~XmwUIWidget()
{
   // Make sure the widget hasn't already been destroyed

   if ( widget_ ) {

	  // Remove destroy callback so Xt cannot call the callback with a
	  // a pointer to an object that has already been freed

	  XtRemoveCallback(
		 widget_, 
		 XmNdestroyCallback,
		 &XmwUIWidget::widgetDestroyedCallback,
		 (XtPointer) this );	
   }
}


void
XmwUIWidget::getResources(
   const XtResourceList resources, 
   const int numResources )
{
   // Check for errors

   assert( widget_ != NULL );
   assert( resources != NULL );
    
   // Retrieve the requested resources relative to the parent of this
   // object's base widget

   XtGetSubresources(
	  XtParent( widget_ ),
	  (XtPointer) this,
	  name_,
	  className(),
	  resources,
	  numResources,
	  NULL,
	  0 );
}


void
XmwUIWidget::setDefaultResources(
   const Widget  w, 
   const String *resourceSpec )
{
   int         i;	
   Display    *dpy = XtDisplay( w ); // Retrieve the display pointer
   XrmDatabase rdb = NULL;		     // A resource data base

   // Create an empty resource database

   rdb = XrmGetStringDatabase( "" );

   // Add the Widget resources, prepending the name of the widget

   i = 0;
   while ( resourceSpec[i] != NULL ) {
	  char buf[1000];
	  sprintf( buf, "*%s%s", name_, resourceSpec[i++] );
	  XrmPutLineResource( &rdb, buf );
   }

   // Merge them into the Xt database, with lowest precendence

   if ( rdb ) {
#if (XlibSpecificationRelease >= 5)
	  XrmDatabase db = XtDatabase( dpy );
	  XrmCombineDatabase( rdb, &db, FALSE );
#else
	  XrmMergeDatabases( dpy->db, &rdb );
	  dpy->db = rdb;
#endif
   }
}


void
XmwUIWidget::activate()
{
   if ( widget_ ) {
	  XtSetSensitive( widget_, True );
   }
}


void
XmwUIWidget::deactivate()
{
   if ( widget_ ) {
	  XtSetSensitive( widget_, False );
   }
}


Boolean
XmwUIWidget::isSensitive()
{
   if ( widget_ ) return XtIsSensitive( widget_ );
   else return False;
}


void
XmwUIWidget::setWidget( Widget w )
{
   assert( XtIsObject( w ) );

   widget_ = w;
   installDestroyHandler();
}
