//-*-c++-*------------------------------------------------------------
// XmwCustom.h
//
// A set of macros for automatic widget creation.
//
// Modified by Qi Yang based on an example by Didier Burton
//
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef XmwCUSTOM_HEADER
#define XmwCUSTOM_HEADER

// includes from C++ library

#include <Xm/Xm.h>
#include <generic.h>

// Define macros which declare and define convenience functions to
// create and retreive widgets created automatically.

#define CustomDeclare(CLASS)                   \
                                               \
extern Widget name2(create,CLASS)(             \
  Widget,                                      \
  String,                                      \
  ArgList, Cardinal );                         \
extern CLASS *name2(get,CLASS)( Widget );      \


#define CustomDefine(CLASS)                    \
                                               \
Widget                                         \
name2(create,CLASS)( Widget   parent,          \
                     String   name,            \
                     ArgList  args,            \
                     Cardinal numArgs ) {      \
                                               \
  CLASS *obj = new CLASS( parent, name,        \
                          args, numArgs );     \
                                               \
  XtVaSetValues( obj->widget(),                \
                 XmNuserData, (XtPointer) obj, \
                 NULL );                       \
                                               \
  return obj->widget();                        \
}                                              \
                                               \
CLASS *                                        \
name2(get,CLASS)( Widget w ) {                 \
                                               \
  assert( XtIsObject( w ) );                   \
                                               \
  XtPointer ptr;                               \
  XtVaGetValues( w,                            \
                 XmNuserData, &ptr,            \
                 NULL );                       \
                                               \
  assert( ptr );                               \
                                               \
  CLASS *obj = (CLASS*) ptr;                   \
                                               \
  return obj;                                  \
}                                              \

#endif
