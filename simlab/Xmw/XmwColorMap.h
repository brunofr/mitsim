#ifndef XmwColorMap_h_
#define XmwColorMap_h_

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

//----------------------------------------------------------------------+

class XmwColorMap
{
public:

  XmwColorMap(Display *display);
  ~XmwColorMap();

  int depth() {	return _depth; }
  Visual* visual() { return _visual; }
  Colormap colormap() { return _colormap; }

private:

  // type is one of: StaticGray, GrayScale, StaticColor, PseudoColor,
  // TrueColor, DirectColor.

  Visual* getVisual( int type, int depth );

  Colormap _colormap;
  Display *_display;
  int _depth;
  Visual* _visual;
};

//----------------------------------------------------------------------+

extern XmwColorMap *theColorMap;

#endif
