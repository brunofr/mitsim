//-*-c++-*------------------------------------------------------------
// XmwSciPlot.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <iostream>
#include <fstream>

#include <cstdio>
#include <cstdlib>
#include <cassert>				// compile with -DNDEBUG to cancel
#include <sys/param.h>

#include <Xm/Xm.h>
#include <Xm/Form.h>
#include <Xm/DrawingA.h>
#include <Xmt/Xmt.h>
#include <Xmt/Layout.h>
#include <Xmt/WidgetType.h>
#include <Xmt/Create.h>
#include <Xmt/Converters.h>
#include <Xmt/Dialogs.h>
#include <Xmt/Menu.h>
#include <Xmt/Chooser.h>

#include <Tools/ToolKit.h>

#include "XmwSciPlot.h"
#include "XmwSciPlotDialog.h"
#include "XmwInterface.h"
#include "XmwColor.h"
using namespace std;

XmwSciPlot::XmwSciPlot(Widget parent, const  String name,
					   ArgList args, Cardinal argc )
   : XmwUIWidget( name ),
	 title_(NULL),
	 xlabel_(NULL),
	 ylabel_(NULL),
	 autoLegended_(0),
	 autoScale_(3),				// both axis are auto scaled
	 nSciColors_(0),
	 sciColors_(NULL)
{
   // The top level widget

      widget_ = XmtBuildToplevel(parent, name);
  //widget_ = XmtBuildDialog(parent, name, NULL, 0);
   assert (widget_);

   // The plot area

   plot_ = XmtNameToWidget(widget_, "*sciplotLayout*plot");
   assert (plot_);

   // Install the destroy handler

   installDestroyHandler();

   // The menu and items (optional)

   Widget menu, item;

   if (menu = XmtNameToWidget(parent, "*sciplotLayout*menu")) {
	  if (item = getMenuItemWidget(menu, "Save")) {
		 addCallback(item, XmNactivateCallback, &XmwSciPlot::save, NULL);
	  }
	  if (item = getMenuItemWidget(menu, "Export")) {
		 addCallback(item, XmNactivateCallback, &XmwSciPlot::eXport, NULL);
	  }
	  if (item = getMenuItemWidget(menu, "Print")) {
		 addCallback(item, XmNactivateCallback, &XmwSciPlot::print, NULL);
	  }
	  if (item = getMenuItemWidget(menu, "Close")) {
		 addCallback(item, XmNactivateCallback, &XmwSciPlot::close, NULL);
	  }
	  if (item = getMenuItemWidget(menu, "Property")) {
		 addCallback(item, XmNactivateCallback, &XmwSciPlot::property, NULL);
	  }

	  if (item = getMenuItemWidget(menu, "About")) {
		 addCallback(item, XmNactivateCallback, &XmwSciPlot::about, NULL);
	  }
	  if (item = getMenuItemWidget(menu, "Index")) {
		 addCallback(item, XmNactivateCallback, &XmwSciPlot::index, NULL);
	  }
   }
   if (menu = XmtNameToWidget(plot_, "*sciplotLayout*popup")) {
	  if (item = getMenuItemWidget(menu, "Property")) {
		 addCallback(item, XmNactivateCallback, &XmwSciPlot::property, NULL);
	  }
   }

   if (!theColorTable) {
	  theColorTable = new XmwColor();
   }

   Pixel clr;
   String *colors;
   int i;
   Widget w = XmtNameToWidget(widget_, "*sciplotSeries*color");
   assert (w);
   XtVaGetValues(w,
				 XmtNnumItems, &nSciColors_,
				 XmtNstrings, &colors,
				 NULL);
   if (nSciColors_) {
	  sciColors_ = new int[nSciColors_];
   } else {
	  sciColors_ = NULL;
   }
   for (i = 0; i < nSciColors_; i ++) {
	  clr = theColorTable->color(colors[i]);
	  sciColors_[i] = SciPlotStoreAllocatedColor(plot_, clr);
   }

   // Used for record the min and max value

   scale_[0] =  FLT_MAX;
   scale_[1] = -FLT_MAX;
   scale_[2] =  FLT_MAX;
   scale_[3] = -FLT_MAX;
}

XmwSciPlot::~XmwSciPlot()
{
   deleteAllSeries();

   // The widget will be automatically destroyed by the base class
   // XmwWidget::destroy(), which is called in the destructor of that
   // class.
}

const char* XmwSciPlot::className() const
{
   return "XmwSciPlot";
}

void XmwSciPlot::set(String *ptr, String s)
{
   if (*ptr == s) return;
   if (*ptr) free(*ptr);
   if (s) *ptr = strdup(s);
   else *ptr = NULL;
}

Widget XmwSciPlot::getMenuItemWidget(Widget parent, const char *name)
{
   if (!parent) return NULL;
   XmtMenuItem *item = XmtMenuGetMenuItem(parent, name);
   if (item) return XmtMenuItemGetWidget(item);
   else return NULL;
}


void XmwSciPlot::post(XmwSciPlot** ptr_to_self_ptr)
{
   // This allows clean destroy itself when the the plot is closed.

   ptr_to_self_ptr_ = ptr_to_self_ptr;

   if (series_.size() > 1) {
	  XtVaSetValues(plot_, XtNshowLegend, True, NULL);
   } else {
	  XtVaSetValues(plot_, XtNshowLegend, False, NULL);
   }

   if (title_) {
	  XtVaSetValues(plot_,
					XtNshowTitle, True,
					XtNplotTitle, title_,
					NULL);
   } else {
	  XtVaSetValues(plot_, XtNshowTitle, False, NULL);
   }

   if (xlabel_) {
	   XtVaSetValues(plot_, XtNxLabel, xlabel_, NULL);
   }

   if (ylabel_) {
	   XtVaSetValues(plot_, XtNyLabel, ylabel_, NULL);
   }

   manage();
   update();
   theBaseInterface->showDefaultCursor();
}

void XmwSciPlot::update()
{
   SciPlotUpdate(plot_);
}

Boolean XmwSciPlot::quickUpdate()
{
   return SciPlotQuickUpdate(plot_);
}

char* XmwSciPlot::pickDefaultLegend()
{
   static char buffer[4];
   autoLegended_ ++;
   sprintf(buffer, "%d", autoLegended_);
   return buffer;
}

void XmwSciPlot::pickDefaultStyle(int series)
{
   const int nMarkStyles = 20;
   int pstyle = (series + 1) % nMarkStyles;
   int color;
   if (nSciColors_) {
	  color = (series + 2) % nSciColors_;
   } else {
	  color = 1;
   }
   SciPlotListSetStyle(plot_, series, color, pstyle, color, XtLINE_SOLID); 
}

void XmwSciPlot::setScales(int n, double *x, double *y)
{
   for (int i = 0; i < n; i ++) {
	  if (x[i] > SCIPLOT_SKIP_VAL) {
		 if (x[i] < scale_[0]) scale_[0] = x[i];
		 if (x[i] > scale_[1]) scale_[1] = x[i];
	  }
	  if (y[i] > SCIPLOT_SKIP_VAL) {
		 if (y[i] < scale_[2]) scale_[2] = y[i];
		 if (y[i] > scale_[3]) scale_[3] = y[i];
	  }
   }
}

void XmwSciPlot::setScales(int n, float *x, float *y)
{
   for (int i = 0; i < n; i ++) {
	  if (x[i] > SCIPLOT_SKIP_VAL) {
		 if (x[i] < scale_[0]) scale_[0] = x[i];
		 if (x[i] > scale_[1]) scale_[1] = x[i];
	  }
	  if (y[i] > SCIPLOT_SKIP_VAL) {
		 if (y[i] < scale_[2]) scale_[2] = y[i];
		 if (y[i] > scale_[3]) scale_[3] = y[i];
	  }
   }
}


int XmwSciPlot::createSeries(int num, float *x, float *y, char *legend)
{
   if (!legend) {
	  legend = pickDefaultLegend();
   }
   int i = SciPlotListCreateFloat(plot_, num, x, y, legend);
   pickDefaultStyle(i);
   series_.push_back(i);
   setScales(num, x, y);
   return i;
}

int XmwSciPlot::createSeries(int num, double *x, double *y, char *legend)
{
   if (!legend) {
	  legend = pickDefaultLegend();
   }
   int i = SciPlotListCreateDouble(plot_, num, x, y, legend);
   pickDefaultStyle(i);
   series_.push_back(i);
   setScales(num, x, y);
   return i;
}

void XmwSciPlot::addToSeries(int series, int num, float *x, float *y)
{
   SciPlotListAddFloat(plot_, series, num, x, y);
   setScales(num, x, y);
}

void XmwSciPlot::addToSeries(int series, int num, double *x, double *y)
{
   SciPlotListAddDouble(plot_, series, num, x, y);
   setScales(num, x, y);
}

void XmwSciPlot::updateSeries(int series, int num, float *x, float *y)
{
   SciPlotListUpdateFloat(plot_, series, num, x, y);
   setScales(num, x, y);
}

void XmwSciPlot::updateSeries(int series, int num, double *x, double *y)
{
   SciPlotListUpdateDouble(plot_, series, num, x, y);
   setScales(num, x, y);
}

void XmwSciPlot::deleteSeries(int idnum)
{
     vector <int> :: iterator i;
//   for (i = series_.begin(); i ++; i < series_.end()) {
     for (i = series_.begin(); i < series_.end();  i ++) {
	  if (*i == idnum) {
		 series_.erase(i);
		 SciPlotListDelete(plot_, idnum);
		 return;
	  }
   }
}

void XmwSciPlot::deleteAllSeries()
{
   for (int i = 0; i < series_.size(); i ++) {
	  SciPlotListDelete(plot_, series_[i]);
   }
   series_.erase(series_.begin(), series_.end());
   autoLegended_ = 0;
}


// THESE ARE CALLBACKS

// Virtual functions to be overloaded in derived classes

void XmwSciPlot::save ( Widget w, XtPointer, XtPointer)
{
   char filename[MAXPATHLEN + 1];
   char directory[MAXPATHLEN + 1];

   filename[0] = '\0';
   directory[0] = '\0';

   Boolean done = 0;

   while (!done &&
		  XmtAskForFilename(w, "askOutputFile", 
							"Enter an output filename:",
							ToolKit::inDir(), NULL,
							filename, sizeof(filename),
							directory, sizeof(directory),
							"*.eps", 0,
							NULL)) {
	  if (ToolKit::fileExists(filename)) {
		 XmtAskForBoolean(w, "confirmFileOverwrite", // reource querey name
						  "File already exists!", // defaul prompt
						  "Overwrite",	// yes default
						  "No",	// no default
						  "Cancel",	// cancel default
						  XmtNoButton, // default button
						  0,	// default icon type
						  False, // show cancel button
						  &done,
						  NULL);
	  } else {
		 done = True;
	  }
	  if (done) {
		 SciPlotPSCreate(plot_, filename);
	  }
   }
}

void XmwSciPlot::eXport ( Widget w, XtPointer, XtPointer)
{
   char filename[MAXPATHLEN + 1];
   char directory[MAXPATHLEN + 1];

   filename[0] = '\0';
   directory[0] = '\0';

   Boolean done = 0;

   while (!done &&
		  XmtAskForFilename(w, "askOutputFile", 
							"Enter an output filename:",
							ToolKit::inDir(), NULL,
							filename, sizeof(filename),
							directory, sizeof(directory),
							"*.txt", 0,
							NULL)) {
	  if (ToolKit::fileExists(filename)) {
		 XmtAskForBoolean(w, "confirmFileOverwrite", // reource querey name
						  "File already exists!", // defaul prompt
						  "Overwrite",	// yes default
						  "No",	// no default
						  "Cancel",	// cancel default
						  XmtNoButton, // default button
						  0,	// default icon type
						  False, // show cancel button
						  &done,
						  NULL);
	  } else {
		 done = True; 
	  }
	  if (done) {
		 FILE *fpo;
		 if (fpo = fopen(filename, "w")) {
			SciPlotExportData(plot_, fpo);
			fclose(fpo);
		 } else {
			theBaseInterface->outputFileError(filename);
		 }
	  }
   }
}

void XmwSciPlot::print ( Widget w, XtPointer, XtPointer)
{
   const int len = 64;
   static char lpcmd[len] = "lp ";

   if (XmtAskForString(w, "printCmd", // query name
					   "Enter the print command",	// default prompt
					   lpcmd,	// buf_in_out
					   len,		// buf_len
					   "Example: lpr -Ptempo")) {
	  char *filename = tmpnam(NULL);
	  SciPlotPSCreate(plot_, filename);
	  
	  const char * cmd = Str("%s %s", lpcmd, filename);
	  system (cmd);
	  remove(filename);
   }
}

void XmwSciPlot::close ( Widget, XtPointer, XtPointer)
{
   if (ptr_to_self_ptr_) {
	  *ptr_to_self_ptr_ = NULL;	  
   }
   delete this;
}

void XmwSciPlot::property ( Widget, XtPointer, XtPointer)
{
   static XmwSciPlotDialog *dialog = NULL;
   if (!dialog) {
	  dialog = new XmwSciPlotDialog(this);
   }
   dialog->post_property_dialog(&dialog);
   // dialog will be set to NULL when close button is pressed.
}


void XmwSciPlot::about ( Widget w, XtPointer, XtPointer)
{
   XmtDisplayInformation(w, "aboutSciPlot",
	  "Thanks Rob McMullen",
	  "About SciPlot");
}

void XmwSciPlot::index ( Widget, XtPointer, XtPointer)
{
   if (theBaseInterface) {
	  theBaseInterface->openUrl("sciplot", "plot.html");
   }
}
