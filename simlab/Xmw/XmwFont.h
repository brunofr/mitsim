//-*-c++-*------------------------------------------------------------
// XmwFont.h
//
// Peter J. Welch and Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class definition for the class XmwFont.
// This class allows you to create an array of scalable fonts with a
// smooth size variance.
//--------------------------------------------------------------------

#ifndef XmwFONT_HEADER
#define XmwFONT_HEADER

#include <X11/Intrinsic.h>

extern char *theFixedFont;
extern char *theScalableFont;
extern int *theFontSizes;
extern int theNumFonts;

class XmwFont
{
   public:

      XmwFont();
      ~XmwFont();
      int nFonts() { return nFonts_; }
      XFontStruct * font(int i);
	  XFontStruct * fixed() { return fixed_; }

   private:

	  Display *display_;
	  double xres_;				// screen resolution
	  double yres_;				// in dots per inch
      XFontStruct * fixed_;
      int nFonts_;
      XFontStruct ** scalable_;

      XFontStruct * queryScalableFont(int size, const char *font_name);
};

extern XmwFont *theFontTable;

#endif
