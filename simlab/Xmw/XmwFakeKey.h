//-*-c++-*------------------------------------------------------------
// XmwFakeKey.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef XMWFAKEKEY_HEADER
#define XMWFAKEKEY_HEADER

#include <X11/Xlib.h>
#include <X11/extensions/XTest.h>

struct XmwFakeKey {
  Window window;
  KeySym keysym;
};

#endif
