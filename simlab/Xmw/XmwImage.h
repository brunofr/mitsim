//-*-c++-*------------------------------------------------------------
// XmwImage.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class definition for the class XmwImage
//--------------------------------------------------------------------

#ifndef XmwIMAGE_HEADER
#define XmwIMAGE_HEADER

class XmwImage
{
  public:

	XmwImage(const char *name,
			  char *img_bits, char *img_mask,
			  int width, int height);

	virtual ~XmwImage();

	const char *name() { return name_; }

  private:

	char *name_;
};

#endif
