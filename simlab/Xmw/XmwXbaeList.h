//-*-c++-*------------------------------------------------------------
// XmwXbaeList.h
//
// Provides a list interface, which contains selectable items includes
// from 'MotifUI' library
//
// Qi Yang -- based an example by Didier Burton
//
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef XmwXbaeLIST_HEADER
#define XmwXbaeLIST_HEADER

#include "XmwUIWidget.h"
#include "XmwCustom.h"
#include "XmwCallback.h"

class XmwXbaeList : public XmwUIWidget {

   public:

	  XmwXbaeList( Widget, const String,
				   ArgList args = NULL, Cardinal argc = 0
		 );

	  String getCell(int row, int col) const;
	  String* getCells() const;

	  void insertRows(int pos,
					  String *rows = NULL,
					  Pixel  *colors = NULL,
					  int num = 1);

	  void appendRows(String *rows = NULL,
					  Pixel  *colors = NULL,
					  int num = 1) {
		 insertRows(nRows(), rows, colors, num);
	  }

	  int  visibleRows();
	  void setVisibleRows( int );

	  void deleteRows( int row, int num = 1);
	  void deleteAll();

	  void selectRow( int );
	  void commitEdit();

	  int nRows() const;
	  int nCols() const;

	  virtual const char * className() const;

	  inline Widget xmw() { return xmw_; }
	  void setColAttrs(int ncols, short int *widths, String *label = NULL);

   private:

	  int row_;					// current row pos
	  Widget xmw_;				// XbaeMatrix
	  Widget insert_, delete_, append_;	// Push buttons

   private:

	  CallbackDeclare(XmwXbaeList);

	  void enterCB(Widget, XtPointer, XtPointer);

	  void insertCB(Widget, XtPointer, XtPointer);
	  void deleteCB(Widget, XtPointer, XtPointer);
	  void appendCB(Widget, XtPointer, XtPointer);
};

// declare a creator for automatic widget creation with Xmt resources.

CustomDeclare(XmwXbaeList)

#endif
