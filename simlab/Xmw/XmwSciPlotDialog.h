//-*-c++-*------------------------------------------------------------
// XmwSciPlotDialog.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef XMW_SCIPLOT_DIALOG_HEADER
#define XMW_SCIPLOT_DIALOG_HEADER

#include <Xmw/XmwDialogManager.h>
#include <Rob/ListTree.h>

class XmwSciPlot;

class XmwSciPlotDialog : public XmwDialogManager
{
   public:

	  XmwSciPlotDialog(XmwSciPlot* parent);
	
	  ~XmwSciPlotDialog() { }

	  void post_property_dialog(XmwSciPlotDialog **ptr);

   private:

	  CallbackDeclare(XmwSciPlotDialog);
	  
   protected:

	  // overload the virtual functions

	  void okay(Widget, XtPointer, XtPointer);
	  void cancel(Widget, XtPointer, XtPointer);
	  void help(Widget, XtPointer, XtPointer);

	  // new callback functions

	  void autoScaleChanged(Widget, XtPointer, XtPointer);
	  void axisOptionChanged(Widget, XtPointer, XtPointer);

	  void treeItemHighlight(Widget, XtPointer , XtPointer);
	  void treeItemActivate(Widget, XtPointer , XtPointer);
#ifdef OUT
	  void treeItemMenu(Widget, XtPointer , XtPointer);
#endif

   private:
	  
	  XmwSciPlotDialog **ptr_;	// used for destroy itself when closed

	  XmwSciPlot *sciplot_;
	  Widget plot_;				// SciPlot widget owned by parent sciplot_

	  Widget tree_;				// ListTree Widget
	  ListTreeItem *series_root_;

	  Widget chart_;			// XmtLayout
	  Widget title_;			// XmtInputField
	  Widget chartOption_;		// XmtChooser - CheckBox
	  Widget marksize_;			// ComboBox

	  Widget axis_;				// XmtLayout
	  Widget label_;			// XmtInputField
	  Widget axisOption_;		// XmtChooser - CheckBox
	  Widget autoScale_;		// XmToggleButton
	  Widget min_;				// XmtInputField
	  Widget max_;				// XmtInputField

	  Widget fonts_;			// XmtLayout
	  Widget fontname_;			// XmtChooser - Option
	  Widget fontsize_;			// ComboBox
	  Widget fontstyle_;		// XmtChooser - CheckBox

	  Widget series_;			// XmtLayout
	  Widget line_;				// XmtChooser
	  Widget marker_;			// XmtChooser
	  Widget color_;			// XmtChooser

	  enum {
		 ITEM_CHART       = 0x0100,
		 ITEM_X_AXIS      = 0x0200,
		 ITEM_Y_AXIS      = 0x0300,
		 ITEM_TITLE_FONT  = 0x0400,
		 ITEM_AXIS_FONT   = 0x0500,
		 ITEM_LABEL_FONT  = 0x0600,
		 ITEM_PLOT        = 0x0F00,
		 ITEM_NONE        = 0x1000
	  };

	  int tag_;
	  Widget current_;

	  ListTreeItem *addTreeItem(ListTreeItem *parent,
		 char *label, int tag);

	  Widget pageUp();
	  Widget pageDown();

	  void getChartPageInfo();
	  Boolean getAxisPageInfo(String str, Boolean checks[], int n,
		 double scale[]);
	  int getFontPageInfo();
	  void getLinePageInfo(int who);

	  void setChartPageInfo();
	  void setAxisPageInfo(int who, Boolean checks[], int n);
	  void setFontPageInfo(int option);
	  void setLinePageInfo(int who);

	  void switchTo(int);
};

#endif
