//-*-c++-*------------------------------------------------------------
// XmwCheckerField.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <assert.h>

#include <Xmt/Xmt.h>
#include <Xmt/Chooser.h>
#include <Xmt/InputField.h>
#include <Xm/ToggleB.h>

#include "XmwCheckerField.h"
#include "XmwInterface.h"

XmwCheckerField::XmwCheckerField(Widget parent, const char *name,
								 unsigned int maskbit)
   : mask_(maskbit)
{
   // Layout of this XmwCheckerField

   Widget w = XtNameToWidget(parent, name);
   if (!w) {
	  theBaseInterface->widgetMissingBoilout(name);
   }

   checker_ = XtNameToWidget(w, "*check");
   assert(checker_);

   input_ = XtNameToWidget(w, "*fld");
   assert(input_);
}

void XmwCheckerField::set(const char *value)
{
   XmtInputFieldSetString(input_, value);
}

const char* XmwCheckerField::get()
{
   return XmtInputFieldGetString(input_);
}

void XmwCheckerField::check(Boolean state)
{
   XmToggleButtonSetState(checker_, state, False);
}

Boolean XmwCheckerField::isChecked()
{
   return XmToggleButtonGetState(checker_);
}

