//-*-c++-*------------------------------------------------------------
// XmwWidget.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class definition for the class XmwWidget.
// This is abstract base class for all Xmw widgets.
//--------------------------------------------------------------------

#ifndef XmwWIDGET_HEADER
#define XmwWIDGET_HEADER

#include <string.h>
#include <Xm/Xm.h>

class XmwWidget
{
   protected:

	  // prevent direct instantiation

	  XmwWidget () : name_(NULL), widget_(NULL) { }
      XmwWidget (const char *name) : name_(name), widget_(NULL) { }
	  XmwWidget (Widget w) : name_(NULL), widget_(w) { }
	  XmwWidget (const char *name, Widget w)
		 : name_(name), widget_(w) {
	  }

   public:

      virtual ~XmwWidget ();

	  const char * name () { return name_; }

      inline Widget widget () { return widget_; }
      virtual inline operator Widget () {
		 return widget_;
	  }

	  inline void manage () {
		 manage (widget_);
	  }
	  inline void unmanage () {
		 unmanage (widget_);
	  }

	  void manage (Widget w);
	  void unmanage (Widget w);
	  void destroy (Widget w);

   protected:

      Widget widget_;
      const char * name_;
};

#define RES_CONVERT(r, v) XtVaTypedArg, r, XmRString, v, strlen(v)+1

#endif
