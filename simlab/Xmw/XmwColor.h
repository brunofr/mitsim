//-*-c++-*------------------------------------------------------------
// XmwColor.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class implementation for the class XmwColor,
// which allocates some named colors and a list of colors for a color
// rain bow.
//
// This class will provide a translation between percentage (0.0 -
// 1.0) and color.  Instead of the user having to deal with pixel
// values directly, this class allows the user to initialize a range
// of colors (by default from "cold" to "hot") and access a location
// in this range by giving a percentage.
//
// Depending on the available colorcells in the default colormap the
// step size may vary.
//
// The aim with this class is to provide a mechanism by which we can
// specify colors in the HSL color map or from hot to cold.
//  
// Here is a summary of the HLS colormap:
//
//   H = hue: range 0 - 360
//   S = chroma (also known as Saturation): range 0-100
//   L = lightness: 0-100
//
// The interface is as follows:
//
//   XmwColor(float hue_start = BLUE, float hue_end = HOT_PINK);
//   Pixel color(float value);
//
// The value returned will be of type Pixel.  
//
//   Example Code:
//
//   XmwColor color;
//   Pixel c;         +--- intensity (0.0-1.0) dark to bright
//                    |
//     c = color(1.0, 1.0);  // pure red
//               |
//   or...       +--- A color value (0.0-1.0) blue to red
//               |
//     c = color(1.0, 0.3);  // darker shade of red
//--------------------------------------------------------------------

#ifndef XmwCOLOR_HEADER
#define XmwCOLOR_HEADER

#include <X11/Intrinsic.h>

class XmwColor
{
public:

  XmwColor();
  ~XmwColor();

  // setsup the color range from hue_ start to hue_end assuming
  // maximum value and chroma

  Pixel background() { return black_; }
  Pixel foreground() { return white_; }

  Pixel white() { return white_; }
  Pixel black() { return black_; }

  Pixel darkBlue() { return darkBlue_; }
  Pixel blue() { return blue_; }
  Pixel lightGreen() { return lightGreen_; }
  Pixel green() { return green_; }
  Pixel yellow() { return yellow_; }
  Pixel orange() { return orange_; }
  Pixel red() { return red_; }
  Pixel lightRed() { return lightRed_; }
  Pixel magenta() { return magenta_; }
  Pixel cyan() { return cyan_; }
  Pixel lightGray() { return lightGray_; }
  Pixel darkGray() { return darkGray_; }
  Pixel gray() { return gray_; }
  Pixel water() { return water_; }
  Pixel inciBefore() { return inciBefore_; }
  Pixel inciAfter() { return inciAfter_; }

  int nCells() { return nCells_; }
  int nCols() { return nCols_; }
  int nRows() { return nRows_; }
  int nColors() { return (nColors_ + nCells_); }

  Pixel color(float clr);
  Pixel color(float clr, float intensity);
  Pixel pixel(int c, int i);
  Pixel pixel(int c);

  Pixel color(const char *name);
  Pixel color(const char *defa, const char *symbol);

private:

  Display *display_;
  Colormap cmap_;				// colormap

  int nColors_;
  Pixel black_;
  Pixel white_;
  Pixel darkBlue_;
  Pixel blue_;
  Pixel green_;
  Pixel lightGreen_;
  Pixel yellow_;
  Pixel orange_;
  Pixel red_;
  Pixel lightRed_;
  Pixel magenta_;
  Pixel lightGray_;
  Pixel darkGray_;
  Pixel gray_;
  Pixel cyan_;
  Pixel water_;
  Pixel inciBefore_;
  Pixel inciAfter_;

  // These defines the rainbows

  int nRows_;
  int nCols_;
  int nCells_;
  Pixel *pixels_;

private:

  // Value for input arguments are in range [0,255]

  Pixel rgb(unsigned int r, unsigned int g, unsigned int b);

  // Value for input arguments are hue=[0,360], saturation=[0,100],
  // value=[0,100]
  
  Pixel hsl(unsigned int h, unsigned int s, unsigned int l);

  // Value for all input arguments are in range of [0.0,1.0].

  Pixel rgb01(float red, float green, float blue);
  Pixel hsl01(float hue, float chroma, float value);

  void createRainBow();
  void organizeRainBow();
};

extern XmwColor * theColorTable;

#endif
