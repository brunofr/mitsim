//-*-c++-*------------------------------------------------------------
// XmwXbaeList.cc
//
// qiyang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <iostream>

#include "XmwXbaeList.h"

#include <Xm/Form.h>
#include <Xm/RowColumn.h>
#include <Xm/PushBG.h>
#include <Xmt/Xmt.h>
#include <Xbae/Matrix.h>
#include <cstdlib>
#include <cassert>				// compile with -DNDEBUG to cancel
using namespace std;

XmwXbaeList::XmwXbaeList(Widget parent, const  String name,
						 ArgList args, Cardinal argc )
   : XmwUIWidget( name ), row_ (-1)
{
   // by default the list has no item and only one column

   static short int widths[] = { 10 };

   widget_ = XtVaCreateWidget(
 	  name, xmFormWidgetClass,
 	  parent,
 	  NULL);

   // Create add and delete buttons

   Widget box = XtVaCreateManagedWidget(
 	  "box", xmRowColumnWidgetClass, widget_,
  	  XmNtopAttachment, XmATTACH_FORM,
 	  XmNtopOffset, 4,
 	  XmNrightAttachment, XmATTACH_FORM,
 	  XmNrightOffset, 4,
 	  NULL);

   insert_ = XtVaCreateManagedWidget(
 	  "insert", xmPushButtonGadgetClass, box,
 	  NULL);
   addCallback(
 	  insert_,
 	  XmNactivateCallback, &XmwXbaeList::insertCB,
 	  NULL);

   delete_ = XtVaCreateManagedWidget(
 	  "delete", xmPushButtonGadgetClass, box,
 	  NULL);
   addCallback(
 	  delete_,
 	  XmNactivateCallback, &XmwXbaeList::deleteCB,
 	  NULL);

   append_ = XtVaCreateManagedWidget(
 	  "append", xmPushButtonGadgetClass, box,
 	  NULL);
   addCallback(
 	  append_,
 	  XmNactivateCallback, &XmwXbaeList::appendCB,
 	  NULL);

   // create a matrix from parent

   xmw_ = XtVaCreateManagedWidget(
	  "XmwXbaeList",
	  xbaeMatrixWidgetClass, widget_,
	  XmNreverseSelect,	True,
	  XmNcolumns, 1,
	  XmNcolumnWidths, widths,
	  XmNtraversalOn, True,
	  XmNverticalScrollBarDisplayPolicy, XmDISPLAY_STATIC,
	  XmNhorizontalScrollBarDisplayPolicy, XmDISPLAY_AS_NEEDED,
 	  XmNtopAttachment, XmATTACH_FORM,
	  XmNtopOffset, 4,
	  XmNleftAttachment, XmATTACH_FORM,
	  XmNleftOffset, 4,
	  XmNbottomAttachment,XmATTACH_FORM,
	  XmNbottomOffset, 4,
 	  XmNrightAttachment, XmATTACH_WIDGET,
 	  XmNrightOffset, 4,
 	  XmNrightWidget, box,
	  NULL);

   addCallback(
	  xmw_,
	  XmNenterCellCallback, &XmwXbaeList::enterCB,
	  NULL);

   // set default resources.

   XtVaSetValues(
	  xmw_,
	  XmNgridType, XmGRID_SHADOW_IN,
	  XmNcellShadowType, XmSHADOW_IN,
	  XmNcellShadowThickness, 1,
	  XmNshadowThickness, 1,
	  XmNvisibleRows, 4,
	  XmNallowColumnResize, True,
	  NULL );

   // Install the destroy handler

   installDestroyHandler();

   // set resources coming from the resource file (which may override
   // previous default ones).

   XtSetValues( xmw_, args, argc );
   
   if (nRows() <= 0) {
	  XtSetSensitive(delete_, False);
   }

   // managing the scrolled list is done by the automatic widget
   // creation process.
}

const char * XmwXbaeList::className() const
{
   return "XmwXbaeList";
}

void XmwXbaeList::setColAttrs(int ncols, short int *widths, String *labels)
{
   XtVaSetValues(
	  xmw_,
	  XmNcolumns, ncols,
	  XmNcolumnWidths, widths,
	  XmNcolumnLabels, labels,
	  NULL);
}


int XmwXbaeList::nRows() const
{
   return XbaeMatrixNumRows(xmw_);
}

int XmwXbaeList::nCols() const
{
   return XbaeMatrixNumColumns(xmw_);
}

String XmwXbaeList::getCell(int row, int col) const
{
   if (row < 0 || row >= nRows() ||
	   col < 0 || col >= nCols()) {
	  return NULL;
   } else {
	  return XbaeMatrixGetCell(xmw_, row, col);
   }
}

String* XmwXbaeList::getCells() const
{
   String* cells = NULL;
   XtVaGetValues(xmw_, XmNcells, &cells, NULL);
   return cells;
}

void XmwXbaeList::commitEdit()
{
   if (row_ >= 0 && row_ < nRows()) {
	  XbaeMatrixCommitEdit(xmw_, False);
   }
}

void XmwXbaeList::insertCB(Widget, XtPointer, XtPointer)
{
   // Bound checking and save previous edits

   if (row_ < 0) row_ = 0;
   else if (row_ > nRows()) row_ = nRows();
   else XbaeMatrixCommitEdit(xmw_, False);

   // Add an empty row
   
   insertRows(row_);
}

void XmwXbaeList::deleteCB(Widget, XtPointer, XtPointer)
{
   // Must have a row selected

   if (row_ < 0 || row_ >= nRows()) {
	  return;					// no row selected
   } 

   // Delete the selected row

   deleteRows(row_);
}


void XmwXbaeList::appendCB(Widget, XtPointer, XtPointer)
{
   commitEdit();
   row_ = nRows();
   insertRows(row_);
}

void XmwXbaeList::enterCB(Widget, XtPointer, XtPointer call)
{
   XbaeMatrixEnterCellCallbackStruct *cbs =
	  (XbaeMatrixEnterCellCallbackStruct*) call;
   XbaeMatrixSelectRow(xmw_, cbs->row);
   selectRow(cbs->row);
   cbs->select_text = True;
}


int
XmwXbaeList::visibleRows()
{
   return XbaeMatrixVisibleRows(xmw_);
}


void
XmwXbaeList::setVisibleRows( int count )
{
   XtVaSetValues(
	  xmw_,
	  XmNvisibleRows, count,
	  NULL );
}

void
XmwXbaeList::selectRow(int row)
{
   XbaeMatrixDeselectAll(xmw_);
   if (row < 0 || row >= nRows()) return;
   XbaeMatrixSelectRow(xmw_, row);
   row_ = row;
}

void
XmwXbaeList::insertRows(
   int pos, String *rows, Pixel *colors, int num )
{
   XbaeMatrixAddRows(xmw_, pos, rows, NULL, colors, num);
   if (!XtIsSensitive(delete_)) {
	  XtSetSensitive(delete_, True);
   }
   selectRow(pos);				// select the 1st inserted row
}


void
XmwXbaeList::deleteRows( int row, int num )
{
   if (row >= 0 && row < nRows()) {
	  XbaeMatrixDeleteRows(xmw_, row, num);
   }
   if (row_ >= nRows()) {		// last row deleted
	  row_ --;					// move to previous row
   }
   if (row_ < 0) {
	  XtSetSensitive(delete_, False);
   } else {
	  selectRow(row_);
   }
}


void
XmwXbaeList::deleteAll()
{
   deleteRows(0, nRows());
}


// define the List automatic creator functions

CustomDefine(XmwXbaeList)
