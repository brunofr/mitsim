//$Id: XmwTab.h,v 1.1 1998/02/06 20:18:09 qiyang Exp $
//----------------------------------------------------------------------+
// FILE NAME   : XmwTab.h
// AUTHOR      : Didier Burton
// CREATION    : Fri Oct 17 16:25:20 1997
//----------------------------------------------------------------------+

#ifndef XmwTab_h_
#define XmwTab_h_

#include "XmwWidget.h"

//----------------------------------------------------------------------+

class XmwTab : public XmwWidget {

public:
                                    // Constructors

  XmwTab( Widget        parent_,
		const String  name_,
		ArgList       args_,
		Cardinal      numArgs_ );

                                    // Destructor
  virtual ~XmwTab();

  Boolean setActivePage( int page_, int option_ );
  int     getActivePage();
  Widget  getActivePageWidget();
                                    // declare a creator for automatic
                                    // widget creation with Xmt resources.

  static Widget createXmwTab( Widget, const String, ArgList, Cardinal );
  static XmwTab* getXmwTab( Widget );

protected:

private:

};

//----------------------------------------------------------------------+

#endif /* XmwTab_h_ */

//----------------------------------------------------------------------+
