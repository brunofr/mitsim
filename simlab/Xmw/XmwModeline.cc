//-*-c++-*------------------------------------------------------------
// XmwModeline.cc
//
// qiyang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <assert.h>
#include <stdio.h>

#include <Xm/ToggleB.h>
#include <Xmt/Xmt.h>
#include <Xmt/Create.h>
#include <Xmt/MsgLine.h>

#include <Tools/SimulationClock.h>

#include "XmwModeline.h"
#include "XmwInterface.h"
#include "XmwColor.h"

XmwModeline::XmwModeline(Widget parent)
{
  timers_ = XmtNameToWidget(parent, "*timers");
  assert(timers_);
  XtVaSetValues(timers_, XmNtraversalOn, False, NULL);

  meters_ = XmtNameToWidget(parent, "*meters");
  assert (meters_);
  XtVaSetValues(meters_, XmNtraversalOn, False, NULL);

  progress_ = XmtNameToWidget(parent, "*progress");
  assert (progress_);
  XtVaSetValues(progress_, XmNtraversalOn, False, NULL);
  static String help_progress[1];
  help_progress[0] = XmtLocalize2(progress_,
								  "Percent done", "progress", "pctdone");
  XtAddCallback(progress_,
			  XmNenterCellCallback, &XmwModeline::cellEnterCB,
			  help_progress);

  key_ = XmtNameToWidget(parent, "*key");
  assert(key_);
  XtAddCallback(key_,
			  XmNvalueChangedCallback, &XmwModeline::keyClickCB,
			  this);
  strcpy(keyTipTag_, "key");
  XmwInterface::addTip(key_, keyTipTag_);

  setKeyState(KEY_STATE_DISABLED);
}

void XmwModeline::cellEnterCB(Widget w, XtPointer client, XtPointer call)
{
  XbaeMatrixEnterCellCallbackStruct *cbs =
	(XbaeMatrixEnterCellCallbackStruct*) call;
  String *msg = (String *)client;
  theBaseInterface->msgSet(msg[cbs->column]);
}


void XmwModeline::keyClickCB(Widget, XtPointer client, XtPointer)
{
  XmwModeline *obj = (XmwModeline *) client;
  if (XmToggleButtonGetState(obj->key_)) {
	obj->keyStateChanged(KEY_STATE_RUN);
  } else {
	obj->keyStateChanged(KEY_STATE_PAUSE);
  }
}

void XmwModeline::updateTimer(int i, double t)
{
  const String s1 = XbaeMatrixGetCell(timers_, 0, i);
  const String s2 = XmwInterface::convertTime(t);
  if (strcmp(s1, s2)) {
	XbaeMatrixSetCell(timers_, 0, i, s2);
  }
}

void XmwModeline::updateMeter(int i, double r)
{
  static char buf[12];
  if (r < 0.1) {
	strcpy(buf, "0");
  } else if (r > 1.0E3) {
	strcpy(buf, "*");
  } else {
	sprintf(buf, "%.1lf", r);
  }
  const String s = XbaeMatrixGetCell(meters_, 0, i);
  if (strcmp(s, buf)) {
	XbaeMatrixSetCell(meters_, 0, i, buf);
  }
}

void XmwModeline::updateProgress(int i, float p)
{
  static char buf[12];
  sprintf(buf, "%3.0f%%", p);
  XbaeMatrixSetCell(progress_, 0, i, buf);
  theBaseInterface->handlePendingEvents();
}

void XmwModeline::update()
{
  updateTimer(0, theSimulationClock->currentTime());
  float pct = theSimulationClock->percentDone();
  long int done = (long int) (10.0 * pct + 0.5);
  if (done % 10 == 0) {
	updateProgress(0, pct);
	updateTimer(1, theSimulationClock->expectedTimeToGo());
  }
  theBaseInterface->handlePendingEvents();
}

void XmwModeline::setKeyState(enum KeyState state)
{
  switch (state) {
  case KEY_STATE_RUN:
	{
	  XmToggleButtonSetState(key_, True, False);
	  XtVaSetValues(key_, XmNsensitive, True, NULL);
	  strcpy(keyTipTag_, "pkey");
	  break;
	}
  case KEY_STATE_PAUSE:
	{
	  XmToggleButtonSetState(key_, False, False);
	  XtVaSetValues(key_, XmNsensitive, True, NULL);
	  strcpy(keyTipTag_, "rkey");
	  break;
	}
  case KEY_STATE_DISABLED:
	{
	  XtVaSetValues(key_, XmNsensitive, False, NULL);
	  strcpy(keyTipTag_, "key");
	  break;
	}
  }
}
