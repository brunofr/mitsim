//-*-c++-*------------------------------------------------------------
// XmwMenu.h
//
// qiyang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef XmwMENU_HEADER
#define XmwMENU_HEADER

#include <Xmt/Menu.h>
#include "XmwWidget.h"

class XmwMenu : public XmwWidget
{
public:

  enum {
	STATE_SAVE_MASTER	= 0x0001,
	STATE_SAVE_NETWORK = 0x0002,
	STATE_SAVE_GDS	    = 0x0004,
	STATE_SAVE         = 0xFFFF
  };

  XmwMenu(Widget parent);
  virtual ~XmwMenu() { }

  void activate(XmtMenuItem *item);
  void deactivate(XmtMenuItem *item);
  void setToggleState(XmtMenuItem *item, Boolean state);

  // state manipulators

  inline unsigned int state(unsigned int mask = 0xFFFFFFFF) {
	return (state_ & mask);
  }
  inline void setState(unsigned int s) {
	state_ |= s;
  }
  inline void unsetState(unsigned int s) {
	state_ &= ~s;
  }

  void needSave(unsigned int what = STATE_SAVE_MASTER);
  Boolean isSaveNeeded(unsigned int flag = STATE_SAVE) {
	return state(flag);
  }

protected:

  virtual void open ( Widget, XtPointer, XtPointer );
  virtual void save ( Widget, XtPointer, XtPointer );
  virtual void saveAs ( Widget, XtPointer, XtPointer );
  virtual void print ( Widget, XtPointer, XtPointer );
  virtual void quit ( Widget, XtPointer, XtPointer );

  virtual void randomizer ( Widget, XtPointer, XtPointer );

  virtual void about ( Widget, XtPointer, XtPointer );
  virtual void contextHelp ( Widget, XtPointer, XtPointer );
  virtual void index ( Widget, XtPointer, XtPointer );
  virtual void credit ( Widget, XtPointer, XtPointer );

private:
	
  // static callbacks

  static void openCB ( Widget, XtPointer, XtPointer );
  static void saveCB ( Widget, XtPointer, XtPointer );
  static void saveAsCB ( Widget, XtPointer, XtPointer );
  static void printCB ( Widget, XtPointer, XtPointer );
  static void quitCB ( Widget, XtPointer, XtPointer );

  static void randomizerCB ( Widget, XtPointer, XtPointer );

  static void aboutCB ( Widget, XtPointer, XtPointer );
  static void contextHelpCB ( Widget, XtPointer, XtPointer );
  static void indexCB ( Widget, XtPointer, XtPointer );
  static void creditCB ( Widget, XtPointer, XtPointer );

protected:

  Widget getItemWidget(Widget parent, const char *name);
  Widget getItemWidget(const char *name);
  Widget getItemWidget(XmtMenuItem *);
  XmtMenuItem *getItem(const char *name);

  unsigned int state_;		// current state

  XmtMenuItem *open_;
  XmtMenuItem *save_;
  XmtMenuItem *saveAs_;
  XmtMenuItem *print_;
  XmtMenuItem *quit_;
};

#endif
