//-*-c++-*------------------------------------------------------------
// XmwDialogManager.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef XmwDIALOGMANAGER_HEADER
#define XmwDIALOGMANAGER_HEADER

#include "XmwWidget.h"

class XmwDialogManager : public XmwWidget
{
public:

  XmwDialogManager(Widget parent, String name,
				   XtResourceList resources = NULL,
				   Cardinal num_resources = 0,
				   const char *okayTag = "okay",
				   const char *cancelTag = "cancel",
				   const char *helpTag = "help");
  virtual ~XmwDialogManager() { }

  virtual void setSensitivity(int s);
  virtual void post();
  void ready();

  static void activate(Widget);
  static void deactivate(Widget);

protected:

  Widget okay_;
  Widget cancel_;
  Widget help_;

  virtual void okay(Widget, XtPointer, XtPointer);
  virtual void cancel(Widget, XtPointer, XtPointer);
  virtual void help(Widget, XtPointer, XtPointer);

private:

  static void OkayCB(Widget w, XtPointer tag, XtPointer data);
  static void CancelCB(Widget w, XtPointer tag, XtPointer data);
  static void HelpCB(Widget w, XtPointer tag, XtPointer data);
};

#endif
