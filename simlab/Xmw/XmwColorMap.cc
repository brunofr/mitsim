// $Id: XmwColorMap.cc,v 1.2 1998/02/02 20:35:39 qiyang Exp $
//----------------------------------------------------------------------+
//
// FILE NAME   : XmwColorMap.cc
// AUTHOR      : Qi Yang
//
//----------------------------------------------------------------------+

#include "XmwColorMap.h"

#include <Xmt/Xmt.h>
#include <iostream>
using namespace std;

XmwColorMap *theColorMap = NULL;

//----------------------------------------------------------------------+

XmwColorMap::XmwColorMap(Display *display_)
  : _display(display_)
{
  theColorMap = this;

  // We may also add StaticColor, GrayScale, and StaticGray.

  int screen_num = DefaultScreen(display_);
  Screen *screen = DefaultScreenOfDisplay(display_);
  Window root_window = DefaultRootWindow(display_);

  cout << "Searching the best visual and colormap ..." << endl;
  static int depths[] = { 24, 16, 8 };
  static int visuals[] = {
	TrueColor, DirectColor, PseudoColor, StaticColor
  };
  static const char* visual_types[] = {
	"true color", "direct color", "pseudo color", "static color"
  };

  XVisualInfo vinfo;
  int num_depths = XtNumber(depths);
  int num_visuals = XtNumber(visuals);
  for (int d = 0; d < num_depths; d ++) {
	for (int v = 0; v < num_visuals; v ++) {
	  cout << "  " << depths[d] << "-bit "
		   << visual_types[v] << " ... ";
	  if (XMatchVisualInfo(display_, screen_num,
						   depths[d], visuals[v], &vinfo)) { 
		if (_colormap = XCreateColormap(display_, root_window,
										vinfo.visual, AllocNone)) {
		  _visual = vinfo.visual;
		  _depth = depths[d];
		  
		  cout << "Yes." << endl;
		  return;				// succeed
		} else {
		  cout << "No." << endl;
		}
	  } else {
		cout << "No." << endl;
	  }
	}
  }
		
  // Use default colormap

  _colormap = DefaultColormapOfScreen(screen);
  _visual = DefaultVisualOfScreen(screen);
  _depth = DefaultDepthOfScreen(screen);
  
  cout << "Fallback to default " << _depth
 	   << "-bit visual and colormap." << endl;
}

//----------------------------------------------------------------------+

XmwColorMap::~XmwColorMap()
{
  theColorMap = NULL;
}

