//-*-c++-*------------------------------------------------------------
// XmwInputField.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef XMWINPUTFIELD_HEADER
#define XMWINPUTFIELD_HEADER

#include <Xmt/Xmt.h>
#include <Xmt/InputField.h>

class XmwInputField
{
public:

  XmwInputField() : input_(NULL) { }
  XmwInputField(Widget parent, const char *name) {
	lookup(parent, name);
  }
  void lookup(Widget parent, const char *name);
  virtual ~XmwInputField() { }

  void set(const char* value);
  void set(double v);
  void set(int v);

  String get();
  double getTime();

  Widget widget() { return input_; }

private:

  Widget input_;
};

#endif
