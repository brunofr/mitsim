//-*-c++-*------------------------------------------------------------
// XmwMouseInteractor.C
//
// Peter Welch and Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class implementation for the class
// XmwMouseInteractor.
//--------------------------------------------------------------------

#include "XmwMouseInteractor.h"
#include "XmwKeyInteractor.h"
#include "XmwDrawingArea.h"

XmwMouseInteractor::XmwMouseInteractor(XmwDrawingArea * drawing_area) 
   : drawing_area_(drawing_area),
     modifier_(0)
{
   Widget w = drawing_area_->widget();

   XtAddEventHandler(
	  w, ButtonPressMask, FALSE,
	  (XtEventHandler) ButtonPressHandler, 
	  this);

   XtAddEventHandler(
	  w, ButtonMotionMask, FALSE,
	  (XtEventHandler) ButtonMotionHandler, 
	  this);

   XtAddEventHandler(
	  w, ButtonReleaseMask, FALSE, 
	  (XtEventHandler)ButtonReleaseHandler, 
	  this);

   XtAddEventHandler(
	  w, KeyPressMask, FALSE,
	  KeyPressCallback, this);

   XtAddEventHandler(
	  w, KeyReleaseMask, FALSE,
	  KeyReleaseCallback, this);
}


XmwMouseInteractor::~XmwMouseInteractor()
{
   Widget w = drawing_area_->widget();

   XtRemoveEventHandler(
	  w, ButtonPressMask, FALSE, 
	  (XtEventHandler) ButtonPressHandler, 
	  this);

   XtRemoveEventHandler(
	  w, ButtonMotionMask, FALSE, 
	  (XtEventHandler) ButtonMotionHandler, 
	  this);

   XtRemoveEventHandler(
	  w, ButtonReleaseMask, FALSE, 
	  (XtEventHandler) ButtonReleaseHandler, 
	  this);

   XtRemoveEventHandler(
	  w, KeyPressMask, FALSE,
	  KeyPressCallback, this);

   XtRemoveEventHandler(
	  w, KeyReleaseMask, FALSE,
	  KeyReleaseCallback, this);
}


void
XmwMouseInteractor::ButtonPressHandler(
   Widget, 
   XtPointer client, 
   XEvent * event, 
   Boolean*) 
{
   XmwMouseInteractor * cli = (XmwMouseInteractor*) client;

   XmwPoint pt(event->xbutton.x, event->xbutton.y);
   
   int control = cli->modifier(MODIFIER_CONTROL);
   int shift   = cli->modifier(MODIFIER_SHIFT);
   int meta    = cli->modifier(MODIFIER_META);
   int alt     = cli->modifier(MODIFIER_ALT);
   int escape  = cli->modifier(MODIFIER_ESCAPE);
  
   switch (event->xbutton.button) {
      case Button1:		// Left Button pressed
      {
		 if (control)     cli->leftButtonControlDown( pt);
		 else if (shift)  cli->leftButtonShiftDown( pt);
		 else if (meta)   cli->leftButtonMetaDown( pt);
		 else if (alt)    cli->leftButtonAltDown( pt);
		 else if (escape) cli->leftButtonEscapeDown( pt);
		 else             cli->leftButtonDown( pt);
		 break;
      }
      case Button2:		// Middle button pressed
      {
		 if (control)     cli->middleButtonControlDown( pt);
		 else if (shift)  cli->middleButtonShiftDown( pt);
		 else if (meta)   cli->middleButtonMetaDown( pt);
		 else if (alt)    cli->middleButtonAltDown( pt);
		 else if (escape) cli->middleButtonEscapeDown( pt);
		 else             cli->middleButtonDown( pt);
		 break;		
      }
      case Button3:		// Right button pressed
      {
		 if (control)     cli->rightButtonControlDown( pt);
		 else if (shift)  cli->rightButtonShiftDown( pt);
		 else if (meta)   cli->rightButtonMetaDown( pt);
		 else if (alt)    cli->rightButtonAltDown( pt);
		 else if (escape) cli->rightButtonEscapeDown( pt);
		 else             cli->rightButtonDown( pt);
		 break;
      }
      default:
      {
		 break;
      }
   } // CASE
}

void
XmwMouseInteractor::ButtonMotionHandler(
   Widget w, 
   XtPointer client, 
   XEvent * event, 
   Boolean*) 
{
   // Attempt to cache most of the motion events so we don't end up
   // updating every time the mouse moves a fraction of a pixel

   XEvent tmpEvent;
   while (XCheckWindowEvent(
	  XtDisplay(w), 
	  XtWindow(w), 
	  ButtonMotionMask,
	  &tmpEvent)) {
      if (tmpEvent.type == MotionNotify) event = &tmpEvent;
   }

   XmwMouseInteractor* cli = (XmwMouseInteractor*) client;

   XmwPoint pt(event->xbutton.x, event->xbutton.y);
 
   int control = cli->modifier(MODIFIER_CONTROL);
   int shift   = cli->modifier(MODIFIER_SHIFT);
   int meta    = cli->modifier(MODIFIER_META);
   int alt     = cli->modifier(MODIFIER_ALT);
   int escape  = cli->modifier(MODIFIER_ESCAPE);
   
   if (event->xmotion.state & Button1Mask) {
      if (control)     cli->leftButtonControlMotion( pt);
      else if (shift)  cli->leftButtonShiftMotion( pt);
      else if (meta)   cli->leftButtonMetaMotion( pt);
      else if (alt)    cli->leftButtonAltMotion( pt);
      else if (escape) cli->leftButtonEscapeMotion( pt);
      else             cli->leftButtonMotion( pt);
   }	
   else if (event->xmotion.state & Button2Mask) {
      if (control)     cli->middleButtonControlMotion( pt);
      else if (shift)  cli->middleButtonShiftMotion( pt);
      else if (meta)   cli->middleButtonMetaMotion( pt);
      else if (alt)    cli->middleButtonAltMotion( pt);
      else if (escape) cli->middleButtonEscapeMotion( pt);
      else             cli->middleButtonMotion( pt);
   }
   else if (event->xmotion.state & Button3Mask) {
      if (control)     cli->rightButtonControlMotion( pt);
      else if (shift)  cli->rightButtonShiftMotion( pt); 
      else if (meta)   cli->rightButtonMetaMotion( pt);
      else if (alt)    cli->rightButtonAltMotion( pt);
      else if (escape) cli->rightButtonEscapeMotion( pt);
      else             cli->rightButtonMotion( pt);
   }
}


void
XmwMouseInteractor::ButtonReleaseHandler(
   Widget , 
   XtPointer client,
   XEvent * event, 
   Boolean*)
{  
   XmwMouseInteractor* cli = (XmwMouseInteractor*) client;

   XmwPoint pt(event->xbutton.x, event->xbutton.y);
 
   // See which buttons are currently held down.

   int control = cli->modifier(MODIFIER_CONTROL);
   int shift   = cli->modifier(MODIFIER_SHIFT);
   int meta    = cli->modifier(MODIFIER_META);
   int alt     = cli->modifier(MODIFIER_ALT);
   int escape  = cli->modifier(MODIFIER_ESCAPE);
  
   switch (event->xbutton.button) {
      case Button1:			// Left button was pushed
		 if (control)     cli->leftButtonControlUp( pt);
		 else if (shift)  cli->leftButtonShiftUp( pt);
		 else if (meta)   cli->leftButtonMetaUp( pt);
		 else if (alt)    cli->leftButtonAltUp( pt);
		 else if (escape) cli->leftButtonEscapeUp( pt);
		 else             cli->leftButtonUp( pt);
		 break;
      case Button2:			// Middle button was pushed
		 if (control)     cli->middleButtonControlUp( pt);
		 else if (shift)  cli->middleButtonShiftUp( pt);
		 else if (meta)   cli->middleButtonMetaUp( pt);
		 else if (alt)    cli->middleButtonAltUp( pt);
		 else if (escape) cli->middleButtonEscapeUp( pt);
		 else             cli->middleButtonUp( pt);
		 break;
      case Button3:			// Right button was pushed
		 if (control)     cli->rightButtonControlUp( pt);
		 else if (shift)  cli->rightButtonShiftUp( pt);
		 else if (meta)   cli->rightButtonMetaUp( pt);
		 else if (alt)    cli->rightButtonAltUp( pt);
		 else if (escape) cli->rightButtonEscapeUp( pt);
		 else             cli->rightButtonUp( pt);
		 break;
      default:
		 break;
   } /* CASE */  
}


void      
XmwMouseInteractor::KeyPressCallback(
   Widget , 
   XtPointer client, 
   XEvent * event,
   Boolean * )
{
   // cast user data to be a mouse interactor.

   XmwMouseInteractor*   cli = (XmwMouseInteractor*) client;

   char buffer[20];
   int bufsize = 20;
   KeySym keysym;
   XComposeStatus compose;

   // Extract keypress from XEvent
   int charcount = XLookupString(
	  (XKeyEvent*)event, buffer, bufsize,
	  &keysym, &compose);

   switch (keysym) {
      case XK_Shift_L: 		// Either of the two shift keys pressed
      case XK_Shift_R:
      {
		 cli->setModifier(MODIFIER_SHIFT);
		 break;
      }
      case XK_Control_L:	// Control key pressed
      case XK_Control_R:
      {
		 cli->setModifier(MODIFIER_CONTROL);
		 break;
      }
      case XK_Meta_L:		// Either of the two meta keys pressed
      case XK_Meta_R:
      {
		 cli->setModifier(MODIFIER_META);
		 break;
      }
      case XK_Alt_L:		// Alt key pressed
      case XK_Alt_R:
      {
		 cli->setModifier(MODIFIER_ALT);
		 break;
      }
      case XK_Escape:		// Escape key pressed
      {
		 cli->setModifier(MODIFIER_ESCAPE);
		 break;
      }
      default:
      {
		 break;
      }
   } /* CASE */
}

 
void      
XmwMouseInteractor::KeyReleaseCallback(
   Widget,
   XtPointer client, 
   XEvent  * event,
   Boolean *)
{
   // cast user data to be a mouse
  
   XmwMouseInteractor* cli = (XmwMouseInteractor*) client; 

   char buffer[20];
   int bufsize = 20;
   KeySym keysym;
   XComposeStatus compose;

   // Extract key from XEvent.

   int charcount = XLookupString((XKeyEvent*)event, buffer, bufsize,
								 &keysym, &compose);

   switch (keysym) {
      case XK_Shift_L:	    	// Either of the two shift keys released
      case XK_Shift_R:
      {
		 cli->unsetModifier(MODIFIER_SHIFT);
		 break;
      }
      case XK_Control_L:		// Control key released
      case XK_Control_R:
      {
		 cli->unsetModifier(MODIFIER_CONTROL);
		 break;
      }
      case XK_Meta_L:			// Either of the two meta keys released
      case XK_Meta_R:
      {
		 cli->unsetModifier(MODIFIER_META);
		 break;
      }
      case XK_Alt_L:			// Alt key released
      case XK_Alt_R:
      {
		 cli->unsetModifier(MODIFIER_ALT);
		 break;
      }
      case XK_Escape:			// Escape key released
      {
		 cli->unsetModifier(MODIFIER_ESCAPE);
		 break;
      }
      default:
      {
		 break;
      }
   } // CASE
}
