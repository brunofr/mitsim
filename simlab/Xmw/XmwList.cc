//-*-c++-*------------------------------------------------------------
// XmwList.cc
//
// qiyang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------


#include "XmwList.h"

#include <Xmt/Xmt.h>
#include <Xm/List.h>
#include <stdlib.h>
#include <assert.h>				// compile with -DNDEBUG to cancel

XmwList::XmwList(Widget parent, const  String name,
				 ArgList args, Cardinal argc )
   : XmwUIWidget( name )
{
   nItems_  = 0;

   // use XtSetArg for resources that can only be set at creation
   // time.

   Arg ar[10];
   Cardinal n = 0;

   XtSetArg( ar[n], XmNlistSizePolicy, XmCONSTANT ); n++;

   // create the scrolled list from parent

   widget_ = XmCreateScrolledList(
	  parent,
	  name,
	  ar, n );

   installDestroyHandler();

   // set default resources.

   XtVaSetValues(
	  widget_,
	  XmNscrollBarDisplayPolicy, XmAS_NEEDED,
	  XmNscrollingPolicy,        XmAUTOMATIC,
	  XmNscrollBarPlacement,     XmBOTTOM_RIGHT,
	  XmNselectionPolicy,        XmBROWSE_SELECT,
	  XmNlistMarginWidth,        (Dimension) 2,
	  XmNlistMarginHeight,       (Dimension) 2,
	  NULL );

   // set resources coming from the resource file (which may override
   // previous default ones).

   XtSetValues( widget_, args, argc );

   // managing the scrolled list is done by the automatic widget
   // creation process.
}

const char * XmwList::className() const
{
   return "XmwList";
}


int
XmwList::visibleItems()
{
   int nbr = 0;
   XtVaGetValues(
	  widget_,
	  XmNvisibleItemCount, &nbr,
	  NULL );
   return nbr;
}


void
XmwList::setVisibleItems( int count )
{
   XtVaSetValues(
	  widget_,
	  XmNvisibleItemCount, count,
	  NULL );
}


void
XmwList::addItem( const char *label, int position )
{
   XmString str = XmtCreateXmString( label );
   XmListAddItem( widget_, str, position );
   ++ nItems_;
   XmStringFree( str );
}


void
XmwList::addItemsAndFreeStrs( XmString *xmstr, int n ) {

   XmListAddItems( widget_, xmstr, n, 0 );
   nItems_ += n;
   for ( int i = 0; i < n; ++ i ) {
	  XmStringFree( xmstr[i]);
   }
}


void
XmwList::deleteItem( const char *label )
{
   XmString str = XmtCreateXmString( label );
   XmListDeselectItem( widget_, str );
   XmListDeleteItem( widget_, str );
   nItems_ --;
   XmStringFree( str );
}


void
XmwList::deleteAllItems()
{

   XmListDeselectAllItems( widget_ );
   XmListDeleteAllItems( widget_ );
   nItems_  = 0;
}


void
XmwList::setBottomItem( const char *label, Boolean call )
{
   XmString str = XmtCreateXmString( label );
   XmListSetBottomItem( widget_, str );
   XmStringFree( str );
}


void
XmwList::setBottomPos( int pos )
{
   XmListSetBottomPos( widget_, pos );
}


void
XmwList::setTopPos( int pos )
{
   XmListSetPos( widget_, pos );
}


void
XmwList::replaceItem( int pos, const char *label, Boolean call )
{
   XmString list[1];

   list[0] = XmtCreateXmString( label );

   XmListReplaceItemsPos( widget_, list, 1, pos );
   selectItem( pos, call );

   XmStringFree( list[0] );
}


void
XmwList::selectItem( int pos, Boolean call )
{
   XmListSelectPos( widget_, pos, call );
}


void
XmwList::deselectItem( int pos )
{
   XmListDeselectPos( widget_, pos );
}


void
XmwList::deselectAllItems()
{
   XmListDeselectAllItems( widget_ );
}


int
XmwList::getSingleSelectedPos()
{
   int *posList;
   int count;

   Boolean status;

   status = XmListGetSelectedPos( widget_, &posList, &count );

   if ( status ) {
	  assert( count == 1 );
	  int pos = *posList;
	  free( posList );		// free position list.
	  return pos;
   }
   else {						// nothing is selected
	  return 0;
   }
}

// define the List automatic creator functions

CustomDefine(XmwList)
