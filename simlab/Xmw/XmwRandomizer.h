//-*-c++-*------------------------------------------------------------
// XmwRandomizer.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef XMW_RANDOMIZER_HEADER
#define XMW_RANDOMIZER_HEADER

#include <vector>
#include <Xmw/XmwDialogManager.h>

class XmwCheckerField;

class XmwRandomizer : public XmwDialogManager
{
public:

  XmwRandomizer(Widget parent, int nflds);
  ~XmwRandomizer();
  void post();				// virtual

private:

  void activate();
  void deactivate();

  // overload the virtual functions in base class

  void okay(Widget, XtPointer, XtPointer);
  void cancel(Widget, XtPointer, XtPointer);
  void help(Widget, XtPointer, XtPointer);

  static void setSeedCB(Widget, XtPointer, XtPointer);

private:

  std::vector<XmwCheckerField*> flds_;
};

#endif

