//-*-c++-*------------------------------------------------------------
// XmwImage.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class definition/implementation for the class
//--------------------------------------------------------------------

#include <string.h>

#include <Xmt/Xmt.h>
#include <Xmt/Pixmap.h>

#include <Tools/ToolKit.h>

#include "XmwImage.h"

XmwImage::XmwImage(
  const char *name,
  char *img_bits, char *img_mask,
  int width, int height)
  : name_(::Copy(name))
{
  XmtRegisterXbmData(
	  name,
	  img_bits, img_mask,
	  width, height,
	  0,0);
}


XmwImage::~XmwImage()
{
  delete [] name_;
}
