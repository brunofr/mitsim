//-*-c++-*------------------------------------------------------------
// XmwCheckerField.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef XMWCHECKERFIELD_HEADER
#define XMWCHECKERFIELD_HEADER


class XmwCheckerField
{
   public:

	  XmwCheckerField(Widget parent, const char *name, unsigned int mask);
	  ~XmwCheckerField() { }
	  void set(const char *value);
	  const char* get();
	  void check(Boolean cheched = True);
	  Boolean isChecked();
	  unsigned int mask() { return mask_; }

	  Widget checker() { return checker_; }
	  Widget input() { return input_; }

   private:

	  unsigned int mask_;
	  Widget checker_;
	  Widget input_;
};

#endif
