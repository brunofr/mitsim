//-*-c++-*------------------------------------------------------------
// XmwWidget.C
//
// This file contains the class implementation for the class
// XmwWidget, a abstract base class for Xmw classes.
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//--------------------------------------------------------------------

#include <stdlib.h>

#include <X11/Xlib.h>
#include <Xmt/Xmt.h>

#include "XmwWidget.h"

XmwWidget::~XmwWidget ()
{
   if (widget_) destroy (widget_);
}

void XmwWidget::manage (Widget w)
{
   if (!w) return;

   if (XtIsTopLevelShell(w)) XtPopup(w, XtGrabNone);
   else XtManageChild(w);
   XRaiseWindow(XtDisplay(w), XtWindow(w));
}

void XmwWidget::unmanage(Widget w)
{
   if (!w) return;

   if (XtIsTopLevelShell(w)) XtPopdown(w);
   else XtUnmanageChild(w);
}

void XmwWidget::destroy (Widget w)
{
   if (!w) return;
   unmanage(w);
   XtDestroyWidget(w);
}
