//-*-c++-*------------------------------------------------------------
// XmwDialogManager.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <iostream>
#include <cassert>

#include <Xmt/Xmt.h>
#include <Xmt/Create.h>
#include <Xmt/Chooser.h>

#include "XmwDialogManager.h"
#include "XmwInterface.h"
using namespace std;

XmwDialogManager::XmwDialogManager(Widget parent, String name,
								   XtResourceList resources,
								   Cardinal num_resources,
								   const char *okayTag,
								   const char *cancelTag,
								   const char *helpTag)
  : XmwWidget(name)
{
  if (theBaseInterface) {
	theBaseInterface->showBusyCursor();
  }

  widget_ = XmtBuildQueryDialog(parent, name, resources, num_resources,
								okayTag, &okay_,
								cancelTag, &cancel_,
								helpTag, &help_,
								NULL);
  assert(widget_);

  XtVaSetValues(widget_,
				XmNdialogStyle, XmDIALOG_MODELESS,
				XmNautoUnmanage, False,
				NULL);

  if (okay_) {
	XtAddCallback(okay_,
				  XmNactivateCallback, &XmwDialogManager::OkayCB,
				  this);
  }

  if (cancel_) {
	XtAddCallback(cancel_,
				  XmNactivateCallback, &XmwDialogManager::CancelCB,
				  this);
  }
 
  if (help_) {
	XtAddCallback(help_,
				  XmNactivateCallback, &XmwDialogManager::HelpCB,
				  this);
  }
}


void XmwDialogManager::activate(Widget w)
{
  if (XtIsSubclass(w, xmtChooserWidgetClass)) {
	int n;
	XtVaGetValues(w, XmtNnumItems, &n, NULL);
	for ( int i = 0; i < n; ++i ) {
	  XmtChooserSetSensitive( w, i, True);
	}
  }
  XtVaSetValues(w, XmNsensitive, True, NULL);
}

void XmwDialogManager::deactivate(Widget w)
{
  if (!XtIsSensitive(w)) return;
  if (XtIsSubclass(w, xmtChooserWidgetClass)) {
	int n;
	XtVaGetValues(w, XmtNnumItems, &n, NULL);
	for ( int i = 0; i < n; ++i ) {
	  XmtChooserSetSensitive( w, i, False);
	}
  }
  XtVaSetValues(w, XmNsensitive, False, NULL);
}

void XmwDialogManager::setSensitivity(int s) // virtual
{
  if (!okay_) return;

  if (s) {
	activate(okay_);
  } else {
	deactivate(okay_);
  }
}

// These are static callbacks

void XmwDialogManager::OkayCB(Widget w, XtPointer tag, XtPointer data)
{
  XmwDialogManager *obj = (XmwDialogManager *)tag;
  obj->okay(w, tag, data);
}

void XmwDialogManager::CancelCB(Widget w, XtPointer tag, XtPointer data)
{
  XmwDialogManager *obj = (XmwDialogManager *)tag;
  obj->cancel(w, tag, data);
}

void XmwDialogManager::HelpCB(Widget w, XtPointer tag, XtPointer data)
{
  XmwDialogManager *obj = (XmwDialogManager *)tag;
  obj->help(w, tag, data);
}


void XmwDialogManager::post()
{
  XmwWidget::manage(widget_);
  ready();
}

void XmwDialogManager::ready()
{
  if (!theBaseInterface) return;
  theBaseInterface->showDefaultCursor();
}

// These are virtual functions and should be overload in derived classes

void XmwDialogManager::okay(Widget, XtPointer, XtPointer)
{
  unmanage();
}

void XmwDialogManager::cancel(Widget, XtPointer, XtPointer)
{
  unmanage();
}

void XmwDialogManager::help(Widget, XtPointer, XtPointer)
{
  cout << "Help!" << endl;
}
