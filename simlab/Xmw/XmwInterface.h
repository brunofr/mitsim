//-*-c++-*------------------------------------------------------------
// XmwInterface.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class definition for the class XmwInterface
//--------------------------------------------------------------------

#ifndef XmwINTERFACE_HEADER
#define XmwINTERFACE_HEADER

#include <iostream>
#include <cstdarg>

#include <Xmt/Xmt.h>
#include <Xmt/MsgLine.h>

#include <UTL/bool.h>
#include "XmwWidget.h"
#include "XmwModeline.h"
//using namespace std;

// Deafult time in milliseconds for temp message.  Can be overide by
// resource MsgStayTime.

extern int MSG_STAY_TIME;

class XmwImage;
class XmwMenu;
class XmwSymbols;

class XmwInterface : public XmwWidget
{
public:

  XmwInterface(int *argc, char **argv, XmwImage *icon);
  virtual ~XmwInterface();

  static Widget toplevel() { return toplevel_; }

  virtual void create();
  virtual void setTitle(const char *title);
  virtual void manage();

  static String convertTime(double t);
  static double convertTime(String);

  void scheduleOneDummyEvent(int n);

  virtual int mainloop();		// forever

  virtual void updateButtons() { }
  virtual void pause (int /* others */) { }
  virtual void run (int /* others */) { }

  void showBusyCursor();
  void showDefaultCursor();

  // Static functions and callbacks

  static void displayWorkingBox(const char *title, const char *msg);
  static void destroyWorkingBox();

  // Display a tip for the widget at mouse pointer

  static void addTip(Widget w, String tag);
  static void addTips(WidgetList pw, String tags[], int n);

  static void showTipCB(Widget button, XtPointer clientData,
						XEvent *event, Boolean *);

  // Objects can be overide in derived class

  virtual XmwSymbols* createSymbols();
  XmwSymbols* symbols() { return symbols_; }

  XmwMenu* menu() { return menu_; }
  virtual XmwMenu* menu(Widget parent);

  XmwModeline* modeline() { return modeline_; }
  virtual XmwModeline* modeline(Widget parent);

  // Managing the message line

  Widget msgline() { return msgline_; }
  void msgPush();
  void msgPop();
  void msgSet(const char *msg);
  void msgClear(int when = XmtMsgLineNow);
  void msgTimedClear(int when = MSG_STAY_TIME) {  msgClear(when); }
  void msgShow(const char *msg, int when = XmtMsgLineOnAction);
  void msgPrintf(const char *fmt, ...);
  void msgAppend(const char *msg);

  static void widgetMissingBoilout(const char *item);
  static void inputFileNotFound(const char *file);
  static void inputFileError(const char *file, int line, const char *item);
  static void outputFileError(const char *file);
	  
  // Open a document page in external broswer.

  void openUrl( const char *marker=NULL, const char *node=NULL,
				const char *url=NULL);
  
  XtAppContext appContext() { return appContext_; }
  Display* display() { return display_; }

  // Processing all events before return.  No block if no event is
  // pending.  This function returns the number of events
  // processed.

  int handlePendingEvents();

  void toggle_zzz(bool is_on);

  // Processing all pending events or block n/100 seconds and wait
  // for the next event.  It calls handlePendingEvents().  This
  // function is used to put CPU to sleep for a short period if
  // there is nothing to do with this process.

  void waitNextEvent(int n);

  Cursor getCursor( unsigned int shape );

  static void makeKeyPressEvent(XtPointer fake_key, XtIntervalId *pid = 0);
  static void makeSnoozeEvent(XtPointer client = 0, XtIntervalId *pid = 0);
  static void makeFakeXEvent(XtPointer client = 0, XtIntervalId *pid = 0);

private:

  static void showTip(XtPointer w, XtIntervalId *);

  bool is_zzz_on() { return is_zzz_on_; }

protected:

  static Widget toplevel_;
  static Display *display_;
  static Widget workingShell_;
  static XmwImage *icon_;

  XtAppContext appContext_;

  XmwSymbols   *symbols_;
  XmwMenu      *menu_;
  XmwModeline  *modeline_;
  int msg_count_;

  Widget	msgline_;
  Widget	zzz_;
  bool is_zzz_on_;

  static Widget tipShell_;	// cached
  static String tipTag_;	// tip tag to be shown, NULL after popup

  const char *helpCmdPrefix_;
  const char *helpCmdSuffix_;
  const char *helpUrl_;
  const char *helpNode_;
};

extern XmwInterface *theBaseInterface;
inline XmwModeline *theModeline() {
  return theBaseInterface->modeline();
}

inline void XmwInterface::msgAppend(const char *msg)
{
  if (!msgline_) return;
  XmtMsgLineAppend(msgline_, msg);
}

#endif
