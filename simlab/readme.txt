This version of MITSIMLab is closed-loop (CL) with additional support for keep-guidance option (KG).

It is a patched version based on the CL version used by Daniel Florian.


Features:
* Large file support (break 2G limit).
* Utility function depends on absolute link time, not relative linktime. (by Rama)
* Support ClosedLoopGuidance field in the master file.
* Show the freeway bias when it is parsed from the parameter file
* [IMPORTANT!] The freeway bias is used in the same way as defined in the manual, i.e., freeway link time is divided by the bias.


The KG option is originally designed for the calibration of NY project. It
picks up the guidance without deleting it, so that we can use the guidance
file to provide updated linktime information. This is normally used together
with a high percentage (100% or slightly less) of guided drivers.


Building MITSIMLab:

$ make clean        # clean previous lib, obj, bin, and temp files
$ make depend_all
$ make              # compiles all "normal" binary: smc mitsim tms meso xsmc xmitsim xtms xmeso
$ make mitsim_kg    # compiles the mitsim with KG options
$ make xmitsim_kg   # compiles the xmitsim with KG options
$ make install      # installs normal binaries to ~/bin/$(OSTYPE)
$ make install_kg   # installs the KG binaries: mitsim_kg xmitsim_kg

Note: general.tmpl.KeepGuidance, TS/Makefile.KeepGuidance, and
Tools/Makefile.KeepGuidance are created for KG option (define macro
MITSIM_KEEP_GUIDANCE). Some other files are also modified accordingly,
including Makefile, hosts.impl, general.impl, and haydn208.mit.edu.tmpl.



Yang Wen
4/7/2006
