//-*-c++-*------------------------------------------------------------
// NAME: Functions for communication to MITSIM and TMS
// AUTH: Qi Yang
// FILE: MESO_Communicator.C
// DATE: Mon Feb 26 15:35:54 1996
//--------------------------------------------------------------------

#include <sys/param.h>
#include <iostream>
using namespace std;

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>
#include <Tools/Math.h>

#include <GRN/RN_Route.h>
#include <GRN/OD_Table.h>
#include <GRN/OD_Cell.h>
#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>

#include "MESO_Communicator.h"
#include "MESO_Engine.h"
#include "MESO_Network.h"
#include "MESO_Segment.h"
#include "MESO_Parameter.h"
#include "MESO_Exception.h"

#ifdef INTERNAL_GUI
#include <DRN/DRN_DrawingArea.h>
#include "MESO_Interface.h"
#include "MESO_Menu.h"
#endif


MESO_Communicator *theCommunicator = NULL;

MESO_Communicator::MESO_Communicator(int sndtag)
   : Communicator(sndtag),
	 odFile_(NULL)
{
}

double
MESO_Communicator::batchStepSize()
{
   return theEngine->batchStepSize_;
}


// Make connection to its parent process (Simlab) and processes for
// other sibling modules.

void MESO_Communicator::makeFriends()
{
   if (!simlab_.isConnected()) return;
  
   simlab_ << MSG_TIMING_DATA
		   << ToolKit::workdir();
   simlab_.flush(SEND_IMMEDIATELY);

   tms_.init("TMS", simlab_.sndtag(), MSG_TAG_TMS);

   if (ToolKit::verbose()) {
      cout << theExec << " waiting process ids from SIMLAB ..." << endl;
   }
}


// Response to the messages received from other processes

int
MESO_Communicator::receiveMessages(double sec)
{
   int msg, flag = 0;

   msg = receiveSimlabMessages(sec);
   if (msg < 0) {
      theEngine->quit(STATE_ERROR_QUIT);
      return -1;
   } else if (msg > 0) {
      flag |= 1;
   }
   
   msg = receiveTmsMessages(sec);
   if (msg < 0) {
      theEngine->quit(STATE_ERROR_QUIT);
      return -1;
   } else if (msg > 0) {
      flag |= 4;
   }

   return flag;
}


// Response to the messages received from other processes

int
MESO_Communicator::receiveSimlabMessages(double sec)
{
   if (!simlab_.isConnected()) return 0;

   // Limited blocking receive (wait upto sec seconds)

   int flag = simlab_.receive(sec);
   if (flag <= 0) return flag;

   unsigned int type;
   simlab_ >> type;

   switch (type) {

	  case MSG_PROCESS_IDS:
		 int id;
		 simlab_ >> id;
		 if (id > 0) {
			tms_.connect(id);
			tms_ << MSG_TIMING_DATA
				 << theGuidedRoute->infoPeriodLength();
			tms_.flush(SEND_IMMEDIATELY);
		 }
		 if (ToolKit::verbose()) {
			cout << theExec << " received process ids from SIMLAB." << endl;
		 }
		 break;

      case MSG_CURRENT_TIME:
		 double now;
		 simlab_ >> now;
		 theSimulationClock->masterTime(now);
		 break;

#ifdef INTERNAL_GUI
      case MSG_PAUSE:
		 theInterface->pause(0);
		 break;

      case MSG_RESUME:
		 theInterface->run(0);
		 break;

      case MSG_WINDOW_SHOW:
		 theInterface->manage();
		 break;

      case MSG_WINDOW_HIDE:
		 theInterface->unmanage();
		 break;
#else
      case MSG_PAUSE:
      case MSG_RESUME:
      case MSG_WINDOW_SHOW:
      case MSG_WINDOW_HIDE:
		 break;			// not used in batch mode
#endif

      case MSG_QUIT:
		 theEngine->state(STATE_QUIT);
		 break;

      case MSG_DONE:
		 theEngine->state(STATE_DONE);
		 break;

      default:
		 cerr << "Warning:: Message type <"
			  << type << "> received from "
			  << simlab_.name() << " not supported." << endl;
		 break;
   }
   return flag;
}


// This function checks and receives messages from TMS

int
MESO_Communicator::receiveTmsMessages(double sec)
{
   if (!tms_.isConnected()) return 0;

   // Limited blocking receive (wait upto sec seconds)

   int flag = tms_.receive(sec);
   if (flag <= 0) return flag;

   int id;
   tms_ >> id;

   switch (id) {
      case MSG_MESO_START:
      {
		 tms_ >> theEngine->iteration_
			  >> theEngine->rollingLength_;
		 theEngine->restart();
		 break;
      }
	  case MSG_MDI_DONE:
	  {
		 char filename[MAXPATHLEN+1];
		 tms_ >> filename
			  >> theEngine->iteration_
			  >> theEngine->rollingLength_;
		 ::Copy(&odFile_, filename);
		 theEngine->restart();
		 break;
	  }
      default:
      {
		 cerr << "Warning:: Unknown TMS message <" << id
			  << "> received." << endl;
		 break;
      }
   }
   
   return flag;
}


void
MESO_Communicator::sendNextTimeToSimlab()
{
   if (!simlab_.isConnected()) return;

   SimulationClock *sc = theSimulationClock;
   
   simlab_ << MSG_END_CYCLE
		   << sc->currentTime()
		   << sc->lastStepCpuUsage();
   simlab_.flush(SEND_IMMEDIATELY);
}
