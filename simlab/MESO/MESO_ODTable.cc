//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_ODTable.C
// DATE: Mon Feb 26 17:38:54 1996
//--------------------------------------------------------------------

#include <GRN/RN_Link.h>

#include "MESO_ODTable.h"
#include "MESO_ODCell.h"

OD_Cell* 
MESO_ODTable::newOD_Cell()
{
   return new MESO_ODCell;
}
