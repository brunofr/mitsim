//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_FileManager.h
// DATE: Mon Feb 26 16:21:44 1996
//--------------------------------------------------------------------

#ifndef MESO_FILEMANAGER_HEADER
#define MESO_FILEMANAGER_HEADER

#include <fstream>
#include <Tools/GenericSwitcher.h>

class GenericVariable;

class MESO_FileManager : public GenericSwitcher
{
  friend class MESO_Engine;
#ifdef INTERNAL_GUI
  friend class MESO_OutputDialog;
#endif

public:

  MESO_FileManager();
  ~MESO_FileManager() { }

  void set(char **ptr, const char *s);

  char* title() { return title_; }
  void title(const char *s) { set(&title_, s); }

  void readMasterFile();

  // This overload the function in base class and will be classed
  // by the parser

  int parseVariable(GenericVariable &gv);

  char* depRecordFile() { return depRecordFile_; }
  char* vehicleFile() { return vehicleFile_; }
  char* stateDumpFile() { return stateDumpFile_; }
  char* state3d() { return state3d_; }

  void openOutputFiles();
  void closeOutputFiles();

  std::ofstream& osVehicle() { return osVehicle_; }
  std::ofstream& osPathRecord() { return osPathRecord_; }

  void linkFlowTravelTimesFile(const char *s) {
	set(&linkFlowTravelTimesFile_, s);
  }
  void linkTravelTimesFile(const char *s) {
	set(&linkTravelTimesFile_, s);
  }
  void depRecordFile(const char *s) { set(&depRecordFile_, s); }
  void vehicleFile(const char *s) { set(&vehicleFile_, s); }
  void pathRecordFile(const char *s) { set(&pathRecordFile_, s); }
  void state3d(const char *s) { set(&state3d_, s); }

private:

  char* title_;
  char* vehicleFile_;
  char* linkFlowTravelTimesFile_;
  char* linkTravelTimesFile_;
  char* state3d_;
  char* pathRecordFile_;
  char* stateDumpFile_;
  char* depRecordFile_;

  std::ofstream osVehicle_;	// vehicle log file
  std::ofstream osPathRecord_;	// entry and travel time on links

  int parseVariable(char ** varptr, GenericVariable &gv);

  int isEqual(const char *s1, const char *s2);
};

extern MESO_FileManager * theFileManager;

#endif
