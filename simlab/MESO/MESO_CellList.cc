//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_CellList.C
// DATE: Tue Feb 27 11:38:29 1996
// NOTE: This list is implemented as a stack
//--------------------------------------------------------------------

#include <Tools/ToolKit.h>

#include "MESO_CellList.h"
#include "MESO_Status.h"
#include "MESO_TrafficCell.h"

MESO_CellList * theCellList = new MESO_CellList;

MESO_CellList::MESO_CellList()
   :	head_(NULL),
		tail_(NULL),
		nCells_(0),
		nPeakCells_(0)
{
}

MESO_CellList::~MESO_CellList()
{
   while (head_) {
      tail_ = head_;
      head_ = head_->trailing_;
      delete tail_;
   }
   tail_ = NULL;
   nPeakCells_ = 0;
   nCells_ = 0;
}


/*
 * add a traffic cell at the beginning of the cell list (stack).
 */

void
MESO_CellList::recycle(MESO_TrafficCell *cell)
{
   cell->clean();
   cell->trailing_ = head_;

   if (head_) {					// at least one in the list
      head_->leading_ = cell;
   } else {						// no cell in the list
      tail_ = cell;
   }
   head_ = cell;
   nCells_ ++;					// one cell deposited in this list
   theStatus->nCells(-1);		// one cell become inactive
}


/*
 * Remove a vehicle from the head of the vehicle list.
 */

MESO_TrafficCell*
MESO_CellList::recycle()
{
   MESO_TrafficCell* cell;

   if (head_) {					// get head from the list
      cell = head_;
      if (tail_ == head_) {		// the onle one cell in list
		 head_ = tail_ = NULL;
      } else {					// at least two cells in list
		 head_ = head_->trailing_;
		 head_->leading_ = NULL;
      }
      nCells_ --;
   } else {						// list is empty
      cell = new MESO_TrafficCell; // create a new cell
      nPeakCells_ ++;
   }

   theStatus->nCells(1);		// one cell become active
   return cell;
}
