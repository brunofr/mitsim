//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_Parameter.h
// DATE: Mon Feb 12 09:26:00 1996
//--------------------------------------------------------------------

#ifndef MESOMESO_PARAMETER_HEADER
#define MESOMESO_PARAMETER_HEADER

#include <GRN/Parameter.h>
#ifdef INTERNAL_GUI
#include <Xmw/XmwColor.h>
#endif

class MESO_Parameter : public Parameter
{
   public:

      MESO_Parameter();
      ~MESO_Parameter() { }
    
      void load();

      // Vehicle classes

      short int nVehicleClasses() { return nVehicleClasses_; }
      float * vehicleClassCDF() { return vehicleClassCDF_; }
      float vehicleClassCDF(short int i) {
		 return vehicleClassCDF_[i];
      }
      float hovRate() { return hovRate_; }
      float etcRate() { return etcRate_; }
      float vehicleLength(short int i);
      char *vehicleName(int i) { return vehicleName_[i]; }

      float minGap(float speed);
	  float maxSpeed(float gap, int nlanes);

	  inline float cellSplitGap() { return cellSplitGap_; }
      inline float rspLower() { return rspLower_; }
      inline float rspUpper() { return rspUpper_; }
      inline float rspRange() {
		 return rspUpper_ - rspLower_;
      }
      inline float channelizeDistance() {
		 return channelizeDistance_;
      }

	  inline float minHeadwayGap() { return limitingParam_[0]; }
	  inline float headwaySpeedSlope() { return limitingParam_[1]; }
      inline float minSpeed() { return limitingParam_[2]; }

	  float queueReleasingSpeed(float t, float v_f);

#ifdef INTERNAL_GUI
      Pixel vehicleColors(int i) { return vehicleColors_[i]; }
#endif

   private:

      int parseVariable(GenericVariable &);
      int loadVehicleClassParas(GenericVariable &);
	  int loadLimitingParameters(GenericVariable &);
	  int loadQueueDispatchParameters(GenericVariable &);

   protected:

      short int nVehicleClasses_;
      float * vehicleClassCDF_;
      char **vehicleName_;

      float hovRate_;
      float etcRate_;
      float *vehicleLength_;	// meter

      float *limitingParam_;	// min headway, headway/speed slope,
								// max acc, min speed, etc.

	  float *queueParam_;		// max speed for queue releasing

	  float cellSplitGap_;		// gap threshold for split a cell
      float rspLower_;
      float rspUpper_;
      float channelizeDistance_; // from dnNode (meter)

#ifdef INTERNAL_GUI
      Pixel *vehicleColors_;
#endif

};

extern MESO_Parameter * theParameter;

#endif
