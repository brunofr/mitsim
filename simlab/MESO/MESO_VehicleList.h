//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_VehicleList.h
// DATE: Tue Feb 27 11:37:18 1996
//--------------------------------------------------------------------

#ifndef MESO_VEHICLELIST_HEADER
#define MESO_VEHICLELIST_HEADER

class MESO_Vehicle;

class MESO_VehicleList
{
  public:

	MESO_VehicleList();
	~MESO_VehicleList();

	void recycle(MESO_Vehicle *); /* put a vehicle into the list */
	MESO_Vehicle* recycle(); /* get a vehicle from the list */

	inline MESO_Vehicle* head() { return head_; }
	inline MESO_Vehicle* tail() { return tail_; }
	inline int nVehicles() { return nVehicles_; }
	inline int nPeakVehicles() { return nPeakVehicles_; }

  private:

	MESO_Vehicle *head_;
	MESO_Vehicle *tail_;
	int nVehicles_;		/* number of vehicles */
	int nPeakVehicles_;	/* max number of vehicles */
};

extern MESO_VehicleList *theVehicleList; /* recyclable vehicles */

#endif

