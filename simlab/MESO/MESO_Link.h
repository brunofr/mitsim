//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_Link.h
// DATE: Mon Feb 26 16:36:10 1996
//--------------------------------------------------------------------

#ifndef MESO_LINK_HEADER
#define MESO_LINK_HEADER

#include <iostream>
#include <GRN/RN_Link.h>

class MESO_Vehicle;
class MESO_TrafficCell;

class MESO_Link : public RN_Link
{
      friend class MESO_Network;

   public:

      MESO_Link();
      ~MESO_Link() { clean(); }

      inline MESO_Vehicle* queueHead() { return queueHead_; }
      inline MESO_Vehicle* queueTail() { return queueTail_; }
      inline int queueLength() { return queueLength_; }

      MESO_TrafficCell * firstCell();
      MESO_TrafficCell * lastCell();
	
      int reportQueueLength(std::ostream &os = std::cout);

      // These two maintains the virtual queue before entering the
      // network
      
      void dequeue(MESO_Vehicle *);
      void queue(MESO_Vehicle *);
      void prequeue(MESO_Vehicle *);

      // These are used in moving vehicles

      void advanceVehicles();

      void append(MESO_Vehicle *);

      void checkConnectivity();

      int isJammed();

      void clean();

      float calcTravelTime(); // virtual

   protected:

      // These maintains the vehicles in virtual queue (before
      // departure, at the upstream end of the link)

      MESO_Vehicle *queueHead_;	// first vehicle in the queue
      MESO_Vehicle *queueTail_;	// last vehicle in the queue
      int queueLength_;			// number of vehicle in the queue
};

#endif
