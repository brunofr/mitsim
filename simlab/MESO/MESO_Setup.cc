//-*-c++-*------------------------------------------------------------
// NAME: Microcscopic Traffic Simulator
// AUTH: Qi Yang
// FILE: MESO_Setup.C
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <new>
#include <cstdio>
#include <cstring>
using namespace std;

#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <Tools/SimulationClock.h>
#include <Tools/Random.h>

#include "MESO_Setup.h"
#include "MESO_Network.h"
#include "MESO_Parameter.h"
#include "MESO_Engine.h"
#include "MESO_Status.h"
#include "MESO_FileManager.h"
#include "MESO_ODTable.h"
#include "MESO_ODCell.h"
#include "MESO_VehicleTable.h"
#include "MESO_Link.h"
#include "MESO_Communicator.h"
#include "MESO_CmdArgsParser.h"
#include "MESO_Exception.h"

#include <GRN/RN_Parser.h>
#include <GRN/RN_PathParser.h>
#include <GRN/RN_PathTable.h>
#include <GRN/RN_DynamicRoute.h>

void 
ParseParameters()
{
   if (theParameter) {
      delete (MESO_Parameter *)theParameter;
   }
   theParameter = new MESO_Parameter;
   theParameter->load();
}


// --------------------------------------------------------------------
// Read the network database. This include network objects (nodes,
// link labels, links, segments, lanes), traffic control objects
// (traffic signals, message signs, toll booths, etc.) and
// surveillance objects (sensors).
// --------------------------------------------------------------------

void ParseNetwork()
{
   if (theNetwork) {
      if (ToolKit::verbose()) {
		 cout << "Unloading <" << theNetwork->name()
			  << ">" << endl;
      }
      delete (MESO_Network *)theNetwork;
   }
   theNetwork = new MESO_Network;
   const char *filename = ToolKit::optionalInputFile(theNetwork->name());
   RN_Parser rn_parser(theNetwork);
   rn_parser.parse(filename);

#ifdef INTERNAL_GUI
   ((MESO_Network*)theNetwork)->calcGeometricData();
#endif   

   if (ToolKit::verbose()) {
      theNetwork->printBasicInfo();
   }
}


// Parse path table 

void ParsePathTables()
{
   if (thePathTable) {
      delete thePathTable;
      thePathTable = NULL;
   }
   if (ToolKit::isValidInputFilename(RN_PathTable::name())) {
      thePathTable = new RN_PathTable;
      const char *filename = ToolKit::optionalInputFile(RN_PathTable::name());
      if (!filename) theException->exit(1);
      RN_PathParser pt_parser(thePathTable);
      pt_parser.parse(filename);
      thePathTable->setPathPointers();
	  theNetwork->calcPathCommonalityFactors();
   } else if (ToolKit::verbose()) {
      cout << "No path tables. "
		   << "Route choice model will be used for all drivers." << endl;
   }
}


// Read OD Trip tables

void ParseODTripTables()
{
   if (theODTable) {
      delete (MESO_ODTable *)theODTable;
      theODTable = NULL;
   }

   MESO_Communicator *c = (MESO_Communicator*)theCommunicator; 
   if (c->odFile()) {
	  theODTable = new MESO_ODTable;
	  theODTable->open(c->odFile());
   } else if (ToolKit::isValidInputFilename(MESO_ODTable::name())) {
      theODTable = new MESO_ODTable;
	  theODTable->open();
   } else if (ToolKit::verbose()) {
      cout << "No trip tables." << endl;
   }
}


// Read vehicle tables

void ParseVehicleTables(double start_time)
{
   if (theVehicleTable) {
      delete (MESO_VehicleTable *)theVehicleTable;
      theVehicleTable = NULL;
   }
   if (ToolKit::isValidInputFilename(VehicleTable::name())) {
      theVehicleTable = new MESO_VehicleTable;
      theVehicleTable->open(start_time);
   } else if (ToolKit::verbose()) {
      cout << "No vehicle tables." << endl;
   }
}

// Setup some other stuffs. 

void SetupMiscellaneous()
{
   // Assign the pointers to control and surveillance devices in
   // MESO_Segment objects.

   theNetwork->assignCtrlListInSegments();
   theNetwork->assignSurvListInSegments();
}
