//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_Segment.C
// DATE: Mon Feb 26 17:50:25 1996
//--------------------------------------------------------------------

#include <Tools/Math.h>

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>

#include <GRN/RN_SdFn.h>

#include "MESO_Network.h"
#include "MESO_Node.h"
#include "MESO_Link.h"
#include "MESO_Segment.h"
#include "MESO_Lane.h"
#include "MESO_Parameter.h"
#include "MESO_Engine.h"
#include "MESO_Vehicle.h"
#include "MESO_CellList.h"
#include "MESO_ODCell.h"
#include "MESO_TrafficCell.h"

#ifdef INTERNAL_GUI
#include <DRN/DRN_DrawingArea.h>
#include <DRN/DRN_ValueDialog.h>
#include "MESO_Interface.h"
#include "MESO_Symbols.h"
#include "MESO_Menu.h"
#include "MESO_DrawingArea.h"
#include "MESO_DrawableVehicle.h"
#endif
#include <iostream>
using namespace std;


MESO_Segment::MESO_Segment() :
  DRN_Segment(),
  nCells_(0),
  firstCell_(NULL),
  lastCell_(NULL)
{
}


MESO_Segment::~MESO_Segment()
{
  clean();
}


void
MESO_Segment::clean()
{
  // Remove cells in the segment

  while (firstCell_) {
	lastCell_ = firstCell_;
	firstCell_ = firstCell_->trailing_;
	theCellList->recycle(lastCell_);
  }
  lastCell_ = NULL;
  nCells_ = 0;
}


// This returns the maximum speed for a given distance gap

float
MESO_Segment::maxSpeed(float gap)
{
  return theParameter->maxSpeed(gap, nLanes_);
}


// Update number of vehicle allowed to move in and out. Called once in
// every update phase

void
MESO_Segment::addCapacity(float cap)
{
  setCapacity(cap + capacity_);
}

// This function is called once at the beginning and updated only when
// capacity changes (e.g. incident or interactively)

void
MESO_Segment::setCapacity(float cap)
{
  float maxcap = defaultCapacity(); 
  if (cap < 0.0) {
	capacity_ = 0.0;
  } else if (cap > maxcap) {
	capacity_ = maxcap;
  } else {
	capacity_ = cap;
  }
  resetEmitTime();
}


MESO_Vehicle *
MESO_Segment::firstVehicle()
{
  if (firstCell_) return firstCell_->firstVehicle();
  else return NULL;
}


MESO_Vehicle *
MESO_Segment::lastVehicle()
{
  if (lastCell_) return lastCell_->lastVehicle();
  else return NULL;
}


// Overload the base class function

void
MESO_Segment::calcStaticInfo()
{
#ifdef INTERNAL_GUI
  DRN_Segment::calcStaticInfo();
#else
  RN_Segment::calcStaticInfo();
#endif

  density_ = 0.0;
  speed_ = maxSpeed();

  // Set to the default maximum capacity

  setCapacity(defaultCapacity());
}


float
MESO_Segment::defaultCapacity()
{
  float vph = theNetwork->sdFn(sdIndex())->capacity();
  return nLanes_ * vph;
}


/*
 * FInd the number of vehicle currently in the segment
 */

int
MESO_Segment::nVehicles()
{
  int num = 0;
  MESO_TrafficCell *cell = firstCell_;
  while (cell) {
	num += cell->nVehicles();
	cell = cell->trailing();
  }
  return num;
}


/*
 *------------------------------------------------------------------
 * Calculate the density of a segment, in vehicle/kilometer
 *------------------------------------------------------------------
 */

float
MESO_Segment::calcDensity(void)
{
  density_ = 1000.0 * nVehicles() / (length_ * nLanes());
  return (density_);
}


/*
 *-------------------------------------------------------------------
 * Calculate the harmonic mean speed
 *-------------------------------------------------------------------
 */

float
MESO_Segment::calcSpeed(void)
{
  if (nVehicles() <= 0) {
	return (speed_ = maxSpeed());
  }
  float sum = 0.0;
  MESO_TrafficCell *cell = firstCell_;
  MESO_Vehicle *pv;
  while (cell) {
	pv = cell->firstVehicle();
	while (pv) {
	  if (pv->currentSpeed() > SPEED_EPSILON) {
		sum += 1.0 / pv->currentSpeed();
	  } else {
		sum += 1.0 / SPEED_EPSILON;
	  }
	  pv = pv->trailing();
	}
	cell = cell->trailing();
  }
  return (speed_ = nVehicles() / sum);
}


int
MESO_Segment::calcFlow()
{
  float x = 3.6 * speed_ * density_;
  return (int) (x + 0.5);
}


int MESO_Segment::flow()
{
  return calcFlow();
}

float MESO_Segment::speed()
{
  return speed_/theParameter->speedFactor();
}

float MESO_Segment::density()
{
  return density_/theParameter->densityFactor();
}


void
MESO_Segment::append(MESO_TrafficCell *cell)
{
  cell->segment_ = this;

  cell->leading_ = lastCell_;
  cell->trailing_ = NULL;
  
  if (lastCell_) {		// append at end
	lastCell_->trailing_ = cell;
  } else {			// queue is empty
	firstCell_ = cell;
  }
  lastCell_ = cell;
  nCells_ ++;
}


void
MESO_Segment::remove(MESO_TrafficCell *cell)
{
  if (cell->leading_) {	// not the first one
	cell->leading_->trailing_ = cell->trailing_;
  } else {			// first one
	firstCell_ = cell->trailing_;
  }
  if (cell->trailing_) {	// not the last one
	cell->trailing_->leading_ = cell->leading_;
  } else {			// last one
	lastCell_ = cell->leading_;
  }
  nCells_ --;
}


/*
 * Returns 1 if this link can NOT accept more vehicles at the upstream
 * end
 */

int MESO_Segment::isJammed()
{
  if (!lastCell_) {	// nobody in the segment
	return 0;
  } else {
	return lastCell_->isJammed();
  }
}


/*
 * Calculate cell variables that do NOT depend on other cells. This
 * functions is called by a function with the same name in class
 * MESO_Network.
 */

void
MESO_Segment::calcTrafficCellUpSpeed()
{
  // Calculate density and upSpeed for each traffic cell

  // cout << " XXXX Segment" ;
  MESO_TrafficCell *cell = firstCell_;
  while (cell) {
	cell->updateTailSpeed();
	cell = cell->trailing();
  }
}


/*
 * Calculate cell variables that depend on other cells. This functions
 * is called by a function with the same name in class MESO_Network.
 */

void
MESO_Segment::calcTrafficCellDnSpeeds()
{
  MESO_TrafficCell *cell = firstCell_;
  while (cell) {
	cell->updateHeadSpeeds();
	cell = cell->trailing();
  }
}


/*
 * Move vehicles based on current cell speeds. This function is called
 * by MESO_Link::advanceVehicles() in a downstream first order.
 */

void
MESO_Segment::advanceVehicles()
{
  MESO_TrafficCell *cell = firstCell_;
  while (cell) {
	cell->advanceVehicles();
	cell = cell->trailing_;
  }
}


/*
 * Organizes the cells in the segment.  Called at the end of the
 * each advance phase
 */

void
MESO_Segment::formatTrafficCells()
{
  MESO_TrafficCell *cell;
  MESO_TrafficCell *front;

  // Remove the empty traffic cells

  cell = firstCell_;
  while (front = cell) {
	cell = cell->trailing_;
	if (front->nVehicles() <= 0 ||
		!front->firstVehicle() ||
		!front->lastVehicle()) { // no vehicle left

	  // use vehicle count should be enough but it seems that there
	  // is a bug somewhere that causes vehicle count to be 1 when
	  // actually there is no vehicle left.

	  remove(front);
	  theCellList->recycle(front);
	}
  }

  // Combine the cells if their gap distance is less than a given
  // threshold

  cell = firstCell_;
  while ((front = cell) &&
		 (cell = cell->trailing_)) {
	if (cell->firstVehicle()->distance() <
		front->lastVehicle()->upPos() + 
		theParameter->rspLower()) {
	  front->append(cell);
	  remove(cell);
	  theCellList->recycle(cell);
	  cell = front;
	}
  }


  // Split a cell into one or more cells if the distance between
  // vehicle are greater than the given threshold

#ifdef SPLIT_TRAFFIC_CELLS		// not used in current version
  cell = firstCell_;
  while (cell) {
	cell->split();
	cell = cell->trailing_;
  }
#endif

}


// Insert a new vehicle into the segment based on its distance
// position

void
MESO_Segment::insert(MESO_Vehicle* vehicle)
{
  if (!lastCell_) {
	append(theCellList->recycle());
	lastCell_->initialize();
  }
  lastCell_->insert(vehicle);
}

/*
 * Add a vehicle at the upstream end of the segment
 */

void
MESO_Segment::append(MESO_Vehicle* vehicle)
{
  if (!lastCell_ ||
	  !lastCell_->isReachable()) {
	append(theCellList->recycle());
	lastCell_->initialize();
  }

  // Put the vehicle in the traffic cell

  lastCell_->append(vehicle);
}

// Called by RN_Segment::print()

void
MESO_Segment::printSdIndex(ostream &os)	// virtual
{
  if (sdIndex_ > 0) {
	os << endc << (sdIndex_ + 1);
  } else {

	// THIS IS A TEMPORARY FIX TO GET THE SD INDEX FOR MY AMSTERDAM
	// NETWORK. IT SHOULD BE REMOVED IN THE FINAL VERSION.

	if (link_->isInTunnel()) {
	  sdIndex_ = 3;
	} else {
	  switch (link_->linkType()) {
	  case LINK_TYPE_URBAN:
		{
		  sdIndex_ = 2;
		  os << endc << 3;
		  break;
		}
	  case LINK_TYPE_RAMP:
		{
		  sdIndex_ = 1;
		  os << endc << 2;
		  break;
		}
	  case LINK_TYPE_FREEWAY:
	  default:
		{
		  sdIndex_ = 0;
		  break;
		}
	  }
	}
  }
}


#ifdef INTERNAL_GUI

void
MESO_Segment::changeCapacity()
{
  static char *title = NULL;
   
  if (title) StrFree(title);

  title = StrCopy("Capacity for Segment %d:%d (vph)", link_->code(), code_);

  float maxvalue = defaultCapacity();
  const float scale = 3600.0;
  DRN_ValueDialog::getValue(theDrawingArea->widget(),
							capacity_,
							0.0, maxvalue,
							scale, 0,
							title, title);
}


// Find the vehicle by click middle mouse button and show its info

void MESO_Segment::queryVehicle(WcsPoint& pressed)
{
  MESO_Symbols *sym = theSymbols();
  if (!sym->viewType().is(DRN_Symbols::VIEW_MICRO) ||
	  theDrawingArea->resolution() < DRN_DrawingArea::LOW_RESOLUTION ||
	  !sym->vehicleBorderCode().value()) {
	return;
  }
  double offset2 = DBL_INF, dis2, pos;
  MESO_TrafficCell *cell = firstCell_;
  MESO_Vehicle *pv;
  MESO_Vehicle *closest = NULL;
  while (cell) {
	pv = cell->firstVehicle();
	while (pv) {
	  if (((MESO_DrawableVehicle *)pv)->isVisible()) {
		pos = (pv->dnPos() + 0.5 * pv->length()) / length();
		dis2 = pressed.distance_squared(centerPoint(pos));
		if (dis2 < offset2) {
		  closest = pv;
		  offset2 = dis2;
		}
	  }
	  pv = pv->trailing();
	}
	cell = cell->trailing();
  }

  if (closest == NULL || sqrt(offset2) > closest->length()) {
	theInterface->msgShow("No vehicle found");
	return;
  }

  ((MESO_DrawableVehicle *)closest)->query();
}

#endif
