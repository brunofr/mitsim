//-*-c++-*------------------------------------------------------------
// NAME: Traffic Management Simulator
// AUTH: Qi Yang
// FILE: MESO_TollBooth.h
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#ifndef MESO_TOLLBOOTH_HEADER
#define MESO_TOLLBOOTH_HEADER

#include <GRN/RN_TollBooth.h>
#include "MESO_Signal.h"

class MESO_TollBooth : public RN_TollBooth, public MESO_Signal
{
   public:

      MESO_TollBooth();
      ~MESO_TollBooth() { }
};

#endif

