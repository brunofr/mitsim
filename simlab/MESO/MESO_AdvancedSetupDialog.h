//-*-c++-*------------------------------------------------------------
// MESO_AdvancedSetupDialog.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI
#ifndef MESO_ADVANCEDSETUPDIALOG_HEADER
#define MESO_ADVANCEDSETUPDIALOG_HEADER

#include <Templates/Pointer.h>
#include <Xmw/XmwDialogManager.h>
#include <Xmw/XmwCallback.h>

class MESO_AdvancedSetupDialog : public XmwDialogManager
{
	  CallbackDeclare(MESO_AdvancedSetupDialog);

   public:

	  MESO_AdvancedSetupDialog(Widget parent);
	  ~MESO_AdvancedSetupDialog() { }
	  void post();				// virtual

   private:

	  void set(char **ptr, const char *s);

	  void activate();
	  void deactivate();

	  // overload the virtual functions in base class

	  void okay(Widget, XtPointer, XtPointer);
	  void cancel(Widget, XtPointer, XtPointer);
	  void help(Widget, XtPointer, XtPointer);

   private:

	  Widget linktimeFile0_;
	  Widget linktimeFile1_;
	  Widget availability_;		// radio box chooser
	  Widget spFlags_;			// check box chooser
};

#endif
#endif // INTERNAL_GUI
