//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// NOTE: Parse vehicles from a file
// AUTH: Qi Yang
// FILE: MESO_VehicleTable.h
// DATE: Tue Mar  5 23:09:12 1996
//--------------------------------------------------------------------

#ifndef MESO_VEHICLETABLE_HEADER
#define MESO_VEHICLETABLE_HEADER

#include <GRN/VehicleTable.h>

class MESO_VehicleTable : public VehicleTable
{
   public:

      MESO_VehicleTable() : VehicleTable() { }
      ~MESO_VehicleTable() { }

      RN_Vehicle* newVehicle();
};

#endif
