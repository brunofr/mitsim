//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_ODCell.h
// DATE: Mon Feb 26 17:37:02 1996
//--------------------------------------------------------------------

#ifndef MESO_ODCELL_HEADER
#define MESO_ODCELL_HEADER

#ifdef INTERNAL_GUI
#include <DRN/DRN_ODCell.h>
class MESO_ODCell : public DRN_ODCell
#else
#include <GRN/OD_Cell.h>
class MESO_ODCell : public OD_Cell
#endif

{
   public:

#ifdef INTERNAL_GUI
      MESO_ODCell() : DRN_ODCell() { }
#else
      MESO_ODCell() : OD_Cell() { }
#endif

      ~MESO_ODCell() { }
      
      void emitVehicles();
      RN_Vehicle* newVehicle();
};

#endif
