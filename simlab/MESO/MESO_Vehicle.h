//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_Vehicle.h
// DATE: Mon Feb 12 10:56:19 1996
//--------------------------------------------------------------------

#ifndef MESOMESO_VEHICLE_HEADER
#define MESOMESO_VEHICLE_HEADER

#include <GRN/RN_Vehicle.h>

class MESO_Node;
class MESO_Link;
class MESO_Segment;
class MESO_TrafficCell;
class MESO_CellList;
class MESO_VehicleList;

class DRN_DrawingArea;
class Reader;

const unsigned int FLAG_PROCESSED         = 0x10000000;

class MESO_Vehicle : public RN_Vehicle
{
      friend class MESO_TrafficCell;
      friend class MESO_CellList;
      friend class MESO_VehicleList;
      friend class MESO_Node;
      friend class MESO_Link;
      friend class MESO_Segment;

   public:

      MESO_Vehicle();
      ~MESO_Vehicle() { }

      // Flags

      void toggleFlag(unsigned int flag) {
		 flags_ ^= flag;
      }
      inline unsigned int flag(unsigned int mask = 0xFFFFFFFF) {
         return (flags_ & mask);
      }
      inline void setFlag(unsigned int s) {
         flags_ |= s;
      }
      inline void unsetFlag(unsigned int s) {
         flags_ &= ~s;
      }

      int load(Reader &is, int queue);
	  int load_error(const char *, Reader &is);

      inline MESO_Link * nextMesoLink() {
		 return (MESO_Link *) nextLink_;
      }

      inline MESO_Link * mesoLink() {
		 return (MESO_Link *) link();
      }

      inline MESO_Segment * mesoSegment() {
		 return (MESO_Segment *) segment();
      }

      RN_Link* link();		// virtual
      RN_Segment* segment();	// virtual

      inline MESO_TrafficCell* trafficCell() {
		 return trafficCell_;
      }

      inline void leading(MESO_Vehicle* pv) {
		 leading_ = pv;
      }
      inline void trailing(MESO_Vehicle* pv) {
		 trailing_ = pv;
      }
      inline MESO_Vehicle * leading() {
		 return leading_;
      }
      inline MESO_Vehicle * trailing() {
		 return trailing_;
      }
      float dnPos() {
		 return distance_;
	  }
      inline float upPos() {
		 return distance_ + spaceInSegment_;
	  }
	  inline float spaceInSegment() {
		 return spaceInSegment_;
	  }
	  void calcSpaceInSegment();

	  MESO_Vehicle* leadingVehicleInStream();
	  float gapDistance(MESO_Vehicle* front);

      void updateSpeed();		// interpolate speed

      int init(int id, int ori, int des,
			   int type_id = 0, int path_id = -1); // virtual

      void initialize();		// virtual, called by init()

      void enterPretripQueue();
      int enterNetwork();
      void appendTo(MESO_TrafficCell *);

      void move();		// update position
      int transpose();		// returns 1 if it success
      void advance();		// update position in list
      float expectedPosition();

      int enRoute() {		// virtual
		 return attr(ATTR_ACCESSED_INFO);
      }

	  // Call route choice model

	  void changeRoute();

      // Called when a vehicle arrived its desitination

      void removeFromNetwork();
      void report();

	  // These are used to prevent to process a vehicle twice in a
	  // single iteration

      void markAsProcessed() { 
		 flags_ ^= FLAG_PROCESSED;
      }
	  int isProcessed() {
		 if (stepCounter_ & 0x1) { // odd step
			return !(flags_ & FLAG_PROCESSED);
		 } else {				// even step
			return (flags_ & FLAG_PROCESSED);
		 }
	  }
	  static void increaseCounter() {
		 stepCounter_ ++;
	  }

#ifdef INTERNAL_GUI
      virtual int draw(DRN_DrawingArea *) = 0;
      virtual void erase(DRN_DrawingArea *) = 0;
#endif

	  int logInfo();

   protected:

      MESO_TrafficCell * trafficCell_;	// pointer to current traffic cell
      MESO_Vehicle * leading_;	// downstream vehicle
      MESO_Vehicle * trailing_; // upstream vehicle

      unsigned int flags_;		// indicator for internal use

	  // These variables are use to cache the calculation for speeding
	  // up

	  float spaceInSegment_;	// update each time it enter a new segment

	  static unsigned int stepCounter_;	// iteration counter
};

#endif
