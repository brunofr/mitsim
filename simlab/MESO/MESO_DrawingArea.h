//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// NOTE: 
// AUTH: Qi Yang
// FILE: MESO_DrawingArea.h
// DATE: Tue Apr 16 18:42:02 1996
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#ifndef MESO_VIEWWINDOW_HEADER
#define MESO_VIEWWINDOW_HEADER

#include <DRN/DRN_DrawingArea.h>

class MESO_DrawingArea : public DRN_DrawingArea
{
   public:

      MESO_DrawingArea(Widget parent);

      ~MESO_DrawingArea() { }

	  // Overload the virtual functions

      DRN_Legend * newLegend();
	  DRN_KeyInteractor * newKeyInteractor();
};

#endif
#endif // INTERNAL_GUI
