//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_TrafficCell.h
// DATE: Mon Feb 12 11:48:12 1996
//--------------------------------------------------------------------

#ifndef MESOMESO_TRAFFICCELL_HEADER
#define MESOMESO_TRAFFICCELL_HEADER

class MESO_Link;
class MESO_Segment;
class MESO_Vehicle;
class MESO_CellList;

class MESO_TrafficCell
{
      friend class MESO_Node;
      friend class MESO_CellList;
      friend class MESO_Segment;
      friend class MESO_Vehicle;

   public:

      MESO_TrafficCell();
      ~MESO_TrafficCell();

      void clean();
  
      inline MESO_TrafficCell* trailing() {
		 return trailing_;
      }
      inline MESO_TrafficCell* leading() {
		 return leading_;
      }

      void initialize();

	  MESO_Link* mesoLink();

      inline MESO_Vehicle* firstVehicle() {
		 return firstVehicle_;
	  }
      inline MESO_Vehicle* lastVehicle() {
		 return lastVehicle_;
	  }
      inline int nVehicles() {
		 return nVehicles_;
	  }

	  // Insert a vehicle into the cell based on its distance position

	  void insert(MESO_Vehicle *);
	  
	  // Append a vehicle at the end of the cell

      void append(MESO_Vehicle *);

	  // Split the traffic cell into two cells at the first gap
	  // smaller than the given threshold

	  void split();

      void remove(MESO_Vehicle *);

	  // Append the following cell to the end of this cell, the
	  // following cell becomes empty but is NOT removed.

      void append(MESO_TrafficCell *follower);

      MESO_Link* link();
      inline MESO_Segment* segment() {
		 return segment_;
	  }

      // This function decide which speed-density function will be
      // used for this traffic cell

      short int sdIndex();

      // These are the dn position of the first and up position of the
      // last vehicles in the cell at the moment this function is
      // called.

      float dnDistance();
      float upDistance();
	  float maxReachablePosition();
      float length();

      // These are the dn position of the first and the up position of
      // the last vehicles in the cell when this traffic cell is
      // updated.  These values are changed at the update phase.

	  inline float tailPosition() {
		 return tailPosition_;
	  }
	  inline float tailSpeed() {
		 return tailSpeed_;
	  }
      inline float headPosition(short int i) {
		 return (i < nHeads_) ? headPositions_[i] : headPositions_[0];
	  }
	  inline float headSpeed(short int i) {
		 return (i < nHeads_) ? headSpeeds_[i] : headSpeeds_[0];
	  }
	  float headSpeed();		// average of the headSpeeds
	  float speed();			// average speed of the heads and tail

	  void setHeadSpeeds(float spd, float pos);
      
	  // These two are based on current state and referenced to the
	  // last vehicle in the cell

	  float calcDensity();
	  float calcSpeed();
	  float upSpeed();

	  // Speed for a given density

	  float calcSpeed(float density);

	  // These calculations are based on states at the beginning of
	  // current update interval

	  void updateTailSpeed();
	  void updateHeadSpeeds();

	  void calcHeadSpeed(short int ith, float head_pos);
	  float calcHeadSpeed(MESO_TrafficCell *);
	  float calcFollowingCellSpeed(float dis, float spd);

	  void advanceVehicles();

	  float distance(MESO_TrafficCell* dp);

	  int isJammed();
	  int isReachable();

	  float timeSinceDispatching();

#ifdef INTERNAL_GUI
	  void draw(class DRN_DrawingArea *);
#endif

   protected:

	  // Constant data members. Once created, never changes.

	  MESO_Segment * segment_; // container

	  // Data members calculated in update phase

	  float tailSpeed_;		// upstream speed
	  float tailPosition_;	// upstream position

	  short int nHeads_;		// number of heads
	  float *headSpeeds_;		// downstream speeds
	  float *headPositions_;	// downstream positions

	  // Data members dynamically changed in advance phase

	  int nVehicles_;		// number of vehicles

	  MESO_Vehicle * firstVehicle_; // first vehicle (dn) in this TC
	  MESO_Vehicle * lastVehicle_; // last vehicle (up) in this TC

	  // Bookkeeping data members. May change in both update and
	  // advance phases

	  MESO_TrafficCell * trailing_; // upstream traffic cell
	  MESO_TrafficCell * leading_; // downstream traffic cell

	  double queueTime_;		// time was a queue
};

#endif
