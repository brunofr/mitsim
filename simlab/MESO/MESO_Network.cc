//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_Network.C
// DATE: Mon Feb 26 16:58:57 1996
//--------------------------------------------------------------------

#include <new>
using namespace std;

#include <Tools/Math.h>

#include <Tools/SimulationClock.h>
#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <Tools/Random.h>

#include <IO/IOService.h>
#include <IO/Exception.h>

#include <GRN/RN_DynamicRoute.h>

#include "MESO_Engine.h"
#include "MESO_Network.h"
#include "MESO_Node.h"
#include "MESO_Link.h"
#include "MESO_Segment.h"
#include "MESO_Lane.h"
#include "MESO_Engine.h"
#include "MESO_Status.h"
#include "MESO_Sensor.h"
#include "MESO_Signal.h"
#include "MESO_TollBooth.h"
#include "MESO_FileManager.h"
#include "MESO_Parameter.h"
#include "MESO_TrafficCell.h"
#include "MESO_VehicleList.h"
#include "MESO_Communicator.h"

#ifdef INTERNAL_GUI
#include <Xmt/Xmt.h>
#include <DRN/DRN_DrawingArea.h>
#include <DRN/DRN_DrawingArea.h>
#include "MESO_Interface.h"
#include "MESO_Symbols.h"
#include "MESO_Menu.h"
#include "MESO_DrawableVehicle.h"
#else
#include "MESO_Vehicle.h"
#endif

MESO_Network::MESO_Network()
#ifdef INTERNAL_GUI
  : DrawableRoadNetwork()
#else  // batch mode
	: RoadNetwork()
#endif // !INTERNAL_GUI
{
}

MESO_Network::~MESO_Network() {
}


RN_Node* MESO_Network::newNode() { 
  return new MESO_Node; 
}

RN_Link* MESO_Network::newLink() { 
  return new MESO_Link;
}

RN_Segment* MESO_Network::newSegment() {
  return new MESO_Segment;
}

RN_Lane* MESO_Network::newLane() {
  return new MESO_Lane;
}

RN_Sensor* MESO_Network::newSensor()
{
  return new MESO_Sensor;
}

RN_Signal* MESO_Network::newSignal() {
  return new MESO_Signal;
}

RN_TollBooth* MESO_Network::newTollBooth() {
  return new MESO_TollBooth;
}


void
MESO_Network::calcStaticInfo(void) {
  RoadNetwork::calcStaticInfo();
  organize();
}


void
MESO_Network::organize()
{
  for (register int i = 0; i < nLinks(); i ++) {
	((MESO_Link *)link(i))->checkConnectivity();
  }
}


/*
 *-------------------------------------------------------------------
 * Calculate the density for each segments
 *-------------------------------------------------------------------
 */

void MESO_Network::calcSegmentData()
{
  MESO_Segment *ps;
  for (register int i = 0; i < nSegments(); i ++) {
	ps = mesoSegment(i);
	ps->calcDensity();
	ps->calcSpeed();
  }
}


/*
 *-------------------------------------------------------------------
 * Add number of vehicles allowed to move out during this time step
 * to the segment balance.
 *-------------------------------------------------------------------
 */

void MESO_Network::resetSegmentEmitTime()
{
  for (register int i = 0; i < nSegments(); i ++) {
	mesoSegment(i)->resetEmitTime();
  }
}

/*
 *--------------------------------------------------------------------
 * Enter vehicles queued at starting link into the network.
 *--------------------------------------------------------------------
 */

void
MESO_Network::enterVehiclesIntoNetwork()
{
  MESO_Vehicle *pv;
    
  for (register int i = 0; i < nLinks(); i ++) {
	/* Find the first vehicle in the queue and enter it into the
	 * network if space is available.  If the link is full, there is
	 * no need for checking other vehicles in the queue, skip to the
	 * next link.
	 */

	while ((pv = ((MESO_Link *)link(i))->queueHead()) &&
		   (pv->enterNetwork()))
      {
		/* push static vehicle attributes on message buffer */
		;
      }
  }
}


/*
 *--------------------------------------------------------------------
 * For each traffic cells in the network, calculate its density and
 * upSpeed. These two variables depend only on the state of a
 * particular traffic cell itself.
 *--------------------------------------------------------------------
 */

void
MESO_Network::calcTrafficCellUpSpeed()
{
  // cout << " YYY Network" ;
  MESO_Segment *ps;
  for (register int i = 0; i < nLinks(); i ++) {
	ps = (MESO_Segment *)link(i)->endSegment();
	while (ps) {
	  ps->calcTrafficCellUpSpeed();
	  ps = (MESO_Segment *)ps->upstream();
	}
  }
}


/*
 *--------------------------------------------------------------------
 * For each traffic cells in the network, calculate its dnSpeed. This
 * variable variable depends on its own state and the states of the
 * traffic cells ahead.  This function is called after
 * calcIndependentTrafficCellStates() is called.
 *--------------------------------------------------------------------
 */

void
MESO_Network::calcTrafficCellDnSpeeds()
{
  MESO_Segment *ps;
  for (register int i = 0; i < nLinks(); i ++) {
	ps = (MESO_Segment *)link(i)->endSegment();
	while (ps) {		// downstream first
	  ps->calcTrafficCellDnSpeeds();
	  ps = (MESO_Segment *)ps->upstream();
	}
  }
}


/*
 *--------------------------------------------------------------------
 * Move vehicles based on the speeds of the traffic cell, capcity
 * of the downstream segment, and the turning movement.
 *--------------------------------------------------------------------
 */

void
MESO_Network::advanceVehicles(void)
{
  // Generate a random order for the links

  static int *permuteLink = NULL;
  static int nPermutedLinks = 0;

  register int i;

  if (nPermutedLinks != nLinks()) { // this piece is executed only once
	if (permuteLink) {
	  delete [] permuteLink;
	}
	nPermutedLinks = nLinks();
	permuteLink = new int[nPermutedLinks];
	for (i = 0; i < nPermutedLinks; i ++) {
	  permuteLink[i] = i;
	}
  }

  // Randomize the link order

  theRandomizers[Random::Misc]->permute(nLinks(), permuteLink);
  for (i = 0; i < nLinks(); i ++) {
	MESO_Link *p = mesoLink(permuteLink[i]);
	p->advanceVehicles();
  }
}

void 
MESO_Network::updateNodeCapacities()
{
  // Update capacities

  for (int i = 0; i < nNodes(); i ++) {
	mesoNode(i)->calcCapacities();
  }
}

/*
 *--------------------------------------------------------------------
 * Some TCs may be dissolved in previous advance step.  This functions
 * remove the dissolved TCs and combines samilar TCs in the network.
 *--------------------------------------------------------------------
 */

void
MESO_Network::formatTrafficCells()
{
  for (register int i = 0; i < nSegments(); i ++) {
	mesoSegment(i)->formatTrafficCells();
  }
}


void
MESO_Network::clean()
{
  register int i;

  // Release current cells

  for (i = 0; i < nLinks(); i ++) {
	mesoLink(i)->clean();
  }

  // Restore capacities

  for (i = 0; i < nSegments(); i ++) {
	MESO_Segment *ps = mesoSegment(i);
	ps->setCapacity(ps->defaultCapacity());
  }

#ifdef INTERNAL_GUI
  theDrawingArea->redraw();
#endif
}

long int
MESO_Network::loadInitialState(const char *name, int check_time)
{
  if (!ToolKit::isValidFilename(name)) {
	cout << "No initial state. " << endl
		 << "Simulation start from empty network." << endl;
	return (long int) theSimulationClock->currentTime();
  }

  Reader is(name);

  MESO_Vehicle *pv;
  int tnum = 0, inum = 0, qnum = 0;

  is.eatwhite();

  // Start time

  double start;
  is >> start;

  // Vehicles in the network

  if (!is.findToken(OPEN_TOKEN)) theException->exit();
  for (is.eatwhite();
	   is.peek() != CLOSE_TOKEN;
	   is.eatwhite()) {
	pv = theVehicleList->recycle(); // create a new vehicle
	if (pv->load(is, 0) != 0) {
	  theVehicleList->recycle(pv); // put it back
	} else {
	  inum ++;
	}
	tnum ++;
  }
  if (!is.findToken(CLOSE_TOKEN)) theException->exit();

  // Vehicles in pretrip queue

  if (!is.findToken(OPEN_TOKEN)) theException->exit();
  for (is.eatwhite();
	   is.peek() != CLOSE_TOKEN;
	   is.eatwhite()) {
	pv = theVehicleList->recycle(); // create a new vehicle
	if (pv->load(is, 1) != 0) {
	  theVehicleList->recycle(pv); // put it back
	} else {
	  qnum ++;
	}
	tnum ++;
  }
  if (!is.findToken(CLOSE_TOKEN)) theException->exit();

  is.close();

  if (ToolKit::verbose()) {
	cout << tnum << " vehicles parsed, "
		 << inum << " loaded, and "
		 << qnum << " queued." << endl;
  }

  // In the simlab, we do not check time becuase the currentTime will
  // be set to the time it read here.

  if (check_time && !AproxEqual(start, theSimulationClock->currentTime())) {
	cerr << "Warning:: State dumped at "
		 << theSimulationClock->convertTime(start)
		 << " loaded at "
		 << theSimulationClock->currentStringTime() << endl;
  }

  return (long) (start + 0.5);
}


void
MESO_Network::saveLinkTravelTimes()
{
  long int offset = (long int) theEngine->beginTime();
  long int len = (long int) theEngine->endTime() - offset;
  int step = theGuidedRoute->infoPeriodLength();
  int col = offset / step;
  int ncols = len / step;
  int num = col + ncols;

  const char* fn;
  fn = Str("l%x.tmp", theCommunicator->simlab().tid());
  ofstream os(fn);
  if (!os.good()) {
	cerr << "Error:: Cannot open output file <" << fn << ">." << endl;
	theException->exit();
  }

  os << "% SIMULATED LINKS TRAVEL TIMES" << endl
	 << col << "\t% Start period" << endl
	 << ncols << "\t% Number of periods" << endl
	 << step << "\t% Length per period" << endl;

  os << OPEN_TOKEN << endl;
  for (int i = 0; i < nLinks(); i ++) {
	MESO_Link *pl = (MESO_Link*) link(i);
	os << indent << pl->code()
	   << endc << pl->type()
	   << endc << pl->length();
	for (int j = col; j < num; j ++) {
	  os << endc << pl->averageTravelTimeEnteringAt(j);
	}
	os << endl;
  }
  os << CLOSE_TOKEN << endl;

  os.close();
}


/*
 * Before receiving sensor data for nect time period, all sensors are
 * reset to zero state.
 */

void
MESO_Network::resetSensorReadings()
{
  for (register int i = 0; i < nSensors(); i ++ ) {
	sensor(i)->resetSensorReadings();
  }
}


// Print the queue length at each loading point.

int
MESO_Network::watchQueueLength(ostream& os)
{
  int n = 0;
  for (register int i = 0; i < nLinks(); i ++) {
	n += mesoLink(i)->reportQueueLength(os);
  }
  return n;
}

void
MESO_Network::guidedVehiclesUpdatePaths()
{
  MESO_TrafficCell *cell;
  MESO_Vehicle *pv;
  register int i;

  // Vehicles already in the network

  for (i = 0; i < nSegments(); i ++) {
	cell = mesoSegment(i)->firstCell();
	while (cell) {
	  pv = cell->firstVehicle();
	  while (pv) {
		if (pv->isGuided()) {
		  pv->changeRoute();
		}
		pv = pv->trailing();
	  }
	  cell = cell->trailing();
	}
  }

  // Vehicles waiting for entering the network
   
  for (i = 0; i < nLinks(); i ++) {
	pv = mesoLink(i)->queueHead();
	while (pv) {
	  if (pv->isGuided()) {
		pv->changeRoute();
	  }
	  pv = pv->trailing();
	}
  }
}


void MESO_Network::recordLinkTravelTimeOfActiveVehicle()
{
  MESO_TrafficCell *cell;
  MESO_Vehicle *pv;
  register int i;

  // Record travel time for vehicle still in the network

  for (i = 0; i < nSegments(); i ++) {
	cell = mesoSegment(i)->firstCell();
	while (cell) {
	  pv = cell->firstVehicle();
	  while (pv) {
		pv->link()->recordExpectedTravelTime(pv);
		pv = pv->trailing();
	  }
	  cell = cell->trailing();
	}
  }

  // Record travel time for vehicle in ths spill back queues

  for (i = 0; i < nLinks(); i ++) {
	pv = mesoLink(i)->queueHead();
	while (pv) {
	  pv->nextLink()->recordExpectedTravelTime(pv);
	  pv = pv->trailing();
	}
  }
}

int
MESO_Network::dumpNetworkState(const char *filename)
{
  register int i;
  MESO_TrafficCell *cell;
  MESO_Vehicle *pv;
   
  ofstream os(filename);

  if (!os.good()) {
	cerr << "Error:: Failed doing a state dump to file <"
		 << filename << ">." << endl;
	return 1;
  }

  os << "/*" << endl
	 << " * A snap shot of the network state in MesoTS" << endl
	 << " * Generated by " << UserName() << endl
	 << " * at " << TimeTag() << endl
	 << " *" << endl
	 << " * {" << endl
	 << " *   Code Type OriIndex DesIndex DepTime Mileage (cont)" << endl
	 << " *   Path PathIndex TimeEntersLink SegIndex SegDist" << endl
	 << " * }" << endl
	 << " */" << endl << endl;

  os << theSimulationClock->currentTime() << "\t# "
	 << theSimulationClock->currentStringTime() << endl;

  os << OPEN_TOKEN
	 << "\t# Vehicles in the network" << endl;
  int ni = 0;
  for (i = 0; i < nSegments(); i ++) {
	cell = mesoSegment(i)->firstCell();
	while (cell) {
	  pv = cell->firstVehicle();
	  while (pv) {
		pv->dumpState(os);
		ni ++;
		pv = pv->trailing();
	  }
	  cell = cell->trailing();
	}
  }
  os << CLOSE_TOKEN << "\t#" << ni << endl;

  os << OPEN_TOKEN
	 << "\t# Vehicles in the pretrip queues" << endl;
  int nq = 0;
  for (i = 0; i < nLinks(); i ++) {
	pv = mesoLink(i)->queueHead();
	while (pv) {
	  pv->dumpState(os);
	  nq ++;
	  pv = pv->trailing();
	}
  }
  os << CLOSE_TOKEN << "\t#" << nq << endl;

  os << endl
	 << "# " << (ni + nq) << " vehciles dumped" << endl;

  os.close();

  return 0;
}

#ifdef INTERNAL_GUI

void
MESO_Network::drawNetwork(DRN_DrawingArea * area)
{
  DrawableRoadNetwork::drawNetwork(area);

  if (area->state(DRN_DrawingArea::MOUSE_DOWN)) return;

  drawTrafficCells(area);
}


// view has changed so clear nBars_ in each cell in the previously
// visible segments.  This function overload the virtual functions in
// base class.

void
MESO_Network::unsetDrawingStates(DRN_DrawingArea * area)
{
  DrawableRoadNetwork::unsetDrawingStates(area);
  MESO_Segment *ps;
  MESO_TrafficCell *cell;
  MESO_Vehicle *vehicle;
  for (int i = 0; i < nSegments(); i++) {
	ps = mesoSegment(i);
	if (!ps->isVisible()) continue;
	cell = ps->firstCell();
	while (cell) {
	  vehicle = cell->firstVehicle();
	  while (vehicle) {
		((MESO_DrawableVehicle *)vehicle)->resetDrawing();
		vehicle = vehicle->trailing();
	  }
	  cell = cell->trailing();
	}
  }
  area->unsetState(DRN_DrawingArea::VEHICLE_ON);
}


void
MESO_Network::drawTrafficCells(DRN_DrawingArea * area)
{
  if (area->state(DRN_DrawingArea::VIEW_NONE)) return;

  if (!area->isVehicleDrawable() ||
	  !theSymbols()->vehicleBorderCode().value()) return;
  
  MESO_Segment *ps;
  MESO_TrafficCell *cell;
  for (register int i = 0; i < nSegments(); i ++) {
	ps = (MESO_Segment*)segment(i);
	if (!ps->isVisible()) continue;
	cell = ps->firstCell();
	while (cell) {
	  cell->draw(area);
	  cell = cell->trailing();
	}
  }
  area->setState(DRN_DrawingArea::VEHICLE_ON);
}

void MESO_Network::queryVehicle(const XmwPoint& point)
{
  WcsPoint pressed = theDrawingArea->window2World(point);
  MESO_Segment *ps = (MESO_Segment *)nearestSegment(pressed);
  if (ps) {
	ps->queryVehicle(pressed);
  }
}

void MESO_Network::setIncident(const XmwPoint& point)
{
  WcsPoint pressed = theDrawingArea->window2World(point);
  MESO_Segment *ps = (MESO_Segment *)nearestSegment(pressed);
  if (ps) {
	ps->changeCapacity();
  }
}

#endif
