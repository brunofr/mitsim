//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_Engine.h
// DATE: Mon Feb 26 16:04:12 1996
//--------------------------------------------------------------------

#ifndef MESO_ENGINE_HEADER
#define MESO_ENGINE_HEADER

#include <Tools/SimulationEngine.h>
#include <iostream>

class MESO_Engine : public SimulationEngine
{
  friend class MESO_CmdArgsParser;
  friend class MESO_Communicator;
  friend class MESO_FileManager;

#ifdef INTERNAL_GUI
  friend class MESO_SetupDialog;
#endif

protected:
      
  double batchStepSize_;	// update console and queue info
  double updateStepSize_;	// update traffic cell variables
  double pathStepSize_;		// update shortest path

  double beginTime_;		// start time for this run
  double endTime_;			// end time for this run

  double depRecordStepSize_;

  int rollingLength_;		// length of the rolling horizon
  int rolls_;				// number of rollings
  int iteration_;			// id of the iteration

  float frequency_;			// 1/step size

  double simlabMsgStepSize_; // update gui info
  double stateStepSize_;	 // write state data for dta 

  double batchTime_;
  double updateTime_;
  double pathTime_;
  double simlabMsgTime_;
  double stateTime_;
  int tm_;				// time period for state3d

public:
      
  MESO_Engine();
  ~MESO_Engine() { }

  inline float frequency() { return frequency_; }

  const char *ext();

  void init();

  // This function starts the simulation loops. It will NOT return
  // until simulation is done (or cancelled in graphical mode).
  // This function calls simulationLoop() iteratively or as a
  // recusive timeout callback.

  void run();
  void quit(int state);

  //      static int information() { return information_; }

  static void toggleRoutingLogic(int waht);
      
  int loadSimulationFiles();
  int loadMasterFile();
  int simulationLoop();

  void saveDefaultGuidance();
  void restart();
  void sendResults();
  double updateStepSize() { return updateStepSize_; }
  double beginTime() { return beginTime_; }
  double endTime() { return endTime_; }

  int receiveMessages(double sec = 0.0);	// virtual

  void save(std::ostream &os = std::cout); // virtual
};

extern MESO_Engine * theEngine;

#endif
