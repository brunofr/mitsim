//-*-c++-*------------------------------------------------------------
// MESO_Interface.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class definition for the class MESO_Interface
//--------------------------------------------------------------------

#ifndef MESO_INTERFACE_HEADER
#define MESO_INTERFACE_HEADER

#include <DRN/DRN_Interface.h>

class MESO_Menu;
class MESO_Symbols;

class MESO_Interface : public DRN_Interface
{
   public:

	  MESO_Interface(int *argc, char **argv);
	  ~MESO_Interface();

	  void create();			// virtual

	  XmwSymbols* createSymbols(); // virtual
	  MESO_Symbols* symbols() { return (MESO_Symbols*) symbols_; }

	  DRN_DrawingArea* drawingArea(Widget w); // virtual

	  XmwMenu *menu(Widget parent);	// virtual
	  MESO_Menu *menu() { return (MESO_Menu*) menu_; }

	  XmwModeline* modeline(Widget parent);	// virtual
	  XmwModeline* modeline() { return modeline_; }
};

extern MESO_Interface *theInterface;
inline MESO_Symbols* theSymbols() {
   return theInterface->symbols();
}

#endif
