//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_CmdArgsParser.C
// DATE: Mon Feb 26 15:27:35 1996
//--------------------------------------------------------------------

#include <iomanip>
using namespace std;

#include <GRN/RN_Route.h>

#include "MESO_CmdArgsParser.h"
#include "MESO_Engine.h"

MESO_CmdArgsParser::MESO_CmdArgsParser()
  : CmdArgsParser()
{
  add(new Clo("-output", &theEngine->chosenOutput_, 0,
			  "Output mask"));

  char *doc;
  doc = StrCopy("Routing options for guided drivers:\n"
				"%6X = Time variant\n"
				"%6X = Update shortest path tree\n"
				"%6X = Update path table info\n"
				"%6X = Use existing tables if available\n"
				"%6X = Updated travel time available for pretrip planning",
				INFO_FLAG_DYNAMIC,
				INFO_FLAG_UPDATE_TREES,
				INFO_FLAG_UPDATE_PATHS,
				INFO_FLAG_USE_EXISTING_TABLES,
				INFO_FLAG_AVAILABILITY);

  add(new Clo("-sp", &theSpFlag, 0, doc));
}
