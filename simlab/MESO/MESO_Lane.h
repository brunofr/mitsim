//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_Lane.h
// DATE: Mon Feb 26 16:26:40 1996
//--------------------------------------------------------------------

#ifndef MESO_LANE_HEADER
#define MESO_LANE_HEADER

//#include <iostream>

// This model does not explicitly represent lanes.

#ifdef INTERNAL_GUI
#include <DRN/DRN_Lane.h>
#define MESO_Lane DRN_Lane
#else
#include <GRN/RN_Lane.h>
#define MESO_Lane RN_Lane
#endif

#endif
