//-*-c++-*------------------------------------------------------------
// NAME: Traffic Simulation
// NOTE: 
// AUTH: Qi Yang
// FILE: MESO_Incident.h
// DATE: Mon Dec 16 15:25:14 1996
//--------------------------------------------------------------------

#ifndef MESO_INCIDENT_HEADER
#define MESO_INCIDENT_HEADER

#include <string.h>
#include <Templates/Object.h>
#include <Templates/Listp.h>

class MESO_Segment;
class MESO_Incident;

typedef Listp<MESO_Incident*> inc_list_type;
typedef inc_list_type::link_type inc_list_link_type;

class MESO_Incident : public CodedObject
{
public:
	  
  MESO_Incident() : CodedObject(),
	time_(86400.0), segment_(NULL), capChange_(0.0) {
  }
  MESO_Incident(double, MESO_Segment *, float);
  ~MESO_Incident() { }

  int comp(CodedObject *);
  int comp(int id) {
	if (id < code_) return -1;
	else if (id > code_) return 1;
	else return 0;
  }
  void updateCapacity();
  double time() { return time_; }

  static char *filename() { return filename_; }
  static char **filenameptr() { return &filename_; }
  static void filename(const char *s) {
	if (filename_) free(filename_);
	if (s) filename_ = strdup(s);
	else filename_ = NULL;
  }

private:
	  
  static char *filename_;

  double time_;
  MESO_Segment* segment_;
  float capChange_;
};

extern void LoadIncidents(const char *filename);
extern void UpdateCapacities();

#endif
