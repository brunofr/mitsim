//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_VehicleList.C
// DATE: Tue Feb 27 11:38:29 1996
//--------------------------------------------------------------------

#include <Tools/ToolKit.h>
#include "MESO_VehicleList.h"

#ifdef INTERNAL_GUI
#include "MESO_DrawableVehicle.h"
#else
#include "MESO_Vehicle.h"
#endif

MESO_VehicleList * theVehicleList = new MESO_VehicleList;

MESO_VehicleList::MESO_VehicleList()
   : head_(NULL),
	 tail_(NULL),
	 nVehicles_(0),
	 nPeakVehicles_(0)
{
}

MESO_VehicleList::~MESO_VehicleList()
{
   while (head_) {
      tail_ = head_;
      head_ = head_->trailing_;
      delete tail_;
   }
   tail_ = NULL;
   nVehicles_ = 0;
}


/*
 * add a vehicle to the end of the vehicle list.
 */

void
MESO_VehicleList::recycle(MESO_Vehicle *pv)
{
   pv->donePathIndex();
   pv->trafficCell_ = NULL;

   pv->trailing_ = NULL;
   pv->leading_ = tail_;

   if (tail_) {					// no vehicle in the list
      tail_->trailing_ = pv;
   } else {						// at least one in the list
      head_ = pv;
   }
   tail_ = pv;
   nVehicles_ ++;
}


/*
 * Remove a vehicle from the head of the vehicle list.
 */

MESO_Vehicle*
MESO_VehicleList::recycle()
{
   MESO_Vehicle* pv;

   if (head_) {					// get head from the list
      pv = head_;
      if (tail_ == head_) {		// the onle one vehicle in list
		 head_ = tail_ = NULL;
      } else {					// at least two vehicles in list
		 head_ = head_->trailing_;
		 head_->leading_ = NULL;
      }
      nVehicles_ --;
   } else {						// list is empty
#ifdef INTERNAL_GUI
      pv = new MESO_DrawableVehicle;
#else
      pv = new MESO_Vehicle;
#endif
      nPeakVehicles_ ++;
   }
   return (pv);
}
