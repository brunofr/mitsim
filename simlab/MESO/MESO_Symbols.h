//-*-c++-*------------------------------------------------------------
// MESO_Symbols.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class definition/implementation for the class
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#ifndef MESO_SYMBOLS_HEADER
#define MESO_SYMBOLS_HEADER

#include <DRN/DRN_Symbols.h>

class MESO_Symbols : public DRN_Symbols
{
   public:

	  enum {
		 VEHICLE_BORDER_NONE     = 0,
		 VEHICLE_BORDER_TYPE     = 1,
		 VEHICLE_BORDER_INFO     = 2,
		 VEHICLE_BORDER_MOVEMENT = 3,
		 VEHICLE_BORDER_SPEED    = 4
      };

	  enum {
		 VEHICLE_SHADE_NONE      = 0,
		 VEHICLE_SHADE_WAITING   = 1
	  };

   public:

	  MESO_Symbols() : DRN_Symbols() { };
	  ~MESO_Symbols() { }

	  void registerAll();		// virtual

	  // Access functions

	  XmwSymbol<int>& vehicleBorderCode() {
		 return vehicleBorderCode_;
	  }
	  XmwSymbol<int>& vehicleShadeCode() {
		 return vehicleShadeCode_;
	  }
	  XmwSymbol<Boolean>& isVehicleLabelOn() {
		 return isVehicleLabelOn_;
	  }

   protected:

	  // New symbols

	  XmwSymbol<int> vehicleBorderCode_;
	  XmwSymbol<int> vehicleShadeCode_;
	  XmwSymbol<Boolean> isVehicleLabelOn_;
};

#endif // MESO_SYMBOLS_HEADER
#endif // INTERNAL_GUI
