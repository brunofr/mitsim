//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_DrawingArea.C
// DATE: Tue Apr 16 18:47:29 1996
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include "MESO_DrawingArea.h"
#include "MESO_Legend.h"
#include "MESO_KeyInteractor.h"

MESO_DrawingArea::MESO_DrawingArea(Widget parent)
   : DRN_DrawingArea(parent)
{
   theDrawingArea = this;
}

DRN_KeyInteractor* MESO_DrawingArea::newKeyInteractor()
{
   return new MESO_KeyInteractor ( this );
}

DRN_Legend* MESO_DrawingArea::newLegend()
{
   return new MESO_Legend ( this );
}

#endif
