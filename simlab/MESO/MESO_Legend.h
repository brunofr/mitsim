//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_Legend.h
// DATE: Mon Feb 26 16:34:29 1996
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#ifndef MESO_LEGEND_HEADER
#define MESO_LEGEND_HEADER

#include <DRN/DRN_Legend.h>

class MESO_Legend : public DRN_Legend
{
   public:
      MESO_Legend(DRN_DrawingArea *d) : DRN_Legend(d) { }
      ~MESO_Legend() { }

      void draw(int mode);	// virtual
      void draw_more();		// virtual
      void drawVehicleLegend();
};

#endif

#endif
