//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_CmdArgsParser.h
// DATE: Mon Feb 26 15:26:26 1996
//--------------------------------------------------------------------

#ifndef MESO_CMDARGSPARSER_HEADER
#define MESO_CMDARGSPARSER_HEADER

// This class register additional command line arguments for TMS

#include <Tools/CmdArgsParser.h>

class MESO_CmdArgsParser : public CmdArgsParser
{
   public:
      
      MESO_CmdArgsParser();
      ~MESO_CmdArgsParser() { }
};

#endif
