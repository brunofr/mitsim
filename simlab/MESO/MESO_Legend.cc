//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_Legend.C
// DATE: Mon Feb 26 16:35:03 1996
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>

#include <DRN/DRN_DrawingArea.h>

#include "MESO_Interface.h"
#include "MESO_Symbols.h"
#include "MESO_Legend.h"
#include "MESO_Segment.h"
#include "MESO_Parameter.h"

void MESO_Legend::draw(int mode)
{
   DRN_Legend::draw(mode & ~2);
}


void MESO_Legend::draw_more()
{
   drawVehicleLegend();
}


void MESO_Legend::drawVehicleLegend()
{
   if (!theDrawingArea->isVehicleDrawable()) return;

   theDrawingArea->foreground(theColorTable->yellow());
   offset_ += theDrawingArea->fontHeight();
   XmwPoint p(pos_.x(), pos_.y() + offset_);

   switch (theSymbols()->vehicleBorderCode().value()) {
      case MESO_Symbols::VEHICLE_BORDER_INFO:
      {
		 theDrawingArea->drawLString("Path/Info Availability", p);
		 drawLongBoxSymbol(theColorTable->black(),      "Unguided");
		 drawLongBoxSymbol(theColorTable->red(),        "  w/ Path");
		 drawLongBoxSymbol(theColorTable->yellow(),     "  w/o Path");
		 drawLongBoxSymbol(theColorTable->black(),      "Guided");
		 drawLongBoxSymbol(theColorTable->cyan(),       "  w/ Path");
		 drawLongBoxSymbol(theColorTable->lightGreen(), "  w/o Path");
		 break;
      }
      case MESO_Symbols::VEHICLE_BORDER_SPEED:
      {
		 if (theSymbols()->segmentColorCode().is(DRN_Symbols::SEGMENT_SPEED)) {
			drawLineSymbols(theParameter->speedLabel(),
							theDrawingArea->speed(),
							theParameter->speedFactor());
		 }
		 break;
      }
      case MESO_Symbols::VEHICLE_BORDER_TYPE:
      {
		 theDrawingArea->drawLString("Vehicle Class", p);
		 for (int i = 0; i < theParameter->nVehicleClasses(); i ++) {
			drawLongBoxSymbol(theParameter->vehicleColors(i),
							  theParameter->vehicleName(i));
		 }
		 break;
      }
      case MESO_Symbols::VEHICLE_BORDER_MOVEMENT:
      {
		 theDrawingArea->drawLString("Vehicle Class", p);
		 char label[2];
		 label[1] = '\0';
		 for (int i = 0; i < 4; i ++) {
			label[0] = '1' + i;
			drawShortLineSymbol(theColorTable->color(0.25 * i),
								label);
		 }
		 drawShortLineSymbol(theColorTable->white(), "Arrival");
		 break;
      }
      default:
      {
		 break;
      }
   }
}


#endif
