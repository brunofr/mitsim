//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_Sensor.h
// DATE: Mon Feb 26 17:55:55 1996
//--------------------------------------------------------------------

#ifndef MESO_SENSOR_HEADER
#define MESO_SENSOR_HEADER

#include <TC/TC_Sensor.h>

// Curently MESO_Sensor are the same as TC_Sensor.  More
// functionalities may need to be added later so I reserved the name
// here.

#define MESO_Sensor		TC_Sensor

#endif



