//-*-c++-*------------------------------------------------------------
// MESO_KeyInteractor.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI
#ifndef MESO_KEYINTERACTOR_HEADER
#define MESO_KEYINTERACTOR_HEADER

#include <DRN/DRN_KeyInteractor.h>

class MESO_KeyInteractor : public DRN_KeyInteractor
{
   public:

	  MESO_KeyInteractor(XmwDrawingArea* drawing_area) :
		 DRN_KeyInteractor(drawing_area) {
	  }

	  ~MESO_KeyInteractor() { }

	  Boolean keyPress(KeySym keysym);
};

#endif
#endif // INTERNAL_GUI
