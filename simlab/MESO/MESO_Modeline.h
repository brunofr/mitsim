//-*-c++-*------------------------------------------------------------
// MESO_Modeline.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef MESO_MODELINE_HEADER
#define MESO_MODELINE_HEADER

#include <DRN/DRN_Modeline.h>

class MESO_Modeline : public DRN_Modeline
{
   public:

	  MESO_Modeline(Widget parent);
	  ~MESO_Modeline() { }

	  void updateCounter(int i, int n);

   protected:
	  
	  Widget counters_;
};

#endif
