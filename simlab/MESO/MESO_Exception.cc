//-*-c++-*------------------------------------------------------------
// NAME: Exception handler
// AUTH: Qi Yang
// FILE: MESO_Exception.C
// DATE: Sun Mar 17 14:36:22 1996
//--------------------------------------------------------------------


#include <GRN/RN_Route.h>
#include <GRN/RN_PathTable.h>

#include <Tools/SimulationClock.h>

#include "MESO_Exception.h"
#include "MESO_Communicator.h"

#include "MESO_Network.h"
#include "MESO_CellList.h"
#include "MESO_VehicleList.h"
#include "MESO_ODTable.h"
#include "MESO_ODCell.h"
#include "MESO_FileManager.h"

void
MESO_Exception::exit(int code)
{
   if (theCommunicator->isSimlabConnected()) {
      theCommunicator->simlab() << MSG_QUIT;
      theCommunicator->simlab().flush(SEND_IMMEDIATELY);
   }
   cleanup();
   IOService::exit();
   Exception::exit(code);
}

void
MESO_Exception::done(int code)
{
   if (theCommunicator->isSimlabConnected()) {
      theCommunicator->simlab() << MSG_DONE;
      theCommunicator->simlab().flush(SEND_IMMEDIATELY);
   }
   cleanup();
   IOService::exit();
   Exception::done(code);
}


void
MESO_Exception::cleanup()
{
   if (theNetwork) {
	 mesoNetwork()->clean();
	 delete mesoNetwork();
	 theNetwork = NULL;
   }
   if (theGuidedRoute) {
     delete theGuidedRoute;
     theGuidedRoute = NULL;
   }
   if (theUnGuidedRoute) {
     delete theUnGuidedRoute;
     theUnGuidedRoute = NULL;
   }
   if (theVehicleList) {
	 delete theVehicleList;
	 theVehicleList = NULL;
   }
   if (theCellList) {
	 delete theCellList;
	 theCellList = NULL;
   }
   if (theODTable) {
	 delete theODTable;
	 theODTable = NULL;
   }
   if (thePathTable) {
	 delete thePathTable;
	 thePathTable = NULL;
   }
   if (theFileManager) {
	 delete theFileManager;
	 theFileManager = NULL;
   }
   if (theSimulationClock) {
	 delete theSimulationClock;
	 theSimulationClock = NULL;
   }
}
