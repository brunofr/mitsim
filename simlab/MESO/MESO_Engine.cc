//-*-c++-*------------------------------------------------------------
// MESO_Engine.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <cstring>
#include <new>
using namespace std;

#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <Tools/SimulationClock.h>
#include <Tools/Options.h>
#include <Tools/Random.h>

#include <Tools/Math.h>

#include <GRN/RN_DynamicRoute.h>
#include <GRN/RN_PathTable.h>
#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>

#include "MESO_Communicator.h"
#include "MESO_Engine.h"
#include "MESO_Setup.h"
#include "MESO_FileManager.h"
#include "MESO_Network.h"
#include "MESO_Status.h"
#include "MESO_ODTable.h"
#include "MESO_ODCell.h"
#include "MESO_VehicleTable.h"
#include "MESO_Segment.h"
#include "MESO_TrafficCell.h"
#include "MESO_Vehicle.h"
#include "MESO_Exception.h"
#include "MESO_Parameter.h"
#include "MESO_Incident.h"

#ifdef INTERNAL_GUI
#include <Xmt/Xmt.h>
#include <Xmt/Dialogs.h>
#include <DRN/DRN_DrawingArea.h>
#include <DRN/DRN_MapFeature.h>
#include <DRN/DRN_ViewMarker.h>
#include <GDS/GDS_Glyph.h>
#include "MESO_Menu.h"
#include "MESO_Interface.h"
#include "MESO_Symbols.h"
#include "MESO_DrawableVehicle.h"
#endif

MESO_Engine * theEngine = NULL;

MESO_Engine::MESO_Engine()
  : SimulationEngine(),
	batchStepSize_(10),
	simlabMsgStepSize_(60.0),
	updateStepSize_(10),
	stateStepSize_(60.0),
	pathStepSize_(ONE_DAY),	// never update
	depRecordStepSize_(5),
	rollingLength_(ONE_DAY),
	rolls_(0),
	iteration_(0)
{
  theException = new MESO_Exception("MesoTS");

  theEngine = this;  
  if (!theSimulationClock) {
	theSimulationClock = new SimulationClock;
  }

  theCommunicator = new MESO_Communicator(MSG_TAG_MESO);

  theSimulationClock->init(28800.0, 32400.0, 1.0);

  // Default parameter filename

  if (theParameter->name()) free(theParameter->name());
  *theParameter->nameptr() = strdup("mesolib.dat");
}


const char* MESO_Engine::ext()
{
  return ".meso";
}


void
MESO_Engine::init()
{
  theSimulationClock->init();

  double now = theSimulationClock->currentTime();
  batchTime_ = now;
  simlabMsgTime_ = FLT_INF;
  updateTime_ = now;
  pathTime_ = now + pathStepSize_;
  stateTime_ = now;
  tm_ = 0;

  frequency_ = 1.0 / theSimulationClock->stepSize();
  Random::create(3);
}


int
MESO_Engine::loadMasterFile()
{
  if (SimulationEngine::loadMasterFile() != 0) {
	const char *msg;
	msg = Str("Error:: Cannot find the master file <%s>.", master());
#ifdef INTERNAL_GUI
	Boolean ok;
	XmtAskForBoolean(theInterface->widget(),	// widget
					 "masterNoFound",  // query_name for customize
					 msg, // prompt_default
					 "Continue",	// yes_default
					 "Quit",	// no_default
					 NULL,	// cancel_default
					 XmtNoButton, // default_button
					 0,		// icon_type_default
					 0,		// show_cancel_button
					 &ok,		// value_return
					 "Check if the file exist.");	// help_text_default
	if (!ok) {
	  quit(STATE_ERROR_QUIT);
	}
#else
	cerr << msg << endl;
	quit(STATE_ERROR_QUIT);
#endif
	return 1;
  }

#ifdef INTERNAL_GUI
  theInterface->showBusyCursor ();
#endif

  theFileManager->readMasterFile();
  state_ = STATE_OK;

  // if (::GetUserName()) theException->exit(1);
  // ::GetUserName();

#ifdef INTERNAL_GUI
  theInterface->tellLoadingIsDone();
  theInterface->showDefaultCursor();
#endif

  return 0;
}


// This function is called every time a new master file is loaded.
// It return 0 if no error.

int
MESO_Engine::loadSimulationFiles()
{
  if (state_ == STATE_NOT_STARTED) {
	if (loadMasterFile() != 0) return 1;
  }
   
  int err_no = ToolKit::makedirs();
  if (err_no) {
#ifdef INTERNAL_GUI
	theInterface->msgSet("Cannot create directory ");
	if (err_no & 0x1) theInterface->msgAppend(ToolKit::outdir()) ;
	else if (err_no & 0x2) theInterface->msgAppend(ToolKit::workdir()) ;
#else
	theException->exit(STATE_ERROR_QUIT) ;
#endif
	return 1;
  }

#ifdef INTERNAL_GUI
  theInterface->showBusyCursor ();
#endif

  init();

  theStatus->openLogFile();

  ::ParseParameters();

  ::ParseNetwork();

  ::ParsePathTables();

  RN_DynamicRoute::parseTravelTimeTables('e');

  ::SetupMiscellaneous();

  if (theSimulationClock->startTime() <
	  theGuidedRoute->infoStartTime() ||
	  theSimulationClock->stopTime() >
	  theGuidedRoute->infoStopTime()) {
	cerr << endl
		 << "Warning:: Simulation period "
		 << theSimulationClock->startTime() << "-"
		 << theSimulationClock->stopTime() << endl
		 << "\tnot covered by time horizon of the input file"
		 << "\t<" << theGuidedRoute->filename() << ">."
		 << endl << endl;
  }

  mesoNetwork()->initializeLinkStatistics();

  start();

#ifdef INTERNAL_GUI
  theInterface->setTitle(theFileManager->title());
  theDrawingArea->setState(DRN_DrawingArea::REDRAW |
						   DRN_DrawingArea::NETWORK_LOADED);
  theDrawingArea->redraw();
  theInterface->showDefaultCursor ();
  theInterface->updateButtons();
  theInterface->handlePendingEvents();
#endif

  // Make connection to sibling modules if they exist.
   
  if (theCommunicator->isSimlabConnected()) {
#ifdef INTERNAL_GUI
	const char *msg;
	msg = XmtLocalize2(theInterface->widget(),
					   "Waiting data from other module(s).", "prompt", "waiting");
	theInterface->msgSet(msg);
#endif
	theCommunicator->makeFriends();
#ifdef INTERNAL_GUI
	msg = XmtLocalize2(theInterface->widget(),
					   "Standby", "prompt", "standby");
	theInterface->msgSet(msg);
#endif

	// Prepare the initial guidance for iterative guidance
	// generation.  OD will be parsed later.

	saveDefaultGuidance();

  } else {
	::ParseODTripTables();	// stand alone mode, parse od now
  }

  return 0;
}

// This procedure starts the simulation loops.

void MESO_Engine::run()
{
  if (canStart()) {
	loadMasterFile();
  }
 

#ifdef INTERNAL_GUI

  // In graphical mode, the simulation is started manually. When the
  // simulation is done (or cancelled), the quit() is called
  // automatically.

  theInterface->mainloop();

#else  // batch mode

  loadSimulationFiles();

  while (state_ >= 0 && (receiveMessages() > 0 ||
						 isWaiting() &&
						 receiveMessages(NO_MSG_WAITING_TIME) >= 0 ||
						 simulationLoop() >= 0)) {
	;
  }
#endif // !INTERNAL_GUI
}


void
MESO_Engine::quit(int state)
{
  theFileManager->closeOutputFiles();

  theStatus->report();

  SimulationEngine::quit(state);

  theStatus->closeLogFile();

#ifdef INTERNAL_GUI
  if (theMenu->isSaveNeeded()) {
	Boolean yes;
	XmtAskForBoolean(theInterface->widget(),
					 "isSaveNeeded", // reource querey name
					 "Save changes?", // defaul prompt
					 "Save",	// yes default
					 "Discard",	// no default
					 "Cancel",	// cancel default
					 XmtYesButton, // default button
					 0,	// default icon type
					 False, // show cancel button
					 &yes,
					 NULL);
	if (yes) {
	  theMenu->save();
	}
  }
#endif

  theException->done(0);
}


// This function is called by ::TimeOutCallback(), defined in
// <DrawableRoadNetwork/DRN_Interface.C>, in graphical mode or a
// while loop of main() in batch mode.  Graphics related functions
// should be placed inside INTERNAL_GUI block.

int
MESO_Engine::simulationLoop()
{

  // cout<< "Manish" ;
  static int first_entry = 1;
  static double depRecordTime;

  const double epsilon = 1.0E-3;
  MESO_Network * meso_network = (MESO_Network *) theNetwork;

#ifdef INTERNAL_GUI
  theInterface->handlePendingEvents();
#endif

  double now = theSimulationClock->currentTime();

  if (first_entry) {

	first_entry = 0;

	theParameter->checkVisibility();
	theFileManager->openOutputFiles();

	// This block is called only once just before the simulation gets
	// started.

	if (!theCommunicator->isTmsConnected()) {
		
	  resetBreakPoint(now);

	  meso_network->resetSegmentEmitTime();
	  const char *filename;

	  filename = MESO_Incident::filename();
	  if (ToolKit::isValidFilename(filename)) {
		filename = ToolKit::infile(filename);
		LoadIncidents(filename);
	  }

	  filename = theFileManager->stateDumpFile();
	  if (ToolKit::isValidFilename(filename)) {
		filename = ToolKit::infile(filename);
		meso_network->loadInitialState(filename);
	  }

	  if (theEngine->chosenOutput(OUTPUT_VEHICLE_DEP_RECORDS)) {
		depRecordTime = now;
	  } else {
		depRecordTime = DBL_INF;
	  }

	  ::ParseVehicleTables(now);

	  rolls_ = 1;
	  tm_ = 0;
	  if (chosenOutput(OUTPUT_STATE_3D)) {
		meso_network->save_3d_state(0, 0, theFileManager->state3d());
		stateTime_ = now + stateStepSize_;
	  } else {
		stateTime_ = DBL_INF;
	  }
	  
	} else {

	  // Update of path info is message based in simlab mode

	  pathStepSize_ = pathTime_ = DBL_INF;
	}
  }

  if (now >= theSimulationClock->masterTime()) {
	return (state_ = STATE_WAITING);
  }

#ifdef INTERNAL_GUI
  if (isTimeToBreak(now)) {

	const char* msg;
	msg = Str("Break point at %s", theSimulationClock->currentStringTime());
	XmtDisplayWarningAndAsk(theInterface->widget(), "bpsDialog",
							msg,
							"Continue", "Quit", XmtYesButton,
							"\nClick @fBContinue@fR to proceed or @fBQuit@fB to\n"
							"Terminate the simulation");

	nextBreakPoint(now + theSimulationClock->stepSize());
  }
#endif

#ifdef INTERNAL_GUI
  theInterface->handlePendingEvents();
#endif
  
  // SAVE GENERATED VEHICLE WHICH CAN BE LOADED IN LATER RUNS

  if (now >= depRecordTime) {
	MESO_Vehicle::nextBlockOfDepartureRecords();
	depRecordTime = now + depRecordStepSize_;
  }

  // UPDATE SHORTEST PATH FOR GUIDED VEHICLES

  if (now >= pathTime_) {

	// Update info network, calc shortest path, and update travel
	// time in path table. Used only in stand alone mode.

	theGuidedRoute->updatePathTable(NULL, theParameter->pathAlpha());

	// Call path choice or switching model for each guided vehicles.
	// This is temporary and assumed that info in broadcased to
	// every guided vehicle at the same time.  I used it for getting
	// evaluation result for the systems that are based on
	// prevailing traffic conditions.

	if (isSpFlag(INFO_FLAG_UPDATE_PATHS)) {
	  meso_network->guidedVehiclesUpdatePaths();
	}

	pathTime_ = now + pathStepSize_;
  }

  // UPDATE DEMAND

  if (theODTable) {

	// Update OD trip tables

	if (theODTable->nextTime() <= now) {
#ifdef INTERNAL_GUI
	  theInterface->msgPush();
#endif
	  theODTable->read();
#ifdef INTERNAL_GUI
	  theInterface->msgPop();
#endif
	}

	// Create vehicles based on trip tables
     
	theODTable->emitVehicles();
  }

  if (theVehicleTable) {

	// Read vehicles from a file if it exists.
   
	theVehicleTable->read();
  }

  // ENTER VEHICLES INTO THE NETWORK

  // Move vehicles from vitual queue into the network if they could
  // enter the network at present time.

  meso_network->enterVehiclesIntoNetwork();


  // UPDATE PHASE : Calculate the density of speeds of all TCs in the
  // network based on their current state.

  // Every update step we reset segment capacity balance to 0 to
  // prevent consumption of accumulated capacities.

  if (now >= updateTime_) {

#ifdef UPDATE_NODE_CAPACITY		// Not used in this version
	meso_network->updateNodeCapacities();
#endif

	UpdateCapacities();			// update incident related capacity

	meso_network->resetSegmentEmitTime();

	// Update densities and speeds

	meso_network->calcSegmentData();

#ifdef INTERNAL_GUI
	theStatus->showStatus();
	meso_network->drawNetwork(theDrawingArea);
	theInterface->handlePendingEvents();
#endif

	updateTime_ = now + updateStepSize_;
  }

  cout << "YYY Now" ;
  meso_network->calcTrafficCellUpSpeed();
  meso_network->calcTrafficCellDnSpeeds();

  // ADVANCE PHASE : Update position of TCs, move vehicles from TC to
  // TC and create/destroy TCs as necessary.

  meso_network->advanceVehicles();
  MESO_Vehicle::increaseCounter();

  if (now >= batchTime_) {
	theStatus->print();
	batchTime_ = now + batchStepSize_;
  }

  // Report network state

  if (now >= stateTime_) {
	tm_ ++;
	meso_network->save_3d_state(rolls_-1, tm_);
	stateTime_ = now + stateStepSize_;
  }

  // Advance the clock

  theSimulationClock->advance();

  if (now >= simlabMsgTime_) {

	// send SIMLAB a message every mininute so that the master
	// controller knows what is going on.  This is used for GUI only
	// and has nothing to do with syncronizing the process.

	theCommunicator->sendNextTimeToSimlab();

	if (theCommunicator->isTmsConnected()) {
	  theCommunicator->tms() << MSG_CURRENT_TIME
							 << now;
	  theCommunicator->tms().flush(SEND_IMMEDIATELY);
	}

	simlabMsgTime_ = now + simlabMsgStepSize_;
  }

  if (theCommunicator->isTmsConnected()) {

	if (theSimulationClock->currentTime() + epsilon > endTime_) {
	  sendResults();
	  theSimulationClock->masterTime(-DBL_INF);
	  return (state_ = STATE_WAITING);
	}
  } else if(now > theSimulationClock->stopTime() + epsilon) {
	return (state_ = STATE_DONE);
  }

  return state_ = STATE_OK;
}


// Process the messages received from other processes

int
MESO_Engine::receiveMessages(double sec)
{
  if (!theCommunicator->isSimlabConnected()) return 0;
  else return  theCommunicator->receiveMessages(sec);
}


void
MESO_Engine::saveDefaultGuidance()
{
  char *o = createFilename("ltt");
  if (theGuidedRoute->filename()) {	// make a copy
	if (ToolKit::copyFile(theGuidedRoute->filename(), o) != 0) {
	  theException->exit();
	}
  } else {						// create
	if (theNetwork->outputReloadableLinkTravelTimes(o) != 0) {
	  theException->exit();
	}
  }
  delete [] o;
}

void
MESO_Engine::sendResults()
{
  mesoNetwork()->recordLinkTravelTimeOfActiveVehicle();

  // Write the predicted link flows and travel times

  char *filename;
  filename = createFilename("lft");
  int offset = (int) (beginTime_ - theGuidedRoute->infoStartTime());
  int len = (long int) (endTime_ - beginTime_);
  int step = theGuidedRoute->infoPeriodLength();
   
  int col = offset / step;		// start periods
  int ncols = len / step;		// number of periods
  if (len % step) ncols ++;	// an extra periods
  if (col + ncols > theGuidedRoute->infoPeriods()) {
	ncols = theGuidedRoute->infoPeriods() - col;
  }
  if (ToolKit::verbose()) {
	cout << endl
		 << "Writing predicted link flows and travel times" << endl
		 << "for time period " << beginTime_ << "-" << endTime_ << endl
		 << "in <" << filename << ">" << endl;
  }
  mesoNetwork()->outputLinkFlowPlusTravelTimes(filename, col, ncols);

  delete [] filename;

  theCommunicator->tms() << MSG_MESO_DONE;
  if (chosenOutput(OUTPUT_STATE_3D)) {
	theCommunicator->tms() << theFileManager->state3d();
  } else {
	theCommunicator->tms() << "";
  }
  theCommunicator->tms().flush(SEND_IMMEDIATELY);

#ifdef INTERNAL_GUI
  theInterface->msgSet("Standby");
#endif

  theStatus->osLogFile()
	<< "Iteration " << rolls_ << '.' << iteration_
	<< " completed at " << TimeTag() << endl;
  theStatus->nMsgs(1);
}


void
MESO_Engine::restart()
{
#ifdef INTERNAL_GUI
  theDrawingArea->redraw();
#endif

  // Let the vehicle id start from 1
 
  RN_Vehicle::resetLastId();

  if (iteration_ == 1) {
	rolls_ ++;
  }
  theStatus->osLogFile()
	<< "Iteration " << rolls_ << '.' << iteration_
	<< " started at " << TimeTag() << endl;
  theStatus->nMsgs(1);

  SimulationClock *clock = theSimulationClock;
  MESO_Network *mn = (MESO_Network *)theNetwork;

  // Clean the newtork

  mn->clean();
	 
  // Clean status counters

  theStatus->clean();


  char *dmp = theEngine->createFilename("dmp");
  beginTime_ = mn->loadInitialState(dmp, 0);
  delete [] dmp;

  endTime_ = theEngine->beginTime_ + rollingLength_;

  // Set the master clock time. This makes the simulation
  // contiinue to the endTime.
	 
  clock->currentTime(beginTime_);
  clock->masterTime(endTime_);
  clock->stopTime(endTime_);

  // Schedule the initial events

  batchTime_ = beginTime_;
  simlabMsgTime_ = beginTime_;
  updateTime_ = beginTime_;
  stateTime_ = beginTime_;

  mn->resetSegmentEmitTime();
  mn->formatTrafficCells();
  mn->calcTrafficCellUpSpeed();
  mn->calcTrafficCellDnSpeeds();

  resetBreakPoint(beginTime_);

  // Calculate the dynamic shortest path based on the previous
  // simulated link travel times

  char *filename = createFilename("ltt", 0);
  theGuidedRoute->updatePathTable(filename, 1.0, beginTime_, endTime_);
  delete [] filename;

  // Clean the link travel time variables

  int offset = (int) (beginTime_ - theGuidedRoute->infoStartTime());
  int len = (long int) (endTime_ - beginTime_);
  int step = theGuidedRoute->infoPeriodLength();
   
  int col = offset / step;		// start periods
  int ncols = len / step;		// number of periods
  if (len % step) ncols ++;	// an extra periods
   
  if (col >= theGuidedRoute->infoPeriods()) {
	cerr << endl
		 << "Error:: Prediction period "
		 << beginTime_ << "-" << endTime_ << endl
		 << "\tis outside the time range "
		 << theGuidedRoute->infoStartTime() << "-"
		 << theGuidedRoute->infoStopTime() << "."
		 << endl << endl;
	theException->exit();
  } else if (col + ncols > theGuidedRoute->infoPeriods()) {
	cerr << endl 
		 << "Warning:: Prediction period "
		 << beginTime_ << "-" << endTime_ << endl
		 << "\texceeds the time range "
		 << theGuidedRoute->infoStartTime() << "-"
		 << theGuidedRoute->infoStopTime() << "."
		 << endl << endl;
	ncols = theGuidedRoute->infoPeriods() - col;
  }

  mn->resetLinkStatistics(0, theGuidedRoute->infoPeriods());

  // Reset the trip table

  ::ParseODTripTables();

  // Open the vehicle table file again.  May need to skip some
  // vehicles.

  ::ParseVehicleTables(beginTime_);

  // Read incident states
   
  filename = createFilename("inc");
  LoadIncidents(filename);
  delete [] filename;

  if (isSpFlag(INFO_FLAG_UPDATE_PATHS)) {
	mn->guidedVehiclesUpdatePaths();
  }

  if (chosenOutput(OUTPUT_STATE_3D)) {
	double t = (beginTime_ - clock->startTime()) / stateStepSize_;
	tm_ = (t < 0) ? 0 : int(t);

	// Update densities and speeds
	mn->calcSegmentData();
	
	// Save initial network state
	mn->save_3d_state(rolls_-1, tm_, theFileManager->state3d());
  } else {
	tm_ = 0;
	stateTime_ = DBL_INF;
  }

#ifdef INTERNAL_GUI
  static char *fmt = "Iteration (%d.%d) for %s.";
  char *t1 = Copy(clock->convertTime(beginTime_)) ;
  char *t2 = Copy(clock->convertTime(endTime_)) ;  
  const char *tw = Str("%s-%s", t1, t2);
  StrFree(t1);
  StrFree(t2);

  if (isDemoMode()) {
	XmtDisplayWarningMsg(theInterface->widget(), "dtaStarted",
						 fmt,
						 "Info",					// default title
						 NULL,					// default help
						 rolls_,					// args
						 iteration_,
						 tw);
  }
  theInterface->msgClear();
  XmtMsgLinePrintf(theInterface->msgline(), fmt,
				   rolls_, iteration_, tw);

#endif
}

// Save the master file

void MESO_Engine::save(ostream &os)
{
  os << "/* " << endl
	 << " * MesoTS master file" << endl
	 << " */" << endl << endl;

  os << "[Title] = \"" << theFileManager->title() << "\"" << endl
	 << endl;

  os << "[Default Parameter Directory]     = \""
	 << NoNull(ToolKit::paradir())
	 << "\"" << endl;
	 
  os << "[Input Directory]                 = \""
	 << NoNull(ToolKit::indir())
	 << "\"" << endl;

  os << "[Output Directory]                = \""
	 << NoNull(ToolKit::outdir())
	 << "\"" << endl;

  os << "[Working Directory]               = \""
	 << NoNull(ToolKit::workdir())
	 << "\"" << endl;

  os << endl;

  os << "[Parameter File]                  = \""
	 << NoNull(theParameter->name())
	 << "\"" << endl;

  os << "[Network Database File]           = \""
	 << NoNull(RoadNetwork::name())
	 << "\"" << endl;

  os << "[Trip Table File]                 = \""
	 << NoNull(OD_Table::name())
	 << "\"" << endl;
	 
  os << "[Vehicle Table File]              = \""
	 << NoNull(VehicleTable::name())
	 << "\"" << endl;

  os << "[State Dump File]                 = \""
	 << NoNull(theFileManager->stateDumpFile())
	 << "\"" << endl;

#ifdef INTERNAL_GUI
  os << "[GDS Files]                       = {" << endl
	 << "%\tFilename \tMinScale \tMaxScale" << endl;
  if (theMapFeatures) {
	GDS_Glyph *glyph;
	for (int i = 0; i < theMapFeatures->nGlyphs(); i ++) {
	  glyph = theMapFeatures->glyph(i);
	  os << "\t\"" << glyph->name() << "\""
		 << "\t" << glyph->minScale()
		 << "\t" << glyph->maxScale()
		 << endl;
	}
  }
  os << "}" << endl;
#endif

  os << "[Link Travel Times Input File]    = {" << endl
	 << "\t\"" << NoNull(RN_DynamicRoute::file(0))
	 << "\"" << "\t# Historical travel time" << endl
	 << "\t\"" << NoNull(RN_DynamicRoute::file(1))
	 << "\"" << "\t# Updated travel time" << endl
	 << "\t0x" << hex << theSpFlag << dec << "\t# SP flags" << endl
	 << "\t% 0x001 Time variant path calculation" << endl
	 << "\t% 0x002 Calulate shortest path peridically" << endl
	 << "\t% 0x004 Update path table travel time peridically" << endl
	 << "\t% 0x008 Use existing (cached) shortest path table" << endl
	 << "\t% 0x100 Updated travel time used for pretrip plan" << endl
	 << "\t% 0x200 Receives updated travel time at beacon" << endl
	 << "}" << endl;

  os << "[Incident File]                   = \""
	 << NoNull(MESO_Incident::filename())
	 << "\"" << endl;

  os << "[Path Table File]                 = \""
	 << NoNull(RN_PathTable::name())
	 << "\"" << endl;

  os << "[Network State Tag]               = \""
	 << NoNull(theFileManager->state3d_)
	 << "\"" << endl;

  os << "[LinkFlowTravelTimes Output File] = \""
	 << NoNull(theFileManager->linkFlowTravelTimesFile_)
	 << "\"" << endl;

  os << "[Link Travel Times Output File]   = \""
	 << NoNull(theFileManager->linkTravelTimesFile_)
	 << "\"" << endl;

  os << "[Vehicle File]                    = \""
	 << NoNull(theFileManager->vehicleFile_)
	 << "\"" << endl;

  os << "[Vehicle Path Record File]        = \""
	 << NoNull(theFileManager->pathRecordFile_)
	 << "\"" << endl;

  os << "[Departure Record File]           = \""
	 << NoNull(theFileManager->depRecordFile_)
	 << "\"" << endl;

  os << endl;

  os << "[Start Time]                      = "
	 << theSimulationClock->startStringTime() << endl;

  os << "[Stop Time]                       = "
	 << theSimulationClock->stopStringTime() << endl;

  os << "[Step Size]                       = "
	 << Fix(theSimulationClock->stepSize(), 0.1) << endl;

  os << endl;

  os << "[Update Step Size]                 = "
	 << Fix(updateStepSize_, 0.1) << endl;

  os << "[Simlab Msg Step Size]             = "
	 << Fix(simlabMsgStepSize_, 0.1) << endl;

  os << "[Console Message Step Size]        = "
	 << Fix(batchStepSize_, 0.1) << endl;

  os << endl;

  os << "[Output]   = 0x" << hex
	 << chosenOutput() << dec << endl
	 << "%   0x00001 = Vehicle log" << endl
	 << "%   0x00010 = Link travel times" << endl
	 << "%   0x00100 = Travel time tables" << endl
	 << "%   0x00200 = Vehicle path records" << endl
	 << "%   0x00400 = Vehicle departure record" << endl
	 << "%   0x01000 = Output rectangular text" << endl
	 << "%   0x02000 = No comments" << endl
	 << "%   0x10000 = State 3D" << endl << endl;

#ifdef INTERNAL_GUI

  os << "[Segments] = "
	 << theSymbols()->segmentColorCode().value() << endl
	 << "%   0 = Link type" << endl
	 << "%   1 = Density" << endl
	 << "%   2 = Speed" << endl
	 << "%   3 = Flow" << endl << endl;

  os << "[Signals]  = 0x" << hex
	 << theDrnSymbols()->signalTypes().value() << dec << endl
	 << "%   0x01 = Traffic signals" << endl
	 << "%   0x02 = Portal signals" << endl
	 << "%   0x04 = Variable speed limit signs" << endl
	 << "%   0x08 = Variable message signs" << endl
	 << "%   0x10 = Lane use signs" << endl
	 << "%   0x20 = Ramp meters" << endl << endl;

  os << "[Sensor Types] = 0x" << hex
	 << theSymbols()->sensorTypes().value() << dec << endl
	 << "%   0x1 = Loop detectors" << endl
	 << "%   0x2 = AVI sensors" << endl
	 << "%   0x4 = Area sensors" << endl << endl;

  os << "[Sensor Color Code]  = "
	 << theSymbols()->sensorColorCode().value() << endl
	 << "%   0 = Count" << endl
	 << "%   1 = Flow" << endl
	 << "%   2 = Speed" << endl
	 << "%   3 = Occupancy" << endl << endl;

  os << "[Vehicles] = "
	 << theSymbols()->vehicleBorderCode().value() << endl
	 << "%   0 = None" << endl
	 << "%   1 = Vehicle type" << endl
	 << "%   2 = Information availability" << endl
	 << "%   3 = Turning movement" << endl << endl;

  os << "[Vehicle Shade Params] = {" << endl
	 << "\t" << theVehicleShadeParam[0]
	 << "\t# Shade" << endl
	 << "\t" << theVehicleShadeParam[1]
	 << "\t# Outstanding time in a segment" << endl
	 << "\t" << theVehicleShadeParam[2]
	 << "\t# Outstanding time in the network" << endl
	 << "}" << endl;

#ifdef INTERNAL_GUI
  DRN_ViewMarker::SaveMarkers(os) ;
#endif

  os << endl;
   
  theOptions->printOptions(os);

#endif
}
