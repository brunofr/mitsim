//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_Node.C
// DATE: Mon Feb 26 17:34:36 1996
//--------------------------------------------------------------------

#include <Tools/Math.h>

#include <Tools/Random.h>
#include <Tools/SimulationClock.h>

#include <GRN/RN_PathTable.h>

#include "MESO_Network.h"
#include "MESO_Node.h"
#include "MESO_Link.h"
#include "MESO_Segment.h"
#include "MESO_VehicleList.h"
#include "MESO_Vehicle.h"
#include "MESO_TrafficCell.h"
#include "MESO_Parameter.h"
#include "MESO_Status.h"
#include "MESO_Engine.h"

const short int MNMS = MAX_NUM_OF_IN_LINKS * MAX_NUM_OF_OUT_LINKS;
short int * MESO_Node::demand_ = new short int [MNMS];

MESO_Node::MESO_Node() :
#ifdef INTERNAL_GUI
   DRN_Node()
#else
RN_Node()
#endif
{
}


MESO_Node::~MESO_Node()
{
   delete [] supply_;
}


void
MESO_Node::calcStaticInfo()
{
#ifdef INTERNAL_GUI
   DRN_Node::calcStaticInfo();
#else
   RN_Node::calcStaticInfo();
#endif

   // Remaining capacity for each turning movement

   supply_ = new short int [nUpLinks() * nDnLinks()];
}


// Move vehicles from upstream links to the virtual queue of this
// node.

void
MESO_Node::calcCapacities()
{
   calcDemands();
   calcSupplies();
}


// Calculate the number of vehicles that may arrive at this node if
// the capacities are not constrained.  The results are stored in a
// static array "demand_".

void
MESO_Node::calcDemands()
{
   short int i, j, n = nUpLinks(), m = nDnLinks();
   RN_Link *link;
   MESO_Segment *ps;
   MESO_TrafficCell *cell;
   MESO_Vehicle *vehicle;
   float dt = theEngine->updateStepSize();
   for (i = 0, j = n * m; i < j; i ++) {
      demand_[i] = 0;
   }

   for (i = 0; i < n; i ++) {
      ps = (MESO_Segment *)upLink(i)->endSegment();
      cell = ps->firstCell();
      while (cell) {
	 vehicle = cell->firstVehicle();
	 while (vehicle) {
	    link = vehicle->nextLink();
		float pos = vehicle->distance() - vehicle->currentSpeed() * dt;
	    if (link &&	pos <= 0.0) {
	       j = link->dnIndex();
	       demand_[i * m + j] ++;
	    }
	    vehicle = vehicle->trailing();
	 }
	 cell = cell->trailing();
      }
   }
}


// Given the demands for each turning movement, this function
// calculates the number of vehicles that can pass this node for each
// turnning movement.  The result is stored in a static array
// "supply_".

void
MESO_Node::calcSupplies()
{
   short int num_ups = nUpLinks();
   short int num_dns = nDnLinks();
   short int i, j, k;

   // Capacity for a particular movement may depends on the geometry,
   // flows of all the turning movements, especially the conflict
   // movements.

   // WE MAY USE HCM MODEL. WILL BE CODED LATER.

   for (i = 0; i < num_ups; i ++) {
      for (j = 0; j < num_dns; j ++) {
	 k = i * num_dns + j;
	 if (((MESO_Link *)dnLink(j))->isJammed()) {
	    supply_[k] = 0;
	 } else {
	    supply_[k] = 100;
	 }
      }
   }
}
