//-*-c++-*------------------------------------------------------------
// NAME: Class for communication to MITSIM and MesoTS.
// AUTH: Qi Yang
// FILE: MESO_Communicator.h
// DATE: Sun Nov 19 12:33:35 1995
//--------------------------------------------------------------------

#ifndef MESO_COMMUNICATOR_HEADER
#define MESO_COMMUNICATOR_HEADER

#include <IO/Communicator.h>

class MESO_Communicator : public Communicator
{
   protected:

      IOService tms_;

   public:

      MESO_Communicator(int sndtag);
      ~MESO_Communicator() { }
      
      inline int isTmsConnected() {
		 return tms_.isConnected();
      }
      
      IOService& tms() { return tms_; }

      void makeFriends(); // virtual
      int receiveMessages(double sec = 0.0);	// virtual

      int receiveSimlabMessages(double sec = 0.0);
      int receiveTmsMessages(double sec = 0.0);

      double batchStepSize();	// virtual
	  void sendNextTimeToSimlab();	// virtual

	  char* odFile() { return odFile_; }

   private:
	  char *odFile_;
};

extern MESO_Communicator* theCommunicator;

#endif
