//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_CellList.h
// DATE: Tue Feb 27 11:37:18 1996
//--------------------------------------------------------------------

#ifndef MESO_CELLLIST_HEADER
#define MESO_CELLLIST_HEADER

class MESO_TrafficCell;

class MESO_CellList
{
  public:

	MESO_CellList();
	~MESO_CellList();

	void recycle(MESO_TrafficCell *); /* put a vehicle into the list */
	MESO_TrafficCell* recycle(); /* get a vehicle from the list */

	inline MESO_TrafficCell* head() { return head_; }
	inline MESO_TrafficCell* tail() { return tail_; }
	inline int nCells() { return nCells_; }
	inline int nPeakCells() { return nPeakCells_; }

  private:

	MESO_TrafficCell *head_;
	MESO_TrafficCell *tail_;
	int nCells_;		/* number of vehicles */
	int nPeakCells_;	/* max number of vehicles */
};

extern MESO_CellList *theCellList; /* recyclable vehicles */

#endif

