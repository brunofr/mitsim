//-*-c++-*------------------------------------------------------------
// NAME: Traffic Simulation
// NOTE: Force instantiation of my template classes
// AUTH: Qi Yang
// FILE: fiTemplates.C
// DATE: Mon Dec 23 13:13:12 1996
//--------------------------------------------------------------------

#ifdef FORCE_INSTANTIATE

#include <Templates/Listp.h>
#include "MESO_Incident.h"

#ifdef __sgi

#pragma instantiate Listp<MESO_Incident*>
#pragma instantiate ListpNode<MESO_Incident*>

#endif // __sgi

#ifdef __GNUC__

template class Listp<MESO_Incident*>;
template class ListpNode<MESO_Incident*>;

#endif  // __GNUC__

#endif


