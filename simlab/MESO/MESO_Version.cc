//-*-c++-*------------------------------------------------------------
// NAME: Simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_Version.C
// DATE: Mon Feb 12 09:01:52 1996
//--------------------------------------------------------------------

#include <iostream>
#include <new>
using namespace std;

#include <Tools/ToolKit.h>

#include "MESO_Exception.h"
#include "MESO_Version.h"

char*  g_majorVersionNumber = "1";
char*  g_minorVersionNumber = "0 Beta";
char*  g_codeDate = "1996";

void
Welcome(char *exec)
{
   theExec = Copy(ToolKit::RealPath(exec));

   cout << "MesoTS Release "
	<< g_majorVersionNumber << "." << g_minorVersionNumber << endl
	<< "Copyright (c) " << g_codeDate << endl
	<< "Massachusetts Institute of Technology" << endl
	<< "All Right Reserved" << endl << endl;

#ifndef FINAL_VERSION
   PrintVersionInfo();
#endif

   cout.flush();

   set_new_handler(::FreeRamException);
}
