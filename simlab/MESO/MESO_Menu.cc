//-*-c++-*------------------------------------------------------------
// MESO_Menu.cc
//
// qiyang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <fstream>
#include <cassert>
using namespace std;

#include <Xmt/Xmt.h>
#include <Xmt/Create.h>
#include <Xmt/Menu.h>
#include <Xmt/Dialogs.h>
#include <Xmt/Dialog.h>

#include <IO/Exception.h>
#include <Tools/ToolKit.h>
#include <Tools/Random.h>

#include <Xmw/XmwDialogManager.h>
#include <Xmw/XmwRandomizer.h>
#include <DRN/DRN_PathDialog.h>

#include "MESO_Menu.h"
#include "MESO_Interface.h"
#include "MESO_Network.h"
#include "MESO_Symbols.h"
#include "MESO_VehicleDialog.h"
#include "MESO_OutputDialog.h"
#include "MESO_SetupDialog.h"
#include "MESO_AdvancedSetupDialog.h"

MESO_Menu::MESO_Menu(Widget parent) : DRN_Menu(parent)
{
  Widget popup = XmtNameToWidget(parent, "*drawingArea*popup");

  Widget w;

  // Simulation pane

  w = getItemWidget("Setup");
  assert (w);
  addCallback(w,
			  XmNactivateCallback, &MESO_Menu::setup,
			  NULL );

  w = getItemWidget("AdvancedSetup");
  assert (w);
  addCallback(w,
			  XmNactivateCallback, &MESO_Menu::advancedSetup,
			  NULL );

  w = getItemWidget("Output");
  assert (w);
  addCallback(w,
			  XmNactivateCallback, &MESO_Menu::output,
			  NULL );

  // View pane
  
  w = getItemWidget("Vehicle");
  assert (w);
  addCallback(w,
			  XmNactivateCallback, &MESO_Menu::vehicle,
			  NULL );
  if (popup && (w = getItemWidget(popup, "Vehicle"))) {
	addCallback(w,
				XmNactivateCallback, &MESO_Menu::vehicle,
				NULL );
  }

  // Tools pane

  w = getItemWidget("BrowsePath");
  assert (w);
  XtAddCallback(w,
			  XmNactivateCallback, &MESO_Menu::browsePath,
			  NULL );

  if (popup && (w = getItemWidget(popup, "BrowsePath"))) {
	XtAddCallback(w,
				  XmNactivateCallback, &MESO_Menu::browsePath,
				  NULL );
  }
}


// Static functions

void MESO_Menu::browsePath ( Widget, XtPointer, XtPointer )
{
   static DRN_PathDialog dialog(theDrnInterface->widget());
   dialog.post();
}

// Overloaded virtual functions

void MESO_Menu::randomizer ( Widget, XtPointer, XtPointer )
{
  static XmwRandomizer dialog(theInterface->widget(),3);
  dialog.post();
}

void MESO_Menu::setup ( Widget, XtPointer, XtPointer )
{
   static MESO_SetupDialog dialog(theInterface->widget());
   dialog.post();
}

void MESO_Menu::advancedSetup ( Widget, XtPointer, XtPointer )
{
   static MESO_AdvancedSetupDialog dialog(theInterface->widget());
   dialog.post();
}

void MESO_Menu::output ( Widget, XtPointer, XtPointer )
{
   static MESO_OutputDialog dialog(theInterface->widget());
   dialog.post();
}

void MESO_Menu::vehicle ( Widget, XtPointer, XtPointer)
{
   static MESO_VehicleDialog dialog(theInterface->widget());
   dialog.post();
}

#endif // INTERNAL_GUI
