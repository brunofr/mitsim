//-*-c++-*------------------------------------------------------------
// NAME: Exception handler
// AUTH: Qi Yang
// FILE: MESO_Exception.h
// DATE: Sun Mar 17 14:36:47 1996
//--------------------------------------------------------------------

#ifndef MESO_EXCEPTION_HEADER
#define MESO_EXCEPTION_HEADER

#include <IO/Exception.h>

class MESO_Exception : public Exception
{
   public:

      MESO_Exception(const char *name) : Exception(name) { }
      ~MESO_Exception() { }

      void exit(int code = 0);
      void done(int code = 0);

   private:

      void cleanup();
};

#endif
