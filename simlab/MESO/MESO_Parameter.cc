//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_Parameter.C
// DATE: Mon Feb 12 09:40:38 1996
//--------------------------------------------------------------------


#include <iostream>
#include <new>
using namespace std;

#include <Tools/Math.h>

#include <Tools/ToolKit.h>
#include <Tools/Random.h>
#include <Tools/GenericVariable.h>
#include <Tools/VariableParser.h>

#ifdef INTERNAL_GUI
#include <Xmw/XmwFont.h>
#include <DRN/DRN_DrawingArea.h>
#endif

#include "MESO_Parameter.h"
#include "MESO_Exception.h"
#include "MESO_Engine.h"
#include "MESO_Link.h"

const float ETC_RATE		= 0.4;
const float HOV_RATE		= 0.2;

const float VEHICLE_LENGTH	= 6.0960; // 20 feet

const float CELL_RSP_LOWER	= 60.0;	// about 200 feet
const float CELL_RSP_UPPER	= 150.0;	// about 500 feet
const float CHANNELIZE_DISTANCE= 120.0; // about 400 feet

MESO_Parameter * theParameter = NULL;

MESO_Parameter::MESO_Parameter()
   : Parameter(),
     nVehicleClasses_(1),
     vehicleClassCDF_(new float [1]),
     vehicleLength_(new float [1]),
     vehicleName_(new char * [1]),
     etcRate_(ETC_RATE),
     hovRate_(HOV_RATE),
     rspLower_(CELL_RSP_LOWER),
     rspUpper_(CELL_RSP_UPPER),
	 cellSplitGap_(0.5 * (CELL_RSP_LOWER + CELL_RSP_UPPER)),
     channelizeDistance_(CHANNELIZE_DISTANCE),
	 limitingParam_(NULL),
	 queueParam_(NULL)
{
   vehicleClassCDF_[0] = 1.0;
   vehicleLength_[0] = VEHICLE_LENGTH;
   vehicleName_[0] = Copy("Cars");
}


float
MESO_Parameter::vehicleLength(short int i)
{
   if (i >= 0 && i < nVehicleClasses_)
      return vehicleLength_[i];
   else
      return vehicleLength_[0];
}


// This returns the minimum distance gap for a given speed

float
MESO_Parameter::minGap(float speed)
{
   return minHeadwayGap() + headwaySpeedSlope() * speed;
}

// This returns a maximum speed for a give gap, in a n-lane segment

float
MESO_Parameter::maxSpeed(float gap, int n)
{
   float dt = theSimulationClock->stepSize() + headwaySpeedSlope() / n;
   float dx = gap - minHeadwayGap() / n;
   float v = dx / dt;
   if (v > 40) return 40;
   return (v > 0.0) ? v : 0.0;
}


void
MESO_Parameter::load()
{
   const char * filename = ToolKit::optionalInputFile(name_,ToolKit::indir());
   if (!filename) theException->exit(1);
	VariableParser vp(this);
	vp.parse( filename );
}


int
MESO_Parameter::parseVariable(GenericVariable &gv)
{
   if (Parameter::parseVariable(gv) == 0) return 0;

   char * token = compact(gv.name());

   if (isEqual(token, "ETCRate")) {
      etcRate_ = gv.element();
   } else if (isEqual(token, "HOVRate")) {
      hovRate_ = gv.element();

   } else if (isEqual(token, "VehicleClasses")) {
      return loadVehicleClassParas(gv);

   } else if (isEqual(token, "limitingParameters")) {
	  return loadLimitingParameters(gv);

   } else if (isEqual(token, "QueueDispatch")) {
	  return loadQueueDispatchParameters(gv);

   } else if (isEqual(token, "CellResponseLowerBound")) {
      rspLower_ = gv.element();
      rspLower_ *= lengthFactor_;
     
   } else if (isEqual(token, "CellResponseUpperBound")) {
      rspUpper_ = gv.element();
      rspUpper_ *= lengthFactor_;

   } else if (isEqual(token, "CellSplitGap")) {
      cellSplitGap_ = gv.element();
      cellSplitGap_ *= lengthFactor_;

   } else if (isEqual(token, "ChannelizeDistance")) {
      channelizeDistance_ = gv.element();
      channelizeDistance_ *= lengthFactor_;

   } else if (isEqual(token, "FontSizes")) {
#ifdef INTERNAL_GUI
	  return theDrawingArea->loadScalableFontSizes(gv);
#else
	  return 0;
#endif

   } else {
      return 1;
   }
   return 0;
}


/*
 * Read parameter on vehicle mix
 */

int
MESO_Parameter::loadVehicleClassParas(GenericVariable &gv)
{
   const int num = 3;
   float pdf, cdf = 0.0;

   nVehicleClasses_ = gv.nElements() / num;
   vehicleClassCDF_ = new float [nVehicleClasses_];
   vehicleLength_ = new float [nVehicleClasses_];
   vehicleName_ = new char * [nVehicleClasses_];

#ifdef INTERNAL_GUI
   vehicleColors_ = new Pixel [nVehicleClasses_];
#endif

   float len;

   for (int i = 0; i < nVehicleClasses_; i ++) {
      vehicleName_[i] = Copy((const char *)gv.element(i * num));
      len = gv.element(i * num + 1);
      vehicleLength_[i] = lengthFactor_ * len;
      pdf = gv.element(i * num + 2);
      cdf += pdf;
      vehicleClassCDF_[i] = cdf;

#ifdef INTERNAL_GUI
      vehicleColors_[i] = theColorTable->color(
		 (float) (i + 1) /
		 (float) nVehicleClasses_);
#endif

   }

   if (!AproxEqual(cdf, (float)1.0)) {
      cerr << "Error:: Sum of vehicle mix ("
		   << vehicleClassCDF_[nVehicleClasses_-1]
		   << ") does not equal to 1.0." << endl;
      return -1;
   }
   return 0;
}


int
MESO_Parameter::loadLimitingParameters(GenericVariable &gv)
{
   int i, n = gv.nElements();
   if (n != 3) {
	  cerr << "Error:: 3 parameters expected, "
		   << n << " found." << endl;
	  return -1;
   }

   limitingParam_ = new float [n];
   for (i = 0; i < n; i ++) {
	  limitingParam_[i] = gv.element(i);
   }

   // 0 = min headway
   limitingParam_[0] *= lengthFactor_;

   // 1 = headway/speed slope (sec)

   // 2 = min speed
   limitingParam_[2] *= speedFactor_;

   return 0;
}


int
MESO_Parameter::loadQueueDispatchParameters(GenericVariable &gv)
{
   int i, n = gv.nElements();
   if (n != 3) {
	  cerr << "Error:: 3 parameters expected, "
		   << n << " found." << endl;
	  return -1;
   }
   
   queueParam_ = new float [n];
   for (i = 0; i < n; i ++) {
	  queueParam_[i] = gv.element(i);
   }

   // 0 = coef wrt time since queue start to dispatch

   if (queueParam_[0] > 0) {
	  queueParam_[0] = -queueParam_[0];
   }

   // 1 = queue releasing speed

   queueParam_[1] *= speedFactor_;

   // 2 = maximum time
   
   return 0;
}


float
MESO_Parameter::queueReleasingSpeed(float t, float v_f)
{
   if (t > queueParam_[2]) return v_f;

   float r = 1.0 - exp(queueParam_[0] * t * t);
   return queueParam_[1] + (v_f - queueParam_[1]) * r;
}
