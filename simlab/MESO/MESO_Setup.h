//-*-c++-*------------------------------------------------------------
// NAME: Microcscopic Traffic Simulator
// AUTH: Qi Yang
// FILE: MESO_Setup.h
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#ifndef MESO_SETUP_HEADER
#define MESO_SETUP_HEADER

extern void ParseParameters();

extern void ParseNetwork();
extern void ParsePathTables();
extern void ParseODTripTables();
extern void ParseVehicleTables(double start_time);
extern void SetupMiscellaneous();

#endif
