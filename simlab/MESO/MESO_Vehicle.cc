//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_Vehicle.C
// DATE: Mon Feb 12 10:56:04 1996
//--------------------------------------------------------------------

#include <Tools/SimulationClock.h>
#include <Tools/Random.h>
#include <Tools/Reader.h>
#include <Tools/ToolKit.h>

#include <IO/Exception.h>
#include <Tools/Math.h>

#include <GRN/RN_Path.h>
#include <GRN/RN_PathTable.h>
#include <GRN/RN_Route.h>
#include <GRN/RN_Lane.h>

#ifdef INTERNAL_GUI
#include <DRN/DRN_DrawingArea.h>
#endif

#include "MESO_Network.h"
#include "MESO_VehicleList.h"
#include "MESO_Vehicle.h"
#include "MESO_Node.h"
#include "MESO_Link.h"
#include "MESO_Segment.h"
#include "MESO_CellList.h"
#include "MESO_TrafficCell.h"
#include "MESO_Parameter.h"
#include "MESO_ODCell.h"
#include "MESO_Status.h"
#include "MESO_Engine.h"
#include "MESO_FileManager.h"


unsigned int MESO_Vehicle::stepCounter_ = 0;
const char *LOG_FILE_HEADER_MSG =
"Trips not completed\nID\tTime\tOD\tSegment\tNextLink";
const char *PATH_ERROR_MSG =
"Too many incomplete trips! I will stop reporting now rather\nthan crash your file system.";

MESO_Vehicle::MESO_Vehicle()
  : RN_Vehicle(),
	trafficCell_(NULL),
	leading_(NULL),
	trailing_(NULL)
{
}

RN_Link*
MESO_Vehicle::link()
{
  return trafficCell_ ? trafficCell_->link() : (RN_Link*) NULL;
}


RN_Segment*
MESO_Vehicle::segment()
{
  return trafficCell_ ? trafficCell_->segment() : (RN_Segment*) NULL;
}


void
MESO_Vehicle::changeRoute()
{
  if (!(isSpFlag(INFO_FLAG_VMS_BASED))) {
    // These bit is usuallly set when it view a ENROUTE_VMS.  Here we
    // bypass the requirement for location based information access if
    // the system is not VMS/Beacon based.
    setAttr(ATTR_ACCESSED_INFO);
  }
  if (link()) {	// alreay in the network
    OnRouteChoosePath(link()->dnNode());
  } else {	// in spillback queue
    OnRouteChoosePath(nextLink_->upNode());
  }
}

// This function is called when the parser read a vehicle from the
// vehicle table file.  This is an alternative method to load vehicles
// into the network (other than OD trip tables)

int
MESO_Vehicle::init(int id, int ori, int des, int type_id, int path_id)
{
  int error = RN_Vehicle::init(id, ori, des, type_id, path_id);
  if (error < 0) return 1;	// show a warning msg
  enterPretripQueue();
  return 0;
}


// After a vehicle is generated for an OD pair, we calculate its
// pre-trip path and append it to the end of the pretrip queue of the
// first link on its path.  This fucntion returns 0 if no error.

void
MESO_Vehicle::enterPretripQueue()
{
  static int error_quota = 100;

  if (!nextLink_) {			// No path	 
	if (!theStatus->nErrors()) {
	  theStatus->osLogFile() << LOG_FILE_HEADER_MSG << endl;
	}
	if (error_quota > 0) {
	  theStatus->osLogFile() 
		<< code_ << "\t"
		<< theSimulationClock->currentTime() << "\t"
		<< oriNode()->code() << "-" << desNode()->code() << "\t" 
		<< endl;
	} else if (error_quota == 0) {
	  theStatus->osLogFile()
		<< endl << PATH_ERROR_MSG
		<< endl;
	}
	error_quota --;
	theStatus->nErrors(+1);
	theVehicleList->recycle(this);
	return;
  }
  nextMesoLink()->queue(this);
  trafficCell_ = NULL;
  float spd = ((MESO_Segment*)nextLink_->startSegment())->maxSpeed();
  distance_ = -spd * theSimulationClock->stepSize();
}


// This function is called by init()

void
MESO_Vehicle::initialize()		// vitual
{
  flags_ = 0;

  int prefix = type_ & (~VEHICLE_CLASS); // prefix, e.g., HOV
  int vehicle_class = (type_ & VEHICLE_CLASS);

  Random* theRandomizer = theRandomizers[Random::Departure];

  if (vehicle_class == 0) {
	vehicle_class = theRandomizer->drandom(theParameter->nVehicleClasses(),
										   theParameter->vehicleClassCDF());
  }
  type_ = vehicle_class;

  // Attach some extra bits to "type" based on given probabilities if
  // the prefix is not specified

  if (prefix) {
	type_ |= prefix; 
  } else {
	if (theRandomizer->brandom(theParameter->etcRate())) {

	  // This implementation treats all ETC vehicles as AVI
     
	  type_ |= (VEHICLE_ETC | VEHICLE_PROBE);
	}

	if (theRandomizer->brandom(theParameter->guidedRate())) {
	  type_ |= VEHICLE_GUIDED;
	}

	if (theRandomizer->brandom(theParameter->hovRate())) {
	  type_ |= VEHICLE_HOV;
	}
  }
  mileage_ = 0.0;
}


// Enter the vehicle into the network if the space is available

int
MESO_Vehicle::enterNetwork(void)
{
  if (nextMesoLink()->isJammed()) return 0;
   
  // Delete this vehicle from the link queue.
     
  nextMesoLink()->dequeue(this);

  nextMesoLink()->append(this);
  OnRouteChoosePath(nextLink_->dnNode());
  updateSpeed();

  theStatus->nActive(1);
   
  return 1;
}


// This function is called by MESO_TrafficCell::append() to add a
// vehicle to the end of traffic cell

void MESO_Vehicle::appendTo(MESO_TrafficCell *cell)
{
  MESO_Segment *ps = cell->segment();
  trafficCell_ = cell;
  calcSpaceInSegment();
  distance_ += ps->length();
  currentSpeed_ = cell->tailSpeed();
  
  // Make sure not crash into front vehicle

  if (leading_) {				// no overtaking
	float pos = leading_->upPos() +
	  theParameter->minHeadwayGap() / ps->nLanes();
	distance_ = Max(distance_, pos);
  }

  if (stepCounter_ & 0x1) {
	setFlag(FLAG_PROCESSED);
  }
  markAsProcessed();
}


void
MESO_Vehicle::calcSpaceInSegment()
{
  spaceInSegment_ = length_ / segment()->nLanes();
}

// Calculate the speed of this vehicle by interpolation of up and dn
// speeds of traffic cell.

void
MESO_Vehicle::updateSpeed()
{
  short int i;

  if (trafficCell_->nHeads_ > 1 &&
	  nextLink_ && segment()->isHead()) {
	i = nextLink_->dnIndex();
  } else {
	i = 0;
  }

  float headspd = trafficCell_->headSpeed(i);
  float headpos = trafficCell_->headPosition(i);

  if (distance_ <= headpos) {
	currentSpeed_ = headspd;
  } else if (distance_ >= trafficCell_->tailPosition()) {
	currentSpeed_ = trafficCell_->tailSpeed();
  } else {
	float dx = trafficCell_->tailPosition() - headpos;
	if (dx < 0.1) {
	  currentSpeed_ = headspd;
	} else {
	  float dv = (trafficCell_->tailSpeed() - headspd) / dx;
	  currentSpeed_ = headspd + dv * (distance_ -  headpos);
	}
  }

  // This avoid large gaps in queue stream

  if (currentSpeed_ < theParameter->minSpeed() &&
	  gapDistance(leading_) > theParameter->minHeadwayGap()) {
	currentSpeed_ = theParameter->minSpeed();
  }
}


float
MESO_Vehicle::gapDistance(MESO_Vehicle* front)
{
  if (!front) return FLT_INF;
   
  MESO_Segment *ps = front->trafficCell_->segment();
  if (trafficCell_->segment() == ps) { // same segment
	return distance_ - front->upPos();
  } else {						// different segment
	float gap = distance_ + (ps->length() - front->upPos());
	return gap;
  }
}



// Current gap from the leading vehicle in the same traffic stream

MESO_Vehicle*
MESO_Vehicle::leadingVehicleInStream()
{
  float dndis = distanceFromDownNode();

  if (dndis > theParameter->channelizeDistance()) {
	return leading_;
  }

  MESO_Vehicle * front = leading_;

  // Find the first vehicle in the same traffic stream that goes
  // to the same direction as this vehicle

  while (front &&
		 front->nextLink_ != nextLink_) {
	front = front->leading_;
  }

  return front;
}


// Advance this vehicle based on current cell state.  This vehicle may
// moves to a new position in current segment, a downstream segment,
// or be removed from the network (when it arrives the destination)

void
MESO_Vehicle::move()
{
#ifdef OUT
#ifdef sgi

  // There is a strange bug that I have not figured out after a week
  // of effort. This is a dirty fix and it works only on sgi.

  if (!finite(distance_) || !finite(currentSpeed_)) {
	theStatus->osLogFile() 
	  << "Vehicle " << code_ << " ("
	  << oriNode()->code() << "-"
	  << desNode()->code() << ") removed at "
	  << theSimulationClock->currentTime() << " from "
	  << link()->code() << ":" << segment()->code() << endl;
#ifdef INTERNAL_GUI
	erase(theDrawingArea);
#endif
	theStatus->nErrors(+1);
	theStatus->nNoPath(+1);
	theStatus->nActive(-1);
	trafficCell_->remove(this);
	theVehicleList->recycle(this);
	return;
  }
#endif
#endif // OUT

  // Prevent to be processed twice if it moved into a new link

  markAsProcessed();

  // Leading vehicle in the same traffic stream

  MESO_Vehicle * front = leadingVehicleInStream();

  float oldpos = distance_;
  int gone = 0;

  // Calculate the current speed

  updateSpeed();

  // Position at the end of this interval

  distance_ -= currentSpeed_ * theSimulationClock->stepSize();

  if (front) {					// no overtaking
	float pos = front->upPos() + 
	  theParameter->minHeadwayGap() / segment()->nLanes();
	distance_ = Max(distance_, pos);
  }

  if (distance_ < 0.0) {		// cross segment

	if ((gone = transpose()) == 0) { // can not moved out
	  distance_ = 0.0;		// the maximum it can move
	}
  }

  if (gone < 0) {				// removed from the nextwork
	return;
  } else if (gone == 0) {		// still in the same segment
	if (distance_ >= oldpos) { // not moved
	  currentSpeed_ = 0.0;
	  return ;				// no need to enter advance()
	} else {
	  currentSpeed_ = (oldpos - distance_) * theEngine->frequency();
	}
  } else {						// moved into a downstream segment
	currentSpeed_ = (oldpos + segment()->length()-distance_) *
	  theEngine->frequency();
  }   

  advance();					// sort position in list
}


// Remove this vehicle or move it into the downstream segment or link.
// Requires: distance_ <= 0. It return 1 if the vehicle is successfuly
// moved into the downstream segment, -1 if the vehicle is removed
// from the network, and 0 otherwise (downstream segment is jammed).

int
MESO_Vehicle::transpose()
{
  static int error_quota = 100;

  MESO_Segment *ps = mesoSegment();

  if (!ps->isMoveAllowed()) return 0;
  mileage_ += ps->length();	// record mileage

  int done = 0;
  MESO_Segment *ds = (MESO_Segment*)segment()->downstream();

  if (ds) {

	if (!ds->isJammed()) {
	  trafficCell_->remove(this);
	  ds->append( this );
	  done = 1;
	}

  } else if (nextLink_) {	// cross link

	if (!nextMesoLink()->isJammed()) {

	  if (theEngine->chosenOutput(OUTPUT_VEHICLE_PATH_RECORDS)) {
		writePathRecord(theFileManager->osPathRecord());
	  }
	  mesoLink()->recordTravelTime(this);

	  // Estimate the time this vehicle enter the link

	  float dt;
	  if (currentSpeed_ > 1.0) {
		dt =  distance_ / currentSpeed_;
	  } else {
		dt = 0.5 * theSimulationClock->stepSize();
	  }
	  timeEntersLink_ = theSimulationClock->currentTime() - dt;

	  trafficCell_->remove(this);

	  if (theNetwork->isNeighbor(link(), nextLink_)) {
		nextMesoLink()->append( this );
		OnRouteChoosePath(nextLink_->dnNode());
		done = 1;
	  } else {
		if (!theStatus->nErrors()) {
		  theStatus->osLogFile() << LOG_FILE_HEADER_MSG << endl;
		}

		if (error_quota > 0) {
		  theStatus->osLogFile()
			<< code_ << "\t"
			<< theSimulationClock->currentTime() << "\t"
			<< oriNode()->code() << "-" << desNode()->code() << "\t" 
			<< segment()->code();
		} else if (error_quota == 0) {
		  theStatus->osLogFile()
			<< endl << PATH_ERROR_MSG
			<< endl;
		}
		error_quota --;

		theStatus->nErrors(1);
		theStatus->nNoPath(1);
		mesoLink()->recordTravelTime(this);
		removeFromNetwork();
		done = -1;
	  }
	}

  } else {			// arrive destination

	mesoLink()->recordTravelTime(this);
	removeFromNetwork();
	done = -1;
  }

  if (done) {
	ps->scheduleNextEmitTime();
#ifdef INTERNAL_GUI
	erase(theDrawingArea);
#endif  
	if (done > 0) {
	  calcSpaceInSegment();
	}
  }

  return done;
}


// Called when a vehicle arrives its destinations

void
MESO_Vehicle::removeFromNetwork()
{
  if (theEngine->chosenOutput(OUTPUT_VEHICLE_PATH_RECORDS)) {
	writePathRecord(theFileManager->osPathRecord());
  }

  report();

#ifdef INTERNAL_GUI
  erase(theDrawingArea);
#endif

  trafficCell_->remove(this);
  theVehicleList->recycle(this);
  theStatus->nActive(-1);
  theStatus->nArrived(1);
}


// Report the travel time, etc

void
MESO_Vehicle::report()
{
  MESO_Parameter& p = *theParameter;

  static int heading = 1;
  float t = theSimulationClock->currentTime() - departTime_;
  ostream& os = theFileManager->osVehicle();
  if (heading) {
	if (!theEngine->skipComment()) {
	  os << "# ID/Type/Ori/DepDes/ArrDes/DepTime/";
	  os << "ArrTime/Mileage/Speed" << endl;
	}
	heading = 0;
  }

  int actual_des_node = link()->dnNode()->code();

  os << code_ << endc
	 << type_ << endc
	 << oriNode()->code() << endc
	 << desNode()->code() << endc
	 << actual_des_node << endc
	 << Fix(departTime_, (float)1.0) << endc
	 << Fix(theSimulationClock->currentTime(), 1.0) << endc
	 << Fix(mileage_ / p.lengthFactor(), (float)1.0) << endc
	 << Fix((mileage_ / t) / p.speedFactor(), (float)0.1) << endl;
}


// Advance the vehicle to a position in the vehicle list that
// corresponding to its current value of "distance_".  This function
// is invoked when a vehicle is moved (including moved into a
// downstream segment), so that the vehicles in macro vehicle list is
// always sorted by their position.

void
MESO_Vehicle::advance()
{
  // (0) Check if this vehicle should be advanced in the list 

  if (!leading_ ||
	  distance_ >= leading_->distance_) {

	// no need to advance this vehicle in the list 

	return;
  }


  // (1) Find the vehicle's position in the list  

  MESO_Vehicle* front = leading_;

  while (front && distance_ < front->distance_) {
	front = front->leading_;
  }


  // (2) Take this vehicle out from the list 

  leading_->trailing_ = trailing_;
	
  if (trailing_) {
	trailing_->leading_ = leading_;
  } else {			// last vehicle in the segment 
	trafficCell_->lastVehicle_ = leading_;
  }
   

  // (3) Insert this vehicle after the front 

   // (3.1) Pointers with the leading vehicle

  leading_ = front;
	
  if (leading_) {
	trailing_ = leading_->trailing_;
	leading_->trailing_ = this;
  } else {
	trailing_ = trafficCell_->firstVehicle_;
	trafficCell_->firstVehicle_ = this;
  }

  // (3.2) Pointers with the trailing vehicle

  if (trailing_) {
	trailing_->leading_ = this;
  } else {			// this vehicle becomes the last one 
	trafficCell_->lastVehicle_ = this;
  }
}


// Read the vehicle information dumpped by MesoTS or MITSIM

int
MESO_Vehicle::load(Reader &is, int queue)
{
  int idx;						// lane index
  int ori;						// origin
  int des;						// destination
  int p, k;					// path

  flags_ = 0;

  is >> code_ >> type_
	 >> ori >> des
	 >> departTime_
	 >> mileage_
	 >> p >> k
	 >> timeEntersLink_
	 >> idx >> distance_;

  RN_Node *o;
  RN_Node *d;

  if ((o = theNetwork->node(ori)) == NULL ||
	  (d = theNetwork->node(des)) == NULL) {

	return load_error("Origin and/or destination", is);

  }

  if (p >= 0) {				// path specified
	setPath(thePathTable->path(p), k);
  } else {						// no path specified
	setPath(NULL, k);			// k must be link index
  }

  MESO_Segment *ps;

  if (idx >= 0) {
	if (idx < theNetwork->nLanes()) {	// position specified
	  RN_Lane *pl = theNetwork->lane(idx);
	  ps = (MESO_Segment *) pl->segment();
	} else {
	  return load_error("lane index", is);
	}
  } else {						// yet to enter the network
	ps = NULL;
  }

  if (queue) {					// Vehicles in the pretrip queue

	if (ps) {
	  nextLink_ = ps->link();
	} else if (nextLink_) {
	  ps = (MESO_Segment*)nextLink_->startSegment();
	} else {				// failed to be set by setPath() above
	  return load_error("Entry link", is);
	}

	// append to the end of the queue

	((MESO_Link *)nextLink_)->queue(this);

	currentSpeed_ = ps->maxSpeed();
	distance_ = -currentSpeed_ * theSimulationClock->stepSize();

  } else {						// vehicles on the road
	if (ps) {
	  currentSpeed_ = ps->maxSpeed();
	  ps->insert(this);
	  theStatus->nActive(1);
	} else {
	  return load_error("segment", is);
	}
  }

  OD_Pair odpair(o, d);
  PtrOD_Pair odptr(&odpair);
  OdPairSetType::iterator i = theOdPairs.find(odptr);
  if (i == theOdPairs.end()) {
	od_ = new OD_Pair(odpair);
	theOdPairs.insert(od_);
  } else {
	od_ = (*i).p();
  }
  code_ = -code_;				// reverse the sign to avoid conflict

  return 0;
}


int MESO_Vehicle::load_error(const char* msg, Reader &is)
{
  cerr << "Warning:: " << msg << " of vehicle <"
	   << code_ << "> parsed at <"
	   << is.name() << ":" << is.line_number()
	   << "> invalid. Droped." << endl;
  return -1;
}
