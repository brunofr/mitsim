//-*-c++-*------------------------------------------------------------
// MESO_SetupDialog.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <cassert>

#include <Xmt/Xmt.h>
#include <Xmt/Chooser.h>
#include <Xmt/InputField.h>
#include <Xm/ToggleB.h>

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>
#include <Tools/Math.h>

#include "MESO_SetupDialog.h"
#include "MESO_Interface.h"
#include "MESO_Engine.h"
#include "MESO_FileManager.h"
#include "MESO_Interface.h"
#include "MESO_Parameter.h"
#include "MESO_Network.h"
#include "MESO_Incident.h"
#include "MESO_Menu.h"

#include <GRN/RN_PathTable.h>
#include <GRN/OD_Table.h>
#include <GRN/OD_Cell.h>
#include <GRN/VehicleTable.h>
#include <GRN/RN_DynamicRoute.h>
using namespace std;

MESO_SetupDialog::MESO_SetupDialog ( Widget parent )
  : XmwDialogManager(parent, "setupDialog", NULL, 0)
{
  XtVaSetValues(widget_,
				XmNdialogStyle, XmDIALOG_PRIMARY_APPLICATION_MODAL,
				NULL);

  title_ = XmtNameToWidget(widget_, "title");
  assert(title_);

  Widget group;
  int i = 0;

  group = XmtNameToWidget(widget_, "*DirectoryGroup");
  assert(group);

  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*paraDir");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*inDir");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*workDir");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*outDir");

  group = XmtNameToWidget(widget_, "*InputGroup");
  assert(group);

  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*paraFile");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*netFile");
   
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*linktimeFile0");
   
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*pathFile");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*odFile");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*vehicleFile");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*inciFile");

  group = XmtNameToWidget(widget_, "*TimingGroup");
  assert(group);

  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*startTime");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*stopTime");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*stepSize");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*pathStepSize");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*updateStepSize");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*simlabMsgStepSize");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*batchStepSize");

  group = XmtNameToWidget(widget_, "*DataGroup");
  assert(group);

  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*stateStepSize");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*depRecordStepSize");
}

void MESO_SetupDialog::set(char **ptr, const char *s)
{
  if (*ptr) {
	free(*ptr);
  }
  if (s) {
	*ptr = strdup(s);
  } else {
	*ptr = strdup("");
  }
}

// These overload the functions in base class

void MESO_SetupDialog::cancel(Widget, XtPointer, XtPointer)
{
  unmanage();
}


void MESO_SetupDialog::okay(Widget, XtPointer, XtPointer)
{
  // Copy the data from dialog 

  theFileManager->title(XmtInputFieldGetString(title_));

  int i = 0;
  ToolKit::paradir(flds_[i++]->get());
  ToolKit::indir(flds_[i++]->get());
  ToolKit::workdir(flds_[i++]->get());
  ToolKit::outdir(flds_[i++]->get());

  set(MESO_Parameter::nameptr(), flds_[i++]->get());
  set(RoadNetwork::nameptr(), flds_[i++]->get());

  set(RN_DynamicRoute::fileptr(0), flds_[i++]->get());

  set(RN_PathTable::nameptr(), flds_[i++]->get());
  set(OD_Table::nameptr(), flds_[i++]->get());
  set(VehicleTable::nameptr(), flds_[i++]->get());
  set(MESO_Incident::filenameptr(), flds_[i++]->get());

  theSimulationClock->startTime(flds_[i++]->get());
  theSimulationClock->stopTime(flds_[i++]->get());
  theSimulationClock->stepSize(flds_[i++]->getTime());
  theEngine->pathStepSize_ = flds_[i++]->getTime();

  theEngine->updateStepSize_ = flds_[i++]->getTime();
  theEngine->simlabMsgStepSize_ = flds_[i++]->getTime();
  theEngine->batchStepSize_ = flds_[i++]->getTime();

  theEngine->stateStepSize_ = flds_[i++]->getTime();
  theEngine->depRecordStepSize_ = flds_[i++]->getTime();

  unmanage();

  theMenu->needSave();
}


void MESO_SetupDialog::help(Widget, XtPointer, XtPointer)
{
  theInterface->openUrl("setup", "mitsim.html");
}


void MESO_SetupDialog::post()
{
  XmtInputFieldSetString(title_, theFileManager->title());
  int i = 0;
  flds_[i++]->set(ToolKit::paradir());
  flds_[i++]->set(ToolKit::indir());
  flds_[i++]->set(ToolKit::workdir());
  flds_[i++]->set(ToolKit::outdir());

  flds_[i++]->set(MESO_Parameter::name());
  flds_[i++]->set(RoadNetwork::name());

  flds_[i++]->set(RN_DynamicRoute::file(0));

  flds_[i++]->set(RN_PathTable::name());
  flds_[i++]->set(OD_Table::name());
  flds_[i++]->set(VehicleTable::name());
  flds_[i++]->set(MESO_Incident::filename());

  flds_[i++]->set(theSimulationClock->startStringTime());
  flds_[i++]->set(theSimulationClock->stopStringTime());
  flds_[i++]->set(theSimulationClock->stepSize());
  flds_[i++]->set(theEngine->pathStepSize_);

  flds_[i++]->set(theEngine->updateStepSize_);
  flds_[i++]->set(theEngine->simlabMsgStepSize_);
  flds_[i++]->set(theEngine->batchStepSize_);

  flds_[i++]->set(theEngine->stateStepSize_);
  flds_[i++]->set(theEngine->depRecordStepSize_);

  // Do not allow changes after simulation is started

  if (theSimulationClock->isStarted()) {
	deactivate();
  } else {
	activate();
  }

  XmwDialogManager::post();
}

void MESO_SetupDialog::activate()
{
  if (XtIsSensitive(title_)) return;

  XmwDialogManager::activate(title_);

  for (int i = 0; i < flds_.size(); i ++) {
	XmwDialogManager::activate(flds_[i]->widget());
  }
}

void MESO_SetupDialog::deactivate()
{
  if (!XtIsSensitive(title_)) return;

  XmwDialogManager::deactivate (title_);

  for (int i = 0; i < flds_.size(); i ++) {
	XmwDialogManager::deactivate(flds_[i]->widget());
  }
}

#endif // INTERNAL_GUI
