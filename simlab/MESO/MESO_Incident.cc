//-*-c++-*------------------------------------------------------------
// NAME: Traffic Simulation
// NOTE: 
// AUTH: Qi Yang
// FILE: MESO_Incident.C
// DATE: Mon Dec 16 14:59:04 1996
//--------------------------------------------------------------------

#include <cstdio>
#include <iostream>
using namespace std;

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>
#include <Tools/Reader.h>

#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>

#include "MESO_Incident.h"
#include "MESO_Network.h"
#include "MESO_Segment.h"

inc_list_type theIncidentList;
inc_list_link_type theNextIncident = NULL;

char* MESO_Incident::filename_ = NULL;

MESO_Incident::MESO_Incident(double t, MESO_Segment* s, float cap)
   : CodedObject(),
	 time_(t),
	 segment_(s),
	 capChange_(cap)
{
   static int id = 0;
   id ++;
   code_ = id;
}


int MESO_Incident::comp(CodedObject* inc)
{
  MESO_Incident *other = (MESO_Incident *)inc;
   const double epsilon = 1.0e-5;
   if (time_ < other->time_ - epsilon) return -1;
   else if (time_ > other->time_ + epsilon) return 1;
   else return 0;
}


void
MESO_Incident::updateCapacity()
{
   segment_->addCapacity(capChange_);
}


void
LoadIncidents(const char *filename)
{
   theIncidentList.remove();

   if (MESO_Incident::filename() != filename) {
	  MESO_Incident::filename(filename);
   }

   if (!ToolKit::fileExists(filename)) {
	 return;
   }

   int si;
   double sch;
   float cap_factor;
   int n_minus = 0, n_plus = 0;
 
   Reader is(filename);

   for (is.eatwhite(); is.good() && !is.eof(); is.eatwhite()) {
	 is >> sch >> si >> cap_factor;
	 MESO_Segment *ps = (MESO_Segment *) theNetwork->findSegment(si);
	 if (ps) {
	   float cap_change = cap_factor * ps->defaultCapacity();
	   MESO_Incident *inc = new MESO_Incident(sch, ps, cap_change);
	   theIncidentList.insert(inc);
	   if (cap_factor < 0) {
		 n_minus ++;
	   } else if (cap_factor > 0) {
		 n_plus ++;
	   }
	 } else {
	   cerr << "Error:: Segment <" << si << "> not found. ";
	   is.reference();
	 }
   }
   is.close();

   if (ToolKit::verbose()) {
	 cout << theIncidentList.nNodes()
		  << " incident records ("
		  << n_minus << "+" << n_plus
		  << ") loaded from <"
		  << filename << ">." << endl;
   }
   theNextIncident = theIncidentList.head();
}


void
UpdateCapacities()
{
   while (theNextIncident &&
		  (*theNextIncident)->time() <= theSimulationClock->currentTime()) {
	  (*theNextIncident)->updateCapacity();
	  theNextIncident = theNextIncident->next();
   }
}
