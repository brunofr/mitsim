//-*-c++-*------------------------------------------------------------
// MESO_Symbols.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include "MESO_Symbols.h"

void MESO_Symbols::registerAll()
{
   DRN_Symbols::registerAll();
   vehicleBorderCode_.create("vehicleBorderCode",
							 XtRInt, VEHICLE_BORDER_TYPE);
   vehicleShadeCode_.create("vehicleShadeCode",
							XtRInt, VEHICLE_SHADE_NONE);
   isVehicleLabelOn_.create("isVehicleLabelOn",
							XtRBoolean, False);
}

#endif // INTERNAL_GUI
