//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_TrafficCell.C
// DATE: Mon Feb 12 11:56:34 1996
//--------------------------------------------------------------------

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>

#include <Tools/Math.h>

#include <TC/TC_Sensor.h>
#include <TC/TC_Signal.h>

#include <GRN/RN_SdFn.h>

#include "MESO_Engine.h"
#include "MESO_Link.h"
#include "MESO_Segment.h"
#include "MESO_TrafficCell.h"
#include "MESO_Node.h"
#include "MESO_Parameter.h"
#include "MESO_VehicleList.h"
#include "MESO_CellList.h"
#include "MESO_Network.h"

#ifdef INTERNAL_GUI
#include <DRN/DRN_DrawingArea.h>
#include "MESO_DrawableVehicle.h"
#else
#include "MESO_Vehicle.h"
#endif

MESO_TrafficCell::MESO_TrafficCell()
   : trailing_(NULL),
     leading_(NULL),
     firstVehicle_(NULL),
     lastVehicle_(NULL),
     nVehicles_(0),
     headSpeeds_(NULL),
     headPositions_(NULL),
     nHeads_(0),
	 segment_(NULL)
{
}


MESO_TrafficCell::~MESO_TrafficCell()
{
   clean();
}


void
MESO_TrafficCell::clean()
{
   while (firstVehicle_) {
      lastVehicle_ = firstVehicle_;
      firstVehicle_ = firstVehicle_->trailing();
      theVehicleList->recycle(lastVehicle_);
   }
   lastVehicle_ = NULL;
   nVehicles_ = 0;

   segment_ = NULL;

   if (headSpeeds_) {
	 delete [] headSpeeds_;
	 headSpeeds_ = NULL;
   }
   if (headPositions_) {
	 delete [] headPositions_;
	 headPositions_ = NULL;
   }
   nHeads_ = 0;
}


MESO_Link*
MESO_TrafficCell::mesoLink()
{
   return (MESO_Link *) link();
}


// Returns the link contains this traffic cell

MESO_Link*
MESO_TrafficCell::link() 
{
   return (segment_ ? (MESO_Link *)segment_->link() : (MESO_Link*)NULL);
}


// Downstream position of a traffic cell is represented by the front
// bumper of the first vehicle.  If there is no vehicle in the cell,
// the maximum reachable position in a updated phase is returned.

float
MESO_TrafficCell::dnDistance()
{
   if (firstVehicle_) {
	  return firstVehicle_->distance_;
   } else {
	  return maxReachablePosition();
   }
}


// Upstream position of a traffic cell is represented by the back
// bumper of the last vehicle.  If there is not vehicle in the cell,
// the maximum reachable position in a updated phase is returned.

float
MESO_TrafficCell::upDistance()
{
   if (lastVehicle_) {
	  return lastVehicle_->upPos();
   } else {
	  return maxReachablePosition();
   }
}


// Assume this cell travels at free flow speed, calculate the position
// it can reach at the end of the advance phase.  It returns 0 if the
// cell may pass the end of the segment.

float
MESO_TrafficCell::maxReachablePosition()
{
   float dx = segment_->maxSpeed() * theSimulationClock->stepSize();
   dx = segment_->length() - dx;
   if (leading_) {
	  float pos = leading_->upDistance();
	  dx = (dx > pos) ? dx : pos;
   }
   return (dx > 0.0 ? dx : 0.0);
}

// Length of a traffic cell is represented by the distance between the
// front bumper of the first vehicle and back bumper of the last
// vehicle.  If no vehicle in the cell, its length is 0.

float
MESO_TrafficCell::length()
{
   if (firstVehicle_) {
      return upDistance() - firstVehicle_->distance_;
   } else {
      return 0.0;
   }
}


// This function decides which speed-density function is used by this
// traffic cell

short int 
MESO_TrafficCell::sdIndex()
{
   return segment_->sdIndex();
}


// Calculate the mean speed of this cell.

float
MESO_TrafficCell::speed()
{
   return 0.5 * (tailSpeed_ + headSpeed());
}

// Average of the downstream speeds

float
MESO_TrafficCell::headSpeed()
{
   if (nHeads_ < 2) {			// single head
	  return headSpeeds_[0];
   } else {						// multiple heads
	  float sum = 0.0;
	  for (short int i = 0; i < nHeads_; i ++) {
		 sum += headSpeeds_[i];
	  }
	  return sum / (float)nHeads_;
   }
}


// This is the current speed of last vehicle in the cell

float
MESO_TrafficCell::upSpeed()
{
   if (lastVehicle_) {
	  return lastVehicle_->currentSpeed();
   } else {
	  return segment_->maxSpeed();
   }
}


// This function calculate the current density

float
MESO_TrafficCell::calcDensity()
{
   float len = length() * segment_->nLanes();
   if (len > theParameter->rspLower()) {
	  return 1000.0 * nVehicles_ / len;
   } else {
      return 0.0;
   }
}


// Calculate the speed of the last vehicle

float
MESO_TrafficCell::calcSpeed()
{
  //  cout << " First Calc Speed " ;
   float len = length();
   if (len > theParameter->cellSplitGap()) {
	  return calcSpeed(1000.0 * nVehicles_ / (len * segment_->nLanes()));
   } else if (len < theParameter->rspLower()) {
	  // for very short cell consider the leading cell also
	  if (leading_) {
		 len += distance(leading_) + leading_->length();
		 int num = nVehicles_ + leading_->nVehicles();
		 return calcSpeed(1000.0 * num / (len * segment_->nLanes()));
	  } else {
		 return segment_->maxSpeed();
	  }
   } else {
	  // speed-density function will not work for short cells
	  return segment_->maxSpeed(len * segment_->nLanes() / nVehicles_);
   }
}


// Calculate speed based on current density

float
MESO_TrafficCell::calcSpeed(float density)
{
  // cout << " Calc Speed Manish " << sdIndex() ;
   RN_SdFn *sdf = theNetwork->sdFn(sdIndex());
   return sdf->densityToSpeed(
	  segment_->maxSpeed(),
	  density,
	  segment_->nLanes());
}


// Calculates the upstream speed based on the density in the traffic
// cell.

void
MESO_TrafficCell::updateTailSpeed()
{
  // cout<< "Update Tail Speed XXX";
   tailPosition_ = upDistance();
   tailSpeed_ = calcSpeed();
}


// Set the speed of each head vehicle to the same speed and record the
// reference position

void
MESO_TrafficCell::setHeadSpeeds(float spd, float pos)
{
   for (short int i = 0; i < nHeads_; i ++) {
      headPositions_[i] = pos;
	  if (headSpeeds_[i] > 0.1) { // This stream was moving
		float maxspd = theParameter->queueReleasingSpeed(
		  timeSinceDispatching(), segment_->maxSpeed());
		if (spd > maxspd) {
		  headSpeeds_[i] = maxspd;
		} else {
		  headSpeeds_[i] = spd;
		}
	  } else {					// stopped
		 headSpeeds_[i] = spd;
		 queueTime_ = theSimulationClock->currentTime();
	  }
   }
}


// Calculates the downstream speeds based on the space from and speed
// of the downstream traffic cells in ealier.  This function is called
// at least once in every update phase and when cells are created or
// combined.

void
MESO_TrafficCell::updateHeadSpeeds()
{
   short int i;

   // downstream position of the cell

   float dnx = dnDistance();

   if (!segment_->isMoveAllowed() && dnx < 1.0) {

	  // The output capacity is a constraint

	  setHeadSpeeds(0.0, dnx);
	  return;
   }

   if (leading_) {

	  // There is a cell ahead, speed is determined based on the
	  // reaction to that cell.

      setHeadSpeeds(calcHeadSpeed(leading_), dnx);
      return;

   } else if (dnx > theParameter->rspUpper()) {

	  // Since no cell ahead and distance is greater than a threshold,
	  // it use free flow speed.

      setHeadSpeeds(segment_->maxSpeed(), dnx);
      return;

   } else if (!segment_->isHead()) {
	  
      // Not the last segment in the link, speed is based on
      // downstrean condition

      setHeadSpeeds(calcHeadSpeed(segment_->downstream()->lastCell()), dnx);
      return;

   } else if (nHeads_ > 1) {
	  
      // At the end of the last segment in the link and there is no
      // cell ahead.  Need to check where the vehicles in this cell
      // want to go and the condition in the downstream links.
   
	  for (i = 0; i < nHeads_; i ++) {
		 calcHeadSpeed(i, dnx);
	  }
	  return;

   } else if (segment_->isTheEnd()) {

	  // Boundary link, no constrains

      setHeadSpeeds(segment_->maxSpeed(), dnx);
      return;

   } else {

	  // At the end of the last segment in the link.  This link has
	  // one outgoing link.

      MESO_Segment *ps = (MESO_Segment *)link()->dnLink(0)->startSegment();
      setHeadSpeeds(calcHeadSpeed(ps->lastCell()), dnx);

      return;
   }
}


// Calculate head speed for the ith traffic stream.

void
MESO_TrafficCell::calcHeadSpeed(short int ith, float dnx)
{
   // Capacity based speed

   float maxspeed = theParameter->queueReleasingSpeed(
	 timeSinceDispatching(),
	 segment_->maxSpeed());
   
   MESO_Link *dnlink = (MESO_Link *) link()->dnLink(ith);

   // Find the first vehicle heading to the ith downstream link and
   // number of vehicles ahead (in the same cell)

   MESO_Vehicle *vehicle = firstVehicle_;
   int n = 0;
   while (vehicle &&
		  vehicle->nextLink_ != dnlink) {
      vehicle = vehicle->trailing_;
      n ++;
   }

   MESO_Segment *dnseg = (MESO_Segment *)dnlink->startSegment();
   MESO_TrafficCell *cell = dnseg->lastCell();

   if (vehicle) {
      headPositions_[ith] = dnx = vehicle->distance_;
   } else {
      headPositions_[ith] = dnx;
   }

   // Speed in response to the downstream traffic cell 

   float spd_by_rsp;
   if (cell) {
      float gap = dnx + dnseg->length() - cell->upDistance();
      spd_by_rsp = calcFollowingCellSpeed(gap, cell->tailSpeed());
   } else {
      spd_by_rsp = maxspeed;
   }

   // Density-based speed

   float spd_by_den;
   int nlanes = segment_->nLanes();
   if (!vehicle || dnx < 10.0 * nlanes) {
      spd_by_den = spd_by_rsp;
   } else if (n > nlanes) {
      float k = 1000.0 * n / (dnx * nlanes);
      spd_by_den = calcSpeed(k);
	  if (nlanes > 1 &&
		  spd_by_den < theParameter->minSpeed()) {
		 int num = link()->dnLink(ith)->startSegment()->nLanes();
		 spd_by_den = theParameter->minSpeed() * num;
	  }
   } else {
      spd_by_den = maxspeed;
   }

   float spd = Min(spd_by_rsp, spd_by_den);

   if (spd < maxspeed) {
	  headSpeeds_[ith] = spd;
   } else {
	  headSpeeds_[ith] = maxspeed;
   }

   if (headSpeeds_[ith] < 0.1) {
	  queueTime_ = theSimulationClock->currentTime(); 
   }
}



// Calculate the speed based on the relationship with the given
// downstream traffic cell

float
MESO_TrafficCell::calcHeadSpeed(MESO_TrafficCell *cell)
{
   float rspspd;
   if (cell) {
      rspspd = calcFollowingCellSpeed(
		 distance(cell),
		 cell->tailSpeed());
   } else {
      rspspd = segment_->maxSpeed();
   }
   return rspspd;
}


// Calculate the speed based on the headway distance from the leading
// cell

float
MESO_TrafficCell::calcFollowingCellSpeed(float x, float v)
{
   float maxgap = theParameter->rspUpper();
   if (x >= maxgap) {
      return segment_->maxSpeed();
   } else if (x <= 0.1) {
	  return v;
   } else {
	  float r = x / maxgap;
	  return r * segment_->maxSpeed(x) + (1.0 - r) * v;
   }
}
 

// Calculate the distance from the leading traffic cell

float
MESO_TrafficCell::distance(MESO_TrafficCell* cell)
{
   // skip the empty cells

   while (cell && !cell->lastVehicle_) {
	  cell = cell->leading_;
   }

   float dis = (firstVehicle_) ? dnDistance() : segment_->length();
   if (cell) {
      if (cell->segment() == segment())	{ // same segment
		 dis -= cell->upDistance();
      } else {					// different segment
		 dis += cell->segment()->length() - cell->upDistance();
      }
   } else {						// no cell ahead
      dis = FLT_INF;
   }
   return dis;
}


// Move vehicles based on current cell states.

void
MESO_TrafficCell::advanceVehicles()
{
   MESO_Vehicle *vehicle = firstVehicle_;
   MESO_Vehicle *next;
   while (vehicle) {
      next = vehicle->trailing_;
	  if (!vehicle->isProcessed()) {
		 vehicle->move();
	  }
      vehicle = next;
   }
}


// Called when a cell is appended to the upstream end of a segment

void
MESO_TrafficCell::initialize()
{
   queueTime_ = -FLT_INF;

   firstVehicle_ = lastVehicle_ = NULL;
   nVehicles_ = 0;

   nHeads_ = link()->nDnLinks();

   if (!segment_->isHead() ||	// not in the last segment in the link
	   !nHeads_) {				// dead end or boundary link
	  nHeads_ = 1;
   }

   headSpeeds_ = new float [nHeads_];
   headPositions_ = new float [nHeads_];

   tailSpeed_ = segment_->maxSpeed();

   tailPosition_ = segment_->length() - 
	 tailSpeed_ * theSimulationClock->stepSize();
   for (register int i = 0; i < nHeads_; i ++) {
	  headSpeeds_[i] = tailSpeed_;
	  headPositions_[i] = tailPosition_; 
   }
}


// Split the traffic cell into two cells at the first gap smaller than
// the given threshold

void MESO_TrafficCell::split()
{
   MESO_Vehicle *front = firstVehicle_;
   MESO_Vehicle *pv = NULL;
   while (front && (pv = front->trailing_) &&
		  pv->distance() <= front->upPos() +
		  theParameter->cellSplitGap()) {
	  front = pv;
   }
   if (!pv) return;				// not need to split

   // SPLIT THE CELL BETWEEN front AND pv

   // Create a new cell and put it after the current cell

   MESO_TrafficCell *cell = theCellList->recycle();

   // We do not call initialize() as usual, but we copy the variable
   // from this to the new cell

   cell->leading_ = this;
   cell->trailing_ = trailing_;
   if (trailing_) {				// this is not the last in segment
	  trailing_->leading_ = cell;
   } else {						// this is the last in segment
	  segment_->lastCell_ = cell;
   }
   trailing_ = cell;
   segment_->nCells_ ++;

   // Copy variables from this cell to the new cell that follows

   cell->segment_ = segment_;
   cell->tailSpeed_ = tailSpeed_;
   cell->tailPosition_ = tailPosition_;
   cell->nHeads_ = nHeads_;
   cell->headSpeeds_ = new float [nHeads_];
   cell->headPositions_ = new float [nHeads_];
   for (int i = 0; i < nHeads_; i ++) {
	  cell->headSpeeds_[i] = headSpeeds_[i];
	  cell->headPositions_[i] = headPositions_[i];
   }

   // Cut the vehicle list into two

   pv->leading_ = NULL;			// first in the new cell
   front->trailing_ = NULL;		// last in this cell
   cell->firstVehicle_ = pv;	// first in the new cell
   cell->lastVehicle_ = lastVehicle_; // last in the new cell
   lastVehicle_ = front;		// last in this cell

   // Change container for the vehicles in the new cell

   cell->nVehicles_ = 0;
   while (pv) {
      pv->trafficCell_ = cell;
      pv = pv->trailing_;
	  cell->nVehicles_ ++;
   }
   
   // Update vehicle counter and length_

   nVehicles_ -= cell->nVehicles_;
}


// Insert a vehicle into the cell based on its distance position

void MESO_TrafficCell::insert(MESO_Vehicle *pv)
{
   // Find the front vehicle

   MESO_Vehicle *front = lastVehicle_;
   while (front && front->distance_ > pv->distance_) {
	  front = front->leading_;
   }

   // Insert after the front vehicle

   if (front) {					// pv is NOT the first in cell
	  pv->trailing_ = front->trailing_;
	  front->trailing_ = pv;
   } else {						// pv is the first in cell
	  pv->trailing_ = firstVehicle_;
	  firstVehicle_ = pv;
   }
   if (pv->trailing_) {
	  pv->trailing_->leading_ = pv;
   } else {
	  lastVehicle_ = pv;
   }

   pv->trafficCell_ = this;
   pv->calcSpaceInSegment();

   nVehicles_ ++;
}


// Add a vehicle at the upstream end

void
MESO_TrafficCell::append(MESO_Vehicle *vehicle)
{
   vehicle->leading_ = lastVehicle_;
   vehicle->trailing_ = NULL;

   if (lastVehicle_) {		// append at end
      lastVehicle_->trailing_ = vehicle;
   } else {			// queue is empty
      firstVehicle_ = vehicle;
   }
   lastVehicle_ = vehicle;
   nVehicles_ ++;

   vehicle->appendTo(this);

   if (nVehicles_ <= 1) {		// first vehicle
	  updateTailSpeed();
	  updateHeadSpeeds();
   }
}


// Remove a vehicle from the list

void
MESO_TrafficCell::remove(MESO_Vehicle *vehicle)
{
   if (vehicle->leading_) {		// not the first one
      vehicle->leading_->trailing_ = vehicle->trailing_;
   } else {						// first one
      firstVehicle_ = vehicle->trailing_;
   }
   if (vehicle->trailing_) {	// not the last one
      vehicle->trailing_->leading_ = vehicle->leading_;
   } else {						// last one
      lastVehicle_ = vehicle->leading_;
   }
   nVehicles_ --;
}


// Append all vehicles in the given cell to the end of this cell.  The
// input cell becomes empty and should be removed in by the caller

void
MESO_TrafficCell::append(MESO_TrafficCell *cell)
{
   MESO_Vehicle *vehicle = cell->firstVehicle_;

   if (!vehicle) {
      return;
   }

   // Change container for the vehicles in cell

   while (vehicle) {
      vehicle->trafficCell_ = this;
      vehicle = vehicle->trailing_;
   }

   // Connect the two cells
   
   if (lastVehicle_) {			// this cell is not empty
      lastVehicle_->trailing_ = cell->firstVehicle_;
   } else {						// this cell is empty
      firstVehicle_ = cell->firstVehicle_;
   }
   cell->firstVehicle_->leading_ = lastVehicle_;
   lastVehicle_ = cell->lastVehicle_;
   
   // Update vehicle counter

   nVehicles_ += cell->nVehicles_;

   // Update the pointers and vehicle counts in cell

   cell->nVehicles_ = 0;
   cell->firstVehicle_ = cell->lastVehicle_ = NULL;
}


int
MESO_TrafficCell::isJammed()
{
   if (lastVehicle_ &&
	   lastVehicle_->upPos() >= segment_->length_) {
	  return 1;
   } else {
	 return 0;
   }
}


int
MESO_TrafficCell::isReachable()
{
   return (segment_->length() - upDistance()) < theParameter->cellSplitGap();
}


float
MESO_TrafficCell::timeSinceDispatching()
{
   return theSimulationClock->currentTime() - queueTime_;
}

#ifdef INTERNAL_GUI

void
MESO_TrafficCell::draw(DRN_DrawingArea * view_area)
{
   if (!segment_->isVisible()) return;

   MESO_Vehicle *vehicle = firstVehicle_;
   while (vehicle) {
      ((MESO_DrawableVehicle *)vehicle)->draw(view_area);
      vehicle = vehicle->trailing_;
   }
}

#endif
