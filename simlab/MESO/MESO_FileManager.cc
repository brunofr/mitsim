//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_FileManager.C
// DATE: Mon Feb 26 16:22:45 1996
//--------------------------------------------------------------------

#include <cstring>

#include "MESO_FileManager.h"

#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <Tools/SimulationClock.h>

// This file is generated from VariableParser.y

#include <Tools/VariableParser.h>

#include <IO/MessageTags.h>
#include <IO/Exception.h>

#include <GRN/OD_Table.h>
#include <GRN/OD_Cell.h>
#include <GRN/RN_PathTable.h>
#include <GRN/RN_DynamicRoute.h>
#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>


#ifdef INTERNAL_GUI
#include <Xmt/Xmt.h>
#include <DRN/DRN_MapFeature.h>
#include <DRN/DRN_Segment.h>
#include <DRN/DRN_ViewMarker.h>
#include "MESO_Interface.h"
#include "MESO_Symbols.h"
#include "MESO_Menu.h"
#endif

#include "MESO_Engine.h"
#include "MESO_Network.h"
#include "MESO_Parameter.h"
#include "MESO_VehicleTable.h"
#include "MESO_Communicator.h"
#include "MESO_Incident.h"
#include "MESO_Vehicle.h"
using namespace std;

MESO_FileManager* theFileManager = new MESO_FileManager;

extern int loadVehicleShadeParams(GenericVariable &);
extern void logSuspiciousVehicles();

MESO_FileManager::MESO_FileManager()
  : GenericSwitcher(),
	title_(NULL),
	vehicleFile_(strdup("vehicle_e.out")),
	linkFlowTravelTimesFile_(strdup("lft_e.out")),
	linkTravelTimesFile_(strdup("ltt_e.out")),
	state3d_(strdup("e3d")),
	pathRecordFile_(strdup("path_e.out")),
	depRecordFile_(strdup("dep.out")),
	stateDumpFile_(NULL)
{
}


void MESO_FileManager::set(char **ptr, const char *s)
{
  if (*ptr) {
	free(*ptr);
  }
  if (s) {
	*ptr = strdup(s);
  } else {
	*ptr = NULL;
  }
}


// Read the control variables from master file.

void
MESO_FileManager::readMasterFile()
{
  VariableParser vp(this);
  vp.parse(theEngine->master());
}


// Check if two token are equal.

int MESO_FileManager::isEqual(const char *s1, const char *s2)
{
  return IsEqual(s1, s2, 1);
}


// This function returns 0 if the variable is parsed or 1 if it is
// skipped (in case command line provides the value), or -1 if error
// occurs.

int MESO_FileManager::parseVariable(char ** varptr, GenericVariable &gv)
{
  *varptr = Copy(gv.string());
  return 0;
}


// All the variables provided in the master file should be parsed in
// this function.

int
MESO_FileManager::parseVariable(GenericVariable &gv) 
{
  char * token = compact(gv.name());

  if (isEqual(token, "Title")) {
	title_ = Copy(gv.string());

  } else if (isEqual(token, "DefaultParameterDirectory")) {
	ToolKit::paradir(gv.string());
	return 0;
  } else if (isEqual(token, "InputDirectory")) {
	ToolKit::indir(gv.string());
	return 0;
  } else if (isEqual(token, "OutputDirectory")) {
	ToolKit::outdir(gv.string());
	return 0;
  } else if (isEqual(token, "WorkingDirectory")) {
	ToolKit::workdir(gv.string());
	return 0;

  } else if (isEqual(token, "NetworkDatabaseFile")) {
	return parseVariable(RoadNetwork::nameptr(), gv);

  } else if (isEqual(token, "GDSFiles")) {
#ifdef INTERNAL_GUI
	theMapFeatures = new DRN_MapFeature;
	theMapFeatures->load(gv);
#endif
	return 0;

  } else if (isEqual(token, "NetworkStateTag")) {
	return parseVariable(&state3d_, gv);

  } else if (isEqual(token, "LinkTravelTimesInputFile")) {
	return RN_DynamicRoute::setFileNamesOfTravelTimeTables(gv);

  } else if (isEqual(token, "PathTableFile")) {
	return parseVariable(RN_PathTable::nameptr(), gv);

  } else if (isEqual(token, "VehicleTableFile")) {
	return parseVariable(VehicleTable::nameptr(), gv);

  } else if (isEqual(token, "TripTableFile")) {
	return parseVariable(OD_Table::nameptr(), gv);

  } else if (isEqual(token, "ParameterFile")) {
	return parseVariable(MESO_Parameter::nameptr(), gv);

  } else if (isEqual(token, "StateDumpFile")) {
	return parseVariable(&stateDumpFile_, gv);

  } else if (isEqual(token, "IncidentFile")) {
	return parseVariable(MESO_Incident::filenameptr(), gv);

  } else if (isEqual(token, "VehicleFile")) {
	return parseVariable(&vehicleFile_, gv);

  } else if (isEqual(token, "LinkFlowTravelTimesOutputFile")) {
	return parseVariable(&linkFlowTravelTimesFile_, gv);

  } else if (isEqual(token, "LinkTravelTimesOutputFile")) {
	return parseVariable(&linkTravelTimesFile_, gv);

  } else if (isEqual(token, "VehiclePathRecordFile")) {
	return parseVariable(&pathRecordFile_, gv);     

  } else if (isEqual(token, "DepartureRecordFile")) {
	return parseVariable(&depRecordFile_, gv);

  } else if (isEqual(token, "StartTime")) {
	theSimulationClock->startTime(gv.getTime());
  } else if (isEqual(token, "StopTime")) {
	theSimulationClock->stopTime(gv.getTime());
  } else if (isEqual(token, "StepSize")) {
	theSimulationClock->stepSize(gv.getTime());

  } else if (isEqual(token, "BreakPoints")) {
	return theEngine->parseBreakPoints(gv);

  } else if (isEqual(token, "UpdateStepSize")) {
	theEngine->updateStepSize_ = gv.getTime();
  } else if (isEqual(token, "ConsoleMessageStepSize")) {
	theEngine->batchStepSize_ = gv.getTime();

  } else if (isEqual(token, "PathStepSize")) {
	theEngine->pathStepSize_ = gv.getTime();
  } else if (isEqual(token, "SPFlags")) {
	theSpFlag = gv.element();

  } else if (isEqual(token, "DepartureRecordStepSize")) {
	theEngine->depRecordStepSize_ = gv.getTime();

  } else if (isEqual(token, "SimlabMsgStepSize")) {
	theEngine->simlabMsgStepSize_ = gv.getTime();

  } else if (isEqual(token, "Signals")) {
#ifdef INTERNAL_GUI
	theDrnSymbols()->signalTypes().set(gv.element());
#endif
	return 0;

  } else if (isEqual(token, "SensorTypes")) {
#ifdef INTERNAL_GUI
	theSymbols()->sensorTypes().set(gv.element());
#endif
	return 0;

  } else if (isEqual(token, "SensorColorCode")) {
#ifdef INTERNAL_GUI
	theSymbols()->sensorColorCode().set(gv.element());
#endif
	return 0;

  } else if (isEqual(token, "Vehicles")) {
#ifdef INTERNAL_GUI
	theSymbols()->vehicleBorderCode().set(gv.element());
#endif
	return 0;

  } else if (isEqual(token, "Segments")) {
#ifdef INTERNAL_GUI
	theSymbols()->segmentColorCode().set(gv.element());
#endif
	return 0;

  } else if (isEqual(token, "ViewMarkers")) {
#ifdef INTERNAL_GUI
	DRN_ViewMarker::LoadViewMarkers(gv) ;
#endif
	return 0;

  } else if (isEqual(token, "VehicleShadeParams")) {
	return loadVehicleShadeParams(gv);

  } else if (isEqual(token, "Output")) {
	theEngine->chooseOutput(gv.element());
 
  } else if (isEqual(token, "Timeout")) {

	// This will overwrite the value parsed from command line

	NO_MSG_WAITING_TIME = gv.element();

  } else if (ToolKit::verbose()) {

	return 1;
  }

  return 0;
}


void
MESO_FileManager::openOutputFiles()
{
  const char *fn;

  // Check the file name is valid if the output is selected

  if (theEngine->chosenOutput(OUTPUT_LINK_TRAVEL_TIMES) &&
	  linkTravelTimesFile_ == NULL) {
	cerr << "Error:: No output file name for link travel times "
		 << "in master file <"
		 << theEngine->master() << ">." << endl;
	theException->exit();
  }

  /* Open vehicle log file */

  if (theEngine->chosenOutput(OUTPUT_VEHICLE_LOG)) {
	if (vehicleFile_ == NULL) {
	  fn = vehicleFile_ = tmpnam(NULL);
	} else {
	  fn = ToolKit::outfile(vehicleFile_);
	}
	osVehicle_.open(fn);
	if (osVehicle_.good()) {
	  cout << "Vehicle output goes to "
		   << fn << "." << endl;
	} else goto error;
  }

  if (theEngine->chosenOutput(OUTPUT_VEHICLE_PATH_RECORDS)) {
	if (pathRecordFile_ == NULL) {
	  fn = pathRecordFile_ = tmpnam(NULL);
	} else {
	  fn = ToolKit::outfile(pathRecordFile_);
	}
	osPathRecord_.open(fn);
	if (osPathRecord_.good()) {
	  cout << "Vehicle path record output goes to "
		   << fn << "." << endl;
	} else goto error;
  }

  if (theEngine->chosenOutput(OUTPUT_VEHICLE_DEP_RECORDS)) {
	MESO_Vehicle::openDepartureRecords(depRecordFile_);
  }

  return;

 error:

  cerr << "Error:: Failed opening output file <"
	   << fn << ">." << endl;
  theException->exit();
}

void
MESO_FileManager::closeOutputFiles()
{
  cout << "Output files are in directory <"
	   << ToolKit::outDir() << ">." << endl;

  if (theEngine->chosenOutput(OUTPUT_VEHICLE_LOG)) {
	cout << "Vehicle trip information saved in <"
		 << vehicleFile_ << ">." << endl;
	osVehicle_.close();
  }

  if (theEngine->chosenOutput(OUTPUT_VEHICLE_PATH_RECORDS)) {
	cout << "Vehicle path output saved in <"
		 << pathRecordFile_ << ">." << endl;
	osPathRecord_.close();
  }

  MESO_Communicator *com = (MESO_Communicator *)theCommunicator;

  if (com->isTmsConnected() ||
	  !theEngine->chosenOutput(
							   OUTPUT_LINK_TRAVEL_TIMES |
							   OUTPUT_TRAVEL_TIMES_TABLE)) {
	return;
  }

  mesoNetwork()->recordLinkTravelTimeOfActiveVehicle();
	  
  if (theEngine->chosenOutput(OUTPUT_LINK_TRAVEL_TIMES)) {
	const char *filename = ToolKit::outfile(linkFlowTravelTimesFile_);
	cout << "Writing link flows and travel times in <"
		 << filename << ">." << endl;
	mesoNetwork()->outputLinkTravelTimes(filename);
  }

  if (theEngine->chosenOutput(OUTPUT_TRAVEL_TIMES_TABLE)) {
	const char *filename = ToolKit::outfile(linkTravelTimesFile_);
	cout << "Writing link travel times in <"
		 << filename << ">" << endl;
	mesoNetwork()->outputReloadableLinkTravelTimes(filename);
  }

  if (theEngine->chosenOutput(OUTPUT_VEHICLE_DEP_RECORDS)) {
	MESO_Vehicle::closeDepartureRecords();
  }

  // Write vehicles waiting too long into the log file

  if (ToolKit::verbose()) {
	logSuspiciousVehicles();
  }
}
