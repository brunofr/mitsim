//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// NOTE: This file is compiled in the graphical mode only
// AUTH: Qi Yang
// FILE: MESO_DrawableVehicle.C
// DATE: Mon Mar  4 17:29:25 1996
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <cstdio>
#include <sstream>
using namespace std;

#include <Xmt/Xmt.h>
#include <Xmt/Dialogs.h>

#include <Tools/Math.h>
#include <Tools/SimulationClock.h>
#include <Tools/ToolKit.h>

#include <GRN/WcsPoint.h>
#include <GRN/Constants.h>
#include <GRN/RN_Path.h>
#include <GRN/RN_Node.h>

#include <DRN/DRN_DrawingArea.h>
#include <DRN/DRN_Interface.h>

#include "MESO_Network.h"
#include "MESO_Menu.h"
#include "MESO_DrawableVehicle.h"
#include "MESO_Lane.h"
#include "MESO_Segment.h"
#include "MESO_Link.h"
#include "MESO_TrafficCell.h"
#include "MESO_Parameter.h"
#include "MESO_Symbols.h"
#include "MESO_Interface.h"

MESO_DrawableVehicle::MESO_DrawableVehicle()
   : MESO_Vehicle(),
     onScreen_(0)
{
}


Pixel
MESO_DrawableVehicle::calcColor()
{
   Pixel clr;

   switch (theSymbols()->vehicleBorderCode().value()) {
      case MESO_Symbols::VEHICLE_BORDER_TYPE:
      {
		 clr = theParameter->vehicleColors(type());
		 break;
      }
      case MESO_Symbols::VEHICLE_BORDER_INFO:
      {
		 if (isGuided()) {
			if (path()) {
			   clr = theColorTable->cyan();
			} else {
			   clr = theColorTable->lightGreen();
			}
		 } else {
			if (path()) {
			   clr = theColorTable->red();
			} else {
			   clr = theColorTable->yellow();
			}
		 }
		 break;
      }
      case MESO_Symbols::VEHICLE_BORDER_SPEED:
      {
		 clr = theDrawingArea->speedColor(currentSpeed());
		 break;
      }
      case MESO_Symbols::VEHICLE_BORDER_MOVEMENT:
      {
		 clr = colorByTurnMovement();
		 break;
      }
      default:
      {
		 clr = theColorTable->white();
		 break;
      }
   }

   return clr;
}


Pixel
MESO_DrawableVehicle::colorByTurnMovement()
{
   if (!nextLink_) {
      return theColorTable->white();
   }
   float x = (float)nextLink_->dnIndex() / (1.0 + link()->nDnLinks());
   return theColorTable->color(x);
}


// Draw this traffic cell with a set of bars. The color of each bar
// represents the speed of the vehicles.

int
MESO_DrawableVehicle::draw(DRN_DrawingArea *area)
{
   erase(area);		// erase previous image if any

   onScreen_ = calcPoints(area);
   if (onScreen_) {
      color_ = calcColor();
      drawVehicle(area);
   }
   return onScreen_;
}


// Erase the previous image

void
MESO_DrawableVehicle::erase(DRN_DrawingArea *area)
{
   if (area->state(DRN_DrawingArea::VEHICLE_ON) && onScreen_)
   {
      drawVehicle(area);
   }
   onScreen_ = 0;
}


// Draw a vehicle.  Head and tail of the cell are shown different.

void
MESO_DrawableVehicle::drawVehicle(DRN_DrawingArea * area)
{
   area->setXorMode();
   area->foreground(color_);
   area->drawLines(points_, 5);

   if (! theSymbols()->isVehicleLabelOn().value() || 
       area->resolution() < DRN_DrawingArea::HIGHEST_RESOLUTION) {
	  return;
   }

   char label[12];

   XmwPoint p((points_[0].x + points_[2].x) / 2,
				   (points_[0].y + points_[2].y) / 2);
   sprintf(label, "%d", code());
   area->foreground(theColorTable->yellow());
   area->XmwDrawingArea::drawCString(label, p);
}


// Calculate the position on screen

int
MESO_DrawableVehicle::calcPoints(DRN_DrawingArea *area)
{
   MESO_Segment *ps = trafficCell_->segment();
   if (!ps->isVisible()) return 0;
   
   short int n = segment()->nLanes();
   float width = 0.35 * n * LANE_WIDTH;
   float len = 0.85 * spaceInSegment();

   float front_ratio =  distance_ / ps->length();
   float back_ratio = (len + distance_) / ps->length();

   WcsPoint front(ps->centerPoint(front_ratio));
   WcsPoint back(ps->centerPoint(back_ratio));

   float angle = back.angle(front);

   // back/left corner

   WcsPoint p1 (back.bearing(width, angle + HALF_PI));
   
   // front/left corner 

   WcsPoint p2 (front.bearing(width, angle + HALF_PI));

   // front/right corner

   WcsPoint p3 (front.bearing(width, angle - HALF_PI));

   // back/right corner

   WcsPoint p4 (back.bearing(width, angle - HALF_PI));

   // We do not draw unless the bar is complete visible

   int visible = (area->world2Window(p1, points_[0]) &&
				  area->world2Window(p2, points_[1]) &&
				  area->world2Window(p3, points_[2]) &&
				  area->world2Window(p4, points_[3]));
   points_[4] = points_[0];

   return visible;
}


void
MESO_DrawableVehicle::query()
{
   SimulationClock *clk = theSimulationClock;
   MESO_Parameter *p = theParameter;

   ostringstream data;

   float cur_spd = currentSpeed() / p->speedFactor();

   data << "Plate No = " << code_ << '\n'
		<< "Type = " << type_ << '\n'
		<< "Departure Time = " << clk->convert(departTime_) << '\n'
		<< "Origin = " << oriNode()->code() << '\n'
		<< "Destination = " << desNode()->code() << '\n'
		<< "Position = "
		<< link()->code() << "/"
		<< segment()->code() << '\n'
		<< p->speedLabel() << " = "
		<< Fix(cur_spd, (float)1.0);

   if (path_) {
      data << '\n'
		   << "Path = " << path_->code();
   }

   if (nextLink_) {
      data << '\n'
		   << "Next Link = " << nextLink_->code();
   }

   //data << null;

   XmtDisplayInformation(theInterface->widget(),
						 NULL, // msg_name,
						 data.str().c_str(), // msg_default
						 clk->currentStringTime());


}

#endif
