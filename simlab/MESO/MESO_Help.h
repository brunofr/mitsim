//-*-c++-*------------------------------------------------------------
// NAME: Additional help info for mitsim
// AUTH: Qi Yang
// FILE: MESO_Help.h
// DATE: Tue May  7 17:22:44 1996
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI
#ifndef MESO_HELP_HEADER
#define MESO_HELP_HEADER

void CreateHelpMsg();

#endif
#endif
