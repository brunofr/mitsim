//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_Signal.C
// DATE: Mon Feb 26 18:29:42 1996
//--------------------------------------------------------------------

#include "MESO_Signal.h"

// The signals currently use the base class defined in
// TC/Signal.h.  This file is left empty now.
