//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_Segment.h
// DATE: Mon Feb 26 17:40:25 1996
//--------------------------------------------------------------------

#ifndef MESO_SEGMENT_HEADER
#define MESO_SEGMENT_HEADER

class MESO_Vehicle;
class MESO_TrafficCell;
class WcsPoint;

//#include <fstream>
#include <iosfwd>
#include <Tools/SimulationClock.h>

#ifdef INTERNAL_GUI
#include <DRN/DRN_Segment.h>
#else
#include <GRN/RN_Segment.h>
#define DRN_Segment RN_Segment
#endif

class MESO_Segment : public DRN_Segment
{
  friend class MESO_Vehicle;
  friend class MESO_TrafficCell;

public:

  MESO_Segment();
  ~MESO_Segment();

  inline MESO_Segment* upstream() {
	return (MESO_Segment*) RN_Segment::upstream();
  }
  inline MESO_Segment* downstream() {
	return (MESO_Segment*) RN_Segment::downstream();
  }

  inline int nVehicles();

  inline MESO_Vehicle* firstVehicle();
  inline MESO_Vehicle* lastVehicle();

  inline MESO_TrafficCell* firstCell() { return firstCell_; }
  inline MESO_TrafficCell* lastCell() { return lastCell_; }

  void append(MESO_TrafficCell *);
  void remove(MESO_TrafficCell *);

  float density();
  float speed();
  int flow();
  float calcDensity(void);
  float calcSpeed(void);
  int calcFlow(void);

  void calcTrafficCellUpSpeed();
  void calcTrafficCellDnSpeeds();

  int isJammed();

  // Move vehicles based on their speeds

  void advanceVehicles();

  // Remove, merge and split traffic cells

  void formatTrafficCells();

  // Append a vehicle at the end of the segment

  void append(MESO_Vehicle*);
 
  // Insert a vehicle into the segment based on its distance
  // position
	  
  void insert(MESO_Vehicle*);

  // Maximum speed for a given gap

  float maxSpeed(float gap);
  float maxSpeed() { return freeSpeed_; }

  float defaultCapacity();

  // Update number of vehicle allowed to move out.

  inline void resetEmitTime() {	// once every update step
	emitTime_ = theSimulationClock->currentTime();
	scheduleNextEmitTime();
  }

  inline void scheduleNextEmitTime() {
	if (capacity_ > 1.E-6) {
	  emitTime_ += 1.0 / capacity_;
	} else {
	  emitTime_ = DBL_INF;
	}
  }

  inline int isMoveAllowed() {
	return (theSimulationClock->currentTime() >= emitTime_);
  }

  // Add cap to current capacity
      
  void addCapacity(float cap_delta);

  // Set current capacity to cap

  void setCapacity(float cap);

  // Overload the base class function

  void calcStaticInfo();

  void clean();		// remove all traffic cells

#ifdef INTERNAL_GUI
  void changeCapacity();
  void queryVehicle(WcsPoint& pressed);
#endif

  void printSdIndex(std::ostream &os);	// virtual

private:

  short int nCells_;		// num of TCs

  MESO_TrafficCell *firstCell_; // first downstream traffic cell
  MESO_TrafficCell *lastCell_; // last upstream traffic cell
      
  double capacity_;			// default capacity (vps)
  double emitTime_;			// time to move next vehicle

  float density_;			// current density
  float speed_;				// current speed
};

#endif
