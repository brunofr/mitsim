//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_DrawableVehicle.h
// DATE: Mon Mar  4 17:10:14 1996
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#ifndef MESO_DRAWABLE_VEHICLE_HEADER
#define MESO_DRAWABLE_VEHICLE_HEADER

#include <DRN/DRN_DrawingArea.h>
#include "MESO_Vehicle.h"


class MESO_DrawableVehicle : public MESO_Vehicle
{
   protected:

      // These variables are used to erase previous images

      Pixel color_;		/* previous color (0=invisible) */
      XPoint points_[5];	/* 4 vertices:
				 * 0 = back_left,  1 = front_left,
				 * 3 = back_right, 2 = front_right
				 * 4 = back_left
				 */
      short int onScreen_;	/* 1 = visible */

   private:

      int calcPoints(DRN_DrawingArea * view_area);
      Pixel calcColor();
      Pixel colorByTurnMovement();

   public:

      MESO_DrawableVehicle();
      ~MESO_DrawableVehicle() { }

      inline int isVisible() { return onScreen_; }
	  int isShaded();			// return true to draw in filled rect

      int draw(DRN_DrawingArea *);
      void erase(DRN_DrawingArea *);

      void drawVehicle(DRN_DrawingArea *);

      void resetDrawing() { onScreen_ = 0; }

      void query();
};

extern int theVehicleShadeParam[];

#endif
#endif // INTERNAL_GUI
