//-*-c++-*------------------------------------------------------------
// MESO_Interface.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class implementation for the class
// MESO_Interface
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <iostream>
#include <cstdlib>
using namespace std;

#include <Xmt/Xmt.h>
#include <Xmt/Create.h>

#include <Xmw/XmwImage.h>
#include <Xmw/XmwColor.h>

#include "MESO_Symbols.h"
#include "MESO_Interface.h"
#include "MESO_DrawingArea.h"
#include "MESO_Menu.h"
#include "MESO_Modeline.h"
#include "MESO_Icon.cc"

MESO_Interface* theInterface = NULL;

MESO_Interface::MESO_Interface(int *argc, char **argv)
   : DRN_Interface(argc, argv,
				   new XmwImage("meso", icon_bits, NULL,
								icon_width, icon_height))
{
   extern void CreateHelpMsg() ;
   CreateHelpMsg() ;
   theInterface = this;
   create();
}

MESO_Interface::~MESO_Interface()
{
}

void MESO_Interface::create()		// virtual
{
   DRN_Interface::create();
   manage ();
}

XmwSymbols* MESO_Interface::createSymbols()
{
   return new MESO_Symbols;
}

XmwMenu* MESO_Interface::menu(Widget parent)
{
   return new MESO_Menu(parent);
}

XmwModeline* MESO_Interface::modeline(Widget parent)
{
   return new MESO_Modeline(parent);
}

DRN_DrawingArea* MESO_Interface::drawingArea(Widget w)
{
   return new MESO_DrawingArea(w);
}

#endif // INTERNAL_GUI
