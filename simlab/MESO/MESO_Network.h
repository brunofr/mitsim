//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_Network.h
// DATE: Mon Feb 26 16:53:49 1996
//--------------------------------------------------------------------

#ifndef MESO_NETWORK_HEADER
#define MESO_NETWORK_HEADER

#include <iostream>

#ifdef INTERNAL_GUI
class DRN_DrawingArea;
#endif

class Reader;
class IOService;
class MESO_Node;
class MESO_Link;
class MESO_Segment;
class MESO_Lane;

#ifdef INTERNAL_GUI
#include <DRN/DrawableRoadNetwork.h>
class MESO_Network : public DrawableRoadNetwork
#else  // match mode
#include <GRN/RoadNetwork.h>
class MESO_Network : public RoadNetwork
#endif // !INTERNAL_GUI

{				// MESO_Network
   public:

      MESO_Network();
      ~MESO_Network();

      // Overload the virtual function defined in the base class

      RN_Node* newNode();
      RN_Link* newLink();
      RN_Segment* newSegment();
      RN_Lane* newLane(); 
      RN_Sensor* newSensor();
      RN_Signal* newSignal();
      RN_TollBooth* newTollBooth();

      inline MESO_Node * mesoNode(int i) {
		 return (MESO_Node *) node(i);
      }
      inline MESO_Link * mesoLink(int i) {
		 return (MESO_Link *) link(i);
      }
      inline MESO_Segment * mesoSegment(int i) {
		 return (MESO_Segment *) segment(i);
      }

      void calcStaticInfo(void);
      void organize();

      void resetSensorReadings();
      void calcSegmentData();

      void enterVehiclesIntoNetwork();

      void resetSegmentEmitTime();

	  void guidedVehiclesUpdatePaths();

	  // Calculate capacity of the nodes

	  void updateNodeCapacities();

      // Update phase

      void calcTrafficCellUpSpeed();
      void calcTrafficCellDnSpeeds();

      // Advance phase

      void advanceVehicles();

	  // Remove, merge, and split cells

	  void formatTrafficCells();

      // Remove all vehicles in the network, including those in
      // pretrip queues

      void clean();
      
      // Interface to MITSIM

      long loadInitialState(const char *name, int check_time = 1);

      void saveLinkTravelTimes();

	  // Save state, return error code (0 = no error)

	  int dumpNetworkState(const char *filename);

	  // Debugging

	  int watchQueueLength(std::ostream& os = std::cout);

#ifdef INTERNAL_GUI
      void unsetDrawingStates(DRN_DrawingArea *); // virtual
      void drawNetwork(DRN_DrawingArea *);
      void drawTrafficCells(DRN_DrawingArea *); // virtual
	  void queryVehicle(const XmwPoint&);
	  void setIncident(const XmwPoint&);
#endif

	  void recordLinkTravelTimeOfActiveVehicle();
};

inline MESO_Network * mesoNetwork() {
   return (MESO_Network *)theNetwork;
}

#endif
