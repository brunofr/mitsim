//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_ODCell.C
// DATE: Mon Feb 26 17:37:37 1996
//--------------------------------------------------------------------

#include <GRN/RN_Link.h>

#include "MESO_ODTable.h"
#include "MESO_ODCell.h"
#include "MESO_VehicleList.h"
#include "MESO_Vehicle.h"

/*
 * Create vehicle based on current departure rate. If space is
 * available in the start link, enter the vehicle into the network.
 */

void
MESO_ODCell::emitVehicles()
{
   MESO_Vehicle *pv;
   while (pv = (MESO_Vehicle *)OD_Cell::emitVehicle()) {
      pv->enterPretripQueue();
   }
}


RN_Vehicle* 
MESO_ODCell::newVehicle()
{
   return theVehicleList->recycle();
}
