//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_Status.h
// DATE: Mon Feb 26 18:31:13 1996
//--------------------------------------------------------------------

#ifndef MESO_STATUS_HEADER
#define MESO_STATUS_HEADER

#include <fstream>

class MESO_Status
{
   public:

      MESO_Status();
      ~MESO_Status();

      std::ofstream& osLogFile() { return osLogFile_; }

      void openLogFile();
      void closeLogFile();
	  void clean();

      int nActive(int n) { return nActive_ += n; }
      int nArrived(int n) { return nArrived_ += n; }
      int nNoPath(int n) { return nNoPath_ += n; }
      int nInQueue(int n) { return nInQueue_ += n; }
      int nCells(int n) { return nCells_ += n; }

      int nActive() { return nActive_; }
      int nArrived() { return nArrived_; }
      int nInQueue() { return nInQueue_; }
      int nCells() { return nCells_; }

      inline void nErrors(int n) { nErrors_ += n; }
      inline int nErrors() { return nErrors_; }
	  inline void nMsgs(int n) { nMsgs_ += n; }

      void report(std::ostream &os = std::cout);
      void print(std::ostream &os = std::cout);

#ifdef INTERNAL_GUI
      void showStatus();
#endif

   protected:

      char* logFile_;
      int nErrors_;				// number of errors (a fatal error
	  int nMsgs_;
								// will terminate the program)
      std::ofstream osLogFile_;		// error log

      int nActive_;				// number of vehicle in network
      int nArrived_;			// number of vehicle arrived
      int nNoPath_;				// no path
      int nInQueue_;			// number of vehicle queuing outside
      int nCells_;				// number of traffic cells
};

extern MESO_Status *theStatus;

#endif
