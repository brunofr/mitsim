//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_Link.C
// DATE: Mon Feb 26 16:37:37 1996
//--------------------------------------------------------------------

#include <cstdio>
#include <iostream>
#include <new>
using namespace std;

#include <Tools/Math.h>

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>

#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>

#include "MESO_Network.h"
#include "MESO_Node.h"
#include "MESO_Link.h"
#include "MESO_Segment.h"
#include "MESO_Lane.h"
#include "MESO_Vehicle.h"
#include "MESO_Status.h"
#include "MESO_VehicleList.h"
#include "MESO_TrafficCell.h"
#include "MESO_Parameter.h"
#include "MESO_Engine.h"


MESO_Link::MESO_Link()
   : RN_Link(),	
     queueHead_(NULL),
     queueTail_(NULL),
     queueLength_(0)
{
}


void
MESO_Link::clean()
{
   // Remove vehicles in pretrip queue

   while (queueHead_) {
      queueTail_ = queueHead_;
      queueHead_ = queueHead_->trailing_;
      theVehicleList->recycle(queueTail_);
   }
   queueTail_ = NULL;
   queueLength_ = 0;

   // Remove vehicles in each segment

   RN_Segment *ps = startSegment();
   while (ps) {
      ((MESO_Segment *)ps)->clean();
      ps = ps->downstream();
   }
}


/*
 *-------------------------------------------------------------------
 * Mark a downstream link as connected if there is at least one
 * connection between the lanes in this link to the lanes in the
 * downstream link.
 *-------------------------------------------------------------------
 */

void
MESO_Link::checkConnectivity()
{
   // Check if this has been down in GRN
}


/*
 * add a vehicle to the end of the queue.
 */

void
MESO_Link::queue(MESO_Vehicle* vehicle)
{
   if (queueTail_ != NULL) {	/* current queue is not empty */
      queueTail_->trailing(vehicle);
      vehicle->leading(queueTail_);
      queueTail_ = vehicle;
   } else {			/* current queue is empty */
      vehicle->leading(NULL);
      queueHead_ = queueTail_ = vehicle;
   }
   vehicle->trailing(NULL);
   queueLength_ ++;
   ::theStatus->nInQueue(1);
}


/*
 * add a vehicle to the beginning of the queue.
 */

void
MESO_Link::prequeue(MESO_Vehicle* vehicle)
{
   if (queueHead_ != NULL) {	/* current queue is not empty */
      queueHead_->leading(vehicle);
      vehicle->trailing(queueHead_);
      queueHead_ = vehicle;
   } else {			/* current queue is empty */
      vehicle->trailing(NULL);
      queueHead_ = queueTail_ = vehicle;
   }
   vehicle->leading(NULL);
   queueLength_ ++;
   ::theStatus->nInQueue(1);
}


/*
 * remove vehicle "pv" from the queue.
 */

void
MESO_Link::dequeue(MESO_Vehicle* pv)
{
   if (pv->leading()) {
      pv->leading()->trailing(pv->trailing());
   } else {			/* first vehicle in the queue */
      queueHead_ = pv->trailing();
   }
   if (pv->trailing()) {
      pv->trailing()->leading(pv->leading());
   } else {			/* last vehicle in the queue */
      queueTail_ = pv->leading();
   }
   queueLength_ --;
   ::theStatus->nInQueue(-1);
}


/* 
 * Print the number of vehicles queue for enter a link
 */

int
MESO_Link::reportQueueLength(ostream &os)
{
   const int MinQueueStepSize = 20;
   if (queueLength_ > MinQueueStepSize) {
	  os << " (" << code_ << ":"
		 << queueLength_ << ")";
      return 1;
   }
   return 0;
}


/*
 * Returns the 1st traffic cell in this link
 */

MESO_TrafficCell *
MESO_Link::firstCell()
{
   return ((MESO_Segment*)endSegment())->firstCell();
}


/*
 * Returns the lasst traffic cell in this link
 */

MESO_TrafficCell *
MESO_Link::lastCell()
{
   return ((MESO_Segment*)startSegment())->lastCell();
}


/*
 * Returns 1 if this link cannot accept more vehicles at the upstream
 * end
 */

int
MESO_Link::isJammed()
{
   return ((MESO_Segment*)startSegment())->isJammed();
}


/*
 * Move vehicles based on current cell speeds.  This function is
 * called by MESO_Node::advanceVehicles() in random permuted order.
 */

void
MESO_Link::advanceVehicles()
{
   MESO_Segment *ps = (MESO_Segment *) endSegment();
   while (ps) {
      ps->advanceVehicles();
	  ps->formatTrafficCells();
      ps = ps->upstream();
   }
}


/*
 * Add a vehicle at the upstream end of the link.
 */

void
MESO_Link::append(MESO_Vehicle* vehicle)
{
   MESO_Segment *ps = (MESO_Segment*) startSegment();
   ps->append(vehicle);
}


float
MESO_Link::calcTravelTime()
{
   MESO_Segment *ps = (MESO_Segment *)startSegment();
   MESO_TrafficCell *pc;
   MESO_Vehicle *pv;
   double sum = 0.0;
   int cnt = 0;
   float tt = travelTime();

   // Vehicles already on the link

   while (ps) {
      pc = firstCell();
      while (pc) {
		 pv = pc->firstVehicle();
		 while (pv) {
		    float pos = pv->distanceFromDownNode();
			sum += pv->timeInLink() + pos / length() * tt;
			cnt ++;
			pv = pv->trailing();
		 }
		 pc = pc->trailing();
      }
      ps = (MESO_Segment *) ps->downstream();
   }

   // Vehicles in pretrip queue

   pv = queueHead_;
   while (pv) {
	  sum += pv->timeInLink() + tt * 1.25;
      cnt ++;
      pv = pv->trailing();
   }

   if (cnt) {
	  return sum / cnt;
   } else {
	  return tt;
   }
}
