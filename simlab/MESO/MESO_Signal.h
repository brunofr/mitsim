//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_Signal.h
// DATE: Mon Feb 26 18:28:13 1996
//--------------------------------------------------------------------

#ifndef MESO_SIGNAL_HEADER
#define MESO_SIGNAL_HEADER

#include <TC/TC_Signal.h>

#define MESO_Signal	TC_Signal

#endif
