//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// NOTE: Parse vehicles from a file
// AUTH: Qi Yang
// FILE: MESO_VehicleTable.C
// DATE: Tue Mar  5 23:11:10 1996
//--------------------------------------------------------------------

#include "MESO_VehicleTable.h"
#include "MESO_Vehicle.h"
#include "MESO_VehicleList.h"

RN_Vehicle*
MESO_VehicleTable::newVehicle()
{
   return theVehicleList->recycle();
}
