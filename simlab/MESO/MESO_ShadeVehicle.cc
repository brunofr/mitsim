//-*-c++-*------------------------------------------------------------
// AUTH: Qi Yang
// FILE: MESO_ShadeVehicle.C
// DATE: Tue Aug 13 13:24:14 1996
//--------------------------------------------------------------------

#include <Tools/VariableParser.h>
#include <Tools/Math.h>

#ifdef INTERNAL_GUI
#include "MESO_Symbols.h"
#include "MESO_Interface.h"
#include "MESO_DrawableVehicle.h"
#else
#include "MESO_Vehicle.h"
#endif

#include "MESO_Status.h"
#include "MESO_FileManager.h"
#include "MESO_Network.h"
#include "MESO_Node.h"
#include "MESO_Segment.h"
#include "MESO_Lane.h"
#include "MESO_TrafficCell.h"
#include "MESO_Parameter.h"

// Shade the vehicle in graphics

int theVehicleShadeParam[] = {
   0,
   ONE_DAY,			// time since in the segment
   ONE_DAY			// time since departure 
};

int
loadVehicleShadeParams(GenericVariable &gv)
{
   int i, n = gv.nElements();
   if (n != 3) return Parameter::error(gv.name());
   for (i = 0; i < n; i ++) {
      theVehicleShadeParam[i] = gv.element(i);
   }
#ifdef INTERNAL_GUI   
   theSymbols()->vehicleShadeCode().set(theVehicleShadeParam[0]);
#endif
   return 0;
}


void logSuspiciousVehicles()
{
   if (theVehicleShadeParam[2] <= 0 ||
	   theVehicleShadeParam[2] >= ONE_DAY) {
	  // This feature not wanted
	  return;
   }

   theStatus->osLogFile()
      << endl << "Suspicious vehicles at "
      << theSimulationClock->currentTime() << endl
      << "ID\t" << "Ori\t" << "Des\t"
      << "Lane\t" << "Pos\t" << "Time" << endl;

   int i, n = mesoNetwork()->nSegments();
   MESO_Segment *ps;
   MESO_TrafficCell *cell;
   MESO_Vehicle *pv;
   for (i = 0; i < n; i ++) {
	  ps = mesoNetwork()->mesoSegment(i);
	  cell = ps->firstCell();
	  while (cell) {
		 pv = cell->firstVehicle();
		 while (pv) {
			pv->logInfo();
			pv = pv->trailing();
		 }
		 cell = cell->trailing();
      }
   }
}


int MESO_Vehicle::logInfo()
{
   if (timeSinceDeparture() < theVehicleShadeParam[2]) return 0;

   theStatus->osLogFile()
      << code() << "\t"
      << oriNode()->code() << "\t"
      << desNode()->code() << "\t"
      << segment()->code() << "\t"
      << Fix(distance(), (float)1.0) << "\t"
      << Fix(timeSinceDeparture(), (float)0.1) << endl;
   theStatus->nErrors(+1);

   return 1;
}

#ifdef INTERNAL_GUI

int MESO_DrawableVehicle::isShaded()
{
   switch (theSymbols()->vehicleShadeCode().value()) {

	  case MESO_Symbols::VEHICLE_SHADE_WAITING:
		 return (timeInLink() > theVehicleShadeParam[1]);

	  default:
		 break;
   }

   return 0;
}

#endif
