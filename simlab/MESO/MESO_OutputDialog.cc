//-*-c++-*------------------------------------------------------------
// MESO_OutputDialog.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <cassert>

#include <Xmt/Xmt.h>
#include <Xmt/Chooser.h>
#include <Xmt/InputField.h>
#include <Xm/ToggleB.h>

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>

#include "MESO_OutputDialog.h"
#include "MESO_Interface.h"
#include "MESO_Engine.h"
#include "MESO_FileManager.h"
#include "MESO_Interface.h"
#include "MESO_Menu.h"
using namespace std;

MESO_OutputDialog::MESO_OutputDialog ( Widget parent )
   : XmwDialogManager(parent, "outputDialog", NULL, 0)
{
   XtVaSetValues(widget_,
				 XmNdialogStyle, XmDIALOG_PRIMARY_APPLICATION_MODAL,
				 NULL);
   Widget group;
   XmwCheckerField *fld;

   group = XmtNameToWidget(widget_, "*GeneralGroup");
   assert(group);

   outDirFld_ = XmtNameToWidget(group, "*outDir");
   assert(outDirFld_);
   isHeadingOnFld_ = XmtNameToWidget(group, "*header");
   assert(isHeadingOnFld_);
   formatFld_ = XmtNameToWidget(group, "*format");
   assert(formatFld_);

   fileFlds_.reserve(6);

   group = XmtNameToWidget(widget_, "*NetworkGroup");
   assert(group);

   fld = new XmwCheckerField(group, "o11", OUTPUT_LINK_TRAVEL_TIMES);
   fileFlds_.push_back(fld);
   fld = new XmwCheckerField(group, "o12", OUTPUT_TRAVEL_TIMES_TABLE);
   fileFlds_.push_back(fld);
   fld = new XmwCheckerField(group, "o13", OUTPUT_STATE_3D);
   fileFlds_.push_back(fld);

   group = XmtNameToWidget(widget_, "*VehicleGroup");
   assert(group);

   fld = new XmwCheckerField(group, "o21", OUTPUT_VEHICLE_DEP_RECORDS);
   fileFlds_.push_back(fld);
   fld = new XmwCheckerField(group, "o22", OUTPUT_VEHICLE_LOG);
   fileFlds_.push_back(fld);
   fld = new XmwCheckerField(group, "o23", OUTPUT_VEHICLE_PATH_RECORDS);
   fileFlds_.push_back(fld);
}

MESO_OutputDialog::~MESO_OutputDialog()
{
   for (int i = 0; i < fileFlds_.size(); i ++) {
	  delete fileFlds_[i];
   }
}

// These overload the functions in base class

void MESO_OutputDialog::cancel(Widget, XtPointer, XtPointer)
{
   unmanage();
}


void MESO_OutputDialog::okay(Widget, XtPointer, XtPointer)
{
   // Copy the data from dialog 

   ToolKit::outdir(XmtInputFieldGetString(outDirFld_));

   theFileManager->linkFlowTravelTimesFile(fileFlds_[0]->get());
   theFileManager->linkTravelTimesFile(fileFlds_[1]->get());
   theFileManager->state3d(fileFlds_[ 2]->get());
   theFileManager->depRecordFile(fileFlds_[3]->get());
   theFileManager->vehicleFile(fileFlds_[4]->get());
   theFileManager->pathRecordFile(fileFlds_[5]->get());

   if (XmToggleButtonGetState(isHeadingOnFld_)) {
	  theEngine->chooseOutput(OUTPUT_SKIP_COMMENT);
   } else {
	  theEngine->removeOutput(OUTPUT_SKIP_COMMENT);
   }

   if (XmtChooserGetState(formatFld_)) {
	  theEngine->chooseOutput(OUTPUT_RECT_TEXT);
   } else {
	  theEngine->removeOutput(OUTPUT_RECT_TEXT);
   }

   for (int i = 0; i < fileFlds_.size(); i ++) {
	  if (fileFlds_[i]->isChecked()) {
		 theEngine->chooseOutput(fileFlds_[i]->mask());
	  } else {
		 theEngine->removeOutput(fileFlds_[i]->mask());
	  }
   }

   unmanage();
   theMenu->needSave();
}


void MESO_OutputDialog::help(Widget, XtPointer, XtPointer)
{
   theDrnInterface->openUrl("chooseOutput", "meso.html");
}


void MESO_OutputDialog::post()
{
   // Copy data to dialog

   XmtInputFieldSetString(outDirFld_, ToolKit::outdir());

   fileFlds_[0]->set(theFileManager->linkFlowTravelTimesFile_);
   fileFlds_[1]->set(theFileManager->linkTravelTimesFile_);
   fileFlds_[2]->set(theFileManager->state3d_);
   fileFlds_[3]->set(theFileManager->depRecordFile_);
   fileFlds_[4]->set(theFileManager->vehicleFile_);
   fileFlds_[5]->set(theFileManager->pathRecordFile_);

   if (theEngine->skipComment()) {
	  XmToggleButtonSetState(isHeadingOnFld_, False, False);
   } else {
	  XmToggleButtonSetState(isHeadingOnFld_, True, False);
   }

   if (theEngine->isRectangleFormat()) {
	  XmtChooserSetState(formatFld_, 1, False);
   } else {
	  XmtChooserSetState(formatFld_, 0, False);
   }

   // Set the check state

   for (int i = 0; i < fileFlds_.size(); i ++) {
	  if (theEngine->chosenOutput(fileFlds_[i]->mask())) {
		 fileFlds_[i]->check(True);
	  } else {
		 fileFlds_[i]->check(False);
	  }
   }

   // Do not allow changes after simulation is started

   if (theSimulationClock->isStarted()) {
	  deactivate();
   } else {
	  activate();
   }
   
   XmwDialogManager::post();
}

void MESO_OutputDialog::activate()
{
   if (XtIsSensitive(okay_)) return;
   XmwDialogManager::activate(outDirFld_);
   XmwDialogManager::activate(isHeadingOnFld_);
   XmwDialogManager::activate(formatFld_);
   for (int i = 0; i < fileFlds_.size(); i ++) {
	  XmwDialogManager::activate(fileFlds_[i]->checker());
	  XmwDialogManager::activate(fileFlds_[i]->input());
   }
   XmwDialogManager::activate(okay_);
}

void MESO_OutputDialog::deactivate()
{
   if (!XtIsSensitive(okay_)) return;
   XmwDialogManager::deactivate(outDirFld_);
   XmwDialogManager::deactivate(isHeadingOnFld_);
   XmwDialogManager::deactivate(formatFld_);
   for (int i = 0; i < fileFlds_.size(); i ++) {
	  XmwDialogManager::deactivate(fileFlds_[i]->checker());
	  XmwDialogManager::deactivate(fileFlds_[i]->input());
   }
   XmwDialogManager::deactivate(okay_);
}

#endif // INTERNAL_GUI
