//-*-c++-*------------------------------------------------------------
// Meso.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <iostream>
using namespace std;

#include <Tools/ToolKit.h>

#include "MESO_Engine.h"
#include "MESO_Setup.h"
#include "MESO_Version.h"
#include "MESO_CmdArgsParser.h"

#ifdef INTERNAL_GUI
#include "MESO_Interface.h"
#endif

int main ( int argc, char **argv )
{
   ::Welcome(argv[0]);

   ToolKit::initialize();

   MESO_Engine engine;

#ifdef INTERNAL_GUI
   theInterface = new MESO_Interface(&argc, argv);
#endif

   MESO_CmdArgsParser cp;
   cp.parse(&argc, argv);

   engine.run();

   engine.quit(STATE_DONE);

   return 0;
}
