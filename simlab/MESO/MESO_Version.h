//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_Version.h
// DATE: Mon Feb 12 09:05:09 1996
//--------------------------------------------------------------------

#ifndef MESOMESO_VERSION_HEADER
#define MESOMESO_VERSION_HEADER

extern char*  g_majorVersionNumber;
extern char*  g_minorVersionNumber;
extern char*  g_codeDate;

extern void Welcome(char *);

#endif
