//-*-c++-*------------------------------------------------------------
// MESO_Menu.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef MESO_MENU_HEADER
#define MESO_MENU_HEADER

#include <DRN/DRN_Menu.h>

class MESO_Menu : public DRN_Menu
{
	  friend class MESO_Interface;
	  friend class MESO_KeyInteractor;
	  
	  CallbackDeclare(MESO_Menu);

   public:

	  MESO_Menu(Widget parent);
	  ~MESO_Menu() { }

   protected:

	  // New callbacks
	  
	  void randomizer ( Widget, XtPointer, XtPointer );
	  void setup ( Widget, XtPointer, XtPointer );
	  void advancedSetup ( Widget, XtPointer, XtPointer );
	  void output ( Widget, XtPointer, XtPointer );
	  void vehicle ( Widget, XtPointer, XtPointer );

   private:

	  static void browsePath ( Widget, XtPointer, XtPointer );
};

#endif
