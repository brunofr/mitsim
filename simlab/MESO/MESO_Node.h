//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_Node.h
// DATE: Mon Feb 26 17:32:40 1996
//--------------------------------------------------------------------

#ifndef MESO_NODE_HEADER
#define MESO_NODE_HEADER

class MESO_Link;
class MESO_Vehicle;
class MESO_TrafficCell;

#ifdef INTERNAL_GUI
#include <DRN/DRN_Node.h>
class MESO_Node : public DRN_Node
#else
#include <GRN/RN_Node.h>
class MESO_Node : public RN_Node
#endif
{
   public:

      MESO_Node();
 
      ~MESO_Node();

      void calcStaticInfo();	// virtual
      void calcCapacities();
      
   protected:

      short int * supply_;	// remaining capacity for each movement

   private:

      static short int * demand_; // for each movement

      void calcDemands();
      void calcSupplies();
};

#endif
