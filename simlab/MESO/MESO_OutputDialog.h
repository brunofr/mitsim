//-*-c++-*------------------------------------------------------------
// MESO_OutputDialog.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI
#ifndef MESO_OUTPUTDIALOG_HEADER
#define MESO_OUTPUTDIALOG_HEADER

#include <vector>
#include <Xmw/XmwDialogManager.h>
#include <Xmw/XmwCheckerField.h>

class MESO_OutputDialog : public XmwDialogManager
{
public:

  MESO_OutputDialog(Widget parent);
  ~MESO_OutputDialog();
  void post();				// virtual

private:

  void activate();
  void deactivate();

  // overload the virtual functions in base class

  void okay(Widget, XtPointer, XtPointer);
  void cancel(Widget, XtPointer, XtPointer);
  void help(Widget, XtPointer, XtPointer);

private:
	  
  Widget outDirFld_;		// XmtInputField
  std::vector<XmwCheckerField*> fileFlds_;
  Widget isHeadingOnFld_;	// XmToggleButton
  Widget formatFld_;		// XmwChooser
};

#endif
#endif // INTERNAL_GUI
