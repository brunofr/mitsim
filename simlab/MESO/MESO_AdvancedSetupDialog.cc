//-*-c++-*------------------------------------------------------------
// MESO_AdvancedSetupDialog.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <cassert>

#include <Xmt/Xmt.h>
#include <Xmt/Chooser.h>
#include <Xmt/InputField.h>
#include <Xm/ToggleB.h>

#include <Tools/ToolKit.h>
#include <Tools/Math.h>
#include <Tools/SimulationClock.h>

#include "MESO_AdvancedSetupDialog.h"
#include "MESO_Interface.h"
#include "MESO_Engine.h"
#include "MESO_Interface.h"
#include "MESO_Menu.h"

#include <GRN/RN_DynamicRoute.h>
using namespace std;

MESO_AdvancedSetupDialog::MESO_AdvancedSetupDialog ( Widget parent )
   : XmwDialogManager(parent, "advancedSetupDialog", NULL, 0)
{
   XtVaSetValues(widget_,
				 XmNdialogStyle, XmDIALOG_PRIMARY_APPLICATION_MODAL,
				 NULL);

   linktimeFile0_ = XmtNameToWidget(widget_, "linktimeFile0");
   assert(linktimeFile0_);

   linktimeFile1_ = XmtNameToWidget(widget_, "linktimeFile1");
   assert(linktimeFile1_);

   availability_ = XmtNameToWidget(widget_, "availability");
   assert(availability_);

   spFlags_ = XmtNameToWidget(widget_, "spFlags");
   assert(spFlags_);
}

void MESO_AdvancedSetupDialog::set(char **ptr, const char *s)
{
   if (*ptr) {
	  free(*ptr);
   }
   if (s) {
	  *ptr = strdup(s);
   } else {
	  *ptr = NULL;
   }
}


// These overload the functions in base class

void MESO_AdvancedSetupDialog::cancel(Widget, XtPointer, XtPointer)
{
   unmanage();
}


void MESO_AdvancedSetupDialog::okay(Widget, XtPointer, XtPointer)
{
   const char *name;

   name = XmtInputFieldGetString(linktimeFile0_);
   set(RN_DynamicRoute::fileptr(0), name);
   
   name = XmtInputFieldGetString(linktimeFile1_);
   set(RN_DynamicRoute::fileptr(1), name);

   int state = XmtChooserGetState(availability_);
   RN_DynamicRoute::availability(state);

   if (state) {
	  state = INFO_FLAG_AVAILABILITY;
   }
   ::theSpFlag = (XmtChooserGetState(spFlags_) | state);

   unmanage();
   theMenu->needSave();
}


void MESO_AdvancedSetupDialog::help(Widget, XtPointer, XtPointer)
{
   theDrnInterface->openUrl("mitsimAdvancedSetup", "drn.html");
}


void MESO_AdvancedSetupDialog::post()
{
   if (theSpFlag & INFO_FLAG_AVAILABILITY) {
	  RN_DynamicRoute::availability(1);
   } else {
	  RN_DynamicRoute::availability(0);
   }

   XmtInputFieldSetString(linktimeFile0_, RN_DynamicRoute::file(0));
   XmtInputFieldSetString(linktimeFile1_, RN_DynamicRoute::file(1));
   XmtChooserSetState(availability_, RN_DynamicRoute::availability(), False);
   XmtChooserSetState(spFlags_, theSpFlag & ~INFO_FLAG_AVAILABILITY, False);

   // Do not allow changes after simulation is started

   if (theSimulationClock->isStarted()) {
	  deactivate();
   } else {
	  activate();
   }

   XmwDialogManager::post();
}

void MESO_AdvancedSetupDialog::activate()
{
   if (XtIsSensitive(okay_)) return;

   XmwDialogManager::activate(linktimeFile0_);
   XmwDialogManager::activate(linktimeFile1_);
   XmwDialogManager::activate(availability_);
   XmwDialogManager::activate(spFlags_);

   XmwDialogManager::activate(okay_);
}

void MESO_AdvancedSetupDialog::deactivate()
{
   if (!XtIsSensitive(okay_)) return;

   XmwDialogManager::deactivate(linktimeFile0_);
   XmwDialogManager::deactivate(linktimeFile1_);
   XmwDialogManager::deactivate(availability_);
   XmwDialogManager::deactivate(spFlags_);

   XmwDialogManager::deactivate(okay_);
}

#endif // INTERNAL_GUI
