//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_ODTable.h
// DATE: Mon Feb 26 17:38:20 1996
//--------------------------------------------------------------------

#ifndef MESO_ODTABLE_HEADER
#define MESO_ODTABLE_HEADER

#ifdef INTERNAL_GUI		// graphical mode
#include <DRN/DRN_ODTable.h>
class MESO_ODTable : public DRN_ODTable
#else  // match mode
#include <GRN/OD_Table.h>
class MESO_ODTable : public OD_Table
#endif // !INTERNAL_GUI

{
   public:

#ifdef INTERNAL_GUI
      MESO_ODTable() : DRN_ODTable() { }
#else
      MESO_ODTable() : OD_Table() { }
#endif

      ~MESO_ODTable() { }

      // These functions override the virtual ones in the base class

      OD_Cell* newOD_Cell();
};

#endif
