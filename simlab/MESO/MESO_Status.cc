//-*-c++-*------------------------------------------------------------
// NAME: A simulation based traffic prediction model
// AUTH: Qi Yang
// FILE: MESO_Status.C
// DATE: Mon Feb 26 18:31:49 1996
//--------------------------------------------------------------------

#include <cstdio>
#include <iomanip>
#include <cstring>
#include <cctype>
using namespace std;

#include <sys/types.h>
#include <unistd.h>

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>

#ifdef INTERNAL_GUI
#include "MESO_Interface.h"
#include "MESO_Modeline.h"
#endif

#include "MESO_Engine.h"
#include "MESO_Status.h"
#include "MESO_Network.h"

MESO_Status *theStatus = new MESO_Status;

MESO_Status::MESO_Status()
   : nErrors_(0),
	 nMsgs_(0),
	 logFile_(NULL)
{
   clean();
}


MESO_Status::~MESO_Status()
{
   clean();
}


void MESO_Status::clean()
{
   nActive_ = 0;
   nArrived_ = 0;
   nNoPath_ = 0;
   nInQueue_ = 0;
   nCells_ = 0;
   if (logFile_) {
	 delete [] logFile_;
	 logFile_ = NULL;
   }
}


void
MESO_Status::openLogFile()
{
   logFile_ = Copy(ToolKit::outfile("meso.out"));
   osLogFile_.open(logFile_);
}


void
MESO_Status::closeLogFile()
{
   osLogFile_.close();
   if (nErrors_) {
	  cout << endl << nErrors_ << " error(s) detected. "
		   << "See <" << logFile_ << "> for details." << endl;
   } else if (!nMsgs_) {
      ::remove(logFile_);
   }
}

void MESO_Status::report(ostream &os)
{
   os << endl << "Number of vehicles:" << endl
      << indent << "Active  = " << nActive_ << endl
      << indent << "Arrived = " << nArrived_ << endl
	  << indent << "Incompleted = " << nNoPath_ << endl
      << indent << "InQueue = " << nInQueue_ << endl;

   os << endl << "Running Time Summary (minutes):" << endl
      << indent << "Simulated = "
      << theSimulationClock->timeSimulated() << endl
      << indent << "Real Time = "
      << theSimulationClock->timeSinceStart() << endl
      << indent << "CPU Time  = "
      << theSimulationClock->cpuTimeSinceStart() << endl;

   os << endl;
}


void
MESO_Status::print(ostream &os)
{
#ifndef INTERNAL_GUI

   static int first = 1;
   if (first && ToolKit::verbose()) {
      os << endl << indent;
      os.fill(' ');
      os << setw(10) << "Time"
		 << setw(8) << "Active"
		 << setw(8) << "Cells"
		 << setw(8) << "Arrived"
		 << setw(8) << "Incomp."
		 << setw(8) << "InQueue"
		 << endl;
      first = 0;
   }
   if (ToolKit::verbose()) {
	  os << indent
		 << setw(10) << theSimulationClock->currentStringTime()
		 << setw(8) << nActive_
		 << setw(8) << nCells_
		 << setw(8) << nArrived_
		 << setw(8) << nNoPath_
		 << setw(8) << nInQueue_;
   }

   int length = mesoNetwork()->watchQueueLength(os);

   if (ToolKit::verbose()) {
	  os << endl;
   } else if (length > 0) {
	  os << " queued at "
		 << theSimulationClock->currentStringTime()
		 << endl;
   }

#else

   if (mesoNetwork()->watchQueueLength()) {
	  os << " queued at "
		   << theSimulationClock->currentStringTime()
		   << endl;
   }

#endif
}


#ifdef INTERNAL_GUI

void MESO_Status::showStatus()
{
   MESO_Modeline *modeline = (MESO_Modeline*) theInterface->modeline();

   modeline->updateCounter(0, nActive_);
   modeline->updateCounter(1, nInQueue_);
   modeline->updateCounter(2, nNoPath_);
   modeline->updateCounter(3, nArrived_);
   modeline->updateCounter(4, nCells_);
}

#endif
