/*
 * Pickup an icon from the icon file simlab.xpm.
 * Usage: pick row col iconame
 * where: row=0-15 col=0-33
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "icons.xpm"
/*
  const int nrows  = 15;
  const int ncols  = 34;
*/
const int size   = 30;
const int colors = 15;
const int chars  = 1;
const int shadow = 0;			/* 3=keep the icon 3D shadows */

/*
 * 123456789012345678901234567890123
 * jjj```````````````````````````jjj
 * 123123456789012345678901234567123
 */

int *calc_used_colors(int row, int col, int &num)
{
  /* 1 = if a color is used for selected area */  
  static int used[colors+1];
  int i, j, k, r, c, isize = size + 2 * shadow - 7;
  for (i = 1; i <= colors; i ++) {
	used[i] = 0;
  }
  r = colors + row * size - shadow + 7;
  c = col * size - shadow + 5;
  for (i = r; i < r + isize; i ++) {
	for (j = c; j < c + isize; j ++) {
	  for (k = 1; k <= colors; k ++) {
		if (icons[i][j] == icons[k][0]) {
		  used[k] = 1;
		  break;
		}
	  }
	}
  }

  num = 0;
  for (k = 1; k <= colors; k ++) {
	num += used[k];
  }

  return used;
}

void pick_xpm(char *iconame, int tag, int row, int col)
{
  int i, j, r, c, isize = size + 2 * shadow - 7;
  int *used = calc_used_colors(row, col, c);

  printf("/* XPM */\n");

  printf("static char *%s%d[] = {\n", iconame, tag);

  printf("/* width height ncolors chars_per_pixel */\n");
  printf("\"%d %d %d %d\",\n", isize, isize, c, chars);

  printf("/* colors */\n");
  for (i = 1; i <= colors; i ++) {
	if (used[i]) {
	  printf("\"%s\",\n", icons[i]);
	}
  }

  printf("/* pixels */\n");

  r = colors + row * size - shadow + 7;
  c = col * size - shadow + 5;
  for (i = r; i < r + isize; i ++) {
	printf("\"");
	for (j = c; j < c + isize; j ++) {
	  printf("%c", icons[i][j]);
	}
	if (i != r + isize - 1) {
	  printf("\",\n");
	} else {
	  printf("\"\n");
	}
  }
  printf("};\n");
}

void pick_res(char *iconame, int tag, int row, int col)
{
  int i, j, r, c, isize = size + 2 * shadow - 7;
  int *used = calc_used_colors(row, col, c);

  printf("_Pixmaps_*%s%d: \\\n", iconame, tag);

  printf("%d %d %d %d\\n\\\n", isize, isize, c, chars);

  for (i = 1; i <= colors; i ++) {
	if (used[i]) {
	  printf("%s\\n\\\n", icons[i]);
	}
  }

  r = colors + row * size - shadow + 7;
  c = col * size - shadow + 5;
  for (i = r; i < r + isize; i ++) {
	for (j = c; j < c + isize; j ++) {
	  printf("%c", icons[i][j]);
	}
	printf("\\n\\\n");
  }
  printf("\n");
}

void main(int argc, char *argv[])
{
  int row, col;

  if (argc != 4) {
	printf("Usage: %s <row:0-15> <col:0-33> <iconame>\n", argv[0]);
	return;
  }
  
  row = atoi(argv[1]);
  col = atoi(argv[2]);

  if (strstr(argv[0], "xpm")) {
	pick_xpm(argv[3], 1, row, col);
	pick_xpm(argv[3], 2, row+1, col);
	pick_xpm(argv[3], 3, row+2, col);
  } else {
	pick_res(argv[3], 1, row, col);
	pick_res(argv[3], 2, row+1, col);
	pick_res(argv[3], 3, row+2, col);
  }
}
