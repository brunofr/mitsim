!===========================================================================
! FILE: xmitsim.ad
!
! Resource File for xmitsim
!
!===========================================================================

#include "colors.ad"
#include "common.ad"
#include "styles.ad"
#include "drnicons.ad"
#include "drnlayout.ad"
#include "drndialog.ad"
#include "sciplot.ad"
#include "xmitsimlogo.ad"

*xmitsim.Title: MITSIM
*xmitsim.xmtChildren: XmtLayout mainLayout;

*timers.rows:         1
*timers.columns:      3
*timers.columnWidths: 8, 8, 8
*timers.columnAlignments: \
  XmALIGNMENT_CENTER, XmALIGNMENT_CENTER, XmALIGNMENT_CENTER

! Message on click for help
_Messages_*timers.current: Time for the simulation clock
_Messages_*timers.left: Estimated computational time remaining 
_Messages_*timers.sensor: Time reported sensor readings 

*counters.rows:         1
*counters.columns:      4
*counters.columnWidths: 5, 5, 5, 5
*counters.columnAlignments: \
  XmALIGNMENT_END, XmALIGNMENT_END, \
  XmALIGNMENT_END, XmALIGNMENT_END

*tools.pixmaps: "pan1","zoomin1","zoomout1","center1","rotate1",\
		"query1","legend1","draw1","text1","trace1"
*tools.selectPixmaps: "pan2","zoomin2","zoomout2","center2","rotate2",\
		"query2","legend2","draw2","text2","trace2"
*tools.InsensitivePixmaps: "pan3","zoomin3","zoomout3","center3","rotate3",\
		"query3","legend3","draw3","text3","trace3"

_Messages_*prompt.helpTrace: Click left mouse button to trace a vehcile.
_Messages_*tips.trace: Trace vehicle

*viewType.pixmaps: "micro1","macro1","odflow1"
*viewType.selectPixmaps: "micro2","macro2","odflow2"
*viewType.InsensitivePixmaps: "micro3","macro3","odflow3"

!
! Define manu bar
!

*menubar.marginWidth: 0
*menubar.marginHeight: 0
*menubar.shadowThickness: 0
*menubar.items: \
  Submenu "_File" -> filePane ; \
  Tearoff "_Simulation" -> simulationPane ; \
  Tearoff "_View" -> viewPane ; \
  Tearoff "_Tools" -> toolsPane ; \
  Help    "_Help" -> helpPane ;

*menubar*filePane.items: \
  Open:   "_Open ..."    [Meta-O] ; \
  Save:   "_Save"       [Meta-S] ; \
  SaveAs: "Save _As ..." [Meta-A] ; \
  DumpState: "_Dump State_ ..." [Meta-D] ; \
  Export: "E_xport ..." [Meta-X] ; \
  -------- ; \
  Print:  "_Print"   [Meta-P] ; \
  -------- ; \
  Quit:   "_Exit"       [Meta-F4] ;

*menubar*simulationPane.items: \
  Run:    "_Run"    [Ctrl-Meta-R] ; \
  Pause:  "_Pause"  [Ctrl-Meta-P] ; \
  Setup:  "_Setup ..." [Ctrl-Meta-S] ; \
  AdvancedSetup:"_Advanced Setup ..." [Ctrl-Meta-A] ; \
  Randomizer:    "Random Seeds ..." ; \
  Parameter: "P_arameters ... " [Ctrl-Meta-P]; \
  Output: "_Output ..." [Ctrl-Meta-O]; \
  Option: "Op_tion ..." [Ctrl-Meta-t];

*menubar*viewPane.title: View
*menubar*viewPane.items: \
  View: Toggle On "Graphical View"; \
  -------- ; \
  MapFeature: "_Map Features ..." [Ctrl-Alt-m]; \
  RoadSegment: "_Road Segments ..." [Ctrl-Alt-r]; \
  Sensor: "_Sensors ..." [Ctrl-Alt-s]; \
  Signal: "S_ignals and Signs ..." [Ctrl-Alt-i]; \
  Vehicle: "_Vehicles ..." [Ctrl-Alt-v]; \
  Submenu "_Colors" -> colorPane; \
  Bookmark: "_Bookmarks ..." [Ctrl-Alt-b];

*menubar*colorPane.title: Color
*menubar*colorPane.items: \
  LinearColor: "_Linear ..." [Ctrl-Alt-l]; \
  SymbolColor: "_Symbol ..." [Ctrl-Alt-p];

*menubar*toolsPane.items: \
  SegmentAttribute: "Edit _Segment Attributes ..." ; \
  FindObject:   "_Where is ..." ; \
  BrowsePath:   "Browse _Path ..." ;

*menubar*helpPane.items: \
  showTips: On "Show _Tips" [Meta+T] $showTips; \
  -------- ; \
  About:       "_About" ; \
  ContextHelp: "_Context Help" [F1] ; \
  Index:       "_Index" ; \
  Credit:      "C_redit" ;

*drawingArea*popup.items: \
  Title       "@fBView and Color Code@fR" ; \
              -------- ; \
  MapFeature: "_Map Features ..."; \
  RoadSegment:"_Road Segments ..."; \
  Sensor:     "_Sensors ..."; \
  Signal:     "S_ignals and Signs ..."; \
  Vehicle:    "_Vehicles ..."; \
              -------- ; \
  Title       "@fBSearch and Edit@fR" ; \
              -------- ; \
  SegmentAttribute: "Edit _Segment Attributes ..." ; \
  FindObject: "_Where is ..."; \
  BrowsePath: "Browse _Path ...";

!
! Vehicles view dialog
!

*vehicleDialog.xmtChildren: unmanaged XmtLayout vehicleLayout;
*vehicleLayout.dialogTitle: Select Color Coding Scheme
*vehicleLayout.xmtChildren: \
  TitleTmpl(Vehicles) dbtitle; \
  XmtChooser vehicleBorderCode; \
  XmToggleButton vehicleLabel; \
  XmtChooser vehicleShadeCode; \
  ButtonBoxTmpl(okay,cancel,help) buttonBox;
*vehicleLayout.layout: \
  Fixed dbtitle \
  Etched Through 6 2 Caption tl "@fB Border Color @fR" vehicleBorderCode \
  vehicleLabel \
  Etched Through 6 2 Caption tl "@fB Shade Color @fR" vehicleShadeCode \
  <> \
  buttonBox

*vehicleBorderCode.chooserType: RadioBox
*vehicleBorderCode.strings: \
  "None",\
  "Vehicle Type",\
  "Information Availability",\
  "Turning Moverment",\
  "Driver Group",\
  "Lane Use Privilege"

*vehicleShadeCode.chooserType: CheckBox
*vehicleShadeCode.strings: \
  "Waiting",\
  "Nosing",\
  "Yielding",\
  "Merging"

*vehicleLabel.labelString: @fBShow identification code@fR
*vehicleLabel.alignment: ALIGNMENT_BEGINNING

!
! Trace Vehicle Dialog
!

*traceDialog.xmtChildren: unmanaged XmtLayout traceLayout;
*traceLayout.dialogTitle: Trace

*traceLayout.xmtChildren: \
  XbaeMatrix info; \
  ButtonBoxTmpl(apply,cancel,help) buttonBox;

*traceLayout.layout: \
  Etched Through 6 2 Col { \
    info \
  } \
  <> \
  buttonBox

*traceLayout.info.traversalOn: True
*traceLayout.info.columns: 1
*traceLayout.info.rows: 10
*traceLayout.info.columnWidths: 10

! Check TS_TraceDialog.cc when modifies these
*traceLayout.info.rowLabels: \
  ID, \
  Type, \
  Driver Group, \
  Dep. Time, \
  Origin, \
  Destination, \
  Position, \
  Speed, \
  Desired Speed, \
  Travel Time

! Gray=Fixed, Bisque=Editable
*traceLayout.info.cellBackgrounds: \
  gray\n\
  gray\n\
  bisque\n\
  gray\n\
  gray\n\
  gray\n\
  gray\n\
  gray\n\
  bisque\n\
  gray\n

!
! Random Seed Dialog
!

*RandomizerDialog.xmtChildren: unmanaged XmtLayout RandomizerLayout;
*RandomizerLayout.dialogTitle: Random Seeds

*RandomizerLayout.xmtChildren: \
  TitleNoLineTmpl(Random Seeds) dbtitle; \
  CheckerFieldTmpl("Miscellaneous", 4) r1; \
  CheckerFieldTmpl("Vehicle Departures", 4) r2; \
  CheckerFieldTmpl("Route Choice", 4) r3; \
  CheckerFieldTmpl("Driving Behavior", 4) r4; \
  ButtonBoxTmpl(okay,cancel,help) buttonBox;

*RandomizerLayout.layout: \
  dbtitle \
  Etched Through 6 2 Col { \
    Row { "@fB Random Seed for @fR" <> "@fB Span @fR" } \
    FlushRight r1 \
    FlushRight r2 \
    FlushRight r3 \
    FlushRight r4 \
  } \
  <> \
  buttonBox

!
! Output dialog
!

*outputDialog.xmtChildren: unmanaged XmtLayout outputLayout;
*outputLayout.dialogTitle: Select Output

*outputLayout.xmtChildren: \
  TitleNoLineTmpl(Output) dbtitle; \
  TabAreaStyle XmwTab tabs; \
  ButtonBoxTmpl(okay,cancel,help) buttonBox;

*outputLayout.tabs.xmtChildren: \
  TabBtnStyle XmPushButton GeneralButton; \
  XmtLayout GeneralGroup; \
  TabBtnStyle XmPushButton SurveillanceButton; \
  XmtLayout SurveillanceGroup; \
  TabBtnStyle XmPushButton NetworkButton; \
  XmtLayout NetworkGroup; \
  TabBtnStyle XmPushButton VehicleButton; \
  XmtLayout VehicleGroup;

*outputLayout*GeneralButton.labelString: General
*outputLayout*SurveillanceButton.labelString: Surveillance
*outputLayout*NetworkButton.labelString: Network
*outputLayout*VehicleButton.labelString: Vehicle Trips

*outputLayout*GeneralGroup.xmtChildren: \
  XmtInputField outDir; \
  XmToggleButton header; \
  XmtChooser format;

*outputLayout*SurveillanceGroup.xmtChildren: \
  CheckFieldTmpl("Sensor readings") o11; \
  CheckFieldTmpl("VRC readings") o12; \

*outputLayout*NetworkGroup.xmtChildren: \
  CheckFieldTmpl("Link flow and travel times") o21; \
  CheckFieldTmpl("Segment travel times") o22; \
  CheckFieldTmpl("Segment statistics") o23; \
  CheckFieldTmpl("Queue statistics") o24; \
  CheckFieldTmpl("Travel time tables") o25; \
  CheckFieldTmpl("3D State") o26;

*outputLayout*VehicleGroup.xmtChildren: \
  CheckFieldTmpl("Departure records") o31; \
  CheckFieldTmpl("Vehicle trip records") o32; \
  CheckFieldTmpl("Vehicle path records") o33; \
  CheckFieldTmpl("Vehicle trajectory") o34; \
  CheckFieldTmpl("Network-wide MOE") o35;

*outputLayout.layout: \
  Fixed dbtitle \
  tabs \
  <> \
  buttonBox

*outputLayout*GeneralGroup.layout: \
  Etched Through 6 2 Caption tl "@fB Output Directory @fR" outDir \
  header \
  Etched Through 6 2 Caption tl "@fB File Format @fR" format

*outputLayout*SurveillanceGroup.layout: \
  Etched Through 6 2 Caption tl "@fB Surveillance Sensors Output  @fR" Col { \
    FlushRight o11 \
    FlushRight o12 \
  }

*outputLayout*NetworkGroup.layout: \
  Etched Through 6 2 Caption tl "@fB Network Performance @fR" Col { \
    FlushRight o21 \
    FlushRight o22 \
    FlushRight o23 \
    FlushRight o24 \
    FlushRight o25 \
	FlushRight o26 \
  }

*outputLayout*VehicleGroup.layout: \
  Etched Through 6 2 Caption tl "@fB Vehicle Trip Output @fR" Col { \
    FlushRight o31 \
    FlushRight o32 \
    FlushRight o33 \
    FlushRight o34 \
    FlushRight o35 \
  }

! Default directory and file names

*header.alignment: ALIGNMENT_BEGINNING
*header.labelString: @fBAdd Headings@fR (i.e. comments) in Output
*format.chooserType: RadioBox
*format.strings: \
  "With curly braces (readable by parsers)",\
  "Rectangular text (loadable in @fImatlab@fR, @fIexcel@fR, etc.)"

!
! Setup dialog
!

*setupDialog.xmtChildren: unmanaged XmtLayout setupLayout;
*setupLayout.dialogTitle: Setup

*setupLayout.xmtChildren: \
  TitleNoLineTmpl(Simulation Setup) dbtitle; \
  XmtInputField title; \
  TabAreaStyle XmwTab tabs; \
  ButtonBoxTmpl(okay,cancel,help) buttonBox;

*setupLayout.tabs.xmtChildren: \
  TabBtnStyle XmPushButton DirectoryButton; \
  XmtLayout DirectoryGroup; \
  TabBtnStyle XmPushButton InputButton; \
  XmtLayout InputGroup; \
  TabBtnStyle XmPushButton TimingButton; \
  XmtLayout TimingGroup; \
  TabBtnStyle XmPushButton DataButton; \
  XmtLayout DataGroup;

*setupLayout*DirectoryButton.labelString: Directories
*setupLayout*InputButton.labelString: Input Files
*setupLayout*TimingButton.labelString: Timing
*setupLayout*DataButton.labelString: Data Collection

*setupLayout*DirectoryGroup.xmtChildren: \
  InputFieldTmpl("Directory for input files") inDir; \
  InputFieldTmpl("Directory for default paramters") paraDir; \
  InputFieldTmpl("Working directory (for tmp files)") workDir; \
  InputFieldTmpl("Directory for output files") outDir;

*setupLayout*InputGroup.xmtChildren: \
  InputFieldTmpl("Parameters") paraFile; \
  InputFieldTmpl("Network database") netFile; \
  InputFieldTmpl("Origin to destination trip tables") odFile; \
  InputFieldTmpl("@fIand/or@fR  Vehicle table (departure records)") vehicleFile; \
  InputFieldTmpl("Habitual path table") pathFile; \
  InputFieldTmpl("Incident description") inciFile; \
  InputFieldTmpl("MOE specification") moeFile; \
  InputFieldTmpl("Historical link travel times") linktimeFile0;

*setupLayout*TimingGroup.xmtChildren: \
  InputFieldTmpl("Simulation start time") startTime; \
  InputFieldTmpl("Simulation stop time") stopTime; \
  InputFieldTmpl("Simulation clock step size") stepSize; \
  InputFieldTmpl("Step size for updating path information") pathStepSize; \
  InputFieldTmpl("Animation of vehicle movements") microStepSize; \
  InputFieldTmpl("Color coding traffic information by segments") macroStepSize; \
  InputFieldTmpl("Aggregate status information") batchStepSize;

*setupLayout*DataGroup.xmtChildren: \
  InputFieldTmpl("Interval length for system-wide MOE") moeStepSize; \
  InputFieldTmpl("Time resolution for departure records") depRecordStepSize; \
  InputFieldTmpl("Time resolution for network state") stateStepSize; \
  InputFieldTmpl("Sampling step size for segment data calculation") segmentStepSize; \
  InputFieldTmpl("Report step size for segment data") segmentReportStepSize; \
  InputFieldTmpl("Point data sensors reporting") pointStepSize; \
  InputFieldTmpl("Area data sensors reporting") areaStepSize; \
  InputFieldTmpl("Vehicle trajectories") trajectoryStepSize;

*setupLayout.layout: \
  Fixed dbtitle \
  Etched Through 6 2 Caption tl "@fB Title @fR" title \
  # \
  tabs \
  <> \
  buttonBox

*setupLayout*DirectoryGroup.layout: \
  Etched Through 6 2 Caption tl "@fB Directory Setting @fR" Col { \
     FlushRight inDir \
     FlushRight paraDir \
     FlushRight outDir \
     FlushRight workDir \
  }

*setupLayout*InputGroup.layout: \
  Etched Through 6 2 Caption tl "@fB Required Input Files @fR" Col { \
     FlushRight netFile \
     FlushRight paraFile \
     FlushRight odFile \
     FlushRight vehicleFile \
  } # \
  Etched Through 6 2 Caption tl \
  "@fB Optional Input Files @fR" Col { \
     FlushRight pathFile \
     FlushRight inciFile \
     FlushRight moeFile \
     FlushRight linktimeFile0 \
  }

*setupLayout*TimingGroup.layout: \
   Etched Through 6 2 Caption tl "@fB Simulation Clocks @fR" Col { \
      FlushRight startTime \
      FlushRight stopTime \
      FlushRight stepSize \
      FlushRight pathStepSize \
   } # \
   Etched Through 6 2 Caption tl "@fB Visualization @fR" Col { \
      FlushRight microStepSize \
      FlushRight macroStepSize \
      FlushRight batchStepSize \
   }

*setupLayout*DataGroup.layout: \
  Etched Through 6 2 Caption tl "@fB Data Collection Step Sizes @fR" Col { \
     FlushRight moeStepSize \
     FlushRight depRecordStepSize \
     FlushRight trajectoryStepSize \
     FlushRight segmentStepSize \
     FlushRight segmentReportStepSize \
	 FlushRight stateStepSize \
     FlushRight pointStepSize \
     FlushRight areaStepSize \
  }


!
! Advanced setup dialog
!

*advancedSetupDialog.xmtChildren: unmanaged XmtLayout advancedSetupLayout;
*advancedSetupLayout.dialogTitle: Advanced Setup
*advancedSetupLayout.xmtChildren: \
  TitleTmpl(Advanced Simulation Setup) dbtitle; \
  XmtInputField linktimeFile0; \
  XmtInputField linktimeFile1; \
  XmtChooser availability; \
  XmtChooser spFlags; \
  ButtonBoxTmpl(okay,cancel,help) buttonBox;

*advancedSetupLayout.layout: \
  Fixed dbtitle \
  Etched Through 6 2 Caption tl \
  "@fB Historical travel time @fR" linktimeFile0 \
  # \
  Etched Through 6 2 Caption tl \
  "@fB Updated Travel Time and Availability @fR" Col { \
     linktimeFile1 \
     availability \
  } \
  # \
  Etched Through 6 2 Caption tl "@fB Shortest Travel Time Table @fR" spFlags \
  <> \
  buttonBox

*advancedSetupLayout*availability.chooserType: RadioBox
*advancedSetupLayout*availability.strings: \
  "Used for pretrip planning",\
  "Used when viewed VMS or received data"

*advancedSetupLayout*spFlags.chooserType: CheckBox
*advancedSetupLayout*spFlags.strings: \
  "Time variant",\
  "Calculate shortest path table peridically",\
  "Update path travel time peridically",\
  "Use existing shortest path table"


!
! Incident Dialog
!

! { VisDist  SegmentID  EndPosition(%)
!   { SeverityCode CapFactor StartTime Duration SpeedLimit LaneID IncidentID }
! }

*incidentDialog.xmtChildren: unmanaged XmtLayout incidentLayout;
*incidentLayout.dialogTitle: Incident
*incidentLayout.xmtChildren: \
   TitleTmpl(Incident Setup) dbtitle; \
   XmLabel position; \
   XmtInputField visibility; \
   XbaeMatrix lanes; \
   ButtonBoxTmpl(okay,cancel,help) buttonBox;

*incidentLayout.layout: \
   dbtitle \
   Margin 0 Row { \
     Caption lc "Position: " position \
     ## \
     Caption lc "Visibility: " Fixed visibility \
   } \
   Margin 0 8em High 20em Wide lanes \
   # \
   buttonBox

*incidentLayout.position.alignment: XmALIGNMENT_BEGINNING
*incidentLayout.visibility.columns: 6

*incidentLayout.lanes.selectedForeground: Blue
*incidentLayout.lanes.selectedBackground: White
*incidentLayout.lanes.columns: 3
*incidentLayout.lanes.columnWidths: 10, 8, 6
*incidentLayout.lanes.columnLabels: \
   Start\nTime, \
   Duration\n(sec), \
   Speed\n(mph)
   
*incidentLayout.lanes.columnLabelColor: blue
*incidentLayout.lanes.columnLabelAlignments: \
		XmALIGNMENT_CENTER, \
		XmALIGNMENT_CENTER, \
		XmALIGNMENT_CENTER
*incidentLayout.lanes.columnAlignments: \
		XmALIGNMENT_CENTER, \
		XmALIGNMENT_CENTER, \
		XmALIGNMENT_CENTER
*incidentLayout.lanes.rowLabelWidth: 6
*incidentLayout.lanes.rowLabelColor: blue

_Messages_*prompt.badIncidentPosition: Not a valid incident position.

!
! Simulation Parameters Dialog
!

*parameterDialog.xmtChildren: unmanaged XmtLayout parameterLayout;
*parameterLayout.dialogTitle: Parameters
*parameterLayout.xmtChildren: \
   TitleTmpl(Simulation Parameters) dbtitle; \
   ButtonBoxTmpl(okay,cancel,help) buttonBox;
*parameterLayout.layout: \
  Fixed dbtitle \
  Centered "@fB Not done yet. @fR" \
  Centered "@fB This needs a good deal of work! @fR" \
  <> \
  buttonBox


!
! Message for counters
!

_Messages_*counters.active: Number of vehicles currently in the network
_Messages_*counters.inqueue: Number of vehicles in spillback queues
_Messages_*counters.nopath: Incomplete vehicle trips
_Messages_*counters.arrived: Number of vehicles reached destinations


!
! Messages for tips
!

_Messages_*tips.micro: Animation
_Messages_*tips.macro: Node/Link diagram
_Messages_*tips.odflow: OD flows
