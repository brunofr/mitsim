// Parse a command line and extract options we know about.

#include <vector>
#include "CloParser.h"
using namespace std;

int main(int argc, char *argv[])
{
  bool quiet;
  int bits;
  double cost;
  char *output;
  vector<char *> names;

  CloParser p;
  p.add(new Clo("-quiet",  &quiet, false));
  p.add(new Clo("-bits",   &bits,  8,  "integer"));
  p.add(new Clo("-cost",   &cost,  1.0));
  p.add(new Clo("-output", &output,"foo"));
  p.add(new Clo("-names", &names, NULL, "multi string"));
  p.parse(&argc, argv);

  cout << "Parsed cmd line options:" << endl;
  p.print();

  cerr << "Remaining cmd options:" << endl;
  p.print(argc, argv);

  return 0;
}

