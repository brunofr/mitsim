//-*-c++-*------------------------------------------------------------
// Misc.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <cstring>
#include <cctype>
#include <cstdarg>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <ctime>
#include <sys/types.h>
#include <sys/param.h>
#include <cassert>

#include <iostream>

#include "Misc.h"
using namespace std;

// Returns the name of the host

#ifndef MAXHOSTNAMELEN
#define MAXHOSTNAMELEN 64
#endif

const char *HostName()
{
  static char name[MAXHOSTNAMELEN+1];
  static const char* host = NULL;
  if (host) {
    return host;
  }
#ifdef SUNOS
  else {
    const char* envhost = 0;
    envhost = getenv("HOST");
    if (envhost != 0 ) {
      strcpy(name, envhost);
      return host=name;
    }
    else {
      return "localhost";
    }
  }
#else
  else if (gethostname(name, MAXHOSTNAMELEN) == 0) {
    return host = name;
  }
  else {
    return "localhost";
  }
#endif
}

const char* UserName()
{
  static char *usr = NULL;
  if (usr) return usr;
  else if (usr = getenv("NAME")) return usr;
  else if (usr = getenv("USER")) return usr;
  else if (usr = getenv("LOGNAME")) return usr;
  else return NULL;
}

const char* TimeTag()
{
  time_t t = time(NULL);
  char *s = ctime(&t);
  char *e = strchr(s, '\n');
  if (e) *e = '\0';
  return (s);
}

bool HasStrFromLeft(const char *me, const char *str)
{
  int m = 0, n = strlen(str);
  while (*me != '\0' && *str != '\0' && *me == *str) {
	m ++;
	me ++;
	str ++;
  }
  if (m == n) return true;
  else return false;
}

bool HasStrFromRight(const char *me, const char *str)
{
  int m = strlen(me);
  int n = strlen(str);
  if (m < n) return false;
  me = me + (m - n);
  m = 0;
  while (*me != '\0' && *str != '\0' && *me == *str) {
	m ++;
	me ++;
	str ++;
  }
  if (m == n) return true;
  else return false;
}


char* ToLower(char *s)
{
  if (!s) return s;
  int n = strlen(s);
  for(register int i = 0; i < n; i ++) {
	if(isupper(s[i])) s[i] = tolower(s[i]);
  }
  return s;
}

char* ToUpper(char *s)
{
  if (!s) return s;
  int n = strlen(s);
  for(register int i = 0; i < n; i ++) {
	if(islower(s[i])) s[i] = toupper(s[i]);
  }
  return s;
}

int Cmp(const char *s1, const char *s2)
{
  if(s1 == NULL && s2 == NULL) return 0;
  else if (s1 == NULL) return 1;
  else if (s2 == NULL) return -1;
  else return strcmp(s1, s2);
}

int CmpCi(const char *s1, const char *s2)
{
  if(s1 == NULL && s2 == NULL) return 0;
  else if (s1 == NULL) return 1;
  else if (s2 == NULL) return -1;
  else return strcasecmp(s1, s2);
}


char *Trim(char *str)
{
  if (!str) return str;
  str = TrimLeft(str);
  str = TrimRight(str);
  return str;
}

char *TrimLeft(char *str)
{
  if (!str) return str;
  while (*str != '\0' && isspace(*str)) {
	str ++;
  }
  return str;
}

char *TrimRight(char *str)
{
  if (!str || *str == '\0') return str;
  char *s = str + strlen(str) - 1;
  while (s != str && isspace(*s)) {
	s --;
  }
  *(s + 1) = '\0';
  return str;
}

char* Copy(const char* s)
{
  if (s && *s != '\0') {
	return strdup(s);
  } else {
	return NULL;
  }
}

char* Copy(char* s1, const char *s2)
{
  if (s1 == s2) return s1;
  assert (s1);
  if (s2) strcpy(s1, s2);
  else *s1 = '\0';
  return s1;
}

void Copy(char** ptr, const char *value)
{
  if (*ptr == value) return;
  if (*ptr) {
	free (*ptr);
  }
  if (value && *value != '\0') {
	*ptr = strdup(value);
  } else {
	*ptr = NULL;
  }
}

char* Copy(int i)
{
  char buf[12];
  sprintf(buf, "%d", i);
  return strdup(buf);
}

char* Copy(float a, int d = 1)
{
  char buf[12];
  sprintf(buf, "%.*f", d, a);
  return strdup(buf);
}

const char *Str(const char* format, ...)
{
  static char buf[1024];
  va_list ap;
  va_start(ap, format);
  vsprintf(buf, format, ap);
  va_end(ap);
  return buf;
}

char *StrCopy(const char* format, ...)
{
  char buf[1024];
  va_list ap;
  va_start(ap, format);
  vsprintf(buf, format, ap);
  va_end(ap);
  return strdup(buf);
}

void StrFree(char *s)
{
  free(s);
}

void Alias(char** ptr, char *value)
{
  if (*ptr == value) return;
  if (*ptr) {
	free (*ptr);
  }
  *ptr = value;
}

bool IsEmpty(const char *s)
{
  if (!s) return true;
  while(*s != '\0' && isspace(*s)) {
	s ++;
  }
  if (*s == '\0') return true;
  else return false;
}

bool IsEqual(const char *s1, const char *s2, bool ci)
{
  if (ci) return (CmpCi(s1, s2) == 0);
  else return (Cmp(s1, s2) == 0);
}

const char *NoNull(const char *s)
{
  if (s) return s;
  else return "";
};

int GetPair(const char* str, int *i, int *j)
{
  if (sscanf(str, "%d-%d", i, j) == 2 ||
	  sscanf(str, "%d,%d", i, j) == 2 ||
	  sscanf(str, "%d%d", i, j) == 2 ||
	  sscanf(str, "%d/%d", i, j) == 2) {
	return 0;
  } else {
	return 1;
  }
}


int PutEnv(const char* format, ...)
{
  char buf[512];

  va_list ap;
  va_start(ap, format);
  vsprintf(buf, format, ap);
  va_end(ap);

  char *p = strdup(buf);

  return ::putenv(p);
}

/*
 *-----------------------------------------------------------------------
 * Expand a string which may contain environment variables. environment
 * variables are prefixed by the '$' character.  The variable name is
 * taken as the unbroken string of letters, numbers, and the characters
 * '-' and '_'.  Optionally, the variable name can be enclosed in braces
 * '{}'.  If the '~' character is the first character in a string, it is
 * interpreted as a shorthand for $HOME.
 *
 * Returns: expansion_space if successful, otherwise NULL.
 *
 * Error conditions: expansion will fail if expansion_space is not big
 * enough or if an environment variable can not be found.
 *-----------------------------------------------------------------------
 */

const char* ExpandEnvVars(const char* original)
{
  const int space_length = MAXPATHLEN;
  static char expansion_space[space_length+1];

  if (!original) return NULL;

  char*	env_val;
  int	env_val_length;

  int	original_position = 0;
  int	expand_position = 0;
	
  if (original[0] == '~') {
	if ((env_val = getenv("HOME")) == NULL) {
	  cerr << endl
		   << "Error:: unable to expand environment variable 'HOME' "
		   << "on " << HostName() << "." << endl;
	  return (NULL);
	} else if ((env_val_length = strlen(env_val)) >= space_length) {
	  cerr << endl
		   << "Error: insufficient buffer length to expand <";
	  cerr << original << ">" << endl;
	  return (NULL);
	}
	original_position++;
	strncpy(expansion_space, env_val, env_val_length);
	expand_position += env_val_length;
  }
	
  for (; original[original_position] != '\0'; original_position++) {
	if (original[original_position] == '$') {
	  const int max_var_len = 40;
	  char	var_name_buff[max_var_len + 1];
	  int	var_name_pos = 0;

	  if (original[original_position + 1] == '{') {
		// take all characters up to matching '}' as name
		++original_position;
		while (original[++original_position] != '}') {
		  if (original[original_position] == '{') {
			cerr << endl
				 << "Error:: nested braces {} in";
			cerr << "variable name in string <";
			cerr << original << ">" << endl;
			return (NULL);
		  } else if (original[original_position] == '\0') {
			cerr << endl
				 << "Error:: unmatched '{' in variable";
			cerr << "name in string <";
			cerr << original << ">" << endl;
			return (NULL);
		  } else if (var_name_pos >= max_var_len) {
			var_name_buff[max_var_len] = '\0';
			cerr << endl
				 << "Error:: variable name <";
			cerr << var_name_buff;
			cerr << "...> in string <";
			cerr << original ;
			cerr << "> exceeds variable name length limit of ";
			cerr << max_var_len << endl;
			return (NULL);
		  } else {
			var_name_buff[var_name_pos++] = 
			  original[original_position];
		  }
		}
	  }	else {
		for (++original_position;
			 (original[original_position] == '-'
			  || original[original_position] == '_'
			  || isalnum(original[original_position]));
			 ++original_position) {
		  if (var_name_pos >= max_var_len) {
			var_name_buff[max_var_len] = '\0';
			cerr << endl
				 << "Error:: variable name <";
			cerr << var_name_buff;
			cerr << "...> in string <";
			cerr << original << ">" << endl;
			cerr << " exceeds variable name length";
			cerr << "limit of " << max_var_len << endl;
			return (NULL);
		  } else {
			var_name_buff[var_name_pos++] = original[original_position];
		  }
		}
		original_position--; /* put back character following name */
	  }

	  if (var_name_pos > 0)	{
		var_name_buff[var_name_pos] = '\0';
		if ((env_val = getenv(var_name_buff)) == NULL) {
		  cerr << endl
			   << "Error:: unable to expand environment variable <";
		  cerr << var_name_buff << ">" << endl;
		  return (NULL);
		} else if ((env_val_length = strlen(env_val)) >= space_length) {
		  cerr << endl
			   << "Error:: insufficient buffer length to expand <";
		  cerr << original << ">" << endl;
		  return (NULL);
		}
		strncpy(&expansion_space[expand_position], 
				env_val, env_val_length);
		expand_position += env_val_length;
	  }
	} else {
	  expansion_space[expand_position++] = original[original_position];
	}
  }
	
  expansion_space[expand_position] = '\0';
  return (expansion_space);
}
