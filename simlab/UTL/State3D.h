#ifndef STATE3D_HEADER
#define STATE3D_HEADER

//-*-c++-*------------------------------------------------------------
// State3D.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// 3-dimensional growing matrix for storing network state.
//      
//              +----------+
//              |          |
//            +----------+ |
//            |          | |
//          +----------+ | |
//          |          | | |
//        t |          | |~+
//        i | x(r,t,l) | |
//        m |          |~+ sheet
//        e |          |
//          +~~~~~~~~~~+
//          
//            position
//
// All data are stored in a binary file.  The file is orgazined as
// the following:
//
// Heading:
//
//   T _na;                       // indicator of missing value
//   unsigned int _num_positions; // number of items per record
//
// List of Records:
//
//   unsigned int _sheet;
//   unsigned int _row;
//   T* _data;
//
// where _sheet and _row uniquely identifies a record in the data
// file. _data is a vector of size _num_positions (eg. representing the
// state of the network at time "_row" as measured/predicted at time
// "_sheet".
//
// Example code of usage:
//
// 1. Save the states pd_[] of n_ positions (segments)
//    estimated/predicted at time s_ for time r_ (in TMS).
//
//      State3D<int> st("foo.dat", -1, n_);
//      st.append(s_, r_, pd_);
//
//    Repeat the method append() for all s_ and r_.
//
// 2. Access network states (in XDTA)
//
//      State3D<int> st("foo.dat");
//
//    Depending on which direction you want to view the 3-Dimensional
//    data, following method maybe used:
//
// 2.1 Cross positions
//
//      int *pd = pread(s_, r_);
//
//    where pd[] stores states estimated/pridicted at time s_ for time
//    t_.  This data set is designed for geographical view (e.g. color
//    coding network objects to show the spatial pattern).
//
// 2.2 Cross time
//
//      int *pd = tread(s_, p_);
//
//    where pd[] are the time dependent states for position p_
//    estimated at time s_.  This data set is designed for prediction
//    view (e.g. plotting a chart with travel time vs departure time).
//
// 2.3 Cross sheet
//
//      int *pd = sread(r_, p_);
//
//    where pd[] are the states for position p_ at time r_ as
//    estimated/predicted in the past computations.  This data set can
//    be used to plot the difference between predictions.
//
// 2.4 Single element
//
//    You can also access the value of a single element in
//    3-dimentional matrix:
//
//      int d = read(s_, r_, p_);
//
//  I tried to make the cross-position access the most efficient one
//  among these methods.
//
//  See other public access functions defined in State3D.h for more
//  information.  See State3D-Test.cc for an test example.
//--------------------------------------------------------------------

#include <cstdlib>
#include <cstdio>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <algorithm>
#include <iostream>
#include <vector>

//static const int MAGIC_NUMBER = 97227;

#define MAGIC_NUMBER 97227


template <class T> class State3D
{
public:

    ~State3D();

    // Create an empty file.
    State3D(const char *filename_, T na_, int psz_);

    // Append a record pointed by pd at the end of the file. Requires:
    // pd contains num_positions data items.  s and r is accending and
    // continuous compared to the value used in last call of this
    // function.
    void append(int s_, int r_, const T* pd_);
    void append(const char* fn);

    // Open an existing file
    State3D(const char *filename_);

    // Cross position (segments) reader: Read an record into the block
    // pointed by pd.  If pd is NULL, a new block of size num_position()
    // is allocated and the caller is responsible to free this block of
    // memory.  If pd is not NULL, it must have the size of
    // num_positions().  pread() returns pd_ on success or NULL if fails
    // (not data available for sheet s_ and row s_).
    T* pread(int s_, int r_, T* pd_ = NULL);

    // Cross sheet (predictions) reader. Size of returned array is
    // num_sheets()
    T* sread(int r_, int p_, T* pd_ = NULL); // expensive

    // Cross row (time) reader. Size of returned array is num_rows(s_).
    T* tread(int s_, int p_, T* pd_ = NULL); // expensive 

    // Single element
    T read(int s_, int r_, int p_);

    const char *filename() { return _filename; }

    // Current dimensions of the 3D matrix
    int num_positions() { return _num_positions; }
    int num_sheets() { return _rows.size(); }
    int num_rows(int s_);

    int first_row_id(int s_);
    int last_row_id(int s_);
    int first_row_index(int s_);
    int last_row_index(int s_);

    void print(T* pd, int sz, std::ostream &os = std::cout);
    void print(std::ostream &os = std::cout);
    void print4(std::ostream &os = std::cout, char deli = ',');

    void file_info(std::ostream &os = std::cout);
    void sheet_info(std::ostream &os = std::cout);
    void record_info(std::ostream &os = std::cout);
    void info(std::ostream &os = std::cout) {
        file_info(os);
        sheet_info(os);
        record_info(os);
    }

private:

    void filename(const char *filename_);
    void initialize();			// set record size, initial position, etc.
    int update(int up2s_ = INT_MAX); // search for new records, return counts

    // Returns the offset of the record for sheet s_ and and row r_ or
    // -1 if no data is available.

    off_t find(int s_, int r_);

    // Convert sheet id to record index

    int sheet_id_to_index(int s_);

    void error_msg(const char *msg, bool die = true);

private:
  
    char *_filename;
    int _fd;						// file descriptor

    T _na;						// missing value
    int _num_positions;			// number of items per record

    // These variables help to locate a data item fast

    std::vector<int> _sids;			// sheet id of each record
    std::vector<int> _rids;			// row id of each record
    std::vector<int> _index;			// index for 1st row of each sheet
    std::vector<int> _rows;			// number of rows for each sheet
    std::vector<int> _pages;			// list of sheets we know about
    std::vector<off_t> _spos;			// beginning of each sheet

    size_t _rsz;					// total size per record
    size_t _dsz;					// data size per record
    off_t _next_pos;				// position for next unread record

    int _last_sid_written;		// last sheet we wrote
    int _last_rid_written;
    int _last_sid_read;			// last sheet we read or known about
};


// Create an empty file.

template <class T>
State3D<T>::State3D(const char *filename_, T na_, int psz_)
    : _na(na_), _num_positions(psz_),
      _last_sid_written(-1), _last_rid_written(-1), _last_sid_read(-1)
{
    // Save the file name for error reporting purpose
  
    filename(filename_);

  // Open the file for read and write mode

    int flags = O_RDWR|O_CREAT|O_TRUNC;
    mode_t mode = S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH;
    if ((_fd = ::open(_filename, flags, mode)) < 0) {
        error_msg("failed opening output file");
    }

    // Save heading data

    size_t szt = sizeof(T);
    size_t szi = sizeof(int);
    int check_sum = MAGIC_NUMBER;
    if (::write(_fd, &_na, szt) != szt ||
        ::write(_fd, &_num_positions, szi) != szi ||
        ::write(_fd, &check_sum, szi) != szi) {
        error_msg("failed writing heading data");
    }

    initialize();
}

// Open an existing file

template <class T>
State3D<T>::State3D(const char *filename_)
    : _last_sid_written(-1), _last_rid_written(-1), _last_sid_read(-1)
{
    // Save the file name for error reporting purpose
  
    filename(filename_);

  // Open the file for read and write mode

    if ((_fd = ::open(_filename, O_RDONLY)) < 0) {
        error_msg("failed opening input file");
    }

    // Get the n/a value and row size

    size_t szt = sizeof(T);
    size_t szi = sizeof(int);
    int check_num;
    if (::read(_fd, &_na, szt) != szt ||
        ::read(_fd, &_num_positions, szi) != szi ||
        ::read(_fd, &check_num, szi) != szi) {
        error_msg("failed reading heading data");
    }

    // Check dimension

    if (check_num != MAGIC_NUMBER) {
        error_msg("data format error or wrong architecture");
    }

    initialize();
    update();
}


// The destructor

template <class T>
State3D<T>::~State3D()
{
    if (_fd) close(_fd);
  
    if (_filename) delete [] _filename;

    _sids.erase(_sids.begin(), _sids.end());
    _rids.erase(_rids.begin(), _rids.end());
    _rows.erase(_rows.begin(), _rows.end());
    _spos.erase(_spos.begin(), _spos.end());
    _index.erase(_index.begin(), _index.end());
    _pages.erase(_pages.begin(), _pages.end());
}


// This function is called only in the constructors

template <class T>
void State3D<T>::initialize()
{
    // Size of each record

    _dsz = _num_positions * sizeof(T);
    _rsz = 2 * sizeof(int) + _dsz;

    // Position for the next reading
  
    _next_pos = sizeof(T) + 2 * sizeof(int);
}


// Search for new records until sheet id is greater than up2s_ or no
// more data is available. It returns counts.

template <class T>
int State3D<T>::update(int up2s_)
{
    if (up2s_ < _last_sid_read) {

        typedef std::vector<int>::iterator iterator;

        // In the records we already know about, search the 1st and last
        // record with sheet id equals to or greater than s_

        iterator s_lower, s_upper;

        s_lower = lower_bound(_sids.begin(), _sids.end(), up2s_);
        s_upper = upper_bound(_sids.begin(), _sids.end(), up2s_);
	
        if (s_lower == s_upper) {		// Not found in knowledge set
            if (s_lower != _sids.end()) { // Does not exist
                return 0;
            }
        } else {						// Found
            if (s_upper != _sids.end()) { // All found
                return 0;
            }
        }
    }

    // Check if more records should be read
  
    int count = 0;

    // Beginning position of the first record

    off_t pos = _next_pos;

    // Scan all the rows and variables for fast searching

    int s;						// sheet id
    int r;						// row id
    size_t szi = sizeof(int);
    while ( (pos = ::lseek(_fd, _next_pos, SEEK_SET)) >= 0 &&
            ::read(_fd, &s, szi) == szi &&
            ::read(_fd, &r, szi) == szi &&
            s <= up2s_) {
        if (s != _last_sid_read) {	// new sheet
            _spos.push_back(pos);		// position of the sheet
            _index.push_back(_sids.size()); // index of the 1st record
            _rows.push_back(0);		// count number of rows
            _pages.push_back(s);
            _last_sid_read = s;
        }
        _sids.push_back(s);			// sheet id of the record
        _rids.push_back(r);			// row id of the record
        _rows.back() ++;			// increase number of rows by 1
        _next_pos += _rsz;			// offset 1 record
        count ++;
    }
    return count;
}


// Save the file name for use by error msg

template <class T>
void State3D<T>::filename(const char *filename_)
{
    _filename = new char [strlen(filename_) + 1];
    strcpy(_filename, filename_);
}

// Print what is the problem. Terminate the program if die is true.

template <class T>
void State3D<T>::error_msg(const char *msg, bool die)
{
    fprintf(stderr, "State3D: %s (%s).\n", msg,  _filename);
    if (die) {
#ifdef EXCEPTION_HEADER
        if (theException) {
            theException->exit(ERROR_FATAL);
        } else {
            exit(1);
        }
#else
        exit(1);
#endif
    }
}


// Append a record pointed by pd at the end of the file. Requires:
// pd contains num_position data items.  s and r is accending and
// continuous compared to the value used in last call of this
// function.

template <class T>
void State3D<T>::append(int s_, int r_, const T* pd_)
{
    // Move pointer to the end

    off_t pos = ::lseek(_fd, 0L, SEEK_END);
    if (pos < 0) {
        error_msg("cannot move to end");
    }

    // Check sheet id
    if (s_ < _last_sid_written) {
        error_msg("sheet not sorted", false);
    }
  
    if (s_ != _last_sid_written) {	// new sheet
        _last_rid_written = -1;		// rewind record counter
        _spos.push_back(pos);		// save position of the sheet
        _rows.push_back(0);			// get a row counter for the new sheet
        _pages.push_back(s_);
        _index.push_back(_sids.size());	// index of 1st record
    }

    // Check row id
    if (r_ < _last_rid_written) {
        error_msg("time not sorted", false);
    } else if (r_ == _last_rid_written) {
        error_msg("time repeated", false);
    }

    _sids.push_back(s_);			// save sheet id of the record
    _rids.push_back(r_);			// save row id of the record
    _rows.back() ++;				// increase number of rows by 1

  // Append the data of this record

    size_t szi = sizeof(int);
    if (::write(_fd, &s_, szi) != szi ||
        ::write(_fd, &r_, szi) != szi ||
        ::write(_fd, pd_, _dsz) != _dsz) {
        error_msg("failed appending data");
    }  

    _last_sid_read = s_;
    _last_sid_written = s_;
    _last_rid_written = r_;
}

// Copy record from fn to the class object

template <class T>
void State3D<T>::append(const char *fn_)
{
    int s, r;
    T* pd = new T[_num_positions];
  
    int fd;
    if ((fd = ::open(fn_, O_RDONLY)) < 0) {
        fprintf(stderr, "Cannot open input file %s\n", fn_); 
        error_msg("failed appending data");
        return;
    }

    // Skip the header

    size_t szt = sizeof(T);
    size_t szi = sizeof(int);
    size_t szd = szt * _num_positions;

    T na;
    int num;
    int check_num;
    if (::read(fd, &na, szt) != szt ||
        ::read(fd, &num, szi) != szi ||
        ::read(fd, &check_num, szi) != szi) {
        error_msg("failed header check");
    }

    // Check dimension

    if (check_num != MAGIC_NUMBER) {
        error_msg("data format error or wrong architecture");
    } else if (num != _num_positions) {
        error_msg("failed column size check");
    }

    // Copy all records

    while (::read(fd, &s, szi) == szi &&
           ::read(fd, &r, szi) == szi &&
           ::read(fd, pd, szd) == szd) {
        append(s, r, pd);
    }

    delete [] pd;
}

template <class T>
int State3D<T>::first_row_id(int s_)
{
    int i = first_row_index(s_);
    if (i < 0) return -1;         /* error */
    else return _rids[i];
}

template <class T>
int State3D<T>::last_row_id(int s_)
{
    int i = last_row_index(s_) ;
    if (i < 0) return -1;         /* error */
    return _rids[i];
}

template <class T>
int State3D<T>::first_row_index(int s_)
{
    int i = sheet_id_to_index(s_);
    if (i < 0) return -1;         /* error */
    int j = _index[i];
}

template <class T>
int State3D<T>::last_row_index(int s_)
{
    int i = sheet_id_to_index(s_);
    if (i < 0) return -1;         /* error */
    return _index[i]+ _rows[i] - 1;
}

template <class T>
int State3D<T>::num_rows(int s_)
{
    int i = sheet_id_to_index(s_);
    if (i < 0) return 0;
    else return _rows[i];
}


// Find the index of a sheet

template <class T>
int State3D<T>::sheet_id_to_index(int s_)
{
    // Find index of the 1st record in sheet s_

    std::vector<int>::iterator si;
    si = lower_bound(_pages.begin(), _pages.end(), s_);

    if (si == _pages.end() || *si != s_) {
        return -1;				// sheet s_ not found
    }
  
    int i = si - _pages.begin();
    return i;
}


// Returns the offset of the record for sheet s_ and and row r_ or
// -1 if no data is available.

template <class T>
off_t State3D<T>::find(int s_, int r_)
{
    typedef std::vector<int>::iterator iterator;

    // In the records we already know about, search the 1st and last
    // record with sheet id equals to or greater than s_

    iterator s_lower, s_upper;

    int count = 0;				// for checking if new records arrived?
    do {
        s_lower = lower_bound(_sids.begin(), _sids.end(), s_);
        s_upper = upper_bound(_sids.begin(), _sids.end(), s_);

        if (s_lower == s_upper) {		// Not found
            if (s_lower == _sids.end()) { // Check if more records arrived
                if ((count = update(s_)) <= 0) return -1;
            } else {					// Does not exist
                return -1;
            }
        } else {					// Found
            if (s_upper == _sids.end()) { // At least one
                count = update(s_);
            } else {					// All found
                break;
            }
        }
    } while (count > 0);

    // Sheet s_ is in the range given by iterators [lower, upper].  Now
    // check if row r_ is available.

    int si = s_lower - _sids.begin();
    int se = s_upper - _sids.begin();
    iterator r_lower = _rids.begin() + si;
    iterator r_upper = _rids.begin() + se;

    iterator pos = lower_bound(r_lower, r_upper, r_);
    if (pos == _rids.end() || *pos != r_) { // Not found
        return -1;
    }
  
    int n = pos - r_lower; // number of rows within the sheet
    int i = sheet_id_to_index(s_); // index of the sheet
    off_t offset = _spos[i] + n * _rsz;
    return offset;
}


// Cross position (segments) reader: Read a record into the block
// pointed by pd.  If pd is NULL, a new block of size num_positions()
// is allocated and the caller is responsible to free this block of
// memory.  If pd is not NULL, it must have the size of
// num_positions().  pread() returns pd_ on success or NULL if fails
// (not data available for sheet s_ and row s_).

template <class T>
T* State3D<T>::pread(int s_, int r_, T* pd_)
{
    // Check if data is ready
  
    off_t offset = find(s_, r_);
    if (offset < 0) {				// data not available
        return NULL;
    }

    // Set the file pointer

    if (::lseek(_fd, offset + 2 * sizeof(int), SEEK_SET) < 0) {
        return NULL;
    }

    // Check the space

    if (!pd_) {					// storage not provided by caller
        pd_ = new T[_num_positions];
    }

    // Read the data

    size_t sz = ::read(_fd, pd_, _dsz);
  
    // Set missing values if any

    for (int i = sz / sizeof(T); i < _num_positions; i ++) {
        pd_[i] = _na;
    }

    return pd_;
}


// Cross sheet (predictions) reader. Size of returned array is
// num_sheets()

template <class T>
T* State3D<T>::sread(int r_, int p_, T* pd_)
{
    typedef std::vector<int>::iterator iterator;

    // Load summary information for all records

    update();
    int s, n = _spos.size();
    if (n <= 0) return NULL;

    // Check the storage

    if (!pd_) {
        pd_ = new T[n];
    }

    for (s = 0; s < n; s ++) {	// loop every sheet

        // records range for sheet s
        iterator ri = _rids.begin() + _index[s];
        iterator re = ri + _rows[s];

        // position of record r_ in sheet s
        iterator i = lower_bound(ri, re, r_);

        if (i == _rids.end() || *i != r_) {	// r_ not found in this sheet
            pd_[s] = _na;				// set to missing value
        } else {					// found the record
            int q = i - _rids.begin();
            int m = q - _index[s];	// # of records before r_ in sheet s
            // Position in file for (s, r_, p_)
            off_t offset = _spos[s] + m * _rsz + 2 * sizeof(int) + p_ * sizeof(T);
            if (::lseek(_fd, offset, SEEK_SET) < 0 ||
                ::read(_fd, (pd_+s), sizeof(T)) != sizeof(T)) {
                pd_[s] = _na;
            }
        }
    }

    return pd_;
}


// Cross row (time) reader. Size of returned array is num_times(s_).

template <class T>
T* State3D<T>::tread(int s_, int p_, T* pd_)
{
    typedef std::vector<int>::iterator iterator;

    int i = sheet_id_to_index(s_);
    if (i < 0) return NULL;

    // records range for sheet s

    iterator ri = _rids.begin() + _index[i];
    iterator re = ri + _rows[i];

    // Check storage space

    if (!pd_) {
        pd_ = new T[_rows[i]];
    }
  
    int r;						// row index
    iterator si;
    off_t pos = _spos[i] + 2 * sizeof(int) + p_ * sizeof(T);

    // loop each row in this sheet
    for (r = 0, si = ri; si != re; si ++, r ++) {
        if (::lseek(_fd, pos + r*_rsz, SEEK_SET) < 0 ||
            ::read(_fd, (pd_+r), sizeof(T)) != sizeof(T)) {
            pd_[r] = _na;
        }
    }
    return pd_;
}


// Read a single element

template <class T>
T State3D<T>::read(int s_, int r_, int p_)
{
    off_t offset = find(s_, r_);
    if (offset < 0) return _na;	// data ont available

    off_t pos = offset + 2 * sizeof(int) + p_ * sizeof(T);
    T pd;
    if (::lseek(_fd, pos, SEEK_SET) &&
        ::read(_fd, &pd, sizeof(T)) == sizeof(T)) {
        return pd;
    } else {
        return _na;
    }
}


// Print an array of T

template <class T>
void State3D<T>::print(T* _pd, int _sz, std::ostream &os)
{
    if (_pd) {
        for (int i = 0; i < _sz; i ++) {
            os << " ";
            os.width(7);
            if (_pd[i] != _na) {
                os << _pd[i];
            } else {
                os << "na";
            }
        }
        os << '\n';
    } else {
        os << " na\n";
    }
}

// Print the entire file

template <class T>
void State3D<T>::print(std::ostream &os)
{
    update();

    if (_spos.size() <= 0) return;
    if (::lseek(_fd, _spos[0], SEEK_SET) < 0) return;

    T *pd = new T[_num_positions];

    size_t sz = sizeof(int);
    int s, r;
    while (::read(_fd, &s, sz) == sz &&
           ::read(_fd, &r, sz) == sz &&
           ::read(_fd, pd, _dsz) == _dsz) {
        os << s << " " << r;
        print(pd, _num_positions, os);
    }

    delete [] pd;
}

template <class T>
void State3D<T>::print4(std::ostream &os, char deli)
{
    update();

    if (_spos.size() <= 0) return;
    if (::lseek(_fd, _spos[0], SEEK_SET) < 0) return;

    T *pd = new T[_num_positions];

    size_t sz = sizeof(int);
    int s, r;
    while (::read(_fd, &s, sz) == sz &&
           ::read(_fd, &r, sz) == sz &&
           ::read(_fd, pd, _dsz) == _dsz) {
        for (int i = 0; i < _num_positions; i ++) {
            if (pd[i] != _na) {
                os << s << deli
                   << r << deli
                   << i << deli
                   << pd[i] << '\n' ;
            }
        }
    }
    delete [] pd;
}

// Print internal information for debugging

template <class T>
void State3D<T>::file_info(std::ostream &os)
{
    os << "# Filename: " << _filename
       << "\n# fd: " << _fd;

    os << "\n# na: " << _na
       << "\n# Positions: " << _num_positions << '\n';
}

template <class T>
void State3D<T>::sheet_info(std::ostream &os)
{
    os << "\n# Sheet Info:\n"
       << "#\tSheet\tPos\tIndex\tRows\n";
    for (int s = 0; s < _rows.size(); s ++) {
        os << '\t' << s
           << '\t' << _spos[s]
           << '\t' << _index[s]
           << '\t' << _rows[s]
           << '\n';
    }
}

template <class T>
void State3D<T>::record_info(std::ostream &os)
{
    os << '\n' << "#Record Info:" << '\n'
       << "#\tRecord\tSheet\tRow" << '\n';
    for (int r = 0; r < _sids.size(); r ++) {
        os << "\t" << r
           << "\t" << _sids[r]
           << "\t" << _sids[r]
           << '\n';
    }
}

#endif
