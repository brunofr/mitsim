//-*-c++-*------------------------------------------------------------
// Misc.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef MISC_HEADER
#define MISC_HEADER

#include <cstring>
#include "bool.h"

extern const char *HostName();
extern const char* UserName();
extern const char* TimeTag();

extern bool HasStrFromLeft(const char *me, const char *str);
extern bool HasStrFromRight(const char *me, const char *str);

extern int Cmp(const char *s1, const char *s2);
extern int CmpCi(const char *s1, const char *s2);

extern char* Trim(char *str);
extern char* TrimLeft(char *str);
extern char* TrimRight(char *str);

extern char* ToLower(char *s);
extern char* ToUpper(char *s);

extern void Copy(char **ptr, const char *);
extern char* Copy(char *s1, const char *s2);
extern char* Copy(const char *);
extern char* Copy(int i);
extern char* Copy(float a, int d);

extern const char* Str(const char *format, ...);
extern char* StrCopy(const char *format, ...);
extern void StrFree(char *s);

extern void Alias(char **ptr, char *);

extern bool IsEmpty(const char *s);
extern bool IsEqual(const char *s1, const char *s2, bool ci = true);

extern const char* NoNull(const char *s);
extern int GetPair(const char* str, int *i, int *j);

extern int PutEnv(const char *format, ...);
extern const char* ExpandEnvVars(const char * ori);

#endif
