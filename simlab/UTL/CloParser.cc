/*
 * CloParse.cc
 *
 * Qi Yang
 * Copyright (C) 1997
 * Massachusetts Institue of Technology
 * All Rights Reserved
 *
 * Parse a command line and extract options we know about.
 *
 * See CloParseTest.cc for an example of its usage.
 *
 * This library file provides a simple interface for a command line
 * parser.  The parser is defined and implemented in the files
 * CloParse.h and CloParse.cc.  CloParse.h provide a structure that
 * can be used to define a list of command line options: the type of
 * arguments these options can handle, token names, default values,
 * pointers to data where the argument values will be stored, and an
 * optional documentation string.  The data locations will be
 * initialized to default values specified in the options table before
 * parsing begins.  The ParseCloLine function is called with argc,
 * argv, the options table, and the number of options in the table.
 * If the parser finds one of the options on the command line, the
 * option (and its argument, if any) are removed from argv and argc is
 * appropriately adjusted.  Thus when parsing is finished, argv
 * contains only unrecognized options (such as file names).
 *
 * The following types of arguments are supported: 
 *
 * Clo::BOOL    -- No argument is expected. The default value is false.
 *                 If the argument is present, the value becomes 1.
 *
 * Clo::INT     -- An integer is expected.
 *					
 * Clo::LONG    -- A long integer is expected.
 *					
 * Clo::FLOAT   -- A float is expected.
 *					
 * Clo::DOUBLE  -- A double float is expected.
 *					
 * Clo::STRING  -- A string is expected.
 *
 * Clo::STRINGS -- At least one string is expected for each occurrence
 *                 of this option.  The option can be repeated with
 *                 different arguments.
 *
 * Code example: 
 *
   bool quiet;
   int bits;
   double cost;
   char *output;
   vector<char *>names;

   CloParser p;
   p.add(new Clo("-quiet", &quiet, false));
   p.add(new Clo("-bits",  &bits,  8,  "integer"));
   p.add(new Clo("-cost",  &cost,  1.0));
   p.add(new Clo("-output",&output,"foo"));
   p.add(new Clo("-names", &names, NULL, "multi string"));
   p.parse(&argc, argv);

   cout << "Parsed cmd line options:" << endl;
   p.print();

   cerr << "Remaining cmd options:" << endl;
   p.print(argc, argv);

 *
 * Command line usage example:
 * 
 *  a.out -quiet -bits 8 -cost:1.0 -names Yang -output=foo.out -names:Qi
 *
 * For the options expecting value(s), the value(s) can be separated
 * from the token either by ':', '=' or a space.
 *
 */

#include <cctype>
#include <cstring>
#include <cstdlib>
#include <cstdio>

#include "CloParser.h"
using namespace std;

CloParser *theCloParser = NULL;

#ifndef INTBOOL
Clo::Clo(const char *name_, bool *data_, bool deflt_, const char *doc_)
  : _name(name_), _data(data_), _deflt(NULL), _doc(doc_), _type(Clo::BOOL)
{
  *data_ = deflt_;
}
#endif

Clo::Clo(const char *name_, int *data_, int deflt_, const char *doc_)
  : _name(name_), _data(data_), _deflt(NULL), _doc(doc_), _type(Clo::INT)
{
  *data_ = deflt_; 
}

Clo::Clo(const char *name_, unsigned int *data_, unsigned int deflt_,
		 const char *doc_)
  : _name(name_), _data(data_), _deflt(NULL), _doc(doc_), _type(Clo::INT)
{
  *data_ = deflt_; 
}

Clo::Clo(const char *name_, long *data_,
		 long deflt_, const char *doc_)
  : _name(name_), _data(data_), _deflt(NULL), _doc(doc_), _type(Clo::LONG)
{
  *data_ = deflt_; 
}

Clo::Clo(const char *name_, unsigned long *data_,
		 unsigned long deflt_, const char *doc_)
  : _name(name_), _data(data_), _deflt(NULL), _doc(doc_), _type(Clo::LONG)
{
  *data_ = deflt_; 
}

Clo::Clo(const char *name_, float *data_, float deflt_, const char *doc_)
  : _name(name_), _data(data_), _deflt(NULL), _doc(doc_), _type(Clo::FLOAT)
{
  *data_ = deflt_; 
}

Clo::Clo(const char *name_, double *data_, double deflt_,
		 const char *doc_)
  : _name(name_), _data(data_), _deflt(NULL), _doc(doc_), _type(Clo::DOUBLE)
{
  *data_ = deflt_; 
}

Clo::Clo(const char *name_, const char **data_, const char *deflt_,
		 const char *doc_)
  : _name(name_), _data(data_), _deflt(NULL), _doc(doc_), _type(Clo::STRING)
{
  *data_ = deflt_;
}
Clo::Clo(const char *name_, char **data_, const char *deflt_,
		 const char *doc_)
  : _name(name_), _data(data_), _deflt(NULL), _doc(doc_), _type(Clo::STRING)
{
  *data_ = (char *)deflt_;
}

Clo::Clo(const char *name_, vector<const char *> *data_, const char *deflt_,
		 const char *doc_)
  : _name(name_), _data(data_), _deflt(NULL), _doc(doc_), _type(Clo::STRINGS)
{
  deflt(deflt_);
  data_->push_back(deflt_);
}
Clo::Clo(const char *name_, vector<char *> *data_, const char *deflt_,
		 const char *doc_)
  : _name(name_), _data(data_), _deflt(NULL), _doc(doc_), _type(Clo::STRINGS)
{
  deflt(deflt_);
  data_->push_back(_deflt);
}

Clo::~Clo()
{
  if (_deflt) free (_deflt);
}

void Clo::deflt(const char *deflt_)
{
  if (_deflt == deflt_) return;
  else if (_deflt) free (_deflt);
  
  if (deflt_) _deflt = strdup(deflt_);
  else _deflt = NULL;
}

//-----------------------------------------------------------------------

CloParser::CloParser() : _help(false)
{
}

void CloParser::add_help()
{
  add(new Clo("-help", &_help, NULL, "Show available options"));
  add(new Clo("-h", &_help, NULL, NULL));
}

CloParser::~CloParser()
{
  for (int i = 0; i < _clos.size(); i ++) {
	delete _clos[i];
  }
  _clos.erase(_clos.begin(), _clos.end());
}

void CloParser::extra_argument(const char *name)
{
  cerr << "CloParser: " << name << " requires no argument." << endl;
}

void CloParser::missing_argument(const char *name, const char *type)
{
  cerr << "CloParser: " << name << " requires an argument of "
	   << type << "." << endl;
}

/*
 * Convert a string to long integer. Hex (0x) and oct (0) numbers are
 * acceptable.
 */

long int CloParser::str_to_int(const char *s)
{
  if (s) return strtol(s, NULL, 0);
  else return 0;
}

/*
 * Convert a string to float.
 */

double CloParser::str_to_float(const char *s)
{
  if (s) return atof(s);
  else return 0.0;
}


/*
 * Return the number of leading white spaces.  It returns:
 *   -1 = s is empty or consists of white spaces only
 *    0 = s is not empty and there is no leading white space
 *    n = the first n chars in s is white spaces
 */

int CloParser::count_leading_white_spaces(const char *s)
{
  if (!s) return -1;
  int n = 0;
  while (s[n] != '\0') {
	if (!isspace(s[n])) return n;
	n ++;
  }
  return -1;
}

/*
 * Check if opt has token at the beginning.  It returns:
 *  -1 = opt has no token
 *   0 = opt and token are exactly the same
 *   n = the first n chars in opt is the same as token (eg.
 *       CmpToken("-o:foo.txt", "-o") returns 2)
 */

int CloParser::cmp_token(const char *opt, const char *token)
{
  char oc, tc;
  int m, n = 0;
  while (opt && token) {
	oc = opt[n];
	tc = token[n];
	if (oc == '\0' && tc == '\0') {
	  return 0;
	} else if (oc == '\0') {
	  return -1;
	} else if (tc == '\0' && (oc == ':' || oc == '=')) {
	  if ((m = count_leading_white_spaces(opt+n+1)) >= 0) {
		return n+1+m;
	  } else {
		return 0;
	  }
	} else if (oc != tc) {
	  return -1;
	}
	n ++;
  }
  return -1;
}

void CloParser::add(Clo *clo)
{
  _clos.push_back(clo);
}

bool CloParser::parse (int *argc, char *argv[])
{
  int ap2, ap = 0;
  int cnt = 0;					/* num of args parsed for an option */
  const char *value;			/* value of an option */
  int i, j;
  bool found;

  /* current position in array for Clo::STRINGS */
  int *elem_num = new int[_clos.size()];

  for (i = 0; i < _clos.size(); i ++) {
	elem_num[i] = 0;
  }

  /* Now search through the argument list for matches. */

  while (ap < *argc) {
	found = false;
	for (i = 0; !found && i < _clos.size(); i ++) {
	  
	  /* Compare the argument with ith token */

	  int n = cmp_token(argv[ap], _clos[i]->_name); 
	  
	  if (n < 0) {				/* different */
		continue;				/* skip this token */
	  } else if (n > 0) {		/* value in argv[ap] itself */
		value = argv[ap]+n;		/* string contains the value */
		cnt = 1;
	  } else if (ap != *argc-1 &&	/* argv[ap] is the token only */
				 *argv[ap+1] != '-') {
		value = argv[ap+1];		/* value is the next argv, if any */
		cnt = 2;
	  } else {
		value = NULL;
		cnt = 1;
	  }

	  switch (_clos[i]->_type) {
	  case Clo::BOOL:
		if (value && cnt == 1) {
		  extra_argument(_clos[i]->_name);
		} else {
		  *(bool*)_clos[i]->_data = true;
		  cnt = 1;
		}
		break;
	  case Clo::INT:
		if (value) *(int*)_clos[i]->_data = str_to_int(value);
		else missing_argument(_clos[i]->_name, "integer");
		break;
	  case Clo::LONG:
		if (value) *(long*)_clos[i]->_data = str_to_int(value);
		else missing_argument(_clos[i]->_name, "long integer");
		break;
	  case Clo::FLOAT:
		if (value) *(float*)_clos[i]->_data = str_to_float(value);
		else missing_argument(_clos[i]->_name, "float");
		break;
	  case Clo::DOUBLE:
		if (value) *(double*)_clos[i]->_data = str_to_float(value);
		else missing_argument(_clos[i]->_name, "double float");
		break;
	  case Clo::STRING:
		if (value) *(const char**)_clos[i]->_data = value;
		else missing_argument(_clos[i]->_name, "string");
		break;
	  case Clo::STRINGS:
		if (value) {
		  j = ap+cnt-1;
		  vector<const char *> *p = (vector<const char *> *)_clos[i]->_data;
		  do {
			(*p)[elem_num[i]] = value;
			++(elem_num[i]);
			(*p).push_back(_clos[i]->_deflt);
			j ++;
			value = argv[j];
		  } while (j < *argc && *value != '-');
		  cnt = j - ap;
		} else {
		  missing_argument(_clos[i]->_name, "string(s)");
		}
		break;
	  }
	  ap2 = ap + cnt;

	  /* Move foreward remaining args. Make sure to copy end NULL */

	  for (j = ap; ap2 <= *argc; j++, ap2++) {
		argv[j] = argv[ap2];
	  }
	  *argc -= cnt;

	  found = true;
	}
	if (!found) ap++;
  }

  delete [] elem_num;

  /* Check if help is needed */

  if (_help) {
	  cout << "Usage: " << argv[0] << " [options]" << endl
		   << "Available options are:" << endl;
	  print(cout);
	  exit(0);
  }

  return (*argc > 1);
}

void CloParser::print(ostream &os)
{
  for (int i = 0; i < _clos.size(); i ++) {
	os << "  ";

	os.width(12);
	os.setf(ios::left, ios::adjustfield);
	os << _clos[i]->_name << ":\t";
	os.width(6);

	switch (_clos[i]->_type) {
	case Clo::BOOL:
	  os << *(bool*)_clos[i]->_data;
	  break;
	case Clo::INT:
	  os << *(int*)_clos[i]->_data;
	  break;
	case Clo::LONG:
	  os << *(long*)_clos[i]->_data;
	  break;
	case Clo::FLOAT:
	  os << *(float*)_clos[i]->_data;
	  break;
	case Clo::DOUBLE:
	  os << *(double*)_clos[i]->_data;
	  break;
	case Clo::STRING:
	  os << *(const char**)_clos[i]->_data;
	  break;
	case Clo::STRINGS:
	  {
		vector<const char *> *p = (vector<const char *> *)_clos[i]->_data;
		for (int j = 0; j < p->size(); j ++) {
		  os << (*p)[j] << " ";
		}
	  }
	  break;
	default:
	  os << "n/a";
	  break;
	}
	if (_clos[i]->_doc) {
	  os << "\t# " << _clos[i]->_doc;
	}
	os << endl;
  }
}

void CloParser::print(int argc, char *argv[], ostream &os)
{
  os << " ";
  for (int i = 0; i < argc; i ++) {
	os << " " << argv[i];
  }
  os << endl;
}
