// An example and testing program for class State3D.
//
// This program creates a binary data file foo.tmp with the following
// data items:
//
//     -1      // N/A indicator
//     6       // Number of objects
//     0 0   0   1   2   3   4   5
//     0 1  10  11  12  13  14  15
//     1 0 100 101 102 103 104 105
//     1 1 110 111 112 113 114 115
//     1 2 120 121 122 123 124 125
//     2 0 200 201 202 203 204 205
//     3 0 300 301 302 303 304 305
//     3 1 310 311 312 313 314 315
//     4 0 400 401 402 403 404 405
//     4 1 410 411 412 413 414 415
//     4 2 420 421 422 423 424 425
//
// These data represents following 3-Dimensional matrix:
//
//           s=4-------------------------+
//             | 400 401 402 403 404 405 |   <-  0
//             | 410 411 412 413 414 415 |   <-  1
//             | 420 421 422 423 424 425 |   <-  2
//	       s=3-------------------------+ +
//           | 300 301 302 303 304 305 |   <-  0
//           | 310 311 312 313 314 315 |   <-  1
//	     s=2-------------------------+ +
//         | 200 201 202 203 204 205 |   <-  0
//	   s=1-------------------------+ +
//       | 100 101 102 103 104 105 |   <-  0
//       | 110 111 112 113 114 115 |   <-  1
//       | 120 121 122 123 124 125 |   <-  2
//	 s=0-------------------------+ +
//     |   0   1   2   3   4   5 |  <-   0
//     |  10  11  12  13  14  15 |  <- r=1
//     +-------------------------+
//     p = 0   1   2   3   4   5
//
// Usgae of this program:
//
//   State3D-Test s r p
//
// where s=[0-4], r=[0-2], p=[0-5].  For example:
//
//   State3D-Test 4 1 1
//
// will pick the element of last sheet (s=4), 2nd row (r=1), and 2nd
// column (p=1), which is 411.
//
// One of s, r, or p can be replaced by a ".".  For example:
//
//   State 3D-Test 4 . 1
//
// will return: 401 411 421, which is the 2nd column (p=1) of the
// last sheet (s=4).

#include "State3D.h"

int main(int argc, char *argv[])
{
  if (argc != 4) {
	cerr << "Usage: " << argv[0] << " sheet row position" << endl;
	return 1;
  }

  static const char *filename = "foo.tmp";
  static int ss[] = { 0, 0, 1, 1, 1, 2, 3, 3, 4, 4, 4 };
  int m = sizeof(ss) / sizeof(int);
  int i, n = 6;
  int *pd = new int[n];

  // Test writing.  Generate some records with sheet id specified in
  // ss[], row id start with 0, and hex value with 3 digits
  // corresponds sheet, row, and position.

  const int NA_VALUE = -1;
  State3D<int> in(filename, NA_VALUE, n);
  int p, s = -1, r = -1;
  for (i = 0; i < m; i ++) {
	// Sheet and row ids
	if (ss[i] != s) {
	  s = ss[i];
	  r = 0;
	} else {
	  r ++;
	}
	// Testing data
	for (p = 0; p < n; p ++) {
	  pd[p] = p + (r << 4) + (s << 8);
	}
	in.append(s, r, pd);
  }

  delete [] pd;

  // Test reading

  State3D<int> out(filename);

  // Print all number in the file

  out.print();

  // Print selected values

  int *q = NULL;
  s = atoi(argv[1]);
  r = atoi(argv[2]);
  p = atoi(argv[3]);

  if (strcmp(argv[1], ".") == 0) { // cross sheet
	q = out.sread(r, p);
	cout << "(" << "." << r << p << ") =";
	out.print(q, out.num_sheets(), cout);
  } else if (strcmp(argv[2], ".") == 0) { // cross row
	q = out.tread(s, p);
	cout << "(" << s << "." << p << ") =";
	out.print(q, out.num_rows(s), cout);
  } else if (strcmp(argv[3], ".") == 0) { // cross position
	q = out.pread(s, r);
	cout << "(" << s << r << "." << ") =";
	out.print(q, out.num_positions(), cout);
  } else {
	int x = out.read(s, r, p);
	cout << "(" << s << r << p << ") = ";
	if (x != NA_VALUE) {
	  cout << hex << x << dec << endl;
	} else {
	  cout << "na" << endl;
	}
  }

  if (q) delete [] q;
}
