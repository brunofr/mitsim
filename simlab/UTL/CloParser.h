/*
 * CloParser.h
 *
 * Qi Yang
 * Copyright (C) 1997
 * Massachusetts Institue of Technology
 * All Rights Reserved
 *
 * Parse a command line and extract options we know about.
 *
 * See CloParseTest.cc for an example of its usage.  See CloParse.cc
 * for more documentation.
 *
 */

#ifndef CLOPARSER_HEADER
#define CLOPARSER_HEADER

#include <vector>
#include <iostream>

#include "bool.h"				// for IRIX -o32

class Clo {

  friend class CloParser;

public:
#ifndef INTBOOL
  Clo(const char *name_, bool *data_, bool deflt_, const char *doc_ = NULL);
#endif
  Clo(const char *name_, int *data_, int deflt_, const char *doc_ = NULL); 
  Clo(const char *name_, long *data_, long deflt_, const char *doc_ = NULL);
  Clo(const char *name_, unsigned int *data_, unsigned int deflt_,
	  const char *doc_ = NULL); 
  Clo(const char *name_, unsigned long *data_, unsigned long deflt_,
	  const char *doc_ = NULL);
  Clo(const char *name_, float *data_, float deflt_, const char *doc_ = NULL);
  Clo(const char *name_, double *data_, double deflt_, const char *doc_ = NULL);
  Clo(const char *name_, const char **data_, const char *deflt_,
	  const char *doc_ = NULL);
  Clo(const char *name_, char **data_, const char *deflt_,
	  const char *doc_ = NULL);
  Clo(const char *name_, std::vector<const char *> *data_, const char *deflt_,
	  const char *doc_ = NULL);
  Clo(const char *name_, std::vector<char *> *data_, const char *deflt_,
	  const char *doc_ = NULL);

  ~Clo();

private:

  void deflt(const char *deflt_);

  enum Type {
	UNKNOWN,
	BOOL,
	INT,
	LONG,
	FLOAT,
	DOUBLE,
	STRING,
	STRINGS
  };

  Type _type;
  const char *_name;
  void *_data;
  char *_deflt;					// used for STRINGS type only
  const char *_doc;
};


class CloParser {
public:

  CloParser();
  virtual ~CloParser();

  virtual void add(Clo *clo);
  virtual bool parse(int *argc, char *argv[]);

  virtual void print(std::ostream &os = std::cout);
  virtual void print(int argc, char *argv[], std::ostream &os = std::cout);

protected:

  void add_help();

private:
  
  void extra_argument(const char *name);
  void missing_argument(const char *name, const char *type);
  long int str_to_int(const char *s);
  double str_to_float(const char *s);
  int count_leading_white_spaces(const char *s);
  int cmp_token(const char *opt, const char *token);

private:

  std::vector<Clo*> _clos;			// array of options
  bool _help;
};

extern CloParser *theCloParser;

#endif
