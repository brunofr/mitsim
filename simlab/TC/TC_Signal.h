//-*-c++-*------------------------------------------------------------
// NAME: Traffic signal and signs with communication support
// AUTH: Qi Yang
// FILE: TC_Signal.h
// DATE: Thu Nov 16 22:51:24 1995
//--------------------------------------------------------------------

#ifndef TC_SIGNAL_HEADER
#define TC_SIGNAL_HEADER

#include <IO/IOService.h>

// TC_Signal inherits DRN_Signal in graphical mode or RN_Signal in
// batch mode.

#ifndef INTERNAL_GUI

#include <GRN/RN_Signal.h>
#define DRN_Signal RN_Signal
#else
#include <DRN/DRN_Signal.h>
#endif

class TC_Signal : public virtual DRN_Signal
{
   public:

      TC_Signal();
      virtual ~TC_Signal() { }

      // These functions are used for interprocess communication.  The
      // default behavior of these two functions are sending and
      // receiving signal state -- a unsigned int.

      virtual void send(IOService &);
      virtual void receive(IOService &);

      virtual void state(unsigned int s);
      virtual unsigned int state();
};

#endif
