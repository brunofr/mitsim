//-*-c++-*------------------------------------------------------------
// NAME: Local Responsive Ramp metering controller
// AUTH: Owen J. Chen & Qi Yang
// FILE: TC_LocalResponsiveController.h
// DATE: Sun Nov 10 21:55:41 EST 1996
//--------------------------------------------------------------------

#ifndef TC_LCOALRESPONSIVECONTROLLER_HEADER
#define TC_LCOALRESPONSIVECONTROLLER_HEADER

#include <iostream>
#include <vector>

#include "TC_ResponsiveController.h"
#include "TC_Sensor.h"

class Reader;
class TC_Phase;

class TC_LocalResponsiveController : public TC_ResponsiveController
{
      friend class TC_Phase;

    protected:
      double regulator_;
      double desiredMeasurement_;
      std::vector<RN_Sensor *> mainlineDetectors_; // measurement sensors

   public:

      TC_LocalResponsiveController(int c, short int t) 
		 : TC_ResponsiveController(c, t) { }
	  virtual ~TC_LocalResponsiveController() { }

      virtual void read(Reader&);
      void readRegulator(Reader&);
      virtual void calculateMeteringRate(double regionRate);
      virtual void print(std::ostream &os = std::cout);


      inline short int nMainlineDetectors() {
		 return mainlineDetectors_.size();
      }
      double calcMainlineDetectorMeasurement();
      void printRegulators(std::ostream &os = std::cout);
};

#endif

