//-*-c++-*------------------------------------------------------------
// NAME: Signal Group for Generic Controller
// AUTH: Angus Davol
// FILE: TC_SignalGroup.cc
// DATE: Feb 2001
//--------------------------------------------------------------------

#include <Tools/ToolKit.h>

#include <IO/Exception.h>

#include <GRN/Constants.h>
#include <GRN/RN_Signal.h>

#include "TC_Controller.h"
#include "TC_GenericController.h"
#include "TC_SignalGroup.h"
#include "TC_Condition.h"
//#include <iostream>
using namespace std;


// Read logic conditions from file

void
TC_SignalGroup::read(Reader& is)
{
  short int num, c;

  //  is >> referenceTime_ >> initialPeriod_ >> num;
  is >> initialPeriod_ >> num;

  conditions_.reserve(num);

  if (!is.findToken(OPEN_TOKEN)) theException->exit(1);

  for (c = 0; c < num; c ++) {

    conditions_.push_back(newCondition());
    conditions_[c]->signalGroup(this);
    conditions_[c]->code(c);
    conditions_[c]->read(is);

  }
  
  if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
  
}


void
TC_SignalGroup::checkConditions()
{

  short int c;

  action(0);  // initialize SignalGroup action
  unsetStatus(BUS_DETECTED); // reset bus detection flag.

  for (c = 0; c < nConditions(); c ++) {
    
    condition(c)->check();
    
    if (condition(c)->status() == CONDITION_TRUE) {
      switch (condition(c)->conditionCode() / 100)
        {
        case HOLD:
          action(HOLD);
// 	  // debug
// 	  cout << "hold condition: " << condition(c)->conditionCode() << endl;
// 	  //
          break;
          
        case CHANGE: 
          nextPeriod(condition(c)->nextPeriod());
          action(CHANGE);
          break;
        }
      break;
    }

    if (condition(c)->status() == CONDITION_SKIP_NEXT)
      c += condition(c)->numToSkip(); // don't check next X conditions

  }

  if ( !action() ) { // not held or forced to change
    if ( !nextPeriod() ) { // nextPeriod not defined
      // use default periods
      if (state() == SIGNAL_GREEN)  nextPeriod(SIGNAL_YELLOW);
      if (state() == SIGNAL_YELLOW)  nextPeriod(SIGNAL_RED);
      if (state() == SIGNAL_RED)  nextPeriod(SIGNAL_GREEN);
      if (state() == 18)  nextPeriod(SIGNAL_GREEN); // yellow-red
      // for protected movements:
      if (state() == SIGNAL_GREEN + SIGNAL_ARROW)
	nextPeriod(SIGNAL_YELLOW + SIGNAL_ARROW);
      if (state() == SIGNAL_YELLOW + SIGNAL_ARROW)
	nextPeriod(SIGNAL_RED + SIGNAL_ARROW);
      if (state() == SIGNAL_RED + SIGNAL_ARROW)
	nextPeriod(SIGNAL_GREEN + SIGNAL_ARROW);
      if (state() == 18 + SIGNAL_ARROW)
	nextPeriod(SIGNAL_GREEN + SIGNAL_ARROW); // yellow-red
    }
    action(CHANGE);
  }

}


void
TC_SignalGroup::state(short int n)
{
  state_ = n;
  periodStartTime_ = theSimulationClock->currentTime();

  nextPeriod(0); // re-initialize
  unsetStatus(GAP_OUT);

  short int color = n & SIGNAL_COLOR;
  if (color == SIGNAL_GREEN) {
    unsetStatus(COMPLETED + SKIPPED);
    setStatus(ACTIVE);
  }
  if (color == SIGNAL_YELLOW) {
    unsetStatus(COMPLETED + SKIPPED + GAP_OUT);
    setStatus(ACTIVE + PASSIVE);
  }
  if (color == SIGNAL_RED) {
    // clear conflicts
    for (short int i = 0; i < nPrecedingGroups_; i ++)
      controller()->signalGroup(precedingGroups(i))->unsetStatus(COMPLETED);
    unsetStatus(ACTIVE + PASSIVE);
    setStatus(COMPLETED);
  }
}
