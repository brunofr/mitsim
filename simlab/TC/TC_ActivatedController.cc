//-*-c++-*------------------------------------------------------------
// NAME: Simulation Widges
// AUTH: Qi Yang
// FILE: TC_ActivatedController.C
// DATE: Fri Oct 27 22:37:38 1995
//--------------------------------------------------------------------

#include "TC_ActivatedController.h"
#include "TC_ActivatedPhase.h"
#include "TC_Phase.h"
#include "TC_Interval.h"
#include "TC_Signal.h"
#include "TC_Sensor.h"

#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <Tools/SimulationClock.h>

#include <IO/Exception.h>

#include <GRN/NameTable.h>
#include <GRN/RoadNetwork.h>
using namespace std;

TC_Phase*
TC_ActivatedController::newPhase()
{
   return new TC_ActivatedPhase;
}


void
TC_ActivatedController::readSignals(Reader& is)
{
   is >> nApproaches_;
   
   // Allocate space for signal related variables

   maxRedTime_ = new short int [nApproaches_];
   redCalledPhaseIndex_ = new short int [nApproaches_];

   short int i, num = nApproaches_ * nEgresses_;
   redTime_ = new short int [num];
   for (i = 0; i < num; i ++) {
      redTime_[i] =  0;
   }

   if (!is.findToken(OPEN_TOKEN)) theException->exit(1); 
   for (i = 0; i < nApproaches_; i ++) {
      signals_.push_back(readSignal(is, i));
   }
   if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
}


RN_Signal*
TC_ActivatedController::readSignal(Reader& is, short int i)
{
   RN_Signal *s;
   int id;
   short int pid;

   if (!is.findToken(OPEN_TOKEN)) theException->exit(1); 
   is >> id >> maxRedTime_[i] >> pid;
   if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);

   if ((s = theNetwork->findSignal(id)) == NULL) {
      cerr << "Error:: Unknown signal <" << id << ">. ";
      is.reference();
      theException->exit(1);
   }

   redCalledPhaseIndex_[i] = pid - 1;
   return s;
}


void
TC_ActivatedController::readCalledPhaseTable(Reader& is)
{
   short int i, id;
   is >> nCalledPhases_;
   calledPhases_ = new short int[nCalledPhases_];
   if (!is.findToken(OPEN_TOKEN)) theException->exit(1);
   for (i = 0; i < nCalledPhases_; i ++) {
      is >> id;
      if (id > 0 && id <= nPhases()) {
		 calledPhases_[i] = id - 1;
      } else {
		 cerr << "Error:: Unknown phase <" << id << ">. ";
		 is.reference();
		 theException->exit (1);
      }
   }
   if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
}


void
TC_ActivatedController::readExtendedPhaseTable(Reader& is)
{
   short int i, id;
   is >> nExtendedPhases_;
   extendedPhases_ = new short int[nExtendedPhases_];
   if (!is.findToken(OPEN_TOKEN)) theException->exit(1);
   for (i = 0; i < nExtendedPhases_; i ++) {
      is >> id;
      if (id > 0 && id <= nPhases()) {
		 extendedPhases_[i]= id - 1;
		 ((TC_ActivatedPhase *)phase(id-1))->extendedIndex(i);
      } else {
		 cerr << "Error:: Unknown phase <" << id << ">. ";
		 is.reference();
		 theException->exit (1);
      }
   }
   if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
}


void
TC_ActivatedController::readDetectorTable(Reader& is)
{
   int n;
   is >> nDetectors_;
   detectors_.reserve(nDetectors_);

   n = nDetectors_ * nCalledPhases() + 1;
   calledSpec_ = new char[n];
   memset(calledSpec_, 0, n);

   n = nDetectors_ * nExtendedPhases() + 1;
   extendedSpec_ = new char[n];
   memset(extendedSpec_, 0, n);

   if (!is.findToken(OPEN_TOKEN)) theException->exit(1);
   for (short int i = 0; i < nDetectors_; i ++) {
      detectors_.push_back(readDetector(is, i));
   }
   if (nDetectors_ != detectors_.size()) {
	  cerr << "Error:: Specification does not match number of detectors. ";
	  is.reference();
	  theException->exit(1);
   }
   if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
}


RN_Sensor*
TC_ActivatedController::readDetector(Reader& is, short int i)
{
   if (!is.findToken(OPEN_TOKEN)) theException->exit(1);
   
   int id;
   is >> id;
   RN_Sensor * s = theNetwork->findSensor(id);

   if (s == NULL) {
      cerr << "Error:: Unknown sensor <" << id << ">. ";
      is.reference();
      theException->exit(1);
   }

   readCalledPhaseSpec(is, i);
   readExtendedPhaseSpec(is, i);

   if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);

   return s;
}


void
TC_ActivatedController::readCalledPhaseSpec(Reader& is, short int i)
{
   short int j, flag;
   if (!is.findToken(OPEN_TOKEN)) theException->exit(1);
   for (j = 0; j < nCalledPhases(); j ++) {
      is >> flag;
      calledSpec_[j * nDetectors_ + i] = flag;
   }
   if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
}


void
TC_ActivatedController::readExtendedPhaseSpec(Reader& is, short int i)
{
   short int j, flag;
   if (!is.findToken(OPEN_TOKEN)) theException->exit(1);
   for (j = 0; j < nExtendedPhases(); j ++) {
      is >> flag;
      extendedSpec_[j * nDetectors_ + i] = flag;
   }
   if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
}

void
TC_ActivatedController::read(Reader& is)
{
   // Read signals, timing and sequence tabeles

   is >> signalType_ >>  nEgresses_;

   startTime_ = (long)theSimulationClock->currentTime();

   if (!is.findToken(OPEN_TOKEN)) theException->exit(1);

   readSignals(is);
   readTimingTable(is);

   // Read ids for called phases
   readCalledPhaseTable(is);

   // Read ids for extended phases
   readExtendedPhaseTable(is);

   // Read detector ids and activation logic
   readDetectorTable(is);

   if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
   switchTime_ = initializeSignalStates();
   updateTime_ = ONE_DAY;
}


void
TC_ActivatedController::printCurrentState(ostream &os)
{
   os << indent << updateTime_ << endc << switchTime_ << endc
      << " # scheduled update and switch time" << endl;
   os << indent << "{ # Current state:" << endl;
   os << indent << indent << (currentPhase_+1)
      << " # Phase" << endl;
   os << indent << indent << (currentInterval_+1)
      << " # Interval" << endl;
   os << indent << "}" << endl;
}


void
TC_ActivatedController::print(ostream &os)
{
   os << indent;
   os << code_ << endc
      << type_ << endc
      << signalType_ << endc;
   os << OPEN_TOKEN << endc << "# Code" << endc
      << NameTable::controller(type_) << endc
      << NameTable::signal(signalType_) << endl;

   printSignals(os);
   printTimingTable(os);
   printCalledPhaseTable(os);
   printExtendedPhaseTable(os);
   printDetectorTable(os);

   os << indent << CLOSE_TOKEN << endl;
}


void
TC_ActivatedController::printSignals(ostream &os)
{
   short int i;
   os << indent << indent << nApproaches() << endc
      << OPEN_TOKEN << endc << "# NumSignals" << endl;
   for (i = 0; i < nApproaches(); i ++) {
      printSignal(os, i);
   }
   os << CLOSE_TOKEN << endl;
}


void
TC_ActivatedController::printSignal(ostream &os, short int i)
{
   os << indent << indent << indent << OPEN_TOKEN << endc;
   os << signals_[i]->code() << endc
      << maxRedTime_[i] << endc
      << (redCalledPhaseIndex_[i] + 1) << endc;
   os << CLOSE_TOKEN << endl;
}


void
TC_ActivatedController::printCalledPhaseTable(ostream &os)
{
   short int j;
   os << indent << indent << nCalledPhases() << endc
      << OPEN_TOKEN << endc
      << "# NumCalledPhases" << endl;
   os << indent << indent << indent
      << "# PhaseIDs" << endl;
   os << indent << indent << indent;
   for (j = 0; j < nCalledPhases(); j ++) {
      os << (calledPhases_[j] + 1) << endc;
   }
   os << endl << indent << indent << CLOSE_TOKEN << endl;
}


void
TC_ActivatedController::printExtendedPhaseTable(ostream &os)
{
   short int j;
   os << indent << indent << nExtendedPhases() << endc
      << OPEN_TOKEN << endc
      << "# Number of extended phases" << endl;
   os << indent << indent << indent
      << "# PhaseIDs" << endl;
   os << indent << indent << indent;
   for (j = 0; j < nExtendedPhases(); j ++) {
      os << (extendedPhases_[j] + 1) << endc;
   }
   os << endl << indent << indent << CLOSE_TOKEN << endl;
}


void
TC_ActivatedController::printDetectorTable(ostream &os)
{
   short int i, j;
   os << indent << indent << nDetectors() << endc
      << OPEN_TOKEN << endc
      << "# NumDetectors" << endl;
   os << indent << indent << indent
      << "# DetectorID {CalledSpecs} {ExtendedSpecs}" << endl;
   for (i = 0; i < nDetectors(); i ++) {
      os << indent << indent << indent 
		 << detectors_[i]->code() << endc;

      os << OPEN_TOKEN << endc; 
      for (j = 0; j < nCalledPhases(); j ++) {
		 os << calledPhaseSpec(i, j) << endc;
      }
      os << CLOSE_TOKEN << endc;

      os << OPEN_TOKEN << endc; 
      for (j = 0; j < nExtendedPhases(); j ++) {
		 os << extendedPhaseSpec(i, j) << endc;
      }
      os << CLOSE_TOKEN << endl;
   }
   os << indent << indent << CLOSE_TOKEN << endl;
}


// Returns specification of ith detector for jth called phase

short int
TC_ActivatedController::calledPhaseSpec(short int i, short int j)
{
   return (calledSpec_[j * nDetectors_ + i]);
}


// Returns specification of ith detector for jth extended phase

short int
TC_ActivatedController::extendedPhaseSpec(short int i, short int j)
{
   return (extendedSpec_[j * nDetectors_ + i]);
}


// The newly created controller call this function to initialize the
// state of signals.  The function returns the scheduled time for the
// next switching (by calling switchInterval())

long TC_ActivatedController::initializeSignalStates()
{
   short int &p = currentPhase_ = 0;
   short int &i = currentInterval_ = 0;

   TC_Interval* si = phase(p)->interval(i);

   clearMaxRedTimes();
   delta_ = si->length();
   si->setSignalStates();

   return (long) theSimulationClock->currentTime() + delta_;
}


// Switch to ith interval of pth phase.  It returns time scheduled for
// next updating.

long TC_ActivatedController::switchTo(short int p, short int i)
{
   currentPhase_ = p;
   currentInterval_ = i;

   TC_Interval *si = interval();
   delta_ = si->length();
   si->setSignalStates();

   return (long)theSimulationClock->currentTime() + delta_;
}


// If red time for any turning movement has reached maximum and
// current phase is completed (last interval expired), it returns the
// index of phase (rcp) to be called; otherwise, it returns -1.

short int
TC_ActivatedController::stopRedPhase(short int rcp)
{
   TC_ActivatedPhase *p = (TC_ActivatedPhase *)phase();
   if (rcp < 0 || currentInterval_ != p->nIntervals() - 1)
      return -1;
   else {
      if (p->recallToMinGreenTime()) {

		 // Current phase is to be terminated and the finished phases
		 // should to reset to the minimun green.

		 p->interval(0)->length(p->minGreenTime());
      }
      return rcp;
   }
}


// This function returns time to be extended if current phase is
// extendable or 0 otherwise.

short int
TC_ActivatedController::extendCurrentPhase(short int rcp)
{
   TC_ActivatedPhase * sp = (TC_ActivatedPhase *)phase();
   TC_Interval *si = interval();

   if (currentInterval_ != 0 ||	// not the first interval
       sp->extendedIndex() < 0 || // not an extendable phase
       si->length() >= sp->maxGreenTime()) // reached max green
      return 0;

   const char *spec = extendedSpec_ + sp->extendedIndex() * nDetectors_;

   // Check if the states of the detectors meet all the requirements
   // to extend this phase (specified by the "spec")

   int status = checkDetectors(spec);

   if (status == PREEMPTIVE || // State of other sensor ignored
       status != 0 &&		// Satisfies all the requirement
       rcp < 0) {		// No signal reached max red
 
      // Phase and interval do not change in this case. Extend the
      // interval for another delta seconds
      
      delta_ = sp->extensionTime();
      TC_Interval *si = interval();
      si->length(si->length() + delta_);
      si->setSignalStates();
      return delta_;
   }
   return 0;
}


// This function returns index to a called phase if current detector
// data satisfies the specification in the control logic, or returns
// -1 otherwise.

short int
TC_ActivatedController::callPhase(short int rcp)
{
   if (currentInterval_ != phase()->nIntervals() - 1)
      return -1;

   // Check each callable phase in order and to go to a phase if it
   // meet all the requirement to call that phase

   register short int j;

   for (j = 0; j < nCalledPhases(); j ++) {
      if (calledPhases_[j] == currentPhase_) continue;

	  const char *spec = calledSpec_ + j * nDetectors_;

      // Check if the states of the detectors and meet the
      // requirements to call phase j (specified by the "spec")

      int status = checkDetectors(spec);

      if (status == PREEMPTIVE || // State of other sensor ignored
		  status != 0 &&	// Satisfies all the requirement
		  rcp < 0) {		// No signal reach max red
		 TC_ActivatedPhase* p = (TC_ActivatedPhase *)phase();
		 if (p->recallToMinGreenTime()) {

			// Current phase is to be terminated and the finished phases
			// should to reset to the minimun green.

			p->interval(0)->length(p->minGreenTime());
		 }
		 return calledPhases_[j];
      }
   }
   return -1;
}


// This function switches the control to next interval and returns the
// time scheduled for next updating

long TC_ActivatedController::switchToNextInterval()
{
   short int i = (currentInterval_ + 1) % phase()->nIntervals();
   short int p = currentPhase_;
   TC_ActivatedPhase *ph = (TC_ActivatedPhase *)phase();
   if (i == 0) {
      if (ph->recallToMinGreenTime()) {

		 // Current phase is to be terminated and the finished phases
		 // should to reset to the minimun green.

		 ph->interval(0)->length(ph->minGreenTime());
      }
      p = (currentPhase_ + 1) % nPhases();
   }
   return switchTo(p, i);
}
   

// This function updates the signal state according to the current
// detector information and the logic specified for this controller.
// It returns the scheduled time for next updating.

long TC_ActivatedController::switchInterval()
{
   short int rcp = updateMaxRedTimes();
   short int p;

   // IF IT IS STILL IN THE FIRST INTERVAL, CHECK IF THE CURRENT PHASE
   // (FIRST INTERVAL) IS EXTENDABLE

   delta_ = extendCurrentPhase(rcp);
   if (delta_ > 1.0e-2) {
      return (long)theSimulationClock->currentTime() + delta_;
   }

   // CHECK IF ANY PHASE SHOULD BE CALLED
   
   if ((p = callPhase(rcp)) >= 0) {
      return switchTo(p);
   }

   // CHECK IF ANY TURNING MOVEMENT REACHES MAX RED TIME AND SWITCH TO
   // THE PHASE ASSIGNED IN FAVOR OF THAT TURNING MOVEMENT IF CURRENT
   // PHASE HAS BEEN FINISHED.

   if ((p = stopRedPhase(rcp)) >= 0) {

      // This checking is done after extending and calling logic
      // because maximum red calling logic is assigned a lower
      // priority to allow simulation of preemptive control in
      // extending and calling logic.

      return switchTo(p);
   }

   // Failed to extend current phase or call another phase. Current
   // phase will be terminated and the control will switch to the next
   // interval in the table.
   
   return switchToNextInterval();
}


// This function checks detector data and returns ACTIVATED,
// PREEMPTIVE, or 0 according to the specification of controller logic.

int
TC_ActivatedController::checkDetectors(const char *spec)
{
   const double epsilon = 1.0E-4;
   short int i, flag;
   double occ;

   int idle = 0;
   int activated = 0;

   for (i = 0; i < nDetectors(); i ++) {

      occ = detectors_[i]->measurement();

      flag = spec[i];

      switch (flag) {
		 case DO_NOT_CARE:
		   break;

		 case MUST_BE_IDLE:
		   if (occ > epsilon) idle += 1;
		   break;

		 case ACTIVATED:
		   if (occ > epsilon) activated += 1;
		   break;

		 case PREEMPTIVE:
		   if (occ > epsilon) return (PREEMPTIVE);
		   // Quit checking other detectors
		   break;

		 default:
		   break;
      }
   }
   if (activated > 0 && idle == 0) return (ACTIVATED);
   else return (0);
}


// Reset redTime[] for each turning movement to zero

void
TC_ActivatedController::clearMaxRedTimes()
{
   register short int i;
   short int n = nApproaches_ * nEgresses_;
   for (i = 0; i < n; i ++) {
      redTime_[i] = 0;
   }
}


// Calculates accumulated red time for a turning movement and returns
// the index to a red calling phase.

short int
TC_ActivatedController::updateMaxRedTimes()
{
   TC_Interval *si = interval();
   unsigned int s;
   short int i, num = nEgresses();
   register short int j, k;
   int extra, max_val = 0;
   short int max_signal = -1;

   for (i = 0; i < nApproaches(); i ++) {
      for (j = 0; j < num; j ++) {

		 s = si->state(i, j);
		 k = i * num + j;

		 if (s != 0xF &&	// Movement should be allowed 
			 (s & SIGNAL_COLOR) == SIGNAL_RED && // Signal is red
			 !(s & SIGNAL_FLASHING)) { // Not flushing
			if (redTime_[k] <= 3600) {
			   redTime_[k] += delta_;
			} else {
			   redTime_[k] = 3600;
			}
		 } else {			// Signal is not red
			redTime_[k] = 0;
		 }
		 if ((extra = redTime_[k] - maxRedTime_[i]) > max_val) {
			max_val = extra;
			max_signal = i;
		 }
      }
   }
   if (max_signal < 0) return (-1);
   else return (redCalledPhaseIndex_[max_signal]);
}

