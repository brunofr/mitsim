//-*-c++-*------------------------------------------------------------
// NAME: Responsive Ramp metring controller
// AUTH: Owen J. Chen & Qi Yang
// FILE: TC_AreaResponsiveController.C
// DATE: Sun Nov 10 21:55:41 EST 1996
//--------------------------------------------------------------------


#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <Tools/SimulationClock.h>
#include <Tools/Reader.h>
#include <IO/Exception.h>

#include <GRN/RoadNetwork.h>
#include <GRN/Constants.h>
#include "TC_Sensor.h"
#include "TC_Signal.h"
#include "TC_ResponsiveController.h"
#include "TC_LogFile.h"
using namespace std;

void
TC_ResponsiveController::read(Reader &is)
{
  is >>  updateStepSize_  >> signalType_ >>  nEgresses_;

  startTime_ = (long) theSimulationClock->currentTime();

  if (!is.findToken(OPEN_TOKEN)) theException->exit(1);

  readSignals(is);
  readTimingConstraints(is);
  readQueueDetectors(is);

  if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);

  initializePhases();
  switchTime_ = initializeSignalStates();
  updateTime_ = (long) theSimulationClock->currentTime();
}


void
TC_ResponsiveController::readTimingConstraints(Reader& is)
{
  int flag;

  is >> flag;

  if (method_) {
	delete [] method_;
  }

  if (flag == TC_MeteringMethod::METERING_METHOD_SINGLE) {
	method_ = new TC_SingleMetering(this);
  } else {
	method_ = new TC_PlatoonMetering(this);
  }
  method_->read(is);

  meteringRate(method_->maxMeteringRate());

  cout << " Initialize Ramp " << code() 
	   << " to maximum rate = " << method_->maxMeteringRate()  << endl;
}

void
TC_ResponsiveController::readQueueDetectors(Reader& is)
{
  int num = 0, id;
  if (!is.findToken(OPEN_TOKEN)) theException->exit(1);
  is >> queueMeasurement_ >> num;
  queueDetectors_.reserve(num);
  if (!is.findToken(OPEN_TOKEN)) theException->exit(1);  
  for (int i = 0; i < num; i ++) {
	is >> id;
	queueDetectors_.push_back(theNetwork->findSensor(id));
	if (!queueDetectors_[i]) {
	  cerr << "Error:: Unknown sensor <" << id << ">. ";
	  is.reference();
	  theException->exit(1);
	}
  }
  if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
  if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
}

void
TC_ResponsiveController::print(ostream &os)
{
  os << indent;
  os << code_ << endc
	 << type_ << endc
	 << signalType_ << endc
	 << nEgresses_ << endc
	 << OPEN_TOKEN << endl;

  printSignals(os);
  method_->print(os);
  printQueueDetectors(os);

  os << indent << CLOSE_TOKEN << endl;
}


void
TC_ResponsiveController::meteringRate(double rate)
{
  rate_ = rate;
}

void
TC_ResponsiveController::printQueueDetectors(ostream &os)
{
  os << indent << indent;
  os << nQueueDetectors() << "# Queue detectors" << endl;
  os << indent << indent << indent << OPEN_TOKEN;
  for (short int i = 0; i < nQueueDetectors(); i ++) {
	os << queueDetectors_[i]->code() << endc;
  }
  os << endl << CLOSE_TOKEN << endl;
}


// Returns scheduled time for next updating

void
TC_ResponsiveController::calculateParameters(double regionRate)
{
  long now = (long)theSimulationClock->currentTime();

  // This differs for each type of controller.
  calculateMeteringRate(regionRate);


  // Translate the metering rate into control intervals. 
  translateRatesToTiming();

  // Let current interval finish, but make it short if it is longer
  // than new length for this interval
  long dt = switchTime_ - now; 
  if (dt > interval()->length()) {
	switchTime_ = now + interval()->length();
  }

  // update scheduled time for next updating.
  updateTime_ = now + updateStepSize_;
}


// This may differ for each algorithm
void 
TC_ResponsiveController::calculateMeteringRate(double regionRate)
{
  TC_LogFile::os() << (long)theSimulationClock->currentTime()
				   << " id=" << code_;

  if (regionRate > 1.0) meteringRate(regionRate);

  // check min and max rates and ramp queue
  checkMeteringRateBoundary();
}


// Translate the metering rate into control intervals. We assume
// the cycle length and yellow time are fixed.

void 
TC_ResponsiveController::translateRatesToTiming()
{
  method_->setSignalTiming(phase(0), meteringRate());
}


int
TC_ResponsiveController::checkMeteringRateBoundary()
{
  int status = 0;

  // Check ramp queue, min and max metering rates
  
  double queue_mst = calcQueueDetectorMeasurement();
  if (queue_mst > queueMeasurement_) {
	// Queue override logic applied
	meteringRate(method_->maxMeteringRate()) ;
	status = STATUS_OVERRIDE_QUEUE;
  } else if (meteringRate() < method_->minMeteringRate()) {
	// minimum metering rate applied
	meteringRate(method_->minMeteringRate());
	status = STATUS_MIN_RATE;
  } else if (meteringRate() > method_->maxMeteringRate()) {
	// maximum metering rate applied
	meteringRate(method_->maxMeteringRate());
	status = STATUS_MAX_RATE;
  }

  TC_LogFile::os() << " q=" << Fix(queue_mst, 0.1)
				   << " s=" << hex << status << dec;

  return status;
}


double
TC_ResponsiveController::calcQueueDetectorMeasurement()
{
  double measure = 0.0;
  short int num = 0;
  register short int i;
  for (i = 0; i < nQueueDetectors(); i ++) {
	if (!(queueDetectors_[i]->state() & SENSOR_BROKEN)) {
	  measure += queueDetectors_[i]->measurement();
	  num ++;
	}
  }
  if (num) return ( measure / num);
  else return 0.0;
}


int
TC_ResponsiveController::calcQueueDetectorFlow()
{
  int measure = 0;
  register short int i;
  for (i = 0; i < nQueueDetectors(); i ++) {
	if (!(queueDetectors_[i]->state() & SENSOR_BROKEN)) {
	  measure += queueDetectors_[i]->flow();
	}
  }
  return measure;
}



//The newly created Reponsive controller call this function to 
// initialize a single phase with three intervals(green,yellow and red)

void
TC_ResponsiveController::initializePhases()
{
  TC_Phase* phase = new TC_Phase;
  phases_.reserve(1);
  phases_.push_back(phase);
  phases_[0]->controller(this);
  phases_[0]->code(1);
  phases_[0]->createDefaultIntervals();
  calculateMeteringRate(method_->maxMeteringRate());
  translateRatesToTiming();
}


// The newly created controller call this function to initialize the
// state of signals.  The function returns the scheduled time for the
// next switching (by calling switchInterval())

long TC_ResponsiveController::initializeSignalStates()
{
  short int &p = currentPhase_ = 0;
  short int &i = currentInterval_ = 0;

  TC_Interval* si = phase(p)->interval(i);
  si->setSignalStates();
  return (long) theSimulationClock->currentTime() + si->length();
}


// Switch to a new interval and schedule the time for next switching
// based on curren parameters.  Called when switchTime equals 0.  The
// function also update the signal states in the network.

long TC_ResponsiveController::switchInterval()
{
  short int &i = currentInterval_;
  short int t;

  do {
	i = (i + 1) % phase(0)->nIntervals();

	// We assume the last interval is adjustable (red) in all cases

	if (i == phase(0)->nIntervals() - 1) {
	  t = method_->calcRedTime(phase(0));
	} else {
	  t = interval(i)->length();
	}
  } while (t == 0);

  interval(i)->setSignalStates();

  return (long) theSimulationClock->currentTime() + t;
}


