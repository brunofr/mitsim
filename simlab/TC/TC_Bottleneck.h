//-*-c++-*------------------------------------------------------------
// NAME:Mainline Bottleneck Section for FLOW Coordinated Traffic
//      Signal Controllers
// AUTH: Owen J. Chen  
// FILE: TC_Bottleneck.h
// DATE: Tue Jan 28 15:59:26 EST 1997
//--------------------------------------------------------------------

#ifndef TC_BOTTLENECK_HEADER
#define TC_BOTTLENECK_HEADER

#include <iostream>
#include <vector>
#include <Templates/Queue.h>
#include <Tools/Math.h>
#include <Tools/Reader.h>
#include "TC_Sensor.h"
#include "TC_UnMeteredRamp.h"
#include "TC_Controller.h"

class RN_Signal;
class TC_Phase;
class TC_Interval;
class RoadNetwork;


//--------------------------------------------------------------------
// CLASS NAME: TC_Bottleneck
// Bottleneck
//--------------------------------------------------------------------


class TC_Bottleneck : public CodedObject
{
protected:

  int volumeReduction_;
  double thresholdOccupancy_;
  std::vector<float> weightingFactors_;
  std::vector<RN_Sensor*> upstreamDetectors_; // upstream volume  sensors
  std::vector<RN_Sensor*> downstreamDetectors_; // downstream volume sensors
  std::vector<TC_UnMeteredRamp*> offRamps_; // off-ramps
  std::vector<TC_UnMeteredRamp*> onRamps_; // unmetered on-ramps
  std::vector<TC_Controller*> meteredRamps_; // metered upstream on-ramps

public:
	
  TC_Bottleneck() : CodedObject() { }
  TC_Bottleneck(int c) : CodedObject(c) { }

  virtual ~TC_Bottleneck(){}
      
  int nMeteredRamps() { return meteredRamps_.size(); }

  inline short int nUpstreamDetectors() {
	return upstreamDetectors_.size();
  }
  inline short int nDownstreamDetectors() {
	return downstreamDetectors_.size();
  }
  inline  int nOffRamps() {
	return  offRamps_.size();
  }
  inline  int nOnRamps() {
	return  onRamps_.size();
  }

  virtual void read(Reader &);
  virtual void print(std::ostream &os = std::cout);
  void readUpstreamDetectors(Reader&);
  void readDownstreamDetectors(Reader&);
  void readUnMeteredRamps(Reader &);
  void readMeteredRamps(Reader &);
  void print(const char *msg, const std::vector<RN_Sensor*> &detector,
			 std::ostream &os = std::cout);
  void print(const char *msg, const std::vector<TC_UnMeteredRamp*> &ramps,
			 std::ostream &os = std::cout);
  double bottleneckOccupancy();
  int upstreamFlow();
  int downstreamFlow();
  int onRampFlow();
  int meteredRampFlow();
  int offRampFlow();
  void calculateParameters();
};

#endif

