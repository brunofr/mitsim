//-*-c++-*------------------------------------------------------------
// NAME: Regional System Manager for FLOW Coordinated Traffic Signal
//       Controllers
// AUTH: Owen J. Chen  
// FILE: TC_FlowControlRegion.h
// DATE: Mon Nov 11 12:18:52 EST 1996
//--------------------------------------------------------------------

#ifndef TC_FLOWCONTROLREGION_HEADER
#define TC_FLOWCONTROLREGION_HEADER

#include <iostream>
#include <vector>
#include <Tools/Math.h>
#include <Templates/Listp.h>
#include <Tools/Reader.h>
#include "TC_Sensor.h"
#include "TC_Bottleneck.h"
#include "TC_ControlRegion.h"

class TC_Phase;
class TC_Interval;
class TC_MeteringMethod;

//--------------------------------------------------------------------
// CLASS NAME: TC_FlowControlRegion
// FlowControlRegion manages the following classes of controllers: 
//        FlowResponsiveController, 
//--------------------------------------------------------------------


class TC_FlowControlRegion : public TC_ControlRegion
{
protected:

  double regionalReductionFactor_;

  // Two type of local metering formula can be used:
  //  1. Target occupancy and ragulator
  //  2. Occupancy-metering rate lookup table
  std::vector<float> localOParams_;	// target occ (ALINEA) or occ (TABLE)
  std::vector<float> localRParams_;	// regular (ALINEA) or rate (TABLE)

  std::vector<TC_Bottleneck *> bottlenecks_; // mainline bottlneck sections

public:
	
  TC_FlowControlRegion(int c, short int t)
	: TC_ControlRegion(c, t) { }

  ~TC_FlowControlRegion() { }

  inline double regionalReductionFactor() {
	return regionalReductionFactor_;
  }
  inline  int nBottlenecks() {
	return  bottlenecks_.size() ;
  }

  virtual void read(Reader &);
  virtual void controllersCalculateParameters();
  virtual void print(std::ostream &os = std::cout);
  void printLocalMeteringRateTable(std::ostream &os = std::cout);
  void readBottlenecks(Reader &);
  void readLocalMeteringRateTable(Reader &);
  double localMeteringRateTable(double occupancy);
  double localMeteringRateAlinea(TC_MeteringMethod *method,
								 double occupancy, double rate);
  int nRowsLookupTable() { return localOParams_.size(); }
};

#endif

