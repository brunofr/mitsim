//-*-c++-*------------------------------------------------------------
// NAME: Phase for a signal controller
// AUTH: Qi Yang
// FILE: TC_Phase.C
// DATE: Fri Oct 27 13:22:26 1995
//--------------------------------------------------------------------

#include <Tools/ToolKit.h>

#include <IO/Exception.h>

#include <GRN/Constants.h>
#include <GRN/RN_Signal.h>

#include "TC_Controller.h"
#include "TC_Phase.h"
#include "TC_Interval.h"
using namespace std;

TC_Phase::~TC_Phase()
{
  for (int i = 0; i < nIntervals(); i ++) {
	delete interval(i);
  }
}

// Read timint table from the file

void
TC_Phase::read(Reader& is)
{
   short int num, dt;
   short int n = controller_->nApproaches();

   if (!is.findToken(OPEN_TOKEN)) theException->exit(1);

   is >> num;			// number of intervals
   intervals_.reserve(num);

   for (short int i = 0; i < num; i ++) {
      is >> dt;			// time allocated to this interval
      intervals_.push_back(new TC_Interval(this, n));
      interval(i)->length(dt);
      readSequence(is, i);
   }
   
   if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
}


// Read sequence table for the ith interval

void
TC_Phase::readSequence(Reader& is, short int i)
{
   short int n = controller_->nApproaches();
   short int m = controller_->nEgresses();
   unsigned int indicators;

   if (!is.findToken(OPEN_TOKEN)) theException->exit(1);

   for (short int k = 0; k < n; k ++) {

      // Fill all the usable bits with ones

	  indicators = is.gethex();
	  interval(i)->setStates(k, indicators);
   }

   if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
}


void
TC_Phase::print(ostream &os)
{
   os << indent << indent << indent;
   os << OPEN_TOKEN << endc << nIntervals()
      << "\t# NumIntervals" << endl;
   for (short int i = 0; i < nIntervals(); i ++) {
      interval(i)->print(os);
   }
   os << indent << indent << indent << CLOSE_TOKEN << endl;
}

//Create default Intervals which consists of Green, Yellow and Red.
void 
TC_Phase::createDefaultIntervals()
{
   short int i;
   int n = controller_->nApproaches();
   intervals_.reserve(3);
   for (i = 0; i < 3; i ++) {
     //	  intervals_.push_back(new TC_Interval(this, 1));
     intervals_.push_back(new TC_Interval(this, n)); // Angus
   }
   for(i = 0; i < n; i++) {
	  interval(0)->setStates(i,SIGNAL_GREEN);
	  interval(1)->setStates(i,SIGNAL_YELLOW);
	  interval(2)->setStates(i,SIGNAL_RED);
   }
}
