//-*-c++-*------------------------------------------------------------
// NAME: Un-metered on-ramp and off-ramp
// AUTH: Owen J. Chen
// FILE: TC_UnMeteredRamp.h
// DATE: Wed Nov 13 22:07:31 EST 1996
//--------------------------------------------------------------------

#ifndef TC_UNMETEREDRAMP_HEADER
#define TC_UNMETEREDRAMP_HEADER

#include <new>
#include <iostream>

#include <vector>
#include <Templates/Object.h>
#include <Templates/Listp.h>
#include "TC_Sensor.h"

#include <Tools/Reader.h>
class RoadNetwork;

const UINT ON_RAMP = 0; // enumeration start
const UINT OFF_RAMP = 1; // enumeration end


//--------------------------------------------------------------------
// CLASS NAME: TC_UnMeteredRamp
// This class is used by TC_ControlRegionFLOW
//--------------------------------------------------------------------

       
class TC_UnMeteredRamp : public CodedObject
{
protected:

  // These are information read from database
  short int type_;		// on-ramp =0 or off-ramp =1
  std::vector<RN_Sensor*> detectors_;  // ramp detectors

public:
	
  TC_UnMeteredRamp(int c, short int t)
	: CodedObject(c), type_(t) { }

  ~TC_UnMeteredRamp() { }

  void read(Reader& is);
  void print(std::ostream &os = std::cout);
  inline short int type() { return type_; }
  void type(int t) { type_ = t;}
  inline short int nDetectors() {
	return detectors_.size();
  }
  double calculateMeasurement();
  int calculateFlow();
};

#endif
