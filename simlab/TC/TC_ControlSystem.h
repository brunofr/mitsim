//-*-c++-*------------------------------------------------------------
// NAME: System Manager for All Traffic Signal Controllers
// AUTH: Owen J. Chen  
// FILE: TC_ControlSystem.h
// DATE: Mon Nov 11 12:18:52 EST 1996
//--------------------------------------------------------------------

#ifndef TC_CONTROLSYSTEM_HEADER
#define TC_CONTROLSYSTEM_HEADER

#include <new>
#include <iostream>

#include <Tools/Math.h>
#include <Templates/Listp.h>

#include <GRN/Constants.h>
#include <Tools/Reader.h>

#include "TC_Phase.h"
#include "TC_ControlRegion.h"

class RN_Signal;
class TC_Phase;
class TC_Interval;
class RoadNetwork;
class TC_Controller ;
class TC_ControlRegion ;

//--------------------------------------------------------------------
// CLASS NAME: TC_ControlSystem
// ControlSystem manages the following classes of controllers: 
//        PretimedController,
//        ActivatedController, 
//        LocalResponsiveController, 
// All controllers are divided into isolated and coordinated controllers.
// Isolated controllers are stored in a linked list 'listIsolatedCoontrollers',
//
//        LocalResponsiveController, 
//        FlowResponsiveController, 
//        ResponsiveController, 
// Coordinated controllers are stored in a linked list 'listCoontrolRegion',
//
//--------------------------------------------------------------------

class TC_ControlSystem
{
   protected:

      static char* name_;		// file name
      static Reader is_;		// file reader
      static long nextTime_;	// time to load new plans

      static tcc_list_type listIsolatedControllers_;
      static tcr_list_type listControlRegions_;

   public:
	
      TC_ControlSystem() { }
      ~TC_ControlSystem() { }

      static char** nameptr() { return &name_; }
	  static char* name() { return name_; }
      static void openSignalFile();
      static long readTime();
      static void loadControllers();
      //      static void addControllers(int id, int type);
      static void printAllControllers(std::ostream &os = std::cout);
      static long nextTime() { return nextTime_; }
      static int nControllers() {
		 return nIsolatedControllers() + nCoordinatedControllers();
      }
      static int nIsolatedControllers() {
		 return listIsolatedControllers_.nNodes();
      }
      static int nCoordinatedControllers() {
		 return listControlRegions_.nNodes();
      }
      static void listIsolatedControllersInsert(int id, TC_Controller* sc);
      static void listControlRegionsInsert(int id, TC_ControlRegion* cr);

      static tcc_list_link_type find(int code);
      
      static void controllersCalculateParameters();
      static void controllersSwitchIntervals();
      
};

#endif
