//-*-c++-*------------------------------------------------------------
// NAME: Flow Responsive ramp metering controller
// AUTH: Owen J. Chen 
// FILE: TC_FlowResponsiveController.h
// DATE: Sun Nov 10 21:55:41 EST 1996
//--------------------------------------------------------------------

#ifndef TC_FLOWRESPONSIVECONTROLLER_HEADER
#define TC_FLOWRESPONSIVECONTROLLER_HEADER

#include <iostream>
#include <vector>

#include "TC_Sensor.h"
#include "TC_ResponsiveController.h"

class Reader;
class TC_Phase;
class TC_ControlRegion;

class TC_FlowResponsiveController : public TC_ResponsiveController
{
  friend class TC_Phase;

protected:

  //      double weightingFactor_; //  being moved to TC_Bottleneck.h

  std::vector<RN_Sensor *> queueAdjustDetectors_;  // queue adjustment detectors
  double queueAdjustOccupancy_; // queue adjustment threshold occupancy 

  // occupancy persistant time for queue adjustment(seconds):
  double queueAdjustPersistTime1_;  

  // second occupancy persistant time for additional queue adjustment
  // (seconds)
  double queueAdjustPersistTime2_; 

  // meter rate Incremental change for queue adjustment(veh/hr):
  double queueAdjustIncrement1_; 
  double queueAdjustIncrement2_; 

  int currentSamplingTime_; 
  int totalSamplingTimes_;

  // measure persistent time over queueAdjustoccupancy at queue
  // Detectors.
  bool *isAdjustPersist_;

  // measure persistent time over queueOverrideoccupancy at queue
  // Detectors.
  bool *isOverridePersist_;

  std::vector<RN_Sensor *> queueOverrideDetectors_;  // queue override detectors
  double queueOverrideOccupancy_; // queue override threshold occupancy 

  // occupancy persistant time for queue override(seconds):
  double queueOverridePersistTime_;  

  // meter rate Incremental change for queue override(veh/hr):
  double queueOverrideMeterRateIncrement_;

  // mainline occupancy measurement sensors
  std::vector<RN_Sensor *> mainlineDetectors_;

  float regionalReduction_;

public:

  TC_FlowResponsiveController(int c, short int t) 
	: TC_ResponsiveController(c, t) {currentSamplingTime_ = 0; }
  ~TC_FlowResponsiveController() { };

  inline short int nMainlineDetectors() {
	return mainlineDetectors_.size();
  }
  inline short int nQueueAdjustDetectors() {
	return queueAdjustDetectors_.size();
  }
  inline short int nQueueOverrideDetectors() {
	return queueOverrideDetectors_.size();
  }

  void meteringRateReduction(double reduction);
  void resetRegionalReduction();
  void regionalReduction(float reduction) {
	regionalReduction_ = reduction;
  }
  float regionalReduction() { return regionalReduction_; }

  virtual void read(Reader&);
  void readMainlineDetectors(Reader&);
  void readQueueAdjustDetectors(Reader&);
  void readQueueOverrideDetectors(Reader&);
  virtual void print(std::ostream &os = std::cout);
  void printMainlineDetectors(std::ostream &os = std::cout);
  void printQueueAdjustDetectors(std::ostream &os = std::cout);
  void printQueueOverrideDetectors(std::ostream &os = std::cout);
  virtual void calculateMeteringRate(double regionReductionFactor);
  double queueAdjustPersistTime();
  double queueOverridePersistTime();
  double calcMainlineMeasurement();
  double calcQueueAdjustMeasurement();
  double calcQueueOverrideMeasurement();
};

#endif
