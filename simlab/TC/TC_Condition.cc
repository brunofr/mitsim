//-*-c++-*------------------------------------------------------------
// NAME: Condition for Generic Controller Logic
// AUTH: Angus Davol
// FILE: TC_Condition.cc
// DATE: Feb 2001
//--------------------------------------------------------------------

#include <Tools/ToolKit.h>
#include <Tools/Random.h>
#include <Tools/Math.h>

#include <IO/Exception.h>

#include <GRN/Constants.h>
#include <GRN/RoadNetwork.h>
#include <GRN/RN_Signal.h>
#include <GRN/RN_Sensor.h>

#include "TC_Controller.h"
#include "TC_GenericController.h"
#include "TC_SignalGroup.h"
#include "TC_Condition.h"
#include <iostream>
using namespace std;


TC_Condition::~TC_Condition()
{
  for (int i = 0; i < nDetectors_; i ++) {
    delete detectors_[i];
  }
  delete [] parameters_;
}


// Read logic conditions from file.

void
TC_Condition::read(Reader& is)
{
  short int p;

  is >> conditionCode_;

  switch (conditionCode_)
    {
//       // no parameters:
//       nParameters_ = 0;
//       break;

      // 1 parameter:
    case   2: // delayed start
    case   3: // advance start
    case   6: // bus priority check-in
    case   7: // bus priority check-out
    case  99: // set controller output flag
    case 208: // hold indefinitely
    case 211: // hold if bus priority extension
    case 212: // hold if bus priority insertion
//     case 303: // skip if group flag = skipped
//     case 304: // skip if group flag != skipped
//     case 305: // skip if group flag = bus extension
//     case 306: // skip if group flag != bus extension
      nParameters_ = 1;
      break;

      // 2 parameters:
    case   1: // next period
    case 201: // minimum time
      nParameters_ = 2;
      break;

      // 3 parameters:
    case   5: // synchronize with other group
    case  14: // reset check-in counter
    case 202: // extension time (w/max, using gap timer)
    case 209: // preceding group not yet red
    case 103: // maximum time
      nParameters_ = 3;
      break;

      // 4 parameters:
    case 303: // skip if group's flag is set/not set
      nParameters_ = 4;
      break;

      // 5 parameters:
    case   8: // bus priority: extension
    case   9: // bus priority: insertion
    case  12: // bus priority: early start
    case 102: // force-off time
    case 104: // follow other group
    case 210: // hold in window
      nParameters_ = 5;
      break;

      // 6 parameters:
    case 106: // force-off (probabilistic)
    case 203: // extension time (w/max & continuous gap reduction, using gap timer)
      nParameters_ = 6;
      break;

      // 7 parameters:
    case 204: // extension time (w/max & stepwise gap reduction, using gap timer)
      nParameters_ = 7;
      break;

      // N parameters:
    case   4: // gap timer
    case  10: // set insertion flags for preceding groups
    case  11: // set insertion flags for following groups
    case  13: // set shortened flags for groups preceding early start
//     case 106: // minimum time (skippable)
//     case 102: // extension time
//     case 105: // extension time (with max)
    case 205: // complementary groups still active
    case 207: // preceding conflicting groups completed (hold)
    case 206: // no conflicting calls
    case 101: // skippable phase
    case 105: // force-off (if demand)
//     case 304: // preceding conflicting groups completed (revised)
    case 301: // skip if any detector occupied
    case 302: // skip if no detector occupied
      is >> nParameters_;
      break;
      
      // error:
    default:
      cerr << "Error:: Unknown ConditionCode <" << conditionCode() << ">. ";
    }
  
  parameters_ = new double [nParameters_];

  if (!is.findToken(OPEN_TOKEN)) theException->exit(1);

  for (p = 0; p < nParameters_; p ++) {
    is >> parameters_[p];
  }
  
  if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
  
}


// Evaluate conditions.

void
TC_Condition::check()
{
  short int i; // counter
  //  double e = theSimulationClock->stepSize() / 2; // epsilon for time equality conditions
  double time = theSimulationClock->currentTime(); // (used in conditions 102/5/6, 204)
  double changeTime; // (used in condition 102/5/6)
  double gap = -1; // (used in condition 203)

  status(-1);  // initialize Condition status

  switch (conditionCode())
    {
      
    case 1: // next period
      if ( signalGroup()->state() != parameter(0) )
        break;
      nextPeriod(parameter(1));
      signalGroup()->nextPeriod(parameter(1));
      break;


    case 2: // delayed start (offset)
      // If now > delayed start time, ignore condition.
      if ( theSimulationClock->currentTime() >
           signalGroup()->initializationTime() + parameter(0) ) {
        break;
      }
      // If now == delayed start time...     , and signal is blank, 
      else if ( AproxEqual(theSimulationClock->currentTime(),
			   signalGroup()->initializationTime() + parameter(0)) ) {
	// ...and signal is blank...
	if ( signalGroup()->state() == SIGNAL_BLANK ) {
	  signalGroup()->nextPeriod(signalGroup()->initialPeriod());
	  signalGroup()->action(CHANGE); // ...change to initial period...
	  status(CONDITION_TRUE); // ... and skip remaining conditions.
	  break;
	}
	// ...and signal is not blank...
	else {
	  break; // ...ignore condition.
	}
      }
      // If now < delayed start time...
      else {
	// ...and signal is already blank...
	if ( signalGroup()->state() == SIGNAL_BLANK ) {
	  signalGroup()->action(HOLD); // ...hold in blank state...
	  status(CONDITION_TRUE); // ... and skip remaining conditions.
	  break;
	}
	// ...and signal is not blank... 
	else {
	  signalGroup()->nextPeriod(SIGNAL_BLANK);
	  signalGroup()->action(CHANGE); // ...change to blank state...
	  status(CONDITION_TRUE); //... and skip remaining conditions.
	  break;
	}
      }

    case 3: // advance start
      if ( signalGroup()->periodStartTime() != controller()->startTime() )
        break;
      else {
	signalGroup()->periodStartTime( controller()->startTime() - parameter(0) );
	break;
      }


    case 4: // gap timer
      if ( nDetectors_ == -1 ) { // detectors have not been read in
	nDetectors_ = nParameters_;
	detectors_.reserve(nDetectors_);
	for (i = 0; i < nParameters_; i ++) {
	  detectors_.push_back(findDetector(parameter(i)));
	}
      }
      if ( checkDetectors() ) // detector occupied
	timer_ = theSimulationClock->currentTime(); // time of actuation
      
      if ( timer_ == -1 ) { // no actuations yet
	signalGroup()->gapTimer(-1);
	break;
      }

      // gap timer = time since last actuation
      signalGroup()->gapTimer( theSimulationClock->currentTime() - timer_ );
      break;


    case 5: // synchronize with other group
      // if already in correct period, hold
      if ( signalGroup()->state() == parameter(0) ) {
	signalGroup()->action(HOLD);
	break;
      }
      // if in wrong period and sync phase is in sync period, change to correct period
      if ( controller()->signalGroup(parameter(1))->state() == parameter(2) ) {
	signalGroup()->nextPeriod(parameter(0));
        signalGroup()->action(CHANGE);
	status(CONDITION_TRUE);
        break;
      }
      // otherwise, hold
      else {
        signalGroup()->action(HOLD);
	//        status(CONDITION_FALSE);
        break;
      }
      

    case 6: // bus priority check-in
      // Note: This condition uses store_ as a toggle, 
      //        with 1 = "sensor occupied" and 0 = "sensor not occupied".

      if ( nDetectors_ == -1 ) { // detector has not been read in
	nDetectors_ = nParameters_;
	detectors_.reserve(nDetectors_);
	detectors_.push_back(findDetector(parameter(0)));
      }

      // If unoccupied detector becomes occupied...
      if ( store_ == 0 && checkDetectors() ) {
	signalGroup()->setStatus(BUS_DETECTED); //...set bus detection flag...
	signalGroup()->incrementCheckinCounter(); //...and check-in.
	//	cout << "counter: " << signalGroup()->checkinCounter() << endl; // debug
      }
      
      // Update toggle:
      if ( checkDetectors() )
	store_ = 1; // occupied
      else
	store_ = 0; // not occupied

      break;
      

    case 7: // bus priority check-out
      // Note: This condition uses store_ as a toggle, 
      //        with 1 = "sensor occupied" and 0 = "sensor not occupied".

      if ( nDetectors_ == -1 ) { // detector has not been read in
	nDetectors_ = nParameters_;
	detectors_.reserve(nDetectors_);
	detectors_.push_back(findDetector(parameter(0)));
      }

      // If unoccupied detector becomes occupied...
      if ( store_ == 0 && checkDetectors() ) {
	signalGroup()->decrementCheckinCounter(); //...check-out.
	//	cout << "counter: " << signalGroup()->checkinCounter() << endl; // debug
      }
      
      // Update toggle:
      if ( checkDetectors() )
	store_ = 1; // occupied
      else
	store_ = 0; // not occupied

      break;
      

    case 8: // bus priority: extension

      time -= signalGroup()->initializationTime(); // Set "time" to time since initialization...
      time -= parameter(4); // ...minus offset...
      while (time >= parameter(3))
	time -= parameter(3); // ...then subtract cycle lengths to get time in cycle.

      // If extension flag is not already set...
      if ( !signalGroup()->status(BUS_EXTENSION) &&
	   //...and if a bus is present...
	   signalGroup()->checkinCounter() > 0 ) {
	   //	   signalGroup()->status(BUS_DETECTED) ) {
	//...and if time in cycle is within window...
	if ( ( parameter(0) < parameter(1) && // (case where start and end are in same cycle)
	       time >= parameter(0) &&
	       time <= parameter(1) ) ||
	     ( parameter(0) > parameter(1) && // (case where end time is in next cycle)
	       ( time >= parameter(0) ||
		 time <= parameter(1) ) ) ) {
	  //...set extension flag.
	  signalGroup()->setStatus(BUS_EXTENSION);
	  signalGroup()->unsetStatus(PASSIVE);
	  cout << "t=" << time << ": start extension @ controller " << controller()->code() 
	       << ", group " << signalGroup()->code() << endl; // debug
	  break;
	}
      }

      // If extension flag is already set and...
      if ( signalGroup()->status(BUS_EXTENSION) &&
	   // if a bus is not present...
	   ( signalGroup()->checkinCounter() == 0 ||
	     //...or if extension limit has been reached...
	     AproxEqual(time, parameter(2)) ) ) {
	//...unset extension flag.
	signalGroup()->unsetStatus(BUS_EXTENSION);
	signalGroup()->setStatus(PASSIVE);
	cout << "t=" << time << ": end extension @ controller " << controller()->code() 
	     << ", group " << signalGroup()->code() << endl; // debug
	break;
      }

//       // Reset check-in counter each cycle (at extension limit)
//       // to account for missed check-outs
//       if ( AproxEqual(time, parameter(2)) )
// 	signalGroup()->checkinCounter(0);

      break;


    case 9: // bus priority: inserted phase

      time -= signalGroup()->initializationTime(); // Set "time" to time since initialization...
      time -= parameter(4); // ...minus offset...
      while (time >= parameter(3))
	time -= parameter(3); // ...then subtract cycle lengths to get time in cycle.

      // If insertion flag is not already set...
      if ( !signalGroup()->status(BUS_INSERTION) &&
	   //...and if a bus is present...
	   signalGroup()->checkinCounter() > 0 ) {
	   //	   signalGroup()->status(BUS_DETECTED) ) {
	//...and if time in cycle is within window...
	if ( ( parameter(0) < parameter(1) && // (case where start and end are in same cycle)
	       time >= parameter(0) &&
	       time <= parameter(1) ) ||
	     ( parameter(0) > parameter(1) && // (case where end time is in next cycle)
	       ( time >= parameter(0) ||
		 time <= parameter(1) ) ) ) {
	  //...set insertion flag.
	  signalGroup()->setStatus(BUS_INSERTION);
	  cout << "t=" << time << ": start insertion @ controller " << controller()->code() 
	       << ", group " << signalGroup()->code() << endl; // debug
	  break;
	}
      }

      // If insertion flag is already set and...
      if ( signalGroup()->status(BUS_INSERTION) &&
	   // if a bus is not present...
	   ( signalGroup()->checkinCounter() == 0 ||
	     //...or if insertion limit has been reached...
	     AproxEqual(time, parameter(2)) ) ) {
	//...unset insertion flag.
	signalGroup()->unsetStatus(BUS_INSERTION);
	cout << "t=" << time << ": end insertion @ controller " << controller()->code() 
	     << ", group " << signalGroup()->code() << endl; // debug
	break;
      }

//       // Reset check-in counter each cycle (at insertion limit)
//       // to account for missed check-outs
//       if ( AproxEqual(time, parameter(2)) )
// 	signalGroup()->checkinCounter(0);

      break;
      
      
    case 10: // bus insertion: set flags for preceding groups
      
      time -= signalGroup()->initializationTime(); // Set "time" to time since initialization...
      time -= parameter(4); // ...minus offset...
      while (time >= parameter(3))
	time -= parameter(3); // ...then subtract cycle lengths to get time in cycle.

      // If insertion flag is set...
      if ( signalGroup()->status(BUS_INSERTION) ) {
	//...and if time in cycle is within window...
	if ( ( parameter(0) < parameter(1) && // (case where start and end are in same cycle)
	       time >= parameter(0) &&
	       time <= parameter(1) ) ||
	     ( parameter(0) > parameter(1) && // (case where end time is in next cycle)
	       ( time >= parameter(0) ||
		 time <= parameter(1) ) ) ) {
	  //...set insertion_after flags.
	  for (i = 5; i < nParameters_; i ++) {
	    controller()->signalGroup(parameter(i))->setStatus(BUS_INSERTED_AFTER);
	  }
	}
      }
      
      // Reset flags at reset time.
      if ( AproxEqual(time, parameter(2)) )
	  for (i = 5; i < nParameters_; i ++) {
	    controller()->signalGroup(parameter(i))->unsetStatus(BUS_INSERTED_AFTER);
	  }

      break;
      
      
    case 11: // bus insertion: set flags for following groups
      
      time -= signalGroup()->initializationTime(); // Set "time" to time since initialization...
      time -= parameter(4); // ...minus offset...
      while (time >= parameter(3))
	time -= parameter(3); // ...then subtract cycle lengths to get time in cycle.

      // If insertion flag is set...
      if ( signalGroup()->status(BUS_INSERTION) ) {
	//...and if time in cycle is within window...
	if ( ( parameter(0) < parameter(1) && // (case where start and end are in same cycle)
	       time >= parameter(0) &&
	       time <= parameter(1) ) ||
	     ( parameter(0) > parameter(1) && // (case where end time is in next cycle)
	       ( time >= parameter(0) ||
		 time <= parameter(1) ) ) ) {
	  //...set insertion_before flags.
	  for (i = 5; i < nParameters_; i ++) {
	    controller()->signalGroup(parameter(i))->setStatus(BUS_INSERTED_BEFORE);
	  }
	}
      }

      // Reset flags at reset time.
      if ( AproxEqual(time, parameter(2)) )
	  for (i = 5; i < nParameters_; i ++) {
	    controller()->signalGroup(parameter(i))->unsetStatus(BUS_INSERTED_BEFORE);
	  }

      break;
      
      
    case 12: // bus priority: early start

      time -= signalGroup()->initializationTime(); // Set "time" to time since initialization...
      time -= parameter(4); // ...minus offset...
      while (time >= parameter(3))
	time -= parameter(3); // ...then subtract cycle lengths to get time in cycle.

      // If early_start flag is not already set...
      if ( !signalGroup()->status(BUS_EARLY_START) &&
	   //...and if a bus is present...
	   signalGroup()->checkinCounter() > 0 ) {
	   //	   signalGroup()->status(BUS_DETECTED) ) {
	//...and if time in cycle is within window...
	if ( ( parameter(0) < parameter(1) && // (case where start and end are in same cycle)
	       time >= parameter(0) &&
	       time <= parameter(1) ) ||
	     ( parameter(0) > parameter(1) && // (case where end time is in next cycle)
	       ( time >= parameter(0) ||
		 time <= parameter(1) ) ) ) {
	  //...set early_start flag.
	  signalGroup()->setStatus(BUS_EARLY_START);
	  signalGroup()->unsetStatus(PASSIVE);
	  cout << "t=" << time << ": shortening @ controller " << controller()->code() 
	       << ", early start for group " << signalGroup()->code() << endl; // debug
	  break;
	}
      }

      // If early_start flag is already set and...
      if ( signalGroup()->status(BUS_EARLY_START) &&
	   //...if reset time has been reached...
	   AproxEqual(time, parameter(2)) ) {
	//...unset early_start flag.
	signalGroup()->unsetStatus(BUS_EARLY_START);
	break;
      }


    case 13: // bus early_start: set shortened flags for preceding groups
      
      time -= signalGroup()->initializationTime(); // Set "time" to time since initialization...
      time -= parameter(4); // ...minus offset...
      while (time >= parameter(3))
	time -= parameter(3); // ...then subtract cycle lengths to get time in cycle.

      // If early_start flag is set...
      if ( signalGroup()->status(BUS_EARLY_START) ) {
	//...and if time in cycle is within window...
	if ( ( parameter(0) < parameter(1) && // (case where start and end are in same cycle)
	       time >= parameter(0) &&
	       time <= parameter(1) ) ||
	     ( parameter(0) > parameter(1) && // (case where end time is in next cycle)
	       ( time >= parameter(0) ||
		 time <= parameter(1) ) ) ) {
	  //...set shortened flags.
	  for (i = 5; i < nParameters_; i ++) {
	    controller()->signalGroup(parameter(i))->setStatus(BUS_SHORTENED);
	  }
	}
      }
      
      // Reset flags at reset time.
      if ( AproxEqual(time, parameter(2)) )
	  for (i = 5; i < nParameters_; i ++) {
	    controller()->signalGroup(parameter(i))->unsetStatus(BUS_SHORTENED);
	  }

      break;
      

    case 14: // reset check-in counter (to account for missed vehicles)
      
      time -= signalGroup()->initializationTime(); // Set "time" to time since initialization...
      time -= parameter(2); // ...minus offset...
      while (time >= parameter(1))
	time -= parameter(1); // ...then subtract cycle lengths to get time in cycle.
     
      // Reset counter at reset time.
      if ( AproxEqual(time, parameter(0)) )
	signalGroup()->checkinCounter(0);
      
      break;
      

    case 99: // set controller output flag

      if (store_ == -1) { // not yet set
	controller()->outputFlag(parameter(0));
	store_ = 1;
      }
      break;

//////////////

    case 101: // skippable phase
      if ( signalGroup()->state() != parameter(0) )
        break;

      if ( nDetectors_ == -1 ) { // detectors have not been read in
	nDetectors_ = nParameters_ - 2;
	detectors_.reserve(nDetectors_);
	for (i = 2; i < nParameters_; i ++) {
	  detectors_.push_back(findDetector(parameter(i)));
	}
      }
      if ( AproxEqual(theSimulationClock->currentTime(),
		      signalGroup()->periodStartTime() ) && // just starting
	   !checkDetectors() ) { // no demand
	nextPeriod(parameter(1));
	signalGroup()->setStatus(SKIPPED);
	status(CONDITION_TRUE);
	break;
      }
      else {
	status(CONDITION_FALSE);
        break;
      }

      
    case 102: // force-off time
      if ( signalGroup()->state() != parameter(0) )
        break;

      time -= signalGroup()->initializationTime(); // set "time" to time since initialization
      while (time >= parameter(3))
	time -= parameter(3); // subtract cycle lengths

      changeTime = parameter(2) + parameter(4); // force-off time + offset
      while (changeTime >= parameter(3))
	changeTime -= parameter(3); // subtract cycle lengths

      if ( AproxEqual(time, changeTime) ) { // force-off time (+ offset) reached
	nextPeriod(parameter(1));
        status(CONDITION_TRUE);
        break;
      }
      else {
        status(CONDITION_FALSE);
        break;
      }
      

    case 103: // maximum time
      if ( signalGroup()->state() != parameter(0) )
        break;

      if ( theSimulationClock->currentTime() >=
	   signalGroup()->periodStartTime() + parameter(2) ) {
	nextPeriod(parameter(1));
	//	signalGroup()->nextPeriod(parameter(1));
        status(CONDITION_TRUE);
        break;
      }
      else {
        status(CONDITION_FALSE);
        break;
      }
      

//     case 104: // change with other group
//       if ( signalGroup()->state() != parameter(0) )
//         break;

//       // If sync group is not in sync period, break.
//       if ( controller()->signalGroup(parameter(2))->state() != parameter(3) ) {
//         status(CONDITION_FALSE);
//         break;
//       }

//       // Otherwise, change to next period.
//       else {
// 	nextPeriod(parameter(1));
// 	status(CONDITION_TRUE);
//         break;
//       }
      

    case 104: // follow other group
      if ( signalGroup()->state() != parameter(0) )
        break;

      // If [sync group] is in [sync period]...
      if ( controller()->signalGroup(parameter(2))->state() == parameter(3) &&
	   changeTime_ == -1 ) // ...and change time has not been set...
	// ...set change time to now + [delay].
        changeTime_ = theSimulationClock->currentTime() + parameter(4);

      // If time for change is now...
      if ( AproxEqual(theSimulationClock->currentTime(), changeTime_) ) {
	changeTime_ = -1; // ...reset change time...
	nextPeriod(parameter(1)); // ...change to [next period].
	status(CONDITION_TRUE);
	break;
      }	

      // Otherwise (not time for change)...
      else {
	status(CONDITION_FALSE);
	break;
      }	

      

    case 105: // force-off (if demand)
      if ( signalGroup()->state() != parameter(0) )
        break;

      if ( nDetectors_ == -1 ) { // detectors have not been read in
	nDetectors_ = nParameters_ - 5;
	detectors_.reserve(nDetectors_);
	for (i = 5; i < nParameters_; i ++) {
	  detectors_.push_back(findDetector(parameter(i)));
	}
      }

      time -= signalGroup()->initializationTime(); // set "time" to time since initialization
      while (time >= parameter(3))
	time -= parameter(3); // subtract cycle times

      changeTime = parameter(2) + parameter(4); // force-off time + offset
      while (changeTime >= parameter(3))
	changeTime -= parameter(3); // subtract cycle lengths

      if ( checkDetectors() && // detector occupied
	   AproxEqual(time, changeTime) ) { // force-off time (+ offset) reached
	nextPeriod(parameter(1));
        status(CONDITION_TRUE);
        break;
      }
      else {
        status(CONDITION_FALSE);
        break;
      }
      

    case 106: // force-off (probabilistic)
      if ( signalGroup()->state() != parameter(0) )
        break;

      time -= signalGroup()->initializationTime(); // set "time" to time since initialization
      while (time >= parameter(3))
	time -= parameter(3); // subtract cycle times

      changeTime = parameter(2) + parameter(4); // force-off time + offset
      while (changeTime >= parameter(3))
	changeTime -= parameter(3); // subtract cycle lengths

      if ( theRandomizers[Random::Misc]->brandom(parameter(5)) && // specified probability
	   AproxEqual(time, changeTime) ) { // force-off time (+ offset) reached
	nextPeriod(parameter(1));
        status(CONDITION_TRUE);
        break;
      }
      else {
        status(CONDITION_FALSE);
        break;
      }
      

//     case 304: // preceding conflicting groups completed (revised)
//       if ( signalGroup()->state() != parameter(0) )
//         break;

//       if ( theSimulationClock->currentTime() > changeTime_ ) { // not in progress

// 	for (i = 3; i < nParameters_; i ++) {
// 	  if ( controller()->signalGroup(parameter(i))->status(COMPLETED) == 0 ) {
// 	    status(-2); // conflicts did not just end
// 	    break;
// 	  }
// 	}
// 	if ( status() == -1 ) // conflicts just ended, add clearance time
// 	  changeTime_ = theSimulationClock->currentTime() + parameter(2);
	
//       }

//       if ( theSimulationClock->currentTime() == changeTime_ ) { // time for change
// 	status(CONDITION_TRUE);
// 	nextPeriod(parameter(1));
// 	for (i = 3; i < nParameters_; i ++) {
// 	  controller()->signalGroup(parameter(i))->unsetStatus(COMPLETED); // reset conflicts
// 	}
// 	break;
//       }

//       else { // not ready for change
// 	status(CONDITION_FALSE);
// 	break;
//       }
      
      
    case 201: // minimum time
      if ( signalGroup()->state() != parameter(0) )
        break;
      else if ( theSimulationClock->currentTime() < // within minimum time
                signalGroup()->periodStartTime() + parameter(1) ) {
        signalGroup()->unsetStatus(PASSIVE + GAP_OUT); 
	status(CONDITION_TRUE);
        break;  
      }
      else if ( AproxEqual(theSimulationClock->currentTime(), // at minimum time
			   signalGroup()->periodStartTime() + parameter(1)) ) {
        status(CONDITION_FALSE);
	signalGroup()->setStatus(PASSIVE); // can change if necessary
//         if ( signalGroup()->state() == SIGNAL_RED && // if red
// 	     signalGroup()->periodStartTime() != controller()->startTime() ) // and not initial period
//           signalGroup()->status(COMPLETED); // flag as completed
        break;  
      }
      else { // past minimum time
	signalGroup()->setStatus(PASSIVE); // can change if necessary
        status(CONDITION_FALSE);
        break;
      }


    case 202: // extension time (with max, using gap timer)
      if ( signalGroup()->state() != parameter(0) )
        break;
      
      if ( signalGroup()->status(GAP_OUT) ) { // group has already gapped-out
	status(CONDITION_FALSE); 
        break;
      }

      if ( theSimulationClock->currentTime() >=
	   signalGroup()->periodStartTime() + parameter(2) ) { // maximum time exceeded
        signalGroup()->setStatus(PASSIVE); 
	status(CONDITION_FALSE);
        break;
      }

      if ( signalGroup()->gapTimer() == -1 || // no actuations, or...
	   signalGroup()->gapTimer() >= parameter(1) ) { // ...allowable gap exceeded
	signalGroup()->setStatus(PASSIVE + GAP_OUT); 
	status(CONDITION_FALSE);
        break;
      }
      else { // within allowable gap
	signalGroup()->unsetStatus(PASSIVE); 
	status(CONDITION_TRUE);
	break;
      }
      
      
    case 203: // extension time (with max & gap reduction, using gap timer)
      if ( signalGroup()->state() != parameter(0) )
        break;
      
      if ( signalGroup()->status(GAP_OUT) ) { // group has already gapped-out
	status(CONDITION_FALSE); 
        break;
      }

      if ( theSimulationClock->currentTime() >=
	   signalGroup()->periodStartTime() + parameter(5) ) { // maximum time exceeded
        signalGroup()->setStatus(PASSIVE); 
	status(CONDITION_FALSE);
        break;
      }

      if ( signalGroup()->gapTimer() == -1 ) { // no actuations, or...
	signalGroup()->setStatus(PASSIVE + GAP_OUT); 
	status(CONDITION_FALSE);
        break;
      }

      // calculate gap:

      // if time in period <= time_before_reduction
      if (  theSimulationClock->currentTime() - signalGroup()->periodStartTime() <=
	    parameter(3) )
	gap = parameter(1); // (extension time)

      // if time in period >= time_before_reduction + time_to_reduce )
      else if ( theSimulationClock->currentTime() - signalGroup()->periodStartTime() >=
	   parameter(3) + parameter(4) )
	gap = parameter(2); // (min gap)

      else // (in gap reduction regime)
	gap = parameter(1) - ( (parameter(1) - parameter(2)) / parameter(4) ) * 
	  ( theSimulationClock->currentTime() - signalGroup()->periodStartTime() - parameter(3) );
	//                    ext_time - min_gap
	// gap = ext_time - ( ------------------ )*( time in period - time_before_reduction )
	//                      time_to_reduce
      
//       // debug
//       cout << "gap: " << gap << endl;

      if ( signalGroup()->gapTimer() >= gap ) { // ...allowable gap exceeded
	signalGroup()->setStatus(PASSIVE + GAP_OUT); 
	status(CONDITION_FALSE);
        break;
      }
      else { // within allowable gap
	signalGroup()->unsetStatus(PASSIVE); 
	status(CONDITION_TRUE);
	break;
      }
      
      
    case 204: // extension time (with max & stepwise gap reduction, using gap timer)
      if ( signalGroup()->state() != parameter(0) )
        break;
      
      if ( signalGroup()->status(GAP_OUT) ) { // group has already gapped-out
	status(CONDITION_FALSE); 
        break;
      }

      if ( theSimulationClock->currentTime() >=
	   signalGroup()->periodStartTime() + parameter(6) ) { // maximum time exceeded
        signalGroup()->setStatus(PASSIVE); 
	status(CONDITION_FALSE);
        break;
      }

      if ( signalGroup()->gapTimer() == -1 ) { // no actuations, or...
	signalGroup()->setStatus(PASSIVE + GAP_OUT); 
	status(CONDITION_FALSE);
        break;
      }

      // calculate gap:

      gap = parameter(2);

      while ( time >= signalGroup()->periodStartTime() + parameter(5) ) {
	gap -= parameter(4); // (reduce by step size)
	time -= parameter(5); // (subtract step time)
      }

      if ( gap > parameter(1) )
	gap = parameter(1); // initial extension time is upper limit on gap...

      if ( gap < parameter(3) )
	gap = parameter(3); // ...and minimum gap is lower limit
      
// //       // debug
// //       cout << "gap: " << gap << endl;

      if ( signalGroup()->gapTimer() >= gap ) { // ...allowable gap exceeded
	signalGroup()->setStatus(PASSIVE + GAP_OUT); 
	status(CONDITION_FALSE);
        break;
      }
      else { // within allowable gap
	signalGroup()->unsetStatus(PASSIVE); 
	status(CONDITION_TRUE);
	break;
      }
      
      
    case 205: // complementary groups still active
      if ( signalGroup()->state() != parameter(0) )
        break;
      
      for (i = 1; i < nParameters_; i ++) {
// 	if ( controller()->signalGroup(parameter(i))->status() != PASSIVE &&
// 	     controller()->signalGroup(parameter(i))->state() == parameter(0) ) {
// 	  // another group in same state (e.g. green) is still active
	if ( controller()->signalGroup(parameter(i))->status(PASSIVE) == 0 && // not ready, and
	     !controller()->signalGroup(parameter(i))->status(SKIPPED + COMPLETED) ) { // not skipped
	  status(CONDITION_TRUE);
	  break;
	}
      }
      if ( status() != CONDITION_TRUE )
	status(CONDITION_FALSE);
      break;

      
      
    case 206: // no conflicting calls
      if ( signalGroup()->state() != parameter(0) )
        break;
      
      if ( nDetectors_ == -1 ) { // detectors have not been read in
	nDetectors_ = nParameters_ - 1;
	detectors_.reserve(nDetectors_);
	for (i = 1; i < nParameters_; i ++)
	  detectors_.push_back(findDetector(parameter(i)));
      }
      
      if ( checkDetectors() ) { // dectector occupied
	signalGroup()->setStatus(PASSIVE); // can change if necessary
	status(CONDITION_FALSE); // don't hold
      }
      else // no detectors occupied
	status(CONDITION_TRUE); // hold
      break;	  
      
      
    case 207: // preceding conflicting groups not completed (hold)
      if ( signalGroup()->state() != parameter(0) )
        break;

      // read in conflicting groups
      if ( signalGroup()->nPrecedingGroups() == 0 ) {
	signalGroup()->nPrecedingGroups(nParameters_ - 2);
	signalGroup()->createPrecedingGroups();
	for (i = 0; i < signalGroup()->nPrecedingGroups(); i ++)
	  signalGroup()->precedingGroups(i, parameter(i+2));
      }
      //
      if ( theSimulationClock->currentTime() > changeTime_ ) { // not being held
	//
	if ( AproxEqual(signalGroup()->periodStartTime(),
			theSimulationClock->currentTime()) ) { 
	  status(CONDITION_TRUE); // hold (don't change back immediately)
	  break;
	}
	//
	for (i = 2; i < nParameters_; i ++) {
	  if ( controller()->signalGroup(parameter(i))->status(COMPLETED) == 0 ) {
// 	  if ( controller()->signalGroup(parameter(i))->status(COMPLETED) == 0 &&
// 	       ( controller()->signalGroup(parameter(i))->status(SKIPPED) == 0 ||
// 		  controller()->signalGroup(parameter(i))->periodStartTime() != 
// 		  theSimulationClock->currentTime() ) ) {
	    status(-2); // conflicts did not just end
	    break;
	  }
	}
	
	if ( status() == -1 ) { // conflicts just ended
	  
	  changeTime_ = theSimulationClock->currentTime(); // time for change is now...
	  
	  for (i = 2; i < nParameters_; i ++) {
	    if ( controller()->signalGroup(parameter(i))->status(SKIPPED) == 0 ) { 
	      changeTime_ = theSimulationClock->currentTime() + parameter(1); 
	      // ... plus clearance time (unless all conflicting groups were skipped)
	      break; 
	    }
	  }

	}

      }

      if ( AproxEqual(theSimulationClock->currentTime(), changeTime_) ) { // time for change
// 	nextPeriod(parameter(1));
// 	signalGroup()->nextPeriod(parameter(1)); // sets next period for signal group
// 	for (i = 2; i < nParameters_; i ++) {
// 	  controller()->signalGroup(parameter(i))->unsetStatus(COMPLETED); // reset conflicts
// 	}
	changeTime_ = -1; // reset
	status(CONDITION_FALSE);
	break;
      }

      else { // not ready for change
	status(CONDITION_TRUE);
	break;
      }
      
      
    case 208: // hold indefinitely
      if ( signalGroup()->state() != parameter(0) )
        break;
      else {
        signalGroup()->unsetStatus(PASSIVE); 
	status(CONDITION_TRUE);
        break;  
      }


    case 209: // preceding group not yet red
      if ( signalGroup()->state() != parameter(0) )
        break;

      // If not being held:
      if ( theSimulationClock->currentTime() > changeTime_ ) {

	// Hold if this group just changed (to red).
	if ( AproxEqual(signalGroup()->periodStartTime(),
			theSimulationClock->currentTime()) ) { 
	  status(CONDITION_TRUE);
	  break;
	}

	// If preceding group just changed...
	if ( AproxEqual(controller()->signalGroup(parameter(2))->periodStartTime(), 
			theSimulationClock->currentTime()) &&
	     // ...to red...
	     controller()->signalGroup(parameter(2))->state() == SIGNAL_RED &&
	     // ...but not at time of initialization...
	     ( controller()->signalGroup(parameter(2))->periodStartTime() != 
	       controller()->signalGroup(parameter(2))->initializationTime() ||
	       // ...unless it was skipped:
	       controller()->signalGroup(parameter(2))->status(SKIPPED) ) ) {
	  
	  // Time for change is now...
	  changeTime_ = theSimulationClock->currentTime();
	  // ... plus clearance time (unless preceding group was skipped)
	  if ( controller()->signalGroup(parameter(2))->status(SKIPPED) == 0 )
	    changeTime_ = theSimulationClock->currentTime() + parameter(1);
	}
      }

      // If time for change:
      if ( AproxEqual(theSimulationClock->currentTime(), changeTime_) ) {
	changeTime_ = -1; // reset
	status(CONDITION_FALSE);
	break;
      }

      else { // not ready for change
	status(CONDITION_TRUE);
	break;
      }
      
      
    case 210: // hold in window
      if ( signalGroup()->state() != parameter(0) )
        break;

      time -= signalGroup()->initializationTime(); // Set "time" to time since initialization...
      time -= parameter(4); // ...minus offset...
      while (time >= parameter(3))
	time -= parameter(3); // ...then subtract cycle lengths to get time in cycle.

      // If time in cycle is within window...
      if ( ( parameter(1) < parameter(2) && // (case where start and end are in same cycle)
	     time >= parameter(1) &&
	     time <= parameter(2) ) ||
	   ( parameter(1) > parameter(2) && // (case where end time is in next cycle)
	     ( time >= parameter(1) ||
	       time <= parameter(2) ) ) ) {
	//...hold current period.
	signalGroup()->unsetStatus(PASSIVE); 
	status(CONDITION_TRUE);
	break;  
      }
      else {
	signalGroup()->setStatus(PASSIVE);
	status(CONDITION_FALSE);
	break;
      }
      

    case 211: // hold if bus priority extension
      if ( signalGroup()->state() != parameter(0) )
        break;

      if ( signalGroup()->status(BUS_EXTENSION) ) {
	signalGroup()->unsetStatus(PASSIVE); 
	status(CONDITION_TRUE);
	break;  
      }
      else {
	signalGroup()->setStatus(PASSIVE);
	status(CONDITION_FALSE);
	break;
      }


    case 212: // hold if bus priority insertion
      if ( signalGroup()->state() != parameter(0) )
        break;

      if ( signalGroup()->status(BUS_INSERTION) ) {
	signalGroup()->unsetStatus(PASSIVE); 
	status(CONDITION_TRUE);
	break;  
      }
      else {
	signalGroup()->setStatus(PASSIVE);
	status(CONDITION_FALSE);
	break;
      }


//     case 106: // minimum time (skippable)
//       if ( signalGroup()->state() != parameter(0) )
//         break;
      
//       if ( nDetectors_ == -1 ) { // detectors have not been read in
// 	nDetectors_ = nParameters_ - 2;
// 	detectors_.reserve(nDetectors_);
// 	for (i = 2; i < nParameters_; i ++) {
// 	  detectors_.push_back(findDetector(parameter(i)));
// 	}
//       }
//       if ( !checkDetectors() && // no demand
// 	   ( theSimulationClock->currentTime() == signalGroup()->periodStartTime() ) ) { // just starting
// 	nextPeriod(SIGNAL_RED);
// 	signalGroup()->nextPeriod(SIGNAL_RED);
// 	signalGroup()->setStatus(SKIPPED);
// 	status(CONDITION_FALSE);
// 	break;
//       }
      
//       if ( theSimulationClock->currentTime() < // within minimum time
//                 signalGroup()->periodStartTime() + parameter(1) ) {
// 	signalGroup()->unsetStatus(PASSIVE);
//         status(CONDITION_TRUE);
//         break;  
//       }
//       else if ( theSimulationClock->currentTime() == // at minimum time
//                 signalGroup()->periodStartTime() + parameter(1) ) {
// 	signalGroup()->setStatus(PASSIVE); // can change if necessary
//         status(CONDITION_FALSE);
//         break;  
//       }
//       else { // past minimum time
// 	signalGroup()->setStatus(PASSIVE); // can change if necessary
//         status(CONDITION_FALSE);
//         break;
//       }


//     case 102: // extension time
//       if ( signalGroup()->state() != parameter(0) )
//         break;
      
//       if ( signalGroup()->status(GAP_OUT) ) { // group has already gapped-out
// 	status(CONDITION_FALSE); 
// 	break;
//       }

//       if ( nDetectors_ == -1 ) { // detectors have not been read in
// 	nDetectors_ = nParameters_ - 2;
// 	detectors_.reserve(nDetectors_);
// 	for (i = 2; i < nParameters_; i ++) {
// 	  detectors_.push_back(findDetector(parameter(i)));
// 	}
//       }
      
//       if ( theSimulationClock->currentTime() == changeTime_ ) { // extension timer just expired
// 	signalGroup()->setStatus(PASSIVE + GAP_OUT);
// 	status(CONDITION_FALSE);
// 	break;
//       }
      
//       if ( theSimulationClock->currentTime() > changeTime_ ) { // extension inactive
	
// 	if ( checkDetectors() ) { // dectector occupied
// 	    changeTime_ = theSimulationClock->currentTime() + parameter(1); // extend
// 	    signalGroup()->unsetStatus(PASSIVE); 
// 	    status(CONDITION_TRUE);
// 	}
// 	else { // no detectors occupied
// 	  signalGroup()->setStatus(PASSIVE + GAP_OUT); // can change if necessary
// 	  status(CONDITION_FALSE); 
// 	}
// 	break;	  
 
//       } 
//       else { // extension already active
	
// 	if ( checkDetectors() ) // dectector occupied
// 	  changeTime_ = theSimulationClock->currentTime() + parameter(1); // re-extend

// 	signalGroup()->unsetStatus(PASSIVE); 
// 	status(CONDITION_TRUE);
// 	break;	  
	
//       }
      
      
//     case 105: // extension time (with max)
//       if ( signalGroup()->state() != parameter(0) )
//         break;
      
//       if ( signalGroup()->status(GAP_OUT) ) { // group has already gapped-out
// 	status(CONDITION_FALSE); 
//         break;
//       }

//       if ( theSimulationClock->currentTime() >=
// 	   signalGroup()->periodStartTime() + parameter(2) ) {
//         signalGroup()->setStatus(PASSIVE); 
// 	status(CONDITION_FALSE); // maximum time exceeded
//         break;
//       }

//       if ( nDetectors_ == -1 ) { // detectors have not been read in
// 	nDetectors_ = nParameters_ - 3;
// 	detectors_.reserve(nDetectors_);
	
// 	for (i = 3; i < nParameters_; i ++) {
// 	  detectors_.push_back(findDetector(parameter(i)));
// 	}
//       }
      
//       if ( theSimulationClock->currentTime() == changeTime_ ) { // extension timer just expired
// 	signalGroup()->setStatus(PASSIVE + GAP_OUT);
// 	status(CONDITION_FALSE);
// 	break;
//       }
      
//       if ( theSimulationClock->currentTime() > changeTime_ ) { // extension inactive
	
// 	if ( checkDetectors() ) { // dectector occupied
// 	  changeTime_ = theSimulationClock->currentTime() + parameter(1); // extend
// 	  signalGroup()->unsetStatus(PASSIVE); 
// 	  status(CONDITION_TRUE);
// 	}
// 	else { // no detectors occupied
// 	  signalGroup()->setStatus(PASSIVE + GAP_OUT); // can change if necessary
// 	  status(CONDITION_FALSE); 
// 	}
// 	break;	  
 
//       } 
//       else { // extension already active
	
// 	if ( checkDetectors() ) // dectector occupied
// 	  changeTime_ = theSimulationClock->currentTime() + parameter(1); // re-extend

// 	signalGroup()->unsetStatus(PASSIVE); 
// 	status(CONDITION_TRUE);
// 	break;	  
	
//       }
      
      
    case 301: // skip next condition if any detector occupied
      if ( nDetectors_ == -1 ) { // detectors have not been read in
	nDetectors_ = nParameters_;
	detectors_.reserve(nDetectors_);
	for (i = 0; i < nParameters_; i ++) {
	  detectors_.push_back(findDetector(parameter(i)));
	}
      }
      if ( checkDetectors() ) // detector occupied
	numToSkip_ = 1;
	status(CONDITION_SKIP_NEXT);
      break;


    case 302: // skip next condition if no detectors occupied
      if ( nDetectors_ == -1 ) { // detectors have not been read in
	nDetectors_ = nParameters_;
	detectors_.reserve(nDetectors_);
	for (i = 0; i < nParameters_; i ++) {
	  detectors_.push_back(findDetector(parameter(i)));
	}
      }
      if ( !checkDetectors() ) // no detector occupied
	numToSkip_ = 1;
	status(CONDITION_SKIP_NEXT);
      break;


    case 303: // skip next [num] conditions if group status flag is set/not set
      if ( ( parameter(3) && // "on"
	     controller()->signalGroup(parameter(1))->status(parameter(2)) ) ||
	   ( !parameter(3) && // "off"
	     !controller()->signalGroup(parameter(1))->status(parameter(2)) ) ) {
	numToSkip(parameter(0));
	status(CONDITION_SKIP_NEXT);
      }
      break;


//     case 303: // skip next condition if group flag = skipped
//       if ( controller()->signalGroup(parameter(0))->status(SKIPPED) )
// 	status(CONDITION_SKIP_NEXT);
//       break;


//     case 304: // skip next condition if group flag != skipped
//       if ( !controller()->signalGroup(parameter(0))->status(SKIPPED) )
// 	status(CONDITION_SKIP_NEXT);
//       break;


//     case 305: // skip next condition if group flag = bus extension
//       if ( controller()->signalGroup(parameter(0)->status(BUS_EXTENSION) )
// 	status(CONDITION_SKIP_NEXT);
//       break;


//     case 306: // skip next condition if group flag != bus extension
//       if ( !controller()->signalGroup(parameter(0)->status(BUS_EXTENSION) )
// 	status(CONDITION_SKIP_NEXT);
//       break;


    default: 
      cerr << "Error:: Unknown ConditionCode <" << conditionCode() << ">. ";
      
    }
  
}   


// // Return pointer to parent Traffic Controller.

// TC_Controller*
// TC_Condition::controller()
// {
//   return signalGroup()->controller();
// }


// Return pointer to parent Generic Traffic Controller.

TC_GenericController*
TC_Condition::controller()
{
  return signalGroup()->controller();
}


// Find detector, and return pointer.

RN_Sensor*
TC_Condition::findDetector(short int id)
{
  RN_Sensor * s = theNetwork->findSensor(id);
  if ( s == NULL ) {
    cerr << "Error:: Unknown sensor <" << id << ">. ";
    theException->exit(1);
  }
  return s;
}


// Check state of detectors.  If any occupied, return 1.
// Otherwise return 0.

short int
TC_Condition::checkDetectors()
{
  short int i;
  for (i = 0; i < nDetectors_; i ++) {
    if ( detectors_[i]->measurement() )
      return 1;
  }
  return 0;
  
}
