//-*-c++-*------------------------------------------------------------
// NAME: Simulation Widges
// AUTH: Qi Yang
// FILE: TC_ActivatedController.h
// DATE: Fri Oct 27 22:37:47 1995
//--------------------------------------------------------------------

#ifndef TC_ACTIVATEDCONTROLLER_HEADER
#define TC_ACTIVATEDCONTROLLER_HEADER

#include <new>
#include <iostream>
#include <vector>

#include "TC_Controller.h"

#include <GRN/Constants.h>

class TC_ActivatedPhase;
class RN_Sensor;
class RN_Signal;
class RoadNetwork;

const int DO_NOT_CARE	= 0;
const int MUST_BE_IDLE	= 1;
const int ACTIVATED	= 2;
const int PREEMPTIVE	= 3;


class TC_ActivatedController : public TC_Controller
{
   protected:
      
      std::vector <RN_Sensor*> detectors_;

      int nDetectors_;

      short int nCalledPhases_;
      short int * calledPhases_;
      char * calledSpec_;	// specification

      short int nExtendedPhases_;
      short int * extendedPhases_;
      char * extendedSpec_;	// specification

      // The three variables below keeps track of the phase to be
      // called when red for a particular movement reaches max RedTime

      // Size of these two arrays equals to number of approaches

      short int * maxRedTime_;	// maximum allowable red time
      short int * redCalledPhaseIndex_;

      // Size of this array equals total number of turning movement

      short int * redTime_;	// current accumulated red time

      // Max red time related variabled ended

   private:

      short int delta_;		// length of the last time interval

   public:
	
      TC_ActivatedController(int c, short int t)
		 : TC_Controller(c, t),
		   calledSpec_(NULL), extendedSpec_(NULL),
		   maxRedTime_(NULL), redCalledPhaseIndex_(NULL), redTime_(NULL) { }

      virtual ~TC_ActivatedController() {
		 delete [] calledPhases_;
		 delete [] calledSpec_;
		 delete [] extendedPhases_;
		 delete [] extendedSpec_;
		 delete [] maxRedTime_;
		 delete [] redCalledPhaseIndex_;
		 delete [] redTime_;
      }
      
      virtual TC_Phase* newPhase();
      
      // Returns specification of ith detector for jth called/extended
      // phase

      virtual short int calledPhaseSpec(short int i, short int j);
      virtual short int extendedPhaseSpec(short int i, short int j);

      virtual short int nDetectors() {
		 return detectors_.size();
      }
      virtual short int nCalledPhases() {
		 return nCalledPhases_;
      }
      virtual short int nExtendedPhases() {
		 return nExtendedPhases_;
      }

      virtual long initializeSignalStates();

      virtual long switchTo(short int phase, short int interval = 0);
      virtual long switchInterval();
      virtual short int stopRedPhase(short int rcp);
      virtual short int callPhase(short int rcp);
      virtual short int extendCurrentPhase(short int rcp);
      virtual long switchToNextInterval();

      virtual int checkDetectors(const char *spec);
      virtual void clearMaxRedTimes();
      virtual short int updateMaxRedTimes();

      virtual void read(Reader &);
      virtual void readSignals(Reader& is);
      virtual RN_Signal* readSignal(Reader& is, short int i);
      virtual RN_Sensor* readDetector(Reader& is, short int i);

      virtual void readCalledPhaseTable(Reader &is);
      virtual void readExtendedPhaseTable(Reader& is);
      virtual void readDetectorTable(Reader& is);
      virtual void readCalledPhaseSpec(Reader& is, short int i);
      virtual void readExtendedPhaseSpec(Reader& is, short int i);

      virtual void printCurrentState(std::ostream &os = std::cout);

      virtual void print(std::ostream &os = std::cout);
      virtual void printSignals(std::ostream &os = std::cout);
      virtual void printCalledPhaseTable(std::ostream &os);
      virtual void printExtendedPhaseTable(std::ostream &os);
      virtual void printDetectorTable(std::ostream &os);

   private:

      virtual void printSignal(std::ostream &, short int);
};

#endif
