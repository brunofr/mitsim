//-*-c++-*------------------------------------------------------------
// TC_Sensor.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef TC_SENSOR_HEADER
#define TC_SENSOR_HEADER

#include <IO/IOService.h>
#include <iosfwd>

class RN_Vehicle;

// TC_Sensor inherits DRN_Sensor in graphical mode and RN_Sensor in
// batch mode

#ifndef INTERNAL_GUI
#include <GRN/RN_Sensor.h>
#define DRN_Sensor RN_Sensor
#else
#include <DRN/DRN_Sensor.h>
#endif

class TC_Sensor : public DRN_Sensor
{
   public:

      TC_Sensor() : DRN_Sensor() { }

      virtual ~TC_Sensor() { }

      virtual void send(IOService &);
      virtual void receive(IOService &);
	  virtual void send_i(IOService &, RN_Vehicle *);
      virtual void receive_i(IOService &);
      virtual void send_s(IOService &, RN_Vehicle *);
      virtual void receive_s(IOService &);

	  virtual void write(std::ostream &os);
	  virtual void write_i(std::ostream &os, RN_Vehicle *);
	  virtual void write_s(std::ostream &os, RN_Vehicle *);
};

#endif
