//-*-c++-*------------------------------------------------------------
// TC_Sensor.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <fstream>
using namespace std;

#include <Tools/ToolKit.h>
#include <Tools/SimulationEngine.h>
#include <Tools/SimulationClock.h>
#include <Tools/Math.h>

#include <IO/IOService.h>

#include <GRN/Constants.h>
#include <GRN/Parameter.h>
#include <GRN/RN_Vehicle.h>
#include <GRN/RN_Node.h>
#include <GRN/RN_Lane.h>
#include <GRN/RN_Path.h>

#include "TC_Sensor.h"


// Aggregate traffic sensor data

void
TC_Sensor::receive(IOService &is)
{
   if (atask() & SENSOR_ACOUNT) is >> count_;
   if (atask() & SENSOR_ASPEED) is >> speed_;
   if (atask() & SENSOR_AOCCUPANCY) is >> occupancy_;
   if (flow()) setState(SENSOR_ACTIVATED);
}

void
TC_Sensor::send(IOService &os)
{
   unsigned int opt = atask();
   //IEM(Jun18) Used to short-circuit if now cars have passed, assuming that
   //           all sensors had been zeroed by reset as part of the send process
   //           I took out that reset, so I'll let it send zeros
   //if (!opt || count_ <= 0 && occupancy_ < 1.0E-5) return;
   if (!opt) return;   

   os << (index_ | MSG_TYPE_SENSOR_A);

   if (opt & SENSOR_ACOUNT) {
	  os << count();
   }
   if (opt & SENSOR_ASPEED) {
	  os << speed();
   }
   if (opt & SENSOR_AOCCUPANCY) {
	  os << occupancy();
   }

   os.flush(SEND_BY_PACKAGE);
}

// Write traffic sensor reading into a file.

void TC_Sensor::write(ostream &os)
{
   unsigned int opt = atask();
   //--- commented by YW 10/28/2005 ---
   // if (!opt || count_ <= 0) return;
   //--- changed to (output even if 0 counts)
   if (!opt || count_ < 0) return;
   //----------------------------------------

   if (theSimulationEngine->isRectangleFormat()) {
      os << Round(theSimulationClock->currentTime()) << endc;
   } else {
      os << indent;
   }

   os << code_ << endc
	  << hex << SENSOR_ACODE(opt) << dec;
   double x;

   if (opt & SENSOR_ACOUNT) {
	  x = count();
	  os << endc << Fix(x, 1.0);
   }

   if (opt & SENSOR_ASPEED) {
	  x = speed() / theBaseParameter->speedFactor();
	  os << endc << Fix(x, 0.1);
   }

   if (opt & SENSOR_AOCCUPANCY) {
	  x = occupancy();
	  os << endc << Fix(x, 0.01);
   }

   os << endl;
}


// Information on probe vehicles

void
TC_Sensor::receive_i(IOService&)
{
   cerr << "Sensor <" << code_ << "> rcved probe info." << endl;
}

void TC_Sensor::send_i(IOService& os, RN_Vehicle *pv)
{
   unsigned int opt = itask();

   if (!opt) return;

   os << (index_ | MSG_TYPE_SENSOR_I);

   if (opt & SENSOR_IID) {
	  os << pv->code();
   }
   if (opt & SENSOR_ITYPE) {
	  os << pv->types();
   }
   if (opt & SENSOR_IDEP) {
	  os << pv->departTime();
   }
   if (opt & SENSOR_IORI) {
	  os << pv->oriNode()->index();
   }
   if (opt & SENSOR_IDES) {
	  os << pv->desNode()->index();
   }
   if (opt & SENSOR_IPATH) {
	  if (pv->path()) {
		 os << pv->path()->index();
	  } else {
		 os << NO_EXPLICIT_PATH;
	  }
   }
   if (opt & SENSOR_IHEIGHT) {
	  if (pv->isType(VEHICLE_LOW))
		 os << OVER_HEIGHT_FALSE;
	  else
		 os << OVER_HEIGHT_TRUE;
   }
   if (opt & SENSOR_ISPEED) {
	  os << pv->currentSpeed();
   }

   os.flush(SEND_IMMEDIATELY);
}

void TC_Sensor::write_i(ostream &os, RN_Vehicle *pv)
{
   unsigned int opt = itask();

   if (!opt) return;

   os << Round(theSimulationClock->currentTime())
	  << endc << code_ << endc
	  << hex << SENSOR_ICODE(opt) << dec;

   if (opt & SENSOR_IID) {
	  os << endc << pv->code();
   }
   if (opt & SENSOR_ITYPE) {
	  os << endc << pv->types();
   }
   if (opt & SENSOR_IDEP) {
	  os << endc << pv->departTime();
   }
   if (opt & SENSOR_IORI) {
	  os << endc << pv->oriNode()->code();
   }
   if (opt & SENSOR_IDES) {
	  os << endc << pv->desNode()->code();
   }
   if (opt & SENSOR_IPATH) {
	  if (pv->path()) {
		 os << endc << pv->path()->code();
	  } else {
		 os << endc << NO_EXPLICIT_PATH;
	  }
   }
   if (opt & SENSOR_IHEIGHT) {
	  if (pv->isType(VEHICLE_LOW))
		 os << endc << OVER_HEIGHT_FALSE;
	  else
		 os << endc << OVER_HEIGHT_TRUE;
   }
   if (opt & SENSOR_ISPEED) {
	  os << endc << pv->currentSpeed();
   }

   os << endl;
}


// Snapshot of a set of vehicles

void
TC_Sensor::receive_s(IOService&)
{
   cerr << "Sensor <" << code_ << "> rcved snapshot info." << endl;
}

void TC_Sensor::send_s(IOService& os, RN_Vehicle *pv)
{
   unsigned int opt = stask();
   
   if (!opt) return ;

   os << (index_ | MSG_TYPE_SENSOR_S);

   if (opt & SENSOR_SID) {
	  os << pv->code();
   }
   if (opt & SENSOR_STYPE) {
	  os << pv->types();
   }
   if (opt & SENSOR_SHEIGHT) {
	  if (pv->isType(VEHICLE_LOW))
		 os << OVER_HEIGHT_FALSE;
	  else
		 os << OVER_HEIGHT_TRUE;
   }
   if (opt & SENSOR_SSPEED) {
	  os << pv->currentSpeed();
   }
   if (opt & SENSOR_SLANE) {
	  if (pv->lane())
		 os << pv->lane()->index();
	  else
		 os << NO_EXPLICIT_LANE;
   }
   if (opt & SENSOR_SPOSITION) {
	  os << pv->distance();
   }

   os.flush(SEND_BY_PACKAGE);
}


void TC_Sensor::write_s(ostream &os, RN_Vehicle *pv)
{
   unsigned int opt = stask();
   
   if (!opt) return ;

   os << code_ << endc
	  << hex << SENSOR_SCODE(opt) << dec;

   if (opt & SENSOR_SID) {
	  os << endc << pv->code();
   }
   if (opt & SENSOR_STYPE) {
	  os << endc << pv->types();
   }
   if (opt & SENSOR_SHEIGHT) {
	  if (pv->isType(VEHICLE_LOW))
		 os << endc << OVER_HEIGHT_FALSE;
	  else
		 os << endc << OVER_HEIGHT_TRUE;
   }
   if (opt & SENSOR_SSPEED) {
	  os << endc << pv->currentSpeed();
   }
   if (opt & SENSOR_SLANE) {
	  if (pv->lane())
		 os << endc << pv->lane()->code();
	  else
		 os << endc << NO_EXPLICIT_LANE;
   }
   if (opt & SENSOR_SPOSITION) {
	  os << endc << pv->distance();
   }

   os << endl;
}
