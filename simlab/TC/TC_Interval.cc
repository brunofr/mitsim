//-*-c++-*------------------------------------------------------------
// NAME: Widgets for Traffic Signal Control
// AUTH: Qi Yang
// FILE: TC_Interval.C
// DATE: Fri Oct 27 09:52:57 1995
//--------------------------------------------------------------------

#include "TC_Controller.h"
#include "TC_Phase.h"
#include "TC_Interval.h"
#include "TC_Signal.h"

#include <GRN/Constants.h>
#include <Tools/ToolKit.h>

#include <iomanip>
using namespace std;


// Returns pointer to the traffic controller

TC_Controller*
TC_Interval::controller() 
{
   return phase_->controller();
}


// Print length and state of the interval

void
TC_Interval::print(ostream &os)
{
   register int i, num = controller()->nEgresses();
   os << indent << indent << indent << indent;
   os << length_ << endc << OPEN_TOKEN << endc;
   for (i = 0; i < num; i ++) {
      os << hextag << hex << states(i) << endc;
   }
   os << CLOSE_TOKEN << endl;
}


//--------------------------------------------------------------------
// Requires: 0 <= i < nApproaches and 0 <= j < nEgresses
// Modifies: None
// Effects : Returns the signal state for ith approach and jth egress.
// The returned state may have following values:
//     0 -- Blank
//     1 -- Red
//     2 -- Yellow
//     3 -- Green
//   plus additional indicators
//     4 -- Arrow mask
//     8 -- Flashing mask
//--------------------------------------------------------------------

unsigned int
TC_Interval::state(short int i, short int j)
{
   return ((states(i) >> (j * SIGNAL_BITS)) & SIGNAL_STATE);
}


// Update the signal state in the network

void
TC_Interval::setSignalStates()
{
   TC_Controller *tc = controller();
   short int i, ns = tc->nApproaches();
   for (i = 0; i < ns; i ++) {
      tc->getSignal(i)->state(states(i));
   }
}
