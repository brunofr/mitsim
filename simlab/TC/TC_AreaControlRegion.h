//-*-c++-*------------------------------------------------------------
// NAME: Regional System Manager for Area Coordinated Traffic Signal Controllers
// AUTH: Owen J. Chen  
// FILE: TC_AreaControlRegion.h
// DATE: Mon Nov 11 12:18:52 EST 1996
//--------------------------------------------------------------------

#ifdef REGIONAL_CONTROL			// need MATLAB support

#ifndef TC_AREACONTROLREGION_HEADER
#define TC_AREACONTROLREGION_HEADER

#ifndef bool
#define bool int
#endif

#include <engine.h>

#include <new.h>
#include <iostream.h>

#include <Tools/Math.h>
#include <Templates/Listp.h>
#include <Tools/Reader.h>

#include <TC_ResponsiveController.h>
#include <TC_ControlRegion.h>

class RN_Signal;
class TC_Phase;
class TC_Interval;
class RoadNetwork;


//--------------------------------------------------------------------
// CLASS NAME: TC_AreaControlRegion
// AreaControlRegion manages the following classes of controllers: 
//        AreaResponsiveController, 
//        BilevelResponsiveController, 
//--------------------------------------------------------------------


class TC_AreaControlRegion : public TC_ControlRegion
{
   protected:

      double *meteringRates_;

      short int scenario_;       //scenario number for matlab demand file
      Engine * pMatlab_;         //Matlab Engine pointer
      int timeHorizon_;                 //time rolling horizon 
      int AreaUpdateStepSize_;   //Step Size for calling Matlab Calculation
      double AreaUpdateTime_ ;      //Time to call Matlab Calculation
      char* matlabInputBuffer_;
      char* matlabOutputBuffer_;
      mxArray* matlabMeteringRates_;  //Matrix is defined by Matlab library

    public:
	
      TC_AreaControlRegion(int c, short int t);

      ~TC_AreaControlRegion();

      virtual void loadControllers(Reader &);
      virtual void controllersCalculateParameters();
      void calculateRegionParameters(); 
};

#endif

#endif
