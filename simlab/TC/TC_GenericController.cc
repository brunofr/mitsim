//-*-c++-*------------------------------------------------------------
// NAME: Generic Traffic Controller
// AUTH: Angus Davol
// FILE: TC_GenericController.C
// DATE: Feb 2001
//--------------------------------------------------------------------

#include <cstdlib>

#include <Templates/Listp.h>

#include <Tools/SimulationClock.h>
#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <IO/Exception.h>

#include <GRN/RoadNetwork.h>

#include "TC_Interval.h"
#include "TC_Phase.h"
#include "TC_Signal.h"
#include "TC_GenericController.h"
#include "TC_SignalGroup.h"
#include "TC_Condition.h"
#include "TC_LogFile.h"
#include <iostream>
using namespace std;

void
TC_GenericController::read(Reader &is)
{
  is >> signalType_ >> nEgresses_; // >> outputFlag_;
  
  startTime_ = (long)theSimulationClock->currentTime();
  
  if (!is.findToken(OPEN_TOKEN)) theException->exit(1);
  
  readSignals(is);
  readSignalGroups(is);
  
  if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
  
  switchTime_ = initializeSignalStates();
  updateTime_ = ONE_DAY;
}


void
TC_GenericController::readSignals(Reader& is)
{
  short int i;

  is >> nApproaches_;  // i.e. number of signals

  groups_ = new short int[nApproaches_*nEgresses_];
  
  if (!is.findToken(OPEN_TOKEN)) theException->exit(1);
  
  for (i = 0; i < nApproaches_; i ++) {
    signals_.push_back(readSignal(is, i));
  }
  
  if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
  
}


RN_Signal*
TC_GenericController::readSignal(Reader& is, short int i)
{
  RN_Signal *s;
  int id;
  short int j, m;

  is >> id;
  
  if (!is.findToken(OPEN_TOKEN)) theException->exit(1);
  
  for (j = 0; j < nEgresses_; j ++) {
    m = i*nEgresses_ + j;
    is >> groups_[m];
  }
  
  if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
  
  if ( (s = theNetwork->findSignal(id)) == NULL ) {
    cerr << "Error:: Unknown signal <" << id << ">. ";
    is.reference();
    theException->exit(1);
  }
  
  return s;
  
}


void
TC_GenericController::readSignalGroups(Reader& is)
{
  short int num, g, id;

  is >> num;

  signalGroups_.reserve(num);
  
  if (!is.findToken(OPEN_TOKEN)) theException->exit(1);
  
  for (g = 0; g < num; g ++) {
    signalGroups_.push_back(newSignalGroup());
    signalGroups_[g]->controller(this);
    
    is >> id;
    signalGroups_[g]->code(id);
    
    signalGroups_[g]->read(is);
  }
  
  if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
  
}


long
TC_GenericController::initializeSignalStates()
{

  short int i, j, m, g;
  unsigned int indicators;

  // Create dummy phase:

  phases_.reserve(1);
  phases_.push_back(newPhase());
  phase(0)->controller(this);
  phase(0)->code(0);

  // Create dummy intervals (uses existing function in TC_Phase:
  // three intervals are created, but only one is needed)

  phase(0)->createDefaultIntervals();

  // Create initial interval

  for (i = 0; i < nApproaches_; i ++) { // signals

    indicators = 0; // interval notation: 000
    
    for (j = 0; j < nEgresses_; j ++) { // movements

      m = i*nEgresses_ + j; // movement ID

      for (g = 0; g < nGroups(); g ++) {

        if (groups_[m] == signalGroups_[g]->code()) {

	  // fix for red/yellow indication
	  int initialState = signalGroups_[g]->initialPeriod() & SIGNAL_STATE;

//           indicators += ( signalGroups_[g]->initialPeriod() << 
// 			  ((nEgresses_ - 1 - j) * SIGNAL_BITS) );
          indicators += ( initialState << 
			  ((nEgresses_ - 1 - j) * SIGNAL_BITS) );
	  
	}

        signalGroups_[g]->state(signalGroups_[g]->initialPeriod());
	
	if ( signalGroups_[g]->state() & SIGNAL_RED )
	  signalGroups_[g]->unsetStatus(COMPLETED);
	// removes COMPLETED flag for signals starting in Red

      }
      // fix: should have error check here (error if group not found)
    }

    phase(0)->interval(0)->setStates(i, indicators);
    
  }

  phase(0)->interval(0)->setSignalStates();

  for (g = 0; g < nGroups(); g ++) {
    signalGroups_[g]->initializationTime( theSimulationClock->currentTime() );

    // Write states to signal output file
    if (outputFlag_) {
//       TC_SignalGroup *sg = signalGroups_[g];
//       writeOutput(sg);
      writeOutput(g);
    }
  }

  return (long) theSimulationClock->currentTime();
  
}


void
TC_GenericController::resetSignalStates()
{

  short int i, j, m, g;
  unsigned int indicators;

  // Re-create interval

  for (i = 0; i < nApproaches_; i ++) { // signals

    indicators = 0; // interval notation: 000
    
    for (j = 0; j < nEgresses_; j ++) { // movements

      m = i*nEgresses_ + j; // movement ID

      for (g = 0; g < nGroups(); g ++) {
        if (groups_[m] == signalGroups_[g]->code()) {

	  // fix for red/yellow indication
	  int resetState = signalGroups_[g]->state() & SIGNAL_STATE;

//           indicators += ( signalGroups_[g]->state() << 
// 			  ((nEgresses_ - 1 - j) * SIGNAL_BITS) );
          indicators += ( resetState << 
			  ((nEgresses_ - 1 - j) * SIGNAL_BITS) );
	}
      }
      // fix: should have error check here (error if group not found)
    }

    phase(0)->interval(0)->setStates(i, indicators);
    
  }

  phase(0)->interval(0)->setSignalStates();

}


long
TC_GenericController::switchInterval()
{
  short int g;

  short int updated = 1;
  short int counter = 0;

  while (updated == 1) { // loop until all groups are done changing

    updated = 0;
    counter ++;
  
//     // debug:
//     cout << endl << theSimulationClock->currentTime() << endl;
//     //
    
    for (g = 0; g < nGroups(); g ++) {
      
      signalGroups_[g]->checkConditions();
      
//       // debug:
//       cout << "group: " << signalGroups_[g]->code() << 
// 	"  state: " << signalGroups_[g]->state() << 
// 	"  status: " << signalGroups_[g]->status() << 
// 	"  action: " << signalGroups_[g]->action() << endl;
//       //
      
      if (signalGroups_[g]->action()==CHANGE) {
	signalGroups_[g]->state(signalGroups_[g]->nextPeriod());  
	updated = 1;

	// Write states to signal output file
	if (outputFlag_) 
	  writeOutput(g);

      }
    }

    // Escape if states are not converging (caught in infinte loop)
    if (counter > 20) {
      cerr << "Warning:: Signal states not converging." << endl
	   << "          Problem in condition specification for Controller "
	   << this->code() << ". ";
      theException->exit(1);
    }
  }
  
  resetSignalStates();
  return (long) theSimulationClock->currentTime();
  
}


// Returns pointer to Signal Group matching the given ID.

TC_SignalGroup*
TC_GenericController::signalGroup(short int id)
{
  TC_SignalGroup *sg = NULL;

  for (short int g = 0; g < nGroups(); g ++) {
    if ( signalGroups_[g]->code() == id ) {
      sg = signalGroups_[g];
      break;
    }
  }
  if (!sg) {
    cerr << "Error:: Unknown Signal Group <" << id << ">. ";
    theException->exit(1);
  }

  return sg;
}


// Write data to signal output file.

void
TC_GenericController::writeOutput(short int g)
{
  static int heading = 1;

  if (heading) {
    TC_LogFile::os() << "# Time/ControllerID/GroupID/GroupState" << endl;
    heading = 0;
  }

  TC_LogFile::os() << Fix(theSimulationClock->currentTime(), 0.1) 
		   << "\t" << code_ << "\t"
		   << signalGroups_[g]->code() << "\t"
		   << signalGroups_[g]->state() << endl;
}
