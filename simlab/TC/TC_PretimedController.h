//-*-c++-*------------------------------------------------------------
// NAME: Pretimed Traffic Controller
// AUTH: Qi Yang
// FILE: TC_PretimedController.h
// DATE: Fri Oct 27 21:10:33 1995
//--------------------------------------------------------------------

#ifndef TC_PRETIMEDCONTROLLER_HEADER
#define TC_PRETIMEDCONTROLLER_HEADER

#include <iostream>

#include "TC_Controller.h"

class Reader;

class TC_PretimedController : public TC_Controller
{
   protected:

      short int cycle_;		// cycle length
      short int offset_;	// offset

   public:

      TC_PretimedController(int c, short int t)
	 : TC_Controller(c, t), cycle_(0), offset_(0) { }

      ~TC_PretimedController() { }

      // Called by TC_Parser to initialize the controller

      virtual void read(Reader &is);

      virtual void calcCycleLength();

      virtual long initializeSignalStates();
      virtual long switchInterval();

      virtual void printTimingTable(std::ostream &os = std::cout);
};

#endif
