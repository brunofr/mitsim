//-*-c++-*------------------------------------------------------------
// NAME: Condition for Generic Controller Logic
// AUTH: Angus Davol
// FILE: TC_Condition.h
// DATE: Feb 2001
//--------------------------------------------------------------------

#ifndef TC_CONDITION_HEADER
#define TC_CONDITION_HEADER

#include <new>
//#include <iostream>
#include <vector>

#include "TC_Controller.h"
#include "TC_GenericController.h"
#include "TC_SignalGroup.h"

#include <GRN/Constants.h>
#include <GRN/RoadNetwork.h>
#include <GRN/RN_Signal.h>
#include <GRN/RN_Sensor.h>

#include <Templates/Object.h>

class TC_Controller;
class TC_GenericController;
class TC_SignalGroup;
class Reader;
class RN_Sensor;
class RoadNetwork;

class TC_Condition : public CodedObject
{
protected:
  
  TC_SignalGroup *signalGroup_;	// parent
  short int conditionCode_;
  short int nParameters_;
  double * parameters_;
  short int status_;
  short int nextPeriod_;
  //  RN_Sensor* detector_;
  std::vector <RN_Sensor*> detectors_;
  short int nDetectors_;
  double changeTime_;
  double timer_;
  short int store_; // used for misc. functions
  short int numToSkip_; // number of next conditions to skip

public:
  
  TC_Condition() :
    signalGroup_(NULL),
    conditionCode_(0),
    nParameters_(0),
    //    parameters_(NULL),
    status_(-1),
    nextPeriod_(0),
    //    detector_(NULL),
    nDetectors_(-1),
    changeTime_(-1),
    timer_(-1),
    store_(-1),
    numToSkip_(0) { }

  TC_Condition(TC_SignalGroup *sg) : signalGroup_(sg) {
  }

  virtual ~TC_Condition();  
//   virtual ~TC_Condition() {
//     delete [] parameters_;
//     delete [] detectors_;
//   }
  
  void signalGroup(TC_SignalGroup* g) {
    signalGroup_ = g;
  }
  
  TC_SignalGroup* signalGroup() {
    return signalGroup_;
  }

  //  short int activePeriod() { return activePeriod_; }
  short int conditionCode() { return conditionCode_; }
  short int nParameters() { return nParameters_; }
  double parameter(short int i) { return parameters_[i]; }

  virtual void status(short int n) { status_ = n; }
  short int status() { return status_; }
  
  virtual void nextPeriod(short int n) { nextPeriod_ = n; }
  short int nextPeriod() { return nextPeriod_; }

  virtual void read(Reader& is);
  void check();

  //  TC_Controller* controller();
  TC_GenericController* controller();
  
//   virtual void detector(RN_Sensor* d) { detector_ = d; }
//   virtual RN_Sensor* detector() { return detector_; }

  RN_Sensor* findDetector(short int id);
  short int checkDetectors();

  void numToSkip(short int n) { numToSkip_ = n; }
  short int numToSkip() { return numToSkip_; }
};

#endif
