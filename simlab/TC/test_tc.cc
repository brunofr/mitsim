//-*-c++-*------------------------------------------------------------
// FILE: test_tc.C
// DATE: Sun Nov 12 23:12:51 1995
//--------------------------------------------------------------------

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>

#include <GRN/RN_Parser.h>

#include "TC_ControlSystem.h"

#ifdef INTERNAL_GUI		// graphical mode
#include <DRN/DRN_CmdArgsParser.h>
#include <DRN/DrawableRoadNetwork.h>
#else
#include <Tools/CmdArgsParser.h>
#include <GRN/RoadNetwork.h>
#endif

#include <new.h>

int main(int argc, char *argv[])
{
   set_new_handler(::FreeRamException);

   // Parsing command line args

#ifdef INTERNAL_GUI
   DRN_CmdArgsParser cmd;
   DrawableRoadNetwork *rnptr = new DrawableRoadNetwork;
#else
   CmdArgsParser cmd;
   RoadNetwork *rnptr = new RoadNetwork;
#endif

   cmd.addOption("ctrl", TC_ControlSystem::nameptr(),
		 "Control plan");
   cmd.parse(argc, argv);


   TC_ControlSystem::rnptr(rnptr);

   RN_Parser rnps(rnptr->name());
   rnps.parse(rnptr);

   TC_ControlSystem::openSignalFile();

   theSimulationClock->currentTime(DBL_INF); // make it read all

   while (TC_ControlSystem::nextTime() <=
	  theSimulationClock->currentTime()) {
      TC_ControlSystem::loadControllers();
      TC_ControlSystem::readTime();
   }
 
   TC_ControlSystem::controllersSwitchIntervals();

   TC_ControlSystem::controllersCalculateParameters();

   TC_ControlSystem::printAllControllers();

   return 0;
}
