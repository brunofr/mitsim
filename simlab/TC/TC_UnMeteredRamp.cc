//-*-c++-*------------------------------------------------------------
// NAME: Un-Metered On-Ramp or Off-Ramp
// AUTH: Owen J. Chen
// FILE: TC_UnMeteredRamp.C
// DATE: Wed Nov 13 22:07:31 EST 1996
//--------------------------------------------------------------------

#include <Templates/Listp.h>
#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <IO/Exception.h>
#include <GRN/RoadNetwork.h>
#include <GRN/Constants.h>

#include "TC_UnMeteredRamp.h"
using namespace std;


void
TC_UnMeteredRamp::read(Reader &is)
{
   int num = 0, id;
   is >>  num;
   detectors_.reserve(num);
   if (!is.findToken(OPEN_TOKEN)) theException->exit(1);  
   for (int i = 0; i < num; i ++) {
      is >> id;
      detectors_.push_back(theNetwork->findSensor(id));
      if (!detectors_[i]) {
	 cerr << "Error:: Unknown sensor <" << id << ">. ";
	 is.reference();
	 theException->exit(1);
      }
   }
   if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
}

double
TC_UnMeteredRamp::calculateMeasurement()
{
   double measure = 0.0;
   short int num = 0;
   short int nd = nDetectors();
   register short int i;
   for (i = 0; i < nd; i ++) {
      if (!(detectors_[i]->state() & SENSOR_BROKEN)) {
         measure += detectors_[i]->measurement();
         num ++;
      }
   }
   if (num) return ( measure / num);
   else return 0.0;
}

int
TC_UnMeteredRamp::calculateFlow()
{
   int measure = 0;
   short int nd = nDetectors();
   register short int i;
   for (i = 0; i < nd; i ++) {
      if (!(detectors_[i]->state() & SENSOR_BROKEN)) {
         measure += detectors_[i]->flow();
      }
   }
   return measure ;
}


void
TC_UnMeteredRamp::print(ostream &os)
{
  os << indent << OPEN_TOKEN << endl;
  os << indent
     << code_ << endc
     << type_ << endc
     << nDetectors() << "# detectors" << endl;
  os << indent << indent
     << OPEN_TOKEN << endc;
   for (short int i = 0; i < nDetectors(); i ++) {
      os << detectors_[i]->code() << endc;
   }
   os << CLOSE_TOKEN << endl;
   os << indent << CLOSE_TOKEN << endl;
}

