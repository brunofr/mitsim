//-*-c++-*------------------------------------------------------------
// NAME: Explicitly instantiate all the template instances we use
// NOTE: We needs this file to get the templates compiled
// AUTH: Qi Yang
// FILE: fiTemplates.C
// DATE: Sat Apr 20 13:43:32 1996
//--------------------------------------------------------------------

#ifdef FORCE_INSTANTIATE

#include <Templates/Listp.h>
#include "TC_Controller.h"
#include "TC_ControlRegion.h"

#ifdef __sgi

#pragma instantiate ListpNode<TC_Controller*>
#pragma instantiate Listp<TC_Controller*>

#pragma instantiate ListpNode<TC_ControlRegion*>
#pragma instantiate Listp<TC_ControlRegion*>

#endif // __sgi

#ifdef __GNUC__

template class ListpNode<TC_Controller*>;
template class Listp<TC_Controller*>;

template class ListpNode<TC_ControlRegion*>;
template class Listp<TC_ControlRegion*>;

#endif // __GNUC__

#endif
