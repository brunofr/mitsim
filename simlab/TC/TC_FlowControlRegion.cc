//-*-c++-*------------------------------------------------------------
// NAME: Regional System Manager for Flowwide Coordinated Traffic
//       Signal Controllers
// AUTH: Owen J. Chen  
// FILE: TC_ControllerRegionFlow.C
// DATE: Mon Nov 11 12:18:52 EST 1996
//--------------------------------------------------------------------

#include <Templates/Listp.h>
#include <Tools/Math.h>

#include <Tools/SimulationClock.h>
#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <IO/Exception.h>
#include <GRN/NameTable.h>
#include <GRN/Constants.h>
#include <GRN/RoadNetwork.h>

#include "TC_FlowControlRegion.h"
#include "TC_ResponsiveController.h"
#include "TC_LocalResponsiveController.h"
#include "TC_FlowResponsiveController.h"
using namespace std;


void
TC_FlowControlRegion::read(Reader &is)
{

  is >> updateStepSize_ ;

  is >> regionalReductionFactor_;

  readLocalMeteringRateTable(is);

  readControllers(is);   // in base TC_ControlRegion

  readBottlenecks(is);
   
  switchTime_ = (long) theSimulationClock->currentTime();
  updateTime_ = (long) theSimulationClock->currentTime();
}


void
TC_FlowControlRegion::readLocalMeteringRateTable(Reader &is)
{
  if (!is.findToken(OPEN_TOKEN)) theException->exit(1);  
  cout <<"Load local metering parameters " << code() << endl;
  for (is.eatwhite();
       is.peek() != CLOSE_TOKEN;
       is.eatwhite()) {
    float o, r;
    is >> o >> r;
    localOParams_.push_back(o);
    localRParams_.push_back(r);
  }
  if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);  
}
 
void
TC_FlowControlRegion::readBottlenecks(Reader &is)
{
  int num = 0, id;

  is >> num;
  bottlenecks_.reserve(num);
  for (int i = 0; i < num; i ++) {
	if (!is.findToken(OPEN_TOKEN)) theException->exit(1);  
     
	is >> id;
	bottlenecks_.push_back(new TC_Bottleneck(id) );
	bottlenecks_[i]->read(is);
	if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);     
  }
}



void
TC_FlowControlRegion::printLocalMeteringRateTable(ostream &os)
{
  os << indent << indent
     << OPEN_TOKEN << endc
     << "# Local Metering Rate Parameters " << endl;
  for (short int i = 0; i < localOParams_.size(); i ++) {
	os << indent << indent << localOParams_[i] << endc 
	   << localRParams_[i] << endl;
  }
  os << indent << indent << CLOSE_TOKEN << endl;
}

void
TC_FlowControlRegion::print(ostream &os)
{
  os << OPEN_TOKEN << endl;
  os << code() << endc << "#Control Region ID" << endl;
  os << type_ << endc << "#Control Region type" << endl;
  os << updateStepSize_ << endc 
     << "#update step size" << endl;

  os << indent << OPEN_TOKEN << " # Bottlenecks" << endl;
  for (int i=0; i < nBottlenecks(); i++)
  {
    bottlenecks_[i]->print(os);
  }

  printLocalMeteringRateTable(os);
  os << indent << CLOSE_TOKEN << endl;

  printAllControllers(os);  //in base TC_ControlRegion
  os <<CLOSE_TOKEN << endl;
}



// Calculate new control parameters and schedule the time for next
// calculation.

void
TC_FlowControlRegion::controllersCalculateParameters()
{
  tcc_list_link_type rc;
  typedef TC_FlowResponsiveController frc_type;

  // Set reginalReduction to negative MaxMeteringRate in order to
  // choose the most restrictive regional reduction among all
  // bottlenecks that affect a controller.

  rc = Controllers_.head();
  while (rc) {
    frc_type *frc = (frc_type *) rc->data();
    frc->resetRegionalReduction();
    rc = rc->next();
  }

  // Local Bottleneck Algorithm
  int i, num = nBottlenecks();
  for (i = 0; i < num; i++) {
    bottlenecks_[i]->calculateParameters();
  }

  // Check Regional Reduction, Local Look up Table & Queue Adjustment

  rc = Controllers_.head();

  while (rc) {
    frc_type *frc = (frc_type *) rc->data();
    double rate = (frc->meteringRate() - frc->regionalReduction());
    frc->calculateParameters(rate * regionalReductionFactor_);
    rc = rc->next();
  }

  updateTime_ = (long) theSimulationClock->currentTime() + updateStepSize_;
}


double
TC_FlowControlRegion::localMeteringRateTable(double occupancy)
{
  int last = localOParams_.size() - 1;
  int i = 0;
  while (occupancy > localOParams_[i] &&
		 i < last) {
    i ++;
  }
  return localRParams_[i];
}

double
TC_FlowControlRegion::localMeteringRateAlinea(TC_MeteringMethod *method,
											  double occupancy,
											  double rate)
{
  float desired = localOParams_[0];
  float regulator = localRParams_[0];
  rate += regulator * (desired - occupancy);
  if (rate < method->minMeteringRate()) {
	rate = method->minMeteringRate();
  } else if (rate > method->maxMeteringRate()) {
	rate = method->maxMeteringRate();
  }
  return rate;
}
