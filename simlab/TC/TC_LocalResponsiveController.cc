//-*-c++-*------------------------------------------------------------
// NAME: Local Responsive Ramp metring controller
// AUTH: Qi Yang & Owen J. Chen
// FILE: TC_LocalResponsiveController.C
// DATE: Sun Nov 10 21:55:41 EST 1996
//--------------------------------------------------------------------


#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <Tools/SimulationClock.h>

#include <IO/Exception.h>

#include <GRN/RoadNetwork.h>

#include "TC_Sensor.h"
#include "TC_Signal.h"
#include "TC_LocalResponsiveController.h"
#include "TC_LogFile.h"
using namespace std;

void
TC_LocalResponsiveController::read(Reader &is)
{
   is >>  updateStepSize_ 
      >> signalType_ >>  nEgresses_;

   startTime_ = (long)theSimulationClock->currentTime();

   if (!is.findToken(OPEN_TOKEN)) theException->exit(1);

   readSignals(is);         // in base TC_Controller
   readTimingConstraints(is); // in base TC_ResponsiveController
   readRegulator(is);
   readQueueDetectors(is);   // in base TC_ResponsiveController

   if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);

   initializePhases();
   switchTime_ = initializeSignalStates();
   updateTime_ = (long)theSimulationClock->currentTime();
}


void
TC_LocalResponsiveController::readRegulator(Reader& is)
{
   int num = 0, id;
   if (!is.findToken(OPEN_TOKEN)) theException->exit(1);
   is >> regulator_ >> desiredMeasurement_ >> num;
   mainlineDetectors_.reserve(num);
   if (!is.findToken(OPEN_TOKEN)) theException->exit(1);  
   for (int i = 0; i < num; i ++) {
      is >> id;
      mainlineDetectors_.push_back(theNetwork->findSensor(id));
      if (!mainlineDetectors_[i]) {
		 cerr << "Error:: Unknown sensor <" << id << ">. ";
		 is.reference();
		 theException->exit(1);
      }
   }
   if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
   if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
}


void
TC_LocalResponsiveController::print(ostream &os)
{
   os << indent;
   os << code_ << endc
      << type_ << endc
      << signalType_ << endc
      << nEgresses_ << endc
      << OPEN_TOKEN << endl;
   method_->print(os);
   printRegulators(os);
   printQueueDetectors(os);

   os << indent << CLOSE_TOKEN << endl;
}

 
void
TC_LocalResponsiveController::printRegulators(ostream &os)
{
   os << indent << indent;
   os << regulator_ << endc
      << desiredMeasurement_ << endc
      << nMainlineDetectors() << endc
      << OPEN_TOKEN << endc 
      << "# Detectors" << endl;
   for (short int i = 0; i < nMainlineDetectors(); i ++) {
      os << mainlineDetectors_[i]->code() << endc;
   }
   os << endl << CLOSE_TOKEN << endl;
}


// Returns scheduled time for next updating


void
TC_LocalResponsiveController::calculateMeteringRate(double regionRate)
{
   double current_measurement = calcMainlineDetectorMeasurement();

   // Output for debugging

   TC_LogFile::os() << Round(theSimulationClock->currentTime())
					<< " id=" << code_
					<< " m=" << Fix(current_measurement, 0.1);

   float rate;
   if (regionRate > 1.0E-8)  // Bilevel Responsive Controller
	  rate = regionRate + regulator_ *
		(desiredMeasurement_ - current_measurement);
   else  // Local Responsive Controller
	  rate = meteringRate() +  regulator_ *
		(desiredMeasurement_ - current_measurement);

   // check min and max rates and ramp queue
   
   meteringRate(rate);

   checkMeteringRateBoundary();  // in base class: TC_ResponsiveController
}

double
TC_LocalResponsiveController::calcMainlineDetectorMeasurement()
{
   double measure = 0.0;
   short int num = 0;
   register short int i;
   for (i = 0; i < nMainlineDetectors(); i ++) {
      if (!(mainlineDetectors_[i]->state() & SENSOR_BROKEN)) {
         measure += mainlineDetectors_[i]->measurement();
         num ++;
      }
   }
   if (num) return ( measure / num);
   else return 0.0;
}
