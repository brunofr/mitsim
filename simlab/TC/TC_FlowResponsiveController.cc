//-*-c++-*------------------------------------------------------------
// NAME: Responsive Ramp metring controller
// AUTH: Owen J. Chen & Qi Yang
// FILE: TC_AreaResponsiveController.C
// DATE: Sun Nov 10 21:55:41 EST 1996
//--------------------------------------------------------------------


#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <Tools/SimulationClock.h>

#include <IO/Exception.h>

#include <GRN/RoadNetwork.h>

#include "TC_Sensor.h"
#include "TC_Signal.h"
#include "TC_ResponsiveController.h"
#include "TC_FlowResponsiveController.h"
#include "TC_FlowControlRegion.h"
#include "TC_ControlSystem.h"
#include "TC_LogFile.h"

//#include <fstream>
using namespace std;

// extern ofstream g_signalOutput;

void
TC_FlowResponsiveController::read(Reader &is)
{
  is >> signalType_ >>  nEgresses_;


  startTime_ = (long) theSimulationClock->currentTime();

  if (!is.findToken(OPEN_TOKEN)) theException->exit(1);

  readSignals(is);
  readTimingConstraints(is);

  // is >> weightingFactor_ ;
   
  readMainlineDetectors(is);
  readQueueAdjustDetectors(is);
  readQueueOverrideDetectors(is);

  totalSamplingTimes_ = 
	(int) ceil(queueAdjustPersistTime2_ / updateStepSize_);
  isAdjustPersist_ = new bool[totalSamplingTimes_];
  isOverridePersist_ = new bool[totalSamplingTimes_];
     
  if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);

  initializePhases();
  switchTime_ = initializeSignalStates();
  updateTime_ = (long) theSimulationClock->currentTime();
}


void
TC_FlowResponsiveController::readMainlineDetectors(Reader& is)
{
  int num = 0, id;
  if (!is.findToken(OPEN_TOKEN)) theException->exit(1);
  is >> num;

  mainlineDetectors_.reserve(num);
  if (!is.findToken(OPEN_TOKEN)) theException->exit(1);  
  for (int i = 0; i < num; i ++) {
	is >> id;
	mainlineDetectors_.push_back(theNetwork->findSensor(id));
	if (!mainlineDetectors_[i]) {
	  cerr << "Error:: Unknown sensor <" << id << ">. ";
	  is.reference();
	  theException->exit(1);
	}
  }
  if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
  if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
}


void
TC_FlowResponsiveController::readQueueAdjustDetectors(Reader& is)
{
  int num = 0, id;
  if (!is.findToken(OPEN_TOKEN)) theException->exit(1);
  is >> queueAdjustOccupancy_
	 >> queueAdjustPersistTime1_
	 >> queueAdjustPersistTime2_
	 >> queueAdjustIncrement1_
	 >> queueAdjustIncrement2_
	 >> num;

  queueAdjustDetectors_.reserve(num);
  if (!is.findToken(OPEN_TOKEN)) theException->exit(1);  
  for (int i = 0; i < num; i ++) {
	is >> id;
	queueAdjustDetectors_.push_back(theNetwork->findSensor(id));
	if (!queueAdjustDetectors_[i]) {
	  cerr << "Error:: Unknown sensor <" << id << ">. ";
	  is.reference();
	  theException->exit(1);
	}
  }
  if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
  if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
}

void
TC_FlowResponsiveController::readQueueOverrideDetectors(Reader& is)
{
  int num = 0, id;
  if (!is.findToken(OPEN_TOKEN)) theException->exit(1);
  is >> queueOverrideOccupancy_
	 >> queueOverridePersistTime_
	 >> queueOverrideMeterRateIncrement_
	 >> num;

  queueOverrideDetectors_.reserve(num);
  if (!is.findToken(OPEN_TOKEN)) theException->exit(1);  
  for (int i = 0; i < num; i ++) {
	is >> id;
	queueOverrideDetectors_.push_back(theNetwork->findSensor(id));
	if (!queueOverrideDetectors_[i]) {
	  cerr << "Error:: Unknown sensor <" << id << ">. ";
	  is.reference();
	  theException->exit(1);
	}
  }
  if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
  if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
}


void TC_FlowResponsiveController::resetRegionalReduction()
{
  regionalReduction_ = -method_->maxMeteringRate();
}


void
TC_FlowResponsiveController::meteringRateReduction(double reduction)
{
  if (reduction > regionalReduction_) {
	regionalReduction_ = reduction;
  }
}

double
TC_FlowResponsiveController::calcMainlineMeasurement()
{
  double measure = 0.0;
  short int num = 0;
  register short int i;
  for (i = 0; i < nMainlineDetectors(); i ++) {
	if (!(mainlineDetectors_[i]->state() & SENSOR_BROKEN)) {
	  measure += mainlineDetectors_[i]->measurement();
	  num ++;
	}
  }
  if (num) return ( measure / num);
  else return 0.0;
}

double
TC_FlowResponsiveController::calcQueueAdjustMeasurement()
{
  double measure = 0.0;
  short int num = 0;
  register short int i;
  for (i = 0; i < nQueueAdjustDetectors(); i ++) {
	if (!(queueAdjustDetectors_[i]->state() & SENSOR_BROKEN)) {
	  measure += queueAdjustDetectors_[i]->measurement();
	  num ++;
	}
  }
  if (num) return ( measure / num);
  else return 0.0;
}

double
TC_FlowResponsiveController::calcQueueOverrideMeasurement()
{
  double measure = 0.0;
  short int num = 0;
  register short int i;
  for (i = 0; i < nQueueOverrideDetectors(); i ++) {
	if (!(queueOverrideDetectors_[i]->state() & SENSOR_BROKEN)) {
	  measure += queueOverrideDetectors_[i]->measurement();
	  num ++;
	}
  }
  if (num) return ( measure / num);
  else return 0.0;
}


void
TC_FlowResponsiveController::calculateMeteringRate(double regionalRate)
{
  TC_FlowControlRegion* region = (TC_FlowControlRegion*)controlRegion_;

  // Local Metering Rate Look up Table (LMR)
  double measurement = calcMainlineMeasurement();
  double rate;
  if (region->nRowsLookupTable() > 1) {
    rate = region->localMeteringRateTable(measurement);
  } else {
    rate = region->localMeteringRateAlinea(method_, measurement,
										   meteringRate());
  }
  
  int status = 0;  

  TC_LogFile::os() << (long) theSimulationClock->currentTime()
				   << " id=" << code_
				   << " m=" << Fix(measurement, 0.1);

  // Get lower of Local Metering Rate (LMR) and Regional Metering Rate (RMR):

  if (rate < regionalRate) {
	status = STATUS_LOCAL_RATE;
  } else {
	rate = regionalRate;
	status = STATUS_REGIONAL_RATE;
  }

  // Check Queue Adjustment and Queue Over-ride logic

  // check ramp queue
  if (queueOverridePersistTime() > queueOverridePersistTime_) {
	// Queue override logic applied
	rate += queueOverrideMeterRateIncrement_;   
	status += STATUS_OVERRIDE_QUEUE;
  } else {
	double tm = queueAdjustPersistTime();
	if (tm  > queueAdjustPersistTime2_) {
	  // Large queue adjustment logic applied
	  rate += queueAdjustIncrement2_;
	  status += STATUS_LARGE_QUEUE;
	} else if ( tm  > queueAdjustPersistTime1_) {
	  // Small queue adjustment logic applied
	  rate += queueAdjustIncrement1_;
	  status += STATUS_SMALL_QUEUE;
	}
  }
 
  //update current sampling time

  currentSamplingTime_ = (currentSamplingTime_ + 1) % totalSamplingTimes_ ;

  // Check min and max metering rates

  if (rate < method_->minMeteringRate()) {
	// minimum metering rate applied
	rate = method_->minMeteringRate();
	status += STATUS_MIN_RATE;
  } else if (rate > method_->maxMeteringRate()) {
	// maximum metering rate applied
	rate = method_->maxMeteringRate();
	status += STATUS_MAX_RATE;
  }

  TC_LogFile::os() << " s=" << hex << status << dec;

  meteringRate(rate);
}  

double
TC_FlowResponsiveController::queueAdjustPersistTime()
{
  // check ramp queue

  int i = currentSamplingTime_;

  // record whether the occupancy is over threshold measure

  isAdjustPersist_[i] =
	(calcQueueAdjustMeasurement() > queueAdjustOccupancy_);

  // check how many continuous sampling periods over threshold measure
  int persist = 0;
  while (isAdjustPersist_[i] &&
		 persist < totalSamplingTimes_ ) {
	persist ++;
	i = (i - 1 + totalSamplingTimes_) % totalSamplingTimes_;
  }

  return ( persist * updateStepSize_ ) ;
}

double
TC_FlowResponsiveController::queueOverridePersistTime()
{
  // check ramp queue

  int i = currentSamplingTime_;

  // record whether the occupancy is over threshold measure
  isOverridePersist_[i] =
	(calcQueueOverrideMeasurement() > queueOverrideOccupancy_);

  // check how many continuous sampling periods over threshold measure
  int persist = 0;
  while (isOverridePersist_[i] &&
		 persist < totalSamplingTimes_ ) {
	persist ++;
	i = (i - 1 + totalSamplingTimes_) % totalSamplingTimes_;
  }

  return ( persist * updateStepSize_ ) ;
}

void
TC_FlowResponsiveController::print(ostream &os)
{
  os << indent << code_  << " # controller ID " << endl
	 << indent << type_ << " # controller type " << endl
	 << indent << signalType_  << " # signal type " << endl
	 << indent << nEgresses_ << " # number of Egresses " << endl
	 << indent << OPEN_TOKEN <<  endl;

  printSignals(os);
  method_->print(os);
  printMainlineDetectors(os);
  printQueueAdjustDetectors(os);
  printQueueOverrideDetectors(os);

  os << indent << CLOSE_TOKEN << endl;
}

void
TC_FlowResponsiveController::printMainlineDetectors(ostream &os)
{
  os << indent << indent
	 << "# Downstream mainline detectors" << endl
	 << indent << indent << OPEN_TOKEN << endc;
  for (short int i = 0; i < nMainlineDetectors(); i ++) {
	os << mainlineDetectors_[i]->code() << endc;
  }
  os << CLOSE_TOKEN << endl;
}

void
TC_FlowResponsiveController::printQueueAdjustDetectors(ostream &os)
{
  os << indent << indent
	 << queueAdjustOccupancy_ << endc
	 << queueAdjustPersistTime1_   << endc
	 << queueAdjustPersistTime2_  << endc 
	 << queueAdjustIncrement1_ << endc 
	 << queueAdjustIncrement2_ << endc
	 << "# Queue adjustment detectors" << endl;
  os << indent << indent << OPEN_TOKEN << endc;
  for (short int i = 0; i < nQueueAdjustDetectors(); i ++) {
	os << queueAdjustDetectors_[i]->code() << endc;
  }
  os << CLOSE_TOKEN << endl;
}

void
TC_FlowResponsiveController::printQueueOverrideDetectors(ostream &os)
{
  os << indent << indent
	 << queueOverrideOccupancy_ << endc
	 << queueOverridePersistTime_  << endc 
	 << queueOverrideMeterRateIncrement_ << endc 
	 << "# Queue override detectors" << endl
	 << indent << indent << OPEN_TOKEN << endc;

  for (short int i = 0; i < nQueueOverrideDetectors(); i ++) {
	os << queueOverrideDetectors_[i]->code() << endc;
  }
  os << CLOSE_TOKEN << endl;
}

