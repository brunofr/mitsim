//-*-c++-*------------------------------------------------------------
// NAME: Widgets for Traffic Signal Control
// AUTH: Qi Yang
// FILE: TC_Interval.h
// DATE: Fri Oct 27 09:51:02 1995
//--------------------------------------------------------------------

#ifndef TC_INTERVAL_HEADER
#define TC_INTERVAL_HEADER

#include <iostream>
#include <new>

class TC_Phase;
class TC_Controller;

class TC_Interval
{
   protected:

      TC_Phase *phase_;		// parent class
      short int length_;	// length in second
      unsigned int *states_;	// state for each approach

   public:

      TC_Interval()
		 : phase_(NULL), length_(0), states_(NULL) { }

      TC_Interval(TC_Phase *sp, short int na) : phase_(sp),
			length_(0), states_(new unsigned int[na]) { }

      virtual ~TC_Interval() { delete [] states_; }

      TC_Controller* controller() ;
      void phase(TC_Phase* p) { phase_ = p; }
      TC_Phase* phase()  { return phase_; }

      virtual void print(std::ostream &os = std::cout);

      // length of this interval

      virtual void length(short int n) { length_ = n; }
      short int length()  { return length_; }

      // state for ith signal (4 bits is for each signal)

      virtual void setStates(short int i, unsigned int s) {
		 states_[i] = s;
      }

      virtual unsigned int states(short int i)  {
		 return states_[i];
      }

      // state for a particular movement
      virtual unsigned int state(short int from, short int to);

      virtual void setSignalStates();

};

#endif
