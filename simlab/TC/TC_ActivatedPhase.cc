//-*-c++-*------------------------------------------------------------
// NAME: Phase for traffic activated controller
// AUTH: Qi Yang
// FILE: TC_ActivatedPhase.C
// DATE: Sun Oct 29 14:22:43 1995
//--------------------------------------------------------------------

#include <Tools/ToolKit.h>
#include <Tools/Reader.h>

#include <IO/Exception.h>

#include <GRN/RN_Signal.h>

#include "TC_Controller.h"
#include "TC_ActivatedPhase.h"
#include "TC_Interval.h"

using namespace std;

// Called by TC_Parser to initial a phase for traffic activated
// controller

void
TC_ActivatedPhase::read(Reader& is)
{
   short int num, dt;
   short int n = controller_->nApproaches();

   if (!is.findToken(OPEN_TOKEN)) theException->exit(1);
   
   is >> extensionTime_ >> maxGreenTime_ >> recallToMinGreenTime_;
   is >> num;			// number of intervals

   intervals_.reserve(num);

   for (short int i = 0; i < num; i ++) {
      is >> dt;			// time allocated to this interval
      intervals_.push_back(new TC_Interval(this, n));
      interval(i)->length(dt);
      readSequence(is, i);
   }

   // The minimum green is the length of the first interval.

   minGreenTime_ = interval(0)->length();

   if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
}


void
TC_ActivatedPhase::print(ostream &os)
{
   os << indent << indent << indent;
   os << OPEN_TOKEN << endc
      << extensionTime_ << endc
      << minGreenTime_ << endc
      << maxGreenTime_ << endc
      << recallToMinGreenTime_ << endc
      << nIntervals() << endc
      << "# Extension MinGreen MaxGreen Reset NumIntervals" << endl;

   for (short int i = 0; i < nIntervals(); i ++) {
      interval(i)->print(os);
   }
   
   os << indent << indent << indent
      << CLOSE_TOKEN << endl;
}

