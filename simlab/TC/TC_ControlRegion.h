//-*-c++-*------------------------------------------------------------
// NAME: Regional System Manager for Coordinated Traffic Signal Controllers
// AUTH: Owen J. Chen  
// FILE: TC_ControlRegion.h
// DATE: Mon Nov 11 12:18:52 EST 1996
//--------------------------------------------------------------------

#ifndef TC_CONTROLREGION_HEADER
#define TC_CONTROLREGION_HEADER

#include <new>
#include <iostream>

#include <Tools/Math.h>
#include <Templates/Listp.h>
#include <Templates/Object.h>

#include <GRN/Constants.h>
#include <Tools/Reader.h>

#include "TC_Phase.h"
#include "TC_Controller.h"

class RN_Signal;
class RoadNetwork;
class TC_Phase;
class TC_Interval;
class TC_ResponsiveController;

//--------------------------------------------------------------------
// CLASS NAME: TC_ControlRegion
// ControlRegion manages the following classes of controllers: 
//        ResponsiveController, 
//        LocalResponsiveController, 
//        FlowResponsiveController, 
// All controllers are divided into isolated and coordinated controllers.
// Isolated controllers are stored in a linked list 'listIsolatedCoontrollers',
// Coordinated controllers are stored in a linked list 'listCoordinatedCoontrollers',
//
//--------------------------------------------------------------------


class TC_ControlRegion : public CodedObject
{
protected:

  short int type_; // Control type
  // All controllers have the same type in a region.

  long updateTime_;        // time to calculate parameters
  long switchTime_;      // time to switch signal states
  int updateStepSize_;        // time to calculate parameters

  tcc_list_type Controllers_;

public:
	
  TC_ControlRegion(int c, short int t);

  ~TC_ControlRegion();

  long updateTime() {return updateTime_; }
  long switchTime() {return switchTime_ ; }
  void updateTime(long uptime) {updateTime_ = uptime; }
  void switchTime(long swtime) {switchTime_  = swtime;}
  int updateStepSize() { return updateStepSize_; }

  int nControllers() { return  Controllers_.nNodes() ; }
  void ControllersAppend(int id, TC_ResponsiveController* rc);
  virtual void read(Reader &);
  virtual void readControllers(Reader &);
  virtual void print(std::ostream &os = std::cout);
  virtual void printAllControllers(std::ostream &os = std::cout);
  virtual void controllersCalculateParameters();
  virtual long controllersSwitchIntervals();
  tcc_list_link_type find(int code);

  // Sorting the controllers by updateTime
  int cmp(CodedObject *other) const;

  // Find controller by id
  int cmp(int id) const { return CodedObject::cmp(id); }
};

typedef Listp<TC_ControlRegion*> tcr_list_type;
typedef tcr_list_type::link_type tcr_list_link_type;

#endif

