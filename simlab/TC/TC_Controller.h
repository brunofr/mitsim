//-*-c++-*------------------------------------------------------------
// NAME: Traffic Signal Controller
// AUTH: Qi Yang & Owen J. Chen
// FILE: TC_Controller.h
// DATE: Wed Nov 13 22:07:31 EST 1996
//--------------------------------------------------------------------

#ifndef TC_CONTROLLER_HEADER
#define TC_CONTROLLER_HEADER

#include <new>
#include <iostream>
#include <vector>

#include <Tools/Math.h>
#include <Tools/SimulationClock.h>
#include <Templates/Listp.h>
#include <Templates/Object.h>

#include <GRN/Constants.h>
#include <Tools/Reader.h>
#include "TC_Phase.h"


class RN_Signal;
class TC_Phase;
class TC_Interval;
class RoadNetwork;

//--------------------------------------------------------------------
// CLASS NAME: TC_Controller
// Signal controller is an abstracted base class for 
//        PretimedController,
//        ActivatedController, 
//        ResponsiveController, 
//        LocalResponsiveController, 
//        FlowResponsiveController, 
//--------------------------------------------------------------------

       
class TC_Controller : public CodedObject
{
protected:

  // These are information read from database

  short int type_;		// type of the controller
  short int signalType_;	// signal type
  std::vector <RN_Signal *> signals_; // array of pointers to signals
  short int nApproaches_;	// number of inbound links
  short int nEgresses_;	// number of outgoing links
  std::vector <TC_Phase *> phases_; // array of phases
  long startTime_;	// time that current plan started
      
  // These variables may change from time to time

  long updateTime_;	// time to calculate parameters
  long switchTime_;	// time to switch signal states

  short int currentPhase_;	// current phase index
  short int currentInterval_; // current interval index

public:
	
  TC_Controller(int c, short int t) : CodedObject(c), type_(t) { }

  virtual ~TC_Controller() { }

  virtual TC_Phase* newPhase() { return new TC_Phase; }
  virtual TC_Interval* newInterval() {
	return new TC_Interval;
  }


  virtual void read(Reader& is);
  virtual void readSignals(Reader& is);
  virtual RN_Signal* readSignal(Reader& is, short int i);
  virtual void readTimingTable(Reader& is);

  virtual void print(std::ostream &os = std::cout);
  virtual void printSignals(std::ostream &os = std::cout);
  virtual void printTimingTable(std::ostream &os = std::cout);

  // Following three functions should be overloaded unless it is a
  // unsignalized controller.

  // Called when a new plan is loaded.

  virtual long initializeSignalStates();

  // Calculate new control parameters and schedule the time for
  // next calculation.

  virtual void calculateParameters()  {}

  // Switch to a new interval and schedule the time for next
  // switch based on curren parameters.  Called when switchTime
  // equals 0.  The function also update the signal states in the
  // network.

  virtual long switchInterval() { return ONE_DAY; }

  // Above three functions should be overloaded unless it is a
  // unsignalized controller.

  // Sorting the controllers by updateTime
  int cmp(CodedObject *other) const ;

  // Find controller by id
  int cmp(int id) const { return CodedObject::cmp(id); }

  inline virtual RN_Signal* getSignal(short int i) {
	return signals_[i];
  }

  // For an unsignalize controller, it has one phase containing
  // only one interval.

  inline virtual short int nPhases() { return phases_.size(); }
  inline virtual TC_Phase* phase(short int i) { return phases_[i]; }

  // Return current phase
  inline virtual TC_Phase* phase() {
	return phases_[currentPhase_];
  }

  // Returns ith interval of current phase

  inline virtual TC_Interval* interval(short int i) {
	return phase()->interval(i);
  }

  inline virtual TC_Interval* interval() {
	return interval(currentInterval_);
  }

  // Each approach should have one and only one signal

  inline short int nApproaches() { return nApproaches_; }

  // number of egresses, do not have to equal to number of
  // outgoing links at the downstream node of the link (signal can
  // be posted in the middle of a link, example, 77 Mass Ave)

  inline short int nEgresses() { return nEgresses_; }
  inline long updateTime() {return updateTime_; }
  inline long switchTime() { return switchTime_; }
  virtual int updateStepSize() { return 3600; }
  void  updateTime(long uptime) {updateTime_ = uptime; }
  void switchTime(long swtime) {switchTime_  = swtime;}   
  inline short int signalType() { return signalType_; }
  inline short int type() { return type_; }

  long startTime() { return startTime_; } // (Angus)
};

typedef Listp<TC_Controller*> tcc_list_type;
typedef tcc_list_type::link_type tcc_list_link_type;

#endif
