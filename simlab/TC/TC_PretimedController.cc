//-*-c++-*------------------------------------------------------------
// NAME: Pretimed Traffic Controller
// AUTH: Qi Yang
// FILE: TC_PretimedController.C
// DATE: Fri Oct 27 22:00:47 1995
//--------------------------------------------------------------------

#include <cstdlib>

#include <Templates/Listp.h>

#include <Tools/SimulationClock.h>
#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <IO/Exception.h>

#include <GRN/RoadNetwork.h>

#include "TC_Interval.h"
#include "TC_Phase.h"
#include "TC_Signal.h"
#include "TC_PretimedController.h"
using namespace std;

// Called when a new plan is loaded.  It calculates the start interval
// and time left for that interval based on the offset.  The function
// also update the signal states in the network and returns the time
// for next updating (by calling function switchInterval()).

long TC_PretimedController::initializeSignalStates()
{
   short int &p = currentPhase_;
   short int &i = currentInterval_;
   short int delta;
   TC_Phase *pp;
   
   short int sum = offset_ %= cycle_;

   for (p = 0; p < nPhases(); p ++) {
     pp = phase(p);
     for (i = 0; i < pp->nIntervals(); i ++) {
       delta = pp->interval(i)->length();
       if (sum >= delta) {
	 sum -= delta;
       } else {
	 delta -= sum;
	 phase(p)->interval(i)->setSignalStates();
	 return (long)theSimulationClock->currentTime() + delta;
       }
     }
   }
   p = 0;
   i = 0;
   delta = phase(p)->interval(i)->length();
   return (long) theSimulationClock->currentTime() + delta;
}


// Switch to a new interval and schedule the time for next switching
// based on curren parameters.  Called when switchTime equals 0.  The
// function also update the signal states in the network.


long TC_PretimedController::switchInterval()
{
   short int &p = currentPhase_;
   short int &i = currentInterval_;
   short int t;

   do {
	  i = (i + 1) % phase(p)->nIntervals();
	  if (i == 0) p = (p + 1) % nPhases();
   } while ((t = phase(p)->interval(i)->length()) == 0);

   phase(p)->interval(i)->setSignalStates();

   return (long) theSimulationClock->currentTime() + t;

}

void
TC_PretimedController::read(Reader &is)
{
   is >> signalType_ >>  nEgresses_;

   startTime_ = (long)theSimulationClock->currentTime();

   if (!is.findToken(OPEN_TOKEN)) theException->exit(1);

   readSignals(is);
   is >> offset_;
   readTimingTable(is);
   calcCycleLength();

   if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);

   switchTime_ = initializeSignalStates();
   updateTime_ = ONE_DAY;
}


// Calculate the cycle length by adding the length of all intervals

void
TC_PretimedController::calcCycleLength()
{
   short int p;
   register short int i;
   cycle_ = 0;
   for (p = 0; p < nPhases(); p ++) {
      for (i = 0; i < phase(p)->nIntervals(); i ++) {
	 cycle_ += phase(p)->interval(i)->length();
      }
   }
}


void
TC_PretimedController::printTimingTable(ostream &os)
{
   short int i;
   os << indent << indent << nPhases() << endc
      << offset_ << endc
      << OPEN_TOKEN << endc
      << "# Offset NumPhases" << endl;
   for (i = 0; i < nPhases(); i ++) {
      phase(i)->print(os);
   }
   os << indent << indent << CLOSE_TOKEN << endl;
}
