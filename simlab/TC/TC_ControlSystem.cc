//-*-c++-*------------------------------------------------------------
// NAME: System Manager for All Traffic Signal Controllers
// AUTH: Owen J. Chen  
// FILE: TC_ControlSystem.C
// DATE: Mon Nov 11 12:18:52 EST 1996
//--------------------------------------------------------------------

#include <Templates/Listp.h>
#include <Tools/Math.h>

#include <Tools/SimulationClock.h>
#include <Tools/ToolKit.h>
#include <Tools/Reader.h>

#include <IO/Exception.h>

#include <GRN/NameTable.h>
#include <GRN/RoadNetwork.h>

#include "TC_Interval.h"
#include "TC_Phase.h"
#include "TC_Signal.h"
#include "TC_Controller.h"
#include "TC_ControlRegion.h"
#include "TC_ControlSystem.h"
#include "TC_AreaControlRegion.h"
#include "TC_FlowControlRegion.h"
#include "TC_PretimedController.h"
#include "TC_ActivatedController.h"
#include "TC_GenericController.h" // (Angus)
#include "TC_SignalGroup.h" // (Angus)
#include "TC_Condition.h" // (Angus)
#include "TC_LocalResponsiveController.h"
//#include <fstream>
using namespace std;


tcc_list_type TC_ControlSystem::listIsolatedControllers_;
tcr_list_type TC_ControlSystem::listControlRegions_;

Reader TC_ControlSystem::is_;
char* TC_ControlSystem::name_ = NULL;
long TC_ControlSystem::nextTime_ = 0;

void
TC_ControlSystem::openSignalFile()
{
  if (ToolKit::isValidInputFilename(name_)) {

	const char *filename = ToolKit::optionalInputFile(name_);
	if (!filename) theException->exit(1);

	is_.open(filename);
	cout << "Signal controller database is <"
		 << is_.name() << ">." << endl;
	TC_ControlSystem::readTime();
  } else {
	cout << "No signal controllers." << endl;
	nextTime_ = ONE_DAY;
  }
}

long TC_ControlSystem::readTime()
{
  is_.eatwhite();
  if (!(is_.eof()) && is_.good()) {
	nextTime_ = (long)is_.gettime();
  } else {
	nextTime_ = ONE_DAY;
  }
  return (nextTime_);
}


void
TC_ControlSystem::loadControllers()
{
  int id;
  short int t;
  TC_Controller *sc;
  TC_ControlRegion *cRegion;

  if (!is_.findToken(OPEN_TOKEN)) theException->exit(1);
  
  for (is_.eatwhite();
	   is_.peek() != CLOSE_TOKEN;
	   is_.eatwhite())	
	{
	  is_ >> id >> t;
	  cout << "Load Controller ID=" << id  << " type=" << t << endl;
    
	  switch (t) 
		{
		case CONTROLLER_UNSIGNALIZED:
		  {
			sc = new TC_Controller(id, t);
			listIsolatedControllersInsert(id, sc);
			sc->read(is_);    // this may differ for each type of controller
			break;
		  }
		case CONTROLLER_PRETIMED:
		  {
			sc = new TC_PretimedController(id, t);
			listIsolatedControllersInsert(id, sc);
			sc->read(is_); // this may differ for each type of controller
			break;
		  }
		case CONTROLLER_ACTIVATED:
		  {
			sc = new TC_ActivatedController(id, t);
			listIsolatedControllersInsert(id, sc);
			sc->read(is_); // this may differ for each type of controller
			break;
		  }
		case CONTROLLER_GENERIC: // (Angus)
		  {
			sc = new TC_GenericController(id, t);
			listIsolatedControllersInsert(id, sc);
			sc->read(is_); // this may differ for each type of controller
			break;
		  }
		case CONTROLLER_RESPONSIVE_LOCAL:
		  {
			sc = new TC_LocalResponsiveController(id, t);
			listIsolatedControllersInsert(id, sc);
			sc->read(is_); // this may differ for each type of controller
			break;
		  }
		case CONTROLLER_RESPONSIVE_AREA:
		case CONTROLLER_RESPONSIVE_BILEVEL:
		  {
#ifdef REGIONAL_CONTROL
			cRegion = new TC_AreaControlRegion(id, t);
			listControlRegionsInsert(id, cRegion);
			cRegion->read(is_);
#else
			cerr << "Error:: Regional controllers not supported." << endl;
			theException->exit(1);
#endif
			break;
		  }
		case CONTROLLER_RESPONSIVE_FLOW:
		  {
			cRegion = new TC_FlowControlRegion(id, t);
			listControlRegionsInsert(id, cRegion);
			cRegion->read(is_);
			break;
		  }
		default:
		  {
			cerr << "Error:: Unknown controller type <" 
				 << t << ">. ";
			is_.reference();
			theException->exit(1);
		  }
		}
	}
  if (!is_.findToken(CLOSE_TOKEN)) theException->exit(1);


  /*  
	  // This is for Debugging purpose.  Print the Control Input to a file
	  cout << "Print Controller Input to `` control_input.tmp '' " << endl;
	  ofstream control_inputFile("control_input.tmp", ios::out);
	  if (!control_inputFile)
	  {
	  cerr << "cannot open ``control_input.tmp'' to write " << endl;
	  exit(-1);
	  }
	  printAllControllers(control_inputFile);
  */
}



void
TC_ControlSystem::listIsolatedControllersInsert(int id, TC_Controller* sc)
{
  tcc_list_link_type exist = listIsolatedControllers_.remove(id);
  if (exist) {
	if (ToolKit::verbose()) {
	  cout << "Controller <" << id << "> replaced at "
		   << theSimulationClock->currentStringTime()
		   << endl;
	}
	delete exist->data();
	delete exist;
  }

  listIsolatedControllers_.insert(sc);
}


void
TC_ControlSystem::listControlRegionsInsert(int id, TC_ControlRegion* cr)
{
  tcr_list_link_type exist = listControlRegions_.remove(id);
  if (exist) {
	if (ToolKit::verbose()) {
	  cout << "Control Region <" << id << "> replaced at "
		   << theSimulationClock->currentStringTime()
		   << endl;
	}
	delete exist->data();
	delete exist;
  }
  listControlRegions_.insert(cr);
}



void
TC_ControlSystem::printAllControllers(ostream &os)
{
  // Isolated controllers
  tcc_list_link_type sc = listIsolatedControllers_.head();
  while (sc) {
	(*sc)->print(os);
	sc = sc->next();
  }
  //Control Region
  tcr_list_link_type cr = listControlRegions_.head();
  while (cr) {
	(*cr)->print(os);
	cr = cr->next();
  }
}


// Find a controller with given ID

tcc_list_link_type TC_ControlSystem::find(int id)
{
  // look for an isolated controller first

  tcc_list_link_type sc = listIsolatedControllers_.find(id);

  // look for a coordinated controller in control Regions 

  tcr_list_link_type cr = listControlRegions_.head();
  while (!sc && cr) {
	sc = (*cr)->find(id);
	cr = cr->next();
  }

  return sc;
}


// Calculate new control parameters and schedule the time for next
// calculation.

void
TC_ControlSystem::controllersCalculateParameters()
{
  long now = (long) theSimulationClock->currentTime() + 1;

  // Isolated controllers

  tcc_list_link_type c;
   
  while ((c = listIsolatedControllers_.head()) &&
		 (*c)->updateTime() <= now) {
	(*c)->calculateParameters();
	listIsolatedControllers_.resort(c);
  }
   
  // Control Regions

  tcr_list_link_type r; 
  while ((r = listControlRegions_.head()) &&
		 (*r)->updateTime() <= now) {
	(*r)->controllersCalculateParameters();
	listControlRegions_.resort(r);
  }
}


// Switch to a new interval and schedule the time for next switch
// based on curren parameters.

void
TC_ControlSystem::controllersSwitchIntervals()
{
  // Isolated controllers
  tcc_list_link_type sc = listIsolatedControllers_.head();
  while (sc) {
	if ((*sc)->switchTime() <= theSimulationClock->currentTime()) {
	  (*sc)->switchTime((*sc)->switchInterval());
	}
	sc = sc->next();
  }
  
  //  Control Regions
  tcr_list_link_type cr = listControlRegions_.head();
  while (cr) {
	if ((*cr)->switchTime() <= theSimulationClock->currentTime()) {
	  (*cr)->switchTime((*cr)->controllersSwitchIntervals());
	}
	cr = cr->next();
  }
}
