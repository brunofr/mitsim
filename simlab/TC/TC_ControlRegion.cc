//-*-c++-*------------------------------------------------------------
// NAME: System Manager for All Traffic Signal Controllers
// AUTH: Owen J. Chen  
// FILE: TC_ControllerSystem.C
// DATE: Mon Nov 11 12:18:52 EST 1996
//--------------------------------------------------------------------

#include <Templates/Listp.h>
#include <Tools/Math.h>

#include <Tools/SimulationClock.h>
#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <new>
#include <IO/Exception.h>
using namespace std;

#include <GRN/NameTable.h>
#include <GRN/Constants.h>
#include <GRN/RoadNetwork.h>
#include "TC_Controller.h"
#include "TC_ControlRegion.h"
#include "TC_ResponsiveController.h"
#include "TC_LocalResponsiveController.h"
#include "TC_FlowResponsiveController.h"


TC_ControlRegion::TC_ControlRegion(int c, short int t)
  : CodedObject(c)
{ 
  type_ = t;
}

TC_ControlRegion::~TC_ControlRegion() 
{
}

void
TC_ControlRegion::read(Reader &is)
{
  is >> updateStepSize_ ; 

  readControllers(is);
  
  switchTime_ = (long) theSimulationClock->currentTime();
  updateTime_ = (long) theSimulationClock->currentTime();
}

void
TC_ControlRegion::readControllers(Reader &is)
{
  TC_ResponsiveController *rc;
  int id, numCtrl;

  is >> numCtrl ;

  for ( int i = 0; i < numCtrl; i++)
	{
	  if (!is.findToken(OPEN_TOKEN)) theException->exit(1);

	  is >> id ;
    
	  cout << "Load controller " << id << " in Region " << code() << endl;

	  switch (type_)
		{
		case CONTROLLER_RESPONSIVE_AREA:
		  {
			rc = new TC_ResponsiveController(id, type_);
			ControllersAppend(id, rc);
			break;
		  }
		case CONTROLLER_RESPONSIVE_BILEVEL:
		  {
			rc = new TC_LocalResponsiveController(id, type_);
			ControllersAppend(id, rc);
			break;
		  }
      
		case  CONTROLLER_RESPONSIVE_FLOW:
		  {
			rc = new TC_FlowResponsiveController(id, type_);
			ControllersAppend(id, rc);
			break;
		  }
		default:
		  {
			cerr << "Error:: controller type is NOT Responsive <" 
				 << type_ << ">. ";
			is.reference();
			theException->exit(1);
		  }
		}

	  // set control region for the controller.
	  rc->controlRegion(this);

	  // use regional updateStepSize
	  rc->setUpdateStepSize((int)updateStepSize_);
	  rc->read(is);      // load the controller
    
	  if (ToolKit::debug()) {
		rc->print();
	  }

	  if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
	}
}

void
TC_ControlRegion::ControllersAppend(int id, TC_ResponsiveController* rc)
{
  tcc_list_link_type exist = Controllers_.find(id);
  if (exist) {
	if (ToolKit::verbose()) {
	  cout << "Controller <" << id << "> replaced at "
		   << theSimulationClock->currentStringTime()
		   << endl;
	}
	Controllers_.remove(rc);
	delete exist->data();
	delete exist;
  }

  Controllers_.append(rc);
}


void
TC_ControlRegion::printAllControllers(ostream &os)
{
  tcc_list_link_type rc = Controllers_.head();
  while (rc) {
	(*rc)->print(os);
	rc = rc->next();
  }
}

void
TC_ControlRegion::print(ostream &os)
{
  os << OPEN_TOKEN << endl;
  os << code() << endc << "#Control Region ID" << endl;
  os << type_ << endc << "#Control Region type" << endl;
  os << updateStepSize_ << endc 
	 << "#update step size " << endl
	 << "# below are controllers in this region " << endl;
  printAllControllers(os);
  os <<CLOSE_TOKEN << endl;
}


// Find a controller with given ID

tcc_list_link_type TC_ControlRegion::find(int id)
{
  return Controllers_.find(id);
}


// Calculate new control parameters and schedule the time for next
// calculation.

void
TC_ControlRegion::controllersCalculateParameters()
{
  int i = 0;
  int n = nControllers();
  tcc_list_link_type rc = Controllers_.head();
  while (rc) {
	if ( i >= n) {
	  cerr << " There are more controllers than they should be!" << endl;
	  theException->exit(1);
	}
	(*rc)->calculateParameters();  
	i++;
	rc = rc->next();
  }
}



// Switch to a new interval and schedule the time for next switch
// based on curren parameters.

long TC_ControlRegion::controllersSwitchIntervals()
{
  tcc_list_link_type rc = Controllers_.head();
  while (rc) {
	if ((*rc)->switchTime() <= theSimulationClock->currentTime()) {
	  (*rc)->switchTime((*rc)->switchInterval());
	}
	rc = rc->next();
  }
  switchTime_ = (long) theSimulationClock->currentTime();
  return switchTime_;
}


// This function overload the virtual function in CodedObject and used
// to sort the controllers by updateTime_.

int TC_ControlRegion::cmp(CodedObject *other) const
{
  TC_ControlRegion *o = (TC_ControlRegion*) other;
  if (updateTime_ < o->updateTime_) {
	return -1;
  } else if (switchTime_ > o->switchTime_) {
	return 1;
  } else {
	return 0;
  }
}
