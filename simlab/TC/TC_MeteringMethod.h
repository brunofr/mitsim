//-*-c++-*------------------------------------------------------------
// TC_MeteringMethod.h
//
// Qi Yang and Masroor
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef TC_METERINGMETHOD_HEADER
#define TC_METERINGMETHOD_HEADER

#include <iostream>
#include <Tools/Reader.h>

class TC_ResponsiveController;
class TC_Phase;

class TC_MeteringMethod {

   public:

	  enum {
		 METERING_METHOD_SINGLE  = 1,
		 METERING_METHOD_PLATOON = 2
	  };

	  TC_MeteringMethod(TC_ResponsiveController *p)
		 : parent_(p) {
	  }
	  virtual ~TC_MeteringMethod() { }

	  // These applies to both platoon and signle vehicle metering

	  inline short int minMeteringRate() { return minMeteringRate_; }
	  inline short int maxMeteringRate() { return maxMeteringRate_; }

	  // This applies to single vehicle metering

	  inline float leftOver() { return 0.0; }
	  inline virtual short int green() { return 0; }

	  // These applies to both platoon metering only

	  inline float cycle() { return cycle_; }
	  virtual inline short int yellow() { return 0; }
	  virtual inline short int lostTime(){ return 0; }
	  virtual inline short int minRed() { return 0; }
	  virtual inline short int maxRed() { return 0; }

	  virtual short int calcRedTime(TC_Phase *) { return 0; }


	  virtual void print(std::ostream &os = std::cout);
	  virtual void read(Reader &is);

	  virtual void setSignalTiming(TC_Phase *, double) { }

   protected:

	  TC_ResponsiveController *parent_;

	  float cycle_;
	  short int minMeteringRate_;
	  short int maxMeteringRate_;
};
	
class TC_PlatoonMetering : public TC_MeteringMethod
{
   public:

	  TC_PlatoonMetering(TC_ResponsiveController *p)
		 : TC_MeteringMethod(p) { }
	  ~TC_PlatoonMetering() { }

	  inline short int yellow() { return yellow_; }
	  inline short int lostTime(){ return lostTime_; }
	  inline short int minRed() { return minRed_; }
	  inline short int maxRed() { return maxRed_; }

	  short int calcRedTime(TC_Phase *p);

	  void print(std::ostream &os = std::cout);
	  void read(Reader &is);

	  void setSignalTiming(TC_Phase *p, double rate);

   protected:

	  short int yellow_;
	  short int lostTime_;
	  short int minRed_;
	  short int maxRed_;
};


class TC_SingleMetering : public TC_MeteringMethod
{
   public:

	  TC_SingleMetering(TC_ResponsiveController *p)
		 : TC_MeteringMethod(p) { }
	  ~TC_SingleMetering() { }

	  inline float leftOver() { return leftOver_; }
	  inline short int green() { return green_; }

	  short int calcRedTime(TC_Phase *p);

	  void print(std::ostream &os = std::cout);
	  void read(Reader &is);

	  void setSignalTiming(TC_Phase *p, double rate);

   protected:

	  char isMetering_;
	  float leftOver_;
	  short int green_;
};

#endif
