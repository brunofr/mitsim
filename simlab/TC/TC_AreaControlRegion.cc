//-*-c++-*------------------------------------------------------------
// NAME: Regional System Manager for Areawide Coordinated Traffic
//       Signal Controllers
// AUTH: Owen J. Chen  
// FILE: TC_AreaControllerRegion.C
// DATE: Mon Nov 11 12:18:52 EST 1996
//--------------------------------------------------------------------

#ifdef REGIONAL_CONTROL

#include <Templates/Listp.h>
#include <Tools/Math.h>

#include <Tools/SimulationClock.h>
#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <IO/Exception.h>

#include <GRN/NameTable.h>
#include <GRN/Constants.h>
#include <GRN/RN_Signal.h>

#include "TC_AreaControlRegion.h"
#include "TC_ResponsiveController.h"
#include "TC_LocalResponsiveController.h"

TC_AreaControlRegion::TC_AreaControlRegion(int c, short int t)
   :TC_ControlRegion(c,t)
{ 
   // initialize MATLAB variables for AREA control
   pMatlab_ = NULL;
   scenario_ = 0;
   timeHorizon_ = 0;
   matlabInputBuffer_ = new char[50];
   matlabOutputBuffer_ = new char[200];
   matlabMeteringRates_ = NULL;
   meteringRates_ = NULL;

}

TC_AreaControlRegion::~TC_AreaControlRegion()
{
   delete[] matlabInputBuffer_; 
   delete[] matlabOutputBuffer_;
   if(pMatlab_)  
   {
	  engClose(pMatlab_); 
	  cout << "close Matlab Engine." << endl;
   }
}


void
TC_AreaControlRegion::loadControllers(Reader &is)
{
   int id, numCtrl;
   TC_ResponsiveController *rc;

   is >> scenario_ >> updateStepSize_ >> AreaUpdateStepSize_ >> numCtrl ;

   for ( int i = 0; i < numCtrl; i++)
   {
	  if (!is.findToken(OPEN_TOKEN)) theException->exit(1);

	  is >> id ;
	  switch (type_) {
      
		 case CONTROLLER_RESPONSIVE_AREA:
			{
			   rc = new TC_ResponsiveController(id, type_);
			   ControllersAppend(id, rc);
			   break;
			}
		 case CONTROLLER_RESPONSIVE_BILEVEL:
			{
			   rc = new TC_LocalResponsiveController(id, type_);
			   ControllersAppend(id, rc);
			   break;
			}

		 default:
			{
			   cerr << "Error:: Unknown controller type <" 
					<< type_ << ">. ";
			   is.reference();
			   theException->exit(1);
			}
	  }

	  rc->setUpdateStepSize(updateStepSize_);  // use regional updateStepSize
	  rc->read(is);      // load the controller
	  rc->controlRegion(this); // set this control region for the controller
	  if (ToolKit::debug()) {
		 rc->print();
	  }

	  if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
	  switchTime_ = theSimulationClock->currentTime();
	  updateTime_ = theSimulationClock->currentTime();
	  AreaUpdateTime_ =  theSimulationClock->currentTime();
   }
}



// Calculate new control parameters and schedule the time for next
// calculation.

void
TC_AreaControlRegion::controllersCalculateParameters()
{

   // Update Area level
   if ( AreaUpdateTime_ <= theSimulationClock->currentTime())
   {
	  calculateRegionParameters();
	  // time for next calculation
	  AreaUpdateTime_ = theSimulationClock->currentTime() + AreaUpdateStepSize_;
   }


   //Update local level
   int i = 0;
   int n = nControllers();
   ListItem<TC_Controller> *rc = Controllers_.head();
   while (rc)
   {
	  if ( i >= n)
	  {
		 cerr << " There are more controllers than they should be!" << endl;
		 theException->exit(1);
	  }

	  ((TC_ResponsiveController*)(rc->item()))->calculateParameters(meteringRates_[i]);  
	  i++;
	  rc = rc->next();
   }

   updateTime_ = theSimulationClock->currentTime() + updateStepSize_;

   //get the lower bound of two update times
   if (updateTime_ > AreaUpdateTime_)
   {
	  updateTime_ = AreaUpdateTime_ ;
   }
   
}


void 
TC_AreaControlRegion::calculateRegionParameters()
{
   // Open Matlab Engine
   if (pMatlab_ == NULL) {
      if (!(pMatlab_ = engOpen("\0"))) {
		 cerr << "Error:: Can not open MATLAB";
		 theException->exit(1);
      }
      cout << "Launch Matlab Engine." << endl;
   }

   cout << "Calling uplevel control module (MATLAB). Be patient ..." << endl;
   
   // Update the Rolling Time Horizon
   timeHorizon_ ++;

   // Running the Matlab Solver
   sprintf(matlabInputBuffer_, "clear; meteringRates=AreaControl(%d,%d);", 
		   scenario_, timeHorizon_); 
   if (engEvalString(pMatlab_, matlabInputBuffer_)) {
	  cerr << "Error:: Calling Matlab AreaControl()." << endl;
	  engClose(pMatlab_);
	  theException->exit (1);
   }   
   
   cout << matlabOutputBuffer_ << endl;
   
   // Getting Results from the Matlab Solver
   matlabMeteringRates_ = engGetArray(pMatlab_, "meteringRates");
   if (!matlabMeteringRates_) {
	  cerr << "Error:: Cannot get meterRates from MATLAB";
	  engClose(pMatlab_);
	  theException->exit(1);
   }

   int row = 0, col = 0;

   // Temporarily disabled

   // get real number part.
   meteringRates_ = mxGetPr(matlabMeteringRates_);

   // get row and col.
   row = mxGetM(matlabMeteringRates_);
   col = mxGetN(matlabMeteringRates_);

   int nRampMeters = nControllers();
      
   if (row == nRampMeters && col == 1 || row == 1 && col == nRampMeters) 
   {
	  cout << "Finished uplevel control module. Next call will be at "
		   << updateTime_ << endl;
   } 
   else {
	  cerr << "Error:: Size of MATLAB returned metering rates <"
		   << row << "," << col << "> should be <" 
		   << nRampMeters << ",1>" << endl;
	  cerr << "        Problem in AreaControl module." << endl;
	  engClose(pMatlab_);
	  theException->exit(1);
   }
}	  

#endif
