//-*-c++-*------------------------------------------------------------
// NAME: Generic Traffic Controller
// AUTH: Angus Davol
// FILE: TC_GenericController.h
// DATE: Feb 2001
//--------------------------------------------------------------------

#ifndef TC_GENERICCONTROLLER_HEADER
#define TC_GENERICCONTROLLER_HEADER

#include <new>
//#include <iostream>
#include <vector>

#include "TC_Controller.h"
#include "TC_SignalGroup.h"
#include "TC_Condition.h"
#include "TC_Phase.h"
#include "TC_Interval.h"

#include <GRN/Constants.h>

class RN_Signal;
class RN_Sensor;
class RoadNetwork;
class Reader;
class TC_Phase;
class TC_Interval;
class TC_SignalGroup;
class TC_Condition;

// Constants for SignalGroup status
const unsigned int ACTIVE              = 0x0001;
const unsigned int COMPLETED           = 0x0002;
const unsigned int PASSIVE             = 0x0004;
const unsigned int SKIPPED             = 0x0008;
const unsigned int GAP_OUT             = 0x0010;
const unsigned int BUS_DETECTED        = 0x0020;
const unsigned int BUS_EXTENSION       = 0x0040;
const unsigned int BUS_INSERTION       = 0x0080;
const unsigned int BUS_INSERTED_AFTER  = 0x0100;
const unsigned int BUS_INSERTED_BEFORE = 0x0200;
const unsigned int BUS_EARLY_START     = 0x0400;
const unsigned int BUS_SHORTENED       = 0x0800;
// const short int INACTIVE = 0;
// const short int COMPLETED = 1;
// const short int PASSIVE = 2;
// const short int ACTIVE = 3;

// Constants for SignalGroup action
const short int CHANGE = 1;
const short int HOLD = 2;

// Constants for Condition status
const short int CONDITION_FALSE = 0;
const short int CONDITION_TRUE = 1;
const short int CONDITION_SKIP_NEXT = 2;

class TC_GenericController : public TC_Controller
{
protected:
  
  short int * groups_;  // signal group for each movement
  std::vector <TC_SignalGroup *> signalGroups_;
  short int outputFlag_; // for signal output file
 
public:
  
  TC_GenericController(int c, short int t) :
    TC_Controller(c, t), 
    groups_(NULL),
    outputFlag_(0) { }
  
  virtual ~TC_GenericController() {
	delete [] groups_;
  }

  virtual void read(Reader& is);
  virtual void readSignals(Reader& is);
  virtual RN_Signal* readSignal(Reader& is, short int i);
  virtual void readSignalGroups(Reader& is);

  virtual TC_SignalGroup* newSignalGroup() {
    return new TC_SignalGroup;
  }

  inline virtual short int nGroups() { return signalGroups_.size(); }
//   inline virtual TC_SignalGroup* signalGroup(short int i) {
//     return signalGroups_[i];
//   }

  virtual long initializeSignalStates();
  virtual void resetSignalStates();
  virtual long switchInterval();

  TC_SignalGroup* signalGroup(short int id);
  void writeOutput(short int g);

  void outputFlag(short int f) { outputFlag_ = f; }
};

#endif
