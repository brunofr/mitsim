//-*-c++-*------------------------------------------------------------
// NAME: Signal and signs with communication support
// AUTH: Qi Yang
// FILE: TC_Signal.C
// DATE: Thu Nov 16 22:56:02 1995
//--------------------------------------------------------------------

//#include <fstream>
#include <Tools/SimulationClock.h>
#include <GRN/Constants.h>

#ifdef INTERNAL_GUI
#include <DRN/DRN_DrawingArea.h>
#endif

#include "TC_Signal.h"

TC_Signal::TC_Signal() :
  DRN_Signal()
{
}


void
TC_Signal::state(unsigned int s)
{
  state_ = s;

#ifdef INTERNAL_GUI
   if (!theDrawingArea->isSignalDrawable()) return;
   draw(theDrawingArea);
#endif
}

unsigned int
TC_Signal::state()
{
  return RN_Signal::state();
}


void
TC_Signal::send(IOService &cli)
{
   cli << (index_ | MSG_TYPE_SIGNAL) << state_;
   cli.flush(SEND_IMMEDIATELY);
}

void
TC_Signal::receive(IOService &svr)
{
   unsigned int s;
   svr >> s;
   state(s);
}
