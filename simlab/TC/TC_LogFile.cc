//-*-c++-*------------------------------------------------------------
// TC_LogFile.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <cstring>
#include <cstdlib>
#include <iostream>
using namespace std;

#include "TC_LogFile.h"

TC_LogFile* TC_LogFile::_instance = 0;

TC_LogFile::TC_LogFile(const char *filename_)
{
  if (filename_) _filename = strdup(filename_);
  else _filename = strdup("tclog.out");
  _os = new ofstream(_filename);
}

TC_LogFile::~TC_LogFile()
{
  if (_instance) {
	cout << "TC log file saved in " << _filename << "." << endl;
	_os->close();
	delete _os;
	free(_filename);
  }
}

TC_LogFile *TC_LogFile::the(const char *filename)
{
  if (_instance) return _instance;
  _instance = new TC_LogFile(filename);

  cout << "Created TC log file " << _instance->_filename
	   << "." << endl; 

  return _instance;
}
