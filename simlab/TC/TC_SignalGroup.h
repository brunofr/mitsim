//-*-c++-*------------------------------------------------------------
// NAME: Signal Group for Generic Controller
// AUTH: Angus Davol
// FILE: TC_SignalGroup.h
// DATE: Feb 2001
//--------------------------------------------------------------------

#ifndef TC_SIGNALGROUP_HEADER
#define TC_SIGNALGROUP_HEADER

//#include <iostream>
#include <new>
#include <vector>

#include "TC_Controller.h"
#include "TC_GenericController.h"
#include "TC_SignalGroup.h"
#include "TC_Condition.h"


#include <Templates/Object.h>

class TC_Controller;
class TC_GenericController;
class Reader;
class TC_Condition;

class TC_SignalGroup : public CodedObject
{
protected:
  
  //  TC_Controller *controller_;	// parent
  TC_GenericController *controller_;	// parent
  //  short int referenceTime_; 
  short int initialPeriod_;
  std::vector <TC_Condition *> conditions_;
  short int state_;
  short int status_;
  short int action_;
  short int nextPeriod_;
  double periodStartTime_;
  double initializationTime_;
  short int nPrecedingGroups_;
  short int * precedingGroups_;
  double gapTimer_;
  short int checkinCounter_;
  
public:
  
  TC_SignalGroup() :
    controller_(NULL),
    //    genericController_(NULL),
    //    referenceTime_(0),
    initialPeriod_(0),
    state_(0),
    status_(0),
    action_(0),
    nextPeriod_(0),
    periodStartTime_(0),
    initializationTime_(0),
    nPrecedingGroups_(0),
    precedingGroups_(NULL),
    gapTimer_(-1),
    checkinCounter_(0) { }

  TC_SignalGroup(TC_GenericController *tc) : controller_(tc) { }
  
  virtual ~TC_SignalGroup() {
    //    delete [] controller_;
    //    delete [] genericController_;
    delete [] precedingGroups_;
  }
  
//   void controller(TC_Controller* c) {
//     controller_ = c;
//   }
  
//   TC_Controller* controller() {
//     return controller_;
//   }
  
  void controller(TC_GenericController* c) {
    controller_ = c;
  }
  
  TC_GenericController* controller() {
    return controller_;
  }

  //  short int referenceTime() { return referenceTime_; }
  short int initialPeriod() { return initialPeriod_; }
  TC_Condition* condition(short int i) { return conditions_[i]; }

  virtual void read(Reader& is);
  virtual void checkConditions();

  inline virtual short int nConditions() { return conditions_.size(); }

  virtual TC_Condition* newCondition() {
    return new TC_Condition;
  }

  virtual void state(short int n);
  short int state() { return state_; }
  
  unsigned int status(unsigned int flag = 0xFFFF ) { return status_ & flag; }
  void setStatus(unsigned int flag) { status_ |= flag; }
  void unsetStatus(unsigned int flag) { status_ &= ~flag; }
  
  void action(short int n) { action_ = n; }
  short int action() { return action_; }
  
  void nextPeriod(short int n) { nextPeriod_ = n; }
  short int nextPeriod() { return nextPeriod_; }

  double periodStartTime() { return periodStartTime_; }
  void periodStartTime(double t) { periodStartTime_ = t; }

  void initializationTime(double t) { initializationTime_ = t; }
  double initializationTime() { return initializationTime_; }

  void nPrecedingGroups(short int i) { nPrecedingGroups_ = i; }
  short int nPrecedingGroups() { return nPrecedingGroups_; }

  void precedingGroups(short int i, short int n) { precedingGroups_[i] = n; }
  short int precedingGroups(short int i) { return precedingGroups_[i]; }
  void createPrecedingGroups() { 
    precedingGroups_ = new short int[nPrecedingGroups_];
  }

  double gapTimer() { return gapTimer_; }
  void gapTimer(double t) { gapTimer_ = t; }
  
  short int checkinCounter() { return checkinCounter_; }
  void checkinCounter(short int i) { checkinCounter_ = i; }
  void incrementCheckinCounter() { checkinCounter_ ++; }
  void decrementCheckinCounter() { 
    if ( checkinCounter_ > 0 )
      checkinCounter_ --; 
  }
};

#endif
