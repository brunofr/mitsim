//-*-c++-*------------------------------------------------------------
// NAME: Phase for traffic activated controller
// AUTH: Qi Yang
// FILE: TC_ActivatedPhase.h
// DATE: Sun Oct 29 14:12:55 1995
//--------------------------------------------------------------------

#ifndef TC_ACTIVATEDPHASE_HEADER
#define TC_ACTIVATEDPHASE_HEADER

#include <iostream>

#include "TC_Phase.h"

class Reader;

class TC_ActivatedPhase : public TC_Phase
{
   protected:
	
      short int extensionTime_;	// vehicle extension time
      short int minGreenTime_; // min (initial) green time
      short int maxGreenTime_; // max green time
      short int recallToMinGreenTime_; // as the name said
      short int extendedIndex_;	// index into array extendedPhase_

   public:

      TC_ActivatedPhase()
	 : TC_Phase(), extendedIndex_(-1) { }

      virtual ~TC_ActivatedPhase() { }

      virtual short int minGreenTime() {
	 return minGreenTime_;
      }
      virtual short int maxGreenTime() {
	 return maxGreenTime_;
      }
      virtual short int extensionTime() {
	 return extensionTime_;
      }

      virtual short int recallToMinGreenTime() {
	 return recallToMinGreenTime_;
      }
      virtual short int extendedIndex() {
	 return extendedIndex_;
      }
      virtual void extendedIndex(short int i) {
	extendedIndex_ = i;
      }

      virtual void read(Reader& is);

      virtual void print(std::ostream &os = std::cout);
};


#endif
