//-*-c++-*------------------------------------------------------------
// NAME: Phase for a signal controller
// AUTH: Qi Yang
// FILE: TC_Phase.h
// DATE: Fri Oct 27 13:23:12 1995
//--------------------------------------------------------------------

#ifndef TC_PHASE_HEADER
#define TC_PHASE_HEADER

#include <iostream>
#include <new>
#include <vector>

#include <Templates/Object.h>

#include "TC_Interval.h"

class TC_Interval;
class TC_Controller;
class Reader;

class TC_Phase : public CodedObject
{
   protected:

      TC_Controller *controller_;	// parent
      std::vector<TC_Interval*> intervals_; // array of signal intervals

   public:

      TC_Phase() : controller_(NULL) { }
      TC_Phase(TC_Controller *tc) : controller_(tc) {
      }
      virtual ~TC_Phase();

      void controller(TC_Controller* c) {
		 controller_ = c;
      }

      TC_Controller* controller() {
		 return controller_;
      }

      short int nIntervals() {
		 return intervals_.size();
      }
      TC_Interval* interval(short int i) {
		 return intervals_[i];
      }
      void createDefaultIntervals();
      virtual void read(Reader&);
      virtual void readSequence(Reader&, short int);

      virtual void print(std::ostream &os = std::cout);
};

#endif
