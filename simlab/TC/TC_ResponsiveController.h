//-*-c++-*------------------------------------------------------------
// NAME: Responsive ramp metering controller
// AUTH: Owen J. Chen & Qi Yang
// FILE: TC_ResponsiveController.h
// DATE: Sun Nov 10 21:55:41 EST 1996
//--------------------------------------------------------------------

#ifndef TC_RESPONSIVECONTROLLER_HEADER
#define TC_RESPONSIVECONTROLLER_HEADER

#include <iostream>
#include <vector>

#include "TC_Controller.h"
#include "TC_Sensor.h"
#include "TC_MeteringMethod.h"

class Reader;
class TC_Phase;
class TC_ControlRegion;

// This is an abstracted base class for all responsive controllers
//
// Areawide responsive controller is the same as this base.

class TC_ResponsiveController : public TC_Controller
{
  friend class TC_Phase;

public:

  enum {
	STATUS_SMALL_QUEUE    = 0x0001,
	STATUS_LARGE_QUEUE    = 0x0002,
	STATUS_OVERRIDE_QUEUE = 0x0003,
	STATUS_LOCAL_RATE     = 0x0010,
	STATUS_REGIONAL_RATE  = 0x0020,
	STATUS_MIN_RATE       = 0x0100,
	STATUS_MAX_RATE       = 0x0200
  };


protected:

  int updateStepSize_;	// step size for calc parameters

  TC_MeteringMethod *method_;

  double rate_;
  double queueMeasurement_;
  std::vector<RN_Sensor*> queueDetectors_; // ramp queue & volume sensors
  TC_ControlRegion* controlRegion_; //control Region which this 
								// controller belongs to

public:

  TC_ResponsiveController(int c, short int t) 
	: TC_Controller(c, t), method_(NULL), controlRegion_(NULL) {
  }
  ~TC_ResponsiveController() { }
  inline int updateStepSize() {	// virtual
	return updateStepSize_;
  }
  inline void setUpdateStepSize(int upstep) {
	updateStepSize_ = upstep;
  }
  inline double meteringRate() {
	return rate_;
  }
  void meteringRate(double rate);

  void initializePhases();
  TC_ControlRegion* controlRegion() { return controlRegion_;}
  void controlRegion(TC_ControlRegion* region) {
	controlRegion_ = region;
  }

  virtual void read(Reader&);
  virtual void readTimingConstraints(Reader&);
  virtual void readQueueDetectors(Reader&);
  inline short int nQueueDetectors() {
	return queueDetectors_.size();
  }

  virtual long initializeSignalStates();
  virtual long switchInterval();

  virtual void calculateParameters() { calculateParameters(-1.0); }
  virtual void calculateParameters(double regionRate);
  virtual void calculateMeteringRate(double regionRate);
  virtual int checkMeteringRateBoundary();
  void translateRatesToTiming();
  double calcQueueDetectorMeasurement();
  int calcQueueDetectorFlow();
  virtual void print(std::ostream &os = std::cout);
  virtual void printQueueDetectors(std::ostream &os = std::cout);
};

#endif
