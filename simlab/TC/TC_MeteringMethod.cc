//-*-c++-*------------------------------------------------------------
// TC_MeteringMethod.C
//
// Qi Yang and Masroor
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <iostream>
using namespace std;

#include <IO/Exception.h>
#include <Tools/ToolKit.h>
#include <Tools/Math.h>
#include <GRN/RN_Signal.h>

#include "TC_ResponsiveController.h"
#include "TC_Phase.h"
#include "TC_Interval.h"
#include "TC_LogFile.h"
#include "TC_MeteringMethod.h"

void TC_MeteringMethod::read(Reader &is)
{
   is >> minMeteringRate_ >> maxMeteringRate_ >> cycle_;
}

void TC_MeteringMethod::print(ostream &os)
{
   os << minMeteringRate_ << "\t# Maximum metering rate (vph)" << endl
	  << maxMeteringRate_ <<"\t# Minumum metering rate (vph)" << endl
	  << cycle() << "\t Initial cycle length (sec)" << endl;
}

void TC_SingleMetering::read(Reader &is)
{
   if (!is.findToken(OPEN_TOKEN)) theException->exit(1);

   TC_MeteringMethod::read(is);
   is >> green_;

   if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
}

void TC_SingleMetering::print(ostream &os)
{
   os << indent << indent
	  << METERING_METHOD_SINGLE << endc
	  << OPEN_TOKEN << "\t# Single vehcile metering" << endl;

   TC_MeteringMethod::print(os);

   os << green_ << "\t Green time" << endl
	  << indent << indent << CLOSE_TOKEN << endl;
}

void TC_SingleMetering::setSignalTiming(TC_Phase *p, double rate)
{
   const double epsilon = 1.0E-5;
   short int g;

   leftOver_ = 0.0;

   if (rate + epsilon > maxMeteringRate_) { //  shutdown metering
	  g = parent_->updateStepSize();
	  cycle_ = g;
	  isMetering_ = 0;
   } else {
	  cycle_ = 3600.0 / rate;
	  g = green_;
	  isMetering_ = 1;
   }

   // Update the parameters of signal phasing
	
   p->interval(0)->length(g);
   p->interval(1)->length(0);
   p->interval(2)->length(calcRedTime(p));

   TC_LogFile::os() << " v=" << Round(rate)
					<< " c=" << Fix(cycle_, (float)0.1)
					<< endl;
}


short int TC_SingleMetering::calcRedTime(TC_Phase *)
{
   short int r;
   if (isMetering_) {
	  float total = leftOver_ + cycle_;
	  short int c = (int) total;
	  leftOver_ = total - c;
	  r = c - green_;
   } else {
	  r = 0;
   }

   return r;
}



void TC_PlatoonMetering::read(Reader &is)
{
   if (!is.findToken(OPEN_TOKEN)) theException->exit(1);

   TC_MeteringMethod::read(is);
   is >> yellow_ >> lostTime_ >> minRed_ >> maxRed_;

   if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
}

void TC_PlatoonMetering::print(ostream &os)
{
   os << indent << indent << METERING_METHOD_PLATOON << endc
	  << OPEN_TOKEN << "\t# Platoon metering" << endl;

   TC_MeteringMethod::print(os);

   os << yellow_ << "\t# Yellow time (sec)" << endl
	  << lostTime_ << "\t# Lost time (sec)" << endl
	  << minRed_ << "\t# Minimum red time (sec)" << endl
	  << maxRed_ << "\t# Maximum red time (sec)" << endl
	  << indent << indent << CLOSE_TOKEN << endl;
}

void TC_PlatoonMetering::setSignalTiming(TC_Phase *p, double rate)
{
   const double epsilon = 1.0E-5;

   short int g, y, r;

   if (rate + epsilon > maxMeteringRate_) {
	  g = parent_->updateStepSize();
	  y = 0;
	  r = 0;
   } else {
	  double ratio = rate / maxMeteringRate_;
	  g = Round((cycle_ - lostTime_) * ratio);
	  y = yellow_;
	  r = (int) (cycle_ - g - y);
	  if (r < minRed_) {
		 r = minRed_;
	  } else if (r > maxRed_) {
		 r = maxRed_;
	  }
	  g = (int)(cycle_ - r - y);
   }

   // Update the parameters of signal phasing

   p->interval(0)->length(g);
   p->interval(1)->length(y);
   p->interval(2)->length(r);

   TC_LogFile::os() << " v=" << Round(rate)
					<< " g=" << g
					<< " r=" << r
					<< endl;
}

short int TC_PlatoonMetering::calcRedTime(TC_Phase *p)
{
   return p->interval(p->nIntervals() - 1)->length();
}
