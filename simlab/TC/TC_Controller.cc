//-*-c++-*------------------------------------------------------------
// NAME: Traffic Signal Controllers
// AUTH: Qi Yang & Owen J. Chen
// FILE: TC_Controller.C
// DATE: Wed Nov 13 22:07:31 EST 1996
//--------------------------------------------------------------------

#include <Templates/Listp.h>
#include <Tools/Math.h>

#include <Tools/SimulationClock.h>
#include <Tools/ToolKit.h>
#include <Tools/Reader.h>

#include <IO/Exception.h>

#include <GRN/NameTable.h>
#include <GRN/RoadNetwork.h>

#include "TC_Interval.h"
#include "TC_Phase.h"
#include "TC_Controller.h"
#include "TC_Signal.h"
using namespace std;


// Initialize a newly created controller and read information from
// the controller database


void
TC_Controller::read(Reader &is)
{
   is >> signalType_ >>  nEgresses_;

   startTime_ = (long) theSimulationClock->currentTime();

   if (!is.findToken(OPEN_TOKEN)) theException->exit(1);

   readSignals(is);
   readTimingTable(is);

   if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);

   switchTime_ = initializeSignalStates();
   updateTime_ = ONE_DAY;
}


// I assume there is a signal from each approach. At minimum there is
// a signal ID.  Additional info such as max red time, red called
// phase, etc for activated signal controls is implemented in derived
// class by overload this function.

void
TC_Controller::readSignals(Reader& is)
{
   short int i;
   is >> nApproaches_;
   if (!is.findToken(OPEN_TOKEN)) theException->exit(1); 
   for (i = 0; i < nApproaches_; i ++) {
      signals_.push_back(readSignal(is, i));
   }
   if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
}


RN_Signal*
TC_Controller::readSignal(Reader& is, short int)
{
   RN_Signal *s;
   int id;

   is >> id;

   if ((s = theNetwork->findSignal(id)) == NULL) {
      cerr << "Error:: Unknown signal <" << id << ">. ";
      is.reference();
      theException->exit(1);
   }

   return s;
}


// A timing table consists of one or more phase tables.

void
TC_Controller::readTimingTable(Reader& is)
{
   short int p, num;
   is >> num;			// number of phases
   phases_.reserve(num);
   if (!is.findToken(OPEN_TOKEN)) theException->exit(1); 
   for (p = 0; p < num; p ++) {
      phases_.push_back(newPhase());
      phases_[p]->controller(this);
      phases_[p]->code(p + 1);
      phases_[p]->read(is);
   }
   if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
}


// The function initialize the signal states and returns infinity
// time.  This function should be overloaded unless it is unsignalized
// controller.

long TC_Controller::initializeSignalStates()
{
   phase(0)->interval(0)->setSignalStates();
   return (ONE_DAY);
}


void
TC_Controller::print(ostream &os)
{
   os << indent << code_  << " # controller ID " << endl
      << indent << type_ << " # controller type " << endl
      << indent << signalType_  << " # signal type " << endl
      << indent << nEgresses_ << " # number of Egresses " << endl
      << indent << OPEN_TOKEN  << "\t# Code" << endc
      << NameTable::controller(type_) << endc
      << NameTable::signal(signalType_) << endl;

   printSignals(os);
   printTimingTable(os);

   os << indent << CLOSE_TOKEN << endl;
}


void
TC_Controller::printSignals(ostream &os)
{
   short int i;
   os << indent << indent << nApproaches() << endc
      << OPEN_TOKEN << endc;
   for (i = 0; i < nApproaches(); i ++) {
      os << signals_[i]->code() << endc;
   }
   os << CLOSE_TOKEN  << " # signal IDs " << endl;
}


void
TC_Controller::printTimingTable(ostream &os)
{
   short int i;
   os << indent << indent << nPhases() << endc
      << OPEN_TOKEN << "\t# NumPhases" << endl;
   for (i = 0; i < nPhases(); i ++) {
      phase(i)->print(os);
   }
   os << indent << indent << CLOSE_TOKEN << endl;
}


// This function overload the virtual function in CodedObject and used
// to sort the controllers by updateTime_.

int TC_Controller::cmp(CodedObject *other) const
{
   TC_Controller *o = (TC_Controller*) other;
   if (updateTime_ < o->updateTime_) {
	  return -1;
   } else if (switchTime_ > o->switchTime_) {
	  return 1;
   } else {
	  return 0;
   }
}


