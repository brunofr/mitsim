//-*-c++-*------------------------------------------------------------
// NAME:Mainline Bottleneck Section for Flow Coordinated Traffic
//      Signal Controllers
//
// AUTH: Owen J. Chen  
// FILE: TC_Bottleneck.h
// DATE: Tue Jan 28 15:59:26 EST 1997
//--------------------------------------------------------------------

#include <Templates/Listp.h>
#include <Tools/Math.h>

#include <Tools/SimulationClock.h>
#include <Tools/ToolKit.h>
#include <Tools/Reader.h>

#include <IO/Exception.h>

#include <GRN/NameTable.h>
#include <GRN/Constants.h>
#include <GRN/RoadNetwork.h>

#include "TC_Controller.h"
#include "TC_ControlRegion.h"
#include "TC_ControlSystem.h"
#include "TC_FlowControlRegion.h"
#include "TC_FlowResponsiveController.h"
#include "TC_Bottleneck.h"
using namespace std;


void
TC_Bottleneck::read(Reader &is)
{
  readUpstreamDetectors(is);
  readDownstreamDetectors(is);
  is >> thresholdOccupancy_;
  readMeteredRamps(is);
  readUnMeteredRamps(is);
}

void
TC_Bottleneck::readUpstreamDetectors(Reader &is)
{
  int num = 0, id;

  cout << "Read upstream mainline detectors in Bottleneck " << code() << endl; 

  is >> num;
  if (!is.findToken(OPEN_TOKEN)) theException->exit(1);  
  upstreamDetectors_.reserve(num);
  for (int i = 0; i < num; i ++) {
	is >> id;
	upstreamDetectors_.push_back(theNetwork->findSensor(id));
	if (!upstreamDetectors_[i]) {
	  cerr << "Error:: Unknown sensor <" << id << ">. ";
	  is.reference();
	  theException->exit(1);
	}
  }
  if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
}
 

void
TC_Bottleneck::readDownstreamDetectors(Reader &is)
{
  int num = 0, id;

  cout << "Read downstream mainline detectors in Bottleneck " << code() << endl; 

  is >> num;
  if (!is.findToken(OPEN_TOKEN)) theException->exit(1);  
  downstreamDetectors_.reserve(num);
  for (int i = 0; i < num; i ++) {
	is >> id;
	downstreamDetectors_.push_back(theNetwork->findSensor(id));
	if (!downstreamDetectors_[i]) {
	  cerr << "Error:: Unknown sensor <" << id << ">. ";
	  is.reference();
	  theException->exit(1);
	}
  }
  if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
}


void
TC_Bottleneck::readUnMeteredRamps(Reader &is)
{
  int id;
  short int type;
  TC_UnMeteredRamp *ramp;
  if (!is.findToken(OPEN_TOKEN)) theException->exit(1);  
  for (is.eatwhite();
       is.peek() != CLOSE_TOKEN;
	   is.eatwhite())	
	{
	  is >> id >> type;
      ramp = new TC_UnMeteredRamp(id, type);
      switch (type)
		{
		case ON_RAMP:
		  {
			onRamps_.push_back(ramp);
			cout <<"Load un-metered on-ramp ID= " << id << endl;
			ramp->read(is);
			break;
		  }
		case OFF_RAMP:
		  {
			offRamps_.push_back(ramp);
			cout <<"Load off-ramp ID= " << id << endl;
			ramp->read(is);
			break;
		  }
		default:
		  {
			cerr << "Error::unknown ramp type <" << type << ">. ";
			is.reference();
			theException->exit(1);
		  }
		}
	}
  if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);  
}
 

void
TC_Bottleneck::readMeteredRamps(Reader &is)
{
  int n;
  is >> n;
  meteredRamps_.reserve(n);
  weightingFactors_.reserve(n);

  if (!is.findToken(OPEN_TOKEN)) theException->exit(1);  

  for (int i = 0; i < n; i ++) {
	int id;
	float w;
	is >> id >> w;
	tcc_list_link_type c = TC_ControlSystem::find(id);
	if (!c) {
	  cerr << "Error:: Unknown Controller <" << id << ">. ";
	  is.reference();
	  theException->exit(1);
	}
	meteredRamps_.push_back(c->data());
	weightingFactors_.push_back(w);
  }
  if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
}
 

void
TC_Bottleneck::print(const char *msg, const vector<RN_Sensor*> &detector,
					 ostream &os)
{
  os << indent << indent << indent
     << "# " << msg << endl
     << indent << indent << indent << OPEN_TOKEN << endc;
  for (int i = 0; i < detector.size(); i ++) {
    os << detector[i]->code() << endc;
  }
  os << CLOSE_TOKEN << endl;
}

void
TC_Bottleneck::print(const char *msg, const vector<TC_UnMeteredRamp*> &ramps,
					 ostream &os)
{
  os << indent << "# " << msg << endl;
  for (int i = 0; i < ramps.size(); i ++) { 
	ramps[i]->print(os);
  }
}

void
TC_Bottleneck::print(ostream &os)
{
  os << indent << indent 
     << OPEN_TOKEN << " # Bottleneck " << code() << endl;
  print("Upstream mainline detectors", upstreamDetectors_, os);
  print("Downstream mainline detectors", downstreamDetectors_, os);
  print("Unmetered off ramps", offRamps_, os);
  print("Unmetered on ramps", onRamps_, os);
  os << indent << CLOSE_TOKEN  << " # end of a bottleneck " << endl;
}



double
TC_Bottleneck:: bottleneckOccupancy()
{
  double measure = 0.0;
  short int num = 0;
  register short int i;
  for (i = 0; i < nDownstreamDetectors(); i ++) {
	if (!(downstreamDetectors_[i]->state() & SENSOR_BROKEN)) {
	  measure += downstreamDetectors_[i]->occupancy();
	  num ++;
	}
  }
  if (num) return ( measure / num);
  else return 0.0;

}

int
TC_Bottleneck::upstreamFlow()
{
  int measure = 0;
  register int i;
  for (i = 0; i < nUpstreamDetectors(); i ++) {
	if (!(upstreamDetectors_[i]->state() & SENSOR_BROKEN)) {
	  measure += upstreamDetectors_[i]->flow();
	}
  }
  return measure;
}

int 
TC_Bottleneck::downstreamFlow()
{
  int measure = 0;
  register int i;
  for (i = 0; i < nDownstreamDetectors(); i ++) {
    if (!(downstreamDetectors_[i]->state() & SENSOR_BROKEN)) {
      measure += downstreamDetectors_[i]->flow();
    }
  }
  return measure;
}

int
TC_Bottleneck::onRampFlow()
{
  int measure = 0;
  for (int i = 0; i < onRamps_.size(); i ++) {
	measure += onRamps_[i]->calculateFlow();
  }
  return measure;
}

int
TC_Bottleneck::offRampFlow()
{
  int measure = 0;
  for (int i = 0; i < offRamps_.size(); i ++) {
	measure += offRamps_[i]->calculateFlow();
  }
  return measure;
}


int 
TC_Bottleneck::meteredRampFlow()
{
  int measure = 0;
  for (int i=0; i < meteredRamps_.size(); i++) {
	TC_ResponsiveController *c =
	  (TC_ResponsiveController *)meteredRamps_[i];
	measure += c->calcQueueDetectorFlow();
  }
  return measure;
}

void
TC_Bottleneck::calculateParameters()
{
  volumeReduction_ = 
	upstreamFlow() 
	+ onRampFlow() 
	+ meteredRampFlow()
	- downstreamFlow() 
	- offRampFlow();

  // Reduce Volume if congested and vehicles stored, increase volume
  // if ungested and vehicles dissipated.

  if (bottleneckOccupancy() > thresholdOccupancy_ &&
	  volumeReduction_ > 0.0 ||
	  bottleneckOccupancy() < thresholdOccupancy_ &&
	  volumeReduction_< 0.0 ) {
	for (int i = 0; i < meteredRamps_.size(); i ++) {
	  TC_FlowResponsiveController *c =
		(TC_FlowResponsiveController *) meteredRamps_[i];
	  c->meteringRateReduction(weightingFactors_[i]*volumeReduction_);
	}
  }
}
