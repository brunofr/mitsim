//-*-c++-*------------------------------------------------------------
// TC_LogFile.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef TC_LOGFILE_HEADER
#define TC_LOGFILE_HEADER

#include <fstream>

class TC_LogFile
{
public:
  ~TC_LogFile();
  static TC_LogFile* the(const char *filename_ = 0);
  static std::ofstream& os() { return *(the()->_os); }

private:
  static TC_LogFile* _instance;
  TC_LogFile(const char *filename);
  char *_filename;
  std::ofstream *_os;
};

#endif
