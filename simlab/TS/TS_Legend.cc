//-*-c++-*------------------------------------------------------------
// TS_Legend.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <DRN/DRN_DrawingArea.h>

#include "TS_Interface.h"
#include "TS_Symbols.h"
#include "TS_Legend.h"
#include "TS_Parameter.h"
#include "TS_VehicleLib.h"

void TS_Legend::draw(int mode)
{
   DRN_Legend::draw(mode);
}


void TS_Legend::draw_more()
{
   drawVehicleLegend();
}


void TS_Legend::drawVehicleLegend()
{
   if (!theDrawingArea->isVehicleDrawable()) return;

   theDrawingArea->foreground(theColorTable->yellow());
   offset_ += theDrawingArea->fontHeight();
   XmwPoint p(pos_.x(), pos_.y() + offset_);

   switch (theSymbols()->vehicleBorderCode().value()) {
      case TS_Symbols::VEHICLE_BORDER_INFO:
      {
		 theDrawingArea->drawLString("Path/Info Availability", p);
		 drawLongBoxSymbol(theColorTable->black(),      "Unguided");
		 drawLongBoxSymbol(theColorTable->red(),        "  w/ Path");
		 drawLongBoxSymbol(theColorTable->yellow(),     "  w/o Path");
		 drawLongBoxSymbol(theColorTable->black(),      "Guided");
		 drawLongBoxSymbol(theColorTable->cyan(),       "  w/ Path");
		 drawLongBoxSymbol(theColorTable->lightGreen(), "  w/o Path");
		 break;
      }
      case TS_Symbols::VEHICLE_BORDER_LANEUSE:
      {
		 theDrawingArea->drawLString("Lane Use", p);
		 drawLongBoxSymbol(theColorTable->lightGreen(), "HOV+ETC");
		 drawLongBoxSymbol(theColorTable->yellow(), "HOV");
		 drawLongBoxSymbol(theColorTable->cyan(), "ETC");
		 drawLongBoxSymbol(theColorTable->red(), "Others");
		 break;
      }
      case TS_Symbols::VEHICLE_BORDER_TYPE:
      {
		 theDrawingArea->drawLString("Vehicle Class", p);
		 for (int i = 0; i < theParameter->nVehicleClasses(); i ++) {
			drawLongBoxSymbol(theParameter->vehicleColors(i),
							  theParameter->vehicleLib(i)->name());
		 }
		 break;
      }
      case TS_Symbols::VEHICLE_BORDER_MOVEMENT:
      {
		 theDrawingArea->drawLString("Turn Movement", p);
		 offset_ += theDrawingArea->fontHeight();
		 char label[2];
		 label[1] = '\0';
		 for (int i = 0; i < 4; i ++) {
			label[0] = '1' + i;
			drawShortLineSymbol(theColorTable->color(0.25 * i),
								label);
		 }
		 drawShortLineSymbol(theColorTable->white(), "Arrival");
		 break;
      }
      case TS_Symbols::VEHICLE_BORDER_GROUP:
      {
		 theDrawingArea->drawLString("Driver Group", p);
		 offset_ += theDrawingArea->fontHeight();
		 char label[2];
		 label[1] = '\0';
		 int n = Min(4, theParameter->nDriverGroups());
		 int s = theParameter->nDriverGroups() / n;
		 float step;
		 if (theParameter->nDriverGroups() > 1) {
		   step = 1.0 / (float) (theParameter->nDriverGroups() - 1);
		 } else {
		   step = 1.0;
		 }
		 for (int i = 0; i < theParameter->nDriverGroups(); i += s) {
		   label[0] = '0' + i;
		   drawShortLineSymbol(theColorTable->color((float)i * step), label);
		 }
		 break;
      }
      default:
      {
		 break;
      }
   }

   if (theDrawingArea->isVehicleStateDrawable() &&
	   theSymbols()->vehicleBorderCode().value()) {
      offset_ += theDrawingArea->fontHeight();
      p.y(pos_.y() + offset_);
      theDrawingArea->foreground(theColorTable->yellow());
      theDrawingArea->drawLString("Lane Change", p);
	  offset_ += theDrawingArea->fontHeight();
      drawShortLineSymbol(theColorTable->magenta(), "Esc");
      drawShortLineSymbol(theColorTable->lightRed(), "M -");
      drawShortLineSymbol(theColorTable->orange(), "D -");
      drawShortLineSymbol(theColorTable->yellow(), "M +");
      drawShortLineSymbol(theColorTable->green(), "D +");
      drawShortLineSymbol(theColorTable->darkGray(), "Stay");
   }
}

#endif
