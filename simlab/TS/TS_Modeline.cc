//-*-c++-*------------------------------------------------------------
// TS_Modeline.cc
//
// qiyang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <assert.h>
#include <stdio.h>

#include "TS_Modeline.h"
#include "TS_Interface.h"

TS_Modeline::TS_Modeline(Widget parent) : DRN_Modeline(parent)
{
   static String help_timers[3];
   help_timers[0] = XmtLocalize2(timers_,
								 "Current time", "timers", "current");
   help_timers[1] = XmtLocalize2(timers_,
								 "Real time left", "timers", "left");
   help_timers[2] = XmtLocalize2(timers_,
								 "Sensor time", "timers", "sensor");
   XtAddCallback(timers_,
				 XmNenterCellCallback, &XmwModeline::cellEnterCB,
				 help_timers);

   counters_ = XmtNameToWidget(parent, "*counters");
   assert (counters_);

   XtVaSetValues(counters_, XmNtraversalOn, False, NULL);
   static String help_counters[4];
   help_counters[0] = XmtLocalize2(counters_,
								   "Active", "counters", "active");
   help_counters[1] = XmtLocalize2(counters_,
								   "In queue", "counters", "inqueue");
   help_counters[2] = XmtLocalize2(counters_,
								   "No path", "counters", "nopath");
   help_counters[3] = XmtLocalize2(counters_,
								   "Arrived", "counters", "arrived");
   XtAddCallback(counters_,
			   XmNenterCellCallback, &XmwModeline::cellEnterCB,
			   help_counters);
}

void TS_Modeline::updateCounter(int i, int n)
{
   static char buf[6];
   sprintf(buf, "%5d", n);
   const char *s = XbaeMatrixGetCell(counters_, 0, i);
   if (strcmp(s, buf)) {
	  XbaeMatrixSetCell(counters_, 0, i, buf);
   }
}

#endif
