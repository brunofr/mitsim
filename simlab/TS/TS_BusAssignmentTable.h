//-*-c++-*------------------------------------------------------------
// NAME: Read buses from a file
// AUTH: Qi Yang & Daniel Morgan
// FILE: TS_BusAssignmentTable.h
// DATE: Fri Nov 30 16:25:20 2001
//--------------------------------------------------------------------

#ifndef TS_BUSASSIGNMENTTABLE_HEADER
#define TS_BUSASSIGNMENTTABLE_HEADER

#include <GRN/BusAssignmentTable.h>

class TS_BusAssignmentTable : public BusAssignmentTable
{
   public:

      TS_BusAssignmentTable() : BusAssignmentTable() { }
      ~TS_BusAssignmentTable() { }

      RN_Vehicle * newVehicle();
};

#endif
