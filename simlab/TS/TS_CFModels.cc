//-*-c++-*------------------------------------------------------------
// NAME: Traffic Simulation
// AUTH: Qi Yang
// FILE: TS_CFModels.C
// DATE: Fri Aug 30 21:56:54 1996
//--------------------------------------------------------------------

#include <Tools/Math.h>

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>
#include <Tools/Random.h>

#include <GRN/Constants.h>
#include <GRN/RN_SurvStation.h>
#include <GRN/RN_CtrlStation.h>
#include <GRN/BusAssignmentTable.h>

#include "TS_Node.h"
#include "TS_Link.h"
#include "TS_Segment.h"
#include "TS_Lane.h"
#include "TS_Vehicle.h"
#include "TS_VehicleLib.h"
#include "TS_Signal.h"
#include "TS_TollBooth.h"
#include "TS_BusStop.h" //margaret
#include "TS_Incident.h"
#include "TS_Parameter.h"
#include "TS_Status.h"
#include "TS_Engine.h"
#include "TS_Network.h"
 


// tomer - adding the intersection file

#include <GRN/RN_IS.h>

// **


 typedef unsigned long MID;


// Used immediate after carFollowingRate()

int TS_Vehicle::regime_ = 0;

/*
 *-------------------------------------------------------------------
 * This function returns the acceleration rate required to
 * accelerate / decelerate from current speed to a target
 * speed within a given distance.
 *-------------------------------------------------------------------
 */

float
TS_Vehicle::brakeToTargetSpeed(float s, float v)
{
  if (s > FLT_EPSILON) {
	float v2 = v * v;
	float u2 = currentSpeed_ * currentSpeed_;
	float acc = (v2 - u2) / s * 0.5;

        return acc;
  }
  else {

	float dt = nextStepSize();
    if (dt <= 0.0) return MAX_ACCELERATION ;
	return (v - currentSpeed_) / dt;
  }
}


float
TS_Vehicle::brakeToStop(float s)
{
  if (s > FLT_EPSILON) {
  
        float u2 = currentSpeed_ * currentSpeed_;
	float acc = - u2 / s * 0.5;  

   
	if (acc <= normalDeceleration_) return acc;
    
	float dt = nextStepSize();
	float vt = currentSpeed_ * dt;
	float a = dt * dt;
	float b = 2.0 * vt - normalDeceleration_ * a;
	float c = u2 + 2.0 * normalDeceleration_ * (s - vt);
	float d = b * b - 4.0 * a * c;

	if (d < 0 || a <= 0.0) return acc;
      
	return (sqrt(d) - b) / a * 0.5;
  }
 
   else {

    	float dt = nextStepSize();
    	return (dt > 0.0) ? -currentSpeed_ / dt : MAX_DECELERATION;
    return MAX_DECELERATION;
  }
}



float
TS_Vehicle::brakeToReject(float s)
{
  if (s > FLT_EPSILON) {
  
        float u2 = currentSpeed_ * currentSpeed_;
	float acc = - u2 / (s-DIS_EPSILON/1000) * 0.5;  
	return acc;
  } 
   else {

    	float dt = nextStepSize();
   	return (dt > 0.0) ? -currentSpeed_ / dt : MAX_DECELERATION;
  }
}



float
TS_Vehicle::crowlingAcc()
{
  float dt = nextStepSize() ;
  float s = distance_/2 ;
  float acc = 2*(s-currentSpeed_*dt)/(dt*dt);
  return acc;
}


// Maximum speed for a vehicle (a function of vehicle type, and
// segment grade).

float
TS_Vehicle::maxSegmentSpeed()
{
  int i = type();
  return theParameter->vehicleLib(i)->maxSegmentSpeed(this);
}


float
TS_Vehicle::maxLaneSpeed()
{
  int i = type();
  return (theParameter->vehicleLib(i)->maxLaneSpeed(this));
}


// This function update the variables that depend only on the speed of
// the vehicle and segment that the vehcile is located.  It is called
// whenever vehicle changes it speed and enters a new segment.

void TS_Vehicle::calcStateBasedVariables()
{
  TS_VehicleLib *lib = theParameter->vehicleLib(type());

  // Acceleration rate for a vehicle (a function of vehicle type,
  // facility type, segment grade, current speed).

  maxAcceleration_ = lib->maxAcceleration(this);

  // Deceleration rate for a vehicle (a function of vehicle type, and
  // segment grade).

  normalDeceleration_ = lib->normalDeceleration(this);

  // Maximum deceleration is function of speed and vehicle class

  maxDeceleration_ = lib->maxDeceleration(this);

  // Calculate the distance required to brake to a stop with the
  // normal deceration rate at current speed.  A vehicle will not
  // decelerate unless its distance from the event is less than the
  // distance computed in this function, which is the distance
  // required to brake to a stop with the normal deceration rate at
  // current speed.

  if (currentSpeed_ > theParameter->minSpeed()) {
	distanceToNormalStop_ = DIS_EPSILON - 
	  0.5 * currentSpeed_ * currentSpeed_ / normalDeceleration_;
	if (distanceToNormalStop_ < theParameter->minResponseDistance()) {
	  distanceToNormalStop_ = theParameter->minResponseDistance();
	}
  } else {
	distanceToNormalStop_ = theParameter->minResponseDistance();
  }

  calcNextStepSize();
}


/*
 *------------------------------------------------------------------
 * Check if this driver considers the gap from an incoming vehciel
 * to be an acceptable gap for merging or crossing.
 *
 * It depends on the speed of the coming vehicle and this driver's
 * behavior parameters (see headwayBuffer() in <omodels.C>). The
 * function returns 1 if the gap is acceptable, or 0 otherwise.
 *------------------------------------------------------------------
 */

int
TS_Vehicle::isGapAcceptable(TS_Vehicle * coming)
{
  float acc_n = this->maxAcceleration();
  float speed_m = coming->currentSpeed_;
  float acc_m = coming->maxAcceleration();
  float speed_n;
  float dt;

  if (distance_ > DIS_EPSILON) {

	// maximum speed can be reached at the end of the lane.

	speed_n = currentSpeed_ * currentSpeed_ +
	  2.0 * acc_n * distance_;
	speed_n = (speed_n > SPEED_EPSILON) ? sqrt(speed_n) : 0.0;

	// shortest time required to arrive at the end of the lane

	dt = (speed_n - currentSpeed_) / acc_n;

  } else {

	speed_n = currentSpeed_;
	dt = 0.0;

  }

  // Max distance traveled by vehicle m in time dt

  float dis_m = speed_m * dt + 0.5 * acc_m * dt * dt;
  float gap_mn = coming->distance_ - dis_m - this->length();
		
  // Speed at the pridicted position

  speed_m += acc_m * dt;
  float sd = (speed_m - speed_n) * this->headwayBuffer();
  float threshold = (sd > 0.0) ? sd : 0.0;

  // check if the gap is acceptable

  if ((gap_mn > threshold)) return 1;
  else return 0;
}


/*
 *--------------------------------------------------------------------
 * The Car-Following model calculates the acceleration rate based on
 * interaction with other vehicles.  The function returns a the
 * most restrictive acceleration (deceleration if negative) rate
 * among the rates given by several constraints.
 *
 * This function updates accRate_ at the end.

 * Car following algorithm is evaluated every CFStepSize seconds,
 * or whenever some special event has set cfTimer of this vehicle
 * to 0. After each evaluation, we set the countdown clock cfTimer
 * back to nextStepSize().
 *--------------------------------------------------------------------
 */

void
TS_Vehicle::makeAcceleratingDecision(void)
{

  TS_Vehicle *front = this->vehicleAhead();

  float aZ;			/* car-following */
  float aB;			/* merging */
  float aC;			/* signal or incidents */
  float aD;			/* courtesy yielding */
  float aE;			/* connection is wrong */
  float aF;			/* should not enter next lane */
  float aG;			/* lane drop */
  float aH;			/* desired speed/Target gap acceleration */

  // tomer - adding the intersection acc/dec

  float aI;			/* intersection behavior */

  // li - adding the intersection acc/dec with intersection delay
  
  float aJ;

  // **

  float acc;			/* returned rate */
  bool No_yield = 0;
  int tight = 0;

  /* the maximum acceleration arte it can achieve, a function of the
   * roadway grade and vehicle's current speed and performance (see
   * <vehlib.h> and <vehlib.C> for details.  */
  // tomer - atemporary fix to override max.acc in beginning  


  //if (theSimulationClock->currentTime()-enterTime_>10.0) {

    acc = this->maxAcceleration();
    //  } else {
    //    acc = MAX_ACCELERATION ;
    //  }

    // aB = this->calcMergingRate();
  aC = this->calcSignalRate();
  aD = this->calcYieldingRate();
  aE = this->waitExitLaneRate();
  aF = this->waitAllowedLaneRate();
  aG = this->calcLaneDropRate();

  //#ifdef USE_TOMER_MODELS

  //  if (status(STATUS_ADJACENT)) {
  // aH = this->calcAdjacentRate();
  //}
  //else if (status(STATUS_BACKWARD)) {
  // aH = this->calcBackwardRate();
  //}
  //else if (status(STATUS_FORWARD)) {
  // aH = this->calcForwardRate();
  //} else if (flag(FLAG_COURTESY) )  //:: VR 06/07
  // {
  //  aH= this->calcAdjacentRate();
  // }
  //else if (flag(FLAG_NOSING))
  //{
  //   TS_Lane *plane;

  //  if (status(STATUS_LEFT)) {
  //  plane = lane_->left();
  //  } else if (status(STATUS_RIGHT)) {
  //      plane = lane_->right();
  //  } else {
  //    plane = lane_;			// No request for lane change 
  //  }

     TS_Vehicle* av = findFrontBumperLeader(lane_);
     aH = this->calcCarFollowingRate(av);    // CF acceleration
     aB=aH;


     //}
     //else {
     //aH = this->desiredSpeedRate(front);
     //}
  //#else

  //aH = this->desiredSpeedRate(front);

  //#endif

  // tomer - acc/dec approacing intersection to achieve the turn speed
 RN_IS* intersection = getApproachingIS();

 int myMovement = -1;
 
 if (intersection ){
   
   myMovement = getMovement(intersection);
   aI = this->calcIntersectionRate(intersection , myMovement);
   if(this->get_no_conflict()== 1)
     aI = this->maxAcceleration();
   // deepak  
   CtrlList device = nextCtrl_;
   float vis =  theParameter->visibility();
   float dis;
   while (device &&
	  (dis = distanceFromDevice(device)) < vis ){   
     //if(device)
     if (dis <= (*device)->visibility()){   
       // {
       if((*device)->isLinkWide()){
	 RN_Signal *signal = (*device)->signal(0);
	 if(signal){
           int s = signal->stateForExit(link()->signalIndex(this->nextLink()));
	   // if((signal->state() & SIGNAL_ARROW) == SIGNAL_ARROW){
 	   if((s & SIGNAL_ARROW) == SIGNAL_ARROW){
	     aI = this->maxAcceleration();
	     this->set_no_conflict(1);
	     No_yield = 1;
	   }
	 }
       }
       /*else{
	 int n = (*device)->nLanes();
	 for (int i = 0; i < n; i++) {
	 RN_Signal *signal = (*device)->signal(i);
	 if(signal)
	 {
	 if((signal->state() & SIGNAL_ARROW) == SIGNAL_ARROW){
	 aI = this->maxAcceleration();
	 //RN_Link *present_link;
	 //present_link->lable = link();
	 No_yield = 1;
	 }
	 }
	 }
	 }*/
     }
     device = device->next();
   }

   // tomer - check if intersection is tight acc/dec

  if (aI < acc) {
	acc = aI;
	tight = 5;
  }
 }

#ifdef USE_IS_DELAY
  // li -  vehicle approaching the intersection with intersection delay


 if ( stoppingDistance(desiredSpeed_)>=distanceFromDownNode()) {

//   cout<<"intersection" <<intersection<<endl;
   
//   myMovement = getMovement(intersection);
   aJ = this->IntersectionDelayRate(front);
   if(this->get_no_conflict()== 1)
     aJ = this->maxAcceleration();
   // deepak  
   CtrlList device = nextCtrl_;
   float vis =  theParameter->visibility();
   float dis;
   while (device &&
	  (dis = distanceFromDevice(device)) < vis ){   
     //if(device)
     if (dis <= (*device)->visibility()){   
       // {
       if((*device)->isLinkWide()){
	 RN_Signal *signal = (*device)->signal(0);
	 if(signal){
           int s = signal->stateForExit(link()->signalIndex(this->nextLink()));
	   // if((signal->state() & SIGNAL_ARROW) == SIGNAL_ARROW){
 	   if((s & SIGNAL_ARROW) == SIGNAL_ARROW){
	     aJ = this->maxAcceleration();
	     this->set_no_conflict(1);
	     No_yield = 1;
	   }
	 }
       }
     }
     device = device->next();
   }

   // tomer - check if intersection is tight acc/dec

  if (aJ < acc) {
	acc = aJ;
	tight = 5;
  }
 } 
#endif
  // **

 /* Use the smallest */

  if (aB < acc) acc = aB;
  if (aD < acc) acc = aD;

  if (aF < acc) acc = aF;

  if (aH < acc) acc = aH;

  if (aG < acc) {
	acc = aG;
	tight = 2;
  }
  if (aC < acc) {
	acc = aC;
	tight = 3;
  }
  if (aE < acc) {
	acc = aE;
	tight = 4;
  }


  // This has to be called at the last step

  aZ = this->calcCarFollowingRate(front);

  if (aZ < acc) {		// car-following

        acc = aZ;
	tight = 1;

  }


  // tomer - check acc/dec for gap acceptance and rejection. here is 
  // where the driver creates the gap for himself if he can
  
  float startLooking = 5.0;

  if(No_yield == 0 && (this->get_no_conflict()==0)){
  if (intersection &&
      myMovement >= 0 ) {
  
   int gapState = acceptableGap (acc , myMovement);

   if (gapState < 2) {

     float myTime = timeToIntersection (acc);
   
     if (myTime < startLooking && ImFirst()) {

       if ( gapState==0 ) {
    
	 acc = Min( acc , brakeToReject(distance_) );
       }
     
//       else if (acc>accRate_ &&
//		acceptableGap(accRate_ , myMovement) && 
//		accRate_>ACC_EPSILON) {
//    acc=accRate_;
     }
    tight=5;
   }

   else if (gapState==3) {

     acc = Min( acc , crowlingAcc() );

   }
  }
}


  // Check the maximum deceleration

  // tomer - temporary thing for max dec.

  if (!tight && acc < maxDeceleration_ 
      //&& ( theSimulationClock->currentTime()-enterTime_>10.0)
) {
	acc = maxDeceleration_;
  }

  accRate_ = acc;

  if (accRate_ < -ACC_EPSILON) {

	// I am braking, alert the vehicle behind if it is close

	TS_Vehicle *back = findFrontBumperFollower(lane_);
	if (back &&
		back->gapDistance(this) < theParameter->visibility() &&
                !(back->isBus() && back->status(STATUS_STOPPED))) {
	  float alert = CF_CRITICAL_TIMER_RATIO * updateStepSize(0);
	  back->cfTimer_ = Min(alert, back->cfTimer_);
	}
  }

  if (status(STATUS_STOPPED)) {

	// Stopped at toll booth, timer will be set by delay function

	return;
  }
   
  if (status(STATUS_REGIME_EMERGENCY) || getApproachingIS() ) {
	cfTimer_ = nextStepSize() * CF_CRITICAL_TIMER_RATIO;
  } else {
	cfTimer_ = nextStepSize();
  }
}


float
TS_Vehicle::calcCarFollowingRate(TS_Vehicle *front)
{

  status_ &= ~(STATUS_REGIME);
  regime_ = 0;
  float acc;
  if (isInMergingArea()) {

	// In the merging area, vehicles are allowed to overlap.  This
	// is a temporary fix to overcome the limitation of discrete
	// lane representation.

	if (front) {
	 
	  // Instead of following the front, it follows the
	  // front->leading_.

	  float acc1 = this->carFollowingRate(front->leading_);

	  // In the worse case, the rate is given by following the
	  // front

	  float acc2 = this->carFollowingRate(front);

	  if (acc2 < acc1) {	// relaxation is in effect

		// Overlap may occur but may sure no overtaking.

		float dt = nextStepSize();
		float v0 = front->currentSpeed_;
		float v1 = v0 + dt * front->accRate_;
		float dx = gapDistance(front) + 0.5 * (v0 + v1) * dt;

		acc2 = brakeToTargetSpeed(dx - 0.25 * front->length(), v1);
		acc = Min(acc1, acc2);

	  } else {		// no problem if following front

		acc = acc2;
	  }
	} else {
	  acc = MAX_ACCELERATION;
	}
  } else {
	unsetFlag(FLAG_MERGING);
	acc = this->carFollowingRate(front);
  }

  status_ |= regime_;
  return acc;
}

/*
 *--------------------------------------------------------------------
 * Calculate acceleration rate by car-following constraint. This
 * function may also be used in the lane changing algorithm to find
 * the potential acceleration rate in neighbor lanes.
 *
 * CAUTION: The two vehicles concerned in this function may not
 * necessarily be in the same lane or even the same segment.
 *
 * A modified GM model is used in this implementation, but I am not
 * sure with its performance for a vehicle which is close to a front
 * vehicle and can still accelerate. We need extensive calbrating 
 * work or we may want to implement other empirical car-following
 * models (e.g. the one used in INTRAS/FRESIM).
 *--------------------------------------------------------------------
 */

float
TS_Vehicle::carFollowingRate(TS_Vehicle *front)
{
  if (!front) {
	regime_ = STATUS_REGIME_FREEFLOWING;
	return (MAX_ACCELERATION);
  }

  TS_Parameter& p = *theParameter;
  float acc;

  // float space = gapDistance(front) - headwayBuffer() * currentSpeed_;
  float space =  gapDistance(front);
  // float speed = currentSpeed_ + maxAcceleration_ * nextStepSize();
  float speed = currentSpeed_ == 0 ? 0.00001:currentSpeed_;

  float headway = 2.0 * space / (speed + currentSpeed_);
  float hupper ;

  float dens = tsSegment()->density();

  hupper = p.hUpper(driverGroup.hUpper) ;

  if (headway < p.cfLower()) { 
    float dv = currentSpeed_ - front->currentSpeed_;
// Original emergency regime acceleration model.
    if (dv < SPEED_EPSILON) {	// the leader is decelerating
      acc = front->accRate_ + 0.25 * normalDeceleration_;
    } else {
      if (space > 0.01) {
	acc = front->accRate_ - 0.5 * dv * dv / space;
      } else {
	float dt = nextStepSize();
	float v = front->currentSpeed_ + front->accRate_ * dt;
	space += 0.5 * (front->currentSpeed_ + v) * dt;
	acc = brakeToTargetSpeed(space, v);
      }
    }
    acc = Min(normalDeceleration_, acc);
// Attempt at improving emergency regime acceleration model.
// - not used as it causes problem in which close-following vehicle
//    brakes suddenly when its leader enters a new link (Angus)
//
//     if (dv < SPEED_EPSILON) { // leader is faster
//    
//       if (front->accRate_ > ACC_EPSILON) { // leader is accelerating
// 	acc = 0.9 * front->accRate_;
//       } else { // leader is decelerating
// 	acc = 1.0 * front->accRate_;
//       }
//
//     } else { // leader is slower
//
//       if (space > 0.01) { // gap exists
//    	acc = front->accRate_ - 0.5 * dv * dv / space;
//       } else { // vehicles overlap
//     	float dt = nextStepSize();
//     	float v = front->currentSpeed_ + front->accRate_ * dt;
//     	space += 0.5 * (front->currentSpeed_ + v) * dt;
//     	acc = brakeToTargetSpeed(space, v);
//       }
//     }
    regime_ = STATUS_REGIME_EMERGENCY;
  } else if (headway > hupper) { // desired speed model will do
	if (space > distanceToNormalStop_) {
	  acc = MAX_ACCELERATION;
	  regime_ = STATUS_REGIME_FREEFLOWING;
	} else {
	  float dt = nextStepSize();
	  float v = front->currentSpeed_ + front->accRate_ * dt;
	  space += 0.5 * (front->currentSpeed_ + v) * dt;
	  acc = brakeToTargetSpeed(space, v);
	  regime_ = STATUS_REGIME_CARFOLLOWING;
	}
  } else {			//In car-following mode, using GM model
	float dv = front->currentSpeed_ - currentSpeed_;
	if (dv < 0) {				// decelerate
	  acc = p.cfAlphaDec() * 
		pow(speed, p.cfBetaDec()) / pow(space, p.cfGammaDec());
	  acc *= pow(-dv, p.cfLambdaDec()) * pow(dens, p.cfRhoDec()) ;
	  acc += theParameter->cfDecAddOn(driverGroup.cfDecAddOn) ;
	  //	  acc = Min(0,acc); // must be negative (Angus)-not used

	} else if (dv > 0) {		// accelerate
	  acc = p.cfAlphaAcc() *
		pow(speed, p.cfBetaAcc()) / pow(space, p.cfGammaAcc());
	  acc *= pow(dv, p.cfLambdaAcc()) * pow(dens, p.cfRhoAcc()) ;
	  acc += theParameter->cfAccAddOn(driverGroup.cfAccAddOn) ;

#ifdef USE_TOMER_MODELS
		  
	  if (status(STATUS_BACKWARD)) { acc = MAX_ACCELERATION; }
#endif

	  //	  acc = Max(0,acc); // must be positive (Angus)- not used
	} else {					// uniform speed
	  acc = 0.0;
	}
	regime_ = STATUS_REGIME_CARFOLLOWING;
  }

  return acc;
}

// li - the intersection acc/dec with delay
#ifdef USE_IS_DELAY
float
TS_Vehicle::IntersectionDelayRate(TS_Vehicle *front)
{
   TS_Parameter& p = *theParameter;
   
  if (!front) {
	regime_ = STATUS_REGIME_FREEFLOWING;
	return (p.cfThetaAcc() * MAX_ACCELERATION);
  }
 
  float acc;
  float space = gapDistance(front);

  float speed = currentSpeed_ ==0 ? 0.00001:currentSpeed_;

  float headway = 2.0 * space / (speed+currentSpeed_);
  float hupper;
  float dens = tsSegment()->density();

  hupper = p.hUpper(driverGroup.hUpper);

  if (headway < p.cfLower()) { 
    float dv = currentSpeed_ - front->currentSpeed_; 

// Original emergency regime acceleration model.
    if (dv < SPEED_EPSILON) {	// the leader is decelerating
      acc = front->accRate_ + 0.25 * normalDeceleration_;
    } else {
      if (space > 0.01) {
	acc = front->accRate_ - 0.5 * dv * dv / space;
      } else {
	float dt = nextStepSize();
	float v = front->currentSpeed_ + front->accRate_ * dt;
	space += 0.5 * (front->currentSpeed_ + v) * dt;
	acc = brakeToTargetSpeed(space, v);
      }
    }
    acc = Min(normalDeceleration_, acc);
  } else if (headway > hupper) { // desired speed model will do
	if (space > distanceToNormalStop_) {
	  acc = p.cfThetaAcc() * MAX_ACCELERATION;
//	  cout<< acc << " free flow " << MAX_ACCELERATION << endl;
	  regime_ = STATUS_REGIME_FREEFLOWING;
	} else {
	  float dt = nextStepSize();
	  float v = front->currentSpeed_ + front->accRate_ * dt;
	  space += 0.5 * (front->currentSpeed_ + v) * dt;
	  acc = brakeToTargetSpeed(space, v);
	  regime_ = STATUS_REGIME_CARFOLLOWING;
	}
  } else {			//In car-following mode, using GM model
	float dv = front->currentSpeed_ - currentSpeed_;
	if (dv < 0) {		// decelerate with intersection delay
	  acc = p.cfThetaDec() * p.cfAlphaDec() * 
		pow(speed, p.cfBetaDec()) / pow(space, p.cfGammaDec());
	  acc *= pow(-dv, p.cfLambdaDec()) * pow(dens, p.cfRhoDec()) ;
	  acc += p.cfDeltaDec() * theParameter->cfDecAddOn(driverGroup.cfDecAddOn) ;
	  //	  acc = Min(0,acc); // must be negative (Angus)-not used
//	  cout<< acc << " car following dec" << acc/p.cfThetaDec() << endl;

	} else if (dv > 0) {	// accelerate with intersection delay
	  acc = p.cfThetaAcc() * p.cfAlphaAcc() *
		pow(speed, p.cfBetaAcc()) / pow(space, p.cfGammaAcc());
	  acc *= pow(dv, p.cfLambdaAcc()) * pow(dens, p.cfRhoAcc()) ;
	  acc += p.cfDeltaAcc() * theParameter->cfAccAddOn(driverGroup.cfAccAddOn) ;
//	  cout<< acc << " car following acc" << acc/p.cfThetaAcc() << endl;

#ifdef USE_TOMER_MODELS
		  
	  if (status(STATUS_BACKWARD)) { acc = MAX_ACCELERATION; }
#endif

	  //	  acc = Max(0,acc); // must be positive (Angus)- not used
	} else {					// uniform speed
	  acc = 0.0;
	}
	regime_ = STATUS_REGIME_CARFOLLOWING;
  }

  return acc;
}
#endif


/*
 *-------------------------------------------------------------------
 * Returns the acceleration rate constrained by the desired speed.
 *-------------------------------------------------------------------
 */


float
TS_Vehicle::desiredSpeedRate(TS_Vehicle *front)
{
//  float maxspd = Min(desiredSpeed_, lane_->maxSpeed())
  //  Note: lane_->maxSpeed() = (segment free-flow speed)*(lane speed ratio)
  //   This was found to be too restrictive a cap on the desired speed. (Angus)
  float maxspd = desiredSpeed_;
  if (currentSpeed_ < maxspd - SPEED_EPSILON) {
	// Use maximum accelerating which will be calculated in the
	// function checkCarFollowing()
	return MAX_ACCELERATION;
  } else if (currentSpeed_ > maxspd + SPEED_EPSILON) {
	// Decelerate
	return this->normalDeceleration();
  } else {
	// Keep current speed.
	return 0.0;
  }
}

// Check if the vehicle is in merging area
// - modified to eliminate merging areas before signalized intersections
//    and intersections with priority movements defined (Angus)

int
TS_Vehicle::isInMergingArea()
{
  CtrlList device = nextCtrl_; // (Angus)

  if (lane_->nUpLanes() > 1 && // upstream connected to multi-lanes
	  lane_->length() < distance_ + theParameter->dnMergingArea()) {

	// This vehicle is in the lower part of the merging area

	setFlag(FLAG_MERGING);

	return 1;

  } else if (nextLane_ 	// not a lane drop or at the boundary
	     && nextLane_->nUpLanes() > 1  // merging area
	     && distance_ < theParameter->upMergingArea()
	     && !(getApproachingIS())  // uncontrolled intersection ahead (Angus)
	     && ( !device // no control ahead or in same segment
		  || distanceFromDevice(device)>distance_ ) ) {
     
	// This vehicle is in the upper part of the merging area

	if (flag(FLAG_MERGING)) return 1; // already tagged
       
	// count number of vehciles in the merging area

	int cnt = 0;
	int maxcnt = theParameter->nVehiclesAllowedInMergingArea();
	float range = theParameter->dnMergingArea();
	TS_Vehicle *pv = nextLane_->lastVehicle();
	while (cnt < maxcnt &&
		   pv &&
		   pv->lane()->length() < pv->distance() + range) {
	  cnt ++;
	  pv = pv->leading();
	}

	// if downstream lane still have capacity, tag this vehicle as
	// merging

	if (cnt < maxcnt) {
	  setFlag(FLAG_MERGING);
	  return 1;
	}
  }

  return 0;
}


/*
 *-------------------------------------------------------------------
 * Calculate the acceleration rate by merging constraint.
 *-------------------------------------------------------------------
 */

float
TS_Vehicle::calcMergingRate()
{
  if (!nextLane_ || nextLane_->nUpLanes() <= 1 ||
	  distance_ > distanceToNormalStop()   || 
      //      lane_->linkType() == LINK_TYPE_URBAN ) { // tomer
          !(isInMergingArea()) ) { // only applies in merging areas (Angus)
      return MAX_ACCELERATION;
  }
 
  float acc;

  TS_Vehicle *first, *second;

  // TS_Vehicles from freeways and on ramps have different
  // priority.  Seperate procedures are applied.
 
  if (lane_->linkType() == LINK_TYPE_FREEWAY) {
	first = vehicleAheadInTypedLanes(LINK_TYPE_FREEWAY);
	if (first &&
		first->distance_ < distance_) {

	  // Competitor is closer to nextLane_.

	  acc = first->accRate_ + brakeToTargetSpeed(
												 distance_, first->currentSpeed_);

	} else {

	  // This is the first vehicle on freeway.  Do not worry about
	  // others.

	  acc = MAX_ACCELERATION;
	}

  } else {			/* Non-freeway */

	// For vehicle from on-ramp, also find the second vehcile in all
	// upstream lanes.

	second = vehicleBehindInTypedLanes(LINK_TYPE_FREEWAY);

	if (second) {				// a vehicle exists on upstream freeway

	  if (second->distance_ < distance_ ||
		  second->currentSpeed_ > currentSpeed_ + maxAcceleration_ ||
		  !theRandomizer->brandom(theParameter->aggresiveRampMergeProb())) {

		// she is ahead or her speed is much higher than mine or I
		// am not aggresive enough

		if (!isGapAcceptable(second)) {

		  // The gap is not acceptable. Prepare to stop.

		  return brakeToStop(distance_);
		}
	  }
	}

	first = vehicleAheadInTypedLanes(0);

	if (first) {

	  acc = brakeToTargetSpeed(distance_, first->currentSpeed_);

	} else {

	  acc = MAX_ACCELERATION;

	}
  }

  return acc;
}

/*
 *-------------------------------------------------------------------
 * If a vehicle is not in the correct lane leading to its
 * destination close to the end of a link, it may decelerate to a
 * stop and wait for lane changing.
 *-------------------------------------------------------------------
 */

float
TS_Vehicle::waitExitLaneRate()
{
  float dx = distanceFromDownNode() - 5.0;

  if (status_ & STATUS_CURRENT_OK ||
	  !(status_ & STATUS_LEFT_OK) &&
	  !(status_ & STATUS_RIGHT_OK) ||
	  dx > distanceToNormalStop())
	{
      return (MAX_ACCELERATION);
	} else if (dx < length() &&
			   flag(FLAG_NOSING) &&
			   flag(FLAG_NOSING_FEASIBLE)) {
	  return (MAX_ACCELERATION);
	} else {
	  return brakeToStop(dx);
	}
}


/*
 *-------------------------------------------------------------------
 * If current lane is incorrect and the distance is close to the an
 * incurrent lane of the segment, the vehicle decelerates to a stop
 * and wait for lane changing.
 *
 *-------------------------------------------------------------------
 */

float
TS_Vehicle::waitAllowedLaneRate()
{
  TS_Lane *pl;
  if (attr(ATTR_GLU_RULE_COMPLY) &&
	  lane_->doesNotAllow(this)) {
	if ((pl = lane_->left()) && !pl->doesNotAllow(this) ||
		(pl = lane_->right()) && !pl->doesNotAllow(this)) {
	  if (flag(FLAG_NOSING)) {
		return maxAcceleration_;
	  } else {
		return normalDeceleration_;
	  }
	}
  }

  float dx = distance_ + length();
  if (!nextLane_ ||
	  !attr(ATTR_GLU_RULE_COMPLY) ||
	  !(nextLane_->doesNotAllow(this)) ||
	  dx > distanceToNormalStop()) {
	return MAX_ACCELERATION;
  }
  if (flag(FLAG_NOSING)) {
	return maxAcceleration_;
  } else {
	return brakeToStop(dx);
  }
}


/*
 * If next lane is NULL and next link !NULL then stop and wait for 
 * merging into mainstream.
 */ 

float
TS_Vehicle::calcLaneDropRate()
{
  float dx = distance_ - DIS_EPSILON;
  if (nextLane_ ||		// knows where to go
	  dx > distanceToNormalStop() || // not close enough
	  !(segment()->downstream()) && // the last segment
	  (!nextLink_ ||
	   !theNetwork->isNeighbor(link(), nextLink_))) { // got lost
	return MAX_ACCELERATION;
  }
  return brakeToStop(dx);
}


/*
 *-------------------------------------------------------------------
 * Calculate acceleration rate required to yield or stop at a
 * downstream traffic signal. This function searches all the
 * downstream control devices or events within the maximum visible
 * distance. A vehicle only response to a VISIBLE signals and
 * message. The function returns the acc/deeleration rate according
 * to the "critical" devices.
 *-------------------------------------------------------------------
 */

float
TS_Vehicle::calcSignalRate()
{
  CtrlList device = nextCtrl_;
  float vis =  theParameter->visibility();

  float busvis = theBaseParameter->busToStopVisibility();
  float minacc = MAX_ACCELERATION;
  float dis, acc = MAX_ACCELERATION;
  float epsilon = 0.5 * DIS_EPSILON;

  unsigned int signature = 1 << lane_->localIndex();

  while (device && (dis = distanceFromDevice(device)) < vis) {

	if (dis <= (*device)->visibility()) {

	  if ((*device)->isLinkWide()) {
		RN_Signal *signal = (*device)->signal(0);
		if (signal) {
		  acc = signal->acceleration( this, dis - epsilon);
		  minacc = Min(minacc, acc);
		}

	  } else {    
	        int n = (*device)->nLanes();
	        for (int i = 0; i < n; i ++) {
	          RN_Signal *signal = (*device)->signal(i);
	          if (signal && (signal->lane()->isConnected(signature)
                        || (!isBus() && (*device)->type() == CTRL_BUSSTOP))) {

		    if (isBusWithAssignment() && !busStopIsOnRoute(signal->code())) {
		      minacc = MAX_ACCELERATION;
		    } else if (!isBus() && (*device)->type() == CTRL_BUSSTOP) {
                      acc = signal->squeeze(this, dis - epsilon);
                      minacc = Min(minacc, acc);
                    } else {
	              acc = signal->acceleration( this, dis - epsilon);
	     	      minacc = Min(minacc, acc);
		    }
	          }
	        }
	  }
	}

    device = device->next();
  }
  return (minacc);
}


/*
 *--------------------------------------------------------------------
 * Calculate the acceleration rate by courtesy yielding constraint.
 * This function chech two cases:
 *   a. If the vehicle is yielding to another vehicle, it follows
 *      that vehicle and subjects to normal deceleration rate
 *   b. If the vehicle is nosing, it follows the leader of the
 *      yielding vehicle. 
 * It may also cancel the yielding or nosing state if they becomes
 * invalid.
 *--------------------------------------------------------------------
 */

float
TS_Vehicle::calcYieldingRate()
{
  TS_Lane *olane;
  TS_Vehicle *other;
  float acc;

  if (flag(FLAG_YIELDING)) {

	// Make sure a vehicle will not yield infinitely.

	if (theSimulationClock->currentTime() - yieldTime_ >
		theParameter->lcYieldingMaxTime()) {
	  yieldVehicle_ = NULL;
	  unsetFlag(FLAG_YIELDING);
	  return MAX_ACCELERATION;
	}

	// This vehicle is yielding to another vehicle

	if (flag(FLAG_YIELDING_RIGHT)) {
	  if ((olane = lane_->right()) &&
		  (other = findFrontBumperLeader(olane)) &&
		  other == yieldVehicle_ &&
		  other->flag(FLAG_NOSING_LEFT)) {
		acc = carFollowingRate(other);
		if (acc < normalDeceleration_) {
		  acc = normalDeceleration_;
		} else if (acc > 0) {
		  acc = 0.0;
		}
		return acc;
	  }
	} else if (flag(FLAG_YIELDING_LEFT)) {
	  if ((olane = lane_->left()) &&
		  (other = findFrontBumperLeader(olane)) &&
		  other == yieldVehicle_ &&
		  other->flag(FLAG_NOSING_RIGHT)) {
		acc = carFollowingRate(other);
		if (acc < normalDeceleration_) {
		  acc = normalDeceleration_;
		} else if (acc > 0) {
		  acc = 0.0;
		}
		return acc;
	  }
	}

	yieldVehicle_ = NULL;
	unsetFlag(FLAG_YIELDING);

	return MAX_ACCELERATION;

  } else if (flag(FLAG_NOSING)) {
      
	// This vehicle is nosing

	if (flag(FLAG_NOSING_RIGHT)) {
	  if ((olane = lane_->right()) &&
		  (other = findFrontBumperFollower(olane)) &&
		  other->flag(FLAG_YIELDING_LEFT)) {
		acc = calcCreateGapRate(other->vehicleAhead(),theParameter->lcMinGap(2) + DIS_EPSILON);
		return Max(maxDeceleration_, acc);
	  }
	} else if (flag(FLAG_NOSING_LEFT)) {
	  if ((olane = lane_->left()) &&
	      (other = findFrontBumperFollower(olane)) &&
	      (other->flag(FLAG_YIELDING_RIGHT)))
	         {
		 acc = calcCreateGapRate(other->vehicleAhead(),	theParameter->lcMinGap(2) + DIS_EPSILON);
		return Max(maxDeceleration_, acc);
	  }
	}

	if (status(STATUS_CHANGING) &&
		flag(FLAG_LC_FAILED_LEAD)) {
	  return normalDeceleration_;
	} else {
	  return MAX_ACCELERATION;
	}

  } else {

	// Currently this vehicle is neither yielding, nor nosing.

	return MAX_ACCELERATION;
  }
}


// Calculate the maximum acceleration rate subject to the the gap from
// the leading vehicle.

float
TS_Vehicle::calcCreateGapRate(TS_Vehicle *front, float gap)
{
  // No vehicle ahead, this constraint does not apply

  if (!front) return MAX_ACCELERATION;

   // freedom left

  float dx = gapDistance(front) - gap;
  float dv = currentSpeed_ - front->currentSpeed_;

  float dt = nextStepSize();
  if (dt <= 0.0) return MAX_DECELERATION ;
  return front->accRate_ + 2.0 * (dx - dv * dt) / (dt * dt);

#ifdef OUT
  if (dx < 0.01 || dv < 0.0) {

	// insufficient gap or my speed is slower than the leader

	float dt = nextStepSize();
	return front->accRate_ + 2.0 * (dx - dv * dt) / (dt * dt);

  } else {

	// gap is ok and my speed is higher.

	return front->accRate_ - 0.5 * dv * dv / dx;
  }
#endif
   
}




// tomer


  // tomer - function that sets the value of a vehicle as approaching an 
  //  intersection.

  void
  TS_Vehicle::setApproachingIS() {

    isApproachingIS_=link()->dnNode()->getInterS();
    impatienceTimerStart_=0;
    impatienceTimer_=0;
  } 


  RN_IS*
  TS_Vehicle::getApproachingIS() {

    if ( stoppingDistance(desiredSpeed_)>=distanceFromDownNode()) {
      
      if ( impatienceTimerStart_>0 ) {
	impatienceTimer_=theSimulationClock->currentTime()-impatienceTimerStart_;
      }
      else if (currentSpeed_<SPEED_EPSILON) {
	impatienceTimerStart_ =theSimulationClock->currentTime();
      }
	
      return isApproachingIS_ ;
    } else {
      return NULL ;
    }
  }   


    // tomer - function that gets the vehicles movement in the intersection
    // returns the movement id (index in the array of movements)
    
    int 
    TS_Vehicle::getMovement(RN_IS* intersection){  
 typedef unsigned long MID ;
 RN_IS* n=intersection;
	int a=lane_->code_;

	if (!nextLane_) { // vehicle is lost (Angus)
	  return (-1);
	}

	int b=nextLane_->code_;
	MID c= (a << 16) | b ;
	for (int i=0; i<n->NumOfManuvers(); i++){
	  if (c==n->getManuver(i)->getID()){
	       return i;
	  }
	}
	return (-1);
	
    }
    // **


       // tomer - this function gets a pointer to the first vehicle in a 
    // conflicting lane

     TS_Vehicle* 
     TS_Vehicle::getFirst(int i) {
	TS_Lane* plane = (TS_Lane*) tsNetwork()->findLane(i);
	TS_Vehicle* pv = plane->firstVehicle();
	return (pv);
      } 


    // **


    
    // tomer - this function finds the conflicting lanes to a movement
    // stores them in a vector and gets reed of duplications
   
    vector <int>
    TS_Vehicle::conflictLanes(int i){
      typedef unsigned long MID; 
      int k=0;
      vector<int> confLanes;
      CManuver* myMove=isApproachingIS_->_manuvers[i];

      for ( int j = 0; j<myMove->NumOfConfs() ; j++ ) 
      { 
        int duplicate=0;
	for( int m = 0 ; m<k ; m++)
	{
	  if (confLanes[m]==myMove->uplane(myMove->getConfs(j)))
	  {
	    duplicate=1;
	    break;
	  }
	} 
	
	if (duplicate==0)
	{ 
	 MID a=myMove->getConfs(j);
	 int b=myMove->uplane(a);
	 confLanes.push_back (b);
	 k++;
	}
      }
      confLanes.push_back (-1);
      return confLanes;
    }

 




    // tomer - this function returns the movement of the conflicting vehicle

      MID 
      TS_Vehicle::firstMove(int i) { 
      
      RN_IS* intersection=isApproachingIS_;

      for ( int j=0; j<intersection->NumOfManuvers(); j++){
	
	int uplane = intersection->_manuvers[j]->uplane();
	int dnlane = intersection->_manuvers[j]->dnlane();
	
	if (uplane==i && 
	    dnlane==destLane(getFirst(i)))
	  {
	      return intersection->_manuvers[j]->getID();
	  }
      }

    return (0);
      }
  

    // ** 



    //tomer - this function checks if the vehicle in the conflicting
    //lane does conflict. i and j are my movement and the conflicting one

      int 
      TS_Vehicle::isConflict(MID j) {
      
      
	if (j==0) {return 0; }
      
      int k = 0;
      int i = getMovement(isApproachingIS_);

      if (i<0) {return 0; } // movement unknown (vehicle lost, e.g.) (Angus)

      int n = isApproachingIS_->_manuvers[i]->NumOfConfs();
      while (k<n) { 
	if (isApproachingIS_->_manuvers[i]->getConfs(k)==j) { return (1);}
      k++;
      }





  //      while (isApproachingIS_->_manuvers[i]->getConfs(k)) {
  //   if (isApproachingIS_->_manuvers[i]->getConfs(k)==j) { return (1);}
  //      k++;
  //      }
    return (0);
      }


    // **


    // tomer - finding the time it will take a vehicle
    // to arrive at the intersection under current speed and accelaration


    float
    TS_Vehicle::timeToIntersection() {
      
      float t;
      
      if (currentSpeed_ <= SPEED_EPSILON && accRate_<= ACC_EPSILON) {

	//	if (distance_<= 2*DIS_EPSILON) {
	//	t = 0.5 ;
	// }
      //   else if (currentSpeed_<=SPEED_EPSILON){
      //
       t = 100;     // again a large number
      }
      else {
	t = distance_/currentSpeed_;
      }
      return (t);
    }


    // tomer - second function for the current vehicle with 
    //specific acceleration 
  
    float 
    TS_Vehicle::timeToIntersection(float acc) {
      
      float t;
      if (distance_<= 2*DIS_EPSILON) {
	return 0.5 ;
      }
      
      if (acc){

        float delta = currentSpeed_*currentSpeed_ + 2*acc*distance_;
	if (delta >= 0) {
	  t=(sqrt(delta)-currentSpeed_)/acc;
	}
	else { 
	  t=100; 
	}
      }

      else if (currentSpeed_<=SPEED_EPSILON){

	t=100;     // again a large number
      }

      else {
	t=distance_/currentSpeed_;
      }
      return (t);
    }




/* tomer
 *-------------------------------------------------------------------
 * Calculate the acceleration rate for intersection approaching
 *-------------------------------------------------------------------
 */

float
TS_Vehicle::calcIntersectionRate(RN_IS* intersection , int myMove)
{
  float acc;

  if (myMove<0 || distance_ < 2*DIS_EPSILON) { return MAX_ACCELERATION; }

  float turnSpeed=intersection->_manuvers[myMove]->getTurnSpeed();
  turnSpeed = turnSpeed*approachSpeedFactor_; // distributed between 0.9 and 1.2


  //#ifdef TEMP_OUT

    RN_Node* frontNode;
    TS_Vehicle* front = vehicleAhead();
    if (front!=NULL) {
    frontNode = front->link()->dnNode();
    }
    else 
      {frontNode=NULL; 
      }
    float nearIS = 10.0;

    if (frontNode!=link()->dnNode() && distance_ < nearIS) {
      return MAX_ACCELERATION ; }
    //#endif  

    if (currentSpeed_ > turnSpeed &&  // faster than turning speed, and...
	distance_ < 50.0 ) {          // ...within 50 m of intersection (Angus)
      acc=brakeToTargetSpeed( distance_ , turnSpeed );
    }
    else {
      acc = MAX_ACCELERATION;
    }
    return (acc);
}



// **



// tomer - gets an accelaration and checks if the gap is acceptable. 
// returns 1 if it is. 0 if not.2 if major movement and 3 if i'm stopped


int
TS_Vehicle::acceptableGap(float ac , int myMove)
{
  float impatienceFactor_=0.2;
  float minimumGap_=0;

  float criticalGap = isApproachingIS_->_manuvers[myMove]->getCriticalGap();

  if (criticalGap==0) { return (2) ; }
  
  // criticalGap = max(criticalGap-impatienceFactor_*impatienceTimer() , minimumGap_ ) + randomGapFactor_;
  
  criticalGap = max(criticalGap-impatienceFactor_*impatienceTimer() + randomGapFactor_ , minimumGap_ );
  
  float myTime = timeToIntersection (ac);

  float otherMovementTime=timeToConflict();

  if (otherMovementTime-myTime < 0 && currentSpeed_ < SPEED_EPSILON) {
    return (3);
  }

  else if (otherMovementTime-myTime>=criticalGap ) {
    return (1);
  }
  return (0);
}


// **



// return the time to conflict - the minimum time until a vehicle will 
// reach thr intersection

float
TS_Vehicle::timeToConflict() 
{
  
  int myMove=getMovement(isApproachingIS_);
  vector <int> conflictLanes_=conflictLanes(myMove);
  float timeToConflict=100;
  for (int i=0; conflictLanes_[i]>=0; i++) {

	 // timeToConflict needs a large number to start with. here I just put 100
	 // but we need to use a constant or something is there anything
	 // existing ?
	 TS_Vehicle* conflictVehicle=getFirst(conflictLanes_[i]);
	 MID otherMovement=firstMove(conflictLanes_[i]);
	 if (isConflict(otherMovement)&&
	     (conflictVehicle->timeToIntersection()<timeToConflict)) {
	   timeToConflict=conflictVehicle->timeToIntersection();
	 }
  }
return (timeToConflict);
}
       

int 
  TS_Vehicle::ImFirst()
 {
   RN_Node* frontNode;
   TS_Vehicle* front = vehicleAhead();
   if (front!=NULL) {
     frontNode = front->link()->dnNode();
   }
   else 
     {frontNode=NULL; 
     }
   
   if (frontNode!=link()->dnNode()) {return 1 ; }
   else {return 0 ; }
 }
 
    // ** 





//#ifdef USE_TOMER_MODELS

float
TS_Vehicle::calcForwardRate()
{
 TS_Lane *plane;

  if (status(STATUS_LEFT)) {
	plane = lane_->left();
  } else if (status(STATUS_RIGHT)) {
	plane = lane_->right();
  } else {
	return MAX_ACCELERATION;	       	// No request for lane change 
  }

  float *a = theParameter->targetGapParams();
  
    TS_Vehicle* av = findFrontBumperLeader(plane);

    if (!av)
      {
      	return MAX_ACCELERATION;	 
      }
    
  float dis = this->gapDistance(av) + av->length() + a[0]*this->eff_fgap;
  float dv = av->currentSpeed() - currentSpeed();
  
  float acc = a[1] * pow(dis, a[3]);
  
  if (dv > 0) {
    acc *= pow(dv, a[4]); 
  } else if (dv < 0) {
    acc *= pow (-dv, a[5]);
  }
  acc += a[2] + theParameter->cfAccAddOn(driverGroup.cfAccAddOn) * a[6] /0.824 ;
  return acc;
}

float
TS_Vehicle::calcBackwardRate()
{
 TS_Lane *plane;

  if (status(STATUS_LEFT)) {
	plane = lane_->left();
  } else if (status(STATUS_RIGHT)) {
	plane = lane_->right();
  } else {
	return MAX_ACCELERATION;	      // No request for lane change 
  }

 float *a = theParameter->targetGapParams();

 
 TS_Vehicle* av = findFrontBumperFollower(plane);

    if (!av)
      {
      	return MAX_ACCELERATION;	 
      }

 float dis = av->gapDistance(this) + av->length() + a[0]*this->eff_bgap;
 float dv = av->currentSpeed() - currentSpeed();

 float acc = a[7] * pow(dis, a[9]);

 if (dv > 0) {
   acc *= pow(dv, a[10]); 
 } else if (dv < 0) {
   acc *= pow (-dv, a[11]);
 }
  acc += a[8] + theParameter->cfAccAddOn(driverGroup.cfAccAddOn) *  a[12] / 0.824 ;
 return acc;
}



float
TS_Vehicle::calcAdjacentRate()
{
 TS_Lane *plane;

  if (status(STATUS_LEFT)) {
	plane = lane_->left();
  } else if (status(STATUS_RIGHT)) {
	plane = lane_->right();
  } else {
	return MAX_ACCELERATION;			// No request for lane change 
  }

 float *a = theParameter->targetGapParams();

 TS_Vehicle* av = findFrontBumperLeader(plane);
 if (!av) return MAX_ACCELERATION;

 TS_Vehicle* bv = findFrontBumperFollower(plane);
 if (!bv) return normalDeceleration();

 // float gap = bv->gapDistance(av);
 float gap = this->eff_agap;
 float position = bv->gapDistance(this)+ length();
 float acc = a[13] * (a[0] * gap - position);

  acc += theParameter->cfAccAddOn(driverGroup.cfAccAddOn) * a[14] / 0.824 ;
 return acc;
}

//#endif
