
//-*-c++-*------------------------------------------------------------
// NAME: Microscopic Traffic Simulator
// AUTH: Qi Yang and Kazi Ahmed
// FILE: TS_LCModels.C
// DATE: Mon Feb 19 21:07:13 1996
//--------------------------------------------------------------------

#include <Tools/Math.h>

#include <vector>
#include <algorithm>

#include <Tools/ToolKit.h>
#include <Tools/Random.h>
#include <Tools/SimulationClock.h>

#include <GRN/RN_CtrlStation.h>

#include <GRN/RN_Path.h>
#include <GRN/BusAssignmentTable.h>

#include "TS_Node.h"
#include "TS_Link.h"
#include "TS_Segment.h"
#include "TS_Lane.h"
#include "TS_VehicleLib.h"
#include "TS_TollBooth.h"
#include "TS_BusStop.h"  //margaret
#include "TS_Incident.h"
#include "TS_Parameter.h"
#include "TS_Status.h"
#include "TS_Engine.h"
#include "TS_Network.h"
#include "TS_FileManager.h"

#ifdef INTERNAL_GUI
#include <DRN/DRN_DrawingArea.h>
#include "TS_DrawableVehicle.h"
#else
#include "TS_Vehicle.h"
#endif

/*
 *--------------------------------------------------------------------
 * Clear the variable 'status', and then set the value for the first
 * 3 bits of this variable according to the correctness of right,
 * current, and left lanes in terms of taking this vehicle to the
 * nextLink on one's path.
 *
 * The first 3 bits indicate whether the right, current, and left
 * are correct lanes. The default constants to be used for masking
 * are:
 *
 *    1=STATUS_RIGHT_OK.
 *    2=STATUS_CURRENT_OK.
 *    4=STATUS_LEFT_OK.
 *
 * The fourth and fifth bit indicate current lane change status,
 * and they are masked by:
 *
 *    8=STATUS_RIGHT
 *   16=STATUS_LEFT
 *   24=STATUS_CHANGING
 *
 * The sixth bit indicates mandatory/discretionary lane changing.
 * The default bit mask is 32, i.e.:
 *
 *   32=STATUS_MANDATORY
 *
 * This function is called when a vehicle is moved into a
 * new lane.
 *--------------------------------------------------------------------
 */

void
TS_Vehicle::setStatus(void)
{
  TS_Lane * plane;
  unsigned int prev_status = status_;
  int escape = 0;
  status_ = 0;
  //arterial
  //  int vid;
  //vid = this->code();

  //int lid;
  //lid = lane_->code();
  if (plane = lane_->right()) {

	// It is not the right shoulder lane. Check if the right lane is
	// a correct lane.

	if (plane->isWrongLane(this) >= 0 &&
		plane->doesNotAllow(this) >= 0) {

	  // Need to change to the right lanes

	  setStatus(STATUS_RIGHT_OK);
	}
  }
   
  if (lane_->isWrongLane(this) == 0 &&
	  (escape = lane_->doesNotAllow(this)) == 0) {

	// Current lane is correct.

	setStatus(STATUS_CURRENT_OK);
  }

  if (plane = lane_->left()) {

	// It is not the left shoulder lane. Check if the left lane is a
	// correct lane.

	if (plane->isWrongLane(this) <= 0 &&
		plane->doesNotAllow(this) <= 0) {
	 
	  // Correct lane or need to change to the left for moving to a
	  // correct lane.

	  setStatus(STATUS_LEFT_OK);
	}
  }

  if (prev_status & STATUS_MANDATORY) {
	setStatus(STATUS_MANDATORY);
  } else if (escape) {
	setMandatoryStatusTag();
  }
 
   if(lane_->laneType()&LANE_TYPE_UP_ONRAMP){
    setStatus(STATUS_MANDATORY);
     }//gunwoo -- MERGING SCENARIO


    float k;
    k = this->position();
   if( (lane_->segment()->code()==link()->endSegment()->code()) && (k<0.1) && (nextLink_))
     { int nextlid;
       nextlid = this->nextLink_->code();
       int j;
       j = lane_->isDownLanesInNextLink((TS_Link *)this->nextLink_);
        
   if(!(lane_->isDownLanesInNextLink((TS_Link *)this->nextLink_)) )
      { 
	//       this->currentSpeed_ = this->currentSpeed_/2;       
       	setMandatoryStatusTag();
     }
     }// VR 03/02/3007
  
}


void
TS_Vehicle::setMandatoryStatusTag()
{
  if (status(STATUS_MANDATORY)) return;
  //  lcTimeTag_ = theSimulationClock->currentTime();
  lcProdMlcTagRejProb_ = 1.0;
  setStatus(STATUS_MANDATORY);
}


int
TS_Vehicle::isReadyForNextDLC(int mode)
{
  float sec = timeSinceTagged();

  switch(mode) {
  case 1:			// request a change to the right
	{
	  if (flag(FLAG_PREV_LC_RIGHT) && // same direction
		  sec > theParameter->dlcMinTimeInLaneSameDir()) {
		return 1;
	  } else if (sec > theParameter->dlcMinTimeInLaneDiffDir()) {
		return 1;
	  } else {
		return 0;
	  }
	}
  case 2:			// request a change to the left
	{
	  if (flag(FLAG_PREV_LC_LEFT) && // same direction
		  sec > theParameter->dlcMinTimeInLaneSameDir()) {
		return 1;
	  } else if (sec > theParameter->dlcMinTimeInLaneDiffDir()) {
		return 1;
	  } else {
		return 0;
	  }
	}
  }
  return sec > theParameter->dlcMinTimeInLaneSameDir();
}


// Check if the target lane is within the incident zone (two car
// length from the incident).

int TS_Vehicle::isInIncidentArea(TS_Lane *plane)
{
  float d = distanceFromDownNode();
  float w = length_ + length_;
  CtrlList ctrl = link()->prevCtrl(d);
  while (ctrl && d + w > (*ctrl)->distance()) {
	if ((*ctrl)->type() == CTRL_INCIDENT) {
	  RN_Signal *inc = (*ctrl)->signal(plane->localIndex());
	  if (inc &&
		  (inc->state() & INCI_STOP) &&
		  (inc->state() & INCI_ACTIVE)) {
		return 1;
	  }
	}
	ctrl = ctrl->prev();
  }
  return 0;
}


/*
 *-------------------------------------------------------------------
 * Determine if this driver will start a mandatory lane change or
 * stay in the correct lanes. This function will update bit 6-7 of
 * "status_" if necessary. This two bits indicate mandatory lane
 * change or "suggested/required" lane changes, i.e.:
 *
 *   32 = STATUS_MANDATORY
 *   64 = STATUS_SUGGESTED
 *
 * This function is called by TS_Vehicle::makeLaneChangingDecision().
 * It returns TRUE if the vehicle's mandatory flag has been turn on,
 * or FALSE otherwise.
 *-------------------------------------------------------------------
 */

int
TS_Vehicle::checkIfMandatory()
{
  // Compute the distance from the critical position.

  int bad = lane_->isThereBadEventAhead(this, &dis2stop_, &vis_);

  unsigned int statusp = status_;

  if (bad < 0) {
	setFlag(FLAG_ESCAPE);
  } else if (bad > 0) {
	setFlag(FLAG_AVOID);
  } else {
	dis2stop_ = distanceFromDownNode();
	vis_ = link()->length();
  }

#ifdef OUT
  if (distance_ < dis2stop_ &&
	  (lane_->laneType(LANE_TYPE_UP_ONRAMP) && !nextLane_ ||
	   lane_->laneType(LANE_TYPE_DN_OFFRAMP))) {
	dis2stop_ = distance_;
	vis_ = theParameter->visibility();
  }
#endif

  if (status(STATUS_MANDATORY)) {

	// This vehicle is already tagged
      
	return (STATUS_MANDATORY);
  }
    
  if ( flag(FLAG_ESCAPE) && flag(FLAG_VMS_LANE_USE_DIR) ) {  // Angus
        // Vehicle flagged for escape from lane by VMS
        setMandatoryStatusTag();
	return (STATUS_MANDATORY);
  }
   
  // Currently this vehicle is not making mandatory lane
  // changing. Check if it should make a mandatory lane change.


    if (dis2stop_ > theParameter->lcLowerBound()) {

	// Number of lanes to change
       
	int n = lane_->isWrongLane(this);

	float y = tsSegment()->density() / theParameter->jamDensity();

	// CMF for making mandatory lane changing.

	float cmf = theParameter->mlcProbCmf(dis2stop_, n, y);

	// PMF for making mandatory lane changing.

	float pmf = probCmfToPmf(cmf, lcProdMlcTagRejProb_);

	// Determine if this driver will start a mandatory lane change
	// or stick in the current lanes.
       
	if (theRandomizer->brandom(pmf)) {
	  setMandatoryStatusTag();
	}
  } else {
	setMandatoryStatusTag();
  }

  return status(STATUS_MANDATORY);
}


// ****************************************************************************
// tomer - this function checks for bad events that will override the lookahead
// ****************************************************************************

int
TS_Vehicle::checkIfLookAheadEvents()
{

  // Compute the distance from the critical position.

  int bad = lane_->isThereBadEventAhead(this, &dis2stop_, &vis_);
  unsigned int statusp = status_;
  if (bad < 0) {
	setFlag(FLAG_ESCAPE);
  } else if (bad > 0) {
	setFlag(FLAG_AVOID);
  } else {
	dis2stop_ = distanceFromDownNode();
	vis_ = link()->length();
  }

  if ( bad < 0 && dis2stop_ < LookAheadDistance() ) {
    setMandatoryStatusTag();  
    return status(STATUS_MANDATORY);
  } 

  // Dan: Evaluate whether a downstream bus stop dictates
  // a mandatory lane change ...

  if (isBusWithAssignment()) {

    CtrlList device = nextCtrl_;
    float busvis = theBaseParameter->busToStopVisibility();
    float dis = 0.;
    int busLCDirectionSet = 0;

    while (device && (dis = distanceFromDevice(device)) < busvis
	   && (*device)->type() == CTRL_BUSSTOP) {

      // Dan: there is a bus stop downstream within the bus' "visibility"

      int n = (*device)->nLanes();
      for (int i = 0; i < n; i ++) {
        RN_Signal *signal = (*device)->signal(i);
        if (signal) {  // there is a signal (bus stop) in this lane
          BusAssignment *ba = theBusAssignmentTable->findBusAssignment(code());
	  if (!status(STATUS_BUS_DEPARTING)) {

	    // Dan: the bus is not already stopped and beginning to depart

	    if (ba->currentTrip()->findBusStop(signal->code())) {

	      // Dan: the bus stop is on the bus' assigned route

              ba->setLeftOrRight(signal->leftOrRight()); // bus stop dictates LC direction
	      setMandatoryStatusTag();
              busLCDirectionSet++;
            }
	  } else { // if bus was already departing stop, unset this value
	    unsetStatus(STATUS_BUS_DEPARTING);
	    unsetStatus(STATUS_BUS_SLOWING);
	  }
        }
      }
      device = device->next();
    }

    if (busLCDirectionSet == 0 && distance() < busvis) {

      // Dan: No bus stop on this link, look at the next link

      if (nextLink_ != NULL) { 

	CtrlList dev = nextLink_->ctrlList();  // first ctrl list on next link
	float devdis = 0.;
	if (dev) devdis = nextLink_->length() - (*dev)->distance();
	float dist = 0.;

        while (dev && (dist = (distance() + devdis) ) < busvis
	        && (*dev)->type() == CTRL_BUSSTOP) {

          // Dan: there is a bus stop downstream within the bus' "visibility"

          int n = (*dev)->nLanes();
          for (int i = 0; i < n; i ++) {
            RN_Signal *signal = (*dev)->signal(i);
            if (signal && n > 1) {  // there is a signal (bus stop) in this lane
              BusAssignment *ba = theBusAssignmentTable->findBusAssignment(code());
	      if (!status(STATUS_BUS_DEPARTING)) {

	        // Dan: the bus is not already stopped and beginning to depart

	        if (ba->currentTrip()->findBusStop(signal->code())) {

	          // Dan: the bus stop is on the bus' assigned route

                  ba->setLeftOrRight(signal->leftOrRight()); // bus stop dictates LC direction
	          setMandatoryStatusTag();
                  busLCDirectionSet++;
                }
	      } else { // if bus was already departing stop, unset this value
	        unsetStatus(STATUS_BUS_DEPARTING);
	        unsetStatus(STATUS_BUS_SLOWING);
	      }
	    }
	  }
          dev = dev->next();
	}
	
      }
    }
  }
  return status(STATUS_MANDATORY);
}
    



/*
 *---------------------------------------------------------------------
 * Determine to which lane a vehicle in a mandatory status will change
 * to. It returns the direction of the change (-1=left, 1=right) or
 * zero if no change.
 *---------------------------------------------------------------------
 */

int
TS_Vehicle::checkMandatoryLaneChanging()
{
  TS_Lane* plane;

  int escape = flag(FLAG_ESCAPE);
  int avoid = flag(FLAG_AVOID);
  unsetFlag(FLAG_ESCAPE | FLAG_AVOID | FLAG_BLOCK);

  int out = escape;				// || avoid;

  // Check if this vehicle has to get out of current lane

  if (out || !status(STATUS_CURRENT_OK)) {

	// number of lane changes to an open lane

	int nl =  1;		// left side
	int nr = -1;		// right side

	setFlag(FLAG_BLOCK);

	// Check left side to find whether a lane if open

	if ((plane = lane_->left()) &&
		(status(STATUS_LEFT_OK) || out)) {
	  while (plane) {
		if (plane->isThereBadEventAhead(this) >= 0) {
		  nl = -nl;
		  unsetFlag(FLAG_BLOCK_LEFT);
		  break;		// found an open lane
		}
		nl ++;
		plane = plane->left();
	  }
	}

	// Check right side to find whether a lane is open

	if ((plane = lane_->right()) &&
		(status(STATUS_RIGHT_OK) || out)) {
	  while (plane) {
		if (plane->isThereBadEventAhead(this) >= 0) {
		  nr = -nr;
		  unsetFlag(FLAG_BLOCK_RIGHT);
		  break;		// found an open lane
		}
		nr --;
		plane = plane->right();
	  }
	}

	// Select the direction with an open lane and minimum number of
	// lane changes

	if (nl < 0 && (nr < 0 || -nl < nr)) {

	  // There is an open left lane and no open right lane or the
	  // open left lane is closer.

	  if (escape) setFlag(FLAG_ESCAPE_LEFT);
	  else if (avoid) setFlag(FLAG_AVOID_LEFT);
	  return -1;

	} else if (nr > 0 && (nl > 0 || nr < -nl)) {

	  // There is an open right lane and no open left lane or the
	  // open right lane is closer.

	  if (escape) setFlag(FLAG_ESCAPE_RIGHT);
	  else if (avoid) setFlag(FLAG_AVOID_RIGHT);
	  return 1;

	} else if (nl < 0 && nr > 0) {

	  // There is open lane on both side. Choose one randomly.

	  if (theRandomizer->brandom(0.5)) {
		if (escape) setFlag(FLAG_ESCAPE_LEFT);
		else if (avoid) setFlag(FLAG_AVOID_LEFT);
		return -1;
	  } else {
		if (escape) setFlag(FLAG_ESCAPE_RIGHT);
		else if (avoid) setFlag(FLAG_AVOID_RIGHT);
		return 1;
	  }

	} else {

	  // All lane are blocked

	  return 0;
	}
  }

  // This vehicle can proceed in current lane. Check for
  // discretionary lane change.

  if (flag(FLAG_VMS_LANE_USE_DIR) == FLAG_VMS_LANE_USE_DIR &&
	  attr(ATTR_GLC_RULE_COMPLY)) {

	// Viewed a message that says stay in lane and this is a
	// cooperative driver.
	 
	return 0;
  }

  double eul = 0.0, eur = 0.0 ;
   
  // Lane change and lane use regulation. 

  int rules = lane_->cRules();
  int change = 0;
	
  // Check the left lane

  if (isReadyForNextDLC(2) &&
	  (plane = lane_->left()) &&
	  status(STATUS_LEFT_OK) &&
	  (!flag(FLAG_VMS_LANE_USE_RIGHT) ||
	   plane->localIndex() > VmsLaneUsePivotToIndex(flag())) &&
	  (rules & LANE_CHANGE_LEFT ||
	   !attr(ATTR_GLC_RULE_COMPLY)) &&
	  plane->isThereBadEventAhead(this) >= 0) {

    eul = dlcExpOfUtility(plane);
  }

  if (isReadyForNextDLC(1) &&
	  (plane = lane_->right()) &&
	  status(STATUS_RIGHT_OK) &&
	  (!flag(FLAG_VMS_LANE_USE_LEFT) ||
	   plane->localIndex() < VmsLaneUsePivotToIndex(flag())) &&
	  (rules & LANE_CHANGE_RIGHT ||
	   !attr(ATTR_GLC_RULE_COMPLY)) &&
	  plane->isThereBadEventAhead(this) >= 0) {

    eur = dlcExpOfUtility(plane);
  }


  double sum = 1.0 + eul + eur ;
  double rnd = theRandomizer->urandom() ;
  if (rnd < 1.0 / sum) change = 0 ;
  else if (rnd < (1.0 + eul) / sum) change = -1 ;
  else change = 1 ;

  return change;
}


// ***************************************************************************
// tomer - this function determines the lane changing that a lookahead vehicle
//         will try if it is constrained by an event. 
// ***************************************************************************


int
TS_Vehicle::checkMandatoryEventLC()
{
  TS_Lane* plane;

  int escape = flag(FLAG_ESCAPE);
  int avoid = flag(FLAG_AVOID);
  unsetFlag(FLAG_ESCAPE | FLAG_AVOID | FLAG_BLOCK);

  int out = escape;				// || avoid;

  int busmove = 0; // Dan: used to determine bus' lane change on approach to bus stop

  /* Dan:the following if statement checks several conditions for buses that have
     bus assignments (i.e. the user has specified schedule information as input to
     MITSIM).  If the CheckIfLookAheadEvents function has determined that the bus
     is approaching a stop on its route, then the bus' leftOrRight value should 
     dictate the direction the bus must move in order to reach the stop.
  */

  if (isBusWithAssignment()) { 
    if ((plane = tsLane()) && plane->isThereBadEventAhead(this) >= 0) {

      // There is no bad event ahead in this lane, bus stop dictates LC ...

      BusAssignment *ba = theBusAssignmentTable->findBusAssignment(code());
      busmove = ba->leftOrRight(); 

      int nl = 0;		// left side
      int nr = 0;		// right side

      // Check left side to find whether a lane is open

      if (lane_->left()) {
        nl --;
      }

      // Check right side to find whether a lane is open

      if (lane_->right()) {
        nr ++;
      }

      if (nl < 0 && busmove < 0) return -1; // lane open to left and stop is on left
      if (nr > 0 && busmove > 0) return 1;  // lane open to right and stop is on right
      if (nl == 0 && busmove < 0 || nr == 0 && busmove > 0) return 0;
         /* no lanes open on either side: bus is in appropriate lane for bus stop, 
            must stay in current lane to reach the stop */
    }
  }


  // Check if this vehicle has to get out of current lane

  if (out || !status(STATUS_CURRENT_OK)) {

	// number of lane changes to an open lane

	int nl =  1;		// left side
	int nr = -1;		// right side

	setFlag(FLAG_BLOCK);

	// Check left side to find whether a lane is open

	if ((plane = lane_->left()) &&
		(status(STATUS_LEFT_OK) || out)) {
	  while (plane) {
		if (plane->isThereBadEventAhead(this) >= 0) {
		  nl = -nl;
		  unsetFlag(FLAG_BLOCK_LEFT);
		  break;		// found an open lane
		}
		nl ++;
		plane = plane->left();
	  }
	}

	// Check right side to find whether a lane is open

	if ((plane = lane_->right()) &&
		(status(STATUS_RIGHT_OK) || out)) {
	  while (plane) {
		if (plane->isThereBadEventAhead(this) >= 0) {
		  nr = -nr;
		  unsetFlag(FLAG_BLOCK_RIGHT);
		  break;		// found an open lane
		}
		nr --;
		plane = plane->right();
	  }
	}

	// Select the direction with an open lane and minimum number of
	// lane changes

	if (nl < 0 && (nr < 0 || -nl < nr)) {

	  // There is an open left lane and no open right lane or the
	  // open left lane is closer.

	  if (escape) setFlag(FLAG_ESCAPE_LEFT);
	  else if (avoid) setFlag(FLAG_AVOID_LEFT);
	  return -1;

	} else if (nr > 0 && (nl > 0 || nr < -nl)) {

	  // There is an open right lane and no open left lane or the
	  // open right lane is closer.

	  if (escape) setFlag(FLAG_ESCAPE_RIGHT);
	  else if (avoid) setFlag(FLAG_AVOID_RIGHT);
	  return 1;

	} else if (nl < 0 && nr > 0) {

	  // There is open lane on both side. Choose one randomly.

	  if (theRandomizer->brandom(0.5)) {
		if (escape) setFlag(FLAG_ESCAPE_LEFT);
		else if (avoid) setFlag(FLAG_AVOID_LEFT);
		return -1;
	  } else {
		if (escape) setFlag(FLAG_ESCAPE_RIGHT);
		else if (avoid) setFlag(FLAG_AVOID_RIGHT);
		return 1;
	  }

	} else {

	  // All lane are blocked

	  return 0;
	}
  }
  RN_Segment* psegment = lane_->segment();
  int laneNum=psegment->nLanes();
  int local=lane_->localIndex();//local index of current lane
  int n = lane_->isWrongLane(this); 

  // Currently this vehicle is not making mandatory lane change
  // Check for discretionary lane change.

  if (flag(FLAG_VMS_LANE_USE_DIR) == FLAG_VMS_LANE_USE_DIR &&
	  attr(ATTR_GLC_RULE_COMPLY) && !n) {

	// Viewed a message that says stay in lane and this is a
	// cooperative driver.
	 
       return 0;
  }

 
#ifdef USE_TOMER_MODELS

   double eul = 0.0, eur = 0.0, euc = 1.0 ;
#endif

#ifdef USE_CFC_MODELS

   // vector <double>probLane=TLProbabilityV();

 vector <double>probLane=TLProbability(lane_);
 double  probOfCurrentLane=0;double probOfLeftLane=0;double probOfRightLane=0;

#endif


#ifdef USE_KAZI_MODELS


 double eul = 0.0, eur = 0.0;

#endif

  int change = 0;

  // Lane change and lane use regulation. 

  int rules = lane_->cRules();

  if ((isReadyForNextDLC(2) || !n ) &&
      (plane = lane_->left()) &&
      (!flag(FLAG_VMS_LANE_USE_RIGHT) ||
       plane->localIndex() > VmsLaneUsePivotToIndex(flag())) &&
      (rules & LANE_CHANGE_LEFT ||
       !attr(ATTR_GLC_RULE_COMPLY)) &&
      plane->isThereBadEventAhead(this) >= 0) {

#ifdef USE_TOMER_MODELS

	  // Lane change to the left is allowed, and there is no critical
	  // event ahead in the left lane.


	  eul = LCUtilityLeft(plane);
#endif

#ifdef USE_KAZI_MODELS

	  eul = dlcExpOfUtility(plane);

#endif
#ifdef USE_CFC_MODELS  //cfc jun 11
	 
	  for (int i=0;i<local;i++){
	    probOfLeftLane=probOfLeftLane+probLane[i];}

       
#endif

  }
		
  if ((isReadyForNextDLC(1)|| !n ) &&
      (plane = lane_->right()) &&
      (!flag(FLAG_VMS_LANE_USE_LEFT) ||
       plane->localIndex() < VmsLaneUsePivotToIndex(flag())) &&
      (rules & LANE_CHANGE_RIGHT ||
       !attr(ATTR_GLC_RULE_COMPLY)) &&
      plane->isThereBadEventAhead(this) >= 0) {

#ifdef USE_TOMER_MODELS

	  // Change to the right is allowed, and there is no critical
	  // event ahead.

	  eur = LCUtilityRight(plane);

#endif

#ifdef USE_KAZI_MODELS

	  eur = dlcExpOfUtility(plane);

#endif


#ifdef USE_CFC_MODELS
 
	  for (int i=local+1;i<laneNum;i++){
	    probOfRightLane=probOfRightLane+probLane[i];}

	 
#endif
  }

#ifdef USE_KAZI_MODELS

  double sum = 1.0 + eul + eur;
  double rnd = theRandomizer->urandom();
  if (rnd < 1.0 / sum) change = 0 ;
  else if (rnd < (1.0 + eul) / sum) change = -1 ;
  else change = 1 ;

#endif
  
#ifdef USE_TOMER_MODELS

  double sum = eul + eur ;

  if (sum > 0 ){
    
    euc = LCUtilityCurrent(lane_);
    
      }

  sum += euc;

 double rnd = theRandomizer->urandom() ;

 double probOfCurrentLane = euc / sum;
 double probOfCL_LL = probOfCurrentLane + eul / sum;
 if (rnd < probOfCurrentLane)  change = 0;
 else if (rnd < probOfCL_LL) change = -1;
 else change = 1 ;

#endif

#ifdef USE_CFC_MODELS
 probOfCurrentLane=probLane[local];
 double probOfCL_LL = probOfCurrentLane + probOfLeftLane;
 double rnd = theRandomizer->urandom() ;
 if (rnd < probOfCurrentLane)  change = 0;
 else if (rnd < probOfCL_LL) change = -1;
 else change = 1 ;
#endif
    


  return change;
}



// ****************************************************************
// tomer - this function gives the LC direction when the driver is 
//         constrained by the lookahead
// ****************************************************************

int
TS_Vehicle::checkMandatoryLookAheadLC(vector <int> & _connectedLanes)
{

  int idx = pathIndex_; 

  // debugging

  //  if (idx<=0)
  // { int a=1;}

  RN_Link* plink = link();
  RN_Segment* psegment = lane_->segment();
  float x=distance_;
  int change = 0;
  RN_Segment* ssegment;
  RN_Lane* plane;
  RN_Lane* slane;
  
  if ( flag(FLAG_ESCAPE) && flag(FLAG_VMS_LANE_USE_DIR) ) {  //  Flagged for escape by VMS (Angus)

    if (flag(FLAG_ESCAPE_LEFT)) {
      change = -1;
    }
    if (flag(FLAG_ESCAPE_RIGHT)) {
      change = 1;
    }
    setStatus(STATUS_MANDATORY);
    return change;
  }

  // Dan: A bus lane should override the look-ahead - assuming a bus lane
  // will not lead a bus off it's assigned route

  if (isBusWithAssignment() && lane_->isBusLane()) return 0;

  if ( x>=lookAheadDistance_ && status(STATUS_CURRENT_OK) ) {
    return change;
  }
  
  while ( x<lookAheadDistance_ )                 // within lookahead distance
    {
      if ( psegment != plink->endSegment() )     // still in this link
	{
	  psegment = psegment->downstream();
	  x += psegment->length();
	}
      else if ( idx < path()->nLinks() )        // looking at the next link
	{
	 
	  if ( !path()->link(idx) )
	    {break;}
	  plink = path()->link(idx);
	  //	  cout << plink << endl;
	  
	  psegment = plink->startSegment();
	  x += psegment->length();
	  idx++;
	}
      else { break; }                            // end of path 
    }

  if ( psegment != plink->startSegment() ) {
    ssegment = psegment->upstream();
    idx--;
  } 
  else {
    idx -= 2;
    if ( path()->link(idx) ) {
      plink = path()->link(idx);
      ssegment = plink->endSegment();
    } 
    else 
      {return 0; }
  }
  
  vector <int > _upLaneList;        // list of local indexes to connected lanes in ssegment


  _connectedLanes.erase(_connectedLanes.begin(), _connectedLanes.end() );

  for (int i = 0; i < psegment->nLanes(); i ++)
    {
      _connectedLanes.push_back(i);
    }


  // advancing upstream segment by segment and creating the connectivity lists

 while ( psegment != segment() )  {  
   bool allMarked = false;
  
   // iterate all connected lanes in the downstream segment
   for (int i = 0; i < _connectedLanes.size(); i ++) { 
     bool lastConnect = false ;
     plane = psegment->lane(_connectedLanes[i]);

     TS_Lane* tlane = (TS_Lane*) psegment->lane(_connectedLanes[i]);
     if (! tlane->doesNotAllow(this)) // avoid lanes not allowed (e.g. HOV lane) (Angus)
       {
	 // iterate all connected lanes in the upstream segment
	 for (int j = 0; j < ssegment->nLanes(); j ++) { 
	   
	   // avoid lanes that the vehicle is not allowed to use
	   
	   TS_Lane* qlane = (TS_Lane*) ssegment->lane(j);
	   if (! qlane->doesNotAllow(this)) { 
	     slane = ssegment->lane(j);
	     bool connect = plane->isInUpLanes(slane);
	     int marked = * ( find (_upLaneList.begin(), _upLaneList.end(), j ) );
	     if ( connect && marked == * (_upLaneList.end() ) ) { 
	       // put it at the end of the list
	       _upLaneList.push_back(j);
	       lastConnect = connect;
	       
	       // stop if all upstream lanes are connected	  
	       if ( _upLaneList.size() == ssegment->nLanes() ) {
		 allMarked = true;
		 break; 
	       }
	       else if (!connect && lastConnect) {break;}
	     }
	   }
	   else if (lastConnect ) { break; }
	 }
       }
     if (allMarked) {break;}
   }

   // the upstream set is empty. LC must happen downstream.  
   if ( _upLaneList.empty() ) { 
     // maybe sorting is not really needed - check it for deletion
     // sort ( _connectedLanes.begin() , _connectedLanes.end() );
     int leftIndex = _connectedLanes[0];
     int rightIndex = _connectedLanes[_connectedLanes.size()-1];
     if (leftIndex > rightIndex) {
       int temp = leftIndex;
       leftIndex = rightIndex;
       rightIndex = temp;
     } 
     while ( _upLaneList.empty() ) {
       if ( ++rightIndex < psegment->nLanes()) {

	 // iterate all connected lanes in the upstream segment
	 for (int j = 0; j < ssegment->nLanes(); j ++) { 
	   plane = psegment->lane(rightIndex);
	   slane = ssegment->lane(j);
	   bool connect = plane->isInUpLanes(slane);
	   if ( connect ) { // put it at the end of the list
	     _upLaneList.push_back(j);
	   }
	 }
       }
       
        if ( leftIndex-- >= 0 && _upLaneList.empty() ) {

	 // iterate all connected lanes in the upstream segment
	  for (int j = 0; j < ssegment->nLanes(); j ++) { 
	    plane = psegment->lane(leftIndex);
	    slane = ssegment->lane(j);
	    bool connect = plane->isInUpLanes(slane);
	    if ( connect ) { // put it at the end of the list
	      _upLaneList.push_back(j);
	    }
	  }
	}
	// a correction for the case there is no connectivity between 
	// the 2 segments (Angus and Tomer)
	if (leftIndex < 0 && rightIndex > psegment->nLanes() ) {
	  for (int j = 0; j < ssegment->nLanes(); j ++) { 
	    _upLaneList.push_back(j);
	  }
	}
	// [end correction]
     }
   }
       
      _connectedLanes = _upLaneList;
      _upLaneList.erase(_upLaneList.begin(), _upLaneList.end() );

      psegment = ssegment;
      
      
      if ( psegment != segment() ){
	if ( ssegment != plink->startSegment() ) {
	  ssegment = ssegment->upstream();
	} 
	else {
	  idx--;
	  plink = path()->link(idx);
	  ssegment = plink->endSegment();
	}		    
      }
 }

 // the result is a list of connected lanes in the current segment. needs to 
 // check that the current one is included in them or else what changes need 
 // to be done.

       
 int stay = * ( find ( _connectedLanes.begin() , _connectedLanes.end() , lane()->localIndex() ) );
 if ( stay != * ( _connectedLanes.end() ) ) { return 0; }

 int current = lane()->localIndex();

 change = (_connectedLanes[0] > current ) ? 1 : -1 ;
 setStatus(STATUS_MANDATORY);
 return change; 
}



/*
 *------------------------------------------------------------------
 * Determine if the vehicle will make a discretion lane change. The
 * The function return the direction for change (-1=left, 1=right)
 * or zero if no change. For a discretionary lane change, a driver
 * may select any lane which he/she may benefit.
 *------------------------------------------------------------------
 */

// In the default model, a driver is assumed to look for a
// discretionary lane changing if his speed is "significant" lower
// than his desired speed and he can not accelerate.
//
// In Kazi's new DLC model, utility for lane change is computed for
// each alternatives and a MNL model is then used to calculate the
// probabilities of choosing each of the available lanes.

int
TS_Vehicle::checkDiscretionaryLaneChanging()
{
  if (flag(FLAG_VMS_LANE_USE_DIR) == FLAG_VMS_LANE_USE_DIR &&
	  attr(ATTR_GLC_RULE_COMPLY)) {

	// Viewed a message that says stay in lane and this is a
	// cooperative driver.

	return 0;
  }

  TS_Parameter& p = *theParameter;

  // Chech if the driver has got impatient.

  if (timeSinceTagged() < p.dlcMinTimeInLaneSameDir()) {
	return 0;

  }

  double eul = 0.0, eur = 0.0 ;

  TS_Vehicle *front = this->vehicleAhead(); // Dan

  TS_Lane* plane;
  int change = 0;

  // Lane change and lane use regulation. 

  int rules = lane_->cRules();

  if (isReadyForNextDLC(2) &&
	  (plane = lane_->left()) &&
	  (!flag(FLAG_VMS_LANE_USE_RIGHT) ||
	   plane->localIndex() > VmsLaneUsePivotToIndex(flag())) &&
	  (rules & LANE_CHANGE_LEFT ||
	   !attr(ATTR_GLC_RULE_COMPLY)) &&
	  plane->isThereBadEventAhead(this) >= 0) {
    if (!(plane->laneType() & LANE_TYPE_RAMP)) {

	  // Lane change to the left is allowed, and there is no critical
	  // event ahead in the left lane.

	  // Dan: since lane to left is open, check to see whether driver
	  // is following a bus that is slowing to a stop; if so move over

	  if (front && (front->type() & STATUS_BUS_SLOWING)
              && (lane_->laneType() & LANE_TYPE_RIGHT_MOST)) {
	    return -1;
	  }

	  eul = dlcExpOfUtility(plane); // exponent of utility of change to left
    }
  }
		
  if (isReadyForNextDLC(1) &&
	  (plane = lane_->right()) &&
	  (!flag(FLAG_VMS_LANE_USE_LEFT) ||
	   plane->localIndex() < VmsLaneUsePivotToIndex(flag())) &&
	  (rules & LANE_CHANGE_RIGHT ||
	   !attr(ATTR_GLC_RULE_COMPLY)) &&
	  plane->isThereBadEventAhead(this) >= 0) {
    if (!(plane->laneType() & LANE_TYPE_RAMP)) {

	  // Change to the right is allowed, and there is no critical
	  // event ahead.

	  // Dan: since lane to right is open, check to see whether driver
	  // is following a bus that is slowing to a stop; if so move over

	  if (front && (front->type() & STATUS_BUS_SLOWING)
              && (lane_->laneType() & LANE_TYPE_LEFT_MOST)) {
	    return 1;
	  }

	  eur = dlcExpOfUtility(plane); // exponent of utility of change to right
    }
  }

  double sum = 1.0 + eul + eur ;
  double rnd = theRandomizer->urandom() ;

  float *b = theParameter->dlcUnsatisfactoryParamsKazi() ;

  float desSpeed = b[0] * tsSegment()->speed();

  // Addressing problem of no vehicles changing from slow-moving lane
  // into adjacent fast-moving lane  (Angus)
  //  if (plane) {
  //    float desSpeed = b[0] * Max(tsSegment()->speed(), plane->speed());    
  //  }
  // end fix

  float heavy_dummy = isType(VEHICLE_SMALL) ? 0.0 : b[3];
  float taleGateDummy = 0;
  TS_Vehicle* behind = this->vehicleBehind() ;
  if (behind) {
    double gap_behind = behind->gapDistance(this);
    float dens = tsSegment()->density();
    taleGateDummy = (gap_behind <= b[5] && dens <= b[6])? b[4] : 0;
  }
 
  float utilUnhappy = b[1] + b[2] * (currentSpeed_ - desSpeed)
    + heavy_dummy + taleGateDummy;
  float probUnhappy = 1 / (1 + exp(-utilUnhappy));

  // Calculate the probability of staying in current lane

  float probOfCurrentLane = 1 - probUnhappy * ((eul+eur) / sum);
  float probOfCL_LL = probOfCurrentLane + probUnhappy* (eul / sum);
  if (rnd < probOfCurrentLane) change = 0 ;
  else if (rnd < probOfCL_LL) change = -1 ;
  else change = 1 ;

  return change;

}



// -----------------------------------------------------------------
// Determine if the vehicle will make a discretion lane change. The
// The function returns the direction for change similiar to
// checkDiscretionaryLaneChanging(). The difference is that here the 
// new lane is checked to see if it is connected to the downstream
// target lanes.
// -----------------------------------------------------------------


int
TS_Vehicle::checkDiscretionaryLookAheadLC(vector <int> & _connectedLanes)
{
  if (flag(FLAG_VMS_LANE_USE_DIR) == FLAG_VMS_LANE_USE_DIR &&
	  attr(ATTR_GLC_RULE_COMPLY)) {

	// Viewed a message that says stay in lane and this is a
	// cooperative driver.

	return 0;
  }

  if (isBus()) {
    if (lane_->isBusLane()) { return 0; } // Dan: Bus in a bus lane is happy to stay put
    else {
      TS_Lane* currentLane = lane_->right();
      while (currentLane) {
	if (currentLane->isBusLane()) { return 1; } // Dan: Bus should move to bus lane
	currentLane = currentLane->right();
      }
      currentLane = lane_->left();
      while (currentLane) {
	if (currentLane->isBusLane()) { return -1; } // Dan: Bus should move to bus lane
	currentLane = currentLane->left();
      }
    }
  }

  TS_Parameter& p = *theParameter;

  // Chech if the driver has got impatient.

  if (timeSinceTagged() < p.dlcMinTimeInLaneSameDir()) {
	return 0;

  }

  double eul = 0.0, eur = 0.0 ;

  int current = lane()->localIndex();

  TS_Vehicle *front = this->vehicleAhead(); // Dan

  TS_Lane* plane;
  int change = 0;

  // Lane change and lane use regulation. 

  int rules = lane_->cRules();

  if (isReadyForNextDLC(2) &&
	  (plane = lane_->left()) &&
	  (!flag(FLAG_VMS_LANE_USE_RIGHT) ||
	   plane->localIndex() > VmsLaneUsePivotToIndex(flag())) &&
	  (rules & LANE_CHANGE_LEFT ||
	   !attr(ATTR_GLC_RULE_COMPLY)) &&
	  plane->isThereBadEventAhead(this) >= 0) {
    if (!(plane->laneType() & LANE_TYPE_RAMP)) {

	  // Lane change to the left is allowed, and there is no critical
	  // event ahead in the left lane.

	  // Dan: since lane to left is open, check to see whether driver
	  // is following a bus that is slowing to a stop; if so move over;
          // vehicle may overtake the bus to return to a target lane

	  if (front && (front->type() & STATUS_BUS_SLOWING)
              && (lane_->laneType() & LANE_TYPE_RIGHT_MOST)) {
	    return -1;
	  }

	  //  no change to a lane which is not connected to the target	 
	  int left = * ( find ( _connectedLanes.begin() , _connectedLanes.end() , current-1 ) );
	  if ( left != * ( _connectedLanes.end() ) )  {
	    eul = dlcExpOfUtility(plane);
	  }
    }
    
  }
		
  if (isReadyForNextDLC(1) &&
	  (plane = lane_->right()) &&
	  (!flag(FLAG_VMS_LANE_USE_LEFT) ||
	   plane->localIndex() < VmsLaneUsePivotToIndex(flag())) &&
	  (rules & LANE_CHANGE_RIGHT ||
	   !attr(ATTR_GLC_RULE_COMPLY)) &&
	  plane->isThereBadEventAhead(this) >= 0) {
    if (!(plane->laneType() & LANE_TYPE_RAMP)) {

	  // Change to the right is allowed, and there is no critical
	  // event ahead.

	  // Dan: since lane to right is open, check to see whether driver
	  // is following a bus that is slowing to a stop; if so move over;
          // vehicle may overtake the bus to return to a target lane

	  if (front && (front->type() & STATUS_BUS_SLOWING)
              && (lane_->laneType() & LANE_TYPE_LEFT_MOST)) {
	    return 1;
	  }

          int right = * ( find ( _connectedLanes.begin() , _connectedLanes.end() , current+1 ) );
          if ( right != * ( _connectedLanes.end() ) )  {
            eur = dlcExpOfUtility(plane);
          } 
    } 

  }

  double sum = 1.0 + eul + eur ;
  double rnd = theRandomizer->urandom() ;

  float *b = theParameter->dlcUnsatisfactoryParamsKazi() ;

  float desSpeed = b[0] * tsSegment()->speed();

  // Addressing problem of no vehicles changing from slow-moving lane
  // into adjacent fast-moving lane  (Angus)
  //  if (plane) {
  //    float desSpeed = b[0] * Max(tsSegment()->speed(), plane->speed());    
  //  }
  // end fix

  float heavy_dummy = isType(VEHICLE_SMALL) ? 0.0 : b[3];
  float taleGateDummy = 0;
  TS_Vehicle* behind = this->vehicleBehind() ;
  if (behind) {
    double gap_behind = behind->gapDistance(this);
    float dens = tsSegment()->density();
    taleGateDummy = (gap_behind <= b[5] && dens <= b[6])? b[4] : 0;
  }
 
  float utilUnhappy = b[1] + b[2] * (currentSpeed_ - desSpeed)
    + heavy_dummy + taleGateDummy;
  float probUnhappy = 1 / (1 + exp(-utilUnhappy));

  // Calculate the probability of staying in current lane

  float probOfCurrentLane = 1 - probUnhappy * ((eul+eur) / sum);
  float probOfCL_LL = probOfCurrentLane + probUnhappy* (eul / sum);
  if (rnd < probOfCurrentLane) change = 0 ;
  else if (rnd < probOfCL_LL) change = -1 ;
  else change = 1 ;

  return change;

}





/*
 *--------------------------------------------------------------------
 * This is the lane changing model.
 *
 * This function sets bits 4-7 of the variable 'status'.
 *
 * The fourth and fifth bit indicate current lane change status,
 * and the bits should be masked by:
 *
 *  8=STATUS_RIGHT
 * 16=STATUS_LEFT
 * 24=STATUS_CHANGING
 *
 * This function is invoked when the countdown clock cfTimer is 0.
 * It returns a non-zero value if the vehicle needs a lane change,
 * and 0 otherwise.
 *--------------------------------------------------------------------
 */

int
TS_Vehicle::makeLaneChangingDecision()
{
  

  //if (nextLink_){

  // int debug =nextLink_->code();
  // int debug2=this->link()->code();

  //if ((this->link()->code())<500){

  //if (((nextLink_->code())<10)&&((this->link()->code())>9)){   

  //if (distance_ <= 10.0){// it's time to choose lanes after the intersection, it's not stopped vehicle

  //assignNextIntLane();
  //return(0);//this basically restricts lane changes in the last 
  //}
  //}
  //}  
  //} 
  // No lane change if there exist only one lane.
    
  // int lanid,linkid;
  //  linkid = this->link()->code();
  //  lanid = lane_->code();
  if (segment()->nLanes() <= 1) return (0);

  // No lane change if vehicle is lost. (Angus)
  //  if ( !nextLink_ && !(lane_->laneType_ & LANE_TYPE_BOUNDARY) ) return (0);

  // Vehicle is hold by trace dialog

  if (desiredSpeed_ < SPEED_EPSILON) return 0 ;

  if (timeSinceTagged() < theParameter->mlcMinTimeInLane()) return 0;

  int change = 0;		// direction to change 


  // tomer - no look ahead for vehicles without a path.

#ifndef USE_KAZI_MODELS

  if (!path()) {

    change = checkForLC();
  } 

#else

  if (!path()) {
  
    if (checkIfMandatory())	// mandatory change 
      {
	change = checkMandatoryLaneChanging();
      } 
    else			// discretionary change 
	{
	  change = checkDiscretionaryLaneChanging();
	}
  }
 
#endif
  
  else {                        // for vehicles which have a path

    vector <int> _connectedLanes;    // list of local indexes to connected lanes in the current segment

    for (int i = 0; i < segment()->nLanes(); i ++)
      {
	_connectedLanes.push_back(i);
      }

    if (checkIfLookAheadEvents())
      {
	//    int des;
	//    des = this->desNode()->code();
       	change = checkMandatoryEventLC();
	// change = checkForLookAheadLC(_connectedLanes);
	//   change = change;
      }
    else
      {
#ifndef USE_KAZI_MODELS
	//       int connf,connl,connsize;
      

	change = checkForLookAheadLC(_connectedLanes);
	//    connf = _connectedLanes[0];
       
	//   connsize = _connectedLanes.size();
	//  connl = _connectedLanes[connsize-1];
#else

	change = checkMandatoryLookAheadLC(_connectedLanes);
	
	if (!change) 
	  {
	    change = checkDiscretionaryLookAheadLC(_connectedLanes);
	  }
#endif
	//   connl = connl + 1;
	//   connl = connl - 1;

      }
  }
 
  if (change == -1) {		// to left
	unsetStatus(STATUS_CHANGING);
	setStatus(STATUS_LEFT);
  } else if (change == 1) {	// to right
	unsetStatus(STATUS_CHANGING);
	setStatus(STATUS_RIGHT);
  } else { // if (!flag(FLAG_VMS_LANE_USE_DIR)) {
	unsetStatus(STATUS_CHANGING);
  }

  return (change);
}


/*
 *-------------------------------------------------------------------
 * Check if the gaps for a lane change is acceptable. If yes, make
 * it.
 *-------------------------------------------------------------------
 */

void
TS_Vehicle::executeLaneChanging()
{
  // SET/UNSET FLAGS

  unsetFlag(FLAG_LC_FAILED);

   // IMMEDIATE TARGET LANE

  TS_Lane *plane;

  if (status(STATUS_LEFT)) {
	plane = lane_->left();
  } else if (status(STATUS_RIGHT)) {
	plane = lane_->right();
  } else {
	return;			// No request for lane change 
  }

  // Fix for sporadic crashes due to null pointer.  (Angus)
  if (!plane) return;

  if (isInIncidentArea(plane)) return;

   // LEADING AND LAG VEHICLES (do not have to be in same segment)

  TS_Vehicle* av = findFrontBumperLeader(plane);
  TS_Vehicle* bv = findFrontBumperFollower(plane);

   // LEADING VEHICLE IN TARGET LANE (must be in same segment).

  TS_Vehicle* front;
  if (av) {
	if (av->segment() == segment()) {
	  front = av;
	} else {
	  front = NULL;
	}
  } else {
	front = NULL;
  }

  // LEADING HEADWAY
 
  float aheadway;		// leading headway 
  if (av) {
	aheadway = this->gapDistance(av);
  } else {
	aheadway = FLT_INF;
  }

  // LAG HEADWAY

  float bheadway;		// lag headway 
  if (bv) {
	bheadway = bv->gapDistance(this);
  } else {
	bheadway = FLT_INF;
  }
  //  #ifndef USE_KAZI_MODELS
  //if (av)
  //  { 
  //  if (av->link() != this->link()) 
  //{
  //float sum = 0;
	
  //    if (this->segment() != this->link()->endSegment())
  //{    
  //  TS_Lane *test = (TS_Lane *)this->lane()->dnLane(0);
  //      sum = sum + test->length();
          
  //     while(!(test->segment() == test->link()->endSegment()))
  //   {
  //     test = test->dnLane(0);
  //         sum = sum + test->length();
  //       }
          
  //    }
  //      aheadway = this->distance_ + sum;
  //    }
      
  //}
  //else
  //{ 
  //  	float sum = 0;
	
  //    if (this->segment() != this->link()->endSegment())
  //{    
  //  TS_Lane *test = (TS_Lane *)this->lane()->dnLane(0);
  //      sum = sum + test->length();
          
  //     while(!(test->segment() == test->link()->endSegment()))
  //   {
  //     test = test->dnLane(0);
  //         sum = sum + test->length();
  //       }
          
  //    }
  //      aheadway = this->distance_ + sum;

  //}
  //#endif

  // Is the lane change is triggered by special events

  int escape = flag(FLAG_ESCAPE);

  int lctype;
  if ( status(STATUS_MANDATORY) && 
       ( !status(STATUS_CURRENT_OK) || path() ) ) {
    // "status(STATUS_CURRENT_OK)" only looks to next link, conflicting
    //  with lookahead, so only use when vehicle has no path (Angus)

  	lctype = 2;		// wrong lane and concerned
    } else if (escape) {
  	lctype = 2;		// special event
    } else {
  	lctype = 0;		// discretionary
    }


#ifndef USE_KAZI_MODELS
	
//if (!(plane->laneType() & LANE_TYPE_RAMP)){ //anita, gunwoo - when vehicles are not on-ramp
float sta = status(STATUS_MANDATORY);
	if(!status(STATUS_MANDATORY)) {
  if (bv && bheadway < theParameter->lcCriticalGap(1, bv->currentSpeed_ - currentSpeed_ ,this)) {
 
#else
 
  if (bv && bheadway < theParameter->lcCriticalGap
	  (lctype + 1, dis2stop_, bv->currentSpeed_,
	   bv->currentSpeed_ - currentSpeed_, bv)) {
#endif
	setFlag(FLAG_LC_FAILED_LAG); // lag gap
#ifndef USE_KAZI_MODELS

	//check later cfc
  } else if ( (av && aheadway < theParameter->lcCriticalGap(0, av->currentSpeed_ - currentSpeed_, this)) || ( (!av) && aheadway < theParameter->lcCriticalGap(0,- currentSpeed_, this) )) {
 
#else

  }else if (av && aheadway < theParameter->lcCriticalGap
			 (lctype, dis2stop_, currentSpeed_,
			  av->currentSpeed_ - currentSpeed_, this)) {
  
#endif
	setFlag(FLAG_LC_FAILED_LEAD); // lead gap
  }
// Anita - added nosing compoment here - Kazi's nosing when vehicle is not onramp. Until return statement

    
 

#ifdef USE_KAZI_MODELS
	if (!lctype) {		// Not a mandatory lane change.
	  return;
	             }
#endif

	// Check for special cases
	if (dis2stop_ < length() &&
		(!av || aheadway + bheadway >
		 2.0 * theParameter->lcMinGap(lctype))) {

	  // this vehicle is very close to the critical point and
	  // the lead gap is available.

	  if (segment()->isHead() && link()->nDnLinks() > 1) { // fork area
		goto execution;
	  } else if (flag(FLAG_STUCK_AT_END)) {
		if (!bv ||
			bv->yieldVehicle_ == this ||
			timeSinceTagged() > theParameter->lcMaxStuckTime()) {
		  // a lane drop, incident, etc
		  goto execution;
		}
	  }
	}
  
	int nlanes = lane_->isWrongLane(this);

#ifndef USE_KAZI_MODELS
   	if (nlanes == 0 && !lctype) {
	  return;
	    }
#endif
	// The gaps are not acceptable and this guy is in mandatory
	// state.  Check if this vehicle is nosing or becomes aggressive
	// and decide to nose in.

	int nosing = flag(FLAG_NOSING);

	if (!(flag(FLAG_NOSING)) && dis2stop_ < theParameter->lcMaxNosingDis()) {

	  float gap = aheadway + bheadway ;
	  float dv = av ? av->currentSpeed_ - currentSpeed_ : 0 ;


//  Original model - Assumes that forced merging must be completed before end of
//                   current link, causing problems when links are short.
	  // float pmf = theParameter->lcNosingProb(dis2stop_, dv, gap, nlanes) ;

//  Revised model - If the current lane is OK (i.e. it connects to the next link),
//                  merging can wait at least until the next link.  This is not
//                  as good as using Tomer's lookahead, but it is an improvement
//                  over the original model. (Angus)
	  float pmf;

	  if ( status(STATUS_CURRENT_OK) && nextLink() ) {
	    // current lane connects to next link on path

	    float longer_dis = dis2stop_ + nextLink()->startSegment()->length() ;
	    pmf = theParameter->lcNosingProb(longer_dis, dv, gap, nlanes) ;
	    // remaining distance for forced merging is the length remaining
	    // in the current link plus at least the length of the first
	    // segment of the next link
	  } 
	  else {
	    // current lane does not connect to next link on path

	    pmf = theParameter->lcNosingProb(dis2stop_, dv, gap, nlanes) ;
	    // forced merging must be performed in current link
	  }
//  end Revised model

	  nosing = theRandomizer->brandom(pmf) ;
	}

	unsetFlag(FLAG_NOSING);	// reset the flag

	if (nosing) {

	  lcProdNoseRejProb_ = 1.0;

	  // Since I am nosing, updating of acceleration rate sooner

	  cfTimer_ = CF_CRITICAL_TIMER_RATIO * nextStepSize();

	  // Now I am going to nose in provided it is feasible and the
	  // lag vehicle is willing to yield

	  if (checkNosingFeasibility(av, bv, dis2stop_) &&
		  (!bv ||
		  bv->willYield(escape?YIELD_TYPE_ESCAPE:YIELD_TYPE_CONNECTION))) {

		setFlag(FLAG_NOSING_FEASIBLE);

		// Nosing is feasible

		if (bv) {

		  // There is a lag vehicle in the target lane

		  bv->yieldVehicle_ = this;
		  if (!(bv->isBus() && bv->status(STATUS_STOPPED))) {
		    bv->cfTimer_ = Min(cfTimer_, bv->cfTimer_);
		  }

		  if (!bv->flag(FLAG_YIELDING)) {
			bv->yieldTime_ = theSimulationClock->currentTime();
		  }
		  if (status(STATUS_LEFT)) {
			setFlag(FLAG_NOSING_LEFT);
			bv->setFlag(FLAG_YIELDING_RIGHT);
		  } else {
			setFlag(FLAG_NOSING_RIGHT);
			bv->setFlag(FLAG_YIELDING_LEFT);
		  }

		} else {

		  // No lag vehicle in the target lane

		  if (status(STATUS_LEFT)) {
			setFlag(FLAG_NOSING_LEFT);
		  } else {
			setFlag(FLAG_NOSING_RIGHT);
		  }
		}

		// Check if the minimum gaps are available.

		if (bheadway > theParameter->lcMinGap(lctype + 1) &&
			aheadway > theParameter->lcMinGap(lctype)) {
			if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES)) //Anita - test
	  
		  goto execution;
		}

	  } else {

		unsetFlag(FLAG_NOSING_FEASIBLE);

		// Nosing is not feasible, but maintain the nosing state

		if (status(STATUS_LEFT)) {
		  setFlag(FLAG_NOSING_LEFT);
		} else {
		  setFlag(FLAG_NOSING_RIGHT);
		}
	  }
	}

	return;
	//  }
} //bracket of if statement, gunwoo's on-ramp definition
	

#ifndef USE_KAZI_MODELS

else { //gunwoo - apply single stage gap acceptance when vehicles are in on-ramp
  int court=0;
  int nosing=0;	
	
//Anita - CHECK IF THE ADJACENT GAP HAS CHANGED	    
if (prevLead_ !=av || prevLag_ !=bv){ 
  //if (prevLag_ !=bv){ 
 	unsetFlag(FLAG_NOSING_FEASIBLE);
 	unsetFlag(FLAG_NOSING);
 	unsetFlag(FLAG_NOSING_LEFT);
 	unsetFlag(FLAG_NOSING_RIGHT);
	unsetFlag(FLAG_COURTESY);//cfc May 06
	// prevLag_->unsetFlag(FLAG_YIELDING);//cfc May 29


 	
//UPDATE prev vehicles if they have changed 	
 	prevLead_=av;
 	prevLag_=bv;

        
 	}
 	
//Anita - If adjacent gap has changed OR vehicle has not initiated nosing 	

 if(!flag(FLAG_NOSING) && (!flag(FLAG_COURTESY))) { 
   // :: Check Normal gap acceptance

 if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
	    {
	      ostream& os = theFileManager->osLaneUtility();
	      os  << "40" << endc //cfc 26 sep 8=12
		  << this->code() << endc
		   << Fix(theSimulationClock->currentTime(), 1.0) << endc
		  << "0"  <<endc
		  <<10001<< endl;
	    }


//cfc May 06

  //lag gap for single gap acceptance model 

  if (bv && bheadway < theParameter->lcCriticalGap(0,1, bv->currentSpeed_ - currentSpeed_ ,
      dis2stop_,bv->accRate_,lane_->averageSpeed(),currentSpeed_,this)) {  //bv->accRate_: acceleration of lag vehicle
 

	setFlag(FLAG_LC_FAILED_LAG); // lag gap
		
  //lead gap for single gap acceptance model
  } else if (av && aheadway < theParameter->lcCriticalGap(0,0, av->currentSpeed_ - currentSpeed_,
   dis2stop_,av->accRate_,lane_->averageSpeed(),currentSpeed_,this)) {
 

	setFlag(FLAG_LC_FAILED_LEAD); // lead gap
  }

  //#ifndef USE_KAZI_MODELS

 // Available Gaps: Gunwoo May 25

  if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
	    {
	      ostream& os = theFileManager->osLaneUtility();
	      os  << "11" << endc   //cfc 26 sep 7=11
		  << this->code() << endc
		   << Fix(theSimulationClock->currentTime(), 1.0) << endc
		  << bheadway  <<endc
		  <<10001<< endl;
	    }

   if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
	    {
	      ostream& os = theFileManager->osLaneUtility();
	      os  << "10" << endc  //cfc 26 sep 6=10
		  << this->code() << endc
		   << Fix(theSimulationClock->currentTime(), 1.0) << endc
		  << aheadway <<endc
		  <<10001<< endl;
	    }
  

   //   if (!(flag(FLAG_LC_FAILED)))   // Varun : 01/14/2007 --> level 3 probability computation
   // {
   //  int prob_acc;
   //  prob_acc = theParameter->lcGapAcc(this);
   //  prob_acc = 1;
   //  if (prob_acc == 0)
   // { setFlag(FLAG_LC_FAILED); }       
   // }
       
   // Check if  Gaps acceptable/not : Gunwoo May 25: 0 if critical gap is smaller than available gap (Acceptable)
  
if (!(flag(FLAG_LC_FAILED))){ //gunwoo

    if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
	    {
	      ostream& os = theFileManager->osLaneUtility();
	      os  << "12" << endc //cfc 26 sep 8=12
		  << this->code() << endc
		   << Fix(theSimulationClock->currentTime(), 1.0) << endc
		  << "0"  <<endc
		  <<10001<< endl;
	    }



    goto execution;
    
  }else{

    //writing whether gap acceptance is acceptable or not, 1 if it is failed, 0 if it is acceptable
        if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
	    {
	   
	     // ******** May 25, Gunwoo **************//
	     // 1 if critical gap is larger than available gap (Failed)
	      ostream& os = theFileManager->osLaneUtility();
	      os  << "12" << endc //cfc 26 sep 8=12
		  << this->code() << endc
		   << Fix(theSimulationClock->currentTime(), 1.0) << endc
		  << "1"  <<endc
		  <<10001<< endl;
	    }
  }


 
} //end of if(!flag(FLAG_NOSING)&&(!flag(FLAG_COURTESY)))  
 //#endif

 //not sure check cfc 
 //cfc May 28

 	// Check for special cases: ensures smooth simulation 

	//if (dis2stop_ < length() &&
	//	(!av || aheadway + bheadway >
 // 2.0 * theParameter->lcMinGap(lctype))) {

	  // this vehicle is very close to the critical point and
	  // the lead gap is available.

 //  if (segment()->isHead() && link()->nDnLinks() > 1) { // fork area
 //	goto execution;

 //  } else if (flag(FLAG_STUCK_AT_END)) {
 //	if (!bv ||
 //		bv->yieldVehicle_ == this ||
 //		timeSinceTagged() > theParameter->lcMaxStuckTime()) {
		  // a lane drop, incident, etc
 //	  goto execution;
 //	}
 //  }
 //}





	int nlanes = lane_->isWrongLane(this);

        #ifndef USE_KAZI_MODELS
   	if (nlanes == 0 && !lctype) {
	  return;
	}
        #endif


court=flag(FLAG_COURTESY);
nosing=flag(FLAG_NOSING);

if ( (flag(FLAG_LC_FAILED)) || (flag(FLAG_NOSING))||(flag(FLAG_COURTESY)) ) 

{ 

//compares flags to FLAG_LC_FAILED - if lead or lag failed then true: check courtesy, forced


// :: Normal failed check courtesy

 if  ((!flag(FLAG_COURTESY))&&(!flag(FLAG_NOSING))) {

	TS_Parameter& p = *theParameter;
        float aTime;
        aTime = p.aUpper(driverGroup.aUpper) ;
	float aGap;
	float leadSpeed;
	float lagSpeed;
	float leadAcc;
	float lagAcc;
	float aheadway;		// leading headway 
	float bheadway;		// lag headway 
  
if (av) {

  aheadway= this->gapDistance(av);
  leadSpeed=av->currentSpeed_;
  leadAcc=av->accRate_;

  } else {

	aheadway = FLT_INF;
        leadSpeed= FLT_INF;
        leadAcc= FLT_INF;

  }

   
  if (bv) {
	bheadway = bv->gapDistance(this);
        lagSpeed=bv->currentSpeed_;
        lagAcc=bv->accRate_;
  } else {
	bheadway = FLT_INF;
        lagSpeed= 0;
        lagAcc= 0;
  }

  // Anticipated gap
      aGap = aheadway + bheadway + aTime*(leadSpeed-lagSpeed )+aTime*aTime*(leadAcc - lagAcc) ;
       
	
      float dens =lane_->density();

      if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
	    {
	      ostream& os = theFileManager->osLaneUtility();
	      os  << "100" << endc // initate courtesy merging
		        << this->code() << endc
		        << Fix(theSimulationClock->currentTime(), 1.0) << endc
		        << aGap  <<endc
		        <<10001<< endl;
	    } 





 

      if (aGap > theParameter->lcAnticipatedGap(lagSpeed - currentSpeed_ ,dens,dis2stop_,lagAcc,this)) 
	{ 
      setFlag(FLAG_COURTESY);
      court=flag(FLAG_COURTESY);

      if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
	    {
	      ostream& os = theFileManager->osLaneUtility();
	      os  << "200" << endc //anita - initate force merging
		        << this->code() << endc
		        << Fix(theSimulationClock->currentTime(), 1.0) << endc
		        << "0"  <<endc
		        <<10001<< endl;
	    }
 
	

	}
     else
	  { if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
	    {
	      ostream& os = theFileManager->osLaneUtility();
	      os  << "200" << endc //anita - initate force merging
		        << this->code() << endc
		        << Fix(theSimulationClock->currentTime(), 1.0) << endc
		        << "1"  <<endc
		        <<10001<< endl;
	    } 
	  }
 }

 

// :: update courtesy if in that state in previous time step



if ((flag(FLAG_COURTESY))&&(!flag(FLAG_NOSING))){
//VERIFY COURTESY 
 
          if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
	    {
	      ostream& os = theFileManager->osLaneUtility();
	      os  << "20" << endc // initate courtesy merging
		        << this->code() << endc
		        << Fix(theSimulationClock->currentTime(), 1.0) << endc
		        << "0"  <<endc
		        <<10001<< endl;
	    }
}

   
    
   
   
              
         
 /***********************************************/
 // ::  normal and courtesy gaps are not acceptable and this guy is in mandatory
 // :: state.  Check if this vehicle is nosing or becomes aggressive
 // :: and decide to nose in.
   
  
 else if(!flag(FLAG_COURTESY)){

   if ((!flag(FLAG_NOSING))){  // :: not nosing already
	    //&& dis2stop_ < theParameter->lcMaxNosingDis()) {//(2)

	  float heavyLag, pmf;

	  
	  if (bv && bv->isType(VEHICLE_SMALL) )
	     {heavyLag = 1.0; }
	  else {heavyLag = 0.0;}
	  	
	  pmf = theParameter->lcNosingProb(heavyLag, this);
	 
	  nosing = theRandomizer->brandom(pmf) ;


	  if (nosing>0){
	    setFlag(FLAG_NOSING);}

	}// nosing updated
        }//if (!flag(FLAG_COURTESY)
    
         //VERIFY FORCED 
 
// int cfc=flag(FLAG_NOSING);       
 if ((flag(FLAG_NOSING))&&(!flag(FLAG_COURTESY)))
	    {
	      // setFlag(FLAG_NOSING);
          if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
	    {
	      ostream& os = theFileManager->osLaneUtility();
	      os  << "30" << endc //anita - initate force merging
		        << this->code() << endc
		        << Fix(theSimulationClock->currentTime(), 1.0) << endc
		        << "0"  <<endc
		        <<10001<< endl;
	    } 

	    }
	    
	    


	//unsetFlag(FLAG_NOSING);	// reset the flag

 //checking feasibilty and updating accelration

 if ((flag(FLAG_NOSING))||(flag(FLAG_COURTESY))){      
          lcProdNoseRejProb_ = 1.0;

	  // Since I am nosing, updating of acceleration rate sooner

	  cfTimer_ = CF_CRITICAL_TIMER_RATIO * nextStepSize(); //ratio is 0.5

	  // Now I am going to nose in provided it is feasible and the
	  // lag vehicle is willing to yield

	  if (checkNosingFeasibility(av, bv, dis2stop_) && (!bv ||
		  bv->willYield(escape?YIELD_TYPE_ESCAPE:YIELD_TYPE_CONNECTION))) {

		setFlag(FLAG_NOSING_FEASIBLE);

		// Nosing is feasible

		if (bv) {

		  // There is a lag vehicle in the target lane

		  bv->yieldVehicle_ = this;
		  if (!(bv->isBus() && bv->status(STATUS_STOPPED))) {
		    bv->cfTimer_ = Min(  cfTimer_, bv->cfTimer_);
		  }

		  if (!bv->flag(FLAG_YIELDING)) {
		    bv->yieldTime_ = Max(1,theSimulationClock->currentTime());//cfc
		  }
		  if (status(STATUS_LEFT)) {
			setFlag(FLAG_NOSING_LEFT);
			bv->setFlag(FLAG_YIELDING_RIGHT);
		  } else {
			setFlag(FLAG_NOSING_RIGHT);
			bv->setFlag(FLAG_YIELDING_LEFT);
		  }

		} else {

		  // No lag vehicle in the target lane

		  if (status(STATUS_LEFT)) {
			setFlag(FLAG_NOSING_LEFT);
		  } else {
			setFlag(FLAG_NOSING_RIGHT);
		  }
		}
		//}

	  //}   //checking feasibility if in nosing or courtesy state


 
// Courtesy Merge/Forced merge is feasible but update depending on  gap  acceptance

if ( flag(FLAG_COURTESY)){ 

  if (bv && bheadway < theParameter->lcCriticalGap(2,1, bv->currentSpeed_ - currentSpeed_ ,
      dis2stop_,bv->accRate_,lane_->averageSpeed(),currentSpeed_,this)) {  //bv->accRate_: acceleration of lag vehicle
 

	setFlag(FLAG_LC_FAILED_LAG); // lag gap
		

	//lead gap for single gap acceptance model
  } else if (av && aheadway < theParameter->lcCriticalGap(2,0, av->currentSpeed_ - currentSpeed_,
   dis2stop_,av->accRate_,lane_->averageSpeed(),currentSpeed_,this)) {
 

	setFlag(FLAG_LC_FAILED_LEAD); // lead gap
  }

 if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
	    {
	      ostream& os = theFileManager->osLaneUtility();
	      os  << "11" << endc   //cfc 26 sep 7=11
		  << this->code() << endc
		   << Fix(theSimulationClock->currentTime(), 1.0) << endc
		  << bheadway  <<endc
		  <<10001<< endl;
	    }

   if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
	    {
	      ostream& os = theFileManager->osLaneUtility();
	      os  << "10" << endc  //cfc 26 sep 6=10
		  << this->code() << endc
		   << Fix(theSimulationClock->currentTime(), 1.0) << endc
		  << aheadway <<endc
		  <<10001<< endl;
	    }

    // check if adj gaps acceptable for courtesy merge 0 if critical gap is smaller than available gap (Acceptable)
  
if (!(flag(FLAG_LC_FAILED))){ //gunwoo

    if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
	    {
	      ostream& os = theFileManager->osLaneUtility();
	      os  << "22" << endc //cfc 26 sep 8=12
		  << this->code() << endc
		   << Fix(theSimulationClock->currentTime(), 1.0) << endc
		  << "0"  <<endc
		  <<10001<< endl;
	    }
    unsetFlag(FLAG_NOSING);
    unsetFlag(FLAG_NOSING_LEFT);
    unsetFlag(FLAG_NOSING_RIGHT);
    unsetFlag(FLAG_COURTESY);

    goto execution;
    
  }else{
    // unsetFlag(FLAG_NOSING);
    setFlag(FLAG_COURTESY);
    unsetFlag(FLAG_NOSING);
    unsetFlag(FLAG_NOSING_LEFT);
    unsetFlag(FLAG_NOSING_RIGHT);

    //writing whether gap acceptance is acceptable or not, 1 if it is failed, 0 if it is acceptable
        if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
	    {
	   
	     // ******** May 25, Gunwoo **************//
	     // 1 if critical gap is larger than available gap (Failed)
	      ostream& os = theFileManager->osLaneUtility();
	      os  << "22" << endc //cfc 26 sep 8=12
		  << this->code() << endc
		   << Fix(theSimulationClock->currentTime(), 1.0) << endc
		  << "1"  <<endc
		  <<10001<< endl;
	    }
	return;
  }



 }//courtesy & feasible		



if ((flag(FLAG_NOSING)) && (!flag(FLAG_COURTESY))){

if (bv && bheadway < theParameter->lcCriticalGap(1,1, bv->currentSpeed_ - currentSpeed_ ,
      dis2stop_,bv->accRate_,lane_->averageSpeed(),currentSpeed_,this)) {  //bv->accRate_: acceleration of lag vehicle
 

	setFlag(FLAG_LC_FAILED_LAG); // lag gap
		

	//lead gap for single gap acceptance model
  } else if (av && aheadway < theParameter->lcCriticalGap(1,0, av->currentSpeed_ - currentSpeed_,
   dis2stop_,av->accRate_,lane_->averageSpeed(),currentSpeed_,this)) {
 

	setFlag(FLAG_LC_FAILED_LEAD); // lead gap
  }

if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
	    {
	      ostream& os = theFileManager->osLaneUtility();
	      os  << "11" << endc   //cfc 26 sep 7=11
		  << this->code() << endc
		   << Fix(theSimulationClock->currentTime(), 1.0) << endc
		  << bheadway  <<endc
		  <<10001<< endl;
	    }

   if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
	    {
	      ostream& os = theFileManager->osLaneUtility();
	      os  << "10" << endc  //cfc 26 sep 6=10
		  << this->code() << endc
		   << Fix(theSimulationClock->currentTime(), 1.0) << endc
		  << aheadway <<endc
		  <<10001<< endl;
	    }



    // ******** May 25, Gunwoo **************//
	     // 0 if critical gap is smaller than available gap (Acceptable)
  if (!(flag(FLAG_LC_FAILED))){ //gunwoo

    if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
	    {
	      ostream& os = theFileManager->osLaneUtility();
	      os  << "32" << endc //cfc 26 sep 8=12
		  << this->code() << endc
		   << Fix(theSimulationClock->currentTime(), 1.0) << endc
		  << "0"  <<endc
		  <<10001<< endl;
	    }

    unsetFlag(FLAG_NOSING);
    unsetFlag(FLAG_NOSING_LEFT);
    unsetFlag(FLAG_NOSING_RIGHT);
    unsetFlag(FLAG_COURTESY);
    
    goto execution;
    
  }else{ 
    setFlag(FLAG_NOSING);
    unsetFlag(FLAG_COURTESY);
  

    //writing whether gap acceptance is acceptable or not, 1 if it is failed, 0 if it is acceptable
        if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
	    {
	   
	     // ******** May 25, Gunwoo **************//
	     // 1 if critical gap is larger than available gap (Failed)
	      ostream& os = theFileManager->osLaneUtility();
	      os  << "32" << endc //cfc 26 sep 8=12
		  << this->code() << endc
		   << Fix(theSimulationClock->currentTime(), 1.0) << endc
		  << "1"  <<endc
		  <<10001<< endl;
	    }
  }


  
 }//nosing
	    
//moved up	  

//chunk of special cases

	// Check for special cases

	if (dis2stop_ < length() &&
		(!av || aheadway + bheadway >
		 2.0 * theParameter->lcMinGap(lctype))) {

	  // this vehicle is very close to the critical point and
	  // the lead gap is available.

	  if (segment()->isHead() && link()->nDnLinks() > 1) { // fork area
		goto execution;

	  } else if (flag(FLAG_STUCK_AT_END)) {
		if (!bv ||
			bv->yieldVehicle_ == this ||
			timeSinceTagged() > theParameter->lcMaxStuckTime()) {
		  // a lane drop, incident, etc
		  goto execution;
		}
	  }
	}




	int nlanes = lane_->isWrongLane(this);

        #ifndef USE_KAZI_MODELS
   	if (nlanes == 0 && !lctype) {
	  return;
	}
        #endif


	
	//	return ;
 }


	  else{//if nosing not feasible
           unsetFlag(FLAG_NOSING_FEASIBLE);
	   //maintain state
	  if (court){
	    setFlag(FLAG_COURTESY);
            return;}
	  else{
	    setFlag(FLAG_NOSING);
	    return;}
	  }//if nosing not feasible



	  //return;

 }

 //return;//debug


 }//LC Failed||courtesy||nosing

return;//debug	    
}
	
 
// gunwoo - writing the gap acceptance.
#endif
 
 execution:

  // Count number of discretionary lane changes
  // tomer for lc recording.

	    if (theEngine->chosenOutput(OUTPUT_LANE_CHANGING))
	    {
	      ostream& os = theFileManager->osLaneChanging();
	      //	      os  << plane->code() << endc;
	      writeLaneChanging(plane);
	    }

#ifdef COUNT_DLC
  if (lctype == 0) {
      tsLink()->num_dlc ++ ;
  }
#endif

   // MAKE THE LANE CHANGE

   // Record the direction of change

  unsetFlag(FLAG_PREV_LC);	// clean bits
  if (status(STATUS_LEFT)) {
	unsetFlag(FLAG_PREV_LC_LEFT);
  } else {
	unsetFlag(FLAG_PREV_LC_RIGHT);
  }

  // REMOVE THIS VEHICLE FROM CURRENT LANE
    
  this->remove();

  // INSERT THIS VEHICLE TO TARGET LANE

  leading_ = front;

  if (front) {
	trailing_ = front->trailing_;
	front->trailing_ = this;
  } else {
	trailing_ = plane->firstVehicle();

      // This vehicle becomes the first vehicle in the target lane.
       
	plane->firstVehicle(this);
  }

  if (trailing_) {
	trailing_->leading_ = this;
  } else {
	// This vehicle becomes the last vehicle in the target lane.
       
	plane->lastVehicle(this);
  }

  plane->nVehicles(+1);

   // UPDATE SOME VARIABLES

  lane_ = plane;
  setStatus();
  assignNextLane();
  cfTimer_ = 0.0;
  lcTimeTag_ = theSimulationClock->currentTime();

  unsetStatus(STATUS_CHANGING);
  unsetStatus(STATUS_MANDATORY); // Angus
  unsetFlag(FLAG_NOSING | FLAG_YIELDING | FLAG_LC_FAILED|FLAG_COURTESY);
  unsetFlag(FLAG_VMS_LANE_USE_BITS | FLAG_ESCAPE | FLAG_AVOID);
  unsetFlag(FLAG_STUCK_AT_END | FLAG_NOSING_FEASIBLE);

#ifndef USE_KAZI_MODELS

  unsetStatus(STATUS_TARGET_GAP);

#endif

  // ERASE PREVIOUS IMAGE

#ifdef INTERNAL_GUI
  if (theDrawingArea->isVehicleDrawable()) {
	((TS_DrawableVehicle*)this)->erase(theDrawingArea);
  }
#endif
	}

	
// Returns 1 if the vehicle can nose in or 0 otherwise

int
TS_Vehicle::checkNosingFeasibility(TS_Vehicle *av, TS_Vehicle *bv, float dis2stop)
{
  if (flag(FLAG_STUCK_AT_END)) {
	if (timeSinceTagged() > theParameter->lcMaxStuckTime()) {
	  // If one stuck for a very long time, skip the feasibility check
	  return 1;
	}
  } else {
	if (dis2stop < length() && currentSpeed_ < SPEED_EPSILON) {
	  // Set the vehicle as stuck if it is close to the end
	  setFlag(FLAG_STUCK_AT_END);
	  //	  lcTimeTag_ = theSimulationClock->currentTime();
	}
  }

  // Constraints of acceleration rate in response to lead and lag
  // vehicles

  float lower = -FLT_INF;
  float upper = FLT_INF;

  if (av) {

	if ((av->flag(FLAG_NOSING) || av->flag(FLAG_YIELDING)) &&
		gapDistance(av) < 2.0 * theParameter->lcMinGap(2)) {

	  // The lead vehicle is yeilding or nosing
	  return 0;		// To avoid dead lock

	} else if (flag(FLAG_LC_FAILED_LEAD)) {

	  // Acceleration rate in order to be slower than the leader

	  upper = (av->currentSpeed_ - currentSpeed_) /
		theParameter->lcNosingConstStateTime() +
		av->accRate_;

	  if (upper < maxDeceleration_) {
		return 0;		// This vehicle is too fast
	  }
	} else if (av->currentSpeed_ < SPEED_EPSILON &&
			   dis2stop > distanceToNormalStop() &&
			   gapDistance(leading_) > distanceToNormalStop()) {
	  return 0;
	}
  }
 
  if (bv) {

        if (isBus() && flag(FLAG_NOSING)) {

	  // Dan: bus must force it's way along the route, and
	  // other vehicles will generally yield;
	  // Acceleration rate in order to be faster than the lag
	  // vehicle and do not cause the lag vehicle to decelerate
	  // harder than its normal deceleration rate

          lower = (bv->currentSpeed_ - currentSpeed_) /
	  theParameter->lcNosingConstStateTime() +
	  normalDeceleration_;

	  if (lower > maxAcceleration_) {	// I am will to acc hard

	    // This vehicle is too slow or close to the lag vehicle

	    return 0;
	  }

	} else if (bv->flag(FLAG_NOSING) ||
		bv->flag(FLAG_YIELDING) &&
		bv->yieldVehicle_ != this &&
		bv->gapDistance(this) < 2.0 * theParameter->lcMinGap(3)) {

	  // The lag vehicle is nosing or yielding to another vehicle or
	  // not willing to yield

	  return 0;		// To avoid dead lock

	} else if (!theRandomizer->brandom(theParameter->lcYieldingProb(flag(FLAG_YIELDING) ? 1 : 0))) {

	  // The lag vehicle is not willing to yield

	  return 0;		// Skip in this iteration

	} else if (flag(FLAG_LC_FAILED_LAG)) {
	
	  // Acceleration rate in order to be faster than the lag
	  // vehicle and do not cause the lag vehicle to decelerate
	  // harder than its normal deceleration rate

	  lower = (bv->currentSpeed_ - currentSpeed_) /
		theParameter->lcNosingConstStateTime() +
		normalDeceleration_;

	  if (lower > maxAcceleration_) {	// I am will to acc hard

		// This vehicle is too slow or close to the lag vehicle

		return 0;
	  }
	}
  }

  // Check if there exists a feasible acceleration rate for this
  // vehicle

  if (lower > upper) return 0;
  else return 1;
}


// Make sure there is no vehicle nose in at the same position.

int
TS_Vehicle::canNoseIn(TS_Vehicle *av, TS_Vehicle *bv)
{
  TS_Vehicle *pv;

  if (av &&
	  (pv = av->vehicleAhead()) &&
	  av->gapDistance(pv) < 0.0) {
	return 0;
  }
  if (bv &&
	  (pv = bv->vehicleBehind()) &&
	  pv->gapDistance(bv) < 0.0) {
	return 0;
  }
  return 1;
}


/*
 *-------------------------------------------------------------------
 * Count the position in queue: the number of stopped vehicles in
 * front of this vehicle.
 *-------------------------------------------------------------------
 */

void
TS_Vehicle::countPositionInQueue()
{
  TS_Vehicle *front = this->vehicleAhead();
  if (front && front->currentSpeed_ < SPEED_EPSILON) {
	positionInQueue_ = front->positionInQueue_ + 1;
  } else {
	positionInQueue_ = 0;
  }
}


// Loop downstream devices/events and find if there is any event block
// the lane or requires a lane change.  It returns:
//
//  -1 = bad event and requires a mandatory lane change
//   0 = nothing serious
//   1 = bad event and requires a discretionary lane change

int
TS_Vehicle::isThereBadEventInList(TS_Lane *reflane,	// 

  CtrlList ctrl,                     // next control device or event
  float vis,                         // visibility
  float x,                           // current distance to reference point
  float *pdis2stop,	             // distance to critical point (output)
  float *pvis)                       // visibility of constrained device
{
  RN_Segment* ps;
  float dis;
  register int i;
  int bad = 0;
  unsigned int state;
  RN_Signal *signal;
  float dis2stop = x;		// this is the maximum
  float thevis = vis;
  RN_CtrlStation *station;
  float dis2busstop = 0.;
  float bsvis = 0.;
  int isBusStop = 0;
  float dis2incident = 0.;
  int isIncident = 0;

  unsigned int signature = 1 << reflane->localIndex();

  while (bad >= 0 && ctrl && (station = ctrl->data()) &&
		 (dis = x - station->distance()) < vis) {

	float vs = station->visibility();

	if (dis <= vs) {
	  ps = station->segment();
	  int dlanes = ps->nLanes();

	  for (i = 0; bad >= 0 && i < dlanes; i ++) {

		if (!ps->lane(i)->isConnected(signature) ||
			!(signal = station->signal(i))) continue;

		// check each marked downstream lane
	       
		state = signal->state();

		switch (station->type()) {
		case CTRL_LUS:
		  {
			if (attr(ATTR_LUS_COMPLY_RED) &&
				(state & SIGNAL_COLOR) == SIGNAL_RED) {
			  bad = -1;
			  dis2stop = dis;
			  thevis = vs;
			} else if (attr(ATTR_LUS_COMPLY_YELLOW) &&
					   (state & SIGNAL_COLOR) == SIGNAL_YELLOW) {
			  bad = 1;
			}
			break;
		  }
		case CTRL_TOLLBOOTH:
		  {
			if ((state & SIGNAL_COLOR) <= SIGNAL_RED)  {
			  bad = 1;
			  dis2stop = dis;
			  thevis = vs;
			}
			break;
		  }
		case CTRL_BUSSTOP:  //margaret
		  {
			if ((state & SIGNAL_COLOR) <= SIGNAL_RED)  {
			  bad = 1;
			  dis2stop = dis2busstop = dis;
                          isBusStop++;
			  thevis = bsvis = vs;
			}
			break;
		  }
		case CTRL_INCIDENT:
		  {
			if ((state & INCI_ACTIVE) &&
				signal->speedLimit() < SPEED_EPSILON) {
			  bad = -1;
			  dis2stop = dis2incident = dis;
                          isIncident++;
			  thevis = vs;
			}
		  }
		}
		break;
	  }
	}
	ctrl = ctrl->next();
  }

  if (isBus() && isBusStop && isIncident && dis2busstop < dis2incident) {
    dis2stop = dis2busstop;
    thevis = bsvis;
    bad = 1;
  }

  if (pdis2stop) *pdis2stop = dis2stop;
  if (pvis) *pvis = thevis;

  return bad;
}


bool TS_Vehicle::willYield(unsigned int reason)
{
  unsigned int n;

  if (!(reason & (YIELD_REASON & yield_)) || // not the same reason
	  lane_->localIndex() != YieldSignatureToLaneIndex(yield_)) {
	// not the same position
	  
	// register the signature and generate the random number
	n = theParameter->mlcNumberToYield();
	yield_ = reason + LaneIndexToYieldSignature(lane_->localIndex()) + n;
  }

  n = yield_ & YIELD_COUNTER;
  if (!n) return false;

  yield_ = (yield_ & ~YIELD_COUNTER) + (n - 1);
  return true;
}

// Speed of the vehicle ahead within a given look ahead range in the
// target lane.

float
TS_Vehicle::speedAhead(TS_Lane *thelane)
{
  float range = theParameter->lcSpdLookAheadDistance();

  // Look the vehicle in current segment

  TS_Vehicle *pv = macroLeading_;
  while (pv && pv->distance_ + range > distance_) {
	if (pv->lane_ == thelane) {
	  return pv->currentSpeed_;
	}
	pv = pv->macroLeading_;
  }

  // No vehicle found in current segment, distance to look in the
  // downstream lane.

  range -= distance_;

  if (range <= 0) {
	return desiredSpeed_;
  }

  // Find the closest vehicle in the downstream lane(s)
   
  TS_Lane* pl;
  register int i, n = thelane->nDnLanes();
  TS_Vehicle *front = NULL;

  for (i = 0; i < n; i ++) {
	pl = thelane->dnLane(i);
	pv = pl->lastVehicle_;
	if (pv && pl->length() < pv->distance_ + range) {
	  front = pv;
	  range = pl->length() - pv->distance_;
	}
  }

  if (front) {
	return front->currentSpeed_;
  } else {
	return desiredSpeed_;
  }
}

// Return the average speed in thelane ahead of this vehicle

#ifdef OUT
float
TS_Vehicle::speedAhead(TS_Lane *thelane)
{
  float range = theParameter->lcSpdLookAheadDistance();
  float spd = 0.0;
  int num = 0;

  // Vehicles in current segment

  TS_Vehicle *pv = macroLeading_;
  while (pv && pv->distance_ + range > distance_) {
	if (pv->lane_ == thelane) {
	  num ++;
	  spd += pv->currentSpeed_;

	  // continue the search in a particular lane only 

	  pv = pv->leading_;
	  continue;
	}
	pv = pv->macroLeading_;
  }

  // Distance to be checked in downstream lane

  range -= distance_;

  if (range > 0) {

	// Vehicles in downstream lane(s)
   
	TS_Lane* pl;
	register int i, n = thelane->nDnLanes();
	for (i = 0; i < n; i ++) {
	  pl = thelane->dnLane(i);
	  pv = pl->lastVehicle_;
	  while (pv && pl->length() < pv->distance_ + range) {
		num ++;
		spd += pv->currentSpeed_;
		pv = pv->leading_;
	  }
	}
  }

  if (num) {
	return spd / (float) num;
  } else {
	return desiredSpeed_;
  }
}
#endif

double TS_Vehicle::dlcExpOfUtility(TS_Lane*plane)
{
   // LEADING AND LAG VEHICLES (do not have to be in same segment)

  TS_Vehicle* av = findFrontBumperLeader(plane);
  TS_Vehicle* bv = findFrontBumperFollower(plane);

  TS_Vehicle* front = this->vehicleAhead() ;

  // Dan: If vehicle ahead is a bus and there is a bus stop ahead
  // in the lane, set busAheadDummy to 1 for disincentive to be
  // applied in the utility.

  int busAheadDummy = 0;

  if (front && front->type() == 0x3 && type() != 0x3) {

    CtrlList device = nextCtrl_;

    while (device && (*device)->type() == CTRL_BUSSTOP) {

      int n = (*device)->nLanes();
      for (int i = 0; i < n; i ++) {
        RN_Signal *signal = (*device)->signal(i);
        if (signal) {
	  if (plane->code() == signal->lane()->code()) {
            busAheadDummy = 1;
	  } 
        }
      }
      device = device->next();
    }
  }

  double vld, vfr, vlg ; 

  if (front) {
    // vfr = front->currentSpeed_ ;
     vfr = Min(desiredSpeed_, front->currentSpeed_) ;
  } else {
	vfr = desiredSpeed_ ;
  }

  if (av) {
    // vld = av->currentSpeed_ ;
	vld = Min(desiredSpeed_, av->currentSpeed_) ;
  } else {
	vld = desiredSpeed_ ;
  }

  if (bv) {
	vlg = bv->currentSpeed_ ;
  } else {
	vlg = 0.0 ;
  }
  
  float *a = theParameter->dlcUnsatisfactoryParamsKazi() ;
  float *b = theParameter->dlcUtilParamsKazi() ;
   
  // tomer -trying to make people change lanes when traffic is slow in one 
  // lane and flowing in others. in the current model hardly no one tries to 
  // change lanes.
  //float desSpeed = a[0] * tsSegment()->speed(); 
 
  float desSpeed = a[0] * Max (tsSegment()->speed(), plane->speed());

  float prev_dlc_dummy = flag(FLAG_PREV_LC) ? b[4] : 0.0 ;
  double u = b[0] + b[1] * (vld - desSpeed) +
	  b[2] * (vfr - desSpeed) + b[3] * (vlg - currentSpeed_) +
	  prev_dlc_dummy + b[5] * busAheadDummy;

  return exp(u) ;
}


#ifndef USE_KAZI_MODELS //cfc jun11

/* _________________________________________________________________________ */

/*
 *-------------------------------------------------------------------
 * Determine if this driver will start a mandatory lane change or
 * stay in the correct lanes. This function will update bit 6-7 of
 * "status_" if necessary. This two bits indicate mandatory lane
 * change or "suggested/required" lane changes, i.e.:
 *
 *   32 = STATUS_MANDATORY
 *   64 = STATUS_SUGGESTED
 *
 * This function is called by TS_Vehicle::makeLaneChangingDecision().
 * It returns TRUE if the vehicle's mandatory flag has been turn on,
 * or FALSE otherwise.
 *-------------------------------------------------------------------
 */

int
TS_Vehicle::checkForLC()
{

TS_Lane* plane;

  // Compute the distance from the critical position.

  int bad = lane_->isThereBadEventAhead(this, &dis2stop_, &vis_);

  if (bad < 0) {
	setFlag(FLAG_ESCAPE);
  } else if (bad > 0) {
	setFlag(FLAG_AVOID);
  } else {
	dis2stop_ = distanceFromDownNode();
	vis_ = link()->length();
  }

  
  if ( flag(FLAG_ESCAPE) && flag(FLAG_VMS_LANE_USE_DIR) ) {  // Angus
        // Vehicle flagged for escape from lane by VMS
        setMandatoryStatusTag();
  }


  int escape = flag(FLAG_ESCAPE);
  int avoid = flag(FLAG_AVOID);
  unsetFlag(FLAG_ESCAPE | FLAG_AVOID | FLAG_BLOCK);

  int out = escape;				// || avoid;

  // Check if this vehicle has to get out of current lane

  if (out || !status(STATUS_CURRENT_OK)) {

	// number of lane changes to an open lane

	int nl =  1;		// left side
	int nr = -1;		// right side

	setFlag(FLAG_BLOCK);

	// Check left side to find whether a lane if open

	if ((plane = lane_->left()) &&
		(status(STATUS_LEFT_OK) || out)) {
	  while (plane) {
		if (plane->isThereBadEventAhead(this) >= 0) {
		  nl = -nl;
		  unsetFlag(FLAG_BLOCK_LEFT);
		  break;		// found an open lane
		}
		nl ++;
		plane = plane->left();
	  }
	}

	// Check right side to find whether a lane is open

	if ((plane = lane_->right()) &&
		(status(STATUS_RIGHT_OK) || out)) {
	  while (plane) {
		if (plane->isThereBadEventAhead(this) >= 0) {
		  nr = -nr;
		  unsetFlag(FLAG_BLOCK_RIGHT);
		  break;		// found an open lane
		}
		nr --;
		plane = plane->right();
	  }
	}

	// Select the direction with an open lane and minimum number of
	// lane changes

	if (nl < 0 && (nr < 0 || -nl < nr)) {

	  // There is an open left lane and no open right lane or the
	  // open left lane is closer.

	  if (escape) setFlag(FLAG_ESCAPE_LEFT);
	  else if (avoid) setFlag(FLAG_AVOID_LEFT);
	  return -1;

	} else if (nr > 0 && (nl > 0 || nr < -nl)) {

	  // There is an open right lane and no open left lane or the
	  // open right lane is closer.

	  if (escape) setFlag(FLAG_ESCAPE_RIGHT);
	  else if (avoid) setFlag(FLAG_AVOID_RIGHT);
	  return 1;

	} else if (nl < 0 && nr > 0) {

	  // There is open lane on both side. Choose one randomly.

	  if (theRandomizer->brandom(0.5)) {
		if (escape) setFlag(FLAG_ESCAPE_LEFT);
		else if (avoid) setFlag(FLAG_AVOID_LEFT);
		return -1;
	  } else {
		if (escape) setFlag(FLAG_ESCAPE_RIGHT);
		else if (avoid) setFlag(FLAG_AVOID_RIGHT);
		return 1;
	  }

	} else {

	  // All lane are blocked

	  return 0;
	}
  }


  int n = lane_->isWrongLane(this);

  int change = 0;


#ifdef USE_CFC_MODELS //calculate vector of probability of all lanes

  RN_Segment* psegment = lane_->segment();
  int laneNum=psegment->nLanes();
  int local =lane_->localIndex();
  //  vector <double>probLane=TLProbabilityV();
    vector <double>probLane=TLProbability(lane_);
double probOfCurrentLane=0;double probOfLeftLane=0;double probOfRightLane=0;


  // Currently this vehicle is not making mandatory lane change
  // This vehicle can proceed in current lane. Check for
  //  lane change.

  if (flag(FLAG_VMS_LANE_USE_DIR) == FLAG_VMS_LANE_USE_DIR &&
	  attr(ATTR_GLC_RULE_COMPLY) && !n ) {

	// Viewed a message that says stay in lane and this is a
	// cooperative driver.
	 
      return 0;
  }
  int rules = lane_->cRules();


  if ((isReadyForNextDLC(2) || n ) &&
	  (plane = lane_->left()) &&
	  (!flag(FLAG_VMS_LANE_USE_RIGHT) ||
	   plane->localIndex() > VmsLaneUsePivotToIndex(flag())) &&
	  (rules & LANE_CHANGE_LEFT ||
	   !attr(ATTR_GLC_RULE_COMPLY)) &&
	  plane->isThereBadEventAhead(this) >= 0) {

	  // Lane change to the left is allowed, and there is no critical
	  // event ahead in the left lane.

	 
	  for (int i=0;i<local;i++){
	    probOfLeftLane=probOfLeftLane+probLane[i];}

      

  }
		
  if ((isReadyForNextDLC(1)|| n ) &&
	  (plane = lane_->right()) &&
	  (!flag(FLAG_VMS_LANE_USE_LEFT) ||
	   plane->localIndex() < VmsLaneUsePivotToIndex(flag())) &&
	  (rules & LANE_CHANGE_RIGHT ||
	   !attr(ATTR_GLC_RULE_COMPLY)) &&
	  plane->isThereBadEventAhead(this) >= 0) {

	  // Change to the right is allowed, and there is no critical
	  // event ahead.




	  for (int i=local+1;i<laneNum;i++){
	    probOfRightLane=probOfRightLane+probLane[i];}

	 

  }
 probOfCurrentLane=probLane[local];
 float probOfCL_LL = probOfCurrentLane + probOfLeftLane;
 double rnd = theRandomizer->urandom() ;
 if (rnd < probOfCurrentLane)  change = 0;
 else if (rnd < probOfCL_LL) change = -1;
 else change = 1 ;
 
#endif

#ifdef USE_TOMER_MODELS
  // Currently this vehicle is not making mandatory lane change
  // This vehicle can proceed in current lane. Check for
  //  lane change.

  if (flag(FLAG_VMS_LANE_USE_DIR) == FLAG_VMS_LANE_USE_DIR &&
	  attr(ATTR_GLC_RULE_COMPLY) && !n ) {

	// Viewed a message that says stay in lane and this is a
	// cooperative driver.
	 
       return 0;
  }

 //    if (dis2stop_ > theParameter->lcLowerBound()) {

	// Number of lanes to change
       
  // int n = lane_->isWrongLane(this);

	// CMF for making mandatory lane changing.



  double eul = 0.0, eur = 0.0, euc = 1.0 ;


  // Lane change and lane use regulation. 

  int rules = lane_->cRules();


  if ((isReadyForNextDLC(2) || n ) &&
	  (plane = lane_->left()) &&
	  (!flag(FLAG_VMS_LANE_USE_RIGHT) ||
	   plane->localIndex() > VmsLaneUsePivotToIndex(flag())) &&
	  (rules & LANE_CHANGE_LEFT ||
	   !attr(ATTR_GLC_RULE_COMPLY)) &&
	  plane->isThereBadEventAhead(this) >= 0) {

	  // Lane change to the left is allowed, and there is no critical
	  // event ahead in the left lane.


	  eul = LCUtilityLeft(plane);





  }
		
  if ((isReadyForNextDLC(1)|| n ) &&
	  (plane = lane_->right()) &&
	  (!flag(FLAG_VMS_LANE_USE_LEFT) ||
	   plane->localIndex() < VmsLaneUsePivotToIndex(flag())) &&
	  (rules & LANE_CHANGE_RIGHT ||
	   !attr(ATTR_GLC_RULE_COMPLY)) &&
	  plane->isThereBadEventAhead(this) >= 0) {

	  // Change to the right is allowed, and there is no critical
	  // event ahead.

	  eur = LCUtilityRight(plane);

  }


  double sum = eul + eur ;

  if (sum > 0 ){
    
    euc = LCUtilityCurrent(lane_);

  } else {
    return (0);
    
      }

  sum += euc;

  double rnd = theRandomizer->urandom() ;

  float probOfCurrentLane = euc / sum;
  float probOfCL_LL = probOfCurrentLane + eul / sum;
  if (rnd < probOfCurrentLane) change = 0 ;
  else if (rnd < probOfCL_LL) change = -1 ;
  else change = 1 ;
#endif

  return change;

}



# ifdef USE_TOMER_MODELS

double TS_Vehicle::LCUtilityLeft(TS_Lane*plane)
{
 
  float *a = theParameter->laneUtilityParams();

   // LEADING AND LAG VEHICLES (do not have to be in same segment)

  TS_Vehicle* av = findFrontBumperLeader(plane);
  TS_Vehicle* bv = findFrontBumperFollower(plane);

  double vld, mlc, density, spacing; 

  int n = abs(plane->isWrongLane(this));

  density = plane->density();

  float heavy_neighbor = 0.0;

  if (av) 
    {
      spacing = this->gapDistance(av);
      vld = Min(desiredSpeed_, av->currentSpeed_) ;
      heavy_neighbor = (av->isType(VEHICLE_SMALL)) ? 0.0 : a[7];
    } 
  else  
    {
      vld = desiredSpeed_ ;
      spacing = distance(); 
	if (nextLane_) { 
	  spacing += nextLane_->length();
	}
    }
  if (bv){

    heavy_neighbor = (bv->isType(VEHICLE_SMALL)) ? heavy_neighbor : a[7];

  }

  float left_most = 0.0;

  if ( theNetwork->drivingDirection()==1) {  // for left hand driving
    left_most = (plane->left()) ? 0.0 : a[2];
  }

  switch (n) {
  case 0:
    {
      mlc = 0;
      break;
    }
  case 1:
    {
      mlc = a[12] * pow(dis2stop_/1000, a[17]) + a[15];
      break;
    }
  case 2:
    {
      mlc = a[13] * pow(dis2stop_/1000, a[17]) + a[15] + a[16];
      break;
    }
  default:
    {
      mlc = (a[13]+a[14]*(n-2)) * pow(dis2stop_/1000, a[17]) +a[15] + a[16] * (n-1);
    }
  }

  // Dan: If vehicle ahead is a bus and there is a bus stop ahead
  // in the lane, set busAheadDummy to 1 for disincentive to be
  // applied in the utility.

  int busAheadDummy = 0;

  if (av && av->type() == 0x3 && type() != 0x3) {

    CtrlList device = nextCtrl_;

    while (device && (*device)->type() == CTRL_BUSSTOP) {

      int n = (*device)->nLanes();
      for (int i = 0; i < n; i ++) {
        RN_Signal *signal = (*device)->signal(i);
        if (signal) {
	  if (plane->code() == signal->lane()->code()) {
            busAheadDummy = 1;
	  } 
        }
      }
      device = device->next();
    }
  }

  double u = a[4] * vld + a[8] * density + a[6] * spacing + mlc + heavy_neighbor + left_most + a[5] * busAheadDummy; 

  return exp(u) ;
}






double TS_Vehicle::LCUtilityRight(TS_Lane*plane)
{

   float *a = theParameter->laneUtilityParams();

   // LEADING AND LAG VEHICLES (do not have to be in same segment)

  TS_Vehicle* av = findFrontBumperLeader(plane);
  TS_Vehicle* bv = findFrontBumperFollower(plane);

  double vld, mlc, density, spacing; 

  int n = abs(plane->isWrongLane(this));

  density = plane->density();

  float heavy_neighbor = 0.0;

  if (av) {
	vld = Min(desiredSpeed_, av->currentSpeed_) ;
	heavy_neighbor = (av->isType(VEHICLE_SMALL)) ? 0.0 : a[7];
	spacing = this->gapDistance(av);
  } else {
	vld = desiredSpeed_ ;
	spacing = distance_;
	if (nextLane_)  {
	  spacing += nextLane_->length();
	}
  }
  if (bv){

    heavy_neighbor = (bv->isType(VEHICLE_SMALL)) ? heavy_neighbor : a[7];

  }

  float right_most = 0.0;

  if ( !theNetwork->drivingDirection()==1) {  // for right hand driving
    right_most = (plane->right()) ? 0.0 : a[2];
  }

  switch (n) {
  case 0:
    {
      mlc = 0;
      break;
    }
  case 1:
    {
      mlc = a[12] * pow(dis2stop_/1000, a[17]) + a[15];
      break;
    }
  case 2:
    {
      mlc = a[13] * pow(dis2stop_/1000, a[17]) + a[15] + a[16];
      break;
    }
  default:
    {
      mlc = (a[13]+a[14]*(n-2)) * pow(dis2stop_/1000, a[17]) +a[15] + a[16] * (n-1);
    }
  } 

  // Dan: If vehicle ahead is a bus and there is a bus stop ahead
  // in the lane, set busAheadDummy to 1 for disincentive to be
  // applied in the utility.

  int busAheadDummy = 0;

  if (av && av->type() == 0x3 && type() != 0x3) {

    CtrlList device = nextCtrl_;

    while (device && (*device)->type() == CTRL_BUSSTOP) {

      int n = (*device)->nLanes();
      for (int i = 0; i < n; i ++) {
        RN_Signal *signal = (*device)->signal(i);
        if (signal) {
	  if (plane->code() == signal->lane()->code()) {
            busAheadDummy = 1;
	  } 
        }
      }
      device = device->next();
    }
  }



  double u = a[1] + a[4] * vld + a[8] * density + a[6] * spacing + mlc + heavy_neighbor + right_most + a[5] * busAheadDummy; 


  return exp(u) ;
  }


double TS_Vehicle::LCUtilityCurrent(TS_Lane*plane)
{

  float *a = theParameter->laneUtilityParams();

   // LEADING AND LAG VEHICLES (do not have to be in same segment)

  TS_Vehicle* av = findFrontBumperLeader(plane);
  TS_Vehicle* bv = findFrontBumperFollower(plane);

  double vld, mlc, spacing, density; 

  int n = abs(plane->isWrongLane(this));

  density = plane->density();
  
  float heavy_neighbor = 0.0;

  if (av) {
	vld = Min(desiredSpeed_, av->currentSpeed_) ;
	heavy_neighbor = (av->isType(VEHICLE_SMALL)) ? 0.0 : a[7];
	spacing = this->gapDistance(av);
  } else {
    vld = desiredSpeed_ ;
    spacing = distance_;
    if (nextLane_)
      spacing += nextLane_->length();
  }
  
  if (bv){

    heavy_neighbor = (bv->isType(VEHICLE_SMALL)) ? heavy_neighbor : a[7];

}
   
  float tailgate_dummy = 0;
  TS_Vehicle* behind = this->vehicleBehind() ;
  if (behind) {
    double gap_behind = behind->gapDistance(this);
    float dens = tsSegment()->density();
    tailgate_dummy = (gap_behind <= a[10] && dens <= a[11])? a[9] : 0;
  }
  

  float right_most = 0.0;

  if ( !theNetwork->drivingDirection()==1) { // for right hand driving
    right_most = (plane->right()) ? 0.0 : a[2];

  } else {  //for left hand driving. used as left_most variable

    right_most = (plane->left()) ? 0.0 : a[2]; 
  }

  switch (n) {
  case 0:
    {
      mlc = 0;
      break;
    }
  case 1:
    {
      mlc = a[12] * pow(dis2stop_/1000, a[17]) + a[15];
      break;
    }
  case 2:
    {
      mlc = a[13] * pow(dis2stop_/1000, a[17]) + a[15] + a[16];
      break;
    }
  default:
    {
      mlc = (a[13]+a[14]*(n-2)) * pow(dis2stop_/1000, a[17]) +a[15] + a[16] * (n-1);
    }
  } 

  // Dan: If vehicle ahead is a bus and there is a bus stop ahead
  // in the lane, set busAheadDummy to 1 for disincentive to be
  // applied in the utility.

  int busAheadDummy = 0;

  if (av && av->type() == 0x3 && type() != 0x3) {

    CtrlList device = nextCtrl_;

    while (device && (*device)->type() == CTRL_BUSSTOP) {

      int n = (*device)->nLanes();
      for (int i = 0; i < n; i ++) {
        RN_Signal *signal = (*device)->signal(i);
        if (signal) {
	  if (plane->code() == signal->lane()->code()) {
            busAheadDummy = 1;
	  } 
        }
      }
      device = device->next();
    }
  }

  double u = a[0]+ a[4] * vld + a[8] * density + a[6] * spacing + mlc + heavy_neighbor + right_most + tailgate_dummy + a[5] * busAheadDummy; 

  return exp(u) ;
} 

#endif

  



// ****************************************************************
// tomer - this function gives the LC direction when the driver is 
//         constrained by the lookahead
// ****************************************************************
int
TS_Vehicle::checkForLookAheadLC(vector <int> & _connectedLanes)
{

  int idx = pathIndex_; 
  int lanid;
  lanid = lane_->code(); 

  RN_Link* plink = link();
  RN_Segment* psegment = lane_->segment();
  int laneNum=psegment->nLanes();//cfc
  int myCurrent=lane_->localIndex();//cfc
  float x=distance_;
  int change = 0;
  RN_Segment* ssegment;
  RN_Lane* plane;
  RN_Lane* slane;
  
  if ( flag(FLAG_ESCAPE) || flag(FLAG_VMS_LANE_USE_DIR) ) {  //  Flagged for escape by VMS (Angus)

    if (flag(FLAG_ESCAPE_LEFT)) {
      change = -1;
    }
    if (flag(FLAG_ESCAPE_RIGHT)) {
      change = 1;
    }
    setStatus(STATUS_MANDATORY);
    return change;
  }

  if ( x>=lookAheadDistance_ // && status(STATUS_CURRENT_OK) 
       ) {
    return change;
  }
  
  while ( x<lookAheadDistance_ )                 // within lookahead distance
    {
      if ( psegment != plink->endSegment() )     // still in this link
	{
	  psegment = psegment->downstream();
	  x += psegment->length();
	}
      else if ( idx < path()->nLinks() )        // looking at the next link
	{
	 
	  if ( !path()->link(idx) )
	    {break;}
	  plink = path()->link(idx);
	  //	  cout << plink << endl;
	  
	  psegment = plink->startSegment();
	  x += psegment->length();
	  idx++;
	}
      else { break; }                            // end of path 
    }

  if ( psegment != plink->startSegment() ) {
    ssegment = psegment->upstream();
    idx--;
  } 
  else {
    idx -= 2;
    if ( path()->link(idx) ) {
      plink = path()->link(idx);
      ssegment = plink->endSegment();
    } 
    else 
      {return 0; }
  }
  
  vector <int > _upLaneList;        // list of local indexes to connected lanes in ssegment


  _connectedLanes.erase(_connectedLanes.begin(), _connectedLanes.end() );

  for (int i = 0; i < psegment->nLanes(); i ++)
    {
      _connectedLanes.push_back(i);
    }


  // advancing upstream segment by segment and creating the connectivity lists

 while ( psegment != segment() )  {  
   bool allMarked = false;
  
   // iterate all connected lanes in the downstream segment
   for (int i = 0; i < _connectedLanes.size(); i ++) { 
     bool lastConnect = false ;
     plane = psegment->lane(_connectedLanes[i]);

     TS_Lane* tlane = (TS_Lane*) psegment->lane(_connectedLanes[i]);
     if (! tlane->doesNotAllow(this)) // avoid lanes not allowed (e.g. HOV lane) (Angus)
       {
	 // iterate all connected lanes in the upstream segment
	 for (int j = 0; j < ssegment->nLanes(); j ++) { 
	   
	   // avoid lanes that the vehicle is not allowed to use
	   
	   TS_Lane* qlane = (TS_Lane*) ssegment->lane(j);
	   if (! qlane->doesNotAllow(this)) { 
	     slane = ssegment->lane(j);
	     bool connect = plane->isInUpLanes(slane);
	     int marked = * (find (_upLaneList.begin(), _upLaneList.end(), j ));
	     if ( connect && marked == * (_upLaneList.end() ) ) { 
	       // put it at the end of the list
	       _upLaneList.push_back(j);
	       lastConnect = connect;
	       
	       // stop if all upstream lanes are connected	  
	       if ( _upLaneList.size() == ssegment->nLanes() ) {
		 allMarked = true;
		 break; 
	       }
	       else if (!connect && lastConnect) {break;}
	     }
	   }
	   else if (lastConnect ) { break; }
	 }
       }
     if (allMarked) {break;}
   }

   // the upstream set is empty. LC must happen downstream.  
   if ( _upLaneList.empty() ) { 
     // maybe sorting is not really needed - check it for deletion
     // sort ( _connectedLanes.begin() , _connectedLanes.end() );
     int leftIndex = _connectedLanes[0];
     int rightIndex = _connectedLanes[_connectedLanes.size()-1];
     if (leftIndex > rightIndex) {
       int temp = leftIndex;
       leftIndex = rightIndex;
       rightIndex = temp;
     } 
     while ( _upLaneList.empty() ) {
       if ( ++rightIndex < psegment->nLanes()) {

	 // iterate all connected lanes in the upstream segment
	 for (int j = 0; j < ssegment->nLanes(); j ++) { 
	   plane = psegment->lane(rightIndex);
	   slane = ssegment->lane(j);
	   bool connect = plane->isInUpLanes(slane);
	   if ( connect ) { // put it at the end of the list
	     _upLaneList.push_back(j);
	   }
	 }
       }
       
        if ( leftIndex-- >= 0 && _upLaneList.empty() ) {

	 // iterate all connected lanes in the upstream segment
	  for (int j = 0; j < ssegment->nLanes(); j ++) { 
	    plane = psegment->lane(leftIndex);
	    slane = ssegment->lane(j);
	    bool connect = plane->isInUpLanes(slane);
	    if ( connect ) { // put it at the end of the list
	      _upLaneList.push_back(j);
	    }
	  }
	}
	// a correction for the case there is no connectivity between 
	// the 2 segments (Angus and Tomer)
	if (leftIndex < 0 && rightIndex > psegment->nLanes() ) {
	  for (int j = 0; j < ssegment->nLanes(); j ++) { 
	    _upLaneList.push_back(j);
	  }
	}
	// [end correction]
     }
   }
       
      _connectedLanes = _upLaneList;
      _upLaneList.erase(_upLaneList.begin(), _upLaneList.end() );

      psegment = ssegment;
      
      
      if ( psegment != segment() ){
	if ( ssegment != plink->startSegment() ) {
	  ssegment = ssegment->upstream();
	} 
	else {
	  idx--;
	  plink = path()->link(idx);
	  ssegment = plink->endSegment();
	}		    
      }
 }

#ifdef USE_CFC_MODELS
 double debug, debug2,debug3,debug4,cfc, debug5, debug6, debug7, debug8, debug9;
// the result is a list of connected lanes in the current segment. needs to 
 // check that the current one is included in them or else what changes need 
 // to be done.


 int nCurrent = 100; // number of lane changes required for the current lane.
 // int myCurrent_ =lane()->localIndex();//current lane index, cfc 26 sep 
 for (int i = 0; i < _connectedLanes.size(); i++) {

    
     int numlcCurrent  = abs (_connectedLanes[i] -  lane()->localIndex()) ;
     nCurrent = Min(nCurrent, numlcCurrent);

   }

 vector <int> reqdChange(laneNum);
 vector <double> LCdist(laneNum);

 for (int j=0;j<reqdChange.size();j++){

   TS_Lane* qlane = (TS_Lane*) psegment->lane(j);
   int minChange=100;
   int reqd=0;

   for (int i=0;i<_connectedLanes.size();i++){
      int numChange=abs(_connectedLanes[i]-j);
      minChange=Min(minChange,numChange);
        }

   reqdChange[j]=minChange;
   reqd=reqdChange[j];

   if (reqd==0) {

   LCdist[j] = dis2stop_;
 
                  } 
 if (reqd>0) {
	   
 LCdist[j] = Min(getOutDistance(qlane),dis2stop_);
 debug4=0;	  }

 debug2=LCdist[j];
 debug3=0;

 }//for


 // int reqdChange0=reqdChange[0];
 //int reqdChange1=reqdChange[1];
 //int reqdChange2=reqdChange[2];
 //int reqdChange3=reqdChange[3];

 // vector <double>probLane=TLProbabilityLookAhead(lane_,reqdChange,LCdist,_connectedLanes);
 
 vector <double >probLane=TLProbabilityLookAhead(lane_,reqdChange,LCdist);
 
debug5=probLane[0];
 debug6=probLane[1];
  debug7=probLane[2];
  debug8=probLane[3];
  debug9=probLane[4];
 

 
 double probOfCurrentLane=0;double probOfLeftLane=0;double probOfRightLane=0;

  for (int i=0;i<myCurrent;i++){
	    probOfLeftLane=probOfLeftLane+probLane[i];}

 for (int i=myCurrent+1;i<laneNum;i++){
	    probOfRightLane=probOfRightLane+probLane[i];}


 
 /*
   //reqd change= # of lane changes wrt current lane


  // Currently this vehicle is not making mandatory lane change
  // This vehicle can proceed in current lane. Check for
  //  lane change.

  if (flag(FLAG_VMS_LANE_USE_DIR) == FLAG_VMS_LANE_USE_DIR &&
	  attr(ATTR_GLC_RULE_COMPLY) && !nCurrent ) {

	// Viewed a message that says stay in lane and this is a
	// cooperative driver.
	 
       return 0;
  }

  int rules = lane_->cRules();


 TS_Lane* tlane = (TS_Lane*) lane_->left();

if ((isReadyForNextDLC(2) || nCurrent ) &&
	  (plane = lane_->left()) &&
	  (!flag(FLAG_VMS_LANE_USE_RIGHT) ||
	   plane->localIndex() > VmsLaneUsePivotToIndex(flag())) &&
	  (rules & LANE_CHANGE_LEFT ||
	   !attr(ATTR_GLC_RULE_COMPLY)) &&
	   tlane->isThereBadEventAhead(this) >= 0) {

	  // Lane change to the left is allowed, and there is no critical
	  // event ahead in the left lane.



	  for (int i=0;i<myCurrent;i++){
	    probOfLeftLane=probOfLeftLane+probLane[i];}


  }

  tlane = (TS_Lane*) lane_->right();

		
  if ((isReadyForNextDLC(1)|| nCurrent ) &&
	  (plane = lane_->right()) &&
	  (!flag(FLAG_VMS_LANE_USE_LEFT) ||
	   plane->localIndex() < VmsLaneUsePivotToIndex(flag())) &&
	  (rules & LANE_CHANGE_RIGHT ||
	   !attr(ATTR_GLC_RULE_COMPLY)) &&
	  tlane->isThereBadEventAhead(this) >= 0) {

	  // Change to the right is allowed, and there is no critical
	  // event ahead.

	 
 double probRight=0;
	  for (int i=myCurrent+1;i<laneNum;i++){
	    probOfRightLane=probOfRightLane+probLane[i];}

	 
 }
  
 */
 probOfCurrentLane=probLane [myCurrent];
 double  probOfCL_LL = probOfCurrentLane + probOfLeftLane;


   // cfc-gunwoo writing the lane probabbilities 26 sep

   

 double rnd = theRandomizer->urandom() ;
 if (rnd < probOfCurrentLane)  change = 0;
 else if (rnd < probOfCL_LL) change = -1;
 else change = 1 ;

#endif
    
#ifdef USE_TOMER_MODELS
 int nRight = 100; // number of lane changes required for the current lane.
 int nLeft = 100; // number of lane changes required for the current lane.
 int nCurrent = 100; // number of lane changes required for the current lane.
 float LCdistance = 0.0;

 for (int i = 0; i < _connectedLanes.size(); i++) {

     int numlcRight  = abs (_connectedLanes[i] -  (lane()->localIndex()+1)) ;
     int numlcLeft  = abs (_connectedLanes[i] -  (lane()->localIndex()-1)) ;
     int numlcCurrent  = abs (_connectedLanes[i] -  lane()->localIndex()) ;

     nRight = Min(nRight, numlcRight);
     nLeft = Min(nLeft, numlcLeft);
     nCurrent = Min(nCurrent, numlcCurrent);

   }

// Currently this vehicle is not making mandatory lane change
  // This vehicle can proceed in current lane. Check for
  //  lane change.

  if (flag(FLAG_VMS_LANE_USE_DIR) == FLAG_VMS_LANE_USE_DIR &&
	  attr(ATTR_GLC_RULE_COMPLY) && !nCurrent ) {

	// Viewed a message that says stay in lane and this is a
	// cooperative driver.
	 
       return 0;
  }

 //    if (dis2stop_ > theParameter->lcLowerBound()) {

	// Number of lanes to change
       
  // int n = lane_->isWrongLane(this);

	// CMF for making mandatory lane changing.

  double eul = 0.0, eur = 0.0, euc = 1.0 ;




  // Lane change and lane use regulation. 
 
  int rules = lane_->cRules();


   TS_Lane* tlane = (TS_Lane*) lane_->left();




  if ((isReadyForNextDLC(2) || nCurrent ) &&
	  (plane = lane_->left()) &&
	  (!flag(FLAG_VMS_LANE_USE_RIGHT) ||
	   plane->localIndex() > VmsLaneUsePivotToIndex(flag())) &&
	  (rules & LANE_CHANGE_LEFT ||
	   !attr(ATTR_GLC_RULE_COMPLY)) &&
	   tlane->isThereBadEventAhead(this) >= 0) {

	  // Lane change to the left is allowed, and there is no critical
	  // event ahead in the left lane.


	  if (nLeft == 0) {
	    LCdistance = FLT_INF;
	  } else {
	    LCdistance = getOutDistance(plane);
	  }
	  eul = LCUtilityLookAheadLeft(tlane, nLeft, LCdistance);
  }

  tlane = (TS_Lane*) lane_->right();

		
  if ((isReadyForNextDLC(1)|| nCurrent ) &&
	  (plane = lane_->right()) &&
	  (!flag(FLAG_VMS_LANE_USE_LEFT) ||
	   plane->localIndex() < VmsLaneUsePivotToIndex(flag())) &&
	  (rules & LANE_CHANGE_RIGHT ||
	   !attr(ATTR_GLC_RULE_COMPLY)) &&
	  tlane->isThereBadEventAhead(this) >= 0) {

	  // Change to the right is allowed, and there is no critical
	  // event ahead.

	 

         if (nRight == 0) {
	    LCdistance = FLT_INF;
	  } else {
	    LCdistance = getOutDistance(plane);
	  }
	  eur = LCUtilityLookAheadRight(tlane, nRight, LCdistance);


	  // double probRight=0;
	  // for (int i=local+1;i<laneNum;i++){
	  // probOfRightLane=probOfRightLane+probLane[i];}

	 
 }
  
 
  double sum = eul + eur ;
  //  TS_Lane* tlane = (TS_Lane*) lane_;

  if (sum > 0 ){
    
  if (nCurrent == 0) {
	    LCdistance = FLT_INF;
	  } else {
	    LCdistance = getOutDistance(lane_);
	  }

	  euc = LCUtilityLookAheadCurrent(lane_, nCurrent, LCdistance);
  
  }else {

    return (0);
    
  }

  sum += euc;

  double rnd = theRandomizer->urandom() ;

  float probOfCurrentLane = euc / sum;
  float probOfCL_LL = probOfCurrentLane + eul / sum;
  if (rnd < probOfCurrentLane) change = 0 ;
  else if (rnd < probOfCL_LL) change = -1 ;
  else change = 1 ;
#endif


 
  return change;

}

#ifdef USE_TOMER_MODELS
	
double TS_Vehicle::LCUtilityLookAheadLeft(TS_Lane* plane, int n, float LCdistance)
{

  float *a = theParameter->laneUtilityParams();

   // LEADING AND LAG VEHICLES (do not have to be in same segment)

  TS_Vehicle* av = findFrontBumperLeader(plane);
  TS_Vehicle* bv = findFrontBumperFollower(plane);

  double vld, mlc, density, spacing; 

  density = plane->density();

  float heavy_neighbor = 0.0;

  if (av) 
    {
	vld = Min(desiredSpeed_, av->currentSpeed_) ;
	heavy_neighbor = (av->isType(VEHICLE_SMALL)) ? 0.0 : a[7];
	spacing = this->gapDistance(av); 
    } 
  else 
    {
      vld = desiredSpeed_ ;
      spacing = distance();
	if (nextLane_){
	  spacing += nextLane_->length(); 
	}
    }

  if (bv){

    heavy_neighbor = (bv->isType(VEHICLE_SMALL)) ? heavy_neighbor : a[7];

  }

  float left_most = 0.0;

  if ( theNetwork->drivingDirection()==1) {  // for left hand driving
    left_most = (plane->left()) ? 0.0 : a[2];
  }

 switch (n) {
  case 0:
    {
      mlc = 0;
      break;
    }
  case 1:
    {
      mlc = a[12] * pow(dis2stop_/1000, a[17]) + a[15];
      break;
    }
  case 2:
    {
      mlc = a[13] * pow(dis2stop_/1000, a[17]) + a[15] + a[16];
      break;
    }
  default:
    {
      mlc = (a[13]+a[14]*(n-2)) * pow(dis2stop_/1000, a[17]) +a[15] + a[16] * (n-1);
    }
  } 

  // Dan: If vehicle ahead is a bus and there is a bus stop ahead
  // in the lane, set busAheadDummy to 1 for disincentive to be
  // applied in the utility.

  int busAheadDummy = 0;

  if (av && av->type() == 0x3 && type() != 0x3) {

    CtrlList device = nextCtrl_;

    while (device && (*device)->type() == CTRL_BUSSTOP) {

      int n = (*device)->nLanes();
      for (int i = 0; i < n; i ++) {
        RN_Signal *signal = (*device)->signal(i);
        if (signal) {
	  if (plane->code() == signal->lane()->code()) {
            busAheadDummy = 1;
	  } 
        }
      }
      device = device->next();
    }
  }




  double u = a[4] * vld + a[8] * spacing + a[6] * density + mlc + heavy_neighbor + left_most + a[5] * busAheadDummy; 


  return exp(u) ;
}



double TS_Vehicle::LCUtilityLookAheadRight(TS_Lane*plane, int n, float LCdistance)
{

  float *a = theParameter->laneUtilityParams();

   // LEADING AND LAG VEHICLES (do not have to be in same segment)

  TS_Vehicle* av = findFrontBumperLeader(plane);
  TS_Vehicle* bv = findFrontBumperFollower(plane);

  //  TS_Vehicle* front = this->vehicleAhead() ;

  double vld, mlc, density, spacing; 

  density = plane->density();

  float heavy_neighbor = 0.0;

  if (av) {
	vld = Min(desiredSpeed_, av->currentSpeed_) ;
	heavy_neighbor = (av->isType(VEHICLE_SMALL)) ? 0.0 : a[7];
	spacing = this->gapDistance(av);
  } else {
	vld = desiredSpeed_ ;
	spacing = distance();
	if (nextLane_) {
	  spacing += nextLane_->length();
	}
  }

  if (bv){

    heavy_neighbor = (bv->isType(VEHICLE_SMALL)) ? heavy_neighbor : a[7];

  }

  float right_most = 0.0;

  if ( !theNetwork->drivingDirection()==1) {  // for right hand driving
    right_most = (plane->right()) ? 0.0 : a[2];
  }

 switch (n) {
  case 0:
    {
      mlc = 0;
      break;
    }
  case 1:
    {
      mlc = a[12] * pow(dis2stop_/1000, a[17]) + a[15];
      break;
    }
  case 2:
    {
      mlc = a[13] * pow(dis2stop_/1000, a[17]) + a[15] + a[16];
      break;
    }
  default:
    {
      mlc = (a[13]+a[14]*(n-2)) * pow(dis2stop_/1000, a[17]) +a[15] + a[16] * (n-1);
    }
  } 

  // Dan: If vehicle ahead is a bus and there is a bus stop ahead
  // in the lane, set busAheadDummy to 1 for disincentive to be
  // applied in the utility.

  int busAheadDummy = 0;

  if (av && av->type() == 0x3 && type() != 0x3) {

    CtrlList device = nextCtrl_;

    while (device && (*device)->type() == CTRL_BUSSTOP) {

      int n = (*device)->nLanes();
      for (int i = 0; i < n; i ++) {
        RN_Signal *signal = (*device)->signal(i);
        if (signal) {
	  if (plane->code() == signal->lane()->code()) {
            busAheadDummy = 1;
	  } 
        }
      }
      device = device->next();
    }
  }

  double u = a[1] + a[4] * vld + a[8] * spacing +a[6] * density + mlc + heavy_neighbor + right_most + a[5] * busAheadDummy; 

  return exp(u) ;
}


double TS_Vehicle::LCUtilityLookAheadCurrent(TS_Lane*plane, int n, float LCdistance)
{

  float *a = theParameter->laneUtilityParams();

   // LEADING AND LAG VEHICLES (do not have to be in same segment)

  TS_Vehicle* av = findFrontBumperLeader(plane);
  TS_Vehicle* bv = findFrontBumperFollower(plane);

  double vld, mlc, density, spacing; 

  density = plane->density();
  
  float heavy_neighbor = 0.0;

  if (av) {
	vld = Min(desiredSpeed_, av->currentSpeed_) ;
	heavy_neighbor = (av->isType(VEHICLE_SMALL)) ? 0.0 : a[7];
	spacing = this->gapDistance(av);
  } else {
    vld = desiredSpeed_ ;
    spacing = distance();
    if (nextLane_) {
      spacing += nextLane_->length();
    }

  }
  
  if (bv){

    heavy_neighbor = (bv->isType(VEHICLE_SMALL)) ? heavy_neighbor : a[7];

  }
 
float tailgate_dummy = 0;
TS_Vehicle* behind = this->vehicleBehind() ;
if (behind) {
double gap_behind = behind->gapDistance(this);
    float dens = tsSegment()->density();
    tailgate_dummy = (gap_behind <= a[10] && dens <= a[11])? a[9] : 0;
   }

  float right_most = 0.0;

  if ( !theNetwork->drivingDirection()==1) {  // for right hand driving
    right_most = (plane->right()) ? 0.0 : a[2]; 
  
  } else {  // for left hand driving, used as left_most variable
    right_most = (plane->left()) ? 0.0 : a[2]; 
  }

 switch (n) {
  case 0:
    {
      mlc = 0;
      break;
    }
  case 1:
    {
      mlc = a[12] * pow(dis2stop_/1000, a[17]) + a[15];
      break;
    }
  case 2:
    {
      mlc = a[13] * pow(dis2stop_/1000, a[17]) + a[15] + a[16];
      break;
    }
  default:
    {
      mlc = (a[13]+a[14]*(n-2)) * pow(dis2stop_/1000, a[17]) +a[15] + a[16] * (n-1);
    }
  } 

  // Dan: If vehicle ahead is a bus and there is a bus stop ahead
  // in the lane, set busAheadDummy to 1 for disincentive to be
  // applied in the utility.

  int busAheadDummy = 0;

  if (av && av->type() == 0x3 && type() != 0x3) {

    CtrlList device = nextCtrl_;

    while (device && (*device)->type() == CTRL_BUSSTOP) {

      int n = (*device)->nLanes();
      for (int i = 0; i < n; i ++) {
        RN_Signal *signal = (*device)->signal(i);
        if (signal) {
	  if (plane->code() == signal->lane()->code()) {
            busAheadDummy = 1;
	  } 
        }
      }
      device = device->next();
    }
  }


  double u = a[0]+ a[4] * vld + a[8] * spacing + a[6] * density + mlc + heavy_neighbor + right_most + tailgate_dummy + a[5] * busAheadDummy; 

  return exp(u) ;
}

#endif


float
TS_Vehicle::getOutDistance(RN_Lane * plane)
{
  // iterate all connected lanes in the downstream segment

  int idx = pathIndex_; 
  RN_Link* slink; 
  RN_Lane* slane;
  RN_Lane* qlane;
  RN_Link* plink = link();
  RN_Segment* psegment = plane->segment();
  float x=distance_;
  RN_Segment* ssegment;
  bool connect;
	    
  int* marked;


 vector <int > _downLaneList;        // list of local indexes to connected lanes in the next segment

 vector <int > _currentLaneList;        // list of local indexes to connected lanes in current segment
 _currentLaneList.erase(_currentLaneList.begin(), _currentLaneList.end() );

 _currentLaneList.push_back(plane->localIndex());

 bool allMarked = false;

  while ( x<lookAheadDistance_ )                 // within lookahead distance
    {
      if ( psegment != plink->endSegment() )     // still in this link
	{
	  ssegment = psegment->downstream();
	  slink = plink;
	}
      else if ( idx < path()->nLinks() )        // looking at the next link
	{
	 
	  if ( !path()->link(idx) )
	    {return x;}
	  slink = path()->link(idx);
	  //	  cout << plink << endl;
	  
	  ssegment = slink->startSegment();
	  idx++;
	}
      else { return x; }                            // end of path 
    
    

      bool lastConnect = false ;
      // iterate all connected lanes in the downstream segment
      for (int i = 0; i < _currentLaneList.size(); i ++) { 
	lastConnect = false ;
	qlane = psegment->lane(_currentLaneList[i]);

	TS_Lane* tlane = (TS_Lane*) psegment->lane(_currentLaneList[i]);
	if (! tlane->doesNotAllow(this)) // avoid lanes not allowed (e.g. HOV lane) (Angus)
       {
	 // iterate all connected lanes in the upstream segment
	 for (int j = 0; j < ssegment->nLanes(); j ++) { 

	   // avoid lanes that the vehicle is not allowed to use
	   
	   TS_Lane* rlane = (TS_Lane*) ssegment->lane(j);
	   if (! rlane->doesNotAllow(this)) { 
	     slane = ssegment->lane(j);
	     connect = qlane->isInDnLanes(slane);
	    
	     int marked = * ( find (_downLaneList.begin(), _downLaneList.end(), j ) );
	     if ( connect && marked == * (_downLaneList.end() )) { 
	       // put it at the end of the list
	       _downLaneList.push_back(j);
	       lastConnect = connect;
	       
	       // stop if all upstream lanes are connected	  
	       if ( _downLaneList.size() == ssegment->nLanes() ) {
		 allMarked = true;
		 break; 
	       }
	       else if (!connect && lastConnect) {break;}
	     }
	   }
	   else if (lastConnect ) { break; }
	 }
       }
	if (allMarked) {break;}
      }

      // the downstream set is empty. LC must happen downstream.  
      if ( _downLaneList.empty() ) { 
	return x;
      } else { 
	x += ssegment->length(); 
      } 
      _currentLaneList = _downLaneList;
      _downLaneList.erase(_downLaneList.begin(), _downLaneList.end() );

      psegment = ssegment;
      plink = slink;
    }		    
      
  return x;

}





// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

/*--------------------------------------------------------------------
 * This is the target gap model.
 *--------------------------------------------------------------------
 */

// Modified by VR 06/22/2007
void
TS_Vehicle::chooseTargetGap()
{
  // SET/UNSET FLAGS

  unsetStatus(STATUS_TARGET_GAP);

 if (flag(FLAG_NOSING) || flag(FLAG_NOSING_FEASIBLE) || flag(FLAG_STUCK_AT_END)) return;


   // TARGET LANE

  TS_Lane *plane;

  if (status(STATUS_LEFT)) {
	plane = lane_->left();
  } else if (status(STATUS_RIGHT)) {
	plane = lane_->right();
  } else {
	return;			// No request for lane change 
  }

  // Fix for sporadic crashes due to null pointer.  (Angus)
  if (!plane) return;

  if (isInIncidentArea(plane)) return;
 

   // FRONT, LEAD AND LAG VEHICLES (do not have to be in same segment)

  TS_Vehicle* av = findFrontBumperLeader(plane);
  // if (!av) {
  // setStatus(STATUS_ADJACENT);
  //   return;
  //}

  TS_Vehicle* bv = findFrontBumperFollower(plane);
  if ((!av)&&(!bv)) {
    setStatus(STATUS_ADJACENT);
      return;
  }
 
  TS_Vehicle* front = this->vehicleAhead();
  TS_Vehicle* back = this->vehicleBehind();

  int fg_av = 1;
  
  int fg_inf = 0;
  
  float dis2gap = 0.0;
  float effectiveGap = 0.0;
  float remainderGap = 0.0;
  float gapSpeed = 0.0;
  float dis2front = 0.0;

  // FORWARD GAP
 
  float d1, s1, d2; // d1 : effective gap, s1: gap rel. speed 

  if (!(av))
    { 
      fg_av = 0;
    }
  else
    {
  if ( av->vehicleAhead() ) { // VR: forward gap is finite
    d1 = av->gapDistance();
    s1 = av->currentSpeed() - av-> vehicleAhead()->currentSpeed();
    //    fg_inf = 0;
  } else {                    //  VR: forward gap is infinite
    d1 = av->distance();
    s1 = 0;
    if (av->nextLane_) {
      d1 = d1 + av->nextLane_->length();
    }
    fg_inf = 1;
  }
  dis2gap = this->gapDistance(av)+ av->length();


  // correct based on front vehicle
  if (!front) {  // VR: effective forward gap not restricted
    effectiveGap = d1;
    remainderGap = 0;
    gapSpeed = s1;
    fg_inf = fg_inf;
  }
  else {
    dis2front = this->gapDistance(front) + front->length();
    if (dis2gap > dis2front) // VR: Forward gap not available
      {
	effectiveGap = (-1) * (front->gapDistance(av) + av->length() + front->length());
	remainderGap = d1;
	gapSpeed = av->currentSpeed() - front->currentSpeed();
        fg_av = 0;
        fg_inf = 0;
      } 
    else {                 // VR : Forward gap available
      d2 = av->gapDistance(front);
      if (d1 >= d2) {      // VR : effective forward gap affected by front vehicle
	effectiveGap =  d2;  
	remainderGap = 1;
	gapSpeed = av->currentSpeed() - front->currentSpeed();
        fg_av = 1;
        fg_inf = 0;
      } 
      else {               // VR : effective forward gap not affected by front vehicle
	effectiveGap =  d1;
	remainderGap = 0;
	gapSpeed = s1;
        fg_av = 1;
        fg_inf = fg_inf;
      }
    }
  }
    }

  this->eff_fgap = effectiveGap;

  double eufwd = gapExpOfUtility(1, effectiveGap, dis2gap, gapSpeed, remainderGap, fg_av, fg_inf);

  float d3, d4, eff;
 
  // ADJACENT GAP
  TS_Vehicle* temp_fr = av;
  TS_Vehicle* temp_bk = bv;
 
  int ag_inf=0;
  int ag_av = 1;

  if ( (!av) || (!bv) )
    {
      ag_inf = 1;
      effectiveGap = 0;
      dis2gap = 0;
      gapSpeed = 0;
      remainderGap = 0;
    }
  else{

  d1 = bv->gapDistance();
  s1 = bv->currentSpeed() - av->currentSpeed();
  
  dis2gap = 0;
  remainderGap = 0;

  if ((!front)&&(!back)) {
    effectiveGap = d1;
    remainderGap = 0;
    gapSpeed = s1;
  }
  else
    {
      if (front)
         {
	   if( this->gapDistance(front) < this->gapDistance(av) )
	     { temp_fr = front;
	       remainderGap = 1;
             }
	 }
        
      if (back)
	{
           if( bv->gapDistance(this) > back->gapDistance(this) )
	     {  temp_bk = back;
                remainderGap = 1;
             } 
        }

  	effectiveGap = temp_bk->gapDistance(temp_fr);
      
	gapSpeed = temp_bk->currentSpeed() - temp_fr->currentSpeed();
     } 
    
 }
 
  this->eff_agap = effectiveGap;

  double euadj = gapExpOfUtility(3, effectiveGap, dis2gap, gapSpeed, remainderGap, ag_av, ag_inf);


  // BACKWARD GAP
  int bg_inf = 0;
  int bg_av = 1;

  if (!bv)
    {
      bg_av = 0;
    }
  else
    {
     
      if ( TS_Vehicle* bv2 = bv->vehicleBehind() ) { // backward gap is finite
      d1 = bv2->gapDistance();
      s1 = bv2->currentSpeed() - bv->currentSpeed();
       }  
      else {           // backward gap is infinite
    s1 = 0;
    d1 = bv->lane()->length() - bv->distance();
    bg_inf = 1;
  
    int i, n = bv->lane_->nUpLanes();
    if (n > 0) {
      d2 = FLT_INF;
      for (i = 0; i < n; i ++) {
	if (d3 = bv->lane_->upLane(i)->length() < d2) {
	  d2 = d3;
	}
      }
      d1 = d1 +d2;
    }

  }
	 
     dis2gap = bv->gapDistance(this)+ this->length();

     if (!back) {
    effectiveGap = d1;
    remainderGap = 0;
    gapSpeed = s1;	
    bg_inf = bg_inf;
    bg_av = 1;
     } else {
         if (back->gapDistance(this)<bv->gapDistance(this))
	 { 
           bg_av = 0;
           bg_inf = 0;
         }
         else 
         {
           if(back->gapDistance(bv)<d1)
	     {
               effectiveGap = back->gapDistance(bv);
               remainderGap = 1;
               gapSpeed = back->currentSpeed() - bv->currentSpeed();
               bg_inf = 0; 
               bg_av = 1;
             }
           else
	     {
               effectiveGap = d1;
               remainderGap = 0;
               gapSpeed = s1;	
               bg_inf = bg_inf;
               bg_av = 1;     
             }
	 }
     }
 }

  this->eff_bgap = effectiveGap;

  double eubck = gapExpOfUtility(2, effectiveGap, dis2gap, gapSpeed, remainderGap, bg_av, bg_inf);

 

 double sum = eufwd + eubck + euadj ;
  double rnd = theRandomizer->urandom() ;
  if (rnd < euadj / sum) setStatus(STATUS_ADJACENT);
  else if (rnd < (euadj + eubck) / sum) setStatus(STATUS_BACKWARD);
  else setStatus(STATUS_FORWARD);
 
  return;

} 

// modified by VR 06/22/2007
double TS_Vehicle::gapExpOfUtility(int n, float effGap, float gSpeed, float gapDis, float remDis, int gap_av, int gap_inf)
{
  
  
  float *a = theParameter->targetGapParams();
  
  double u = a[2] * effGap + a[3] * gSpeed + a[4] * gapDis  + a[5]*remDis;
  
  if (gap_av == 0)
    {
      double uu = 0.0;
      return uu;
    }

  if (gap_inf == 1)
    {
      u = a[6];
    } 
      //   if (remDis > 0.1) {
      //    u += a[5];
      //  }

  if (n == 1) u += a[0];
  else if (n==2) u += a[1];


  return exp(u) ;
}

#endif //end of ifndef USE_KAZI_MODELS

/*******************************************************************
 *******************************************************************
 *cfc: These are the target lane models 
 *******************************************************************
 *******************************************************************
 */

#ifdef USE_CFC_MODELS //only for target lane modesl : cfc



vector <double> TS_Vehicle::TLProbability(TS_Lane*plane)//cfc jun11 

{          
int n = plane->localIndex();  
RN_Segment* psegment = plane->segment(); 
float *c = theParameter->targetLaneParams(); // cfc june 3
int rules = lane_->cRules();
float nextExitDummy=0;


if (plane->isWrongLane(this) > 0){
  nextExitDummy=c[14];}


double uc=0, ul=0, ur=0;

//calculation of CL utility

TS_Vehicle* av = findFrontBumperLeader(plane);
TS_Vehicle* bv = findFrontBumperFollower(plane);

double vld, spacing;
float heavy_neighbor = 0.0;

  if (av) {
	vld = Min(desiredSpeed_, av->currentSpeed_) ;
	heavy_neighbor = (av->isType(VEHICLE_SMALL)) ? 0.0 : c[18];
	spacing = this->gapDistance(av);
  } else {
    vld = desiredSpeed_ ;
    spacing = distance_;
    if (nextLane_)
      spacing += nextLane_->length();
  }
  
  if (bv){
  heavy_neighbor = (bv->isType(VEHICLE_SMALL)) ? heavy_neighbor : c[18];

  }


  float tailgate_dummy = 0;
  TS_Vehicle* behind = this->vehicleBehind() ;
  if (behind) {
    double gap_behind = behind->gapDistance(this);
    float dens = tsSegment()->density();
    tailgate_dummy = (gap_behind <= c[9] && dens <= c[10])? c[8] : 0;
  }
  
  uc = c[6]+ c[7] * vld + c[15] * spacing  + tailgate_dummy+heavy_neighbor; 



//calutilation of RL utility : only if RL is in choice set

if( (plane = lane_->right()) && (!flag(FLAG_VMS_LANE_USE_LEFT) ||plane->localIndex() < VmsLaneUsePivotToIndex(flag())) &&
	  (rules & LANE_CHANGE_RIGHT || !attr(ATTR_GLC_RULE_COMPLY)) &&	  plane->isThereBadEventAhead(this) >= 0) {

double vld_right;
float heavy_neighbor_right = 0.0;
    

TS_Vehicle* av = findFrontBumperLeader(plane);
TS_Vehicle* bv = findFrontBumperFollower(plane);

 if (av) {
	vld_right = Min(desiredSpeed_, av->currentSpeed_) ;
	heavy_neighbor_right = (av->isType(VEHICLE_SMALL)) ? 0.0 : c[18];

  } else {
	vld_right = desiredSpeed_ ;
	
  }
  if (bv){

    heavy_neighbor_right = (bv->isType(VEHICLE_SMALL)) ? heavy_neighbor : c[18];

  }
 ur =c[18]+ c[7] * vld_right + heavy_neighbor_right; 

 }


//calculation of LL utility : only if LL is in choice set
  
 if( (plane = lane_->left()) && (!flag(FLAG_VMS_LANE_USE_LEFT) ||plane->localIndex() < VmsLaneUsePivotToIndex(flag())) &&
	  (rules & LANE_CHANGE_LEFT || !attr(ATTR_GLC_RULE_COMPLY)) &&	  plane->isThereBadEventAhead(this) >= 0) {
	  

double vld_left;
float heavy_neighbor_left = 0.0;
    
TS_Vehicle* av = findFrontBumperLeader(plane);
TS_Vehicle* bv = findFrontBumperFollower(plane);


  if (av) {
	vld_left = Min(desiredSpeed_, av->currentSpeed_) ;
	heavy_neighbor_left = (av->isType(VEHICLE_SMALL)) ? 0.0 : c[18];

  } else {
	vld_left = desiredSpeed_ ;
	
  }
  if (bv){

    heavy_neighbor_left = (bv->isType(VEHICLE_SMALL)) ? heavy_neighbor : c[18];

  }
 ul = c[7] * vld_left + heavy_neighbor_left; 

 }

//cfc:  calculating lane specific variables

 int laneNum= psegment->nLanes();
 vector <double> ulane(laneNum);
 vector<double> laneDensity(laneNum);
 vector<double> laneSpeed(laneNum);
  
for (int j = 0; j <laneNum; j ++) { 

        TS_Lane* qlane = (TS_Lane*) psegment->lane(j);
	laneDensity[j]=qlane->density();
	laneSpeed[j]=qlane->calcSpeed();
	ulane[j]=0; //initialize
	if ((qlane->isHovLane())&&(!(qlane->doesNotAllow(this))))
	  {ulane[0]=c[1];}// cfc june 24


	
}

//if allowed to go to hov add hov dummy



//allocate ucl, url,ul;

switch (n){
 case 0://leftmost lane
   {
     ulane[0]=ulane[0]+uc;
     ulane[1]=ur;
     break;
   }
 
 default://anylane
   { 
     ulane[n]=uc;
     ulane[n-1]=ul;
     if (n!=(laneNum-1)) {//not rightmost lane
           ulane[n+1]=ur;
                         }
      else {//rightmost lane
           ulane[n]=ulane[n];}//c[0]=righmost lane dummy
   }//default

}//switch

 for (int k=0;k<laneNum;k++){

//allocating mlc and constants if next exit


 if (k==(laneNum-1))//no lane change required: (laneNum-1) is rightmostLane local Index

   {ulane[k]=ulane[k];
       }

else   if (k==(laneNum-2))//1 lane change required: (laneNum-1) is rightmostLane local Index

       {ulane[k]=ulane[k]+c[11]*pow(dis2stop_/1000,c[17]);
       }
  
       else if (k==(laneNum-3)){//2 lane chaneg Required :(laneNum-1) is rightmostLane local Index
      ulane[k]=ulane[k]+c[12]*pow(dis2stop_/1000,c[17]);
      }
   
      else  {
        ulane[k]=ulane[k]+c[13]*(k-2)*pow(dis2stop_/1000,c[17]);//c[13]is for each additional change after 2
  
	     }

   

 ulane[k]=exp(ulane[k]+c[3]*laneDensity[k]+c[4]*laneSpeed[k]+c[5]*(abs(n-k))+nextExitDummy*((laneNum-1)-k)+c[19]*((laneNum-1)-k));

 }//for

  double ulane0=ulane[0];//debug:cfc
 double ulane1=ulane[1];//debug:cfc
 double ulane2=ulane[2];//debug:cfc
 double ulane3=ulane[3];//debug:cfc

 double sum=0;

 for (int i=0;i<laneNum;i++){
   sum=sum+ulane[i];
 }

 vector <double> probLane(laneNum);

 for (int i=0;i<laneNum;i++){
   probLane[i]=ulane[i]/sum;
   double prob=probLane[i];
 }
 return probLane;

}




  /*
  ****************************TLProbailityLookAhead**********
  */


vector <double> TS_Vehicle::TLProbabilityLookAhead(TS_Lane*plane, vector <int> & change, vector <double> & LCdistance) 

{          
int n = plane->localIndex();  
RN_Segment* psegment = plane->segment(); 
float *c = theParameter->targetLaneParams(); // cfc june 3
int rules = lane_->cRules();

double uc=0, ul=0, ur=0;

//calculation of CL utility

TS_Vehicle* av = findFrontBumperLeader(plane);
TS_Vehicle* bv = findFrontBumperFollower(plane);

double vld, spacing; 

float heavy_neighbor = 0.0;

  if (av) {
	vld = Min(desiredSpeed_, av->currentSpeed_) ;
	heavy_neighbor = (av->isType(VEHICLE_SMALL)) ? 0.0 : 0;//c[18];
	spacing = this->gapDistance(av);
  } else {
    vld = desiredSpeed_ ;
    spacing = distance_;
    if (nextLane_)
      spacing += nextLane_->length();
  }
  
  if (bv){
  heavy_neighbor = (bv->isType(VEHICLE_SMALL)) ? heavy_neighbor : 0; // c[18];

  }


  float tailgate_dummy = 0;
  TS_Vehicle* behind = this->vehicleBehind() ;
  if (behind) {
    double gap_behind = behind->gapDistance(this);
    float dens = tsSegment()->density();
    tailgate_dummy = (gap_behind <= c[9] && dens <= c[10])? c[8] : 0;
  }
  
  uc = c[6]+ c[7] * vld + c[15] * spacing  + tailgate_dummy+heavy_neighbor; 

 
double vld_right=0;
double vld_left=0;


//calutilation of RL utility : only if RL is in choice set



if( (plane = lane_->right()) && (!flag(FLAG_VMS_LANE_USE_RIGHT) ||plane->localIndex() < VmsLaneUsePivotToIndex(flag())) &&
	  (rules & LANE_CHANGE_RIGHT || !attr(ATTR_GLC_RULE_COMPLY)) &&	  plane->isThereBadEventAhead(this) >= 0) {


  float heavy_neighbor_right = 0.0;
    

TS_Vehicle* av = findFrontBumperLeader(plane);
TS_Vehicle* bv = findFrontBumperFollower(plane);

 if (av) {
	vld_right = Min(desiredSpeed_, av->currentSpeed_) ;
	heavy_neighbor_right = (av->isType(VEHICLE_SMALL)) ? 0.0 : 0 ;// c[18];

  } else {
	vld_right = desiredSpeed_ ;
	
  }
  if (bv){

    heavy_neighbor_right = (bv->isType(VEHICLE_SMALL)) ? heavy_neighbor : 0 ;// c[18];

  }
 ur = c[16]+c[7] * vld_right + heavy_neighbor_right; 

 }


//calculation of LL utility : only if LL is in choice set
  
 if( (plane = lane_->left()) && (!flag(FLAG_VMS_LANE_USE_LEFT) ||plane->localIndex() < VmsLaneUsePivotToIndex(flag())) &&
	  (rules & LANE_CHANGE_LEFT || !attr(ATTR_GLC_RULE_COMPLY)) &&	  plane->isThereBadEventAhead(this) >= 0) {
	  
   //double vld_left;
float heavy_neighbor_left = 0.0;
    

TS_Vehicle* av = findFrontBumperLeader(plane);
TS_Vehicle* bv = findFrontBumperFollower(plane);


  if (av) {
	vld_left = Min(desiredSpeed_, av->currentSpeed_) ;
	heavy_neighbor_left = (av->isType(VEHICLE_SMALL)) ? 0.0 : 0 ;// c[18];

  } else {
	vld_left = desiredSpeed_ ;
	
  }
  if (bv){

    heavy_neighbor_left = (bv->isType(VEHICLE_SMALL)) ? heavy_neighbor : 0 ;// c[18];

  }
 ul = c[16]+c[7] * vld_left + heavy_neighbor_left; 

 }

//cfc:  calculating lane specific variables

 int laneNum= psegment->nLanes();
 vector <double> ulane(laneNum);
 vector<double> laneDensity(laneNum);
 vector<double> laneSpeed(laneNum);

 vector <int> dummyCL(laneNum);
 vector<int>  dummyRL(laneNum);
 vector<int>  dummyLL(laneNum);
 vector<int>  hovDummy(laneNum);
  
 for (int j = 0; j <laneNum; j ++) { //loop through all lanes to fill in the probability vector

        TS_Lane* qlane = (TS_Lane*) psegment->lane(j);
	laneDensity[j]=qlane->density();
	laneSpeed[j]=qlane->calcSpeed();
	ulane[j]=0; //initialize
	if ((qlane->isHovLane())&&(!(qlane->doesNotAllow(this))))
	  {hovDummy[0]=1;
	  ulane[0]= c[19];}// cfc june 24

 }



// ulane[0]=c[19];//c[19] is HOV lane dummy

//allocate ucl, url,ul;



switch (n){
 case 0://leftmost lane
   {
     ulane[0]=ulane[0]+uc;
     ulane[1]=ur;
     dummyCL[0]=1;
     dummyRL[1]=1;
      break;
   }
 
 default://anylane
   { 
     ulane[n]=uc;
     ulane[n-1]=ul;
 
     dummyCL[n]=1;
     dummyLL[n-1]=1;

     if (n!=(laneNum-1)) {//not rightmost lane
       ulane[n+1]=ur;
       dummyRL[n+1]=1;}

     // else //rightmost lane
     // {ulane[n]=0;}
   
}//default

}//switch

double ulane0=ulane[0];//debug:cfc
double ulane1=ulane[1];//debug:cfc
double ulane2=ulane[2];//debug:cfc
double ulane3=ulane[3];//debug:cfc



for (int k=0;k<laneNum;k++){ //allocating constants

  
      if (k==(laneNum-1))//rightmost lane
      {ulane[k]=ulane[k]+c[0]+c[20]*aggresiveness();}

      else{
	    if (k==(laneNum-2))//2nd from rightmost lane
	       {ulane[k]=ulane[k]+c[1]+c[21]*aggresiveness();}

	    else{ //not rightmost or 2nd from right 
	      if (k==(laneNum-3))//3rd from right
		{ulane[k]=ulane[k]+c[2]+c[22]*aggresiveness();}
	      else//4th or higher
{ ulane[k]=ulane[k]+(c[1]+((c[2]-c[1])*(laneNum-k-2)))+c[18]*aggresiveness();}

	        
                 }//
	
//c[18]=constant for lane>3 (c[2]-c[1])=coeff for each additional lane change reqd>2
 
      }

int numChange=change[k];

float  changeDummy=0;

switch (numChange){
 
case 0:
   {ulane[k]=ulane[k];

   break;
   }
 case 1:
   {ulane[k]=ulane[k]+c[14]+c[11]*pow(LCdistance[k]/1000,c[17]);
   changeDummy=c[14];//LCdistance[k]
   break;
   }
 case 2:
   {ulane[k]=ulane[k]+c[14]+c[12]*pow(LCdistance[k]/1000,c[17]);
   changeDummy=c[14];
   break;
   }
 default:
 
   {ulane[k]=ulane[k]+c[14]+(c[12]+(c[13]-c[12]))*(numChange-2)*pow(LCdistance[k]/1000,c[17]);//c[13]-c[12]=coeff for each additional lane change reqd>2
    changeDummy=c[14];
   }
}//switch

 ulane[k]=exp(ulane[k]+c[3]*laneDensity[k]+c[4]*laneSpeed[k]+c[5]*(abs(n-k)-1)); //c[5]=coeff of each additional lc reqd

                                                                                                                                            


ulane0=ulane[0];//debug:cfc
ulane1=ulane[1];//debug:cfc
ulane2=ulane[2];//debug:cfc
ulane3=ulane[3];//debug:cfc

}//for         

ulane0=ulane[0];//debug:cfc
ulane1=ulane[1];//debug:cfc
ulane2=ulane[2];//debug:cfc
ulane3=ulane[3];//debug:cfc


double sum=0;

 for (int i=0;i<laneNum;i++){
   sum=sum+ulane[i];
 }

 vector <double> probLane(laneNum);

 for (int i=0;i<laneNum;i++){
   probLane[i]=ulane[i]/sum;
   double prob=probLane[i];
 }

  
 return probLane;
                                                                                                                                     

}



# endif


