//-*-c++-*------------------------------------------------------------
// NAME: Microscopic Traffic Simulator
// NOTE: 
// AUTH: Qi Yang
// FILE: TS_Link.h
// DATE: Fri May 10 10:38:20 1996
//--------------------------------------------------------------------

#ifndef TS_LINK_HEADER
#define TS_LINK_HEADER

#include <iostream>
#include <GRN/RN_Link.h>

class TS_Vehicle;

class TS_Link : public RN_Link
{
      friend class TS_Network;

   public:

      TS_Link();
      ~TS_Link() { }

      inline TS_Vehicle* queueHead() { return queueHead_; }
      inline TS_Vehicle* queueTail() { return queueTail_; }
      inline int queueLength() { return queueLength_; }
	
      int reportQueueLength(ostream &os = cout);

      void dequeue(TS_Vehicle *);
      void queue(TS_Vehicle *);

      void checkConnectivity();

      // Calculate average speed for the vehicles currently in the
      // link

      float calcTravelTime(); // virtual

	  void vehicleUpdateNextCtrl(RN_CtrlStation *);

#ifdef COUNT_DLC
      long int num_dlc ;
#endif

   protected:

      TS_Vehicle *queueHead_;	// first vehicle in the queue
      TS_Vehicle *queueTail_;	// last vehicle in the queue
      int queueLength_;		// number of vehicle in the queue
};

#endif

