//-*-c++-*------------------------------------------------------------
// NAME: Bus sensing parameters table
// AUTH: Qi Yang & Daniel Morgan
// FILE: TS_BusCommPrmTable.h
// DATE: Fri Nov 30 16:25:20 2001
//--------------------------------------------------------------------

#ifndef TS_BUSCOMMPRMTABLE_HEADER
#define TS_BUSCOMMPRMTABLE_HEADER

#include <GRN/RN_BusCommPrmTable.h>

class TS_BusCommPrmTable : public RN_BusCommPrmTable
{
   public:

      TS_BusCommPrmTable() : RN_BusCommPrmTable() { }
      ~TS_BusCommPrmTable() { }

};

#endif
