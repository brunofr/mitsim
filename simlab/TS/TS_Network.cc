//-*-c++-*------------------------------------------------------------
// NAME: Microcscopic Traffic Simulator
// AUTH: Qi Yang
// FILE: TS_Network.C
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#include <new>
using namespace std;

#include <Tools/Math.h>

#include <Tools/SimulationClock.h>
#include <Tools/ToolKit.h>
#include <Tools/Random.h>
#include <Tools/Reader.h>

#include <IO/IOService.h>
#include <GRN/RN_DynamicRoute.h>
#include <GRN/RN_CtrlStation.h>
#include "TS_Engine.h"
#include "TS_Network.h"
#include "TS_Node.h"
#include "TS_Link.h"
#include "TS_Segment.h"
#include "TS_Lane.h"
#include "TS_Engine.h"
#include "TS_Status.h"
#include "TS_Sensor.h"
#include "TS_Signal.h"
#include "TS_TollBooth.h"
#include "TS_BusStop.h"  //margaret
#include "TS_FileManager.h"
#include "TS_Parameter.h"
#include "TS_MoeCollector.h"
#include "TS_QueueHead.h"
#include "TS_Exception.h"
#include "TS_Incident.h"
#include "TS_VehicleList.h"

#ifdef INTERNAL_GUI
#include <DRN/DRN_DrawingArea.h>
#include "TS_Symbols.h"
#include "TS_Interface.h"
#include "TS_DrawableVehicle.h"
#include "TS_Incident.h"
#include "TS_IncidentDialog.h"
#include "TS_TraceDialog.h"
#endif

#include "TS_Vehicle.h"

#ifdef EXTERNAL_MOE
#include <MOE/MOE_TSSampler.h>
#endif

TS_Network::TS_Network()
  : DrawableRoadNetwork()
{
}

TS_Network::~TS_Network() {
}


RN_Node* TS_Network::newNode() { 
  return new TS_Node; 
}

RN_Link* TS_Network::newLink() { 
  return new TS_Link;
}

RN_Segment* TS_Network::newSegment() {
  return new TS_Segment;
}

RN_Lane* TS_Network::newLane() {
  return new TS_Lane;
}

RN_Sensor* TS_Network::newSensor()
{
  return new TS_Sensor;
}

RN_Signal* TS_Network::newSignal()
{
  return new TS_Signal;
}

RN_TollBooth* TS_Network::newTollBooth()
{
  return new TS_TollBooth;
}


RN_BusStop* TS_Network::newBusStopChars()
{
  return new TS_BusStop;
}

void
TS_Network::calcStaticInfo(void)
{
  RoadNetwork::calcStaticInfo();
  organize();
}


void
TS_Network::organize()
{
  register int i;

  for (i = 0; i < nLinks(); i ++) {
	tsLink(i)->checkConnectivity();
  }
}



/*
 *-------------------------------------------------------------------
 * Print all vehicles
 *-------------------------------------------------------------------
 */

void
TS_Network::watchVehicles(ostream &os)
{
  watchVehiclesInPretripQueue(os);
  watchVehiclesInNetwork(os);
}


void
TS_Network::watchVehiclesInPretripQueue(ostream &os) 
{
  if (theStatus->nInQueue() <= 0) return;

  TS_Link * plink; 
  TS_Vehicle *pv;				/* Pointer to a vehicle */

  os << endl << theStatus->nInQueue()
	 << " Vehicles in the pretrip queues at time "
	 << theSimulationClock->currentTime() << ":" << endl;

  for (register int i = 0; i < nLinks(); i ++) /* Loop all links */
	{
      plink = tsLink(i);
      if (pv = plink->queueHead())
		{
		  os << indent << plink->name()
			 << " (" << plink->code() << ")" << endl;
		  while (pv)			/* Loop all vehicles */
			{
			  os << indent;
			  pv->watch(os);
			  pv = pv->trailing();
			}
		}
	}
}


/*
 *--------------------------------------------------------------------
 * Print information of each vehicles currently in the network.
 *--------------------------------------------------------------------
 */

void
TS_Network::watchVehiclesInNetwork(ostream &os)
{
  if (theStatus->nActive() <= 0) return;

  TS_Lane *plane;
  TS_Vehicle *pv;				/* Pointer to a vehicle */

  os << endl << theStatus->nActive()
	 << " Vehicles in the network at time "
	 << theSimulationClock->currentTime() << ":" << endl;

  for (register int i = 0; i < nLanes(); i ++) {
	plane = tsLane(i);
	if (pv = plane->firstVehicle())
      {
		os << indent << plane->link()->name()
		   << " (" << plane->link()->code()
		   << "," << plane->segment()->localIndex()
		   << "," << plane->localIndex() << ")" << endl;

		while (pv)				/* Loop all vehicles */
		  {
			os << indent;
			pv->watch(os);
			pv = pv->trailing();
		  }
      }
  }
}


/*
 *-------------------------------------------------------------------
 * Print the queue length at each loading point.
 *-------------------------------------------------------------------
 */

int TS_Network::watchQueueLength(ostream& os)
{
  int n = 0;
  for (register int i = 0; i < nLinks(); i ++) {
	n += tsLink(i)->reportQueueLength(os);
  }
  return n;
}

/*
 *-------------------------------------------------------------------
 * Calculate the density for each segments
 *-------------------------------------------------------------------
 */

void TS_Network::calcSegmentData()
{
  TS_Segment *ps;
  for (register int i = 0; i < nSegments(); i ++) {
	ps = tsSegment(i);
	ps->calcDensity();
	ps->calcSpeed();
	ps->calcFlow();
  }
}


// Calculate the lane speed

void TS_Network::calcLaneSpeeds()
{
  register int i;
  for (i = 0; i < nLanes(); i ++) {
	tsLane(i)->calcSpeed();
  }
   
  for (i = 0; i < nLanes(); i ++) {
	tsLane(i)->calcMaxSpeed();
  }
}


/*
 *--------------------------------------------------------------------
 * Enter vehicles queued at starting link into the network.
 *--------------------------------------------------------------------
 */

void
TS_Network::enterVehiclesIntoNetwork()
{
  TS_Vehicle *pv;
    
  for (register int i = 0; i < nLinks(); i ++)	{ /* Loop each link */

	/* Find the first vehicle in the queue and enter it into the
	 * network if space is available. If the link is full, there is
	 * no need for checking other vehicles in the queue, skip to the
	 * next link.  */

	while ((pv = tsLink(i)->queueHead()) &&
		   (pv->enterNetwork())) {
	  /* push static vehicle attributes on message buffer */
	  ;
	}
  }
}


/*
 *--------------------------------------------------------------------
 * For each vehicle in the network, check if a lane change is
 * necessary.  If yes, it checks if the lane changing is safe.
 * If yes, it makes the lane change.
 *--------------------------------------------------------------------
 */

void
TS_Network::calcVehicleStates()
{
  TS_Segment *ps;
  TS_Vehicle *pv;
  TS_Vehicle *next;
   
  for (register int i = 0; i < nLinks(); i ++) {

	ps = (TS_Segment *)link(i)->endSegment();

	while (ps) {		// loop every segment, downstream first

	  next = ps->firstVehicle();
		 
	  bool marked = false;

	  while((pv = next) != NULL) {
			 
		// Save the pointer to the next vehicle because the
		// vehicle "pv" may moves into other lanes.

		next = pv->macroTrailing();

		if (pv->cfTimer() <= TIME_EPSILON) {
			 
		  if (!marked) {
			ps->markConnectedDnLanes();
			marked = true;
		  }

#ifndef USE_KAZI_MODELS
		  // Invoke lane changing logic. Tomer: lane changing before acceleration
		  //		  int z = pv->code();
		  
		  // TARGET LANE MODEL
		  pv->makeLaneChangingDecision();
		  
		  if (pv->status() & STATUS_CHANGING) {
		    // GAP ACCEPTANCE MODEL
			pv->executeLaneChanging();			 
			
			if ( (pv->flag(FLAG_LC_FAILED))&& (!(pv->flag(FLAG_NOSING)))&& (!(pv->flag(FLAG_COURTESY))) ) { // VR 06/07
			  // Gap rejected under normal state: TARGET GAP MODEL
			  pv->chooseTargetGap();
			}
			else
			  {
			    //	    unsetStatus(STATUS_TARGET_GAP);
                          }
		  }
#endif
		  // Invoke car-following logic.
		  // ACCELERATION MODEL
		  pv->makeAcceleratingDecision();


		  // Invoke lane changing logic.

#ifdef USE_KAZI_MODELS
		 
		  pv->makeLaneChangingDecision();

		  if (pv->status() & STATUS_CHANGING) {
			pv->executeLaneChanging();			 
		  }
#endif
		}
	  }
		 
	  if (marked) {
		ps->unmarkConnectedDnLanes();
	  }

	  ps = (TS_Segment *)ps->upstream();
	}
  }
}


/*
 *--------------------------------------------------------------------
 * Move vehicles based on their speed and acceleration rate as
 * computed by computeTS_Vehicle().
 *--------------------------------------------------------------------
 */

void
TS_Network::moveVehicles(void)
{
  TS_Segment* ps;
  TS_Vehicle *pv;
  TS_Vehicle *next;
  register int i;

  /* generate a random order for the links */

  static int *permuteLink = NULL;

  if (!permuteLink) {
	permuteLink = new int[nLinks()];
	for (i = 0; i < nLinks(); i ++) permuteLink[i] = i;
  }

  theRandomizers[Random::Misc]->permute(nLinks(), permuteLink);

  for (i = 0; i < nLinks(); i ++) {
	ps = (TS_Segment *)link(permuteLink[i])->endSegment();
 
	while (ps) {

	  next = ps->firstVehicle();

	  while((pv = next) != NULL) {

		/* Save the pointer to the next vehicle because the
		 * vehicle "pv" may moves into a downsteam lane or removed
		 * from the network. */

		next = pv->macroTrailing();
		if (!pv->isProcessed(FLAG_PROCESSED)) {
		  pv->move();
		  pv->toggleFlag(FLAG_PROCESSED);
		}
	  }
	  ps = (TS_Segment *)ps->upstream();
	}
  }
	
  // Static variable of value 1/0. Used to flag whether a vehicle has
  // been moved in a cycle (its flag has same value as cycle_flag)

  TS_Vehicle::toggleCycleFlag(FLAG_PROCESSED);
}

//IEM(Jun20) This is the old, pre-vector, version
//void
//TS_Network::resetSensorReadings()
//{
//  SimulationClock *clock = theSimulationClock;
//  for (register int i = 0; i < nSensors(); i ++) { 
//	sensor(i)->resetSensorReadings();
//  }
//  TS_Sensor::resetTime_ = clock->currentTime();
// //IEM(Jun21) this is a comment within a comment, kind od like a
// // vector of vectors, and unlike a vector of ofstreams, in that
// // it can be done.
//#ifdef INTERNAL_GUI
//  if (clock->currentTime() > clock->startTime()) {
//	theModeline()->updateTimer(2, clock->currentTime());
//  }
//#endif
//}


//IEM(May2) overloaded for vector [old version commented out above]
void
TS_Network::resetSensorReadings(int intervalType)
{
  
  if(nSensors() == 0) {return;} //IEM(Jul25) Anti-segfault short-circuit kludge
  
  SimulationClock *clock = theSimulationClock;
  for (register int i = 0; i < nSensors(intervalType); i ++) {
	sensor(intervalType, i)->resetSensorReadings();
  }
  
  TS_Sensor::resetTimes_[intervalType] = clock->currentTime();

#ifdef INTERNAL_GUI
  if (clock->currentTime() > clock->startTime()) {
	theModeline()->updateTimer(2, clock->currentTime());
  }
#endif
}



/*
 *------------------------------------------------------------------
 * Writes sensor readings into a file if "Sensor readings"
 * is selected by command line switch (o).
 *------------------------------------------------------------------
 */

//IEM(Jun20) This is the old, pre-vector, version
//void
//TS_Network::writeSensorReadings()
//{
//  static int heading = 1;
//   static const char* spd[] = {
// 	"Speed", "HarmonicMeanSpeed"
//   };

//   ofstream &os = theFileManager->osSensor();

//   if (heading) {
// 	if (!theEngine->skipComment()) {
// 	  int i = TS_Sensor::harmonic() ? 1 : 0;
// 	  if (theEngine->isRectangleFormat()) {
// 		os << "# Time Sensor Keys"
// 		   << " Count(" << hex
// 		   << SENSOR_ACODE(SENSOR_ACOUNT) << dec << ")"
// 		   << " " << spd[i] << "(" << hex
// 		   << SENSOR_ACODE(SENSOR_ASPEED) << dec << ")"
// 		   << " Occupancy(" << hex
// 		   << SENSOR_ACODE(SENSOR_AOCCUPANCY) << dec << ")"
// 		   << endl;
// 	  } else {
// 		os << "# Time " << OPEN_TOKEN << endl
// 		   << "#   Sensor Keys"
// 		   << " Count(" << hex
// 		   << SENSOR_ACODE(SENSOR_ACOUNT) << dec << ")"
// 		   << " " << spd[i] << "(" << hex
// 		   << SENSOR_ACODE(SENSOR_ASPEED) << dec << ")"
// 		   << " Occupancy(" << hex
// 		   << SENSOR_ACODE(SENSOR_AOCCUPANCY) << dec << ")"
// 		   << endl
// 		   << "# " << CLOSE_TOKEN << endl;
// 	  }
// 	}
// 	heading = 0;
//   }

//   if (!theEngine->isRectangleFormat()) {
// 	os << endl << theSimulationClock->currentTime()
// 	   << endc << OPEN_TOKEN << endl;
//   }

//   for (register int i = 0; i < nSensors(); i ++) {
// 	sensor(i)->write(theFileManager->osSensor());
//   }

//   if (!theEngine->isRectangleFormat()) {
// 	os << CLOSE_TOKEN << endl;
//   }
//   os.flush();
// }

//IEM(May2) overloaded version for vector of types [commented-out old version above]
void
TS_Network::writeSensorReadings(int intervalType)
{
  static vector<int> headings(theEngine->nSurvPointStepSizes(), 1);
        //IEM(Jun19) So that the additional files get headings also
  static const char* spd[] = {
	"Speed", "HarmonicMeanSpeed"
  };

  //IEM(Jun19) Use correct output file for this type of sensor 
  ofstream &os = theFileManager->osSensor(intervalType);

  if (headings[intervalType]) {

	if (!theEngine->skipComment()) {
	  int i = TS_Sensor::harmonic() ? 1 : 0;
	  if (theEngine->isRectangleFormat()) {
		os << "# Time Sensor Keys"
		   << " Count(" << hex
		   << SENSOR_ACODE(SENSOR_ACOUNT) << dec << ")"
		   << " " << spd[i] << "(" << hex
		   << SENSOR_ACODE(SENSOR_ASPEED) << dec << ")"
		   << " Occupancy(" << hex
		   << SENSOR_ACODE(SENSOR_AOCCUPANCY) << dec << ")"
		   << endl;
	  } else {
		os << "# Time " << OPEN_TOKEN << endl
		   << "#   Sensor Keys"
		   << " Count(" << hex
		   << SENSOR_ACODE(SENSOR_ACOUNT) << dec << ")"
		   << " " << spd[i] << "(" << hex
		   << SENSOR_ACODE(SENSOR_ASPEED) << dec << ")"
		   << " Occupancy(" << hex
		   << SENSOR_ACODE(SENSOR_AOCCUPANCY) << dec << ")"
		   << endl
		   << "# " << CLOSE_TOKEN << endl;
	  }
	}
	headings[intervalType] = 0;
  }

  if (!theEngine->isRectangleFormat()) {
	os << endl << theSimulationClock->currentTime()
	   << endc << OPEN_TOKEN << endl;
  }

  for (register int i = 0; i < nSensors(intervalType); i ++) {
        //IEM(Jun19) Use correct output file for this type of sensor
	  sensor(intervalType, i)->write(theFileManager->osSensor(intervalType));
  }

  if (!theEngine->isRectangleFormat()) {
	os << CLOSE_TOKEN << endl;
  }
  os.flush();
}

//IEM(Jun20) This is the old, pre-vector, version
// void
// TS_Network::dumpSensorReadings(double fromTime, double toTime)
// {
//   char* fileName = Copy( ToolKit::outfile("sdr.tmp"));
//   static ofstream os( fileName );
//   free( fileName );

//   os << "[" << fromTime << "," << toTime <<"]" << endl << "{" << endl;

//   for (register int i = 0; i < nSensors(); i ++) 
//   {
// 	sensor(i)->write( os );
//   }

//   os << "}" << endl;

//   os.flush();
// }

//IEM(May2) overload for vector [delete old version, or is this version useless?]
void
TS_Network::dumpSensorReadings(double fromTime, double toTime, int intervalType)
{
  char* fileName = Copy( ToolKit::outfile("sdr.tmp"));
  static ofstream os( fileName );
  free( fileName );

  os << "[" << fromTime << "," << toTime <<"]" << endl << "{" << endl;

  for (register int i = 0; i < nSensors(intervalType); i ++) 
  {
	sensor(intervalType, i)->write( os );
  }

  os << "}" << endl;

  os.flush();
}

/*
 *------------------------------------------------------------------
 * Broadcasts sensor readings to clients if MULTIPLE_PROCESSORS is
 * defined and "Sending surveillance readings" is enabled by the
 * command line switch (c)
 *------------------------------------------------------------------
 */

//IEM(Jun20) This is the old, pre-vector, version
// void
// TS_Network::sendSensorReadings(IOService& service, unsigned int code)
// {
// //IEM(Jun18) Removed sensor reset so that sensors won't get zeroed on others' intervals
// //           I think this is safe.  See TC_Sensor::send() for a change I made to make
// //           sure this is OK.
// //Removed: service << MSG_SENSOR_RESET << code;
// //Removed: service.flush(SEND_IMMEDIATELY);

//   for (register int i = 0; i < nSensors(); i ++) {
// 	RN_Sensor * s = sensor(i);
// 	if (s->tasks() & code) s->send(service);
//   }
//   service.flush(SEND_END);

//   service << MSG_SENSOR_READY << code;
//   service.flush(SEND_IMMEDIATELY);
// }

//IEM(May2) overload for vector [commented-out old version above]
void
TS_Network::sendSensorReadings(IOService& service, unsigned int code, int intervalType)
{
//IEM(Jun18) Removed sensor reset so that sensors won't get zeroed on others' intervals
//           I think this is safe.  See TC_Sensor::send() for a change I made to make
//           sure this is OK.
//Removed: service << MSG_SENSOR_RESET << code;
//Removed: service.flush(SEND_IMMEDIATELY);

  for (register int i = 0; i < nSensors(intervalType); i ++) {
	RN_Sensor * s = sensor(intervalType, i);
	if (s->tasks() & code) s->send(service);
  }
  service.flush(SEND_END);

  service << MSG_SENSOR_READY << code;
  service.flush(SEND_IMMEDIATELY);
}


/*
 * This function is called ONLY ONCE after reading the
 * network database and before starting simulation.
 */

void
TS_Network::initializeSegmentStatistics()
{
  for (register int i = 0; i < nSegments(); i ++) {
	tsSegment(i)->initializeStatistics();
  }
}

/*
 * This function is called every time when the segment statistics
 * has been reported.
 */

void
TS_Network::resetSegmentStatistics()
{
  for (register int i = 0; i < nSegments(); i ++) {
	tsSegment(i)->resetLeavingStatistics();
  }
}

/*
 * This function is called at the a fixed sampling rate
 */

void
TS_Network::updateSegmentStatistics()
{
  for (register int i = 0; i < nSegments(); i ++) {

	// accumulate current density and speed

	tsSegment(i)->samplingStatistics();

	/* Other segment statistics are recorded as a vehicle leaves the
	 * segments. These include: (a) num of vehicles enter and leave
	 * the segment (b) travel time vehicles spent in the segment You
	 * may want to add the some other statistics such as number of
	 * vehicles in queue, emission, etc. */
  }
} 


/*
 * This function is called at the end of each reporting interval
 * during the simulation.
 */

void
TS_Network::outputSegmentStatistics()
{
  static int head = 1;
  ofstream &os = theFileManager->osSegmentStatistics();
  if (head) {
	os << "% SEGMENT STATISTICS" << endl;
	os << "% Generated by MITSIM on " << TimeTag() << endl;
	os << "% Reporting time " << OPEN_TOKEN << endl;
	os << "%   Segment EnterCount LeaveCount ";
	os << "Density Speed TravelTime " << OPEN_TOKEN << endl;
	os << "%     { LaneDensity LaneSpeed }" << endl;
	os << "%   " << CLOSE_TOKEN << endl;
	os << "% " << CLOSE_TOKEN << endl;
	head = 0;
  }
  os << endl << Round(theSimulationClock->currentTime());
  os << endc << OPEN_TOKEN << endl;
   
  for (register int i = 0; i < nSegments(); i ++) {

#ifdef EXTERNAL_MOE
	if (tsSegment(i)->isMOESelected())
#endif
	  tsSegment(i)->printStatistics(os);
  }
  os << CLOSE_TOKEN << endl;
}


/*
 * This function should be called at the end of the simulation
 */

void
TS_Network::outputSegmentTravelTimes()
{
  /* Open segment travel time file */

  char * fname = theFileManager->segmentTravelTimesFile();
  ofstream os(ToolKit::outfile(fname));
  if (!theEngine->skipComment()) {
	os << "% SEGMENT TRAVEL TIMES" << endl;
	os << "% Generated on " << TimeTag() << endl;
	os << theGuidedRoute->infoStartTime()
	   << "\t% Start time" << endl;
	os << theGuidedRoute->infoPeriods()
	   << "\t% Number of periods" << endl;
	os << theGuidedRoute->infoPeriodLength()
	   << "\t% Seconds per period" << endl;
	if (!theEngine->isRectangleFormat()) {
	  os << "% Code Type Length {" << endl
		 << "%   {Count-1 Time-1} ... {Count-N Time-N}" << endl
		 << "% }" << endl << endl;
	}
  }
  for (register int i = 0; i < nSegments(); i ++) {
	tsSegment(i)->printTravelTimes(os);
  }

  os.close();
}


void TS_Network::recordLinkTravelTimeOfActiveVehicle()
{
  register int i;
  TS_Vehicle *pv;
  for (i = 0; i < nSegments(); i ++) {
	pv = tsSegment(i)->firstVehicle();
	while (pv) {
	  pv->link()->recordExpectedTravelTime(pv);
	  pv = pv->macroTrailing();
	}
  }
  for (i = 0; i < nLinks(); i ++) {
	pv = tsLink(i)->queueHead();
	while (pv) {
	  pv->link()->recordExpectedTravelTime(pv);
	  pv = pv->trailing();
	}
  }

  // Section commented out Joseph Scariza 11/6/01
  // for (i = 0; i < nLinks(); i ++) {
  // 	pv = tsLink(i)->queueHead();
  //	while (pv) {
  //	  pv->link()->recordExpectedTravelTime(pv);
  //	  pv = pv->trailing();
  //	}
  //}
}

// Dump vehicles currently in the network in to a file

int
TS_Network::dumpNetworkState(const char *filename)
{
  ofstream os(filename);
  if (!os.good()) {
	cerr << "Error:: Failed doing a state dump to file <"
		 << filename << ">." << endl;
	return 1;
  }
  register int i;
  TS_Vehicle *pv;

  os << "/*" << endl
	 << " * A snap shot of the network state in MITSIM" << endl
	 << " * Generated by " << UserName() << endl
	 << " * at " << TimeTag() << endl
	 << " *" << endl
	 << " * {" << endl
	 << " *   Code Type OriIndex DesIndex DepTime Mileage (cont)" << endl
	 << " *   Path PathIndex TimeEntersLink LaneIndex Dist" << endl
	 << " * }" << endl
	 << " */" << endl << endl;

  os << theSimulationClock->currentTime() << "\t# "
	 << theSimulationClock->currentStringTime() << endl;

  os << OPEN_TOKEN
	 << "\t# Vehicles in the network" << endl;
  int ni = 0;
  for (i = 0; i < nSegments(); i ++) {
	pv = tsSegment(i)->firstVehicle();
	while (pv) {
	  pv->dumpState(os);
	  ni ++;
	  pv = pv->macroTrailing();
	}
  }
  os << CLOSE_TOKEN << "\t# " << ni << endl;

  os << OPEN_TOKEN
	 << "\t# Vehicles in the pretrip queues" << endl;
  int nq = 0;
  for (i = 0; i < nLinks(); i ++) {
	pv = tsLink(i)->queueHead();
	while (pv) {
	  pv->dumpState(os);
	  nq ++;
	  pv = pv->trailing();
	}
  }
  os << CLOSE_TOKEN << "\t# " << nq << endl;
   
  os << endl
	 << "# " << (ni + nq) << " vehciles dumped" << endl;

  os.close();
  return 0;
}


void
TS_Network::dumpTrajectoryRecords()
{
  static char opened = 0;
  static ofstream os;

  static char topened = 0;
  static ofstream tos;
  int transit = 0;

  if(theEngine->chosenOutput(OUTPUT_TRANSIT_TRAJECTORY)) transit++;

  if (!opened) {
	const char *file =  ToolKit::outfile(theFileManager->trajectoryFile());
	os.open(file);
	if (!os.good()) {
	  cerr << "Error:: problem with output file <"
		   << file
		   << ">" << endl;
	  theException->exit();
	}
	if (!theEngine->chosenOutput(OUTPUT_SKIP_COMMENT)) {
	  os << "# Vehicle trajectory" << endl;
	  if (theEngine->chosenOutput(OUTPUT_RECT_TEXT)) {
		os << "# Time ID Segment Lane Position Speed Acceleration Vehicle Type" << endl;
	  } else {
		os << "# Time {" << endl
		   << "#   ID Segment Lane Position Speed Acceleration Vehicle Type" << endl
		   << "# }" << endl;
	  }
	  os << endl;
	}
	opened = 1;
  }

  // If transit trajectory is chosen, also dump transit trajectory records

  if (transit > 0.5) {

    if (!topened) {
	const char *file =  ToolKit::outfile(theFileManager->transitTrajectoryFile());
	tos.open(file);
	if (!tos.good()) {
	  cerr << "Error:: problem with output file <"
		   << file
		   << ">" << endl;
	  theException->exit();
	}
	if (!theEngine->chosenOutput(OUTPUT_SKIP_COMMENT)) {
	  tos << "# Transit trajectory" << endl;
	  if (theEngine->chosenOutput(OUTPUT_RECT_TEXT)) {
		tos << "# Time Bus_ID Route_ID Trip_ID Dist_Traveled Load Sched_Dev Prevail_Headway" << endl;
	  } else {
		tos << "# Time {" << endl
		    << "#   Bus_ID Route_ID Trip_ID Dist_Traveled Load Sched_Dev Prevail_Headway" << endl
		    << "# }" << endl;
	  }
	  tos << endl;
	}
	topened = 1;
    }
  }

  TS_Vehicle *pv;

  if (!theEngine->chosenOutput(OUTPUT_RECT_TEXT)) {
	float t = theSimulationClock->currentTime();
	os << Fix(t, (float)0.1) << " {" << endl;

	if (transit > 0.5) {
  	  float tt = theSimulationClock->currentTime();
	  tos << Fix(tt, (float)0.1) << " {" << endl;
	}
  }

  for (register int i = 0; i < nSegments(); i ++) {
	pv = tsSegment(i)->firstVehicle();
	while (pv) {
	  pv->saveTrajectoryRecord(os);	

	  if (transit > 0.5 && pv->isBusWithAssignment()) {
	    pv->saveTransitTrajectoryRecord(tos);
	  }
	  pv = pv->macroTrailing();
	}
  }
  if (!theEngine->chosenOutput(OUTPUT_RECT_TEXT)) {
	os << "}" << endl;

	if (transit > 0.5) {
	  tos << "}" << endl;
	}
  }
}

void
TS_Network::collectMoeForUnfinishedTrips()
{
  TS_Vehicle *pv;
  for (register int i = 0; i < nSegments(); i ++) {
	pv = tsSegment(i)->firstVehicle();
	while (pv) {
	  theMoeCollector->recordUnFinished(pv);
	  pv = pv->macroTrailing();
	}
  }  
}

void
TS_Network::guidedVehiclesUpdatePaths()
{
  TS_Vehicle *pv;
  register int i;

  // Vehicles already in the network

  for (i = 0; i < nSegments(); i ++) {
	pv = tsSegment(i)->firstVehicle();
	while (pv) {
	  if (pv->isGuided()) {
		pv->changeRoute();
	  }
	  pv = pv->macroTrailing();
	}
  }

  // Vehicles waiting for entering the network
   
  for (i = 0; i < nLinks(); i ++) {
	pv = tsLink(i)->queueHead();
	while (pv) {
	  if (pv->isGuided()) {
		pv->changeRoute();
	  }
	  pv = pv->trailing();
	}
  }
}


double
TS_Network::loadInitialState(const char *name)
{
  if (!ToolKit::isValidFilename(name)) {
	cout << "No initial state. " << endl
		 << "Simulation start from empty network." << endl;
	return theSimulationClock->currentTime();
  }

  Reader is(name);

  TS_Vehicle *pv;
  int tnum = 0, inum = 0, qnum = 0;

  is.eatwhite();

  // Start time

  double start;
  is >> start;

  // Vehicles in the network

  if (!is.findToken(OPEN_TOKEN)) theException->exit();
  for (is.eatwhite();
	   is.peek() != CLOSE_TOKEN;
	   is.eatwhite()) {
	pv = theVehicleList->recycle(); // create a new vehicle
	if (pv->load(is, 0) != 0) {
	  theVehicleList->recycle(pv); // put it back
	} else {
	  inum ++;
	}
	tnum ++;
  }
  if (!is.findToken(CLOSE_TOKEN)) theException->exit();

  // Vehicles in pretrip queue

  if (!is.findToken(OPEN_TOKEN)) theException->exit();
  for (is.eatwhite();
	   is.peek() != CLOSE_TOKEN;
	   is.eatwhite()) {
	pv = theVehicleList->recycle(); // create a new vehicle
	if (pv->load(is, 1) != 0) {
	  theVehicleList->recycle(pv); // put it back
	} else {
	  qnum ++;
	}
	tnum ++;
  }
  if (!is.findToken(CLOSE_TOKEN)) theException->exit();

  is.close();

  if (ToolKit::verbose()) {
	cout << tnum << " vehicles parsed, "
		 << inum << " loaded, and "
		 << qnum << " queued." << endl;
  }

  // In the simlab, we do not check time becuase the currentTime will
  // be set to the time it read here.

  if (!AproxEqual(start, theSimulationClock->currentTime())) {
	cerr << "Warning:: State dumped at "
		 << theSimulationClock->convertTime(start)
		 << " loaded at "
		 << theSimulationClock->currentStringTime() << endl;
  }

  return start;
}


#ifdef INTERNAL_GUI

void
TS_Network::drawNetwork(DRN_DrawingArea * area)
{
  DrawableRoadNetwork::drawNetwork(area);

  if (area->state(DRN_DrawingArea::MOUSE_DOWN)) return;

  drawVehicles(area);
}


// This function is called when view has changed. It clear onScreen_
// bit in each vehicle in the previously visible segments.

void
TS_Network::unsetDrawingStates(DRN_DrawingArea *area)
{
  DrawableRoadNetwork::unsetDrawingStates(area);
  TS_Segment *ps;
  TS_Vehicle *pv;
  for (int i = 0; i < nSegments(); i++) {
	ps = tsSegment(i);
	if (!ps->isVisible()) continue;
	pv = ps->firstVehicle();
	while (pv) {
	  ((TS_DrawableVehicle *)pv)->resetDrawing();
	  pv = pv->macroTrailing();
	}
  }
  area->unsetState(DRN_DrawingArea::VEHICLE_ON);
}


void
TS_Network::drawVehicles(DRN_DrawingArea * area)
{
  if (area->state(DRN_DrawingArea::VIEW_NONE)) return;

  if (!area->isVehicleDrawable() ||
	  !theSymbols()->vehicleBorderCode().value()) {
	return;
  }

  TS_DrawableVehicle *sv = TS_TraceDialog::the()->vehicle();
  if (sv) {
	TS_Lane *pl = (TS_Lane *)sv->lane();
	WcsPoint p(pl->centerPoint(sv->position()));
	area->keepInScope(p);
	TS_TraceDialog::the()->update(sv);
  }

  TS_Segment *ps;
  TS_Vehicle *pv;

  for (int i = 0; i < nSegments(); i++) {
	ps = tsSegment(i);
	if (!ps->isVisible()) continue;
	pv = ps->firstVehicle();
	while (pv) {
	  ((TS_DrawableVehicle *)pv)->draw(area);
	  pv = pv->macroTrailing();
	}
  }
  area->setState(DRN_DrawingArea::VEHICLE_ON);
}


void TS_Network::queryVehicle(const XmwPoint& point)
{
  WcsPoint pressed = theDrawingArea->window2World(point);
  TS_Segment *ps = (TS_Segment *) nearestSegment(pressed);
  if (ps) {
	ps->queryVehicle(pressed);
  }
}

void TS_Network::traceVehicle(const XmwPoint& point)
{
  WcsPoint pressed = theDrawingArea->window2World(point);
  TS_Segment *ps = (TS_Segment *) nearestSegment(pressed);
  if (ps) {
	ps->traceVehicle(pressed);
  }
}

void TS_Network::traceVehicle()
{
  TS_DrawableVehicle *pv = TS_TraceDialog::the()->vehicle();
  if (pv) {
	TS_TraceDialog::the()->post_info(pv);
  }
}

void TS_Network::setIncident(const XmwPoint& point)
{
  WcsPoint p = theDrawingArea->window2World(point);

  TS_Incident *inc = NULL;

  // Find the incident at the cursor point, if any

  const float epsilon = LANE_WIDTH * 2.0;
  float dis, mindis = FLT_INF;
  list<TS_Incident*> &lst = tsNetwork()->incidents();
  list<TS_Incident*>::iterator i;
  for (i = lst.begin(); i != lst.end(); i ++) {
	dis = p.distance((*i)->center());
	if (dis < mindis) {
	  inc = *i;
	  mindis = dis;
	}
  }

  // Create or change an incident

  TS_Segment *ps = NULL;
  if (inc && mindis < epsilon) {
	editIncident(inc);
  } else if (ps = (TS_Segment*)nearestSegment(p)) {
	dis = p.distance(ps->centerPoint(1.0)); // distance from up end
	addIncident(ps, dis/ps->length());
  } else {
	const char *msg = XmtLocalize2(theInterface->widget(),
								   "Bad incident position.", "prompt", "badIncidentPosition");
	theInterface->msgSet(msg);
  }
}

void TS_Network::editIncident(TS_Incident *p)
{
  RN_CtrlStation *s = p->station();
  RN_Segment* ps = s->segment();

  list<TS_Incident*> &lst = tsNetwork()->incidents();
  list<TS_Incident*>::iterator c;
  for (c = lst.begin(); c != lst.end(); c ++) {
	if ((*c) == s->signal(0)) {
	  TS_IncidentDialog::createAndPost(*c);
	}
  }
}

void TS_Network::addIncident(TS_Segment *ps, float pos)
{
  RN_CtrlStation* s;
  s = newCtrlStation();
  s->initWithoutInsert(CTRL_INCIDENT, theParameter->visibility(),
					   ps->code(), pos);
  TS_Incident *inc = new TS_Incident[ps->nLanes()];
  for (int i = 0; i <  ps->nLanes(); i ++) {
	inc[i].set(s, ps->lane(i));
  }
  TS_IncidentDialog::createAndPost(inc, s);
}


#endif



//IEM(May2) Fill sensorsOfType_ with sensors sorted by intervalType
// NOTE: depends on master file being already read

void TS_Network::sortSensors()
{
  for (int typeCounter = 0; typeCounter < theEngine->nSurvPointStepSizes(); typeCounter++) {
    vector <RN_Sensor *> newSensorVector;
    for (int sCounter = 0; sCounter < nSensors(); sCounter++) {
      if (sensor(sCounter)->intervalType() == typeCounter) {
        newSensorVector.push_back(sensor(sCounter)); //Add this sensor to the appropriate set
      }
    }

    sensorsOfType_.push_back(newSensorVector);
    TS_Sensor::resetTimes_.push_back(0); //IEM(Jun15) Initialize resetTimes_ vector to correct # of 0s.

  }
}
