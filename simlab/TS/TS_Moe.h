//-*-c++-*------------------------------------------------------------
// NAME: This generates the moe needed in my thesis
// AUTH: Qi Yang
// FILE: TS_Moe.h
// DATE: Fri Jun 14 11:57:14 1996
//--------------------------------------------------------------------

#ifndef TS_MOE_HEADER
#define TS_MOE_HEADER

class TS_Vehicle;

class TS_Moe
{
   public:

      TS_Moe();
      ~TS_Moe() { }

      void reset();

      void record(TS_Vehicle *);

      // total

      int count() { return count_; }

      // average

      double travelTime();
      double speed();
      double vkmt();

   protected:

      // Variables aggregated by entry time

      int count_;		// number of vehicles entered
      double travelTimeSum_;	// sum travel time
      double speedSum_;		// sum of speed
      double vkmtSum_;		// total vehicle miles traveled
};

#endif
