//-*-c++-*------------------------------------------------------------
// NAME: This generates the moe needed in my thesis
// AUTH: Qi Yang
// FILE: TS_MoeCollector.h
// DATE: Fri Jun 14 13:22:05 1996
//--------------------------------------------------------------------

#ifndef TS_MOECOLLECTOR_HEADER
#define TS_MOECOLLECTOR_HEADER

#include <stdio.h>
#include <vector>

#include <Templates/STL_Configure.h>
#include <GRN/OD_Pair.h>

#include "TS_Moe.h"

class TS_Vehicle;

class TS_MoeCollector
{
   public:

      TS_MoeCollector();
      ~TS_MoeCollector();

      void create(int step = 600);
      void remove();
      int isInitialized() { return (nIntervals_ > 0); }

      int choosePairs(GenericVariable &gv);
      void findSelectedPairs();

      bool isPairSelected(OD_Pair *);

      void recordFinished(TS_Vehicle *pv);
      void recordUnFinished(TS_Vehicle *pv);
      void output(const char *filename);
      void outputCounts(FILE *);
      void outputTravelTimes(FILE *);
      void outputSpeeds(FILE *);
      void outputVKMT(FILE *);

      int printSelectedPairs(FILE *fpo);
      void printSelectedPairs(ostream& os = cout);
      
	  int stepSize() { return stepSize_; }
	  void stepSize(int s) { stepSize_ = s; }

   protected:

      int stepSize_;
      int nIntervals_;

	  unsigned int nComplete_;
	  unsigned int nInComplete_;

      // The following 3D arrays follow this format:
      // [i][j][k] i: 0=arrived 1=not arrived
      //           j: 0=unguided 1=guided
      //           k: interval index

      // Vehicle arrived in each intervals.

      int * arrivals_[2][2];

      // For vehicles departed in each intervals

      TS_Moe * finished_[2][2];	

      // For a set selected OD pairs
      
      OdPairSetType selectedPairs_;

      TS_Moe * chosenPair_[2][2];

      vector<int ALLOCATOR> tmp;
};

extern TS_MoeCollector *theMoeCollector;

#endif
