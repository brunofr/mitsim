//-*-c++-*------------------------------------------------------------
// NAME: Microscopic Traffic Simulation
// AUTH: Qi Yang
// FILE: TS_Parameter.h
// DATE: Mon Feb 12 10:29:05 1996
//--------------------------------------------------------------------

#ifndef TS_PARAMETER_HEADER
#define TS_PARAMETER_HEADER

#include <GRN/Parameter.h>
#include <Tools/Scaler.h>
#ifdef INTERNAL_GUI
#include <Xmw/XmwColor.h>
#endif


class TS_VehicleLib;
class TS_BusLib;
class Random;

class TS_Parameter : public Parameter
{
  friend class TS_Vehicle;

public:

  TS_Parameter();
  ~TS_Parameter() { }
    
  void load();

  // Update step sizes

 float* updateStepSizeParams(int type);
  float constantStateTime() { return constantStateTime_; }

  // Car-following parameters
  float minResponseDistance() {
	return minResponseDistance_;
  }
  float cfLower() { return cfLower_; }
  float cfUpper() { return cfUpper_; }

  float cfAlphaAcc() { return accParams_[0][0]; }
  float cfBetaAcc() { return accParams_[0][1]; } 
  float cfGammaAcc() { return accParams_[0][2]; }
  float cfRhoAcc() { return accParams_[0][3]; }
  float cfLambdaAcc() { return accParams_[0][4]; }

  float cfAlphaDec() { return accParams_[1][0]; }
  float cfBetaDec() { return accParams_[1][1]; } 
  float cfGammaDec() { return accParams_[1][2]; }
  float cfRhoDec() { return accParams_[1][3]; }
  float cfLambdaDec() { return accParams_[1][4]; }

#ifdef USE_IS_DELAY
  float cfThetaAcc() { return accParams_[0][5]; } // time-dependent influence ratios due to intersection delay
  float cfDeltaAcc() { return accParams_[0][6]; } // time-dependent fluctuation ratios for the random term due to intersection delay
  float cfThetaDec() { return accParams_[1][5]; } // time-dependent influence ratios due to intersection delay
  float cfDeltaDec() { return accParams_[1][6];}  // time-dependent fluctuation ratios for the random term due to intersection delay
#endif

  float cfAlphaAccWZ() { return accParams_[2][0]; }//cfc 19Dec
  float cfBetaAccWZ() { return accParams_[2][1]; } 
  float cfGammaAccWZ() { return accParams_[2][2]; }
  float cfRhoAccWZ() { return accParams_[2][3]; }
  float cfLambdaAccWZ() { return accParams_[2][4]; }

  float cfAlphaDecWZ() { return accParams_[3][0]; }
  float cfBetaDecWZ() { return accParams_[3][1]; } 
  float cfGammaDecWZ() { return accParams_[3][2]; }
  float cfRhoDecWZ() { return accParams_[3][3]; }
  float cfLambdaDecWZ() { return accParams_[3][4]; }

  float loadingHeadway() { return loadingParams_[0]; }
  float loadingQueueBeta() { return loadingParams_[1]; }
  float loadingSpeed() { return loadingParams_[2]; }

  float upMergingArea() { return mergingParams_[0]; }
  float dnMergingArea() { return mergingParams_[1]; }
  
  int nVehiclesAllowedInMergingArea() {
	return (int) mergingParams_[2];
  }
  float aggresiveRampMergeProb() {
	return mergingParams_[3];
  }

  // Lane-changing parameters

  float mlcProbCmf(float dis, int num, float congestion);

  // tomer - mlc distance for lookahead vehicles

  float mlcDistance(float n);

  float lcCriticalGap(
					  int type,		// 0-1:discretionary 2-3:mandatory
					  float dis,		// from critical pos
					  float spd,		// spd of the follower
					  float dv,		// spd difference from the leader
					  TS_Vehicle *pv);

#ifndef USE_KAZI_MODELS // cfc : was if USE_TOMER_MODELS


  // Varun - for 3rd level of gap acceptance decisions 
  int lcGapAcc(TS_Vehicle *pv);



  // tomer - overloading for lc model

  float lcCriticalGap(int type, float dv);
 // gunwoo - critical gap with aggresiveness

   float lcCriticalGap(int type, float dv, TS_Vehicle *pv);
   
// anita, gunwoo - critical gap  , overloading for single stage gap acceptance
   float lcCriticalGap(
   int type, 
   float dv,        // speed difference
   float dis,      //distance to MLC
   float acc,      //accelearation of lag
   float avgspd,   //average speed in lane 6
   float spd,     // subject speed
   TS_Vehicle *pv); //aggressiveness
   
// Anita - critical gap, overloading for forced, normal
   float lcCriticalGap(
   int merge_type,   //0-normal, 1- forced 2- courtesy
   int type,        
   float dv,        // speed difference
   float dis,      //distance to MLC
   float acc,      //accelearation of lag
   float avgspd,   //average speed in lane 6
   float spd,     // subject speed
   TS_Vehicle *pv); //aggressiveness

  float lcAnticipatedGap(//cfc 06
   float dv,        // speed difference
   float density,      
   float dis,      
   float acc,        
   TS_Vehicle *pv); //aggressiveness
   
#endif

  float lcMinGap(int type);

  // Yielding probability

  float lcYieldingProb(int yielding);

  // returns probabilty to nose in

  float lcNosingConstStateTime() { return nosingParams_[0]; }
  float lcNosingProbCmf(float dis, int nlanes, float tm);
  float lcMaxNosingProbACmf() { return nosingParams_[4]; }
  float lcMaxNosingProbBCmf() { return nosingParams_[5]; }
  float lcYieldingMaxTime() { return nosingParams_[6]; }
  float lcMaxStuckTime() { return nosingParams_[7]; }
  float lcMaxNosingDis() { return nosingParams_[8]; }
  float lcMinNosingDis() { return nosingParams_[9]; }

  float* lcKaziNosingParams() { return kaziNosingParams_; }
  float* lcAnitaNosingParams() { return anitaNosingParams_; } //Anita's initiate force merging

  // Discreationary lane change params

  float* dlcUnsatisfactoryParamsKazi() { return dlcKazi_ ; }
  float* dlcUtilParamsKazi() { return dlcKazi_ + 7 ; }

  float laneSpeedStepSize() { return dlcParams_[0]; }
  float lcNoDlcToDlcProb() { return dlcParams_[1]; }
  float lcDlcToDlcProb() { return dlcParams_[2]; }
  float lcImpLower() { return dlcParams_[3]; } 
  float lcImpUpper() { return dlcParams_[4]; }
  float lcSlowerLeadThreshold() { return dlcParams_[5]; }
  float lcSpeedThreshold() { return dlcParams_[6]; }
  float lcAccThreshold() { return dlcParams_[7]; }
  float lcSpdLookAheadDistance() { return dlcParams_[8]; }
  float dlcMinTimeInLaneSameDir() { return dlcParams_[9]; }
  float dlcMinTimeInLaneDiffDir() { return dlcParams_[10]; }
  float lcMaxLaneSpeedDiff() { return dlcParams_[11]; }

  float lcLowerBound() { return mlcParams_[0]; }
  float lcUpperBound() { return mlcParams_[1]; }
  float mlcMinTimeInLane() { return mlcParams_[4]; }

  unsigned int mlcNumberToYield();
      
  float lcEntryLaneCheckRightnessProb() {
	return lcEntryLaneCheckRightnessProb_;
  }

  // TS_Parameters for stop-and-going traffic

  float startupDelay(int pos);

  inline float familiarity() { return familiarity_; }

  // Compliance rate to various rules and signs

  inline float tsCompRate() { return complianceRates_[0]; }
  inline float psCompRate() { return complianceRates_[1]; }
  inline float rmCompRate() { return complianceRates_[2]; }
  inline float rLusCompRate() { return complianceRates_[3]; }
  inline float yLusCompRate() { return complianceRates_[4]; }
  inline float gLurCompRate() { return complianceRates_[5]; }
  inline float gLcrCompRate() { return complianceRates_[6]; }
  inline float typeMsgCompRate() { return complianceRates_[7]; }
  inline float pathMsgCompRate() { return complianceRates_[8]; }
  inline float guideCompRate() { return complianceRates_[9]; }

  inline float yellowStopHeadway() {
	return yellowStopHeadway_;
  }
  inline float maxTollBoothDelay() {
	return maxTollBoothDelay_;
  }

  // TS_Parameters for headway buffers

  float hBufferLower() { return hBufferLower_; }
  float hBufferUpper() { return hBufferUpper_; }

  // Limiting parameters

  float jamDensity() { return jamDensity_; }
  float minSpeed() { return minSpeed_; }

  // Rand variation of driver characteristics such as desired speed,
  // accelerations, and so on

  enum {
	MaxAccScale    = 0,
	MaxDecScale    = 1,
	NormalDecScale = 2,
	CFAccAddOn     = 3,
	CFDecAddOn     = 4,
	FFAccAddOn     = 5,
	SpdLmtAddOn    = 6,
	HUpper         = 7,
        AUpper         = 8,
	NumRndVars     = 9			// total number of items above
  } ;
  int nDriverGroups() { return nDriverGroups_; }

  float maxAccScale(int bin) {
	return rndParams_[bin * NumRndVars + MaxAccScale] ;
  }
  float maxDecScale(int bin) {
	return rndParams_[bin * NumRndVars + MaxDecScale] ;
  }
  float normalDecScale(int bin) {
	return rndParams_[bin * NumRndVars + NormalDecScale] ;
  }
  float cfAccAddOn(int bin) {
	return rndParams_[bin * NumRndVars + CFAccAddOn] ;
  }
  float cfDecAddOn(int bin) {
	return rndParams_[bin * NumRndVars + CFDecAddOn] ;
  }
  float ffAccAddOn(int bin) {
	return rndParams_[bin * NumRndVars + FFAccAddOn] ;
  }
  float spdLmtAddOn(int bin) {
	return rndParams_[bin * NumRndVars + SpdLmtAddOn] ;
  }
  float hUpper(int bin) {
	return rndParams_[bin * NumRndVars + HUpper] ;
  }
 float aUpper(int bin) {
	return rndParams_[bin * NumRndVars + AUpper] ;
 }//cfc 06
  int numRndVars() { return NumRndVars ; }

  float* ffAccParams() { return ffAccParams_; }

  float lcNosingProb(float dis, float lead_rel_spd, float gap,
					 int num) ;
  float lcNosingProb(float HVDummy, //heavy lag vehicle
                   TS_Vehicle *pv );//aggressiveness); //anita - probability to initiate forcing

  // TS_Parameters for each facility type and vehile class

  inline int nVehicleClasses() { return nVehicleClasses_; }
  inline int nBusClasses() { return nBusClasses_; }
  float heavyVehicleLength() { return heavyVehicleLength_; }

  inline int nGradeGroups() {
	return gradeScaler_.nIntervals();
  }
  inline Scaler& gradeScaler() { return gradeScaler_; }

  inline int nSpeedGroups() {
	return speedScaler_.nIntervals();
  }
  inline Scaler& speedScaler() { return speedScaler_; }

  inline float accConverting() { return accConverting_; }
  inline float speedConverting() { return speedConverting_; }
  inline float accScaler() { return accScaler_; }
  inline float decScaler() { return decScaler_; }
  inline float accGradeFactor() { return accGradeFactor_; }

  TS_VehicleLib* vehicleLib(int);
  inline float* vehicleClassCDF() {
	return vehicleClassCDF_;
  }

  TS_BusLib* busLib(int);
	
  inline float vehicleClassCDF(int i) {
	return vehicleClassCDF_[i];
  }

  void vehicleClassCDF(int i, float x);

  inline int maxNumOfLanes() { return maxNumOfLanes_; }
  float laneSpeedRatio(int n, int i);

  float detectedMinSpeed() { return detectedMinSpeed_; }

  // tomer - impatience factor for gap acceptance
  //
  //float impatienceFactor() { return impatienceFactor_ ;}
  //
  // **

#ifndef USE_KAZI_MODELS //cfc june 3 : in both tomer & cfc's model

  float* criticalGapParams() { return criticalGapParams_ ; }
  float* targetGapAccParams() { return targetGapAccParams_ ; } 
  float* targetGapParams() { return targetGapParams_ ; }


#endif


#ifdef USE_TOMER_MODELS //cfc : not in  target lane model


float* laneUtilityParams() { return laneUtilityParams_ ; }
 
#endif

#ifdef USE_CFC_MODELS // only in target lane model

float* targetLaneParams() { return targetLaneParams_ ; }
float* courtesyMergeParams() { return courtesyMergeParams_ ; }
float* arterialParams() { return arterialParams_ ; }// cfc Dec 06

#endif



#ifdef INTERNAL_GUI
  Pixel vehicleColors(int i) { return vehicleColors_[i]; }
#endif

  private:

  int parseVariable(GenericVariable &);

  int loadVehicleLoadingModel(GenericVariable &);
  int loadVehicleClassAttributes(GenericVariable &);
  int loadBusClassAttributes(GenericVariable &);
  int loadDriverGroups(GenericVariable &);
  int loadCFParameters(GenericVariable &gv);
  int loadMergingModel(GenericVariable &gv);
  int loadYieldingModel(GenericVariable &gv);
  int loadDLCModel(GenericVariable &gv);

  int loadKaziDLCModel(GenericVariable &gv);
  int loadFFAccParams(GenericVariable &gv) ;
  int loadMLCKaziNosingModel(GenericVariable &gv) ;
  int loadMLCAnitaNosingModel(GenericVariable &gv); //Anita

  int loadMLCNosingModel(GenericVariable &gv);
  int loadMLCProbabilityModel(GenericVariable &gv);
  int loadLCGapModels(GenericVariable &gv);
  int loadLaneSpeedRatios(GenericVariable &);
  int loadStartUpDelays(GenericVariable &);
  int loadMaximumAcceleration(GenericVariable &);
  int loadMaximumDeceleration(GenericVariable &);
  int loadNormalDeceleration(GenericVariable &);
  int loadLimitingSpeed(GenericVariable &);
  int loadUpdateStepSizeParams(GenericVariable &);
  int loadComplianceRates(GenericVariable &);
  int loadMLCYieldProbabilities(GenericVariable &);
  
  int loadTargetGapAccModel(GenericVariable &gv);
  int loadCriticalGapModel(GenericVariable &gv);
  int loadLaneUtilityModel(GenericVariable &gv);
  int loadTargetGapModel(GenericVariable &gv);
  int loadTargetLaneModel(GenericVariable &gv);//cfc june 3
 int loadCourtesyMergeModel(GenericVariable &gv);//cfc june 3
 int loadArterialModel(GenericVariable &gv);//cfc dec06

  protected:

  float *updateStepSizeParams_; // car-following, lane-change
  float constantStateTime_;	// seconds ahead for assuming const state

  // Car-following parameters

  float minResponseDistance_;
  float cfLower_;				// car-following lower bound
  float cfUpper_;				// car-following upper bound
  float *accParams_[4];			// row: 0=acc 1=dec

  //cfc : originally 2, now row 3 and 4 for Work Zone 
								// col: 0=alpha
								//      1=beta
								//      2=gamma
								//      3=rho
								//      4=lambda
								//      5=sigma (std dev)


  float *loadingParams_;

  // Yielding

  float *lcYieldingProb_;	// yielding probabilities

  // Merging

  float *mergingParams_;	// parameters in merging model

  // Lane-changing parameters

  float *lcGapModels_[4];

  int mlcYieldBins_;		// number of yield probabilities
  float *mlcYieldProbs_;	// probabilities to yield MLC vehicle

  float lcEntryLaneCheckRightnessProb_; // check rightness
  float *mlcParams_;	// params for mandatory lane change
  float *dlcParams_;	// params for discretionary lane change

  float* dlcKazi_;		// params in Kazi's DLC model
  float* ffAccParams_ ;
  float* kaziNosingParams_ ;
  float* anitaNosingParams_; //anita - initiate forcing

  float* targetGapAccParams_ ; 
  float* criticalGapParams_ ; 
  float* laneUtilityParams_ ; 
  float* targetGapParams_ ; 
  float* targetLaneParams_ ; //cfc june3
  float* courtesyMergeParams_;//cfc 06
  float* arterialParams_;//cfc dec 06

  // no positive lag gap needed with prob given by nosing model
  float *nosingParams_;	// nosing model

  // Parameters for stop-and-going traffic

  float sDelayMax_;		// max start-up delay
  int sDelayNum_;		// number of delay entries
  float *sDelayMu_;		// mean of start-up delays

#ifdef USE_IS_DELAY
  float sDelayTheta_; // time-dependent influence ratios due to intersection delay
#endif

  // Parameters for responding traffic ocntrols

  float familiarity_;	// familiarity with network
  float *complianceRates_;	// to various rules and signs
  float yellowStopHeadway_; // stop if time is greater than
  float maxTollBoothDelay_; // upbound of delay at toll booth

  // Parameters for headway buffers

  float hBufferLower_;	// lower bound for headway buffer
  float hBufferUpper_;	// upper bound for headway buffer

  // Limiting parameters

  float jamDensity_;	// jam density (v/km)
  float minSpeed_;		// minimum speed (m/sec)

  // Driver behavior parameters

  int nDriverGroups_;			// number of driver groups
  float *rndParams_;			// value for each percentile and variable

  // Parameters for each facility type and vehile class

  int nVehicleClasses_;	// number of vehicle types
  int nBusClasses_;	// number of vehicle types
  float heavyVehicleLength_;

  // Parameters for acceleration and deceleration tables

  float accGradeFactor_;	// = 0.01 * Gravity/Grade
  float accScaler_;
  float decScaler_;
  float accConverting_;	// acceleration (1=meter/sec2)
  float speedConverting_;	// speed (1=meter/sec)
  Scaler gradeScaler_;
  Scaler speedScaler_;

  TS_VehicleLib *vehicleLib_; // TS_VehicleClasses
  TS_BusLib *busLib_; // TS_BusClasses
  float *vehicleClassCDF_;	// accumulated percentage

  int maxNumOfLanes_;	// max num of lane per segment
  float *laneSpeedRatio_;	// mean lane speed ratio
  float detectedMinSpeed_;	// avoid divided by zero

  // float impatienceFactor_;

#ifdef INTERNAL_GUI
  Pixel *vehicleColors_;
#endif
  };

  extern TS_Parameter * theParameter;
  extern Random* theRandomizer;

#endif


