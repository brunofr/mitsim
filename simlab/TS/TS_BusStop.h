//-*-c++-*------------------------------------------------------------
// 
// margaret:  Bus Stop file based on Toll Booth logic
//
// NAME: Microscopic traffic simulator
// AUTH: Qi Yang
// FILE: TS_BusStop.h
// DATE: Fri Aug 30 22:12:28 1996
//--------------------------------------------------------------------

#ifndef TS_BUSSTOP_HEADER
#define TS_BUSSTOP_HEADER

#include <vector>

class TS_Vehicle;

#include <GRN/RN_BusStop.h>
#include <GRN/BusAssignment.h>
#include "TS_Signal.h"

class TS_BusStop : public RN_BusStop, public TS_Signal
{
   public:

      TS_BusStop();
      ~TS_BusStop() { }

      float delay(TS_Vehicle *);
      float calcDelay(TS_Vehicle *, BusAssignment *);
      float acceleration(TS_Vehicle *, float);
      float squeeze(TS_Vehicle *, float);
      int scheduleArrivals(int rc, float sa);

      void logBusArrival(TS_Vehicle* pv, int rcode, float dt, double hw, double sa);
      void resetPassengerData(int c, int arrive, int alight, int left);
      void updateLastBusArrival(int c, int cycle, int interval, float t, float wt);

      void writeArrivalRecord(int rid);	
      void saveArrivalRecord(ofstream &os, int id);

  /* Dan: the following functions return the given value
     corresponding to route id rc.
  */
      int lastBusID(int rc);
      double lastArrivalTime(int rc);
      double lastSchedArrival(int rc);
      double lastHeadway(int rc);
      float lastBusDwellTime(int rc);
      float arrivalRate(int rc);
      float alightingPct(int rc);
      int passengersArrived(int rc);
      int passengersAlighted(int rc);
      int passengersLeft(int rc);
      int numBusArrivals(int rc);
      float startTime(int rc);
      float intervalA(int rc);
      float intervalB(int rc);
      float intervalC(int rc);
      float peakRate(int rc);
      int lastBusCycle(int rc);
      int lastBusInterval(int rc);
      float lastBusTime(int rc);
      float lastWaitTime(int rc);

};

#endif
