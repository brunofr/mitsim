//-*-c++-*------------------------------------------------------------
// NAME: Microcscopic Traffic Simulator
// AUTH: Qi Yang
// FILE: TS_VehicleLib.C
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#include <new>
using namespace std;

#include <Tools/Math.h>

#include <Tools/ToolKit.h>
#include <Tools/Random.h>
#include <Tools/Scaler.h>
#include <Tools/GenericVariable.h>

#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>

#include "TS_VehicleLib.h"
#include "TS_Parameter.h"
#include "TS_Network.h"
#include "TS_Segment.h"
#include "TS_Lane.h"
#include "TS_Vehicle.h"

TS_VehicleLib::TS_VehicleLib()
   :	acc_(NULL),
	dec_(NULL),
	maxDec_(NULL),
	maxSpeed_(NULL)
{
}

TS_VehicleLib::~TS_VehicleLib()
{
   if (acc_) delete [] acc_;
   if (dec_) delete [] dec_;
   if (maxDec_) delete [] maxDec_;
   if(maxSpeed_) delete [] maxSpeed_;
}

Scaler&
TS_VehicleLib::gradeScaler()
{
   return theParameter->gradeScaler();
}

int
TS_VehicleLib::nGradeGroups()
{
   return theParameter->gradeScaler().nIntervals();
}


Scaler&
TS_VehicleLib::speedScaler()
{
   return theParameter->speedScaler();
}

int
TS_VehicleLib::nSpeedGroups()
{
   return theParameter->speedScaler().nIntervals();
}


/*
 * Calculate the maximum acceleration based on grade and speed.
 */

float
TS_VehicleLib::maxAcceleration(TS_Vehicle* pv)
{
   float g = pv->tsLane()->grade();

   // Convert speed value to a table index.

   int j = speedScaler().index(pv->currentSpeed());

   float acc = acc_[j] - theParameter->accGradeFactor() * g;
   acc *= theParameter->maxAccScale(pv->driverGroup.maxAccScale);

   return acc;
}


/*
 * Calculate maximum deceleration based on speed.
 */

float
TS_VehicleLib::maxDeceleration(TS_Vehicle* pv)
{
   int j = speedScaler().index(pv->currentSpeed());
   float dec = *(maxDec_ + j) ;
   dec *= theParameter->maxDecScale(pv->driverGroup.maxDecScale);
   return dec;
}


/*
 * Calculate normal deceleration based on speed.
 */

float
TS_VehicleLib::normalDeceleration(TS_Vehicle* pv)
{
   int j = speedScaler().index(pv->currentSpeed());
   float dec = *(dec_ + j) ;
   dec *= theParameter->normalDecScale(pv->driverGroup.normalDecScale);
   return dec;
}


/*
 * Calculate maximum speed based on grade.
 */

float
TS_VehicleLib::maxLaneSpeed(TS_Vehicle* pv)
{
   int j = gradeScaler().index(pv->lane()->grade());
   float speed1 = maxSpeed_[j];
   float speed2 = pv->lane()->freeSpeed();
   return Min(speed1, speed2);
}

float
TS_VehicleLib::maxSegmentSpeed(TS_Vehicle* pv)
{
   RN_Segment* ps = pv->segment();
   int j = gradeScaler().index(ps->grade());
   float speed1 = ps->freeSpeed();
   float speed2 = maxSpeed_[j];
   //   return Min(speed1, speed2);
   return speed2; // changed since freeflow speed was always limiting (Angus)
}


/*
 * Read vehicle information from the given file.
 */

void
TS_VehicleLib::loadAttributes(ArrayElement *data)
{
   static float accumulated = 0.0;
   static int type = 0;

   name_ = Copy((const char*)data[0]);
   length_ = data[1];
   width_ = data[2];
   percent_ = data[3];
   tollBoothDelayFactor_ = data[4];
   etcRate_ = data[5];
   overHeightRate_ = data[6];
   hovRate_ = data[7];

   accumulated += percent_;
   theParameter->vehicleClassCDF(type, accumulated);

   length_ *= theParameter->lengthFactor();
   width_ *= theParameter->lengthFactor();

   type ++;
}

void
TS_VehicleLib::print(ostream &os)
{
   os << name_ << endc 
      << (length_/theParameter->lengthFactor()) << endc
      << (width_/theParameter->lengthFactor()) << endc
      << percent_ << endc
      << tollBoothDelayFactor_ << endc
      << etcRate_ << endc
      << overHeightRate_ << endc
      << hovRate_ << endl;
}


/*
 * Read acceleration rates for each vehicle type.
 */

void
TS_VehicleLib::loadMaximumAcceleration(ArrayElement *data)
{
   int ns = nSpeedGroups();
   acc_ = new float[ns];

   int p = 0;
   for (int i = 0; i < ns; i ++) {
     acc_[i] = data[p];
     acc_[i] *= theParameter->accConverting()
       * theParameter->accScaler();
     p ++;
   }
}


/*
 * Read normal deceleration rate for each vehicle type.
 */

void
TS_VehicleLib::loadNormalDeceleration(ArrayElement *data)
{
   dec_ = new float[nSpeedGroups()];
   for (register int i = 0; i < nSpeedGroups(); i ++) {
      float dec = theParameter->accConverting() * ((float)data[i]);
      *(dec_ + i) = ((dec > 0) ? (-dec) : (dec))
	* theParameter->decScaler();
   }
}


/*
 * Read maximum deceleration rate for each vehicle type.
 */

void
TS_VehicleLib::loadMaximumDeceleration(ArrayElement *data)
{
   maxDec_ = new float[nSpeedGroups()];
   for (register int i = 0; i < nSpeedGroups(); i ++) {
      float dec = theParameter->accConverting() * ((float)data[i]);
      *(maxDec_ + i) = ((dec > 0) ? (-dec) : (dec))
	* theParameter->decScaler();
   }
}


/*
 * Read normal deceleration rate for each vehicle type.
 */

void
TS_VehicleLib::loadMaxSpeed(ArrayElement *data)
{
   maxSpeed_ = new float[nGradeGroups()];
   for (register int i = 0; i < nGradeGroups(); i ++) {
      float maxspeed = ((float)data[i]) * theParameter->speedConverting();
      *(maxSpeed_ + i) = maxspeed;
   }
}


/*
 * Calculate an impatient ratio for seeking a lane discretionary
 * lane changing. A vehicle will look a lane changing only if its
 * current speed is lower than impatient*desiredSpeed. We treat
 * all types of vehicels identically and assign each vehicle a
 * uniform distributed random number as its impatient ratio.
 */

float
TS_VehicleLib::impatient()
{
   TS_Parameter &p = *theParameter;
   return (theRandomizer->urandom(p.lcImpLower(), p.lcImpUpper()));
}

