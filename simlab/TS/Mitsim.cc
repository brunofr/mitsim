//-*-c++-*------------------------------------------------------------
// Mitsim.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <iostream>
using namespace std;

#include <Tools/ToolKit.h>

#include "TS_Engine.h"
#include "TS_Setup.h"
#include "TS_Version.h"
#include "TS_CmdArgsParser.h"

#ifdef INTERNAL_GUI
#include "TS_Interface.h"
#endif

int main( int argc, char **argv )
{
  ::Welcome(argv[0]);

  ToolKit::initialize();

  TS_Engine engine;

#ifdef INTERNAL_GUI
  theInterface = new TS_Interface(&argc, argv);
#endif

  theCloParser = new TS_CmdArgsParser;
  theCloParser->parse(&argc, argv);

  // Run the simulation.

  engine.run();

  engine.quit(STATE_DONE);

  return 0;
}

