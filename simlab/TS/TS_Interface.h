//-*-c++-*------------------------------------------------------------
// TS_Interface.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class definition for the class TS_Interface
//--------------------------------------------------------------------

#ifndef TS_INTERFACE_HEADER
#define TS_INTERFACE_HEADER

#include <DRN/DRN_Interface.h>

class TS_Menu;
class TS_Symbols;

class TS_Interface : public DRN_Interface
{
   public:

	  TS_Interface(int *argc, char **argv);
	  ~TS_Interface();

	  void create();			// virtual

	  XmwSymbols* createSymbols(); // virtual
	  TS_Symbols* symbols() { return (TS_Symbols*) symbols_; }

	  DRN_DrawingArea* drawingArea(Widget w); // virtual

	  XmwMenu *menu(Widget parent);	// virtual
	  TS_Menu *menu() { return menu_; }

	  XmwModeline* modeline(Widget parent);	// virtual
	  XmwModeline* modeline() { return modeline_; }

   private:

	  TS_Menu *menu_;
};

extern TS_Interface *theInterface;
inline TS_Symbols* theSymbols() {
   return theInterface->symbols();
}

#endif
