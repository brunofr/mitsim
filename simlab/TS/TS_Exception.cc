//-*-c++-*------------------------------------------------------------
// NAME: Exception handler
// AUTH: Qi Yang
// FILE: TS_Exception.C
// DATE: Sun Mar 17 14:36:22 1996
//--------------------------------------------------------------------


#include <GRN/RN_Route.h>

#include "TS_Exception.h"
#include "TS_Communicator.h"


void
TS_Exception::exit(int code)
{
   if (theCommunicator->isSimlabConnected()) {
      theCommunicator->simlab() << MSG_QUIT;
      theCommunicator->simlab().flush(SEND_IMMEDIATELY);
   }
   cleanup();
   IOService::exit();
   Exception::exit(code);
}

void
TS_Exception::done(int code)
{
   if (theCommunicator->isSimlabConnected()) {
      theCommunicator->simlab() << MSG_DONE;
      theCommunicator->simlab().flush(SEND_IMMEDIATELY);
   }
   cleanup();
   IOService::exit();
   Exception::done(code);
}


void
TS_Exception::cleanup()
{
   // Remove temporary files

   if (theGuidedRoute) {
     delete theGuidedRoute;
     theGuidedRoute = NULL;
   }
   if (theUnGuidedRoute) {
     delete theUnGuidedRoute;
     theUnGuidedRoute = NULL;
   }
}
