//-*-c++-*------------------------------------------------------------
// NAME: Read bus stop parameters from a file
// AUTH: Qi Yang & Daniel Morgan
// FILE: TS_BusStopPrmTable.h
// DATE: Fri Nov 30 16:25:20 2001
//--------------------------------------------------------------------

#ifndef TS_BUSSTOPPRMTABLE_HEADER
#define TS_BUSSTOPPRMTABLE_HEADER

#include <GRN/RN_BusStopPrmTable.h>

class TS_BusStopPrmTable : public RN_BusStopPrmTable
{
   public:

      TS_BusStopPrmTable() : RN_BusStopPrmTable() { }
      ~TS_BusStopPrmTable() { }

};

#endif
