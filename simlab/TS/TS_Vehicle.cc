//-*-c++-*------------------------------------------------------------
// TS_Vehicle.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <Tools/ToolKit.h>
#include <Tools/Random.h>
#include <Tools/SimulationClock.h>
#include <IO/Exception.h>

#include <Tools/Math.h>

#include <GRN/Constants.h>
#include <GRN/RN_SurvStation.h>
#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_Path.h>
#include <GRN/RN_DrawableLane.h>
#include <GRN/BusAssignmentTable.h>
#include <GRN/BusAssignment.h>
#include <GRN/RN_BusCommPrmTable.h>
#include <GRN/RN_BusRoute.h>
#include <GRN/RN_BusStop.h>

#include "TS_Communicator.h"

#include "TS_Node.h"
#include "TS_Link.h"
#include "TS_Segment.h"
#include "TS_Lane.h"
#include "TS_ODCell.h"
#include "TS_VehicleList.h"
#include "TS_VehicleLib.h"
#include "TS_Sensor.h"
#include "TS_Signal.h"
#include "TS_Parameter.h"
#include "TS_Engine.h"
#include "TS_Status.h"
#include "TS_FileManager.h"
#include "TS_MoeCollector.h"
#include "TS_Network.h"

#ifdef INTERNAL_GUI
#include <DRN/DRN_DrawingArea.h>
#include "TS_DrawableVehicle.h"
#else
#include "TS_Vehicle.h"
#endif

// MOE gatherer

#ifdef EXTERNAL_MOE
#include <MOE/MOE_TSSampler.h>
#endif

int TS_Vehicle::cycleFlags_ = 0;
float TS_Vehicle::dis2stop_ = FLT_INF;
float TS_Vehicle::vis_ = FLT_INF;

extern const char *LOG_FILE_HEADER_MSG;
extern const char *PATH_ERROR_MSG;

TS_Vehicle::TS_Vehicle()
  : RN_Vehicle(),no_conflict(0)
{
}

#define GET4BIT(f,i) (((f) >> (4 * (i))) & 0xF)
#define SET4BIT(f,i) (((f) & 0xF) << (4 * (i)))

void TS_Vehicle::setDriverGroup(unsigned int f)
{
  int b , n = theParameter->nDriverGroups() ;

  driverGroup.maxAccScale    = (b = GET4BIT(f,0)) < n ? b : n-1 ;
  driverGroup.maxDecScale    = (b = GET4BIT(f,1)) < n ? b : n-1 ;
  driverGroup.normalDecScale = (b = GET4BIT(f,2)) < n ? b : n-1 ;
  driverGroup.cfAccAddOn     = (b = GET4BIT(f,3)) < n ? b : n-1 ;
  driverGroup.cfDecAddOn     = (b = GET4BIT(f,4)) < n ? b : n-1 ;
  driverGroup.ffAccAddOn     = (b = GET4BIT(f,5)) < n ? b : n-1 ;
  driverGroup.spdLmtAddOn    = (b = GET4BIT(f,6)) < n ? b : n-1 ;
  driverGroup.hUpper         = (b = GET4BIT(f,7)) < n ? b : n-1 ;
  driverGroup.aUpper         = (b = GET4BIT(f,8)) < n ? b : n-1 ;// cfc 06
}

unsigned int TS_Vehicle::getDriverGroup()
{
  unsigned int f = 0 ;
  f += SET4BIT(driverGroup.maxAccScale   , 0) ;
  f += SET4BIT(driverGroup.maxDecScale   , 1) ;
  f += SET4BIT(driverGroup.normalDecScale, 2) ;
  f += SET4BIT(driverGroup.cfAccAddOn    , 3) ;
  f += SET4BIT(driverGroup.cfDecAddOn    , 4) ;
  f += SET4BIT(driverGroup.ffAccAddOn    , 5) ;
  f += SET4BIT(driverGroup.spdLmtAddOn   , 6) ;
  f += SET4BIT(driverGroup.hUpper        , 7) ;
  f += SET4BIT(driverGroup.aUpper        , 8) ;//cfc 06
  return (f) ;
}


// Calculate the step sizes for making car-following and lane change
// decisions

void
TS_Vehicle::calcUpdateStepSizes()
{
  float r = theRandomizer->nrandom(); // standard normal random number

  for (int i = 0; i < 4; i ++) {
	float *params = theParameter->updateStepSizeParams(i);
	float v = params[0] + r * params[1];
	if (v < params[2]) v = params[2];
	else if (v > params[3]) v = params[3];
	updateStepSize_[i] = (unsigned char) (10.0 * v);
  }
}


void
TS_Vehicle::calcNextStepSize()
{
  int i;
  if (accRate_ < -ACC_EPSILON) i = 0;
  else if (accRate_ > ACC_EPSILON) i = 1;
  else if (currentSpeed_ > SPEED_EPSILON) i = 2;
  else i = 3;
  nextStepSize_ = updateStepSize(i);
}

int TS_Vehicle::direction()
{
 return direction_;
}

float
TS_Vehicle::position() {
  return distance_ / lane_->length();
}


// tomer

float
TS_Vehicle::stoppingDistance(float s)
{
float d=-s*s/normalDeceleration();
return d;
}


float
TS_Vehicle::timeSinceTagged()
{
  return theSimulationClock->currentTime() - lcTimeTag_;
}


int
TS_Vehicle::destLane(TS_Vehicle* v) 
{
  if ( v==NULL ||
       v->nextLane_==NULL ) { // vehicle is lost (Angus)
    return (-1);
  }
  else
    { return v->nextLane_->code_;} 
}
/*
 *-------------------------------------------------------------------
 * Find the distance to a control device.
 * CAUTION: The device must be in the same link this vehicle is
 * in or the downstream link.
 *-------------------------------------------------------------------
 */

float
TS_Vehicle::distanceFromDevice(CtrlList device)
{
  float dis = distanceFromDownNode();
  RN_Link* plink = (*device)->link();
 
  if (plink == link()) {	/* same link */
	dis -=  (*device)->distance();
  } else {			/* different link */
	dis += plink->length() - (*device)->distance();
  }

  return (dis);
}

/*
 *-------------------------------------------------------------------
 * Find the distance to a surveillance device.
 * CAUTION: The device must be in the same link.
 *-------------------------------------------------------------------
 */

float
TS_Vehicle::distanceFromDevice(SurvList device)
{
  return (distanceFromDownNode() - (*device)->distance());
}


float
TS_Vehicle::probCmfToPmf(float cmf, float &prod_rej)
{
  float pmf = 1.0 - (1.0 - cmf) / prod_rej;
  if (pmf > 0.99999) {
	prod_rej = 1.0;
  } else {
	prod_rej *= 1.0 - pmf;
  }
  return pmf;
}


float TS_Vehicle::width()
{
  return (theParameter->vehicleLib(type())->width());
}

RN_Segment* TS_Vehicle::segment()
{
  return (lane_ ? lane_->segment() : (RN_Segment*) NULL);
}

RN_Link* TS_Vehicle::link()
{
  return (lane_ ? lane_->segment()->link() : (RN_Link*) NULL);
}


float
TS_Vehicle::tollBoothDelayFactor()
{
  int i = type();
  return (theParameter->vehicleLib(i)->tollBoothDelayFactor());
}

/*
 *--------------------------------------------------------------------
 * Returns the pointer to the vehicle ahead.
 *
 * CAUTION: It checks at most one segment ahead.
 *--------------------------------------------------------------------
 */

TS_Vehicle *
TS_Vehicle::vehicleAhead()
{
  if (leading_) {
	return leading_;
  } else if (nextLane_) {
	return nextLane_->lastVehicle();
  } else {
	return NULL;
  }
}


// Find the leading vehicle in the lanes connected to the nextLane_

TS_Vehicle*
TS_Vehicle::vehicleAheadInTypedLanes(int required_type)
{
  if (!nextLane_) return (NULL);
  
  int i, n = nextLane_->nUpLanes();

  TS_Vehicle *front = NULL, *pv;
  TS_Lane* olane;

  for (i = 0; i < n; i ++) {

	olane = (TS_Lane *)nextLane_->upLane(i);

	// Skip if type does not match or no vehicles

	if (required_type &&
		olane->linkType() != required_type) {
	  continue;
	} else if (olane == lane_) {
	  if (!front ||
		  leading_ &&
		  leading_->distance_ > front->distance_) {
		front = leading_;
	  }
	} else {
	  pv = olane->firstVehicle();
	  while (pv && pv->distance_ < distance_) {
		if (!front ||
			pv->distance_ > front->distance_) {
		  front = pv;
		}
		pv = pv->trailing_;
	  }
	}
  }
  return front;
}


TS_Vehicle *
TS_Vehicle::vehicleBehind()
{
  if (trailing_) {
	return (trailing_);
  }
  int i, n = lane_->nUpLanes();
  TS_Vehicle* pv, *closest = NULL;
  float mindis = FLT_INF;
  for (i = 0; i < n; i ++) {
	pv = lane_->upLane(i)->firstVehicle();
	if (pv && pv->distance_ < mindis) {
	  mindis = pv->distance_;
	  closest = pv;
	}
  }
  return closest;
}


TS_Vehicle*
TS_Vehicle::vehicleBehindInTypedLanes(int required_type)
{
  if (!nextLane_) return (NULL);
  
  int i, n = nextLane_->nUpLanes();

  TS_Vehicle *behind = NULL, *pv;
  TS_Lane* olane;

  for (i = 0; i < n; i ++) {

	olane = (TS_Lane *)nextLane_->upLane(i);

	// Skip if type does not match or no vehicles

	if (required_type &&
		olane->linkType() != required_type) {
	  continue;
	} else if (olane == lane_) {
	  if (!behind ||
		  trailing_ &&
		  trailing_->distance_ < behind->distance_) {
		behind = trailing_;
	  }
	} else {
	  pv = olane->firstVehicle();
	  while (pv && pv->distance_ < distance_) {
		pv = pv->trailing_;
	  }
	  if (!behind ||
		  pv && pv->distance_ < behind->distance_) {
		behind = pv;
	  }
	}
  }
  return (behind);
}


/*
 *--------------------------------------------------------------------
 * Find the distance from front vehicle.
 *
 * CAUTION: TS_Vehicles "front" and this vehicle may not be in the same
 * lane (could be in the left or right neighbor lane), but they have
 * to be in either the same segment or in a downstream NEIGHBOR
 * segment.
 *--------------------------------------------------------------------
 */

float
TS_Vehicle::gapDistance(TS_Vehicle* front)
{
  float headway;

  if (front)			/* vehicle ahead */
	{
      if (lane_->segment() == front->segment())
		{				/* same segment */
		  headway = distance_ - front->distance_ - front->length();
		} else
		  {				/* different segment */
			headway = distance_ + (front->lane_->length() -
								   front->distance_ -
								   front->length());
		  }
	} else			/* no vehicle ahead. */
	  {
		headway = FLT_INF;
	  }

  return (headway);
}

float
TS_Vehicle::gapDistance()
{
  float headway;
  TS_Vehicle *front;
  if (leading_) {
	headway = distance_ - (leading_->distance_ + leading_->length());
  } else if (nextLane_ && (front = nextLane_->lastVehicle())) {
	headway = distance_ + (front->lane_->length() -
						   (front->distance_ + front->length()));
  } else {
	headway = FLT_INF;
  }
  return (headway);
}


// This function returns a pointer to the leading vehicle in plane.
// It searches only the in the same segment of plane.  The position is
// based on front bumper.

TS_Vehicle*
TS_Vehicle::findFrontBumperLeaderInSameSegment(TS_Lane *plane)
{
  TS_Vehicle *front = macroLeading_;
  while (front != NULL && front->lane_ != plane) {
	front = front->macroLeading_;
  }
  return front;
}

// Returns the leading vehicle in plane or the connected dowstream
// lanes.  The position is based on front bumper.

TS_Vehicle*
TS_Vehicle::findFrontBumperLeader(TS_Lane *plane)
{
  TS_Vehicle *front = findFrontBumperLeaderInSameSegment(plane);
  if (front) return front;
  else return plane->lastInDnLanes();
}


// This function returns a pointer to the following vehicle in plane.
// It searches only the same segment of plane.  The position is based
// on front bumper.

TS_Vehicle*
TS_Vehicle::findFrontBumperFollowerInSameSegment(TS_Lane *plane)
{
  TS_Vehicle *back = macroTrailing_;
  while (back != NULL && back->lane_ != plane) {
	back = back->macroTrailing_;
  }
  return back;
}

// This function returns a pointer to the following vehicle in plane
// or the connected upstream lanes.  The position is based on front
// bumper.

TS_Vehicle*
TS_Vehicle::findFrontBumperFollower(TS_Lane *plane)
{
  TS_Vehicle *back = findFrontBumperFollowerInSameSegment(plane);
  if (back) return back;
  else return plane->firstFromUpLanes();
}


// Return the leading vehicle in plane or the connected downstream
// lanes.  The position is based on back bumper.

TS_Vehicle*
TS_Vehicle::findBackBumperLeader(TS_Lane *plane)
{
  TS_Vehicle *back = findBackBumperFollower(plane);
  TS_Vehicle *front;
  if (back) {
	if ((front = back->leading_) == NULL) {
	  front = plane->lastInDnLanes();
	}
  } else {
	if ((front = plane->lastVehicle()) == NULL) {
	  front = plane->lastInDnLanes();
	}
  }
  return front;
}

// Return the trailing vehicle in plane or the connected upstream
// lanes.  The position is based on back bumper.

TS_Vehicle*
TS_Vehicle::findBackBumperFollower(TS_Lane *plane)
{
  TS_Vehicle *back = macroTrailing_;
  float pos = distance_ + length();
  while (back != NULL &&
		 (back->lane_ != plane || back->distance_ <= pos)) {
	back = back->macroTrailing_;
  }
  if (back) return back;
  else return plane->firstFromUpLanes();
}


/*
 *--------------------------------------------------------------------
 * Print current information of a vehicle.
 *--------------------------------------------------------------------
 */

void
TS_Vehicle::watch(ostream &os)
{
  os << indent 
	 << "T=" << theSimulationClock->currentTime() << endc
	 << link()->name()
	 << " (" << link()->code()
	 << "," << segment()->localIndex()
	 << "," << lane()->localIndex()
	 << ")" << endc
	 << "I=" << code_ << endc
	 << "o=" << oriNode()->name() << "(" << oriNode()->code() << ")" << endc
	 << "d=" << desNode()->name() << "(" << desNode()->code() << ")" << endc
	 << "p=" << (distance_ / theParameter->lengthFactor()) << endc
	 << "v=" << (currentSpeed_ / theParameter->speedFactor()) << endc
	 << "a=" << (accRate_ / theParameter->accConverting()) << endc
	 << "u=" << status_ << endl;
}


/*
 *--------------------------------------------------------------------
 * Calculate new location and speed after an iteration based on its
 * current location, speed and acceleration. The vehicle will be
 * removed from the network if it arrives its destination. It may
 * also activates surv devices and updates its pointers to surv and
 * ctrl lists.
 *--------------------------------------------------------------------
 */

void
TS_Vehicle::move()
{

  int vid;
  vid=this->code();
  int lid;
  lid=lane_->code();
  
  int nlid;
   if (nextLane_){
 nlid=nextLane_->code();
   }
  float oldpos = distanceFromDownNode();
  float step = theSimulationClock->stepSize();
  cfTimer_ = (cfTimer_ > step) ? (cfTimer_ - step) : (0);

  if (currentSpeed_ < SPEED_EPSILON &&
	  accRate_ < ACC_EPSILON) {
	updateNextSurv(oldpos, currentSpeed_);
	return;					// did not move
  }

  float oldv = currentSpeed_;	// Velocity at the beginning
  float oldd = distance_;		// Position at the beginning

  // distance can be traveled in last time interval based on
  // the speed and the acceleration rate calculated earlier.

  float dx;

  // Maximum distance it can travel in last time interval

  float dv = accRate_ * step;
  if (dv > -currentSpeed_) {	// still moving at the end of the cycle
	dx = currentSpeed_ * step + 0.5 * dv * step;
  } else {						// stops before the cycle end
	dx = - 0.5 * currentSpeed_ * currentSpeed_ / accRate_;
  }

  // This tries to solve the "crashing" problem caused by
  // discretization of continuous time.

  TS_Vehicle *front = this->vehicleAhead();
  if (front && !flag(FLAG_MERGING)) {
	float gap = gapDistance(front);
	if (gap > 0.0) {
	  dx = Min(dx, gap);
	} else {
	  dx = 0.0;
	}
  }


 

  // actual acceleration rate applied in last time interval.

  accRate_ = 2.0 * (dx - oldv * step) / (step * step);

  // update position

  distance_ -= dx;

  // update speed

  currentSpeed_ += accRate_ * step;

  // Since this is a time based simulation, one step could results in
  // over accelerating or decelerating. To avoiding this problem,
  // accelerating/decelerating and speed is be adjusted.
   

  if (currentSpeed_ <= SPEED_EPSILON) {

	// No back up allowed for a decelerating vehicle.

	currentSpeed_ = 0.0;
	accRate_ = 0.0;

  } else if (currentSpeed_ >= desiredSpeed_ &&
			 accRate_ > ACC_EPSILON) {

	// Stop acceleration and traverse at desired speed.

	currentSpeed_ = desiredSpeed_;
	accRate_ = (currentSpeed_ - oldv) / step;
  }

  if (!AproxEqual(currentSpeed_, oldv)) {
	calcStateBasedVariables();
  }

  if (oldv > SPEED_EPSILON &&
	  currentSpeed_ < SPEED_EPSILON) {

	// This vehicle was moving in previous time interval but it is
	// stopped now

	countPositionInQueue();

  } else {

	positionInQueue_ = 0;		// not in queue
  }

  if (distance_ < oldd) {		// moved in this iteration

	// Invoke signal and event response logic.

	responseSignalsAndSigns();

	// Update the pointer to the next control device.

	this->updateNextCtrl();

	// Activate the surveillance device and update the pointer to
	// the next surveillance devices.

	this->updateNextSurv(oldpos, oldv);
	this->advanceInMacroList();
  }

  if (distance_ <= 0.0) {		// arrived at end of a segment

	tsSegment()->countVehiclesLeft();
	tsSegment()->recordTravelTime(this);

	int is_at_end = !segment()->downstream();

	if (is_at_end) {			// last segment in the link done
	  tsLink()->recordTravelTime(this);
	}

	if (nextLane_) {			// move to next segment

	  this->transposeToNextLane(oldd, oldv);

	} else if (is_at_end) {
	  if (link()->dnNode() == desNode()) {
          mileage_+= tsSegment()->length(); //cfc

		this->removeFromNetwork();

	  } else if (!nextLink_ ||
				 !theNetwork->isNeighbor(link(), nextLink_)) {
		goto error;
	  }
	}
  }

  return;

 error:			// We branch to here when error occurs

  static int error_quota = 100;

  if (!theStatus->nErrors()) {
	theStatus->osLogFile() << LOG_FILE_HEADER_MSG << endl;
  }
  if (error_quota > 0) {
	theStatus->osLogFile()
	  << code_ << "\t"
	  << theSimulationClock->currentTime() << "\t"
	  << oriNode()->code() << "-" << desNode()->code() << "\t" 
	  << lane()->code();
	if (nextLink_) {
	  theStatus->osLogFile()
		<< "\t" << nextLink_->code() << endl;
	} else {
	  theStatus->osLogFile() << endl;
	}
  } else if (error_quota == 0) {
	theStatus->osLogFile()
	  << endl << PATH_ERROR_MSG
	  << endl;
  }
  error_quota --;

  this->removeFromNetwork();
  theStatus->nErrors(1);
  theStatus->nNoPath(1);
}


/*
 *------------------------------------------------------------------
 * Remove this vehicle from the current lane.
 *------------------------------------------------------------------
 */

void
TS_Vehicle::remove()
{
  if (leading_) {
	leading_->trailing_ = trailing_;
  } else {			/* first vehicle in lane */
	lane_->firstVehicle_ = trailing_;
  }
	
  if (trailing_) {
	trailing_->leading_ = leading_;
  } else {			/* last vehicle in lane */
	lane_->lastVehicle_ = leading_;
  }

  /* Update vehicle counts in current lane */

  lane_->nVehicles_--;
}


/*
 *-------------------------------------------------------------------
 * Append a vehicle to the vehicle list in "plane"
 *-------------------------------------------------------------------
 */

void
TS_Vehicle::append(TS_Lane* plane)
{
  leading_ = plane->lastVehicle_;
  trailing_ = NULL;

  plane->lastVehicle_ = this;
  // this->set_no_conflict(0);

  if (leading_) {		/* there is vehicle ahead */
	leading_->trailing_ = this;
  } else {			/* the only one in nextLane */
	plane->firstVehicle_ = this;
  }
	
  /* Update vehicle counts in nextLane */

  plane->nVehicles_++;
	
  lane_ = plane;
}

/*
 *------------------------------------------------------------------
 * Remove this vehicle from the macro vehicle list in the current
 * segment
 *------------------------------------------------------------------
 */

void
TS_Vehicle::removeFromMacroList()
{
  TS_Segment * ps = (TS_Segment *)lane_->segment_;
  if (macroLeading_) {
	macroLeading_->macroTrailing_ = macroTrailing_;
  } else {			/* first vehicle in the segment */
	ps->firstVehicle_ = macroTrailing_;
  }
	
  if (macroTrailing_) {
	macroTrailing_->macroLeading_ = macroLeading_;
  } else {			/* last vehicle in the segment */
	ps->lastVehicle_ = macroLeading_;
  }
  ps->nVehicles_--;
}

/*
 *-------------------------------------------------------------------
 * Append a vehicle to the macro vehicle list in the segment "ps"
 *-------------------------------------------------------------------
 */

void
TS_Vehicle::appendToMacroList(TS_Segment* ps)
{
  macroLeading_ = ps->lastVehicle_;
  macroTrailing_ = NULL;

  ps->lastVehicle_ = this;

  if (macroLeading_) {		/* there is vehicle ahead */
	macroLeading_->macroTrailing_ = this;
  } else {			/* the only one in plink */
	ps->firstVehicle_ = this;
  }
  advanceInMacroList();

  /* Increase the number of vehicles in the contaners by 1 */
 
  ps->nVehicles_++;

}

/*
 *-------------------------------------------------------------------
 * Advance a vehicle to the position in macro vehicle list that
 * corresponding to its current distance. This function is invoked
 * whenever a vehicle is moved (including moved into a downstream
 * segment), so that the vehicles in macro vehicle list is always
 * sorted by their position.
 *-------------------------------------------------------------------
 */

void
TS_Vehicle::advanceInMacroList()
{
  /* (0) Check if this vehicle should be advanced in the list */

  if (!macroLeading_ ||
	  distance_ >= macroLeading_->distance_) {

	/* no need to advance this vehicle in the list */

	return;
  }


  /* (1) Find the vehicle's position in the list  */

  TS_Vehicle* front = macroLeading_;

  while (front && distance_ < front->distance_) {
	front = front->macroLeading_;
  }


  /* (2) Take this vehicle out from the list */

  TS_Segment* ps = (TS_Segment *) lane_->segment_;

  macroLeading_->macroTrailing_ = macroTrailing_;
	
  if (macroTrailing_) {
	macroTrailing_->macroLeading_ = macroLeading_;
  } else {			/* last vehicle in the segment */
	ps->lastVehicle_ = macroLeading_;
  }
   

  /* (3) Insert this vehicle after the front */

   // (3.1) Pointers with the leading vehicle

  macroLeading_ = front;
	
  if (macroLeading_) {
	macroTrailing_ = macroLeading_->macroTrailing_;
	macroLeading_->macroTrailing_ = this;
  } else {
	macroTrailing_ = ps->firstVehicle_;
	ps->firstVehicle_ = this;
  }

  // (3.2) Pointers with the trailing vehicle

  if (macroTrailing_) {
	macroTrailing_->macroLeading_ = this;
  } else {			/* this vehicle becomes the last one */
	ps->lastVehicle_ = this;
  }
}


/*
 *-------------------------------------------------------------------
 * Transpose a vehicle to a nextLane_.
 *-------------------------------------------------------------------
 */

void
TS_Vehicle::transposeToNextLane(float oldd, float oldv)
{
  TS_Link *pl;
  TS_Segment *ps = tsSegment();

  // finished current link

  int link_done = ps->downstream() == NULL;

  mileage_ += ps->length();

  if (link_done) {

    if (theBusAssignmentTable) {
      if (BusAssignment* ba = theBusAssignmentTable->findBusAssignment(code())) {
        if (link()->code() == ba->route()->lastRouteLink()->code()) {
          ba->updateCurrentTrip();
        }   /* Dan: if this vehicle is a bus with a bus assignment, and has reached the
             final link on it's trip (route), then update it's current trip (route) 
             to the next */
      }
    }

    if (theEngine->chosenOutput(OUTPUT_VEHICLE_PATH_RECORDS)) {
      writePathRecord(theFileManager->osPathRecord());
    }
  }

  // Remove it from macro vehicle list in current segment

  this->removeFromMacroList();

  // Remove from vehicle list in current lane.

  this->remove();
  this->set_no_conflict(0); // for intersection (Deepak)

  // Put the vehicle into downstream lane.
  int vID;
  int CID;
  int LID;

  vID=this->code();
  CID=lane_->code();

   if (nextLane_){
  LID=nextLane_->code();
   }
  this->append(nextLane_);
  
  CID=lane_->code();
   if (nextLane_){  
LID=nextLane_->code();
   }
  //  !! NOTE !! data member lane_ HAS BEEN UPDATED!!

  ps = tsSegment();			// new segment
  pl = tsLink();				// new link
  ps->countVehiclesEntered();

  // Update vehicle's variables.

  // calculation of travel time in the link and segment should be
  // called before this function is invoked.  See move() and
  // segment()->recordTravelTime().

  timeEntersSegment_ = theSimulationClock->currentTime();

  if (link_done) {		// now it is in a new link
      
	timeEntersLink_ = theSimulationClock->currentTime();

	// Update the nextLink to follow on its path.

	if (nextLink_) {		// not the last link on path
	  OnRouteChoosePath(pl->dnNode());

	  //tomer

	  setApproachingIS() ;
	 

	  // **
	}

	unsetStatus(STATUS_MANDATORY);
	unsetStatus(STATUS_ALREADY_PAID);
	unsetFlag(FLAG_NOSING | FLAG_YIELDING | FLAG_LC_FAILED|FLAG_COURTESY);// cfc 06
	unsetFlag(FLAG_VMS_LANE_USE_BITS);

	// Set pointers to the first control device in the new link

	nextCtrl_ = pl->ctrlList();

	// Set pointers surveillance devices.

	nextSurv_ = pl->survList();
  }
	
  unsetFlag(FLAG_STUCK_AT_END | FLAG_ESCAPE | FLAG_AVOID |
	    FLAG_NOSING_FEASIBLE);
  //cfc Feb 28

  this->assignNextLane();


this->setStatus();

  distance_ += ps->length();

  // put this vehicle into the macro vehicle list of the new segment.

  appendToMacroList(ps);

  calcDesiredSpeed();
  calcStateBasedVariables();

  // Since it just join in a new lane, it is better to to compute
  // acc/dec immediately.

  cfTimer_ = 0;

  // update the pointer to the next control device.

  this->updateNextCtrl();

  // activate the surveillance device and update the pointer to the
  // next surveillance devices.

  this->updateNextSurv(oldd + ps->length() + ps->distance(), oldv);

#ifdef INTERNAL_GUI
  if (theDrawingArea->isVehicleDrawable()) {
	((TS_DrawableVehicle*)this)->erase(theDrawingArea);
  }
#endif

  
}

/*
 *-------------------------------------------------------------------
 * Update the pointer to the next control devices.  The vehicle will
 * not take any action in this function but update its pointer to
 * the next control device.
 *
 * The function is called in TS_Vehicle::move() when a vehicle has
 * been moved to a new position.
 *-------------------------------------------------------------------
 */

void
TS_Vehicle::updateNextCtrl()
{
while (nextCtrl_ &&
	 distanceFromDownNode() <= (*nextCtrl_)->distance()) {

	/* This vehicle has crossed the control device. */

	nextCtrl_ = nextCtrl_->next();
	info_ = INT_INF; 
  }
}


/*
 *------------------------------------------------------------------
 * Activate surveillance device if the vehicle passed and/or
 * occupied the detection zone of the sensor. It also updates the
 * pointer to the next surveillance station.  This function is
 * called in TS_Vehicle::move() when a vehicle has been moved to a
 * new position.
 *------------------------------------------------------------------
 */

void
TS_Vehicle::updateNextSurv(float prevpos, float prevvel)
{
  /*
   * Occupance is calculated when the vehicle is present in
   * the detection zone of a sensor. Flow counts, instantaneous
   * speed, and other point data are calculate when the back
   * bumper crosses the down-edge of the detection zone.
    *
    * NOTE: We do not count a vehicle until its back-bumper
    * leaves the detection zone of the sensor. This is to
    * avoid double counting.
    */

   /*
    * Average acceleration (deceleratio) rate used in last step
    */

  float acc = (currentSpeed_ - prevvel) / theSimulationClock->stepSize();

  SurvList surv = nextSurv_;
  float vpos = distanceFromDownNode();
  float len = length();
  int k = lane_->localIndex();
	
  /*
	* Some data items such as occupancy are accumulated if the entire
    * or a part of the vehicle was/is in the detection zone.
	*/

  while (surv && vpos < (*surv)->zoneLength() + (*surv)->distance()) 
    {
      RN_Sensor* sensor = (*surv)->sensor(k);
      if (sensor != NULL) {
		if (!(sensor->station()->isForEqBus() && !isPriorityGiven(sensor->code()))) { 
	          sensor->calcActivatingData(this, prevpos, prevvel, acc);
		  sensor->setState(SENSOR_ACTIVATED);
	        } 
      }
      surv = surv->next();
    }
	
  /*
    * Other data items such as flow and headways are counted when
    * the back bumper of the vehicle crosses the down-edge of the
    * of detection zone. This loop also update the pointer to the
    * next sensor station.
    */

  while (nextSurv_ != NULL &&
		 vpos + len < (*nextSurv_)->distance()) {

	// This vehicle has left the detector.
		
	RN_Sensor* sensor = (*nextSurv_)->sensor(k);
	if (sensor != NULL) {
	  if (!( sensor->station()->isForEqBus() && !isPriorityGiven(sensor->code()))) { 
	    sensor->calcPassingData(this, prevpos, prevvel, acc);
	    sensor->setState(SENSOR_ACTIVATED);
	  
	    // Write output for assignment matrix (Angus)
	    if (theEngine->chosenOutput(OUTPUT_ASSIGNMENT_MATRIX))
	    {
	      ostream& os = theFileManager->osAssignmentMatrix();
	      os  << sensor->code() << endc;
	      writeAssignmentMatrix();
	    }
	  }

	}
	nextSurv_ = nextSurv_->next();
  }
}



// Returns the time a vehicle has spent in current segment

float
TS_Vehicle::timeInSegment()
{
  return (theSimulationClock->currentTime() - timeEntersSegment_);
}


// Returns the average speed in this segment

float
TS_Vehicle::speedInSegment()
{
  float t = timeInSegment();
  float v;
  if (t < 0.1) {
	v = currentSpeed_;
  } else {
	v = (segment()->length() - distance_) / t;
  }
  return v;
}



/*
 *--------------------------------------------------------------------
 * Report summary information for arrived vehicle.
 *--------------------------------------------------------------------
 */

void
TS_Vehicle::reportTripSummary(void)
{
  TS_Parameter& p = *theParameter;

#ifdef INTERNAL_MOE
  static int heading = 1;
  float t = theSimulationClock->currentTime() - departTime_;
  ostream& os = theFileManager->osVehicle();
  if (heading) {
	if (!theEngine->skipComment()) {
	  os << "# ID/Type/Ori/DepDes/ArrDes/DepTime/";
	  os << "ArrTime/Mileage/Speed" << endl;
	}
	heading = 0;
  }

  int actual_des_node = link()->dnNode()->code();

  os << code_ << endc
	 << type_ << endc
	 << oriNode()->code() << endc
	 << desNode()->code() << endc
	 << actual_des_node << endc
	 << Fix(departTime_, (float)1.0) << endc
	 << Fix(theSimulationClock->currentTime(), 1.0) << endc
	 << Fix(mileage_ / p.lengthFactor(), (float)1.0) << endc
	 << Fix((mileage_ / t) / p.speedFactor(), (float)0.1) << endl;
#endif
   
  //    if ( (theNetwork->checkODPair(oriNode(), actual_des_node)) &&
  // 	(theNetwork->travelTimeCollectionInterval(departTime_)) &&
  // 	(theNetwork->travelTimeVehTypes(type)) )

#ifdef EXTERNAL_MOE
  theMOESampler->writeVehTripMsg(
								 code_, 
								 type_,
								 oriNode()->code(),
								 desNode()->code(),
								 link()->dnNode()->code(),
								 departTime_,
								 theSimulationClock->currentTime(),
								 (mileage_ / t) / p.lengthFactor(),
								 1);
#endif

}


// Save trajectory record

void TS_Vehicle::saveTrajectoryRecord(ofstream &os)
{
  float x = distance()/theParameter->lengthFactor();
  float odometer = (mileage_ + segment()->length() - distance()) / 
    theParameter->lengthFactor(); // total distance traveled from origin (Angus)

  if (theEngine->chosenOutput(OUTPUT_RECT_TEXT)) {
    float t = theSimulationClock->currentTime();
    os << Fix(t, (float)0.1);
  }                                          // time
 
  // Dan - added changes for gingging CMI output
    os << endc << code_ << endc              // veh id
    << segment()->code() << endc             // seg id
    << lane()->code() << endc                // lane id
    << Fix(distance(), (float)0.1) << endc   // dist from US seg end
    << currentSpeed() << endc                // speed
    << accRate() << endc                     // acceleration
    << type() << endl;                       // veh type
}

// Save transit trajectory record

void TS_Vehicle::saveTransitTrajectoryRecord(ofstream &os)
{
  BusAssignment *ba = theBusAssignmentTable->findBusAssignment(code_);
  float x = distance()/theParameter->lengthFactor();
  float odometer = (mileage_ + segment()->length() - distance()) / 
    theParameter->lengthFactor(); // total distance traveled from origin (Angus)

  if (theEngine->chosenOutput(OUTPUT_RECT_TEXT)) {
    float t = theSimulationClock->currentTime();
    os << Fix(t, (float)0.1);
  }                                                     // time
    os << endc << code_ << endc                         // veh id
    << ba->route()->code() << endc                      // route id
    << ba->currentTrip()->code() << endc                // trip id
    << Fix(odometer, (float)0.1) << endc                // distance traveled (Angus)    
    << ba->load() << endc                               // load
    << ba->schedDeviation() << endc                     // schedule deviation
    << ba->prevailingHeadway() << endc                  // headway
    << endl;                                  
}

/* Angus:
 *--------------------------------------------------------------------
 * Write vehicle record output for assignment matrix.  Called when
 * vehicle leaves a sensor.
 *--------------------------------------------------------------------
 */

void
TS_Vehicle::writeAssignmentMatrix(void)
{ 
  ostream& os = theFileManager->osAssignmentMatrix();
  
  os << Fix(theSimulationClock->currentTime(), 1.0) << endc
     << this->code() << endc
     << oriNode()->code() << endc
     << desNode()->code() << endc
     << Fix(departTime_, (float)1.0)<< endc
     << endl;
}


/* tomer for LC:
 *--------------------------------------------------------------------
 * Write vehicle record output for assignment matrix.  Called when
 * vehicle leaves a sensor.
 *--------------------------------------------------------------------
 */



void
TS_Vehicle::writeLaneChanging(TS_Lane *plane)
{ 
  ostream& os = theFileManager->osLaneChanging();
  
  os << this->code() << endc 
     << oriNode()->code() << endc
     << desNode()->code() << endc
     << Fix(theSimulationClock->currentTime(), 1.0) << endc
     << departTime() << endc //gunwoo,anita
     << lane()->code() << endc
     << plane->code() << endc
     << dis2stop_<<endc //gunwoo, anita for the final
     << Fix(distance(), (float)1.0) <<endl;

}
  
 

/* Dan:
 *--------------------------------------------------------------------
 * The following functions are used for bus operations to check for
 * bus-specific lane-changing and car-following behaviors.
 *--------------------------------------------------------------------
 */

bool 
TS_Vehicle::isBus() 
{ 
  return (type() == 0x3); 
}

bool 
TS_Vehicle::isBusWithAssignment() 
{ 
  if (type() == 0x3 && theBusAssignmentTable) return true;
  else return false; 
}

bool 
TS_Vehicle::busStopIsOnRoute(int id) 
{ 
  if (theBusAssignmentTable->findBusAssignment(code())->
	currentTrip()->findBusStop(id)) return true;
  else return false;
}

bool 
TS_Vehicle::isPriorityGiven(int c) 
{ 
  if (!isBus()) return false;                   // not a bus: no priority

  // a bus with no bus assignments, or a bus with assignment and no
  // priority conditions defined: assume unconditional priority:

  else if (!isBusWithAssignment() ||
            (isBusWithAssignment() && !theBusCommPrmTable)) return true;

  // a bus with an assignment and with priority conditions defined:

  else {
    BusAssignment *ba = theBusAssignmentTable->findBusAssignment(code());
    int pr = ba->priorityRequest();

    if (pr != c) {

      // First instant to arrive at this sensor, request priority...

      ba->updatePriorityRequest(c); 
      int priority = 0;
      if(theBusCommPrmTable->conditionSatisfied(code())) priority++;
      ba->updatePriorityGranted(priority);
      writePriorityRecord(c, priority);
      return priority;
    }
    else return ba->priorityGranted(); // already requested priority at this sensor
                                       // return that result
  }
}

// Save priority event record

void 
TS_Vehicle::savePriorityRecord(ofstream &os, int c, int p)
{
  BusAssignment *ba = theBusAssignmentTable->findBusAssignment(code());

  if (theEngine->chosenOutput(OUTPUT_RECT_TEXT)) {
    float t = theSimulationClock->currentTime();
    os << Fix(t, (float)0.1);
  }           
  os << endc << c << endc               // sensor id
  << ba->route()->code() << endc        // route id
  << code_ << endc                      // bus id
  << ba->schedDeviation() << endc       // schedule deviation at last stop
  << ba->prevailingHeadway() << endc    // headway between this bus and the previous bus
  << ba->load() << endc                 // passenger load on bus
  << p << endc                          // whether or not priority is given
  << endl;      
}

void
TS_Vehicle::writePriorityRecord(int c, int p)
{
  static char opened = 0;
  static ofstream os;

  if (!opened) {
	const char *file =  ToolKit::outfile(theFileManager->signalPriorityFile());
	os.open(file);
	if (!os.good()) {
	  cerr << "Error:: problem with output file <"
		   << file
		   << ">" << endl;
	  theException->exit();
	}
	if (!theEngine->chosenOutput(OUTPUT_SKIP_COMMENT)) {
	  os << "# Signal priority events" << endl;
	  if (theEngine->chosenOutput(OUTPUT_RECT_TEXT)) {
		os << "# Time Sensor_ID Route_ID Bus_ID Sched_Dev Headway Load Priority (0=no, 1=yes) " << endl;
	  } else {
		os << "# Time {" << endl
		   << "#   Sensor_ID Route_ID Bus_ID Sched_Dev Headway Load Priority (0=no, 1=yes) " << endl
		   << "# }" << endl;
	  }
	  os << endl;
	}
	opened = 1;
  }

  if (!theEngine->chosenOutput(OUTPUT_RECT_TEXT)) {
	float t = theSimulationClock->currentTime();
	os << Fix(t, (float)0.1) << " {" << endl;
  }

  savePriorityRecord(os, c, p);

  if (!theEngine->chosenOutput(OUTPUT_RECT_TEXT)) {
	os << "}" << endl;
  }

//void
//  TS_Vehicle::gun(ofstream &os)      //gun
 
}
