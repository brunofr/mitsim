//-*-c++-*------------------------------------------------------------
// TS_VehicleLoader.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <Tools/ToolKit.h>
#include <Tools/Random.h>
#include <Tools/SimulationClock.h>
#include <IO/Exception.h>

#include <Tools/Math.h>
#include <Tools/Reader.h>

#include <GRN/Constants.h>
#include <GRN/RN_PathTable.h>
#include <GRN/BusAssignmentTable.h>
#include <GRN/RN_Route.h>

#include "TS_Communicator.h"

#include "TS_Node.h"
#include "TS_Link.h"
#include "TS_Segment.h"
#include "TS_Lane.h"
#include "TS_Network.h"
#include "TS_ODCell.h"
#include "TS_VehicleList.h"
#include "TS_VehicleLib.h"
#include "TS_BusLib.h"
#include "TS_Parameter.h"
#include "TS_Engine.h"
#include "TS_Status.h"
#include "TS_FileManager.h"
#include "TS_MoeCollector.h"
#ifdef INTERNAL_GUI
#include <DRN/DRN_DrawingArea.h>
#include "TS_DrawableVehicle.h"
#include "TS_TraceDialog.h"
#else
#include "TS_Vehicle.h"
#endif

using namespace std;

const char *LOG_FILE_HEADER_MSG =
"Trips not completed\nID\tTime\tOD\tLane\tNextLink";
const char *PATH_ERROR_MSG =
"Too many incomplete trips! I will stop reporting now rather\nthan crash your file system.";


// This function is called when the parser reads a vehicle from the
// vehicle table file.  This is an alternative method to load vehicles
// into the network (other than OD trip tables)


int TS_Vehicle::init(int id, int ori, int des, int type_id, int path_id)
{
  setFlag(FLAG_IN_HOME);
  int error = RN_Vehicle::init(id, ori, des, type_id, path_id);
  if (error < 0) return 1;	// show a warning msg
  enterPretripQueue();
  return 0;
}

int TS_Vehicle::initBus(int bid, int ori, int des, int path_id)
{
  setFlag(FLAG_IN_HOME);
  int error = RN_Vehicle::initBus(bid, ori, des, path_id);
  if (error < 0) return 1;	// show a warning msg
  enterPretripQueue();
  return 0;
}

// Load vehicle dumped by a previous MITSIM run

int TS_Vehicle::load(Reader &is, int queue)
{
  int idx;						// lane index
  int ori;						// origin
  int des;						// destination
  int p, k;						// path
  float mlg;

  flags_ = 0;
  this->eff_fgap = 0.0;
  this->eff_bgap = 0.0;
  this->eff_agap = 0.0;


  is >> code_ >> type_
	 >> ori >> des
	 >> departTime_
	 >> mlg
	 >> p >> k
	 >> timeEntersLink_
	 >> idx >> distance_;


  initialize();
  mileage_ = mlg;

  RN_Node *o;
  RN_Node *d;

  if ((o = theNetwork->node(ori)) == NULL ||
	  (d = theNetwork->node(des)) == NULL) {
	load_error("Origin and/or destination", is);
	return -1;
  }

  if (p >= 0) {					// path specified
	setPath(thePathTable->path(p), k);
	//    }
  } else {						// no path specified
    setPath(NULL, k);			// k must be link index
  }

  if (idx >= 0 && idx < theNetwork->nLanes()) {	// position specified
	  lane_ = (TS_Lane *) theNetwork->lane(idx);
  } else {
	  load_error("lane index", is);
	  return -1;
  }

  calcDesiredSpeed();
  currentSpeed_ = desiredSpeed_;

  if (queue) {					// Vehicles in the pretrip queue

	tsLink()->queue(this);

  } else {						// vehicles on the road

	// Put in the list

	append(lane_);
	appendToMacroList(tsSegment());

	// Update pointer to control devices

	nextCtrl_ = link()->ctrlList();
	updateNextCtrl();

	// Update pointer to surveillance devices

	nextSurv_ = link()->survList();
	updateNextSurv(distance_, currentSpeed_);

	// Find the downstream lane

	assignNextLane();

	// Set state variables

	setStatus();
	lcTimeTag_ = theSimulationClock->currentTime();

	accRate_ = 0;
	cfTimer_ = 0;
	positionInQueue_ = 0;

	calcStateBasedVariables();
	
	theStatus->nActive(1);

	tsSegment()->countVehiclesEntered();
	
	unsetFlag(FLAG_IN_HOME);
  }

  // Register the OD pair

  OD_Pair odpair(o, d);
  PtrOD_Pair odptr(&odpair);
  OdPairSetType::iterator i = theOdPairs.find(odptr);
  if (i == theOdPairs.end()) {
	od_ = new OD_Pair(odpair);
	theOdPairs.insert(od_);
  } else {
	od_ = (*i).p();
  }

  code_ = -code_;

  return 0;
}


void TS_Vehicle::load_error(const char* msg, Reader &is)
{
  cerr << "Warning:: " << msg << " of vehicle <"
	   << code_ << "> parsed at <"
	   << is.name() << ":" << is.line_number()
	   << "> invalid. Droped." << endl;
}


/*
 * Assign an vehicle a serial number and the behavior parameters.
 */

void
TS_Vehicle::initialize()
{
  lane_ = NULL;
  nextLane_ = NULL;
  nextCtrl_ = NULL;
  nextSurv_ = NULL;
  cfTimer_ = 0;
  attrs_ = 0;
  flags_ = 0;
  yield_ = 0;
  signedSpeed_ = 0;

  prevLead_ = NULL;//Anita - assigning previous lead and lag, force merging
  prevLag_ = NULL;

  lcProdMlcTagRejProb_ = 1.0;
  lcProdNoseRejProb_ = 1.0;

  int prefix = type_ & (~VEHICLE_CLASS); /* prefix, e.g., HOV */
  int vehicle_class = (type_ & VEHICLE_CLASS);

  if (vehicle_class == 0) {
	vehicle_class = theRandomizers[Random::Departure]->
	  drandom(theParameter->nVehicleClasses(),
			  theParameter->vehicleClassCDF());
  } else {
	vehicle_class --;
  }
  type_ = vehicle_class;

  TS_VehicleLib *plib = theParameter->vehicleLib(type());
  TS_BusLib *blib = NULL;  // Dan: to get bus-specific characteristics

  // Dan: If it's a bus with an assignment, get the bus characteristic
  // library for that bus' type and set a random bus load at start time
  // if it wasn't given a start load in the bus assignment file.

  if (isBusWithAssignment()) {
    BusAssignment *ba = theBusAssignmentTable->findBusAssignment(code());
    blib = theParameter->busLib(ba->busType());
    if (ba->load() < 0) {  // no bus load given, so randomize the load
      int ld = 0;
      ld = (int) theRandomizer->urandom(0, blib->seatingCapacity());
      ba->updateBusLoad(ld);
    }
  }

  // Attach some extra bits to "type" based on given probabilities if
  // the prefix of type is not specified

  if (prefix) {

	type_ |= prefix;

  } else {

	if (theRandomizers[Random::Departure]->brandom(plib->etcRate())) {

	  // This implementation treats all ETC vehicles as PROBE
	  // vehicle also.
     
	  type_ |= VEHICLE_ETC | VEHICLE_PROBE;
	}

	if (theRandomizers[Random::Departure]->
		brandom(theParameter->guidedRate())) {
	  type_ |= VEHICLE_GUIDED;
	}

	if (!theRandomizers[Random::Departure]->
		brandom(plib->overHeightRate())) {
	  type_ |= VEHICLE_LOW;
	}

	if (theRandomizers[Random::Departure]->
		brandom(plib->hovRate())) {
	  type_ |= VEHICLE_HOV;
	}

	if (isBus()) {
	  type_ |= VEHICLE_COMMERCIAL;
	}
  }

  // Dan: if is a bus with bus assignment, then classify its
  // path as fixed: no route switching model

  if (isBusWithAssignment()) {
    type_ |= VEHICLE_FIXEDPATH;
  }

  if (theRandomizer->brandom(theParameter->familiarity()) || isBus()) {
	setAttr(ATTR_FAMILIARITY);
  }

  // By default, nobody complies to any rules and signs

  if (theRandomizer->brandom(theParameter->rmCompRate())) {
	setAttr(ATTR_RAMPMETER_COMPLY |
			ATTR_TS_COMPLY |
			ATTR_PS_COMPLY);
  } else if (theRandomizer->brandom(theParameter->tsCompRate())) {
	setAttr(ATTR_TS_COMPLY | ATTR_PS_COMPLY);
  } else if (theRandomizer->brandom(theParameter->psCompRate())) {
	setAttr(ATTR_PS_COMPLY);
  }

  if (theRandomizer->brandom(theParameter->yLusCompRate())) {
	// This group would to comply to all LUS
	setAttr(ATTR_LUS_COMPLY_YELLOW | ATTR_LUS_COMPLY_RED);
  } else if (theRandomizer->brandom(theParameter->rLusCompRate())) {
	// This group would comply to only red LUS
	setAttr(ATTR_LUS_COMPLY_RED);
  }

  if (theRandomizer->brandom(theParameter->gLcrCompRate())) {
	// This group would to comply to both lane change and use rules
	setAttr(ATTR_GLU_RULE_COMPLY | ATTR_GLC_RULE_COMPLY);
  } else if (theRandomizer->brandom(theParameter->gLurCompRate())) {
	// This group would comply to only lane use rule
	setAttr(ATTR_GLU_RULE_COMPLY);
  }

  if (theRandomizer->brandom(theParameter->typeMsgCompRate())) {
	setAttr(ATTR_MSG_TYPE_LU_COMPLY);
  }

  if (theRandomizer->brandom(theParameter->pathMsgCompRate())) {
	setAttr(ATTR_MSG_PATH_LU_COMPLY);
  }

  if (theRandomizer->brandom(theParameter->guideCompRate())) {
	setAttr(ATTR_MSG_GUIDE_COMPLY);
  }

  int bins = theParameter->nDriverGroups() ;

  driverGroup.maxAccScale    = theRandomizer->urandom(bins) ;
  driverGroup.maxDecScale    = theRandomizer->urandom(bins) ;
  driverGroup.normalDecScale = theRandomizer->urandom(bins) ;
  driverGroup.cfAccAddOn     = theRandomizer->urandom(bins) ;
  driverGroup.cfDecAddOn     = theRandomizer->urandom(bins) ;
  driverGroup.ffAccAddOn     = theRandomizer->urandom(bins) ;
  driverGroup.spdLmtAddOn    = theRandomizer->urandom(bins) ;
  driverGroup.hUpper         = theRandomizer->urandom(bins) ;
  driverGroup.aUpper         = theRandomizer->urandom(bins) ;

  if (isBusWithAssignment()) length_ = blib->length();
  else length_ = plib->length();

  impatient_	= plib->impatient();
  mileage_ = 0.0;
  status_ = 0;
  yieldVehicle_ = NULL;
  lcTimeTag_ = theSimulationClock->currentTime();
  calcUpdateStepSizes();

  // Long vehicle is classified as heavy vehicle

  if (length() < theParameter->heavyVehicleLength()) {
	type_ |= VEHICLE_SMALL;
  }

  // tomer

  randomGapFactor_ = theRandomizer->nrandom_trunc(0.0, 2.5);

  approachSpeedFactor_ = theRandomizer->urandom(0.8, 1.2); // Angus

  // tomer - assign basic look ahead distance

  setLookAheadDistance();

  // gunwoo

  setAggresiveness();


  // random term in kazi's LC gap model

  lcNormalRand_ = theRandomizer->nrandom_trunc(0.0, 1.0);
}


/*
 *--------------------------------------------------------------------
 * Put the vehicle into the queue of its starting link (upstream node
 * must be an origin node).
 *
 * CAUTION: The vehicle is always appended to the end of the queue.
 * Only one queue is maintained for each link. The vehicle to be
 * removed from the queue (enter network) is the head in a lane but
 * not necessarily the head in the link.
 *--------------------------------------------------------------------
 */

void
TS_Vehicle::enterPretripQueue()
{
  static int error_quota = 100;

  timeEntersSegment_ = departTime_;

  /* But this vehicle will not be counted until it actually enters
   * the segment (see enterNetwork)
   */

  if (!nextLink_) {		// no path from orinode to desNode

	if (!theStatus->nErrors()) {
	  theStatus->osLogFile() << LOG_FILE_HEADER_MSG << endl;
	}
	if (error_quota > 0) {
	  theStatus->osLogFile()
		<< code_ << "\t"
		<< theSimulationClock->currentTime() << "\t"
		<< oriNode()->code() << "-" << desNode()->code()
		<< endl;
	} if (error_quota == 0) {
	  theStatus->osLogFile()
		<< endl << PATH_ERROR_MSG
		<< endl;
	}
	error_quota --;
	  
	theStatus->nErrors(1);
	theStatus->nNoPath(1);
	theVehicleList->recycle(this);
	return;
  }

  /* Set the initial desired speed to free flow speed. It will be
   * updated as the vehicle actually enters the network and passes
   * speed limit signs.
   */

  //cfc:For LHD start pretrip queue  from leftmost lane,for RHD start pretrip  queue  from rightmost lane

  

  if ( theNetwork->drivingDirection()==1) {
	       lane_ = (TS_Lane *)nextLink_->startSegment()->leftLane();
  } else {
               lane_ = (TS_Lane *)nextLink_->startSegment()->rightLane();
  }
  calcDesiredSpeed();
  nextTsLink()->queue(this);

  // Find the next link to travel. Because a vehicle may need to
  // choose a correct lane in the first link it travels, the nextLink
  // has to be calculate before the vehicle enters the network.

  OnRouteChoosePath(nextLink_->dnNode());
}

/*
 *--------------------------------------------------------------------
 * Set a downstream target lane for a vehicle according to the next
 * link on its path.
 *--------------------------------------------------------------------
 */

void
TS_Vehicle::assignNextLane()
{
  int i, n = lane_->nDnLanes();
  nextLane_ = NULL;

  if (n == 0) return;

  // First, randomly choose a starting lane

  int k = (int) theRandomizer->urandom(0, n);



  // Find a lane that is in correct direction and has the permission
  // to use

  if (segment()->downstream() != NULL)  { 

    // there is a downstream segment in same link

    for (i = 0; i < n; i ++)  {
      nextLane_ = (TS_Lane *)lane_->dnLane((i + k) % n);
      if (!(nextLane_->isWrongLane(this) ||
    		nextLane_->doesNotAllow(this))) {
      	return;
      }
    }

  } else if (nextLink_ != NULL) { 

    // this is the last segment in this link, there is a new link downstream

    // Dan: first check if there is a bus lane, buses will choose this
    // lane (e.g. for turning at an intersection, buses will deterministically
    // choose the bus lane to enter.)

    for (i = 0; i < n; i ++)  {
      nextLane_ = (TS_Lane *)lane_->dnLane((i + k) % n);

      if (isBus() && nextLane_-> isBusLane() &&
	   (nextLink_ == nextLane_->link())) {
	// Dan: bus is stopped at an intersection (I assume - ?)
	return;
      }
    }

	// Find a lane that is in correct direction 

    for (i = 0; i < n; i ++)  {
      nextLane_ = (TS_Lane *)lane_->dnLane((i + k) % n);

	  if (!nextLane_->doesNotAllow(this) &&
	      (nextLink_ == nextLane_->link())) {

		return;
	  }
    }
  }
}

/*
 *--------------------------------------------------------------------
 * Enter the vehicle into the network if the headway is acceptable.
 * If the vehicle has enter the network successfully, it will be
 * removed from the pretrip queue of the link.
 *
 * Note: The vehicle is is put as far downstream as possible based
 * on its desired speed (or the speed of the vehicle ahead) and the
 * size of time step. Variable "nextLink_" and "desiredSpeed_" must
 * be set before this function is invoked.
 *
 * At the end this function assigns "lane_", "nextLink_", and
 * "nextLane_" to the vehicle. It also calculates the initial
 * position in "lane_", updates pointers between neighbor vehicles
 * and pointers to ctrl and surv devices.
 *--------------------------------------------------------------------
 */

int
TS_Vehicle::enterNetwork()
{
  TS_Segment* psegment = (TS_Segment *)link()->startSegment();

  //cfc:For LHD, start loading vehicles  from leftmost lane,for RHD start loading vehicles  from rightmost lane

  TS_Lane* plane;

 if ( theNetwork->drivingDirection()==1){
               plane = (TS_Lane *)psegment->leftLane();
	       
 } else {
               plane = (TS_Lane *)psegment->rightLane();
 }

  float seglen = psegment->length();

  TS_Vehicle *front;
  TS_Lane *firstlane = NULL;
  distance_ = 0.0;

  float p = theParameter->lcEntryLaneCheckRightnessProb();
  int check_lane = theRandomizer->brandom(p);

  while (plane) {
	if (!(plane->doesNotAllow(this) ||
		  (check_lane && plane->isWrongLane(this)))) {
	  front = plane->lastVehicle();
	  if (front) {
		float gap = seglen - (front->distance() + front->length());
		if (gap > distance_) {
		  distance_ = gap;
		  firstlane = plane;
		}
	  } else {
		distance_ = seglen;
		firstlane = plane;
		break; 
	  }
	}

 //cfc:For LHD, continue  to load in   lanes that are the right of the leftmost lane, for RHD continue to load  lanes
//that are  in the left of the rightmost lane

 if ( theNetwork->drivingDirection()==1){
              plane=(TS_Lane *)plane->right();
	       
 } else {
              plane=(TS_Lane*)plane->left();
 }
  }

  // Check if the vehicle can enter the network now.

  if (!firstlane) {
	return 0;			// no space
  }

  // Calculate the initial speed, which has a value between desired speed
  // and speed the mean speed in the segment.

  // float capspd = theParameter->loadingSpeed();
  float minSpeed_=20.0;
   float capspd = psegment->calcSpeed();
  capspd=max(minSpeed_,capspd);//segment speed has to be greater than 20, cfc 06
  float a=0.0;
 

  float queue = tsLink()->queueLength() / psegment->nLanes();
  float r = theParameter->loadingQueueBeta() * queue * queue ;
  if (r > -5) currentSpeed_ = capspd + exp(r) * (desiredSpeed_ - capspd);
  else currentSpeed_ = capspd ;

  float delta = theParameter->loadingHeadway();
  if (firstlane->lastVehicle() &&
	  distance_ < currentSpeed_ * delta) {
	return 0;
  }


  // Remove this vehicle from the link queue.
     
  tsLink()->dequeue(this);

  // Calculate the position

  distance_ = seglen - currentSpeed_ * theSimulationClock->stepSize();

  // Put this vehicle as the last vehicle in lane_

  lane_ = firstlane;
  append(lane_);

  // Also put in the macro list in the segment

  appendToMacroList(psegment);

  // Set pointers to control and surveillance devices.

  nextCtrl_ = link()->ctrlList();
  nextSurv_ = link()->survList();

  // Find the downstream lane

  assignNextLane();

  // Compute the value for lane change variables.

  setStatus();
  lcTimeTag_ = theSimulationClock->currentTime();

  accRate_ = 0;
  cfTimer_ = 0;
  positionInQueue_ = 0;

  // this->eff_fgap = 0.0;
  // this->eff_bgap = 0.0;
  // this->eff_agap = 0.0;

  calcStateBasedVariables();

  // In case a signal/signs and sensor device is located at the
  // upstream end of the segment, these functions need to be called.

  responseSignalsAndSigns();
  updateNextCtrl();
  updateNextSurv(psegment->length(), currentSpeed_);

  theStatus->nActive(1);

  psegment->countVehiclesEntered();


  unsetFlag(FLAG_IN_HOME);

  // tomer - set approaching intersection in the downstream node.

  setApproachingIS();

  // **

  return (1);
}


void
TS_Vehicle::changeRoute()
{
  RN_Link *p = nextLink_;

  if (!(isSpFlag(INFO_FLAG_VMS_BASED))) {
    // These bit is usuallly set when it view a ENROUTE_VMS.  Here we
    // bypass the requirement for location based information access if
    // the system is not VMS/Beacon based.
    setAttr(ATTR_ACCESSED_INFO);
  }
  OnRouteChoosePath(link()->dnNode());
  
  if (!flag(FLAG_IN_HOME) &&	// on the road
	  nextLink_ != p) {		// path changed
	assignNextLane();
	setStatus();
  }
}


void 
TS_Vehicle::setLookAheadDistance()
{

  float n = theRandomizer->urandom();
  if (!isBus()) {
  lookAheadDistance_ = theParameter->mlcDistance(n);
  }

  // Dan: buses know their routes, have high look ahead distances
  else lookAheadDistance_ = theParameter->lcUpperBound();
}



// gunwoo - aggresiveness


void 
TS_Vehicle::setAggresiveness()
{
   aggresiveness_ = theParameter->cfAccAddOn(driverGroup.cfAccAddOn) / 0.824;
}


/*
 *--------------------------------------------------------------------
 * Record the statistics of an arrived vehicle and remove
 * it from the network.
 *--------------------------------------------------------------------
 */

void
TS_Vehicle::removeFromNetwork()
{
  // For vehicles that have finished their trip, collect moe.

  if (theMoeCollector &&
	  desNode()->code() == link()->dnNode()->code()) {
	theMoeCollector->recordFinished(this);
  }

  if (theEngine->chosenOutput(OUTPUT_VEHICLE_PATH_RECORDS)) {
	writePathRecord(theFileManager->osPathRecord());
  }

  /*
    * Calculate and report statistics of the arrived vehicle.
    */
	
  if (theEngine->chosenOutput(OUTPUT_VEHICLE_LOG))
	{
      reportTripSummary();
	}

  /* remove this vehicle from the macro vehicle list
    */
  this->removeFromMacroList();

  /*
    * remove this vehicle from the list in lane_.
    */
  this->remove();

  theStatus->nActive(-1);
  theStatus->nArrived(1);

#ifdef INTERNAL_GUI
  if (theDrawingArea->isVehicleDrawable()) {
	((TS_DrawableVehicle*)this)->erase(theDrawingArea);
  }
  if (TS_TraceDialog::the()->vehicle() == this) {
	TS_TraceDialog::the()->update(0);
  }
#endif

  /* recycle the memory it used */

  theVehicleList->recycle(this);
}


//////////////////////////////////////////////////////////////////////////
// Intersection lane choice (enters if at end of link,  not last link)////
//////////////////////////////////////////////////////////////////////////

// cfc 07 this function is the assignment of lanes when vehicle comes to end of current link. called from TS_LCModels.cc

void
TS_Vehicle::assignNextIntLane()
{
  int i, n = lane_->nDnLanes();
  nextLane_ = NULL;


  int l1;
  int l2;
  int l3;
  int l4;


  
//cfc Feb 2007
  if (n<2){ // this part is same as assignNextLane
 
   int k = (int) theRandomizer->urandom(0, n);
   for (i = 0; i < n; i ++)  {
      nextLane_ = (TS_Lane *)lane_->dnLane((i + k) % n);

	  if (!nextLane_->doesNotAllow(this) &&
	      (nextLink_ == nextLane_->link())) {

		return;
	  }
   }
  }//n<2
  
   else {//n>2
     int pp;
    vector <int> check(n);
    vector <int> _continuingLanes;    // list of local indexes to connected lanes in the next segment. can be different based on lookahead
    vector <int> reqdChange(n);// n is the number of lanes in downstream segment.reqdChange can be different based on lookahead
   
    
// calculate the path plan variable , assuming all vehicles have paths
   
    float lookAhead_=  LookAheadDistance() ;// lookahead distance of the driver
    int myopic=0;// 1 if driver does not lookahead beyond immediate next section, 0 otherwise
    

    if (lookAhead_<nextLink_ ->length()){
      myopic =1;
    }

 
    for (int j=0;j<n ;j++){

    
      TS_Lane* qlane = (TS_Lane*) lane_->dnLane(j);// qlane is  j th lane the next segment in path
      int n_ds=qlane->nDnLanes();
      int nchange=0;
      

    

      check[j]=((path()->link(2)->code())==(qlane->link()->code()));//qlane is in path
      
      if(check[j]==0) {//cfc 14 Jan
	nchange=100;
	reqdChange[j]=100;
	}//qlane not  in path
    
      else 
      {
      
      for (int i = 0; i < n_ds; i ++)
	{if (check[j]==1)
	_continuingLanes.push_back(i);
      }
      
        if (checkIfLookAheadEvents())
      { 
	nchange = checkMandatoryEventLC();
      }
	else 
      {
	nchange = checkMandatoryLookAhead_discrete(qlane, _continuingLanes);
	
      }
	reqdChange[j]=nchange;      
      //check
    }
}//for j  


#ifdef USE_CFC_MODELS 
		   
vector <double >probL = interLaneChoice(lane_,reqdChange, myopic);//this returns (prob immediate lane choices)

#endif


//final check

 for (int p = 0; p < n; p ++)  {
      TS_Lane* templane = (TS_Lane *)lane_->dnLane(p);

	  if (nextLink_ != templane->link()) {

	    probL[p]=0;
	  }
 }


double cdf=0;

double kk = theRandomizer->urandom(0, 1);

int m ;
for (m = 0;
     m <(n-1) && kk > cdf;
     m ++) {
     cdf += probL[m];
     }

nextLane_ = (TS_Lane *)lane_->dnLane(m-1);
 
return;
      
}//if n>1 and turnign vehicle
  
}

    
 /*
  ****************************Intersection Lane Choice********** */



vector <double> TS_Vehicle::interLaneChoice(TS_Lane*plane, vector <int> & change, int myopic) 

{          
float *d = theParameter->arterialParams(); // cfc  dec 06

int current = plane->localIndex();  //index of naturally connecting lane
int laneNum= change.size();// total number of connected lanes

// define the vectors 
 int ddd;
vector <double> ulane_t(laneNum);// utility of target lanes
vector <double> probLane_t(laneNum);//probabilities of target lanes 

vector <double> ulane_i(laneNum);// utility of target lanes
vector <double> probLane_i(laneNum);//probability vector of immediate lanes 


vector <int> available(laneNum);
vector <int> l_Index(laneNum);

vector <double> logsum(laneNum);
 vector <int> conflict(laneNum);

//logsum is calculated first
// logsum of lane 1 = (ln(exp(util_tL1_iL1)+exp(util_tL2_iL1)+exp(util_tL3_iL1)+exp(util_tL4_iL1)+..))

for (int l = 0; l <laneNum; l ++) { //loop through all available downstream lanes 

TS_Lane* qlane_l = (TS_Lane*) lane_->dnLane(l);
available[l]=((path()->link(2)->code())==(qlane_l->link()->code())&&(nextLink_==qlane_l->link()));//qlane is in path
l_Index[l]=qlane_l->localIndex();

//calculating variable 'conflict '

TS_Vehicle* av_c = findFrontBumperLeader(qlane_l);
TS_Vehicle* bv_c = findFrontBumperFollower(qlane_l);

 // LEADING HEADWAY
 
  float aheadway_c;		// leading headway 
  if (av_c) {
	aheadway_c = this->gapDistance(av_c);
  } else {
	aheadway_c = FLT_INF;
  }

  // LAG HEADWAY

  float bheadway_c;		// lag headway 
  if (bv_c) {
	bheadway_c = bv_c->gapDistance(this);
  } else {
	bheadway_c = FLT_INF;
  }

  conflict[l]=(aheadway_c+bheadway_c)<(this->length());

ddd=conflict[l];
logsum[l]=0;
double u=0;
for (int ll=0;ll<laneNum;ll++){

u=(d[11]/(d[12]+d[13]*aggresiveness()))*abs(current-ll)+(ll==l_Index[l])*d[14]+abs(ll-l_Index[l])*(d[15]/(d[16]+d[17]*aggresiveness()))+d[18]*conflict[l];

 logsum[l]=logsum[l]+available[l]*exp(u);
 }
logsum[l]=log(Max(.00001,logsum[l]));


}


//target lane probabilities are calculated first

for (int t = 0; t <laneNum; t++) { //loop through all downstream target lanes 

TS_Lane* qlane_tl = (TS_Lane*) lane_->dnLane(t);


if (available[t]==1) {//qlane_tl is in path

//assigning constants for target lane 

	switch (l_Index[t]){
	case 0:	{
	  ulane_t[t]=0;
	  break;
	}
	case 1:	{
	  ulane_t[t]=d[0];
	  break;
	}//left most lane lane0 is the base 
	case 2:	{
	  ulane_t[t]=d[1];
	  break;
	}

	default: { 
	  ulane_t[t]=d[2];
	  break;
	}
	}

 //adding other variables

int lQ=qlane_tl-> nVehicles(); 
double dischargeQ= qlane_tl->dischargeRate(qlane_tl);//this is discharge rate (historic)of lane.hardcoded
double delay;
if (lQ>1)
   {delay=2+ 1/(1+exp(-lQ/dischargeQ));}
else { delay=2;}



ulane_t[t]=ulane_t[t]+d[3]*delay+ myopic *(d[4]/(d[5]+d[6]*aggresiveness()))*change[t]+ (1-myopic)*(d[7]/(d[8]+d[9]*aggresiveness())) * change[t]+d[10] * logsum[t];
if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
{
	  	     // ******** May 25, Gunwoo **************//
	     // target lane utility
	      ostream& os = theFileManager->osLaneUtility();
	      os  <<"101"<<" "<<this->code()<<" "<<l_Index[t]<<" "<<myopic<<" "<<change[t]<<" "<<delay<<" "<<logsum[t]<<" "<<aggresiveness()<<" "<<ulane_t[t]<<endl;
}
ulane_t[t]=exp(ulane_t[t]); 


}//if available         
                                                                                                                                     
else{ulane_t[t]=available[t]*exp(-9999999);}// qlane_i not in path
  
}//all ds lanes

 double sum;
  sum = 0.0;
// calculate sum 
 for (int i=0;i<laneNum;i++){
   sum=sum+available[i]*ulane_t[i];
 }

 //calculate probabilty 
 if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
{
	  	     // ******** May 25, Gunwoo **************//
	     // target lane utility
	      ostream& os = theFileManager->osLaneUtility();
	      os  <<endl<< "101" << " " //cfc 26 sep 8=12
		  << this->code() << " " << Fix(theSimulationClock->currentTime(), 1.0) <<" ";
}

 for (int i=0;i<laneNum;i++){

   
   if (available[i]==0)
     {probLane_t[i]=0;}
   else{
     probLane_t[i]=available[i]*ulane_t[i]/sum;}

     if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
{
	  	     // ******** May 25, Gunwoo **************//
	     // target lane utility
	      ostream& os = theFileManager->osLaneUtility();
	      os  <<available[i]<<" "<<probLane_t[i]<<"    ";
}
 }
 // select target lane
 



 double cdf2=0;//probLane_t[0];// since local index starts from 0
 double r = theRandomizer->urandom(0, 1);

int TL ;
for (TL = 0;
     TL < (laneNum-1) && r > cdf2;
     TL ++) {
     cdf2 += probLane_t[TL];
     }

 TL=TL-1;

// now calculate immediate lane probability

 for (int j=0;j<laneNum;j++){

 if (available[j]==1){

  ulane_i[j]=(d[11]/(d[12]+d[13]*aggresiveness()))*abs(current-l_Index[j])+(l_Index[j]==TL)*d[14]+abs(l_Index[j]-TL)*(d[15]/(d[16]+d[17]*aggresiveness()))+d[18]*conflict[j];
 
if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
{
	  	     // ******** May 25, Gunwoo **************//
	     // target lane utility
	      ostream& os = theFileManager->osLaneUtility();
              if (j==0)
		os<<endl;

	      os  <<"102"<<" "<<this->code()<<" "<<(current-l_Index[j])<<" "<<(l_Index[j]-TL)<<" "<<conflict[j]<<" "<<" "<<aggresiveness()<<" "<<ulane_i[j]<<endl;
} 
  ulane_i[j]=exp(ulane_i[j]);
}
else{ulane_i[j]=available[j]*exp(-9999999);}// qlane_i not in path
 }
// calculate sum 
 double sum_i;
sum_i = 0.0;
 for (int i=0;i<laneNum;i++){
   sum_i=sum_i+(available[i]==1)*ulane_i[i];
 }
 //calculate probabilty 

 if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
{
	  	     // ******** May 25, Gunwoo **************//
	     // target lane utility
	      ostream& os = theFileManager->osLaneUtility();
	      os  << "102" << " " //cfc 26 sep 8=12
		  << this->code() << " " << Fix(theSimulationClock->currentTime(), 1.0) <<" ";
}

 for (int i=0;i<laneNum;i++){

   if (available[i]==0)
     {probLane_i[i]=0;}
   else{
     probLane_i[i]=available[i]*ulane_i[i]/sum_i;}
   if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
{
	  	     // ******** May 25, Gunwoo **************//
	     // target lane utility
	      ostream& os = theFileManager->osLaneUtility();
	      os  <<available[i]<<" "<<probLane_i[i]<<"    ";
}
     
 }
return probLane_i;
}//end of function

/*This function returns the number of changes according to the path plan, continuing lane defined based on lookahead */

int
TS_Vehicle::checkMandatoryLookAhead_discrete(RN_Lane* qlane, vector <int> & _continuingLanes)
{
  int idx = pathIndex_+1; 
  int lanid;
  lanid = qlane->code(); 
  int vehid;
  vehid=this->code();
  int des;
  des=this->desNode()->code();


  
  RN_Link* plink = qlane->link();
  RN_Segment* psegment = qlane->segment();
  int laneNum=psegment->nLanes();//cfc
  float x=distance_;
  int change = 0;
  RN_Segment* ssegment;
  RN_Lane* plane;
  RN_Lane* slane;
  
 double lookAheadDistance_is= Max(lookAheadDistance_, plink->length());// for intersection model all drivers at least look 1 intersection 




  if ( x>=lookAheadDistance_is// && status(STATUS_CURRENT_OK) 
       ) {
    return change;
  }
  
  while ( x<lookAheadDistance_is )                 // within lookahead distance
    {
      if ( psegment != plink->endSegment() )     // still in this link
	{
	  psegment = psegment->downstream();
	  x += psegment->length();
	}
      else if ( idx < path()->nLinks() )        // looking at the next link
	{
	 
	  if ( !path()->link(idx) )
	    {break;}
	  plink = path()->link(idx);
	  //	  cout << plink << endl;
	  
	  psegment = plink->startSegment();
	  x += psegment->length();
	  idx++;
	}
      else { break; }                            // end of path 
    }

  if ( psegment != plink->startSegment() ) {
    ssegment = psegment->upstream();
    idx--;
  } 
  else {
    idx -= 2;
    if ( path()->link(idx) ) {
      plink = path()->link(idx);
      ssegment = plink->endSegment();
    } 
    else 
      {return 0; }
  }
  
  vector <int > _upLaneList;        // list of local indexes to connected lanes in ssegment


  _continuingLanes.erase(_continuingLanes.begin(), _continuingLanes.end() );

  for (int i = 0; i < psegment->nLanes(); i ++)
    {
      _continuingLanes.push_back(i);
    }


  // advancing upstream segment by segment and creating the connectivity lists

 while ( psegment != qlane->segment() )  {  
   bool allMarked = false;
  
   // iterate all connected lanes in the downstream segment
   for (int i = 0; i < _continuingLanes.size(); i ++) { 
     bool lastConnect = false ;
     plane = psegment->lane(_continuingLanes[i]);

     TS_Lane* tlane = (TS_Lane*) psegment->lane(_continuingLanes[i]);
     if (! tlane->doesNotAllow(this)) // avoid lanes not allowed (e.g. HOV lane) (Angus)
       {
	 // iterate all connected lanes in the upstream segment
	 for (int j = 0; j < ssegment->nLanes(); j ++) { 
	   
	   // avoid lanes that the vehicle is not allowed to use
	   
	   TS_Lane* tlane = (TS_Lane*) ssegment->lane(j);
	   if (! tlane->doesNotAllow(this)) { 
	     slane = ssegment->lane(j);
	     bool connect = plane->isInUpLanes(slane);
//	     int* marked = find (_upLaneList.begin(), _upLaneList.end(), j );
	     int marked = * ( find (_upLaneList.begin(),_upLaneList.end(),j ) );
	     if ( connect && marked == * _upLaneList.end() ) { 
	       // put it at the end of the list
	       _upLaneList.push_back(j);
	       lastConnect = connect;
	       
	       // stop if all upstream lanes are connected	  
	       if ( _upLaneList.size() == ssegment->nLanes() ) {
		 allMarked = true;
		 break; 
	       }
	       else if (!connect && lastConnect) {break;}
	     }
	   }
	   else if (lastConnect ) { break; }
	 }
       }
     if (allMarked) {break;}
   }

   // the upstream set is empty. LC must happen downstream.  
   if ( _upLaneList.empty() ) { 
     // maybe sorting is not really needed - check it for deletion
     // sort ( _continuingLanes.begin() , _continuingLanes.end() );
     int leftIndex = _continuingLanes[0];
     int rightIndex = _continuingLanes[_continuingLanes.size()-1];
     if (leftIndex > rightIndex) {
       int temp = leftIndex;
       leftIndex = rightIndex;
       rightIndex = temp;
     } 
     while ( _upLaneList.empty() ) {
       if ( ++rightIndex < psegment->nLanes()) {

	 // iterate all continuing lanes in the upstream segment
	 for (int j = 0; j < ssegment->nLanes(); j ++) { 
	   plane = psegment->lane(rightIndex);
	   slane = ssegment->lane(j);
	   bool connect = plane->isInUpLanes(slane);
	   if ( connect ) { // put it at the end of the list
	     _upLaneList.push_back(j);
	   }
	 }
       }
       
        if ( leftIndex-- >= 0 && _upLaneList.empty() ) {

	 // iterate all connected lanes in the upstream segment
	  for (int j = 0; j < ssegment->nLanes(); j ++) { 
	    plane = psegment->lane(leftIndex);
	    slane = ssegment->lane(j);
	    bool connect = plane->isInUpLanes(slane);
	    if ( connect ) { // put it at the end of the list
	      _upLaneList.push_back(j);
	    }
	  }
	}
	// a correction for the case there is no connectivity between 
	// the 2 segments (Angus and Tomer)
	if (leftIndex < 0 && rightIndex > psegment->nLanes() ) {
	  for (int j = 0; j < ssegment->nLanes(); j ++) { 
	    _upLaneList.push_back(j);
	  }
	}
	// [end correction]
     }
   }
       
      _continuingLanes = _upLaneList;
      _upLaneList.erase(_upLaneList.begin(), _upLaneList.end() );

      psegment = ssegment;
      
      
      if ( psegment != (qlane->segment()) ){
	if ( ssegment != plink->startSegment() ) {
	  ssegment = ssegment->upstream();
	} 
	else {
	  idx--;
	  plink = path()->link(idx);
	  ssegment = plink->endSegment();
	}		    
      }
 }


 double debug, debug2,debug3,debug4,cfc, debug5, debug6, debug7, debug8, debug9;
// the result is a list of connected lanes in the current segment. needs to 
 // check that the current one is included in them or else what changes need 
 // to be done.


 int nCurrent = 100; // number of lane changes required for the current lane.
 for (int i = 0; i < _continuingLanes.size(); i++) {

    
     int numlcCurrent  = abs (_continuingLanes[i] -  (qlane->localIndex())) ;
     nCurrent = Min(nCurrent, numlcCurrent);

   }



  
   int minChange=100;
  

   for (int i=0;i<_continuingLanes.size();i++){
      int numChange=abs(_continuingLanes[i]-qlane->localIndex());
      minChange=Min(minChange,numChange);
        }
   
   change=minChange;
   return change;
}
