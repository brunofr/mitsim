//-*-c++-*------------------------------------------------------------
// NAME: Traffic Simulation 
// AUTH: Qi Yang
// FILE: TS_TollBooth.C
// DATE: Fri Aug 30 21:59:01 1996
//--------------------------------------------------------------------

#include <Tools/ToolKit.h>
#include <Tools/Random.h>
#include <Tools/SimulationClock.h>
#include <Tools/Math.h>

#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>
#include <GRN/BusAssignment.h>
#include <GRN/BusAssignmentTable.h>
#include <GRN/RN_BusRoute.h>
#include <GRN/Parameter.h> //IEM(Aug13)
#include <GRN/RN_Segment.h>

#include "TS_Vehicle.h"
#include "TS_BusStop.h"
#include "TS_Parameter.h"
#include "TS_BusLib.h"
#include "TS_Engine.h"
#include "TS_Network.h"
#include "TS_FileManager.h"
#include "TS_Exception.h"
#include "TS_Lane.h"

#ifdef INTERNAL_GUI
#include <DRN/DRN_DrawingArea.h>
#include "TS_Vehicle.h"
#endif

TS_BusStop::TS_BusStop()
   : TS_Signal(),
     RN_BusStop()
{
}

/*
 *------------------------------------------------------------------
 * Returns dwell time in seconds.
 *------------------------------------------------------------------
 */

float 
TS_BusStop::delay(TS_Vehicle* pv)
{
  float dwell = 0.;

  if (theBusAssignmentTable) {
    BusAssignment *ba = theBusAssignmentTable->findBusAssignment(pv->code());
    TS_BusLib *plib = theParameter->busLib(ba->busType());

    int rid = ba->route()->code();
    float arate = arrivalRate(rid);
    float stime = startTime(rid);

    /* Dan: the following if conditions assume that, if an arrival rate
       is specified, then arrival rates and alighting percentages are 
       used to calculate dwell time.  And, if a start time is specified,
       then the passengers arrive according to the schedule and the 
       parameters of the triangular passenger arrival rate are used
       to calculate dwell time.  It is further assumed that only one
       or the other is used throughout the simulation (i.e. not 
       arrival rates specified at 8:00 and start times
       specified at 9:00). */

    if (arate > 0 || stime > 0) dwell = calcDelay(pv, ba);
    else {
      dwell = theRandomizer->urandom(minDwell_, maxDwell_);
    }
  }
  else {
    dwell = theRandomizer->urandom(minDwell_, maxDwell_);
  }

  return dwell;
}


/*
 *--------------------------------------------------------------------
 * Returns the acceleration at bus stop.
 *--------------------------------------------------------------------
 */

// margaret:  try to isolate buses only

float 
TS_BusStop::acceleration(TS_Vehicle* pv, float dist)
{
  if (pv->isBus()) {
  
    // float limit = stopLength_ * theParameter->speedFactor();

    float limit = 0; // IEM(Aug13) It's always 0, right?  Otherwise,
                     // you'd have to run to board a bus.
    float length = stopLength() * theBaseParameter->lengthFactor(); //IEM(Aug13)

    //IEM(Aug13) Reset target distance to the back of the front bus, if
    // it's stopped and you can fit into the stop.
    if(pv->leading()) {
      if(pv->leading()->status(STATUS_STOPPED) &&
	 pv->leading()->distance() + pv->leading()->length() 
         + .1+ pv->length()< distance() + length ) {
	dist = pv->distance() - (pv->leading()->distance() 
               + pv->leading()->length() + .1);
      }
    }

    if ((state() & SIGNAL_COLOR) <= SIGNAL_RED) {	// lane is closed
      pv->setStatus(STATUS_BUS_SLOWING);
      return pv->brakeToStop(dist);

    } else if (pv->status(STATUS_STOPPED)) {
        // stopped already - need to set flag
        pv->unsetStatus(STATUS_STOPPED);
        pv->setStatus(STATUS_ALREADY_PAID);
        return MAX_ACCELERATION;

    } else if (pv->status(STATUS_ALREADY_PAID)) {
        // stopped already - flag already set
        pv->unsetStatus(STATUS_MANDATORY);
        if (theBusAssignmentTable) {
          BusAssignment *ba = theBusAssignmentTable->findBusAssignment(pv->code());
	  ba->setLeftOrRight(0);
	  pv->setStatus(STATUS_BUS_DEPARTING);
        } // Dan: bus' status is set to departing to override bus stop as an
          // influence in the lane making decision

	if (isWayside()) pv->setFlag(FLAG_NOSING);
	if (requiresSqueeze()) {
          state_ &= (~INCI_ACTIVE);
	}

        return MAX_ACCELERATION;

    } else if (dist > pv->distanceToNormalStop() && 
	       (pv->currentSpeed() > SPEED_EPSILON ||
		pv->distance() + pv->length() > distance() + stopLength() ) ) {
        // constraints is not applied in this case
        return MAX_ACCELERATION;

    } else if (dist > DIS_EPSILON) {
        // prepared to stop or reduce speed
        if (pv->currentSpeed()>SPEED_EPSILON) {
          return pv->brakeToTargetSpeed(dist, limit);
	} 
      //IEM(Aug13) If the bus is already not moving, and not about to move,
      // and is completely in the bus stop, don't accelerate it.
        else if (pv->distance() + pv->length() > distance() + stopLength() ) {
	  return MAX_ACCELERATION; 
	}

    } else {
        // stop for duration of dwell time
        pv->setStatus(STATUS_STOPPED);

	if (requiresSqueeze()) {
	  state_ |= INCI_ACTIVE;
	}
      
        pv->cfTimer(delay(pv));

        return -FLT_INF;
      }
   }
}

void 
TS_BusStop::logBusArrival(TS_Vehicle* pv, int c, float dt, double hw, double sa)
{
  for (int i = 0; i < nRoutesServeStop(); i ++) {
    if (routeid_[i] == c) {
      numbusarrivals_[i]++;
      lastbusid_[i] = pv->code();
      lastarrivaltime_[i] = theSimulationClock->currentTime();
      lastbusdwell_[i] = dt;
      lastheadway_[i] = hw;
      lastschedarrival_[i] = sa;
    }
  }
}

int 
TS_BusStop::lastBusID(int rc)
{
  for (int i = 0; i < nRoutesServeStop(); i ++) {
    if (routeid_[i] == rc) {
      return lastbusid_[i];
    }
  }
  return 0;
}  

double 
TS_BusStop::lastArrivalTime(int rc)
{
  for (int i = 0; i < nRoutesServeStop(); i ++) {
    if (routeid_[i] == rc) {
      return lastarrivaltime_[i];
    }
  }
  return 0;
}  

double 
TS_BusStop::lastSchedArrival(int rc)
{
  for (int i = 0; i < nRoutesServeStop(); i ++) {
    if (routeid_[i] == rc) {
      return lastschedarrival_[i];
    }
  }
  return 0;
} 
 
double 
TS_BusStop::lastHeadway(int rc)
{
  for (int i = 0; i < nRoutesServeStop(); i ++) {
    if (routeid_[i] == rc) {
      return lastheadway_[i];
    }
  }
  return 0;
}  

float 
TS_BusStop::lastBusDwellTime(int rc)
{
  for (int i = 0; i < nRoutesServeStop(); i ++) {
    if (routeid_[i] == rc) {
      return lastbusdwell_[i];
    }
  }
  return 0;
}  

float 
TS_BusStop::arrivalRate(int rc)
{
  for (int i = 0; i < nRoutesServeStop(); i ++) {
    if (routeid_[i] == rc) {
      return arrivalrate_[i];
    }
  }
  return 0;
}  

float 
TS_BusStop::alightingPct(int rc)
{
  for (int i = 0; i < nRoutesServeStop(); i ++) {
    if (routeid_[i] == rc) {
      return alightpct_[i];
    }
  }
  return 0;
}  

int 
TS_BusStop::passengersArrived(int rc)
{
  for (int i = 0; i < nRoutesServeStop(); i ++) {
    if (routeid_[i] == rc) {
      return passarrive_[i];
    }
  }
  return 0;
} 

int 
TS_BusStop::passengersAlighted(int rc)
{
  for (int i = 0; i < nRoutesServeStop(); i ++) {
    if (routeid_[i] == rc) {
      return passalight_[i];
    }
  }
  return 0;
} 

int 
TS_BusStop::passengersLeft(int rc)
{
  for (int i = 0; i < nRoutesServeStop(); i ++) {
    if (routeid_[i] == rc) {
      return passengersleft_[i];
    }
  }
  return 0;
} 

int 
TS_BusStop::numBusArrivals(int rc)
{
  for (int i = 0; i < nRoutesServeStop(); i ++) {
    if (routeid_[i] == rc) {
      return numbusarrivals_[i];
    }
  }
  return 0;
} 

float 
TS_BusStop::startTime(int rc)
{
  for (int i = 0; i < nRoutesServeStop(); i ++) {
    if (routeid_[i] == rc) {
      return starttime_[i];
    }
  }
  return 0;
} 

float 
TS_BusStop::intervalA(int rc)
{
  for (int i = 0; i < nRoutesServeStop(); i ++) {
    if (routeid_[i] == rc) {
      return intervala_[i];
    }
  }
  return 0;
} 

float 
TS_BusStop::intervalB(int rc)
{
  for (int i = 0; i < nRoutesServeStop(); i ++) {
    if (routeid_[i] == rc) {
      return intervalb_[i];
    }
  }
  return 0;
}
 
float 
TS_BusStop::intervalC(int rc)
{
  for (int i = 0; i < nRoutesServeStop(); i ++) {
    if (routeid_[i] == rc) {
      return intervalc_[i];
    }
  }
  return 0;
}
 
float 
TS_BusStop::peakRate(int rc)
{
  for (int i = 0; i < nRoutesServeStop(); i ++) {
    if (routeid_[i] == rc) {
      return peak_[i];
    }
  }
  return 0;
} 
 
int 
TS_BusStop::lastBusCycle(int rc)
{
  for (int i = 0; i < nRoutesServeStop(); i ++) {
    if (routeid_[i] == rc) {
      return lastbuscycle_[i];
    }
  }
  return 0;
}
 
int 
TS_BusStop::lastBusInterval(int rc)
{
  for (int i = 0; i < nRoutesServeStop(); i ++) {
    if (routeid_[i] == rc) {
      return lastbusinterval_[i];
    }
  }
  return 0;
} 
 
float 
TS_BusStop::lastBusTime(int rc)
{
  for (int i = 0; i < nRoutesServeStop(); i ++) {
    if (routeid_[i] == rc) {
      return lastbustime_[i];
    }
  }
  return 0;
} 

float 
TS_BusStop::lastWaitTime(int rc)
{
  for (int i = 0; i < nRoutesServeStop(); i ++) {
    if (routeid_[i] == rc) {
      return lastwaittime_[i];
    }
  }
  return 0;
}  

void
TS_BusStop::resetPassengerData(int c, int arrive, int alight, int left)
{
  for (int i = 0; i < nRoutesServeStop(); i ++) {
    if (routeid_[i] == c) {
      passarrive_[i] = arrive;
      passalight_[i] = alight;
      passengersleft_[i] = left;
    }
  }
} 

void
TS_BusStop::updateLastBusArrival(int c, int cycle, int interval, float t, float wt)
{
  for (int i = 0; i < nRoutesServeStop(); i ++) {
    if (routeid_[i] == c) {
      lastwaittime_[i] = wt;
      lastbuscycle_[i] = cycle;
      lastbusinterval_[i] = interval;
      lastbustime_[i] = t;
    }
  }
} 

// Save bus stop arrival record

void 
TS_BusStop::saveArrivalRecord(ofstream &os, int id)
{
  int busError = 0;

  if (theEngine->chosenOutput(OUTPUT_RECT_TEXT)) {
    float t = theSimulationClock->currentTime();
    os << Fix(t, (float)0.1);
  }           
  for (int i = 0; i < nRoutesServeStop(); i ++) {
    if (routeid_[i] == id) {
      busError++;
      os << endc << code_ << endc           // stop id
      << id << endc                         // route id
      << lastbusid_[i] << endc              // bus id
      << lastschedarrival_[i] << endc       // scheduled arrival time
      << lastheadway_[i] << endc            // headway between this bus and the previous bus
      << lastbusdwell_[i] << endc           // dwell time
      << passarrive_[i] << endc             // number arriving to board
      << passalight_[i] << endc             // number alighting
      << passengersleft_[i] << endc         // passengers left behind by this bus
      << lastwaittime_[i] << endc           // wait time for passengers boarding bus
      << endl;  
    }
  }     
  if (busError < 0.5) {
    os << " Route " << id << " does not serve stop " << code_ << endl; 
  }                             
}

void
TS_BusStop::writeArrivalRecord(int rid)
{
  static char opened = 0;
  static ofstream os;

  if (!opened) {
	const char *file =  ToolKit::outfile(theFileManager->busStopArrivalFile());
	os.open(file);
	if (!os.good()) {
	  cerr << "Error:: problem with output file <"
		   << file
		   << ">" << endl;
	  theException->exit();
	}
	if (!theEngine->chosenOutput(OUTPUT_SKIP_COMMENT)) {
	  os << "# Bus stop arrivals" << endl;
	  if (theEngine->chosenOutput(OUTPUT_RECT_TEXT)) {
		os << "# Time Stop_ID Route_ID Bus_ID Sched_Arrival Headway Dwell nArrive nAlight nLeft " << endl;
	  } else {
		os << "# Time {" << endl
		   << "#   Stop_ID Route_ID Bus_ID Sched_Arrival Headway Dwell nArrive nAlight nLeft " << endl
		   << "# }" << endl;
	  }
	  os << endl;
	}
	opened = 1;
  }

  if (!theEngine->chosenOutput(OUTPUT_RECT_TEXT)) {
	float t = theSimulationClock->currentTime();
	os << Fix(t, (float)0.1) << " {" << endl;
  }

  saveArrivalRecord(os, rid);

  if (!theEngine->chosenOutput(OUTPUT_RECT_TEXT)) {
	os << "}" << endl;
  }
}

/*
 * Acceleration rate when vehicles must "squeeze" around buses at a stop 
 */

float
TS_BusStop::squeeze(TS_Vehicle* pv, float dist)
{
  float acc = 0.;
  float factor = theBaseParameter->busStopSqueezeFactor();
  float speed = (segment()->speedLimit())*factor;

  if (state_ & INCI_ACTIVE) {
     acc = pv->brakeToTargetSpeed(dist, speed);
  }
  else acc = MAX_ACCELERATION;
  return (acc);
}

float 
TS_BusStop::calcDelay(TS_Vehicle* pv, BusAssignment* ba)
{
  float dwell = 0.;

  TS_BusLib *plib = theParameter->busLib(ba->busType());
  int rid = ba->route()->code();
  float arate = arrivalRate(rid);
  float la = lastArrivalTime(rid);
  float nsa = ba->nextSchedArrival();
  float ct = theSimulationClock->currentTime();

  /* Dan: calculate two headways, headway = time since last bus
    *departed*, used for dwell time calculations, hway = time
    since last bus *arrived*, used to record the prevailing
    headway on the bus. */

  double headway = 0.;
  double hway = -1.;

  if (la > 0) {

    headway = ct - (la + lastBusDwellTime(rid));

    /* Dan: if the previous bus is still at the stop, and the current
       bus has arrived within the previous bus' dwell time, then headway
       will be negative, don't want to generate negative passengers, so
       set headway to zero. */

    if (headway < 0) headway = 0.;

    hway = ct - lastArrivalTime(rid);
  }
  else {  
    /* Dan: This is the first bus on route rid to arrive in this simulation,
       so we'll assume that the headway at the stop is the bus'
       initial, entering headway */
    headway = hway = ba->prevailingHeadway();
  }

  /* Calculate arriving and alighting passengers */

  int load = ba->load();

  // Dan: generate random dead time, add to dwell time...

  float deadt = 0.;
  if (headway > 0.5 || la < 0.5) {
    /* Dan: If headway < 0.5, then the bus has just arrived behind
       another on the same route, and it should proceed without
       stopping, thus deadt would remain zero. OR, if the last 
       arrival time is < 0.5, then this is the first bus to arrive
       at the stop on this route, calculate the dead time ... */

    deadt = theRandomizer->urandom(plib->deadTimeLowerBound(), 
			 plib->deadTimeUpperBound());
  }
  dwell += deadt;

  // Dan: calculate time for passengers to alight, add to dwell time...

  float alirate = plib->alightingRate();
  alirate = theRandomizer->urandom((alirate-0.5), (alirate+0.5)); // randomize alighting rate
  int nalighting = (int) floor((load * alightingPct(rid)) + 0.5); // floor(x+0.5) rounds to nearest integer
  dwell += alirate * nalighting;
  load -= nalighting;  // subtract alighting passengers from bus load

  // Dan: calculate time for passengers to board, add to dwell time...
   
  int narrivals = -1;  // must begin negative, see below
  float meanarr = 0.;
  if (isControlEnabled(rid) && nsa > ct) {
    meanarr = arate * (headway + (ct - nsa))/3600.;
  }
  else meanarr = arate * headway/3600.;

  if (arate > 0) {
    while (narrivals < 0) {  // ensure a positive number of arriving passengers
      float z = theRandomizer->nrandom();
      float narr = meanarr + sqrt(meanarr)*z - 0.5;  // normal distr approximating Poisson
      narrivals = (int) floor(narr + 0.5);     // floor(x+0.5) rounds to nearest integer
    }
  }
  else { // assume schedule-based arrival rates are specified
    narrivals = scheduleArrivals(rid, nsa);
  }

  narrivals += passengersLeft(rid);
  float br = plib->boardingRate();
  float brate = theRandomizer->urandom((br-0.4),(br+0.6)); // randomize boarding rate
  int sc = plib->seatingCapacity();
  int tc = plib->totalCapacity();
  int nleft = 0;
  float cf = plib->crowdingFactor();

  if (load > sc) {
    dwell += min((tc - load), narrivals) * (brate + cf);
    if (narrivals > (tc - load)) {
      nleft = narrivals + load - tc;
      load = tc;
    }
    else {
      nleft = 0;
      load += narrivals;
    }
  } 
  else {
    dwell += min((sc - load), narrivals) * brate;
    if ((narrivals + load) > sc) {
      dwell += min((narrivals + load - sc), (tc - sc)) * (brate + cf);
    }
    if ((narrivals + load) > tc) {
      nleft = narrivals + load - tc;
      load = tc;
    }
    else {
      nleft = 0;
      load += narrivals;
    }
  }

  float holdmax = 0.;
  float limit = 120.;  // Dan - hard-coded max holding (dwell) time = 2 minutes

  if(isControlEnabled(rid)) {
    holdmax = min(limit, (nsa-ct));
    dwell = max(dwell, holdmax);
  }

  // Dan: update the bus load and update the stop's "last bus" passenger variables

  ba->updateBusLoad(load);
  resetPassengerData(rid, narrivals, nalighting, nleft);

  // Dan: update the bus' prevailing schedule information, schedule deviation,
  // headway from last bus on the route, etc.

  ba->updateSchedDeviation(nsa - ct);
  ba->updateHeadway(hway);

  // Dan: update the current dwell time variables for the bus stop

  logBusArrival(pv, rid, dwell, hway, nsa);

  // Dan: Write the bus arrival record only if it is not the first bus on the route
  // to arrive at the bus stop (don't know anything about headway yet, etc.)

  if (numBusArrivals(rid) > 1) {
    writeArrivalRecord(rid);
  }

  ba->updateNextArrival();
  pv->unsetStatus(STATUS_MANDATORY);
  return dwell;
}

/*
float 
TS_BusStop::arrivalDelay(TS_Vehicle* pv, BusAssignment* ba)
{
  float dwell = 0.;

  TS_BusLib *plib = theParameter->busLib(ba->busType());
  int rid = ba->route()->code();
  float la = lastArrivalTime(rid);

//   Dan: calculate two headways, headway = time since last bus
//   *departed*, used for dwell time calculations, hway = time
//   since last bus *arrived*, used to record the prevailing
//   headway on the bus. 

  double headway = 0.;
  double hway = -1.;

  if (la > 0) {

    headway = theSimulationClock->currentTime() - 
      (la + lastBusDwellTime(rid));

//   Dan: if the previous bus is still at the stop, and the current
//   bus has arrived within the previous bus' dwell time, then headway
//   will be negative, don't want to generate negative passengers, so
//   set headway to zero. 

    if (headway < 0) headway = 0.;

    hway = theSimulationClock->currentTime() - lastArrivalTime(rid);
  }
  else {  
//   Dan: This is the first bus on route rid to arrive in this simulation,
//   so we'll assume that the headway at the stop is the bus'
//   initial, entering headway 
    headway = hway = ba->prevailingHeadway();
  }

// Calculate arriving and alighting passengers 

  int load = ba->load();

  // Dan: generate random dead time, add to dwell time...

  float deadt = 0.;
  if (headway > 0.5) {
//   Dan: If headway < 0.5, then the bus has just arrived behind
//   another on the same route, and it should proceed without
//   stopping, thus deadt would remain zero, otherwise ... 

    float deadt = theRandomizer->urandom(plib->deadTimeLowerBound(), 
			 plib->deadTimeUpperBound());
  }
  dwell += deadt;

  // Dan: calculate time for passengers to alight, add to dwell time...

  int nalighting = (int) floor((load * alightingPct(rid)) + 0.5); // floor(x+0.5) rounds to nearest integer
  dwell += plib->alightingRate() * nalighting;
  load -= nalighting;  // subtract alighting passengers from bus load

  // Dan: calculate time for passengers to board, add to dwell time...
   
  int narrivals = scheduleArrivals(rid) + passengersLeft(rid);
  float brate = plib->boardingRate();
  int sc = plib->seatingCapacity();
  int tc = plib->totalCapacity();
  int nleft = 0;

  if ((sc - load) > 0) dwell += min((sc - load), narrivals) * brate;

  if ((narrivals + load) > sc) {
    float cf = plib->crowdingFactor();
    dwell += min((narrivals + load - sc), (tc - sc)) * (brate + cf);
  }

  if ((narrivals + load) > tc) {
    nleft = narrivals + load - tc;
    load = tc;
  }
  else {
    nleft = 0;
    load += narrivals;
  }

  // Dan: update the bus load and update the stop's "last bus" passenger variables

  ba->updateBusLoad(load);
  resetPassengerData(rid, narrivals, nalighting, nleft);

  // Dan: update the bus' prevailing schedule information, schedule deviation,
  // headway from last bus on the route, etc.

  ba->updateSchedDeviation(ba->nextSchedArrival() - theSimulationClock->currentTime());
  ba->updateHeadway(hway);

  // Dan: update the current dwell time variables for the bus stop

  logBusArrival(pv, rid, dwell, hway, ba->nextSchedArrival());

  // Dan: Write the bus arrival record only if it is not the first bus on the route
  // to arrive at the bus stop (don't know anything about headway yet, etc.)

  if (numBusArrivals(rid) > 1) {
    writeArrivalRecord(rid);
  }

  ba->updateNextArrival();
  pv->unsetStatus(STATUS_MANDATORY);
}
*/

int 
TS_BusStop::scheduleArrivals(int rc, float sa)
{
  float stime = startTime(rc);
  float ia = intervalA(rc);
  float ib = intervalB(rc);
  float ic = intervalC(rc);
  float pk = peakRate(rc)/3600.;  // pass/sec
  float la = lastArrivalTime(rc);
  int c1 = lastBusCycle(rc);
  int i1 = lastBusInterval(rc);
  float t1 = lastBusTime(rc);

  float ctime = theSimulationClock->currentTime();
  if (isControlEnabled(rc) && sa > ctime) ctime = sa;
  float clength = ia + ib + ic;
  
  int c2 = int((ctime - stime)/(clength));
  int i2 = 0;
  float telapse = fmod((ctime-stime), clength);
  float t2 = 0.;

  // Dan: determine current bus' interval, i2, and time
  // elapsed in interval, t2

  if (telapse > (ia + ib)) {
    i2 = 3;
    t2 = telapse - (ia + ib);
  } else if (telapse > ia) {
    i2 = 2;
    t2 = telapse - ia;
  } else {
    i2 = 1;
    t2 = telapse;
  }

  /* Dan: If first bus, calculate generate passengers based on
     time elapsed since (bus stop attribute) starttime_ */

  int npass = 0;    // number of passengers arrived since last bus on route
  int tempna = 0;
  int tempnb = 0;
  float wtime = 0.; // mean passenger waiting time
  float tempwa = 0; 
  float tempwb = 0; 

  if (la < 0.5) {  // first bus on route to arrive at stop
    tempna = (int) floor((c2 *(0.5*ib*pk)) + 0.5); // floor(x+0.5) rounds to nearest integer
    if (i2 == 1) {
      npass += tempna;
      if (tempna > 0) wtime = (0.3333 * ib) + ic + t2;
    }
    else if (i2 == 2) {
      npass += tempna;
      tempnb = (int) floor((0.5*(t2/ib)*pk*t2) + 0.5); // floor(x+0.5) rounds to nearest integer
      npass += tempnb;
      if (tempna > 0) tempwa = (0.3333 * ib) + ic + ia + t2;
      tempwb = 0.333 * t2;
      if ((tempna + tempnb) > 0) wtime = (tempna*tempwa + tempnb*tempwb)/(tempna + tempnb);
    }
    else {    // i2 = 3
      npass += tempna;
      tempnb = (int) floor((0.5*ib*pk) + 0.5); // floor(x+0.5) rounds to nearest integer
      npass += tempnb;
      if (tempna > 0) tempwa = (0.3333 * ib) + ic + ia + ib + t2;
      tempwb = 0.3333 * ib + t2;
      if ((tempna + tempnb) > 0) wtime = (tempna*tempwa + tempnb*tempwb)/(tempna + tempnb);
    }
  }
  else {
    
    /* Dan: This is not the first bus on this route to arrive
       at this stop, calculate the number of full cycles elapsed 
       since last bus on route */

    float tempt = 0.;  // time elapsed in this bus' cycle
    if (i2 == 1) tempt = t2;
    else if (i2 == 2 ) tempt = ia + t2;
    else tempt = ia + ib + t2;   // i2 = 3

    int cycles = c2 - c1;

    if (cycles > 1) {
      tempna = (cycles - 1) * ((int) floor((0.5*ib*pk) + 0.5)); // floor(x+0.5) rounds to nearest integer

      // Dan: the following calculation of wait time assumes that not
      // more than 4 schedule cycles will pass between consecutive buses

      if (cycles == 4) {
        tempwa = 0.3333*(0.3333*ib + ic + 2*clength + tempt
                         + 0.3333*ib + ic + clength + tempt
                         + 0.3333*ib + ic + tempt);
      }
      if (cycles == 3) {
	tempwa = 0.5*(0.3333*ib + ic + clength + tempt
                      + 0.3333*ib + ic + tempt);
      }
      if (cycles == 2) {
	tempwa = 0.3333*ib + ic + tempt;
      }

      npass += tempna;
      wtime += tempwa;
    }

    tempna = 0;
    tempwa = 0.;

    if (cycles > 0) {
      
      // Dan: add "split" left by last bus:

      if (i1 == 1) {
        tempna = (int) floor((0.5*ib*pk) + 0.5); // floor(x+0.5) rounds to nearest integer
        tempwa = 0.3333*ib + ic + (cycles - 1) * clength + tempt;
      }
      else if (i1 == 2) {
                             // floor(x+0.5) rounds to nearest integer
	tempna = (int) floor(((ib - t1)*0.5*((t1/ib)*pk + pk)) + 0.5);
	tempwa = (pk*((2*t1)/ib) + 1)/(3*((t1/ib) + 1)) + ic + (cycles - 1) * clength + tempt;
      }
      else {   // i1 = 3
	tempna = 0;  // last bus picked up all the passengers in its cycle
	tempwa = 0;
      }
    
      if ((npass + tempna) > 0) wtime = (wtime*npass + tempna*tempwa)/(npass + tempna);
      npass += tempna;
      tempna = 0;
      tempwa = 0.;

      // Dan: add "split" picked up by this bus:

      if (i2 == 1) {
	tempna = 0;
	tempwa = 0;
      }
      else if (i2 == 2) {
        tempna = (int) floor((0.5*t2*(t2/ib)*pk) + 0.5); // floor(x+0.5) rounds to nearest integer
        tempwa = 0.3333*t2;
      }
      else {   // i2 = 3
        tempna = (int) floor((0.5*ib*pk) + 0.5); // floor(x+0.5) rounds to nearest integer
        tempwa = 0.3333*ib + t2;
      }

      if ((npass + tempna) > 0) wtime = (wtime*npass + tempna*tempwa)/(npass + tempna);
      npass += tempna;
      tempna = 0;
      tempwa = 0.;
    }

    if (cycles == 0) {
      
      // Dan: two buses arrived within the same schedule cycle

      if (i1 == i2) {
	if (i2 == 1) {
	  tempna = 0;
	  tempwa = 0.;
	}
	else if (i2 == 2) {
                                // floor(x+0.5) rounds to nearest integer
	  tempna = (int) floor((0.5*t2*(t2/ib)*pk) + 0.5) - (int) floor((0.5*t1*(t1/ib)*pk) + 0.5);
 	  tempwa = ( ((t1*t1/ib*ib)*pk) + ((t1*t2/ib*ib)*pk) + ((t2*t2/ib*ib)*pk) )/
	                 ( 3*(t1/ib + t2/ib) );
	}
	else {  // i2 = 3
	  tempna = tempna = 0;
	  tempwa = 0.;
	}
      }
      else if ((i2 - i1) == 1) {
	if (i2 == 2) {
	  tempna = (int) floor((0.5*t2*(t2/ib)*pk) + 0.5); // floor(x+0.5) rounds to nearest integer
	  tempwa = 0.3333*t2;
	}
	else {  // i2 = 3
	  tempna = (int) floor(((ib-t1)*0.5*((t1/ib)*pk + pk)) + 0.5); // floor(x+0.5) rounds to nearest integer
	  tempwa = (pk*((2*t1/ib) + 1))/(3*((t1/ib) + 1)) + t2;
	}
      }
      else {  // i2 - i1 = 2
	tempna = (int) floor((0.5*ib*pk) + 0.5); // floor(x+0.5) rounds to nearest integer
	tempwa = 0.3333*ib + t2;
      }

      npass += tempna;
      wtime = tempwa;
    }
  }

  // Dan: account for added wait time of passengers left behind
  // by last bus

  int nl = passengersLeft(rc);

  if (la > 0.5) {  // a bus has arrived previous to this one
    float leftt = lastWaitTime(rc) + (ctime -la);
    if ((npass + nl) > 0) wtime = (npass*wtime + nl*leftt)/(npass + nl);
  }  // number of passengers left by last bus is added to boarding passengers
     // later in calcDelay

  updateLastBusArrival(rc, c2, i2, t2, wtime);
  return npass;
}
