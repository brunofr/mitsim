//-*-c++-*------------------------------------------------------------
// TS_IncidentDialog.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <iostream>
using namespace std;

#include <Xmt/Xmt.h>
#include <Xmt/Dialogs.h>
#include <Xmt/Create.h>
#include <Xmt/InputField.h>
#include <Xbae/Matrix.h>

#include <Tools/ToolKit.h>
#include <Tools/Math.h>
#include <Tools/SimulationClock.h>

#include <Xmw/XmwColor.h>
#include <DRN/DRN_KeyInteractor.h>

#include "TS_IncidentDialog.h"
#include "TS_Interface.h"
#include "TS_Link.h"
#include "TS_Segment.h"
#include "TS_Lane.h"
#include "TS_DrawingArea.h"
#include "TS_Network.h"
#include "TS_Parameter.h"
#include "TS_Incident.h"

#include <GRN/RN_CtrlStation.h>


TS_IncidentDialog::TS_IncidentDialog ( Widget parent, TS_Incident *p )
   : XmwDialogManager(parent, "incidentDialog", NULL, 0,
					  "okay", "cancel"),
	 incidents_(p)
{
   if (theBaseInterface) {
	  theBaseInterface->showBusyCursor();
   }

   XtVaSetValues(widget_,
				 XmNdialogStyle, XmDIALOG_APPLICATION_MODAL,
				 NULL);
  
   position_ = XmtNameToWidget(widget_, "position");
   assert(position_);

   const char *s;
   s = Str("%d / %.2f", p->segment()->code(), p->position());
   XtVaSetValues(position_,
				 RES_CONVERT(XmNlabelString, s), NULL);

   visibility_ = XmtNameToWidget(widget_, "visibility");
   assert(visibility_);
   
   s = Str("%.0f", p->visibility() / theParameter->lengthFactor());
   XmtInputFieldSetString(visibility_, s);

   lanes_ = XmtNameToWidget(widget_, "lanes");
   assert(lanes_);

   addCallback(lanes_, XmNenterCellCallback,
			   &TS_IncidentDialog::enter, NULL);

//    addCallback(lanes_, XmNleaveCellCallback,
// 			   &TS_IncidentDialog::leave, NULL);

   int i, j, nrows, ncols = 3;

   XtVaGetValues(lanes_, XmNrows, &nrows, NULL);
   XbaeMatrixDeleteRows(lanes_, 0, nrows);

   RN_CtrlStation *station = p->station();
   TS_Segment *ps = (TS_Segment*)station->segment();
   nrows = ps->nLanes();

   String *row_labels = new String [nrows];
   String *attrs = new String [nrows * ncols];

   float spd, delta;
   if (theDrawingArea->key_interactor()->lastKeySym() == XK_x) {
	   delta = (float) ps->speedLimit() / ps->nLanes();
	   spd = -delta;
   } else {						// XK_X
	   spd = ps->speedLimit();
	   delta = - spd / ps->nLanes();
   }

   for (i = 0; i < nrows; i ++) {
	  spd += delta ;
	  row_labels[i]  = Copy(ps->lane(i)->code());
	  attrs[i*3 + 0] = Copy(theSimulationClock->convertTime(p->startTime()));
	  attrs[i*3 + 1] = Copy((int)p->duration());
	  attrs[i*3 + 2] = Copy(spd, 0);
	  p ++;
   }

   XbaeMatrixAddRows(lanes_, 0, attrs, row_labels, NULL, nrows);

   XmwWidget::manage(widget_);

   for (i = 0; i < nrows; i ++) {
	  delete [] row_labels[i];
	  for (j = 0; j < ncols; j ++) {
		 delete [] attrs[i*ncols + j];
	  }
   }
   delete [] attrs;
   delete [] row_labels;

   if (!theBaseInterface) return;
   theBaseInterface->showDefaultCursor();
}


TS_IncidentDialog::~TS_IncidentDialog()
{
}


void TS_IncidentDialog::enter( Widget /* w */, XtPointer,
							   XtPointer data )
{
    XbaeMatrixEnterCellCallbackStruct *p;
    p = (XbaeMatrixEnterCellCallbackStruct *) data;

   p->select_text = True;

//    cout << "Enter Row" << p->row << endl;
}

// void TS_IncidentDialog::leave( Widget w, XtPointer, XtPointer data )
// {
//    XbaeMatrixLeaveCellCallbackStruct *p;
//    p = (XbaeMatrixLeaveCellCallbackStruct *) data;

//    if (!ToolKit::comp(p->value, XbaeMatrixGetCell(w, p->row, p->column))) {
// 	  return;
//    }
// }

// These overload the functions in base class

void TS_IncidentDialog::cancel(Widget, XtPointer, XtPointer)
{
   unmanage();
   if (station_) {
	  delete station_;
   } else {
	  cout << "Sorry. Remove feature not implemented yet!"
		   << endl;
   }

   // delete [] incidents_;
   // Above line dumps core, very stange!
}


void TS_IncidentDialog::okay(Widget, XtPointer, XtPointer)
{
   RN_CtrlStation *s = incidents_->station();
   RN_Segment *ps = s->segment();
   const char* str = XmtInputFieldGetString(visibility_);
   s->visibility(atof(str) * theParameter->lengthFactor());
   SimulationClock *clk = theSimulationClock;
   int n = ps->nLanes();

   XbaeMatrixCommitEdit(lanes_, False);

   for (int i = 0; i < n; i ++) {
     TS_Incident &inc = incidents_[i] ;
     str = XbaeMatrixGetCell(lanes_, i, 0);
     if (strchr(str, ':')) {
       inc.startTime_ = clk->convertTime(str);
     } else {
       inc.startTime_ = atof(str);
     }
     str = XbaeMatrixGetCell(lanes_, i, 1);
     if (strchr(str, ':')) {
       inc.duration_ = clk->convertTime(str);
     } else {
       inc.duration_ = atof(str);
     }
     str = XbaeMatrixGetCell(lanes_, i, 2);
     inc.speedLimit_ = atof(str) * theParameter->speedFactor();
     
     if (inc.speedLimit_ > SPEED_EPSILON) {
       inc.clearState(INCI_STOP) ;
       inc.setState(INCI_MINOR) ;
       inc.severity_ = 0.5 ;
     } else {
       inc.setState(INCI_STOP|INCI_MAJOR);
       inc.severity_ = 1.0 ;
     }

     if (station_) {	// existing station
       inc.calcGeometricData();
       inc.addIntoNetwork();
       tsNetwork()->incidents().push_back(&inc);
     }

     // Add new pointer into the list
   }

   if (station_) {				// station_ should be same as s
	  station_->addIntoNetwork();
	  station_->link()->assignCtrlListInSegments();
	  ((TS_Link *)station_->link())->vehicleUpdateNextCtrl(station_);
   }

   unmanage();
}

void TS_IncidentDialog::help(Widget, XtPointer, XtPointer)
{
   theInterface->openUrl("incident", "mitsim.html");
}


void TS_IncidentDialog::postAndWait(RN_CtrlStation *s)
{
   if (s) {
	  activate(cancel_);
   } else {
	  deactivate(cancel_);
   }
   station_ = s;
   manage(widget_);
   ready();
}


void TS_IncidentDialog::createAndPost(TS_Incident *inc, RN_CtrlStation *s)
{   
   static TS_IncidentDialog *dialog = NULL;
   if (dialog) delete dialog;
   dialog = new TS_IncidentDialog(theInterface->widget(), inc);
   dialog->postAndWait(s);
}

#endif
