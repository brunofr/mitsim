//-*-c++-*------------------------------------------------------------
// NAME: Additional help info for mitsim
// AUTH: Qi Yang
// FILE: TS_Help.C
// DATE: Tue May  7 16:55:16 1996
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <DRN/DRN_Help.h>
#include "TS_Help.h"

void CreateHelpMsg()
{
  static char *msg =
    "  p -> Compare alternative paths\n"
    "  v -> Label vehicles" ;

  aux_help_msg = msg;
}

#endif


