//-*-c++-*------------------------------------------------------------
// NAME: Microscopic Traffic Simulator
// AUTH: Qi Yang
// FILE: TS_Engine.C
// DATE: Fri Nov 17 10:12:59 1995
//--------------------------------------------------------------------

#include <cstring>
#include <new>
using namespace std;

#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <Tools/SimulationClock.h>
#include <Tools/Options.h>
#include <Tools/Math.h>
#include <Tools/Random.h>
#include <Tools/GenericVariable.h> //IEM(Apr24)

#include <GRN/RN_DynamicRoute.h>
#include <GRN/RN_PathTable.h>
#include <GRN/RN_Path.h>
#include <GRN/RN_BusRouteTable.h>
#include <GRN/RN_BusRoute.h>
#include <GRN/RN_BusScheduleTable.h>
#include <GRN/RN_BusSchedule.h>
#include <GRN/RN_BusRunTable.h>
#include <GRN/RN_BusRun.h>
#include <GRN/RN_BusStopPrmTable.h>
#include <GRN/RN_BusCommPrmTable.h>
#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>

#include "TS_Communicator.h"
#include "TS_Engine.h"
#include "TS_Setup.h"
#include "TS_FileManager.h"
#include "TS_Vehicle.h"
#include "TS_Status.h"
#include "TS_QueueHead.h"
#include "TS_Segment.h"
#include "TS_ODTable.h"
#include "TS_ODCell.h"
#include "TS_VehicleTable.h"
#include "TS_BusAssignmentTable.h"
#include "TS_Incident.h"
#include "TS_Exception.h"
#include "TS_Parameter.h"
#include "TS_MoeCollector.h"
#include "TS_Sensor.h"
#include "TS_Network.h"
#include "TS_Link.h"

#ifdef INTERNAL_GUI
#include <Xmt/Xmt.h>
#include <Xmt/Dialogs.h>
#include <DRN/DRN_DrawingArea.h>
#include <DRN/DRN_MapFeature.h>
#include <DRN/DRN_ViewMarker.h>
#include <GDS/GDS_Glyph.h>
#include "TS_Menu.h"
#include "TS_Interface.h"
#include "TS_Symbols.h"
#include "TS_DrawableVehicle.h"
#endif

TS_Engine * theEngine = NULL;

TS_Engine::TS_Engine()
  : SimulationEngine(),
	depRecordStepSize_(5),
	trajectoryStepSize_(1),
	pathStepSize_(ONE_DAY),
	segmentStatisticsSamplingStepSize_(60.0),
	segmentStatisticsReportStepSize_(300.0),
	stateStepSize_(60.0),
	survPointStepSize_(300.0),
	survAreaStepSize_(60.0),
	microStepSize_(0.1),
	macroStepSize_(60.0),
	batchStepSize_(60.0),
	dumpTime_(ONE_DAY),
        sensorDumpNeeded_(0)
{
  theException = new TS_Exception("MITSIM");

  theEngine = this;
  if (!theSimulationClock) {
	theSimulationClock = new SimulationClock;
  }

  theCommunicator = new TS_Communicator(MSG_TAG_MITSIM);

  theSimulationClock->init(28800.0, 32400.0, 0.1);

  // Default parameter filename

  if (theParameter->name()) free(theParameter->name());
  *theParameter->nameptr() = strdup("paralib.dat");
}


const char* TS_Engine::ext()
{
  return ".mitsim";
}


int
TS_Engine::loadMasterFile()
{
  if (SimulationEngine::loadMasterFile() != 0) {
	char *msg;
	msg = StrCopy("Error:: Cannot find the master file <%s>.", master());
#ifdef INTERNAL_GUI
	Boolean ok;
	XmtAskForBoolean(theInterface->widget(),	// widget
					 "masterNoFound",  // query_name for customize
					 msg, // prompt_default
					 "Continue",	// yes_default
					 "Quit",	// no_default
					 NULL,	// cancel_default
					 XmtNoButton, // default_button
					 0,		// icon_type_default
					 0,		// show_cancel_button
					 &ok,		// value_return
					 "Check if the file exist.");	// help_text_default
	if (!ok) {
	  quit(STATE_ERROR_QUIT);
	}
#else
	cerr << msg << endl;
	quit(STATE_ERROR_QUIT);
#endif
    StrFree(msg);
	return 1;
  }

#ifdef INTERNAL_GUI
  theInterface->showBusyCursor ();
#endif
  theFileManager->readMasterFile();
  state_ = STATE_OK;

  // if (::GetUserName()) theException->exit(1);
  // ::GetUserName();

#ifdef INTERNAL_GUI
  theInterface->tellLoadingIsDone();
  theInterface->showDefaultCursor ();
#endif

  return 0;
}


// This function is called every time a new master file is loaded.
// It return 0 if no error.

int TS_Engine::loadSimulationFiles()
{
  if (state_ == STATE_NOT_STARTED) {
	if (loadMasterFile() != 0) return 1;
  }

  int err_no = ToolKit::makedirs();
  if (err_no) {
#ifdef INTERNAL_GUI
	theInterface->msgSet("Cannot create directory ");
	if (err_no & 0x1) theInterface->msgAppend(ToolKit::outdir()) ;
	else if (err_no & 0x2) theInterface->msgAppend(ToolKit::workdir()) ;
#else
	theException->exit(STATE_ERROR_QUIT) ;
#endif
	return 1;
  }

#ifdef INTERNAL_GUI
  theInterface->showBusyCursor ();
#endif

  Random::create(4);

  //::exit(1);

  theStatus->openLogFile();

  ::ParseParameters();

  ::ParseNetwork();

  ::ParsePathTables();

  RN_DynamicRoute::parseTravelTimeTables('i');

  ::ParseODTripTables();

  ::ParseVehicleTables();



  //Dan - adding call to parse function for bus routes, schedules, assignments

  ::ParseBusRoutes();

  ::ParseBusSchedules();

  ::ParseBusRuns();

  ::ParseBusAssignments();

  ::ParseBusStopPrms();

  ::ParseBusCommPrms();

  ::SetupScenarios();

  ::SetupMiscellaneous();

  if (theMoeCollector) {
	theMoeCollector->findSelectedPairs();
  }

  for(int tCounter = 0; tCounter < nSurvPointStepSizes(); tCounter++){
    tsNetwork()->resetSensorReadings(tCounter); //IEM(Jun20) stuck this in a for loop so that it will do all of the types
  }
  tsNetwork()->initializeSegmentStatistics();
  tsNetwork()->initializeLinkStatistics();

  start();

#ifdef INTERNAL_GUI
  theInterface->setTitle(theFileManager->title());
  theDrawingArea->setState(DRN_DrawingArea::REDRAW |
						   DRN_DrawingArea::NETWORK_LOADED);
  theDrawingArea->redraw();
  theInterface->showDefaultCursor ();
  theInterface->updateButtons();
  theInterface->handlePendingEvents();
#endif

  // Make connection to other sibling modules if they exist and
  // sending timing data to simlab.

  if (theCommunicator->isSimlabConnected()) {
#ifdef INTERNAL_GUI
	const char *msg = XmtLocalize2(theInterface->widget(),
								   "Waiting data from other module(s).", "prompt", "waiting");
	theInterface->msgSet(msg);
#endif	  
	theCommunicator->makeFriends();
#ifdef INTERNAL_GUI
	theInterface->msgClear();
#endif
  }

  return 0;
}


// This procedure starts the simulation loops.

void TS_Engine::run()
{
  if (canStart()) {
	loadMasterFile();
  }

#ifdef INTERNAL_GUI

  // In graphical mode, the simulation controlled by event loop

  theInterface->mainloop();

#else  // batch mode

  loadSimulationFiles();

  while (state_ >= 0 && (receiveMessages() > 0 ||
						 isWaiting() &&
						 receiveMessages(NO_MSG_WAITING_TIME) >= 0 ||
						 simulationLoop() >= 0)) {
	;
  }
#endif // !INTERNAL_GUI
}


void
TS_Engine::quit(int state)
{
  // Report DLC count
#ifdef COUNT_DLC
  const char *dlcfile = ToolKit::outfile("dlc.txt") ;
  ofstream os(dlcfile) ;
  int n = theNetwork->nLinks() ;
  for (int i = 0; i < n ; i ++) {
	TS_Link *pl = tsNetwork()->tsLink(i) ;
	os << pl->code() << "\t" << pl->num_dlc << endl ;
  }
  os.close() ;
  if (ToolKit::verbose()) {
	cout << "DLC count saved in " << dlcfile << "." << endl;
  }
#endif

  // Report run information here.

  theStatus->report();

  SimulationEngine::quit(state);

#ifdef INTERNAL_GUI
  if (theMenu->isSaveNeeded()) {
	Boolean yes;
	XmtAskForBoolean(theInterface->widget(),
					 "isSaveNeeded", // reource querey name
					 "Save changes?", // defaul prompt
					 "Save",	// yes default
					 "Discard",	// no default
					 "Cancel",	// cancel default
					 XmtYesButton, // default button
					 0,	// default icon type
					 False, // show cancel button
					 &yes,
					 NULL);
	if (yes) {
	  theMenu->save();
	}
  }
#endif

  theFileManager->closeOutputFiles();
  theStatus->closeLogFile();

  if (state_ == STATE_DONE) {
	theException->done(0);
  } else {
	theException->exit(0);
  }
}

// This function is called by DRN_Interface::mainloop() in graphics
// mode or TS_Engine:run().  A calling of this function represents a
// simulation of one time interval.  Graphics related functions should
// be placed inside INTERNAL_GUI block.

int
TS_Engine::simulationLoop()
{
  static int first_entry = 1;

  static double laneSpeedTime = -DBL_INF;
  static double trajectoryTime = DBL_INF;
  static double depRecordTime = DBL_INF;
#ifdef INTERNAL_GUI
  static double microTime;
#endif
  static double macroTime;
  static double batchTime;
  static double segmentStatisticsSamplingTime = DBL_INF;
  static double segmentStatisticsReportTime = DBL_INF;
  static double stateTime = DBL_INF;
  static double survPointTime = DBL_INF;
  static vector <double> survPointTimes_(nSurvPointStepSizes(), DBL_INF);
               //IEM(May2) Interval sizes, as parsed out of master.mitsim
	       //          0 is default, >=1 additional
  static double survAreaTime = DBL_INF;
  static double pathTime = DBL_INF;

  TS_Network * tsNetwork = (TS_Network*) theNetwork;

  double now = theSimulationClock->currentTime();

  if (first_entry) {

	first_entry = 0;

	theParameter->checkVisibility();

	batchTime = now;
	macroTime = now;

#ifdef INTERNAL_GUI
	microTime = now;
#endif

	if (chosenOutput(OUTPUT_SEGMENT_STATISTICS)) {
	   	segmentStatisticsSamplingTime = now + segmentStatisticsSamplingStepSize_;
		segmentStatisticsReportTime = now + segmentStatisticsReportStepSize_;
		tsNetwork->resetSegmentStatistics();
	}
   
	survPointTime = now + survPointStepSize_;
        //IEM(May2) Initialize all aggregation reset times
	for (int tCounter = 0; tCounter < nSurvPointStepSizes(); tCounter++) {
          survPointTimes_[tCounter] = now + survPointStepSizes_[tCounter];
	}
	survAreaTime = now + survAreaStepSize_; 
	pathTime = now + pathStepSize_;

	resetBreakPoint(now);

	// This block is called only once just before the simulation get
	// realy started.

	// MOE specification is parsed here because it may need the last
	// moment selection of the output from menu before the
	// simulation get started.

	::ParseMOESpecifications();

	theFileManager->openOutputFiles();
      
	if (theCommunicator->isTmsConnected()) {
	  if (pathTime < theSimulationClock->stopTime()) {
		cerr << "Warning:: [Path Step Size] may be used incorrectly."
			 << endl;
	  }
	}

	if (theEngine->chosenOutput(OUTPUT_VEHICLE_DEP_RECORDS)) {
	  depRecordTime = now;
	} else {
	  depRecordTime = DBL_INF;
	}

	if (theEngine->chosenOutput(OUTPUT_VEHICLE_TRAJECTORY)) {
	  trajectoryTime = now;
	} else {
	  trajectoryTime = DBL_INF;
	}

	const char * filename = theFileManager->stateDumpFile();
	filename = ToolKit::infile(filename);
	tsNetwork->loadInitialState(filename);

	if (chosenOutput(OUTPUT_STATE_3D)) {
	  tsNetwork->save_3d_state(0, 0, theFileManager->state3d());
	  stateTime = now + stateStepSize_;
	} else {
	  stateTime = DBL_INF;
	}
  }

#ifdef INTERNAL_GUI
  theInterface->handlePendingEvents();
#endif
  
  if (now >= theSimulationClock->masterTime()) {
	return (state_ = STATE_WAITING);
  }

#ifdef INTERNAL_GUI
  if (isTimeToBreak(now)) {

	const char *msg;
	msg = Str("Break point at %s",
			  theSimulationClock->currentStringTime());

	XmtDisplayWarningAndAsk(theInterface->widget(), "bpsDialog",
							msg,
							"Continue", "Quit", XmtYesButton,
							"\nClick @fBContinue@fR to proceed or @fBQuit@fB to\n"
							"Terminate the simulation");

	nextBreakPoint(now + theSimulationClock->stepSize());
  }
#endif

  if (now >= depRecordTime) {
	TS_Vehicle::nextBlockOfDepartureRecords();
	depRecordTime = now + depRecordStepSize_;
  }

  if (now >= pathTime) {

	// Update info network, calc shhortest path, and update travel
	// time in path table

	theGuidedRoute->updatePathTable(NULL, theParameter->pathAlpha());

	// Call path choice or switching model for each guided vehicles.
	// This is temporary and assumed that info in broadcased to
	// every guided vehicle at the same time.  I used it for getting
	// evaluation result for the systems that are based on
	// prevailing traffic conditions.

	if (isSpFlag(INFO_FLAG_UPDATE_PATHS)) {
	  tsNetwork->guidedVehiclesUpdatePaths();
	}

	pathTime = now + pathStepSize_;
  }

  if (now >= batchTime) {
	theStatus->print();
	batchTime = now + batchStepSize_;
  }

  // Reqort all queues at selected positions

  if (theEngine->chosenOutput(OUTPUT_QUEUE_STATISTICS)) {
	if (TS_QueueHead::nextTime() <= now) {
	  TS_QueueHead::reportQueues();
	  TS_QueueHead::scheduleNextTime();
	}
  }
  
  if (now >= macroTime) {

	tsNetwork->calcSegmentData();

#ifdef INTERNAL_GUI

	tsNetwork->updateRoads(theDrawingArea);

#endif /* !INTERNAL_GUI */

	macroTime = now + macroStepSize_;
  }

#ifdef INTERNAL_GUI

  if (now >= microTime) {
	tsNetwork->drawSensors(theDrawingArea);
	tsNetwork->drawVehicles(theDrawingArea);
	microTime = now + microStepSize_;
  }

  theInterface->handlePendingEvents();

  theStatus->showStatus();

#endif /* !INTERNAL_GUI */

  // check occurance and clearance of incidents

  TS_Incident::checkIncidents();
	
  /*
  // report readings of segment stats
  if ( now >= segmentStatisticsReportTime)
  {
      tsNetwork->outputSegmentStatistics();
      tsNetwork->resetSegmentStatistics();
      segmentStatisticsReportTime = now + segmentStatisticsReportStepSize_;
  }

  */
 

  // Report network state

  if (now >= stateTime) {
	static int tm = 0;
	tm ++;
 
	if (theCommunicator->isTmsConnected()) {
	  tsNetwork->sendSensorReadings(theCommunicator->tms(),
					SENSOR_AGGREGATE, 0); //IEM(Jun29) 0 is default intervaltype
	}
	tsNetwork->save_3d_state(0, tm);
	//	if (chosenOutput(OUTPUT_SEGMENT_STATISTICS)) {
	//  tsNetwork->outputSegmentStatistics();
	//	}
	// tsNetwork->resetSegmentStatistics();
	stateTime = now + stateStepSize_;
  }

  // Report readings of point sensor devices //IEM(Jun15) for default-intervalled sensors

  if (now >= survPointTime && theNetwork->nSensors() > 0) { //IEM(Jul25) Second condition prevents segfaults if no sensors

	if (theCommunicator->isTmsConnected()) {
	  tsNetwork->sendSensorReadings(theCommunicator->tms(),
							SENSOR_AGGREGATE, 0);
						//IEM(Jun15) 0 is default intervaltype
	}
     
	if (chosenOutput(OUTPUT_SENSOR_READINGS)) { // "_READINGS" added by Angus)
	  tsNetwork->writeSensorReadings(0); //IEM(Jun15) 0 is default intervaltype
	}

	// dump the state of the sensors to a file
	// then tell TMS that there is a new file available
	if( sensorDumpNeeded_ && theCommunicator->isTmsConnected() )
	{
	  tsNetwork->dumpSensorReadings(survPointTime - survPointStepSize_, survPointTime, 0);
							//IEM(Jun15) 0 is default intervaltype
	  theCommunicator->tms() << MSG_NEW_SURVEILLANCE_READY 
				 << survPointTime - survPointStepSize_
				 << survPointTime;
	  theCommunicator->tms().flush(SEND_IMMEDIATELY);
	}
	tsNetwork->resetSensorReadings(0); //IEM(Jun15) 0 is default intervaltype
	survPointTime = now + survPointStepSize_;
	survPointTimes_[0] = now + survPointStepSizes_[0]; //IEM(Jun15)Updating in vector just in case
  }

  //IEM(May2) Duplicate above functionality for vector of survPointTimes_
  //IEM(Jun15) Intervaltype 0 taken care of above, so start with 1.
  for (int tCounter = 1; tCounter < nSurvPointStepSizes(); tCounter++) {
    if (now >= survPointTimes_[tCounter]) {

	  if (theCommunicator->isTmsConnected()) {
	    tsNetwork->sendSensorReadings(theCommunicator->tms(),
									SENSOR_AGGREGATE, tCounter);
       	  }
     
	  if (chosenOutput(OUTPUT_SENSOR_READINGS) && sensorOutputFlags_[tCounter - 1]) { // "_READINGS" added by Angus)
	    tsNetwork->writeSensorReadings(tCounter);                     //IEM(Jun19) check sensorOutputFlags
	  }

	  //IEM(Jun21) No Dumping for the additional sensors.

	  tsNetwork->resetSensorReadings(tCounter);
	  survPointTimes_[tCounter] = now + survPointStepSizes_[tCounter];
    }
  }


  // Report readings of area sensor devices

  if (now >= survAreaTime) {
	if (theCommunicator->isTmsConnected()) {
	  tsNetwork->sendSensorReadings(theCommunicator->tms(), 
					SENSOR_SNAPSHOT, 0); //IEM(Jun20) added 0 for default
	}
	survAreaTime = now + survAreaStepSize_;
  }
  

  // UPDATE DEMAND

  if (theODTable) {

	// Update OD trip tables

	theODTable->read();

	// Create vehicles based on trip tables
     
	theODTable->emitVehicles();
  }

  if (theVehicleTable) {

	// Read vehicles from a file if it exists.

	theVehicleTable->read();
  }

  //Dan - creating bus assignments if they exist

  if (theBusAssignmentTable && (theBusAssignmentTable->BRTOnly() > 0)) {

	// Read buses from a file if it exists.

	theBusAssignmentTable->read();
  }

  if (theBusStopPrmTable) {

	// Read bus stop parameters from a file if it exists.

	theBusStopPrmTable->read();
  }

  if (theBusCommPrmTable) {

	// Read bus sensing parameters from a file if it exists.

	theBusCommPrmTable->read();
  }

  // UPDATE LANE SPEED (USED IN LANE CHANGE MODEL)

  if (now >= laneSpeedTime) {
	tsNetwork->calcLaneSpeeds();
	laneSpeedTime = now + theParameter->laneSpeedStepSize();
  }

  // ENTER VEHICLES INTO THE NETWORK

  // Move vehicles from vitual queue into the network if they could
  // enter the network at present time.  To send static vehicle data
  // from the MITSIM, once per vehicle.

  tsNetwork->enterVehiclesIntoNetwork();

  // CHANGING VEHICLE STATE

  // Loop each vehicles currently in the network to check for lane
  // changes, and calculate acceleration rate.

  tsNetwork->calcVehicleStates();
  
  // MOVE VEHICLE OVER THE NETWORK OR TAKE OUT VEHICLE FROM THE
  // NETWORK

  // Actually move the vehicle based on its current speed and
  // acceleration computed in the previous step.

#ifdef INTERNAL_GUI
  theInterface->handlePendingEvents();
#endif
  
  tsNetwork->moveVehicles();

  // CALCULATE SEGMENT STATISTICAL

  // Segment statistics are calculated by sampling at fixed frequency
  // for all the vehicles in the network.

  if (now >= segmentStatisticsSamplingTime) 
  {
      tsNetwork->updateSegmentStatistics();
      segmentStatisticsSamplingTime = 
		  now + segmentStatisticsSamplingStepSize_;
  }

  
 // report readings of segment stats
  if ( now >= segmentStatisticsReportTime)
  {
      tsNetwork->outputSegmentStatistics();
      tsNetwork->resetSegmentStatistics();
      segmentStatisticsReportTime = now + segmentStatisticsReportStepSize_;
  }


  // Dump the network state into a file
   
  if (now >= dumpTime_) {

	char *filename = theEngine->createFilename("dmp");
	tsNetwork->dumpNetworkState(filename);
	delete [] filename;
   
	theCommunicator->tms() << MSG_STATE_DONE;
	theCommunicator->tms().flush(SEND_IMMEDIATELY);

	dumpTime_ = DBL_INF;
  }

  if (now >= trajectoryTime) {
	tsNetwork->dumpTrajectoryRecords();
	trajectoryTime += trajectoryStepSize_;
  }

  // Advance the clock

  theSimulationClock->advance();

#ifdef INTERNAL_GUI
  theInterface->handlePendingEvents();
#endif

  // Tell the master controller that this cycle is done

  theCommunicator->sendNextTimeToSimlab();

  if (theCommunicator->isSimlabConnected()) {
	return (state_ = STATE_WAITING);
  } else if (now >= theSimulationClock->stopTime()) {
	return (state_ = STATE_DONE);
  } else {
	return (state_ = STATE_OK);
  }
}

// Process the messages received from other processes

int
TS_Engine::receiveMessages(double sec)
{
  if (!theCommunicator->isSimlabConnected()) return 0;
  else return theCommunicator->receiveMessages(sec);
}

// Save the master file

void TS_Engine::save(ostream &os)
{
  os << "/* " << endl
	 << " * MITSIM master file" << endl
	 << " */" << endl << endl;

  os << "[Title] = \"" << theFileManager->title() << "\"" << endl
	 << endl;

  os << "[Default Parameter Directory]     = \""
	 << NoNull(ToolKit::paradir())
	 << "\"" << endl;
	 
  os << "[Input Directory]                 = \""
	 << NoNull(ToolKit::indir())
	 << "\"" << endl;

  os << "[Output Directory]                = \""
	 << NoNull(ToolKit::outdir())
	 << "\"" << endl;

  os << "[Working Directory]               = \""
	 << NoNull(ToolKit::workdir())
	 << "\"" << endl;

  os << endl;

  os << "[Parameter File]                  = \""
	 << NoNull(theParameter->name())
	 << "\"" << endl;

  os << "[Network Database File]           = \""
	 << NoNull(RoadNetwork::name())
	 << "\"" << endl;

  os << "[Trip Table File]                 = \""
	 << NoNull(OD_Table::name())
	 << "\"" << endl;
	 
  os << "[Vehicle Table File]              = \""
	 << NoNull(VehicleTable::name())
	 << "\"" << endl;

  //Dan - adding for bus route, schedule, assignment files

  os << "[Transit Network File]              = \""
	 << NoNull(RN_BusRouteTable::name())
	 << "\"" << endl;

  os << "[Bus Schedule File]              = \""
	 << NoNull(RN_BusScheduleTable::name())
	 << "\"" << endl;

  os << "[Bus Run File]              = \""
	 << NoNull(RN_BusRunTable::name())
	 << "\"" << endl;

  os << "[Bus Assignment File]              = \""
	 << NoNull(BusAssignmentTable::name())
	 << "\"" << endl;

  os << "[Transit Demand File]              = \""
	 << NoNull(RN_BusStopPrmTable::name())
	 << "\"" << endl;

  os << "[Bus Surveillance File]              = \""
	 << NoNull(RN_BusCommPrmTable::name())
	 << "\"" << endl;

  os << "[State Dump File]                 = \""
	 << NoNull(theFileManager->stateDumpFile())
	 << "\"" << endl;

#ifdef INTERNAL_GUI
  os << "[GDS Files]                       = {" << endl
	 << "%\tFilename \tMinScale \tMaxScale" << endl;
  if (theMapFeatures) {
	GDS_Glyph *glyph;
	for (int i = 0; i < theMapFeatures->nGlyphs(); i ++) {
	  glyph = theMapFeatures->glyph(i);
	  os << "\t\"" << glyph->name() << "\""
		 << "\t" << glyph->minScale()
		 << "\t" << glyph->maxScale()
		 << endl;
	}
  }
  os << "}" << endl;
#endif

  os << "[Link Travel Times Input File]    = {" << endl
	 << "\t\"" << NoNull(RN_DynamicRoute::file(0))
	 << "\"" << "\t# Historical travel time" << endl
	 << "\t\"" << NoNull(RN_DynamicRoute::file(1))
	 << "\"" << "\t# Updated travel time" << endl
	 << "\t0x" << hex << theSpFlag << dec << "\t# SP flags" << endl
	 << "\t% 0x001 Time variant path calculation" << endl
	 << "\t% 0x002 Calulate shortest path peridically" << endl
	 << "\t% 0x004 Update path table travel time peridically" << endl
	 << "\t% 0x008 Use existing (cached) shortest path table" << endl
	 << "\t% 0x100 Updated travel time used for pretrip plan" << endl
	 << "\t% 0x200 Receives updated travel time at beacon" << endl
	 << "}" << endl;

  os << "[Incident File]                   = \""
	 << NoNull(TS_Incident::filename())
	 << "\"" << endl;

  os << "[Path Table File]                 = \""
	 << NoNull(RN_PathTable::name())
	 << "\"" << endl;

  os << "[MOE Specification File]          = \""		            
	 << NoNull(theFileManager->moeSpecFile_)
	 << "\"" << endl;

  os << "[MOE Output File]                 = \""
	 << NoNull(theFileManager->moeOutputFile_)
	 << "\"" << endl;

  os << "[Network State Tag]               = \""
	 << NoNull(theFileManager->state3d_)
	 << "\"" << endl;

  os << "[Segment Statistics File]         = \""
	 << NoNull(theFileManager->segmentStatisticsFile_)
	 << "\"" << endl;

  os << "[Segment Travel Times File]       = \""
	 << NoNull(theFileManager->segmentTravelTimesFile_)
	 << "\"" << endl;

  os << "[LinkFlowTravelTimes Output File] = \""
	 << NoNull(theFileManager->linkFlowTravelTimesFile_)
	 << "\"" << endl;

  os << "[Link Travel Times Output File]   = \""
	 << NoNull(theFileManager->linkTravelTimesFile_)
	 << "\"" << endl;

  os << "[Vehicle File]                    = \""
	 << NoNull(theFileManager->vehicleFile_)
	 << "\"" << endl;

  os << "[Vehicle Trajectory File]         = \""
	 << NoNull(theFileManager->trajectoryFile_)
	 << "\"" << endl;

  os << "[Transit Trajectory File]         = \""
	 << NoNull(theFileManager->transitTrajectoryFile_)
	 << "\"" << endl;

  os << "[Bus Stop Arrival File]         = \""
	 << NoNull(theFileManager->busStopArrivalFile_)
	 << "\"" << endl;

  os << "[Signal Priority File]         = \""
	 << NoNull(theFileManager->signalPriorityFile_)
	 << "\"" << endl;

  os << "[Vehicle Path Record File]        = \""
	 << NoNull(theFileManager->pathRecordFile_)
	 << "\"" << endl;

  os << "[Departure Record File]           = \""
	 << NoNull(theFileManager->depRecordFile_)
	 << "\"" << endl;

  os << "[Queue File]                      = \""
	 << NoNull(theFileManager->queueFile_)
	 << "\"" << endl;

  os << "[Point Sensor File]               = \""
	 << NoNull(theFileManager->sensorFile_)
	 << "\"" << endl;

  os << "[VRC Sensor File]                 = \""
	 << NoNull(theFileManager->sensorVRCFile_)
	 << "\"" << endl;

  os << "[Assignment Matrix File]          = \""
         << NoNull(theFileManager->assignmentMatrixFile_)  // Angus
	 << "\"" << endl;

 os << "[Lane Changing File]          = \""
         << NoNull(theFileManager->laneChangingFile_)  // tomer for LC
	 << "\"" << endl;

os << "[Lane Utilities File]          = \""
         << NoNull(theFileManager->laneUtilitiesFile_)  // gunwoo - this re-writes the master file at the end of the simulation
	 << "\"" << endl;



  if (TS_Sensor::harmonic()) {
	os << "[Harmonic Speed]                  = "
	   << TS_Sensor::harmonic() << endl;
  }

  os << endl;

  os << "[Start Time]                      = "
	 << theSimulationClock->startStringTime() << endl;

  os << "[Stop Time]                       = "
	 << theSimulationClock->stopStringTime() << endl;

  os << "[Step Size]                       = "
	 << Fix(theSimulationClock->stepSize(), 0.1) << endl;

  os << endl;

  os << "[Segment Data Sampling Step Size] = "
	 << Round(segmentStatisticsSamplingStepSize_) << endl;

  os << "[Segment Data Report Step Size] = "
	 << Round(segmentStatisticsReportStepSize_) << endl;

  os << "[Point Sensor Step Size]          = "
	 << Fix(survPointStepSize_, 0.1) << endl;

  //IEM(May2) Add additional interval sizes and output flags, if there are, to the saved data
  if (nSurvPointStepSizes() > 1) {
    os << "[Point Sensor Step Sizes]         = { ";
    //IEM(Jun15) Start with 1 since 0 is the default.
    for (int sCounter = 1; sCounter < nSurvPointStepSizes(); sCounter++) {
      os << Fix(survPointStepSizes_[sCounter], 0.1) << " ";
    }
    os << "}" << endl;

    os << "[Sensor Output Flags]             = { ";
    for (int fCounter = 0; fCounter < sensorOutputFlags_.size(); fCounter++) {
      os << sensorOutputFlags_[fCounter] << " ";
    }
    os << "}" << endl;
  }

  

  os << "[Area Sensor Step Size]           = "
	 << Fix(survAreaStepSize_, 0.1) << endl;

  os << "[Animatiion Step Size]            = "
	 << Fix(microStepSize_, 0.1) << endl;

  os << "[Segment Color Step Size]         = "
	 << Fix(macroStepSize_, 0.1) << endl;

  os << "[Console Message Step Size]       = "
	 << Fix(batchStepSize_, 0.1) << endl;

  if (theMoeCollector) {
	os << "[MOE Step Size]                   = "
	   << theMoeCollector->stepSize() << endl;

	os << "[MOE OD Pairs]                    = {" << endl;
	os << "\t"; theMoeCollector->printSelectedPairs(os);
	os << endl << "}" << endl;
  }

  os << endl;

  os << "[Output]   = 0x" << hex
	 << chosenOutput() << dec << endl
	 << "%   0x000001 = Vehicle log" << endl         
	 << "%   0x000002 = Sensor readings" << endl     
	 << "%   0x000004 = VRC readings " << endl           
         << "%   0x000008 = Assignment matrix output" << endl  // Angus        
	 << "%   0x000010 = Link travel times" << endl    
	 << "%   0x000020 = Segment travel times" << endl     
	 << "%   0x000040 = Segment statistics" << endl    
	 << "%   0x000080 = Queue statistics" << endl    
	 << "%   0x000100 = Travel time tables" << endl    
	 << "%   0x000200 = Vehicle path records" << endl
	 << "%   0x000400 = Vehicle departure record" << endl
	 << "%   0x000800 = Vehicle trajectories" << endl
	 << "%   0x001000 = Output rectangular text" << endl
	 << "%   0x002000 = No comments" << endl
	 << "%   0x004000 = Transit trajectories" << endl
	 << "%   0x008000 = Bus stop arrivals" << endl
	 << "%   0x010000 = State 3D" << endl
	 << "%   0x020000 = Priority events" << endl << endl
         << "%   0x040000 = Lane changing file" << endl // tomer for LC
         << "%   0x080000 = Lane utilities file" << endl; // gunwoo - legend for lane utilities output constant

#ifdef INTERNAL_GUI

  os << "[Segments] = "
	 << theSymbols()->segmentColorCode().value() << endl
	 << "%   0 = Link type" << endl
	 << "%   1 = Density" << endl
	 << "%   2 = Speed" << endl
	 << "%   3 = Flow" << endl << endl;

  os << "[Signals]  = 0x" << hex
	 << theDrnSymbols()->signalTypes().value() << dec << endl
	 << "%   0x01 = Traffic signals" << endl
	 << "%   0x02 = Portal signals" << endl
	 << "%   0x04 = Variable speed limit signs" << endl
	 << "%   0x08 = Variable message signs" << endl
	 << "%   0x10 = Lane use signs" << endl
	 << "%   0x20 = Ramp meters" << endl << endl;

  os << "[Sensor Types] = 0x" << hex
	 << theSymbols()->sensorTypes().value() << dec << endl
	 << "%   0x1 = Loop detectors" << endl
	 << "%   0x2 = VRC sensors" << endl
	 << "%   0x4 = Area sensors" << endl << endl;

  os << "[Sensor Color Code]  = "
	 << theSymbols()->sensorColorCode().value() << endl
	 << "%   0 = Count" << endl
	 << "%   1 = Flow" << endl
	 << "%   2 = Speed" << endl
	 << "%   3 = Occupancy" << endl << endl;

  os << "[Vehicles] = "
	 << theSymbols()->vehicleBorderCode().value() << endl
	 << "%   0 = None" << endl
	 << "%   1 = Vehicle type" << endl
	 << "%   2 = Information availability" << endl
	 << "%   3 = Turning movement" << endl
	 << "%   4 = Driver behavior group" << endl
	 << "%   5 = Lane use" << endl << endl;

  os << "[Vehicle Shade Params] = {" << endl
	 << "\t" << theVehicleShadeParam[0]
	 << "\t# Shade" << endl
	 << "\t" << theVehicleShadeParam[1]
	 << "\t# Outstanding time in a segment" << endl
	 << "\t" << theVehicleShadeParam[2]
	 << "\t# Outstanding time in the network" << endl
	 << "}" << endl;

#ifdef INTERNAL_GUI
  DRN_ViewMarker::SaveMarkers(os) ;
#endif

  os << endl;
   
  theOptions->printOptions(os);

#endif
}

//IEM(Apr24) Given a vector of numbers (by master.mitsim parser), sticks them in survPointStepSizes_
//IEM(Jun15) Requires that [Point Sensor Step Size] comes before
// [Point Sensor Step Sizes] in master.mitsim, so that the default element
// will be added before the other ones.
int TS_Engine::survPointStepSizes(GenericVariable &gv) 
{

  int nSensorTypes_ = gv.nElements();

  for (int i = 0; i < nSensorTypes_; i++) {
    survPointStepSizes_.push_back(gv.element(i));
  }  
  
  return 0;

}

//IEM(Jun19) Given a vector of 0s and 1s, sticks them in sensorOutputFlags_
int TS_Engine::sensorOutputFlags(GenericVariable &gv) 
{

  int nSensorOutputFlags_ = gv.nElements();

  for (int i = 0; i < nSensorOutputFlags_; i++) {
    sensorOutputFlags_.push_back(gv.element(i));
  }  
  
  return 0;

}
