//-*-c++-*------------------------------------------------------------
// TS_VehicleDialog.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <assert.h>

#include <Xmt/Xmt.h>
#include <Xmt/Chooser.h>
#include <Xm/ToggleB.h>

#include "TS_VehicleDialog.h"
#include "TS_Interface.h"
#include "TS_Symbols.h"
#include "TS_DrawingArea.h"
#include "TS_Menu.h"

TS_VehicleDialog::TS_VehicleDialog ( Widget parent )
   : XmwDialogManager(parent, "vehicleDialog", NULL, 0)
{
   borderCodeFld_ = theSymbols()->associate(widget_, "vehicleBorderCode");
   assert(borderCodeFld_);
   addCallback(borderCodeFld_, XmtNvalueChangedCallback,
			   &TS_VehicleDialog::clickBorderCodeCB, this);

   shadeCodeFld_ = theSymbols()->associate(widget_, "vehicleShadeCode");
   assert(shadeCodeFld_);
   addCallback(shadeCodeFld_, XmtNvalueChangedCallback,
			   &TS_VehicleDialog::clickCB, this);

   labelFld_ = theSymbols()->associate(widget_, "vehicleLabel");
   assert(labelFld_);
   addCallback(labelFld_, XmtNvalueChangedCallback,
			   &TS_VehicleDialog::clickLabelCB, this);
}

// These overload the functions in base class

void TS_VehicleDialog::cancel(Widget, XtPointer, XtPointer)
{
   // Restore the original code before close the box
   
   theSymbols()->vehicleBorderCode().set(borderCode_);
   theSymbols()->vehicleShadeCodes().set(shadeCode_);
   theSymbols()->isVehicleLabelOn().set(label_);

   theDrawingArea->setState(TS_DrawingArea::REDRAW);
   unmanage();
}


void TS_VehicleDialog::okay(Widget, XtPointer, XtPointer)
{
   theDrawingArea->setState(TS_DrawingArea::REDRAW);
   unmanage();
   theMenu->needSave();
}


void TS_VehicleDialog::help(Widget, XtPointer, XtPointer)
{
   theDrnInterface->openUrl("vehicleColorCode", "drn.html");
}


void TS_VehicleDialog::post()
{
   // Record the value when the box is posted.

   borderCode_ = theSymbols()->vehicleBorderCode().value();
   shadeCode_ = theSymbols()->vehicleShadeCodes().value();
   label_ = theSymbols()->isVehicleLabelOn().value();

   // BUG COMMENTS BEGIN: The state of choosers do not replect the
   // latest value of the linked symbols when after the cancel button
   // is pressed and the dialog box is posed again.  This should be a
   // bug somewhere, but I could not figure out where.  Following is a
   // work around which force to update the chooser state based on
   // latest symbol value.  This is not necessary if the Xmt symbols
   // work as supported.

   XmtChooserSetState(borderCodeFld_, borderCode_, False);
   XmtChooserSetState(shadeCodeFld_, shadeCode_, False);
   XmToggleButtonSetState(labelFld_, label_, False);

   // BUG COMMENTS END

   if (borderCode_) {
	  activate(shadeCodeFld_);
	  activate(labelFld_);
   } else {
	  deactivate(shadeCodeFld_);
	  deactivate(labelFld_);
   }
   XmwDialogManager::post();
}


void TS_VehicleDialog::clickBorderCodeCB(Widget, XtPointer, XtPointer)
{
   if (theSymbols()->vehicleBorderCode().value()) {
	  activate(shadeCodeFld_);
	  activate(labelFld_);
   } else {
	  deactivate(shadeCodeFld_);
	  deactivate(labelFld_);
   }
   theDrawingArea->redraw();
}

void TS_VehicleDialog::clickCB(Widget, XtPointer, XtPointer)
{
   theDrawingArea->redraw();
}

void TS_VehicleDialog::clickLabelCB(Widget, XtPointer, XtPointer)
{
   if (XmToggleButtonGetState(labelFld_)) {
	  theSymbols()->isVehicleLabelOn().set(True);
   } else {
	  theSymbols()->isVehicleLabelOn().set(False);
   }
   theDrawingArea->redraw();
}

#endif // INTERNAL_GUI
