/*-*-c++-*-******************************************************/
/*    NAME: Traffic Simulation                                  */
/*    AUTH: Qi Yang                                             */
/*    FILE: odtable.h                                           */
/*    DATE: Mon Aug 28 16:37:23 1995                            */
/****************************************************************/


#ifndef TS_ODTABLE_HEADER
#define TS_ODTABLE_HEADER

#ifdef INTERNAL_GUI		// graphical mode
#include <DRN/DRN_ODTable.h>
class TS_ODTable : public DRN_ODTable
#else  // match mode
#include <GRN/OD_Table.h>
class TS_ODTable : public OD_Table
#endif // !INTERNAL_GUI

{
   public:

#ifdef INTERNAL_GUI
      TS_ODTable() : DRN_ODTable() { }
#else
      TS_ODTable() : OD_Table() { }
#endif

      ~TS_ODTable() { }

      // These functions override the virtual ones in the base class

      OD_Cell* newOD_Cell();
};

#endif
