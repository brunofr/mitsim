//-*-c++-*------------------------------------------------------------
// TS_OutputDialog.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <assert.h>

#include <Xmt/Xmt.h>
#include <Xmt/Chooser.h>
#include <Xmt/InputField.h>
#include <Xm/ToggleB.h>

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>

#include "TS_OutputDialog.h"
#include "TS_Interface.h"
#include "TS_Engine.h"
#include "TS_FileManager.h"
#include "TS_Interface.h"
#include "TS_Menu.h"

TS_OutputDialog::TS_OutputDialog ( Widget parent )
  : XmwDialogManager(parent, "outputDialog", NULL, 0)
{
  XtVaSetValues(widget_,
				XmNdialogStyle, XmDIALOG_PRIMARY_APPLICATION_MODAL,
				NULL);

  Widget group;
  XmwCheckerField *fld;

  group = XtNameToWidget(widget_, "*GeneralGroup");
  assert(group);

  outDirFld_ = XtNameToWidget(group, "*outDir");
  assert(outDirFld_);
  isHeadingOnFld_ = XtNameToWidget(group, "*header");
  assert(isHeadingOnFld_);
  formatFld_ = XtNameToWidget(group, "*format");
  assert(formatFld_);

  fileFlds_.reserve(16);

  group = XtNameToWidget(widget_, "*SurveillanceGroup");
  assert(group);

  fld = new XmwCheckerField(group, "o11", OUTPUT_SENSOR_READINGS);
  fileFlds_.push_back(fld);
  fld = new XmwCheckerField(group, "o12", OUTPUT_VRC_READINGS);
  fileFlds_.push_back(fld);

  group = XtNameToWidget(widget_, "*NetworkGroup");
  assert(group);

  fld = new XmwCheckerField(group, "o21", OUTPUT_LINK_TRAVEL_TIMES);
  fileFlds_.push_back(fld);
  fld = new XmwCheckerField(group, "o22", OUTPUT_SEGMENT_TRAVEL_TIMES);
  fileFlds_.push_back(fld);
  fld = new XmwCheckerField(group, "o23", OUTPUT_SEGMENT_STATISTICS);
  fileFlds_.push_back(fld);
  fld = new XmwCheckerField(group, "o24", OUTPUT_QUEUE_STATISTICS);
  fileFlds_.push_back(fld);
  fld = new XmwCheckerField(group, "o25", OUTPUT_TRAVEL_TIMES_TABLE);
  fileFlds_.push_back(fld);
  fld = new XmwCheckerField(group, "o26", OUTPUT_STATE_3D);
  fileFlds_.push_back(fld);

  group = XtNameToWidget(widget_, "*VehicleGroup");
  assert(group);

  fld = new XmwCheckerField(group, "o31", OUTPUT_VEHICLE_DEP_RECORDS);
  fileFlds_.push_back(fld);
  fld = new XmwCheckerField(group, "o32", OUTPUT_VEHICLE_LOG);
  fileFlds_.push_back(fld);
  fld = new XmwCheckerField(group, "o33", OUTPUT_VEHICLE_PATH_RECORDS);
  fileFlds_.push_back(fld);
  fld = new XmwCheckerField(group, "o34", OUTPUT_VEHICLE_TRAJECTORY);
  fileFlds_.push_back(fld);
  fld = new XmwCheckerField(group, "o35", OUTPUT_SYSTEM_MOE);
  fileFlds_.push_back(fld);
  fld = new XmwCheckerField(group, "o36", OUTPUT_TRANSIT_TRAJECTORY);
  fileFlds_.push_back(fld);
  fld = new XmwCheckerField(group, "o37", OUTPUT_STOP_ARRIVAL);
  fileFlds_.push_back(fld);
  fld = new XmwCheckerField(group, "o38", OUTPUT_SIGNAL_PRIORITY);
  fileFlds_.push_back(fld);
}

TS_OutputDialog::~TS_OutputDialog()
{
  for (int i = 0; i < fileFlds_.size(); i ++) {
	delete fileFlds_[i];
  }
}

// These overload the functions in base class

void TS_OutputDialog::cancel(Widget, XtPointer, XtPointer)
{
  unmanage();
}


void TS_OutputDialog::okay(Widget, XtPointer, XtPointer)
{
  // Copy the data from dialog 

  ToolKit::outdir(XmtInputFieldGetString(outDirFld_));

  theFileManager->sensorFile(fileFlds_[ 0]->get());
  theFileManager->sensorVRCFile(fileFlds_[ 1]->get());
  theFileManager->linkFlowTravelTimesFile(fileFlds_[ 2]->get());
  theFileManager->segmentTravelTimesFile(fileFlds_[ 3]->get());
  theFileManager->segmentStatisticsFile(fileFlds_[ 4]->get());
  theFileManager->queueFile(fileFlds_[ 5]->get());
  theFileManager->linkTravelTimesFile(fileFlds_[ 6]->get());
  theFileManager->state3d(fileFlds_[ 7]->get());
  theFileManager->depRecordFile(fileFlds_[ 8]->get());
  theFileManager->vehicleFile(fileFlds_[ 9]->get());
  theFileManager->pathRecordFile(fileFlds_[10]->get());
  theFileManager->trajectoryFile(fileFlds_[11]->get());
  theFileManager->moeOutputFile(fileFlds_[12]->get());
  theFileManager->transitTrajectoryFile(fileFlds_[13]->get());
  theFileManager->busStopArrivalFile(fileFlds_[14]->get());
  theFileManager->signalPriorityFile(fileFlds_[15]->get());

  if (XmToggleButtonGetState(isHeadingOnFld_)) {
	theEngine->removeOutput(OUTPUT_SKIP_COMMENT);
  } else {
	theEngine->chooseOutput(OUTPUT_SKIP_COMMENT);
  }

  if (XmtChooserGetState(formatFld_)) {
	theEngine->chooseOutput(OUTPUT_RECT_TEXT);
  } else {
	theEngine->removeOutput(OUTPUT_RECT_TEXT);
  }

  for (int i = 0; i < fileFlds_.size(); i ++) {
	if (fileFlds_[i]->isChecked()) {
	  theEngine->chooseOutput(fileFlds_[i]->mask());
	} else {
	  theEngine->removeOutput(fileFlds_[i]->mask());
	}
  }

  unmanage();
  theMenu->needSave();
}


void TS_OutputDialog::help(Widget, XtPointer, XtPointer)
{
  theDrnInterface->openUrl("chooseOutput", "mitsim.html");
}


void TS_OutputDialog::post()
{
  // Copy data to dialog

  XmtInputFieldSetString(outDirFld_, ToolKit::outdir());

  fileFlds_[ 0]->set(theFileManager->sensorFile_);
  fileFlds_[ 1]->set(theFileManager->sensorVRCFile_);
  fileFlds_[ 2]->set(theFileManager->linkFlowTravelTimesFile_);
  fileFlds_[ 3]->set(theFileManager->segmentTravelTimesFile_);
  fileFlds_[ 4]->set(theFileManager->segmentStatisticsFile_);
  fileFlds_[ 5]->set(theFileManager->queueFile_);
  fileFlds_[ 6]->set(theFileManager->linkTravelTimesFile_);
  fileFlds_[ 7]->set(theFileManager->state3d_);
  fileFlds_[ 8]->set(theFileManager->depRecordFile_);
  fileFlds_[ 9]->set(theFileManager->vehicleFile_);
  fileFlds_[10]->set(theFileManager->pathRecordFile_);
  fileFlds_[11]->set(theFileManager->trajectoryFile_);
  fileFlds_[11]->set(theFileManager->trajectoryFile_);
  fileFlds_[12]->set(theFileManager->moeOutputFile_);
  fileFlds_[13]->set(theFileManager->transitTrajectoryFile_);
  fileFlds_[14]->set(theFileManager->busStopArrivalFile_);
  fileFlds_[15]->set(theFileManager->signalPriorityFile_);

  if (theEngine->skipComment()) {
	XmToggleButtonSetState(isHeadingOnFld_, False, False);
  } else {
	XmToggleButtonSetState(isHeadingOnFld_, True, False);
  }

  if (theEngine->isRectangleFormat()) {
	XmtChooserSetState(formatFld_, 1, False);
  } else {
	XmtChooserSetState(formatFld_, 0, False);
  }

  // Set the check state

  for (int i = 0; i < fileFlds_.size(); i ++) {
	if (theEngine->chosenOutput(fileFlds_[i]->mask())) {
	  fileFlds_[i]->check(True);
	} else {
	  fileFlds_[i]->check(False);
	}
  }

  // Do not allow changes after simulation is started

  if (theSimulationClock->isStarted()) {
	deactivate();
  } else {
	activate();
  }
   
  XmwDialogManager::post();
}

void TS_OutputDialog::activate()
{
  if (XtIsSensitive(okay_)) return;
  XmwDialogManager::activate(outDirFld_);
  XmwDialogManager::activate(isHeadingOnFld_);
  XmwDialogManager::activate(formatFld_);
  for (int i = 0; i < fileFlds_.size(); i ++) {
	XmwDialogManager::activate(fileFlds_[i]->checker());
	XmwDialogManager::activate(fileFlds_[i]->input());
  }
  XmwDialogManager::activate(okay_);
}

void TS_OutputDialog::deactivate()
{
  if (!XtIsSensitive(okay_)) return;
  XmwDialogManager::deactivate(outDirFld_);
  XmwDialogManager::deactivate(isHeadingOnFld_);
  XmwDialogManager::deactivate(formatFld_);
  for (int i = 0; i < fileFlds_.size(); i ++) {
	XmwDialogManager::deactivate(fileFlds_[i]->checker());
	XmwDialogManager::deactivate(fileFlds_[i]->input());
  }
  XmwDialogManager::deactivate(okay_);
}

#endif // INTERNAL_GUI
