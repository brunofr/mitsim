//-*-c++-*------------------------------------------------------------
// NAME: Microcscopic Traffic Simulator
// AUTH: Qi Yang
// FILE: TS_Setup.C
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#include <iostream>
#include <iomanip>
#include <new>
#include <cstdio>
#include <cstring>
using namespace std;

#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <Tools/SimulationClock.h>
#include <Tools/Random.h>

#include <Tools/Math.h>

#include "TS_Setup.h"
#include "TS_Network.h"
#include "TS_ODTable.h"
#include "TS_ODCell.h"
#include "TS_VehicleTable.h"
#include "TS_BusAssignmentTable.h"
#include "TS_BusStopPrmTable.h"
#include "TS_BusCommPrmTable.h"
#include "TS_Parameter.h"
#include "TS_Engine.h"
#include "TS_Status.h"
#include "TS_Segment.h"
#include "TS_QueueHead.h"
#include "TS_FileManager.h"
#include "TS_Incident.h"
#include "TS_Exception.h"
#include "TS_Communicator.h"
#include "TS_CmdArgsParser.h"

#include <GRN/RN_Parser.h>
#include <GRN/RN_PathParser.h>
#include <GRN/RN_PathTable.h>

//Dan:
#include <GRN/RN_BusRouteParser.h>  
#include <GRN/RN_BusRouteTable.h>  
#include <GRN/RN_BusScheduleParser.h>
#include <GRN/RN_BusScheduleTable.h> 
#include <GRN/RN_BusRunParser.h>  
#include <GRN/RN_BusRunTable.h>  

#include <GRN/RN_DynamicRoute.h>

#ifdef EXTERNAL_MOE
#include <MOE/MOE_TSSampler.h>
#endif


void 
ParseParameters() {
   if (theParameter) {
      delete (TS_Parameter *)theParameter;
   }
   theParameter = new TS_Parameter;
   theParameter->load();
}


// --------------------------------------------------------------------
// Read the network database. This include network objects (nodes,
// link labels, links, segments, lanes), traffic control objects
// (traffic signals, message signs, toll booths, etc.) and
// surveillance objects (sensors).
// --------------------------------------------------------------------

void ParseNetwork()
{
   if (theNetwork) {
      if (ToolKit::verbose()) {
		 cout << "Unloading <" << theNetwork->name()
			  << ">" << endl;
      }
      delete (TS_Network*)theNetwork;
   }

   theNetwork = new TS_Network;
   const char * filename = ToolKit::optionalInputFile(theNetwork->name());
   if (!filename) theException->exit(1);
   RN_Parser rn_parser(theNetwork);
   rn_parser.parse(filename);

#ifdef INTERNAL_GUI
   ((TS_Network*)theNetwork)->calcGeometricData();
#endif

   if (ToolKit::verbose()) {
      theNetwork->printBasicInfo();
   }
}


// Parse path table 

void ParsePathTables()
{
   if (thePathTable) {
      delete thePathTable;
      thePathTable = NULL;
   }
   if (ToolKit::isValidInputFilename(RN_PathTable::name())) {
      thePathTable = new RN_PathTable;
      const char *filename = ToolKit::optionalInputFile(RN_PathTable::name());
      if (!filename) theException->exit(1);
      RN_PathParser pt_parser(thePathTable);
      pt_parser.parse(filename);
      thePathTable->setPathPointers();
	  theNetwork->calcPathCommonalityFactors();
   } else if (ToolKit::verbose()) {
      cout << "No path tables. "
		   << "All vehicle will use progressive route choice model." << endl;
   }
}


// Dan - parse bus route tables 

void ParseBusRoutes()
{
   if (theBusRouteTable) {
      delete theBusRouteTable;
      theBusRouteTable = NULL;
   }
   if (ToolKit::isValidInputFilename(RN_BusRouteTable::name())) {
      theBusRouteTable = new RN_BusRouteTable;
      const char *filename = ToolKit::optionalInputFile(RN_BusRouteTable::name());
      if (!filename) theException->exit(1);
      RN_BusRouteParser br_parser(theBusRouteTable);
      br_parser.parse(filename);
   } else if (ToolKit::verbose()) {
      cout << "No bus routes. "
		   << endl;
   }
}

// Dan - parse bus schedule tables 

void ParseBusSchedules()
{
   if (theBusScheduleTable) {
      delete theBusScheduleTable;
      theBusScheduleTable = NULL;
   }
   if (ToolKit::isValidInputFilename(RN_BusScheduleTable::name())) {
      theBusScheduleTable = new RN_BusScheduleTable;
      const char *filename = ToolKit::optionalInputFile(RN_BusScheduleTable::name());
      if (!filename) theException->exit(1);
      RN_BusScheduleParser bs_parser(theBusScheduleTable);
      bs_parser.parse(filename);
   } else if (ToolKit::verbose()) {
      cout << "No bus schedules. "
		   << endl;
   }
}

// Dan - parse bus run tables 

void ParseBusRuns()
{
   if (theBusRunTable) {
      delete theBusRunTable;
      theBusRunTable = NULL;
   }
   if (ToolKit::isValidInputFilename(RN_BusRunTable::name())) {
      theBusRunTable = new RN_BusRunTable;
      const char *filename = ToolKit::optionalInputFile(RN_BusRunTable::name());
      if (!filename) theException->exit(1);
      RN_BusRunParser r_parser(theBusRunTable);
      r_parser.parse(filename);
      theBusRunTable->setBusRunPointers();
   } else if (ToolKit::verbose()) {
      cout << "No bus routes. "
		   << endl;
   }
}

// Read OD Trip tables

void ParseODTripTables()
{
   if (theODTable) {
      delete (TS_ODTable *)theODTable;
      theODTable = NULL;
   }
   if (ToolKit::isValidInputFilename(TS_ODTable::name())) {
      theODTable = new TS_ODTable;
      theODTable->open();
   } else if (ToolKit::verbose()) {
      cout << "No trip tables." << endl;
   }
}


// Read vehicle tables

void ParseVehicleTables()
{
   if (theVehicleTable) {
      delete (TS_VehicleTable *)theVehicleTable;
      theVehicleTable = NULL;
   }
   if (ToolKit::isValidInputFilename(VehicleTable::name())) {
      theVehicleTable = new TS_VehicleTable;
      theVehicleTable->open(theSimulationClock->startTime());
   } else if (ToolKit::verbose()) {
      cout << "No vehicle tables." << endl;
   }
}


// Dan - Read bus assignments

void ParseBusAssignments()
{
   if (theBusAssignmentTable) {
      delete (TS_BusAssignmentTable *)theBusAssignmentTable;
      theBusAssignmentTable = NULL;
   }
   if (ToolKit::isValidInputFilename(BusAssignmentTable::name())) {
      theBusAssignmentTable = new TS_BusAssignmentTable;
      theBusAssignmentTable->open(theSimulationClock->startTime());
   } else if (ToolKit::verbose()) {
      cout << "No bus assignments." << endl;
   }
}

// Dan - Read bus stop parameters

void ParseBusStopPrms()
{
   if (theBusStopPrmTable) {
      delete (TS_BusStopPrmTable *)theBusStopPrmTable;
      theBusStopPrmTable = NULL;
   }
   if (ToolKit::isValidInputFilename(RN_BusStopPrmTable::name())) {
      theBusStopPrmTable = new TS_BusStopPrmTable;
      theBusStopPrmTable->open(theSimulationClock->startTime());
   } else if (ToolKit::verbose()) {
      cout << "No bus stop parameters." << endl;
   }
}

// Dan - Read bus sensing parameters (e.g. for signal priority)

void ParseBusCommPrms()
{
   if (theBusCommPrmTable) {
      delete (TS_BusCommPrmTable *)theBusCommPrmTable;
      theBusCommPrmTable = NULL;
   }
   if (ToolKit::isValidInputFilename(RN_BusCommPrmTable::name())) {
      theBusCommPrmTable = new TS_BusCommPrmTable;
      theBusCommPrmTable->open(theSimulationClock->startTime());
   } else if (ToolKit::verbose()) {
      cout << "No bus sensing parameters." << endl;
   }
}

// Parse moe specifications

void ParseMOESpecifications()
{
   if (ToolKit::isValidInputFilename(theFileManager->moeSpecFile())) {

      const char *fname = ToolKit::optionalInputFile(
													 theFileManager->moeSpecFile());

      if (!fname) theException->exit(1);

      cout << endl << "Loading MOE specification file:" << endl;
      cout << indent << fname << endl;

      // Parse specification for queue calculation

      Reader is(fname);
      TS_QueueHead::load(is);
      if (theEngine->chosenOutput(OUTPUT_QUEUE_STATISTICS)) {
		 TS_QueueHead::nextTime(theSimulationClock->startTime()+
								TS_QueueHead::stepSize());
      } else {
		 TS_QueueHead::nextTime(theSimulationClock->stopTime()+
								TS_QueueHead::stepSize());
      }

#ifdef EXTERNAL_MOE

      if (theMOESampler) {
		 delete theMOESampler;
      }
      theMOESampler = 
		 new MOE_TSSampler(
						   ToolKit::outfile(theFileManager->moeOutputFile()), WRITE);

      RoadNetwork::parseMOESpecification(is);
      initializeSegmentStatistics(NULL);
      theEngine->setTimes();
       
#endif

      is.close();

   } else if (theEngine->chosenOutput(OUTPUT_QUEUE_STATISTICS)) {
	  cerr << "Input data file for MOE not specified." << endl
		   << "Collection of queuing data ignored." << endl;
	  theEngine->removeOutput(OUTPUT_QUEUE_STATISTICS);
   } else if (ToolKit::verbose()) {
	  cout << "No MOE specification." << endl;
	  TS_QueueHead::nextTime(DBL_INF);
   }
}


// -------------------------------------------------------------------
// Read scenarios files 
// -------------------------------------------------------------------

void SetupScenarios()
{
   // Read incidents

   TS_Incident::readIncidents(TS_Incident::filename());
}


// Setup some other stuffs. 

void SetupMiscellaneous()
{
   // Assign the pointers to control and surveillance devices in
   // TS_Segment objects.

   theNetwork->assignCtrlListInSegments();
   theNetwork->assignSurvListInSegments();
}
