/*-*-c++-*-******************************************************/
/*    NAME: Traffic Simulation                                  */
/*    AUTH: Qi Yang                                             */
/*    FILE: TS_ODCell.C                                         */
/*    DATE: Fri Jun 23 22:07:37 1995                            */
/****************************************************************/

#include <GRN/RN_Link.h>
#include <GRN/Constants.h>
#include "TS_ODTable.h"
#include "TS_ODCell.h"
#include "TS_VehicleList.h"
#include "TS_Vehicle.h"
#include "TS_BusAssignmentTable.h"

/*
 * Create vehicle based on current departure rate. If space is
 * available in the start link, enter the vehicle into the network.
 */

void
TS_ODCell::emitVehicles()
{
  if ((type() & VEHICLE_BUS_RAPID) && !theBusAssignmentTable) {
    theBusAssignmentTable = new TS_BusAssignmentTable;
    theBusAssignmentTable->setBRTOnly();
  }

  TS_Vehicle *pv;
  while (pv = (TS_Vehicle *)OD_Cell::emitVehicle()) {
    pv->enterPretripQueue();
  }
}


RN_Vehicle* 
TS_ODCell::newVehicle()
{
  return theVehicleList->recycle();
}
