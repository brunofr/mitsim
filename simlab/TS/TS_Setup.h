//-*-c++-*------------------------------------------------------------
// NAME: Microcscopic Traffic Simulator
// AUTH: Qi Yang
// FILE: TS_Setup.h
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#ifndef TS_SETUP_HEADER
#define TS_SETUP_HEADER

extern void ParseParameters();

extern void ParseNetwork();
extern void ParsePathTables();

extern void ParseODTripTables();
extern void ParseVehicleTables();

//Dan
extern void ParseBusAssignments();
extern void ParseBusSchedules();
extern void ParseBusRoutes();
extern void ParseBusRuns();
extern void ParseBusStopPrms();
extern void ParseBusCommPrms();

extern void SetupScenarios();
extern void SetupMiscellaneous();

extern void ParseMOESpecifications();

#endif
