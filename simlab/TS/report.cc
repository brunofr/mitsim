  /*
  ****************************Target Lane Probability Function**********
  */

// arguments passed -- plane: current lane of subject vehicle, change: number of lanes from exit, LCdistance: distance to exit location, connected: array of permissible lanes in vehicle's current segment
vector <double> TS_Vehicle::TLProbabilityLookAhead(TS_Lane*plane, vector <int> & change, vector <double> & LCdistance, vector <int> & connected) 

{
        
unsigned int prev_status = status_; // last updated status of vehicle

int n = plane->localIndex();  // current lane index

int ex_id; // exit lane local index  

RN_Segment* psegment = plane->segment();  // current segment

float *c = theParameter->targetLaneParams(); // array of estimated parameters within TL selection model 

int rules = lane_->cRules();

double uc=0, ul=0, ur=0; 

//calculation of CL utility

//CL Dummy as well as aggressiveness

  uc = c[6] +  c[20] * aggresiveness(); 


//calculation of RL utility : only if RL is in choice set

 ur = 0; 


//calculation of LL utility : only if LL is in choice set

 ul = 0;
 


// CALCULATING LANE-SPECIFIC UTILITIES

 vector <double> ulane(laneNum); // lane-specific utility

// CALCULATING LANE-SPECIFIC VARIABLES -- speed, density, queue length and availability
  
 double segpos=0;
 int laneNum= psegment->nLanes(); // number of lanes

 vector<double> laneDensity(laneNum); // lane density
 vector<double> laneSpeed(laneNum);   // lane speed
 vector<double> lanequeue(laneNum); //  lane queue  
 vector<int> lan_av(laneNum);       // lane availability

 //loop through all lanes to fill in the availability vector
 for (int j = 0; j <laneNum; j ++) { 

        TS_Lane* qlane = (TS_Lane*) psegment->lane(j); // lane j in current segment
	laneDensity[j]=qlane->density();               // density
	laneSpeed[j]=qlane->calcSpeed();               // average speed
        lanequeue[j] = qlane->queueahead(this->position()); // queue ahead of subjet vehicle
  
        //initialize utility
	ulane[j]=0; 

        // estimate availability of lane j

        int vid;
	vid = this->code();
	if ( (j < connected[0])||(j> connected[connected.size()-1]) )
	  {
            lan_av[j]=0;
	    TS_Node * dnode = (TS_Node *)link()->dnNode();
            short int ndlinks = dnode->nDnLinks();
            for(int ii=0;ii<ndlinks;ii++)
	      {
                TS_Link * target = (TS_Link *)dnode->dnLink(ii);
                if ( (target->dnNode()->code() - dnode->code() == 1)||(target->dnNode()->code() - dnode->code() == -1) )
		  { 
                    if (qlane->isDownLanesInNextLink(target))
		      {   lan_av[j] = 1;}
                  }
	      }
	   
          }
        else
	  {
            lan_av[j] = 1;
          }
   
 }
  

//ALLOCATE uc, ur, and ul TO CORRESPONDING LANE UTILITIES 

switch (n){
 case 0: //leftmost lane
   {
     ulane[0]=ulane[0]+uc;
     ulane[1]=ur;

      break;
   }
 
 default://anylane
   { 
     ulane[n]=uc;
     ulane[n-1]=ul;
 
    if (n!=(laneNum-1)) {//not rightmost lane
     ulane[n+1]=ur;
    }
    
}//default

}//switch

 

// INCORPORATING OTHER VARIABLES IN LANE-SPECIFIC UTILITY

TS_Vehicle* front = this->vehicleAhead();

for (int k=0;k<laneNum;k++){ //allocating constants

int numChange=change[k]; // number of lanes from exit

double dist = LCdistance[k]; // distance to exit

 if ( (dest == 0) || (dest == 5) ) // through vehicle -- no immediate exit point
   { 
     LCdistance[k] = 2000;
   }


 // 1. path-plan component of utility
 ulane[k]=ulane[k]+c[11]*(numChange)*pow(LCdistance[k]/100,c[17])+c[1]*(numChange);

 // 2. extra-bay dummy
 if ( (laneNum == 6) && (k == 0) )
   { ulane[k] = ulane[k]+c[22];}

 // 3. variable with interaction between front relative speed and front spacing -- only for Current Lane
 float relsp = 0.0; 
 float relgap = 0.0;
 if (k==n)
   {
 if(front)
   {
     relsp = front->currentSpeed() - this->currentSpeed();
     relgap = this->gapDistance(front);
    
     float val;

     if (relsp < 0)
       {
	 val = relsp/(1+exp(relgap));
       }
     else
       {
	 val = 0;
       }
     ulane[k] = ulane[k] + 0.0276*val;
   }
   } 


 // 4. utility component for lane queue length

 float uq; 
 if (lanequeue[k] <= 3)
 { 
   uq = lanequeue[k]*c[21];
 }
 else
 {
   uq = c[21]*3;
 }
  
 // 5. utility component for no. of LC from current lane
 float uclc; 
 if (abs(n-k) <= 3)
   { 
     uclc = c[5]*abs(n-k);
   }
 else
   {
     uclc = c[10]*abs(n-k);
   }


 // incorporating lanespeed, queuelength, and number of lanes from CL into lane utilities

    ulane[k]=ulane[k]+c[3]*laneDensity[k]+c[4]*laneSpeed[k]+ uclc + uq; //c[5]=coeff of each additional lc reqd


 ulane[k] = exp(ulane[k]);
	     

}//end of for loop



// CALCULATING LANE PROBABILITIES

 
 double sum=0;

 for (int i=0;i<laneNum;i++){
   ulane[i] = lan_av[i]*ulane[i]; 
   sum=sum+ulane[i];
 }

 for (int i=0;i<laneNum;i++){
   probLane[i]=ulane[i]/sum;
    
 }


 return probLane;
                                                                                                                                     

}

/*
*******************Once a vehicle decides to change lanes based on Target Lane selection, the following function is called to decide the simulator's next step*********
 */

void
TS_Vehicle::executeLaneChanging()
{
  // SET/UNSET FLAGS

  unsetFlag(FLAG_LC_FAILED);

   // set TARGET LANE - plane

  TS_Lane *plane;

  if (status(STATUS_LEFT)) {
	plane = lane_->left();
  } else if (status(STATUS_RIGHT)) {
	plane = lane_->right();
  } else {
	return;			// No request for lane change 
  }

  // Fix for sporadic crashes due to null pointer.  (Angus)
  if (!plane) return;

  // If in incident area, skip lane change function
  if (isInIncidentArea(plane)) return;

   // LEAD AND LAG VEHICLES (do not have to be in same segment as per current definition)

  TS_Vehicle* av = findFrontBumperLeader(plane);
  TS_Vehicle* bv = findFrontBumperFollower(plane);

  // LEAD HEADWAY
  float aheadway;	 
  if (av) {
	aheadway = this->gapDistance(av);
  } else {
	aheadway = FLT_INF;
  }

  // LAG HEADWAY
  float bheadway;	      
  if (bv) {
	bheadway = bv->gapDistance(this);
  } else {
	bheadway = FLT_INF;
  }
 
  if (av)
    { // if lead vehicle not in same link, assign lead gap = distance to upstream end of link
      if (av->link() != this->link()) 
	{
	float sum = 0;
	
        if (this->segment() != this->link()->endSegment())
	{    
	  TS_Lane *test = (TS_Lane *)this->lane()->dnLane(0);
          sum = sum + test->length();
          
          while(!(test->segment() == test->link()->endSegment()))
	   {
	     test = test->dnLane(0);
             sum = sum + test->length();
           }
          
        }
          aheadway = this->distance_ + sum;
        }
      
    }
  else // if no lead vehicle, assign lead gap = distance to upstream end of link
    { 
      	float sum = 0;
	
        if (this->segment() != this->link()->endSegment())
	{    
	  TS_Lane *test = (TS_Lane *)this->lane()->dnLane(0);
          sum = sum + test->length();
          
          while(!(test->segment() == test->link()->endSegment()))
	   {
	     test = test->dnLane(0);
             sum = sum + test->length();
           }          
        }
          aheadway = this->distance_ + sum;

    }


  // COMPARISON WITH CRITICAL GAPS

   if (bv && bheadway < theParameter->lcCriticalGap(1, bv->currentSpeed_ - currentSpeed_ ,this)) {
 	setFlag(FLAG_LC_FAILED_LAG); // lag gap not acceptable
  } else if ( (av && aheadway < theParameter->lcCriticalGap(0, av->currentSpeed_ - currentSpeed_, this)) || ( (!av) && aheadway < theParameter->lcCriticalGap(0,- currentSpeed_, this) )) {
	setFlag(FLAG_LC_FAILED_LEAD); // lead gap not acceptable
  }


   // LANE-CHANGING EXECUTION (LEVEL 3) PROBABILITY  

   if (!(flag(FLAG_LC_FAILED)))   // if Gap acceptable
     {
       int prob_acc;
       prob_acc = theParameter->lcGapAcc(this);
       if (prob_acc == 0)
	 { setFlag(FLAG_LC_FAILED); }       
     }
       
   goto execution;  // change lane of subject vehicle in the simulator

}



/*
  ****************************Function for critical gaps -- "lcCriticalGap"**********
 */

float
TS_Parameter::lcCriticalGap(
  int type,  // 0=leading 1=lag, 
  float dv,//spd diff from leader
 TS_Vehicle *pv)// the subject vehicle		
{
  float *a = criticalGapParams();
 
  float gap =0.0;

  float error;  
 
  switch (type){
  case 0: {     // lead critical gap
 
    gap = a[0] + a[1] * dv + a[2] * pv->aggresiveness();//Varun 07
    error = theRandomizer->nrandom(0, a[3]);
    gap = gap + error;
    
    break;
  }
  case 1: {     // lag critical gap

    gap = a[4] + a[5]  * dv  + a[6] * pv->aggresiveness();//Varun 07
    error = theRandomizer->nrandom(0, a[7]);
    gap = gap + error;

    break;
  }
    
  }
 
  // compute actual value of critical gap
  float cri_gap  = exp(gap);

  return cri_gap ;
}


/*
****************************Function for calculating LC Execution probability -- "lcGapAcc"****************************
*/

int
TS_Parameter::lcGapAcc(TS_Vehicle *pv)		
{ 
  float *a = criticalGapParams(); // parameters for critical gap and LC execution
  TS_Lane *plane;                // current lane

  // Setting TARGET LANE
  if (pv->status(STATUS_LEFT)) {
	plane = (TS_Lane*)pv->lane()->left();
  } else if (pv->status(STATUS_RIGHT)) {
	plane = (TS_Lane*)pv->lane()->right();
  } else {
	return 0;			// No request for lane change 
  }

  TS_Vehicle* av = pv->findFrontBumperLeader(plane); // front vehicle
  TS_Vehicle* bv = pv->findFrontBumperFollower(plane); // trailing vehicle

  double p; // probability of LC execution

  float fcrgap;  // lead critical gap
  float bcrgap;  // lag critical gap

  fcrgap = 0.0;
  bcrgap = 0.0;

  // GENERATING VARIABLES FOR LC EXECUTION UTILITY

  float frd;  // available lead gap
  float brd; // available lag gap

  // lead/lag vehicle absent dummy  
  int frd_a = 0; // 1 if lead vehicle absent
  int brd_a = 0; // 1 if lag vehicle absent
  
  
  if(av && av->link()==pv->link()) // if lead vehicle in same link
    { 
      frd = pv->gapDistance(av);
      fcrgap = lcCriticalGap(0,av->currentSpeed() - pv->currentSpeed(),pv);
    }
  else
    {
      frd = 0;
      frd_a = 1;
    }

  if(bv && bv->link()==pv->link())  // if lag vehicle in same link
    { 
      brd = bv->gapDistance(pv);
      bcrgap = lcCriticalGap(1,bv->currentSpeed() - pv->currentSpeed(),pv);
    }
  else
    {
      brd = 0;
      brd_a = 1;
    }

   
  // calculating lane execution probability
  p = 1/(1+exp(-a[8] - a[9]*(frd + brd - fcrgap - bcrgap)  - a[10]*pv->currentSpeed() - a[12]*frd_a ));

  double k;
  k = (double)theRandomizer->urandom(0,1);
  if (k <= p)
   { return 1;}
  else
   { return 0;}

}
