//-*-c++-*------------------------------------------------------------
// TS_KeyInteractor.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <DRN/DRN_DrawingArea.h>
#include "TS_KeyInteractor.h"
#include "TS_Symbols.h"
#include "TS_Interface.h"
#include "TS_Menu.h"

Boolean TS_KeyInteractor::keyPress(KeySym keysym)
{
  if (DRN_KeyInteractor::keyPress(keysym)) {
	return True;
  }
  switch (keysym)
	{
	case XK_p:
	case XK_P:
	  {
		((TS_Menu *)theMenu)->browsePath(NULL, NULL, NULL);
		break;
	  }
	case XK_v:
	case XK_V:
	  {
		Boolean on = theSymbols()->isVehicleLabelOn().value();
		theSymbols()->isVehicleLabelOn().set(!on);
		drawingArea()->setState(DRN_DrawingArea::REDRAW);
		break;
	  }
	default:
	  {
		return False;
	  }
	}
  return True;
}

#endif // INTERNAL_GUI
