//-*-c++-*------------------------------------------------------------
// NAME: Traffic Simulation
// NOTE: Holds queue information
// AUTH: Qi Yang
// FILE: TS_QueueHead.h
// DATE: Wed Oct 23 20:24:22 1996
//--------------------------------------------------------------------

#ifndef TS_QUEUEHEAD_HEADER
#define TS_QUEUEHEAD_HEADER

#include <cstdio>
#include <iostream>
using namespace std;

class Reader;
class TS_Lane;
class TS_Vehicle;

class TS_QueueHead
{

   public:

      TS_QueueHead() { }
      ~TS_QueueHead() { }

      static double stepSize() { return stepSize_; }
      static double nextTime() { return nextTime_; }
      static void nextTime(double d) { nextTime_ = d; }
      static void scheduleNextTime() { nextTime_ += stepSize_; }

      static int nElements() { return nElements_; }
      static TS_QueueHead* element(int i) { return (elements_ + i); }
      static void load(Reader&);
      static FILE *fpo() { return fpo_; }
      static void close();

      static void reportQueues();

      inline float firstMaxSpeed() { return firstMaxSpeed_; }
      float lastMaxSpeed() { return lastMaxSpeed_; }
      float maxGap() { return maxGap_; }

      inline TS_Lane* lane() { return lane_; }
      void read(Reader&);
      void print(std::ostream &os = cout);

      int report();
  

   protected:

      static double stepSize_;	// freq for report
      static double nextTime_;	// next report time
      static FILE *fpo_;	// file pointer
      static int nElements_;	// number of queue heads
      static TS_QueueHead* elements_; // array of queue heads
  
      TS_Lane* lane_;		// container
      float position_;		// start position
      float firstMaxSpeed_;	// max speed of first vehicle
      float lastMaxSpeed_;	// speed threshold
      float maxGap_;		// gap threshold

      float distance_;		// dist from dn end
};

#endif
