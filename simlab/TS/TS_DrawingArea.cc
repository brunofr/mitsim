//-*-c++-*------------------------------------------------------------
// TS_DrawingArea.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include "TS_DrawingArea.h"
#include "TS_Legend.h"
#include "TS_KeyInteractor.h"

TS_DrawingArea::TS_DrawingArea(Widget parent)
   : DRN_DrawingArea(parent)
{
   theDrawingArea = this;
}

DRN_KeyInteractor* TS_DrawingArea::newKeyInteractor()
{
   return new TS_KeyInteractor( this );
}

DRN_Legend* TS_DrawingArea::newLegend()
{
   return new TS_Legend(this);
}

#endif // INTERNAL_GUI
