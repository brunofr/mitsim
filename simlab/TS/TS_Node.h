//-*-c++-*------------------------------------------------------------
// NAME: Microscopic Traffic Simulator
// AUTH: Qi Yang
// FILE: TS_Node.h
// DATE: Mon Jun 10 22:05:51 1996
//--------------------------------------------------------------------

#ifndef TS_NODE_HEADER
#define TS_NODE_HEADER

#ifdef INTERNAL_GUI
#include <DRN/DRN_Node.h>
#else
#include <GRN/RN_Node.h>
#endif

class TS_Link;
class TS_Vehicle;

#ifdef INTERNAL_GUI
class TS_Node : public DRN_Node
#else
class TS_Node : public RN_Node
#endif
{
   public:

#ifdef INTERNAL_GUI
      TS_Node() : DRN_Node() { }
#else
      TS_Node() : RN_Node() { }
#endif

      ~TS_Node() { }
};

#endif







