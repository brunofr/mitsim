//-*-c++-*------------------------------------------------------------
// NAME: Microcscopic Traffic Simulator
// AUTH: Qi Yang
// FILE: TS_FileManager.h
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#ifndef TS_FILEMANAGER_HEADER
#define TS_FILEMANAGER_HEADER

#include <fstream>
#include <Tools/GenericSwitcher.h>
#include <vector> //IEM(Jun19)
using namespace std;

class GenericVariable;

class TS_FileManager : public GenericSwitcher
{

#ifdef INTERNAL_GUI
  friend class TS_OutputDialog;
#endif
  friend class TS_Engine;

public:

  TS_FileManager();
  ~TS_FileManager() { }

  void set(char **ptr, const char *s);

  char* title() { return title_; }
  void title(const char *s) { set(&title_, s); }

  char* moeSpecFile() { return moeSpecFile_; }
  char* segmentTravelTimesFile() { return segmentTravelTimesFile_; }
  char* trajectoryFile() { return trajectoryFile_; }
  char* transitTrajectoryFile() { return transitTrajectoryFile_; }
  char* busStopArrivalFile() { return busStopArrivalFile_; }
  char* signalPriorityFile() { return signalPriorityFile_; }
  char* queueFile() { return queueFile_; }
  char* depRecordFile() { return depRecordFile_; }
  char* sensorVRCFile() { return sensorVRCFile_; }
  char* assignmentMatrixFile() { return assignmentMatrixFile_; }  // Angus
  char* laneChangingFile() { return laneChangingFile_; }  // tomer for LC
  char* laneUtilitiesFile() { return laneUtilitiesFile_; }  // gunwoo - a cfunction to get the lane utilities output file
  char* stateDumpFile() { return stateDumpFile_; }
  char* state3d() { return state3d_; }

  void readMasterFile();

  // This overload the function in base class and will be classed
  // by the parser

  int parseVariable(GenericVariable &gv);

  void openOutputFiles();
  void closeOutputFiles();

  std::ofstream& osVehicle() { return osVehicle_; }
  std::ofstream& osSensor() { return osSensor_; }
  std::ofstream& osSensor(int intervalType) { if(!intervalType) return osSensor_; else return *osSensorPtrs_[intervalType-1]; }
                                                        //IEM(Jun19) vector version
  std::ofstream& osVRCSensor() { return osVRCSensor_; }
  std::ofstream& osAssignmentMatrix() { return osAssignmentMatrix_; }  // Angus
  std::ofstream& osLaneChanging() { return osLaneChanging_; }  // tomer for LC 
  std::ofstream& osLaneUtility() { return osLaneUtility_; }  // gunwoo - a call to the lane utility output stream 
  std::ofstream& osSegmentStatistics() { return osSegmentStatistics_; }
  std::ofstream& osPathRecord() { return osPathRecord_; }

  void sensorFile(const char *s) { set(&sensorFile_, s); }
  void sensorVRCFile(const char *s) { set(&sensorVRCFile_, s); }
  void assignmentMatrixFile(const char *s) { set(&assignmentMatrixFile_, s); }  // Angus
  void laneChangingFile(const char *s) { set(&laneChangingFile_, s); }  // tomer temp for LC
  void laneUtilitiesFile(const char *s) { set(&laneUtilitiesFile_, s); }  // gunwoo - opening the output file for writing
  void linkFlowTravelTimesFile(const char *s) {
	set(&linkFlowTravelTimesFile_, s);
  }
  void segmentTravelTimesFile(const char *s) {
	set(&segmentTravelTimesFile_, s); 
  }
  void segmentStatisticsFile(const char *s) {
	set(&segmentStatisticsFile_, s); 
  }
  void queueFile(const char *s) {
	set(&queueFile_, s); }
  void linkTravelTimesFile(const char *s) {
	set(&linkTravelTimesFile_, s);
  }
  void state3d(const char *s) {
	set(&state3d_, s);
  }

  void depRecordFile(const char *s) { set(&depRecordFile_, s); }
  void vehicleFile(const char *s) { set(&vehicleFile_, s); }
  void pathRecordFile(const char *s) { set(&pathRecordFile_, s); }
  void trajectoryFile(const char *s) { set(&trajectoryFile_, s); }
  void transitTrajectoryFile(const char *s) { set(&transitTrajectoryFile_, s); }
  void busStopArrivalFile(const char *s) { set(&busStopArrivalFile_, s); }
  void signalPriorityFile(const char *s) { set(&signalPriorityFile_, s); }
  void moeOutputFile(const char *s) { set(&moeOutputFile_, s); }

  void moeSpecFile(const char *s) { set(&moeSpecFile_, s); }

private:

  char* title_;

  char* moeSpecFile_;
  char* stateDumpFile_;

  // OUTPUT FILES

  char* moeOutputFile_;
  char* vehicleFile_;
  char* sensorFile_;
  std::vector <std::string> sensorFiles_; //IEM(Jun19) Additional file names
  char* sensorVRCFile_;
  char* assignmentMatrixFile_;  // Angus
  char* laneChangingFile_;  // tomer for LC
  char* laneUtilitiesFile_;  // gunwoo
  char* segmentStatisticsFile_;
  char* linkFlowTravelTimesFile_;
  char* linkTravelTimesFile_;
  char* segmentTravelTimesFile_;
  char* state3d_;
  char* pathRecordFile_;
  char* trajectoryFile_;
  char* transitTrajectoryFile_;
  char* busStopArrivalFile_;
  char* signalPriorityFile_;
  char* queueFile_;
  char* depRecordFile_;

  int parseVariable(char ** varptr, GenericVariable &gv);

  // OUTPUT FILE STREAMS

  std::ofstream osVehicle_;	// vehicle log file
  std::ofstream osSensor_;	// sensor log file
  std::vector<std::ofstream*> osSensorPtrs_; //IEM(Jun19) Additional log files for additional sensors
  std::ofstream osVRCSensor_;	// VRC data log file
  std::ofstream osAssignmentMatrix_;	// output for assignment matrix (Angus)
  std::ofstream osLaneChanging_;	// tomer for LC
  std::ofstream osLaneUtility_;	// gunwoo 
  std::ofstream osSegmentStatistics_; // statistics for each period
  std::ofstream osPathRecord_;	// entry and travel time on links

  int isEqual(const char *s1, const char *s2);
};

extern TS_FileManager * theFileManager;

#endif
