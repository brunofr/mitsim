//-*-c++-*------------------------------------------------------------
// NAME: Microscopic Traffic Simulator
// AUTH: Qi Yang
// FILE: TS_Parameter.C
// DATE: Tue Feb 20 08:28:15 1996
//--------------------------------------------------------------------

#include <iostream>
#include <cstdlib>
#include <new>
#include <Tools/Math.h>
#include <Tools/ToolKit.h>
#include <Tools/Random.h>
#include <Tools/Scaler.h>
#include <Tools/GenericVariable.h>
#include <Tools/VariableParser.h>
#include <GRN/RN_Lane.h>   // VR 03/20/07

#ifdef INTERNAL_GUI
#include <Xmw/XmwFont.h>
#include <DRN/DRN_DrawingArea.h>
#endif

#include "TS_Parameter.h"
#include "TS_Network.h"
#include "TS_Link.h"
#include "TS_VehicleLib.h"
#include "TS_Vehicle.h"
#include "TS_BusLib.h"
#include "TS_Exception.h"
#include "TS_Engine.h"
#include "TS_FileManager.h"
#include "TS_Lane.h"   //  VR 03/20/07


TS_Parameter * theParameter = NULL;
Random* theRandomizer = 0;

TS_Parameter::TS_Parameter()
  : Parameter(),
    heavyVehicleLength_(9.144),
    nVehicleClasses_(0),
    nBusClasses_(0),
    nDriverGroups_(0),
    rndParams_(NULL),
    vehicleLib_(NULL),
    busLib_(NULL),
    detectedMinSpeed_(0.1),
    minResponseDistance_(5.0),
    familiarity_(0.5),
    cfLower_(0.4),
    cfUpper_(1.36),
    accGradeFactor_(0.09296),	// grade to m/sec/sec
    accScaler_(1.0),
    decScaler_(1.0),
    mlcParams_(NULL),
    mlcYieldBins_(0),
    mlcYieldProbs_(NULL),
    dlcParams_(NULL),
    dlcKazi_(NULL),
    ffAccParams_(NULL),
    kaziNosingParams_(NULL),
    anitaNosingParams_(NULL), //Anita initiate fm
    nosingParams_(NULL),
    loadingParams_(NULL),
    lcYieldingProb_(NULL),
    sDelayMu_(NULL),
    complianceRates_(NULL),   
    targetGapAccParams_(NULL), 
    criticalGapParams_(NULL), 
    laneUtilityParams_(NULL), 
    targetGapParams_(NULL),
    targetLaneParams_(NULL),//cfc june 3
    courtesyMergeParams_(NULL),//cfc apr 06
  arterialParams_(NULL)// cfc dec 06

{ 
}

/* TS_Vehicle data for vehile type i */

TS_VehicleLib *
TS_Parameter::vehicleLib(int i)
{
  if (i >= 0 && i < nVehicleClasses()) {
	return (vehicleLib_ + i);
  } else {
	cerr << "Error:: Unknown vehicle class " << i << endl;
	theException->exit(1);
  }
  return (NULL);		/* to make compiler happy */
}

TS_BusLib *
TS_Parameter::busLib(int i)
{
  if (i >= 0 && i < nBusClasses()) {
	return (busLib_ + i);
  } else {
	cerr << "Error:: Unknown bus class " << i << endl;
	theException->exit(1);
  }
  return (NULL);		/* to make compiler happy */
}

void
TS_Parameter::vehicleClassCDF(int i, float x)
{
  vehicleClassCDF_[i] = x;
}

void
TS_Parameter::load()
{
  theRandomizer = theRandomizers[Random::Behavior];
  const char * filename = ToolKit::optionalInputFile(name_);
  if (filename) {
	VariableParser vp(this);
	vp.parse( filename );
  } else {
	theException->exit(1);
  }
}


int
TS_Parameter::parseVariable(GenericVariable &gv)
{
  if (Parameter::parseVariable(gv) == 0) return 0;

  char * token = compact(gv.name());

  if (isEqual(token, "MinResponseDistance")) {
	minResponseDistance_ = ((float)gv.element()) * lengthFactor_;     

  } else if (isEqual(token, "LoadingModel")) {
	return loadVehicleLoadingModel(gv);

  } else if (isEqual(token, "CFLowerBound")) {
	cfLower_ = gv.element();     

  } else if (isEqual(token, "CFUpperBound")) {
	cfUpper_ = gv.element();     

  } else if (isEqual(token, "CFParameters")) {
	return 0;

  } else if (isEqual(token, "CFKaziParameters")) {
	return loadCFParameters(gv);

  } else if (isEqual(token, "FFAccParams")) {
	return loadFFAccParams(gv);

  } else if (isEqual(token, "MergingModel")) {
	return loadMergingModel(gv);

  } else if (isEqual(token, "UpdateStepSizes")) {
	return loadUpdateStepSizeParams(gv);

  } else if (isEqual(token, "ConstantStateTime")) {
	constantStateTime_ = gv.element();
	return 0;

  } else if (isEqual(token, "LCEntryLaneCheckRightnessProb")) {
	lcEntryLaneCheckRightnessProb_ = gv.element();

  } else if (isEqual(token, "LCMandatoryProbabilityModel")) {
	return loadMLCProbabilityModel(gv);

  } else if (isEqual(token, "LCDiscretionaryLaneChangeModel")) {
	return loadDLCModel(gv);

  } else if (isEqual(token, "LCKaziDLCModel")) {
	return loadKaziDLCModel(gv);

  } else if (isEqual(token, "QiLCGapModels")) {
	return 0;

  } else if (isEqual(token, "KaziLCGapModels")) {
	return loadLCGapModels(gv);

  } else if (isEqual(token, "MLCYieldingProbabilities")) {
	return loadMLCYieldProbabilities(gv);

  } else if (isEqual(token, "LCNosingModel")) {
	return loadMLCNosingModel(gv);

  } else if (isEqual(token, "LCKaziNosingModel")) {
	return loadMLCKaziNosingModel(gv);

  } else if (isEqual(token, "LCAnitaNosingModel")) {
	return loadMLCAnitaNosingModel(gv); //Anita Nosing

  } else if (isEqual(token, "LCYieldingModel")) {
	return loadYieldingModel(gv);

  } else if (isEqual(token, "StartUpDelayParameters")) {
	return loadStartUpDelays(gv);

  } else if (isEqual(token, "Familiarity")) {
	familiarity_ = gv.element();

  } else if (isEqual(token, "ComplianceRates")) {
	return loadComplianceRates(gv);

  } else if (isEqual(token, "TimeToTreatYellowAsRed")) {
	yellowStopHeadway_ = gv.element();

  } else if (isEqual(token, "MaximumTollBoothDelay")) {
	maxTollBoothDelay_ = gv.element();

  } else if (isEqual(token, "HeadwayBufferLowerBound")) {
	hBufferLower_ = gv.element();

  } else if (isEqual(token, "HeadwayBufferUpperBound")) {
	hBufferUpper_ = gv.element();

  } else if (isEqual(token, "JamDensity")) {
	jamDensity_ = gv.element();
	jamDensity_ *= densityFactor_;

  } else if (isEqual(token, "MinimumSpeed")) {
	minSpeed_ = gv.element();
	minSpeed_ *= speedFactor_;

  } else if (isEqual(token, "DetectedMinimumSpeed")) {
	detectedMinSpeed_ = gv.element();
	detectedMinSpeed_ *= speedFactor_;

  } else if (isEqual(token, "DriverGroups")) {
	return loadDriverGroups(gv);

  } else if (isEqual(token, "VehicleClasses")) {
	return loadVehicleClassAttributes(gv);

	// Dan: adding input parameters for bus dwell time calculations
  } else if (isEqual(token, "BusClasses")) {
	return loadBusClassAttributes(gv);

  } else if (isEqual(token, "MinHeavyVehicleLength")) {
	heavyVehicleLength_ = gv.element();
	heavyVehicleLength_ *= lengthFactor_ ;

  } else if (isEqual(token, "AccTableSpeedToMetersPerSecond")) {
	speedConverting_ = gv.element();

  } else if (isEqual(token, "AccTableAccToMetersPerSqSecond")) {
	accConverting_ = gv.element();

  } else if (isEqual(token, "AccelerationGradeFactor")) {
	accGradeFactor_ = gv.element();
	accGradeFactor_ *= accConverting_;
 
  } else if (isEqual(token, "GradeScaler")) {
	return gradeScaler_.init(gv);

  } else if (isEqual(token, "SpeedScaler")) {
	return speedScaler_.init(gv, speedConverting_);

  } else if (isEqual(token, "AccelerationScaler")) {
	accScaler_ = gv.element();

  } else if (isEqual(token, "DecelerationScaler")) {
	decScaler_ = gv.element();

  } else if (isEqual(token, "MaximumAcceleration")) {
	return loadMaximumAcceleration(gv);

  } else if (isEqual(token, "NormalDeceleration")) {
	return loadNormalDeceleration(gv);

  } else if (isEqual(token, "MaximumDeceleration")) {
	return loadMaximumDeceleration(gv);

  } else if (isEqual(token, "LimitingSpeed")) {
	return loadLimitingSpeed(gv);

  } else if (isEqual(token, "LaneSpeedRatio")) {
	return loadLaneSpeedRatios(gv);

  } else if (isEqual(token, "FontSizes")) {

#ifdef INTERNAL_GUI
	return theDrawingArea->loadScalableFontSizes(gv);
#else
	return 0;
#endif
  
  } else if (isEqual(token, "TargetGapAcceleration")) {
	return loadTargetGapAccModel(gv);
  
  } else if (isEqual(token, "CriticalGaps")) {
	return loadCriticalGapModel(gv);

  } else if (isEqual(token, "LaneUtilityModel")) {
	return loadLaneUtilityModel(gv);
  
  } else if (isEqual(token, "TargetGapModel")) {
	return loadTargetGapModel(gv);
  } else if (isEqual(token, "TargetLaneUtilityModel")) {
    return loadTargetLaneModel(gv);// cfc june 3
    // } else if (isEqual(token, "AnticipationTimeLowerBound")) {
    //anticipationTimeLower_ = gv.element();

    //} else if (isEqual(token, "AnticipationTimeUpperBound")) {
    //anticipationTimeUpper_ = gv.element();// cfc 06
  
  } else if (isEqual(token, "CourtesyMergeModel")) {
  return loadCourtesyMergeModel(gv);// cfc 06

  } else if (isEqual(token, "ArterialModel")) {
  return loadArterialModel(gv);// cfc dec06
  	
  } else {
	return 1;
  }
  return 0;
}


int
TS_Parameter::loadDLCModel(GenericVariable &gv)
{
  int n = gv.nElements();
  if (n < 12) return error(gv.name());
  dlcParams_ = new float [n];
  for (int i = 0; i < n; i ++) {
	dlcParams_[i] = gv.element(i);
  }
  dlcParams_[8] *= lengthFactor_;
  dlcParams_[11] *= speedFactor_;
  return 0;
}


int
TS_Parameter::loadVehicleLoadingModel(GenericVariable &gv)
{
  int n = gv.nElements();
  if (n < 3) return error(gv.name());
  loadingParams_ = new float [n];
  for (int i = 0; i < n; i ++) {
	loadingParams_[i] = gv.element(i);
  }
  loadingParams_[2] *= speedFactor_;
  return 0;
}


// Load attributes data for each vehicle class

int
TS_Parameter::loadVehicleClassAttributes(GenericVariable &gv)
{

  nVehicleClasses_ = gv.nElements() / 8;

  /* Acceleration, deceleration, etc. */

  vehicleLib_ = new TS_VehicleLib[nVehicleClasses_];

  /* Percentage of vehicles for each class */

  vehicleClassCDF_ = new float [nVehicleClasses_];

#ifdef INTERNAL_GUI
  vehicleColors_ = new Pixel [nVehicleClasses_];
#endif

  for (int i = 0; i < nVehicleClasses_; i ++) {
	vehicleLib_[i].loadAttributes(gv.elements(i * 8));

#ifdef INTERNAL_GUI
	vehicleColors_[i] = theColorTable->color((float) (i + 1) /
	  (float) nVehicleClasses_);
#endif

  }
  if (!AproxEqual(vehicleClassCDF_[nVehicleClasses_-1], (float)1.0)) {
	cerr << "Error: Sum of vehicle mix (";
	cerr << vehicleClassCDF_[nVehicleClasses_-1];
	cerr << ") does not equal to 1.0." << endl;
	return 1;
  }

  return 0;
}

// Dan: function for loading bus class attributes
int
TS_Parameter::loadBusClassAttributes(GenericVariable &gv)
{
  nBusClasses_ = gv.nElements() / 9;   

  busLib_ = new TS_BusLib[nBusClasses_];

  for (int i = 0; i < nBusClasses_; i ++) {
	busLib_[i].loadAttributes(gv.elements(i * 9));
  }

  return 0;
}

/*
 * Read driver behavior parameters
 */

int
TS_Parameter::loadDriverGroups(GenericVariable &gv)
{
  int i, n = gv.nElements() ;
  nDriverGroups_ = n / NumRndVars;
  rndParams_ = new float[n];
  for (i = 0; i < n; i ++)  {
	rndParams_[i] = gv.element(i);
  }
  return 0;
}
//cfc 06 reads one additional column for anticipation time but no chaneg in code

/*
 * Parameters in the car-following model.
 */

//row 3 and 4 for WZ ; cfc19Dec
int
TS_Parameter::loadCFParameters(GenericVariable &gv)
{
  int num = 5;

  // li - add this for intersection delay--
  #ifdef USE_IS_DELAY
    num = 7;
  #endif
  //---------------------------------------

  if (gv.nElements() != 4 * num) return error(gv.name());
  for (int i = 0; i < 4 ; i ++) {
	accParams_[i] = new float[num] ;
	for (int j = 0; j < num; j ++) {
	  accParams_[i][j] = gv.element(i * num + j) ;
	  // cout <<"cfc"<< accParams_[i][j] <<endl;
	}
  }
  return 0;
}

	
/*
 * Read lane speed ratio. This ratio is used to find the free mean
 * free speed for each lane based on the total number of lanes and
 * position of the lane concerned
 */

int
TS_Parameter::loadLaneSpeedRatios(GenericVariable &gv)
{
  maxNumOfLanes_ = ((int)(sqrt(1.0 + 8.0 * gv.nElements())) - 1) / 2;
  int n = maxNumOfLanes_ * (maxNumOfLanes_ + 1) / 2;
  if (n != gv.nElements()) return error(gv.name());
  laneSpeedRatio_ = new float[n];
  for (int i = 0; i < n; i ++)
	{
      laneSpeedRatio_[i] = gv.element(i);
	}
  return 0;
}


/*
 * Load parameter for startup delays
 */

int
TS_Parameter::loadStartUpDelays(GenericVariable &gv)
{
  sDelayNum_ = (gv.nElements() - 1) / 2;
  sDelayMax_ = gv.element(0);
  sDelayMu_    = new float[sDelayNum_];
  for (int i = 0; i < sDelayNum_; i ++) {
	sDelayMu_[i] = gv.element(i * 2 + 2); 
  }

  // li - add the parameter for intersection delay--
  #ifdef USE_IS_DELAY
    sDelayTheta_ = gv.element(sDelayNum_ * 2 -1 );
  #endif
  //------------------------------------------------
  return 0;
}

float
TS_Parameter::startupDelay(int pos)
{
  if (pos < 0) pos = 0;
  else if (pos >= sDelayNum_) pos = sDelayNum_ - 1;

  float delay = theRandomizer->erandom(sDelayMu_[pos]);
  if (delay < sDelayMax_) {
  // li - add this for intersection delay--
  	#ifdef USE_IS_DELAY
		return sDelayTheta_ * delay;
	#endif
  //---------------------------------------
	return delay;
  } else {
	return theRandomizer->urandom(0, sDelayMax_);
  }
}

/*
 * Loading maximum acceleration table
 */

int
TS_Parameter::loadMaximumAcceleration(GenericVariable &gv)
{
  int num = nSpeedGroups();
  if (gv.nElements() != nVehicleClasses_ * num) {
	return error(gv.name());
  }
  for (int i = 0; i < nVehicleClasses_; i ++) {
	vehicleLib_[i].loadMaximumAcceleration(gv.elements(i * num));
  }
  return 0;
}


/*
 * Loading normal deceleration table
 */

int
TS_Parameter::loadNormalDeceleration(GenericVariable &gv)
{
  int num = nVehicleClasses_ * nSpeedGroups();
  if (gv.nElements() != num) return error(gv.name());
  for (int i = 0; i < nVehicleClasses_; i ++) {
	vehicleLib_[i].loadNormalDeceleration(
										  gv.elements(i * nSpeedGroups()));
  }
  return 0;
}


/*
 * Loading maximum deceleration table
 */

int
TS_Parameter::loadMaximumDeceleration(GenericVariable &gv)
{
  int num = nVehicleClasses_ * nSpeedGroups();
  if (gv.nElements() != num) return error(gv.name());
  for (int i = 0; i < nVehicleClasses_; i ++) {
	vehicleLib_[i].loadMaximumDeceleration(gv.elements(i * nSpeedGroups()));
  }
  return 0;
}


/*
 * Loading limiting speed table
 */

int
TS_Parameter::loadLimitingSpeed(GenericVariable &gv)
{
  int num = nVehicleClasses_ * nGradeGroups();
  if (gv.nElements() != num) return error(gv.name());
  for (int i = 0; i < nVehicleClasses_; i ++) {
	vehicleLib_[i].loadMaxSpeed(gv.elements(i * nGradeGroups()));
  }
  return 0;
}


// Return the probability to nose in

float
TS_Parameter::lcNosingProbCmf(float pos, int num, float tm)
{
  if (num < 0) num = - num;
  else if (num == 0) num = 1;

  float z = nosingParams_[1] +
	nosingParams_[2] * num + nosingParams_[3] * tm ;

  float p = 0.5 * (1.0 + cos(ONE_PI * pow(pos, z))) ;

  return p;
}


int TS_Parameter::loadMLCKaziNosingModel(GenericVariable &gv)
{
  int n = gv.nElements() ;
  if (n != 6) return error(gv.name());
  kaziNosingParams_ = new float[n] ; // changed from "(n)" to "[n]" by Angus
  for (int i = 0; i < n; i ++) {
	kaziNosingParams_[i] = gv.element(i) ;
  }
  return 0 ;
}
float
TS_Parameter::lcNosingProb(float dis, float lead_rel_spd, float gap,
						   int num)
{
  if (num < 0) num = - num;
  else if (num == 0) num = 1;

  float *b = kaziNosingParams_ ;
  float rel_spd = (lead_rel_spd > 0) ? 0 : lead_rel_spd ;
  float rm_dist_impact = 10 - 10 / (1.0 + exp(b[2] * dis)) ;
  if (gap > 100.0) {			// 100 meters
	gap = 100.0 ;
  }
  float u = b[0] + b[1] * rel_spd + b[3] * rm_dist_impact + b[4] * gap +
	b[5] * (num - 1);
  float p = 1.0 / (1 + exp(-u)) ;
  return p ;
}

//Anita's initiate force merging fucntion
int TS_Parameter::loadMLCAnitaNosingModel(GenericVariable &gv)
{
  int n = gv.nElements() ;
  if (n != 3) return error(gv.name());
  anitaNosingParams_ = new float[n] ; // changed from "(n)" to "[n]" by Angus
  for (int i = 0; i < n; i ++) {
	anitaNosingParams_[i] = gv.element(i) ;
  }
  return 0 ;
}
float
TS_Parameter::lcNosingProb(float HVDummy, TS_Vehicle *pv)
{
  
  float *b = anitaNosingParams_ ;
  float u = b[0] + b[1] * HVDummy  +b[2] * pv->aggresiveness();
  float p = 1.0 / (1 + exp(-u)) ;
  

// cfc- verify forced function

	    if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
	    {
	      ostream& os = theFileManager->osLaneUtility();

	      
	      {
		os  << "99" << endc
		    << pv->code() << endc
		    << Fix(theSimulationClock->currentTime(), 1.0) << endc
		  // << b[0] << endc
		  //  << b[1] << endc
		  //  << b[2] << endc
		  //  << HVDummy   << endc
	          //  << pv->aggresiveness() << endc
		    << p << endc
		    <<10001<< endl;
	  }
    
	 }

return p ;

}


// Parameter's in Kazi's LC gap model (see Kazi's PhD thesis)

// scale:	scaling the critical gap
// alpha:   minimum gap (meter)
// lambda:  rem dist impact = 10 [1 - 1/{1+exp(lambda * dis)}]
// beta0:	constant
// beta1:   coef of rem dist impact
// beta2:	coef of speed difference (m/s, d/s-u/s)
// beta3:	coef of negative speed difference (m/s, d/s-u/s)
// beta4:	coef of positive speed difference (m/s, d/s-u/s)
// sigma:	std dev of generic random term

int
TS_Parameter::loadLCGapModels(GenericVariable &gv)
{
  const int nrows  = 4 ;
  const int ncols = 9 ;
  if (gv.nElements() != nrows * ncols) return error(gv.name());
  int i, j, k = 0;
  for (i = 0; i < nrows; i ++) {
	lcGapModels_[i] = new float [ncols];
	for (j = 0; j < ncols; j ++) {
	  lcGapModels_[i][j] = gv.element(k++);
	}
  }
  return 0;
}

// Kazi's LC gap model (see Kazi's MS thesis)

float
TS_Parameter::lcCriticalGap(
  int type,			// 0=leading 1=lag + 2=mandatory (mask)
  float dis,		// from critical pos
  float spd,		// spd of the follower
  float dv,			// spd difference from the leader
  TS_Vehicle *pv)
{
  float *a = lcGapModels_[type] ;
  float *b = lcGapModels_[type] + 3 ;
  float rem_dist_impact = (type < 3) ?
	0.0 : (1.0 - 1.0 / (1 + exp(a[2] * dis)));
  float dvNegative = (dv < 0) ? dv : 0.0;
  float dvPositive = (dv > 0) ? dv : 0.0;
  float gap = b[0] + b[1] * rem_dist_impact +
              b[2] * dv + b[3] * dvNegative + b[4] *  dvPositive;


  float u = gap + theRandomizer->nrandom(0, b[5]);
  float cri_gap ;

  if (u < -4.0) cri_gap = 0.0183 * a[0] ;      // exp(-4)=0.0183
  else if (u > 6.0) cri_gap = 403.4 * a[0] ;   // exp(6)=403.4
  else cri_gap = a[0] * exp(u);

  if (cri_gap < a[1]) return a[1] ;
  else return cri_gap ;
}

//Varun 07  -- Third level - LC Execution

#ifndef USE_KAZI_MODELS 
int
TS_Parameter::lcGapAcc(TS_Vehicle *pv)		
{ 
  float *a = criticalGapParams();
   TS_Lane *plane;

  if (pv->status(STATUS_LEFT)) {
	plane = (TS_Lane*)pv->lane()->left();
  } else if (pv->status(STATUS_RIGHT)) {
	plane = (TS_Lane*)pv->lane()->right();
  } else {
	return 0;			// No request for lane change 
  }
  TS_Vehicle* av = pv->findFrontBumperLeader(plane);
  TS_Vehicle* bv = pv->findFrontBumperFollower(plane);
  double p;

  float fcrgap;  // lead critical gap
  float bcrgap;  // lag critical gap

  fcrgap = 0.0;
  bcrgap = 0.0;

  float frd;  // diff. between lead gap and lead critical gap
  float brd; // diff. between lag gap and lag critical gap

  float dv_a;//lead speed if lead in same link
  float dv_b;//lag speed if lag in same link
  int frd_a = 0; // 1 if lead vehicle absent
  int brd_a = 0; // 1 if lag vehicle absent
  
  if(av && av->link()==pv->link())
    { 
      frd = pv->gapDistance(av);
      fcrgap = lcCriticalGap(0,av->currentSpeed() - pv->currentSpeed(),pv);
      dv_a=av->currentSpeed();
    }
  else
    {
      frd = 0;
      frd_a = 1;
      dv_a=0;
    }

  if(bv && bv->link()==pv->link())
    { 
      brd = bv->gapDistance(pv);
      bcrgap = lcCriticalGap(1,bv->currentSpeed() - pv->currentSpeed(),pv);
      dv_b=bv->currentSpeed();
    }
  else
    {
      brd = 0;
      brd_a = 1;
      dv_b=0;
    }

   
  
  // p = 1/(1+exp(-a[8] - a[12]*(frd + brd - fcrgap - bcrgap)  - a[10]*pv->currentSpeed() - a[11]*frd_a ));
 p = 1/(1+exp(-(a[8] + a[10]*pv->currentSpeed() + a[11]*((dv_b-dv_a) > 0))));

  double k;
  k = (double)theRandomizer->urandom(0,1);
  if (k <= p)
   { return 1;}
  else
   { return 0;}
 }
         
  //#ifndef USE_KAZI_MODELS //was USE_TOMER_MODELS before : cfc june 3
        //gunwoo - this one is previous cfc and Tomer gap acceptance model

//#ifndef USE_KAZI_MODELS //was USE_TOMER_MODELS before : cfc june 3
        //gunwoo - this one is previous cfc and Tomer gap acceptance model
float
TS_Parameter::lcCriticalGap(
  int type,  // 0=leading 1=lag, 
  float dv,//spd diff from leader
 TS_Vehicle *pv)		
	// the subject vehicle 
{
  float *a = criticalGapParams() ;
  // VR 03/22/2007
  //   TS_Lane *plane;

  //  if (pv->status(STATUS_LEFT)) {
  //	plane =(TS_Lane*) pv->lane()->left();
  //  } else if (pv->status(STATUS_RIGHT)) {
  //	plane =(TS_Lane*) pv->lane()->right();
  //  } else {
  //	return 0;			// No request for lane change 
  //  }

  // TS_Vehicle* neighbor;
  //  if (type == 0)
  //    {
  //  neighbor = pv->findFrontBumperLeader(plane);
  //  }
  //  if (type == 1)
  //  {
      //  neighbor = pv->findFrontBumperFollower(plane);
  //  }

  // float dvNegative = (dv < 0) ? dv : 0.0;
  // float dvPositive = (dv > 0) ? dv : 0.0;

  float gap =0.0;

  float error;  
 
  

  //float maxdiff = a[8]; 

  switch (type){
case 0: {     // lead gap
  // if (neighbor && neighbor->link() == pv->link()) // check if vehicle is in the same link
    
    gap = a[0] + a[1] * dv + a[2] * pv->aggresiveness();//Varun 07
    error = theRandomizer->nrandom(0, a[3]);
    gap = gap + error;
    
    break;
}
  case 1: {     // lag gap

    gap = a[4] + a[5]  * dv  + a[6] * pv->aggresiveness();//Varun 07
    error = theRandomizer->nrandom(0, a[7]);
    gap = gap + error;

    break;
  }
    
  }
 

  float cri_gap  = exp(gap);

  //deleted  gunwoo - writing the critical gap function

	 

  return cri_gap ;
}


// this is the initiate courtesy model : cfc 

float
TS_Parameter::lcAnticipatedGap( 
  float dv,//spd diff with lag
  float density, //density in target lane
  float remainingDist_,
  float lagAcc_,
    TS_Vehicle *pv //subject veh		
  )  
{
  float *a = courtesyMergeParams() ;


  float dvPositive = (dv > 0) ? dv : 0.0;
float dist_=remainingDist_/10.0;
float dens=density/100.0;

  float gap =0.0;

  float error;  

  //float maxdiff = a[8]; 

 

  
    gap = a[0]  + a[1] * dvPositive + a[2]*dens+(a[3]*dist_)/(1+exp(a[6]+a[7]* pv->aggresiveness()))+a[4] * pv->aggresiveness();
    error = theRandomizer->nrandom(0, a[5]);
    gap = gap + error;
 
    
  float cri_gap  = exp(gap);

  // cfc- anticipated gap function

	    if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
	    {
	      ostream& os = theFileManager->osLaneUtility();

	      
	      
	      {
		os  << "88" << endc
		    << pv->code() << endc
		    << Fix(theSimulationClock->currentTime(), 1.0) << endc
		  //  << a[0] << endc
		  //  << a[1] << endc
		  //  << a[2] << endc
		  //  << a[3] << endc
		  //  << a[4] << endc
                  //  << a[5] << endc
                  //  << a[6] << endc
                  //  << a[7] << endc
		  //  << dvPositive   << endc
		  //  <<dens<<endc
		  //  <<dist_<<endc
		  //  << pv->aggresiveness() << endc
		    << cri_gap << endc
		  //  << error <<endc
		    <<10001<< endl;
	  }
    
	 }

  return cri_gap ;
}//end of initiate courtesy


// gunwoo - this one is single stage gap acceptance model when vehicles are in on-ramp
// The parameter for gap acceptance starts with a[10] in paralib.dat
// Anita - added merge_type and switch for merge_type.
float
TS_Parameter::lcCriticalGap(
	 int merge_type,  //0=normal, 1 = forced, 2 = courtesy
   int type,       // 0=leading 1=lag,
   float dv,        // speed difference
   float dis,      //distance to MLC
   float acc,      //accelearation of lag
   float avgspd,   //average speed in lane 6
   float spd,     // subject speed
   TS_Vehicle *pv) //aggressiveness);
{
  float *a = criticalGapParams() ;

  float dvNegative = (dv < 0) ? dv : 0.0;
  float dvPositive = (dv > 0) ? dv : 0.0;
  float avgPos = ((avgspd-spd) >0) ? (avgspd-spd) : 0.0;
  float accNeg = (acc>0) ? acc : 0.0; //acceleration of lag vehicle (positive)
  float dist    = Min(dis/10.0,300.0);//gunwoo- changed remaining distance units(meter->10meters)

  float gap =0.0;

  float error;  

  //float maxdiff = a[8]; 

switch(merge_type){

//Normal Gap	
case 0: { switch (type){
	
	case 0: {     // lead gap

    	gap = a[10] + (a[11]/(1+exp(-avgPos))) + a[12] * dvNegative + (a[13] * dist)/(1+exp(a[35]+a[36]* pv->aggresiveness())) + a[14] * pv->aggresiveness();
 	    error = theRandomizer->nrandom(0, a[15]);
 	    gap = gap + error;
  break;
         }
  case 1: {     // lag gap
  	  gap = a[16] + a[17] * dvPositive + a[18] * dvNegative + (a[19] * dist)/(1+exp(a[37]+a[38]* pv->aggresiveness())) + a[20] * accNeg + a[21] * pv->aggresiveness();
 	    error = theRandomizer->nrandom(0, a[22]);
 	    gap = gap + error;
 	break;
          }
                     }
break;
    }
    
//Forced Gap
case 1: { switch (type){
	
	case 0: {     // lead gap
    	gap = a[23] + (a[11]/(1+exp(-avgPos))) + a[12] * dvNegative + (a[13] * dist)/(1+exp(a[35]+a[36]* pv->aggresiveness())) + a[24] * pv->aggresiveness();
 	    error = theRandomizer->nrandom(0, a[25]);
 	    gap = gap + error;
  break;
         }
  case 1: {     // lag gap
  	  gap = a[26] + a[17] * dvPositive + a[18] * dvNegative + (a[19] * dist)/(1+exp(a[37]+a[38]* pv->aggresiveness())) + a[20] * accNeg + a[27] * pv->aggresiveness();
 	    error = theRandomizer->nrandom(0, a[28]);
 	    gap = gap + error;
 	break;
          }
                     }
break;
    }
   
//Courtesy Gap
case 2: { switch (type){
	
	case 0: {     // lead gap
    	gap = a[29] + (a[11]/(1+exp(-avgPos))) + a[12] * dvNegative + (a[13] * dist)/(1+exp(a[35]+a[36]* pv->aggresiveness())) + a[30] * pv->aggresiveness();
 	    error = theRandomizer->nrandom(0, a[31]);
 	    gap = gap + error;
  break;
         }
  case 1: {     // lag gap
  	  gap = a[32] + a[17] * dvPositive + a[18] * dvNegative + (a[19] * dist)/(1+exp(a[37]+a[38]* pv->aggresiveness())) + a[20] * accNeg + a[33] * pv->aggresiveness();
 	    error = theRandomizer->nrandom(0, a[34]);
 	    gap = gap + error;
 	break;
          }
                     }
break;
    }
  } //end of switch merge type
  
  float cri_gap  = exp(gap);
  //this part is for verification

	    if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES))
	    {
	      ostream& os = theFileManager->osLaneUtility();
	      switch (merge_type){
	      
	      // Normal Gap	
	      case 0: {	
	           switch (type){
	      
	           case 0: {  
		           os  << "18" << endc
		     			<< pv->code() << endc
		    			<< Fix(theSimulationClock->currentTime(), 1.0) << endc
			     //	<< a[10] << endc //lead constant
			     //	<< a[11] << endc //AverageSpeed in rightmost lane - suject speed (positive)
			     //        << a[12] << endc //12 lead gap - negative speed difference (negative) 
			     //	<< a[13] << endc //13 distance to MLC (lead)
			     //         << a[35] << endc //35 lead gap -dist constant   
			     //	<< a[36] << endc //36 lead gap -dist niu
			     //	<< a[14] << endc //14 lead gap - aggresiveness  (alpha)   
			     //	<< a[15] << endc //15 lead gap - standard deviation    (sigma)
			     // 	<< dv   << endc 
			     //	<< dis/10  << endc
			     //	<< acc  << endc
			     //	<< avgspd << endc
			     //	<< spd     << endc
			     //	<< pv->aggresiveness() << endc
		    			<< cri_gap << endc
			     //	<< error <<endc
		    			<<10001<< endl;
						break;
 						      }
	      		case 1: {
					os  << "19" << endc
		    			<< pv->code() << endc
		    			<< Fix(theSimulationClock->currentTime(), 1.0) << endc
					  //	<< a[16] << endc //lag gap - constant                   
					  //		<< a[17] << endc //lag gap - positive speed difference (positive) 
					  //	<< a[18] << endc //lag gap - negative speed difference (negative)
					  //		<< a[19] << endc //distance to MLC (lag)
					  //		<< a[37] << endc //37 lag gap -dist constant   
					  //	<< a[38] << endc //38 lag gap -dist niu
					  //	<< a[20] << endc //acceleration of lag vehicle (positive)  
					  //	<< a[21] << endc //lag gap - aggresiveness  
					  //	<< a[22] << endc //lag gap - standard deviation    
					  //	<< dv   << endc
					  //	<< dis/10  << endc
					  //	<< acc  << endc
					  //	<< avgspd << endc
					  //	<< spd     << endc
					  //	<< pv->aggresiveness() << endc
		    			<< cri_gap << endc
					  //	<< error  <<endc
		    			<<10001<<endl;
		    	break;
	               }
	      } //switch type loop
	      break;
	      }//switch merge_type loop
	      
	      //Forced Gap
	      case 1: {	
	           switch (type){
	      
	           case 0: {  
		           os  << "28" << endc
		     			<< pv->code() << endc
		    			<< Fix(theSimulationClock->currentTime(), 1.0) << endc
			     //	<< a[23] << endc
			     //	<< a[11] << endc
			     //	<< a[12] << endc
			     //	<< a[13] << endc
			     //         << a[35] << endc //35 lead gap -dist constant   
			     //		<< a[36] << endc //36 lead gap -dist niu
			     //	<< a[24] << endc //lag gap - aggresiveness
			     //	<< a[25] << endc // lag gap - standard deviations
			     //	<< dv   << endc
			     //	<< dis/10  << endc
			     //	<< acc  << endc
			     //	<< avgspd << endc
			     //	<< spd     << endc
			     //	<< pv->aggresiveness() << endc
		    			<< cri_gap << endc
			     //	<< error <<endc
		    			<<10001<< endl;
						break;
 						      }
	      		case 1: {
			    os  << "29" << endc
		    			<< pv->code() << endc
		    			<< Fix(theSimulationClock->currentTime(), 1.0) << endc
			      //	<< a[26] << endc
			      //	<< a[17] << endc
			      //	<< a[18] << endc
			      //	<< a[19] << endc
			      //	<< a[37] << endc //37 lag gap -dist constant   
			      //	<< a[38] << endc //38 lag gap -dist niu
			      //	<< a[20] << endc
			      //	<< a[27] << endc // lead gap - aggresiveness
			      //	<< a[28] << endc // lead gap - standard deviation
			      //	<< dv   << endc
			      //	<< dis/10 << endc
			      //	<< acc  << endc
			      //	<< avgspd << endc
			      //	<< spd     << endc
			      //	<< pv->aggresiveness() << endc
		    			<< cri_gap << endc
			      //	<< error  <<endc
		    			<<10001<<endl;
	break;
	               }
	      } //switch type loop
	      break;
	      }//switch merge_type loop
		       

 //Courtesy  Gap
	      case 2: {	
	           switch (type){
	      
	           case 0: {  
		           os  << "38" << endc
		     			<< pv->code() << endc
		    			<< Fix(theSimulationClock->currentTime(), 1.0) << endc
			     //	<< a[29] << endc
			     //	<< a[11] << endc
			     //	<< a[12] << endc
			     //	<< a[13] << endc
			     //         << a[35] << endc //35 lead gap -dist constant   
			     //	<< a[36] << endc //36 lead gap -dist niu
			     //	<< a[30] << endc //lag gap - aggresiveness
			     //	<< a[31] << endc // lag gap - standard deviations
			     //	<< dv   << endc
			     //	<< dis/10  << endc
			     //	<< acc  << endc
			     //	<< avgspd << endc
			     //	<< spd     << endc
			     //	<< pv->aggresiveness() << endc
		    			<< cri_gap << endc
			     //	<< error <<endc
		    			<<10001<< endl;
						break;
 						      }
	      		case 1: {
					os  << "39" << endc
		    			<< pv->code() << endc
		    			<< Fix(theSimulationClock->currentTime(), 1.0) << endc
					  //	<< a[32] << endc
					  //			<< a[17] << endc
					  //	<< a[18] << endc
					  //	<< a[19] << endc
					  //	<< a[37] << endc //37 lag gap -dist constant   
					  //	<< a[38] << endc //38 lag gap -dist niu
					  //	<< a[20] << endc
					  //	<< a[33] << endc // lead gap - aggresiveness
					  //	<< a[34] << endc // lead gap - standard deviation
					  //	<< dv   << endc
					  //	<< dis/10 << endc
					  //	<< acc  << endc
					  //	<< avgspd << endc
					  //	<< spd     << endc
					  //	<< pv->aggresiveness() << endc
		    			<< cri_gap << endc
					  //	<< error  <<endc
		    			<<10001<<endl;
		    	break;
	               }
	      } //switch type loop
	      break;
	      }
	    } //switch merge_type loop

}
  return cri_gap ;
}

#endif


int TS_Parameter::loadKaziDLCModel(GenericVariable &gv)
{
  int n = gv.nElements();
  if (n < 13) return error(gv.name());
  dlcKazi_ = new float [n];
  for (int i = 0; i < n; i ++) {
	dlcKazi_[i] = gv.element(i);
  }
  return 0;
}


int TS_Parameter::loadFFAccParams(GenericVariable &gv)
{
  int n = gv.nElements();
  if (n < 7) return error(gv.name());
  ffAccParams_ = new float [n];
  for (int i = 0; i < n; i ++) {
	ffAccParams_[i] = gv.element(i);
  }
  return 0;
}


float
TS_Parameter::lcMinGap(int type)
{
  float *b = lcGapModels_[type];
  return b[2] * b[0];
}


// Loading MLC probability model

int
TS_Parameter::loadMLCProbabilityModel(GenericVariable &gv)
{
  int i, n = gv.nElements();
  if (n < 5) return error(gv.name());

  mlcParams_ = new float[n];
  for (i = 0; i < n; i ++) {
	mlcParams_[i] = gv.element(i);
  }
  mlcParams_[0] *= lengthFactor_;
  mlcParams_[1] *= lengthFactor_;

  return 0;
}


int
TS_Parameter::loadMLCNosingModel(GenericVariable &gv)
{
  int i, n = gv.nElements();
  if (n < 10) return error(gv.name());
  nosingParams_ = new float[n];
  for (i = 0; i < n; i ++) {
	nosingParams_[i] = gv.element(i);
  }
  nosingParams_[3] /= 60.0;	// we use second internally
  nosingParams_[8] *= lengthFactor_;
  nosingParams_[9] *= lengthFactor_;
  return 0;
}


int
TS_Parameter::loadMergingModel(GenericVariable &gv)
{
  int i, n = gv.nElements();
  if (n < 4) return error (gv.name());
  mergingParams_ = new float[n];
  for (i = 0; i < n; i ++) {
	mergingParams_[i] = gv.element(i);
  }
  mergingParams_[0] *= lengthFactor_;
  mergingParams_[1] *= lengthFactor_;
  return 0;
}


// Lane change probability
// Requires: dis is greater than mlcParams_[0].

float
TS_Parameter::mlcProbCmf(float dis, int num, float y)
{
  dis = dis - mlcParams_[0];
  num = (num > 0) ? num : -num;
  float delta = 1.0 + mlcParams_[2] * num + mlcParams_[3] * y;
  delta *= mlcParams_[1];

  // Probability for getting into the right lane

  float cmf = exp(-dis * dis / (delta * delta));
  return cmf;
}


// mlc distance for lookahead vehicles
 
float 
TS_Parameter::mlcDistance(float n)
{
float dis = mlcParams_[0] + n*(mlcParams_[1] - mlcParams_[0]);
return dis;
}


/*
 * Returns a speed ratio for lane i of a n-lane segment
 */

float
TS_Parameter::laneSpeedRatio(int n, int i)
{
  if (n > maxNumOfLanes_) n = maxNumOfLanes_;
  if (i >= n) i = n - 1;
  return laneSpeedRatio_[n  * (n - 1) / 2 + i];
}


// Load parameters for calculating step sizes of car-following and
// lane changes

int
TS_Parameter::loadUpdateStepSizeParams(GenericVariable &gv)
{
  int i, n = gv.nElements();
  if (n < 16) return error(gv.name());
  updateStepSizeParams_ = new float[n];
  for (i = 0; i < n; i ++) {
	updateStepSizeParams_[i] = gv.element(i);
  }
  return 0;
}

// Returns a randomly assigned step size according to the distribution
// for various type: 0=dec, 1=acc, 2=uniform, 3=stopped

float*
TS_Parameter::updateStepSizeParams(int type)
{
  // If you make change here, make sure you update the
  // TS_Vehicle::updateStepSize() too

  const int nrows = 4;		// 0=mean, 1=stddev, 2=min, 3=max
  return updateStepSizeParams_ + type * nrows;
}


// Load the parameters for courtesy yielding

int
TS_Parameter::loadYieldingModel(GenericVariable &gv)
{
  int i, n = gv.nElements();
  if (n < 2) return error(gv.name());
  lcYieldingProb_ = new float[n];
  for (i = 0; i < n; i ++) {
	lcYieldingProb_[i] = gv.element(i);
  }
  return 0;
}

// Returns the probability for courtesy yeilding

float
TS_Parameter::lcYieldingProb(int yielding)
{
  return lcYieldingProb_[yielding];
}


// Load compliance rates to various rules and signs

int
TS_Parameter::loadComplianceRates(GenericVariable &gv)
{
  int i, n = gv.nElements();
  if (n < 10) return error(gv.name());
  complianceRates_ = new float[n];
  for (i = 0; i < n; i ++) {
	complianceRates_[i] = gv.element(i);
  }
  return 0;
}


int TS_Parameter::loadMLCYieldProbabilities(GenericVariable &gv)
{
  // number of yield probabilities

  mlcYieldBins_ = gv.nElements();

  mlcYieldProbs_ = new float [mlcYieldBins_];

  float cdf = 0.0;
  for (int i = 0; i < mlcYieldBins_; i ++) {
	cdf += (float)gv.element(i); 
	mlcYieldProbs_[i] = cdf;
  }

  return 0;
}

int
TS_Parameter::loadTargetGapAccModel(GenericVariable &gv)
{
  int i, n = gv.nElements();
  if (n < 13) return error(gv.name());
  targetGapAccParams_ = new float[n];
  for (i = 0; i < n; i ++) {
	targetGapAccParams_[i] = gv.element(i);
  }
  return 0;
}

int
TS_Parameter::loadCriticalGapModel(GenericVariable &gv) //gunwoo - we add more variables in critical gaps, so check the no.of parameters
{
  int i, n = gv.nElements();
  if (n < 39) return error(gv.name());    //anita- previous gap acceptance model has 10, gunwoo's has 23 in paralib.dat.cfc 06 35 , May 17
  criticalGapParams_ = new float[n];
  for (i = 0; i < n; i ++) {
	criticalGapParams_[i] = gv.element(i);
  }
  return 0;
}

int
TS_Parameter::loadLaneUtilityModel(GenericVariable &gv)
{
  int i, n = gv.nElements();
  if (n < 18) return error(gv.name());
  laneUtilityParams_ = new float[n];
  for (i = 0; i < n; i ++) {
	laneUtilityParams_[i] = gv.element(i);
  }
  return 0;
}

// cfc june 3

int
TS_Parameter::loadTargetLaneModel(GenericVariable &gv)
{
  int i, n = gv.nElements();
  if (n < 22) return error(gv.name());
  targetLaneParams_ = new float[n];
  for (i = 0; i < n; i ++) {
	targetLaneParams_[i] = gv.element(i);
	//	cout<< "tl: "<< targetLaneParams_[i]<<endl;
  }
  return 0;
}

int
TS_Parameter::loadTargetGapModel(GenericVariable &gv)
{
  int i, n = gv.nElements();
  if (n < 7) return error(gv.name());
  targetGapParams_ = new float[n];
  for (i = 0; i < n; i ++) {
	targetGapParams_[i] = gv.element(i);
  }
  return 0;
}
// VR: 06/26/07 additional parameter added to target gap model



unsigned int TS_Parameter::mlcNumberToYield()
{
  return theRandomizer->drandom(mlcYieldBins_, mlcYieldProbs_);
}


int
TS_Parameter::loadCourtesyMergeModel(GenericVariable &gv)
{
  int i, n = gv.nElements();
  if (n < 6) return error(gv.name());
  courtesyMergeParams_ = new float[n];
  for (i = 0; i < n; i ++) {
	courtesyMergeParams_[i] = gv.element(i);
  }
  return 0;
}


int
TS_Parameter::loadArterialModel(GenericVariable &gv)
{
  int i, n = gv.nElements();
  if (n < 20) return error(gv.name());
  arterialParams_ = new float[n];
  for (i = 0; i < n; i ++) {
	arterialParams_[i] = gv.element(i);
	cout<< "tl: "<<arterialParams_[i]<<endl;//debug cfc dec06
  }
  return 0;
}









