//-*-c++-*------------------------------------------------------------
// NAME: Traffic Simulation
// NOTE: 
// AUTH: Qi Yang
// FILE: TS_Lane.C
// DATE: Thu Oct  3 15:25:32 1996
//--------------------------------------------------------------------

#include <new>
#include <cstring>
using namespace std;

#include <Tools/ToolKit.h>
#include <Tools/Reader.h>

#include <Tools/Math.h>

#include <GRN/Constants.h>
#include <GRN/RN_SurvStation.h>
#include <GRN/RN_CtrlStation.h>

#include "TS_Network.h"
#include "TS_Node.h"
#include "TS_Link.h"
#include "TS_Segment.h"
#include "TS_Lane.h"
#include "TS_Vehicle.h"
#include "TS_Signal.h"
#include "TS_Sensor.h"
#include "TS_Incident.h"
#include "TS_TollBooth.h"
#include "TS_BusStop.h"  //margaret
#include "TS_Parameter.h"

#include "TS_QueueHead.h"
#include "TS_Queue.h"

TS_Lane::TS_Lane() :
#ifdef INTERNAL_GUI
   DRN_Lane(),
#else
   RN_Lane(),
#endif
   lRules_(0),
   rRules_(0),
   nChanges_(NULL),
   firstVehicle_(NULL),
   lastVehicle_(NULL),
   nVehicles_(0)
{
}


void
TS_Lane::calcStaticInfo()	// virtual
{
#ifdef INTERNAL_GUI
   DRN_Lane::calcStaticInfo();
#else
   RN_Lane::calcStaticInfo();
#endif
   calcFreeSpeed();
   maxSpeed_ = speed_ = freeSpeed_;
}


/*
 *--------------------------------------------------------------------
 * Returns the last vehicle in the downstream lanes. The vehicle
 * closest to the upstream end is returned.
 *--------------------------------------------------------------------
 */

TS_Vehicle*
TS_Lane::lastInDnLanes()
{
   TS_Vehicle *last = NULL, *pv;
   double mindis = FLT_INF;
   double dis;
   TS_Lane * dlane;
   register int i;

   for (i = 0; i < nDnLanes(); i ++) {
      dlane = (TS_Lane *)dnLane(i);
      pv = dlane->lastVehicle();
      if (pv != NULL) {
		 dis = dlane->length() - (pv->distance() + pv->length());
		 if (dis < mindis) {
			mindis = dis;
			last = pv;
		 }
      }
   }
   return (last);
}


/*
 *--------------------------------------------------------------------
 * Returns the first vehicle that comes from upstream lanes. The
 * vehicle that has the smallest headway is returned.
 *--------------------------------------------------------------------
 */

TS_Vehicle *
TS_Lane::firstFromUpLanes()
{
   TS_Vehicle *first = NULL, *pv;
   double minheadway = FLT_INF;
   register int i;

   for (i = 0; i < nUpLanes(); i ++) {
      pv = ((TS_Lane*)upLane(i))->firstVehicle();
      if (pv != NULL) {
		 if (pv->distance() < minheadway) {
			minheadway = pv->distance();
			first = pv;
		 }
      }
   }
   return (first);
}


/*
 *--------------------------------------------------------------------
 * Returns the first vehicle that comes from competing upstream
 * lanes. The vehicle that has the smallest headway is returned.
 *
 * CAUTION: This function is to be used in the merging operation.
 * Only lanes match the link type "fac" are examined.
 *--------------------------------------------------------------------
 */

TS_Vehicle*
TS_Lane::firstFromOtherUpLanes(TS_Lane* plane,
							   int fac = LINK_TYPE_MASK)
{
   TS_Vehicle *first = NULL, *pv;
   double minheadway = FLT_INF;
   TS_Lane * olane;
   register int i;

   for (i = 0; i < nUpLanes(); i ++) {
      olane = (TS_Lane*)upLane(i);

      /* Skip "plane" and the lanes that are not of type "fac" */

      if (olane != plane &&
		  (olane->linkType() & fac) &&
		  (pv = olane->firstVehicle()) != NULL) {
		 if (pv->distance() < minheadway) {
			minheadway = pv->distance();
			first = pv;
		 }
      }
   }
   return (first);
}


/*
 *----------------------------------------------------------------------
 * Returns the limiting speed in this lane.  Speed is in meter/sec.
 * This function overload the one defined in RN_Lane to consider lane
 * position
 *
 *----------------------------------------------------------------------
 */

void
TS_Lane::calcFreeSpeed() 
{
   float mean = segment_->freeSpeed();
   float ratio = theParameter->laneSpeedRatio(
      segment_->nLanes(),
      localIndex());
   freeSpeed_ =  mean * ratio;
}

/*
 *--------------------------------------------------------------------
 * Check if any of its downstream lanes is connected to kth outgoing
 * arc of the downstream node. It returns 1 if one of its downstreams
 * lane is connected, or 0 if none of them are connected.
 * 
 * CAUTION: The argument is kth outgoing arc, not kth link of the
 * network.
 *-------------------------------------------------------------------- */

int
TS_Lane::isDownLanes2Link(int k)
{
   register int i;
   for (i = 0; i < nDnLanes(); i ++) {
      if (((TS_Lane *)dnLane(i))->nChanges(k) == 0) return 1;
   }
   return 0;
}


int
TS_Lane::isDownLanesInLink(TS_Link *plink)
{
   register int i;
   for (i = 0; i < nDnLanes(); i ++) {
      if (dnLane(i)->link() == plink) return 1;
   }
   return 0;
}

/* computes whether the lane connects to next link in the vehicle path */
int
TS_Lane::isDownLanesInNextLink(TS_Link *plink) // VR : 01/06/07
{  
   register int i;
   register int k; 
   if (this->segment() == link()->endSegment())
    {
      k = this->isDownLanesInLink(plink);
      return k;
    }
   
    for (i = 0; i < nDnLanes(); i ++) {  
  	 k = dnLane(i)->isDownLanesInNextLink(plink);
         if ( k == 1) {
         return k;
         }
       
    }     
    return 0;    
}      

// Create and assign value to nChanges_. See "TS_Lane.h" for its
// definition.

void
TS_Lane::createNumberOfChanges2OutArcs()
{
   TS_Node * dnode = (TS_Node *)link()->dnNode();
   short int ndlinks = dnode->nDnLinks();
   short int change;
   TS_Link * target;
   TS_Lane * plane;
   register short int k;

   // alloacate memory for nChanges_

   if (ndlinks > 1) {

	  nChanges_ = new short int[ndlinks];

   } else {

	  // this is a special case to deal with boundary link which
	  // has lane drops

	  nChanges_ = new short int[1];

	  if (nDnLanes() > 0) {
		 nChanges_[0] = 0;
	  } else if ((plane = left()) && plane->nDnLanes() > 0) {
		 nChanges_[0] = -1;
	  } else if ((plane = right()) && plane->nDnLanes() > 0) {
		 nChanges_[0] = 1;
	  } else {
		 nChanges_[0] = 0;
	  }

	  return;
   }

   if (segment()->downstream() == NULL) { // the last segment
	  k = 0; 
	  while (k < ndlinks) {
		 target = (TS_Link *)dnode->dnLink(k);
		 if (this->isDownLanesInLink(target)) {
			nChanges_[k] = 0;
		 } else	{		// check neighbor lanes
			// check left lanes
			plane = this;
			change = 0;
			while (plane = (TS_Lane *)plane->left()) {
			   change --;
			   if (plane->isDownLanesInLink(target)) {
				  nChanges_[k] = change;
				  goto found_1;
			   }
			}
			// check right lanes
			plane = this;
			change = 0;
			while (plane = (TS_Lane*)plane->right()) {
			   change ++;
			   if (plane->isDownLanesInLink(target)) {
				  nChanges_[k] = change;
				  goto found_1;
			   }
			}
			nChanges_[k] = 0;
		 }

		found_1:
		 k ++;
	  }

   } else {	// Not the last segment

	  k = 0;
	  while (k < ndlinks) {

		 // Check if any downstream lane is eventually connected to
		 // kth outgoing arc.

		 if (this->isDownLanes2Link(k))	 {
			nChanges_[k] = 0;
		 } else {		// check neighbor lanes
			plane = this;
			change = 0;
			while (plane = (TS_Lane *)plane->left()) {
			   change --;
			   if (plane->isDownLanes2Link(k)) {
				  nChanges_[k] = change;
				  goto found_2;
			   }
			}
			plane = this;
			change = 0;
			while (plane = (TS_Lane*)plane->right()) {
			   change ++;
			   if (plane->isDownLanes2Link(k)) {
				  nChanges_[k] = change;
				  goto found_2;
			   }
			}
			nChanges_[k] = 0;
		 }

		found_2:
		 k ++;
	  }
   }
}


void
TS_Lane::createLaneUseRules()
{
   TS_Lane* plane;
   unsigned int crules;

   /* Right side */

   plane = this;
   while (plane = (TS_Lane *)plane->right()) {
      crules = plane->cRules() & VEHICLE_LANE_USE;
      if (crules) {		/* regulated lane */
		 rRules_ |= crules;	/* mark allowed vehicle types */
      } else {			/* no regulation */
		 rRules_ = VEHICLE_LANE_USE; /* every one can use */
		 break;
      }
   }

   /* Left side */

   plane = this;
   while (plane = (TS_Lane *)plane->left()) {
      crules = (plane->cRules()) & VEHICLE_LANE_USE;
      if (crules) {		/* regulated lane */
		 lRules_ |= crules;	/* mark allowed vehicle type */
      } else {			/* no regulation */
		 lRules_ = VEHICLE_LANE_USE; /* every one can use */
		 break;
      }
   }
}


/*
 *--------------------------------------------------------------------
 * Finds which lane is correct for a vehicle "pv" and returns:
 *
 *	-n = number of lane changes to right required
 *	 0 = this lane is fine
 *	+n = number of lane changes to left requied
 *
 * CAUTION: (1) This lane may not be the lane vehicle "pv" is in. (2)
 * This lane must be in the same link that vehicle "pv" is in. (3)
 * This function is valid only for checking the lanes in the upstream
 * segments of pv->nextLink.
 *
 *--------------------------------------------------------------------
 */

short int
TS_Lane::isWrongLane(TS_Vehicle * pv)
{
   RN_Link *nextlink = pv->nextLink();

   if (nextlink == NULL || nextlink == link()) {
      // current link is last in path (or vehicle is lost)

     if (segment() == link()->endSegment()) { // (Angus)
       // current segment is the last in link
       return 0; // no lane change necessary

     } else { // more segments ahead (lane drop is possible)
       return nChanges_[0]; // lane changes needed to reach end of link
     }
     
   } else { // more links ahead

     return nChanges_[nextlink->dnIndex()];
     // number of lane changes to be make

   }
}


/*
 *-------------------------------------------------------------------
 * Check lane use regulation in terms of vehicle type. It returns
 *    -1 if a right lane is correct
 *     0 if this lane is correct
 *    +1 if a left lane is corerct
 * If a lane has no lane use regulation, this function returns
 * always 0.
 *-------------------------------------------------------------------
 */

int
TS_Lane::doesNotAllow(TS_Vehicle* pv)
{
   int laneuse = (pv->types() & VEHICLE_LANE_USE);
   if ((rules_ & VEHICLE_LANE_USE) &&
       ((rules_ & laneuse) == 0)) {

      /* This lane has rules that do not allow "pv" to use the
       * lane. Check the neighbor lanes
       */

      if (rRules_ & laneuse) {
		 /* Right lane has no regulation or the vehicle "pv" is
		  * allowed to use the right lane */
		 return 1;
      } else if (lRules_ & laneuse) {
		 /* Left lane has no regulation or the vehicle "pv" is allowed
		  * to use the left lane. */
		 return -1;
      } else {
		 /* No lane is allowed. Stay in current lane. */
		 return 0;
      }
   } else {
      /* No rules in this lane or "pv" is allowed to use the lane
       * according to the rules */
      return 0;
   }
}



// Check if there is a critical downstream control device or event.
// This could be a red LUS sign or active incident. Start searching
// from the device pointed by ctrl.  It returns:
//
//  -1 = bad event and requires a mandatory lane change
//   0 = nothing serious
//   1 = bad event and requires a discretionary lane change

int
TS_Lane::isThereBadEventAhead(TS_Vehicle* pv,
							  float *pdis2stop, float *pvis)
{
   CtrlList ctrl = pv->nextCtrl();
   double x = pv->distanceFromDownNode();

   // Forget about all these signs I have already passed.

   while (ctrl &&
		  (*ctrl)->distance() > x) {
      ctrl = ctrl->next();
   }

   // For familiar drivers, visibility is doubled
   
   float vis;
   if (pv->attr(ATTR_FAMILIARITY)) {
      vis = 2.0 * theParameter->visibility();
   } else {
      vis = theParameter->visibility();
   }

   // Check the current link. Save the distance.

   float thevis;		// visbility of the critical event
   float dis2stop;
   int blocked = pv->isThereBadEventInList(this,
	  ctrl, vis, x, &dis2stop, &thevis);

   // Check the downstream link

   if (blocked >= 0 &&
       pv->nextLink() &&
       x < vis) {
	  int bad = pv->isThereBadEventInList(this,
		 pv->nextLink()->ctrlList(), vis,
		 x + pv->nextLink()->length(), &dis2stop, &thevis);
	  if (bad) blocked = bad;
   }

   if (isDropped()) {		// this lane is to be dropped
      if (blocked >= 0) {
		 if (pv->segment() == segment()) {
			dis2stop = pv->distance();
		 } else {
			dis2stop = pv->distance() + length();
		 }
		 thevis = theParameter->visibility();
		 blocked = -1;
      }
   }

   if (pv->attr(ATTR_GLU_RULE_COMPLY) && doesNotAllow(pv)) {
      if (pv->segment() == segment()) {
		 dis2stop = 0;
		 thevis = theParameter->visibility();
      } else if (dis2stop > pv->distance()) {
		 dis2stop = pv->distance();
		 thevis = theParameter->visibility();
      }
      blocked = -1;				  // ????
   }

   if (pdis2stop) *pdis2stop = dis2stop;
   if (pvis) *pvis = thevis;

   return blocked;
}


// This function search and report queue length upstream of vehicle
// "pv" recursively.  The search will start from the first "Stopped"
// vehicle and terminate when a moving vehicle is found or it reaches
// the network boundary.  Depth First Search is used.

int
TS_Lane::searchQueue(TS_QueueHead* queueHead,
					 TS_Queue* frontQueue, TS_Vehicle* pv)
{
   TS_Lane * plane;

   // Find the queue in this lane

   TS_Queue * queue = calcQueueInfo(queueHead, frontQueue, pv);
   int num = queue->nVehicles();

   setState(STATE_MARKED);
   if (queue->isFull() ||
       queue->isEmpty() &&
       queue->nVehiclesAhead() == 0) {
      int n = 0;
      for (int i = 0; i < nUpLanes(); i ++) {
		 plane = (TS_Lane *)upLane(i);
		 if (!(plane->hasMarker())) {
			n += plane->searchQueue(
			  queueHead,
			  queue,
			  plane->firstVehicle());
		 }
      }
      if (n > 0) num += n;
      else if (num > 0) queue->report();
   } else if (!(queue->isEmpty())) {
      queue->report();
   }
   
   delete queue;
   return (num);
}


TS_Queue*
TS_Lane::calcQueueInfo(TS_QueueHead* queueHead,
					   TS_Queue* frontQueue, TS_Vehicle* pv)
{
   TS_Queue* queue = new TS_Queue(queueHead, frontQueue, this);

   // Find the first vehicle in queue

   if (!frontQueue || frontQueue->isEmpty()) {
      while (pv &&
			 pv->currentSpeed() > queueHead->firstMaxSpeed()) {
		 pv = pv->trailing();
      }
      if (pv) {			// this guy is the first in queue
		 queue->start(1);
		 queue->update(pv);
		 pv = pv->trailing();
      }
   }

   while (pv &&
		  pv->currentSpeed() < queue->lastMaxSpeed() &&
		  pv->gapDistance() < queue->maxGap()) {
      queue->update(pv);	// this guy is in queue
      pv = pv->trailing();
   }

   return (queue);
}


TS_Vehicle*
TS_Lane::findUpVehicle(double pos) 
{
   TS_Vehicle * pv = firstVehicle_;
   while (pv && pv->distance() < pos) {
      pv = pv->trailing();
   }
   return (pv);
}


double
TS_Lane::density() 
{
   return (1000.0 * (double)nVehicles_ / length());
}

double
TS_Lane:: dischargeRate(TS_Lane *qlane)
{
  int lindex=qlane->code();
  switch(lindex){
	case 5:	{
	  dischargeRate_=2.3;
	  break;
	}
	case 6:	{
	  dischargeRate_=1.9;
	  break;
	}
	case 7:	{
	  dischargeRate_=2.2;
	  break;
	}
	case 8:	{
	  dischargeRate_=2.2;
	  break;
	}
	case 9:	{
	  dischargeRate_=2.2;
	  break;
	}
	case 10:	{
	  dischargeRate_=2.0;
	  break;
	}
	case 11:	{
	  dischargeRate_=2.3;
	  break;
	}
	case 12:	{
	  dischargeRate_=1.9;
	  break;
	}
	case 13:	{
	  dischargeRate_=1.8;
	  break;
	}
	case 14:	{
	  dischargeRate_=2.75;
	  break;
	}
	case 15:	{
	  dischargeRate_=1.74;
	  break;
	}
	case 16:	{
	  dischargeRate_=1.96;
	  break;
	}
	case 17:	{
	  dischargeRate_=1.82;
	  break;
	}
	case 22:	{
	  dischargeRate_=2.3;
	  break;
	}
	case 23:	{
	  dischargeRate_=2.33;
	  break;
	}
	case 24:	{
	  dischargeRate_=2.16;
	  break;
	}
	case 25:	{
	  dischargeRate_=1;
	  break;
	}
	default: { 
	  dischargeRate_=2;
	  break;
	}

	return (1/dischargeRate_);//cfc Jan 07
      }

}
// Mean speed

float
TS_Lane::calcSpeed()
{
   if (nVehicles_ <= 0) return speed_ = freeSpeed();
   double sum = 0.0;
   TS_Vehicle *pv = firstVehicle();
   while (pv) {
	  sum += pv->currentSpeed();
	  pv = pv->trailing();
   }
   return (speed_ = sum / nVehicles_);
}

// queue length -- consider all vehicles ahead of the vehicle with speed lower than threshold 
int 
TS_Lane::queueahead(float pos)  // VR : 01/06/07
{ 
  int queuelength = 0;
  if (nVehicles_ > 0)
  {
    TS_Vehicle* start = firstVehicle();
    while(start && start->currentSpeed() <= 3 && start->position() <= pos)
      {
        queuelength++;
        start = start->trailing();
      }
  }
 
    return queuelength;
}


float
TS_Lane::calcMaxSpeed()
{
  //   float v;
   maxSpeed_ = freeSpeed();
  
   // tomer - this part is very problematic when there is an un even speed 
   // profile across lanes. i will try to run without it. temporarily ???


   // TS_Lane *p;

   //  if (p = (TS_Lane *)left()) {
   //  if (!(rules_ & (VEHICLE_ETC | VEHICLE_HOV)) ||
   //	  (p->rules_ & (VEHICLE_ETC | VEHICLE_HOV))) {
   //	 v = p->speed_ + theParameter->lcMaxLaneSpeedDiff();
   //	 if (maxSpeed_ > v) {
   //		maxSpeed_ = v;
   //	 }
   //  }
   // }
   
   // if (p = (TS_Lane *)right()) {
   //  if (!(rules_ & (VEHICLE_ETC | VEHICLE_HOV)) ||
   //	  (p->rules_ & (VEHICLE_ETC | VEHICLE_HOV))) {
   //	 v = p->speed_ + theParameter->lcMaxLaneSpeedDiff();
   //	 if (maxSpeed_ > v) {
   //		maxSpeed_ = v;
   //	 }
   //  }
   // }

   return maxSpeed_;
}

void
TS_Lane::resetStatistics()
{
   accumulatedSpeed_ = 0.0;
   accumulatedDensity_ = 0.0;
}

void
TS_Lane::printStatistics(ostream& os)
{
  double d = averageDensity() / theParameter->densityFactor();
   double v = averageSpeed() / theParameter->speedFactor();
  //double d = averageDensity();
  // double v = averageSpeed();
   os << indent << indent << OPEN_TOKEN;
   os << Fix(d, 0.1) << endc
      << Fix(v, 0.1);
   os << CLOSE_TOKEN << endl;
}


double
TS_Lane::averageDensity()
{
   int num = ((TS_Segment *)segment_)->nSamples();
   return (num ? (accumulatedDensity_ / num) : 0.0);
}


double
TS_Lane::averageSpeed()
{
   int num = ((TS_Segment *)segment_)->nSamples();
   return (num ? (accumulatedSpeed_ / num) : freeSpeed());
}

void
TS_Lane::samplingStatistics()
{
   accumulatedDensity_ += density();
   accumulatedSpeed_ += calcSpeed();
}
