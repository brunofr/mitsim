//-*-c++-*------------------------------------------------------------
// TS_Menu.cc
//
// qiyang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <fstream>
#include <cassert>
#include <sys/param.h>

#include <Xmt/Xmt.h>
#include <Xmt/Create.h>
#include <Xmt/Menu.h>
#include <Xmt/Dialogs.h>
#include <Xmt/Dialog.h>

#include <IO/Exception.h>
#include <Tools/ToolKit.h>
#include <Tools/Random.h>

#include <Xmw/XmwDialogManager.h>
#include <Xmw/XmwRandomizer.h>
#include <DRN/DRN_PathDialog.h>

#include "TS_Menu.h"
#include "TS_Interface.h"
#include "TS_Network.h"
#include "TS_Symbols.h"
#include "TS_VehicleDialog.h"
#include "TS_OutputDialog.h"
#include "TS_SetupDialog.h"
#include "TS_AdvancedSetupDialog.h"

TS_Menu::TS_Menu(Widget parent) : DRN_Menu(parent)
{
  Widget popup = XmtNameToWidget(parent, "*drawingArea*popup");

  Widget w;

  // Simulation pane

  w = getItemWidget("Setup");
  assert (w);
  addCallback(w,
	      XmNactivateCallback, &TS_Menu::setup,
	      NULL );

  w = getItemWidget("AdvancedSetup");
  assert (w);
  addCallback(w,
	      XmNactivateCallback, &TS_Menu::advancedSetup,
	      NULL );

  w = getItemWidget("Output");
  assert (w);
  addCallback(w,
	      XmNactivateCallback, &TS_Menu::output,
	      NULL );

  // View pane

  w = getItemWidget("Vehicle");
  assert (w);
  addCallback(w,
	      XmNactivateCallback, &TS_Menu::vehicle,
	      NULL );
  if (popup && (w = getItemWidget(popup, "Vehicle"))) {
    addCallback(w,
		XmNactivateCallback, &TS_Menu::vehicle,
		NULL );
  }

  // File pane

  w = getItemWidget("DumpState");
  assert (w);
  addCallback(w,
	      XmNactivateCallback, &TS_Menu::dumpState,
	      NULL );

  // Tools pane

  w = getItemWidget("BrowsePath");
  assert (w);
  XtAddCallback(w,
		XmNactivateCallback, &TS_Menu::browsePath,
		NULL );
  if (popup && (w = getItemWidget(popup, "BrowsePath"))) {
    XtAddCallback(w,
		  XmNactivateCallback, &TS_Menu::browsePath,
		  NULL );
  }
}


// Static functions

void TS_Menu::browsePath ( Widget, XtPointer, XtPointer )
{
  static DRN_PathDialog dialog(theDrnInterface->widget());
  dialog.post();
}

// Overloaded virtual functions

void TS_Menu::setup ( Widget, XtPointer, XtPointer )
{
  static TS_SetupDialog dialog(theInterface->widget());
  dialog.post();
}

void TS_Menu::advancedSetup ( Widget, XtPointer, XtPointer )
{
  static TS_AdvancedSetupDialog dialog(theInterface->widget());
  dialog.post();
}

void TS_Menu::randomizer ( Widget, XtPointer, XtPointer )
{
  static XmwRandomizer dialog(theInterface->widget(),4);
  dialog.post();
}

void TS_Menu::output ( Widget, XtPointer, XtPointer )
{
  static TS_OutputDialog dialog(theInterface->widget());
  dialog.post();
}

void TS_Menu::vehicle ( Widget, XtPointer, XtPointer)
{
  static TS_VehicleDialog dialog(theInterface->widget());
  dialog.post();
}

void TS_Menu::dumpState ( Widget w, XtPointer, XtPointer )
{
  if (!theNetwork) {
    theInterface->NoNetwork() ;
    return;
  }
  char filename[MAXPATHLEN + 1];
  char directory[MAXPATHLEN + 1];

  filename[0] = '\0';
  directory[0] = '\0';

  if (XmtAskForFilename(w, "openFileDialog", 
			"Please type or select a file for state dump:",
			ToolKit::outDir(), NULL,
			filename, sizeof(filename),
			directory, sizeof(directory),
			"*.dmp", 0,
			NULL)) {
    if (tsNetwork()->dumpNetworkState(filename)) {
      XmtDisplayErrorMsgAndWait(w, "StateDump",
				"Failed saving network state to @B%s@R",
				"Error", "Check permission of the file",
				filename);
    }
  }
}

#endif // INTERNAL_GUI
