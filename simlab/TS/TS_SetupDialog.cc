//-*-c++-*------------------------------------------------------------
// TS_SetupDialog.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <assert.h>

#include <Xmt/Xmt.h>
#include <Xmt/Chooser.h>
#include <Xmt/InputField.h>
#include <Xm/ToggleB.h>

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>
#include <Tools/Math.h>

#include "TS_SetupDialog.h"
#include "TS_Interface.h"
#include "TS_Engine.h"
#include "TS_FileManager.h"
#include "TS_Interface.h"
#include "TS_Parameter.h"
#include "TS_Network.h"
#include "TS_Incident.h"
#include "TS_MoeCollector.h"
#include "TS_Menu.h"

#include <GRN/RN_PathTable.h>
#include <GRN/OD_Table.h>
#include <GRN/OD_Cell.h>
#include <GRN/VehicleTable.h>

//Dan
#include <GRN/BusAssignmentTable.h>
#include <GRN/RN_BusScheduleTable.h>
#include <GRN/RN_BusRouteTable.h>
#include <GRN/RN_BusRunTable.h>

#include <GRN/RN_DynamicRoute.h>

TS_SetupDialog::TS_SetupDialog ( Widget parent )
  : XmwDialogManager(parent, "setupDialog", NULL, 0)
{
  XtVaSetValues(widget_,
				XmNdialogStyle, XmDIALOG_PRIMARY_APPLICATION_MODAL,
				NULL);

  title_ = XtNameToWidget(widget_, "title");
  assert(title_);

  Widget group;
  int i = 0;

  group = XmtNameToWidget(widget_, "*DirectoryGroup");
  assert(group);

  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*paraDir");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*inDir");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*workDir");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*outDir");
		
  group = XmtNameToWidget(widget_, "*InputGroup");
  assert(group);
		
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*paraFile");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*netFile");
		
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*linktimeFile0");
		
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*pathFile");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*odFile");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*vehicleFile");
  flds_.push_back(new XmwInputField);


  //Dan - looking for bus assignment input file?
  flds_[i++]->lookup(group, "*busFile");
  flds_.push_back(new XmwInputField);


  flds_[i++]->lookup(group, "*inciFile");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*moeFile");

  group = XmtNameToWidget(widget_, "*TimingGroup");
  assert(group);

  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*startTime");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*stopTime");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*stepSize");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*pathStepSize");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*microStepSize");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*macroStepSize");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*batchStepSize");

  group = XmtNameToWidget(widget_, "*DataGroup");
  assert(group);

  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*segmentStepSize");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*segmentReportStepSize");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*stateStepSize");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*pointStepSize");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*areaStepSize");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*depRecordStepSize");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*trajectoryStepSize");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*moeStepSize");
  flds_.push_back(new XmwInputField);
  flds_[i++]->lookup(group, "*transitTrajectoryStepSize");
}

TS_SetupDialog::~TS_SetupDialog ( )
{
  for (int i = 0; i < flds_.size(); i ++) {
	delete flds_[i];
  }
}

void TS_SetupDialog::set(char **ptr, const char *s)
{
  ::Copy(ptr, s);
}


// These overload the functions in base class

void TS_SetupDialog::cancel(Widget, XtPointer, XtPointer)
{
  unmanage();
}


void TS_SetupDialog::okay(Widget, XtPointer, XtPointer)
{
  // Copy the data from dialog 

  if (!theSimulationClock->isStarted()) {
	theFileManager->title(XmtInputFieldGetString(title_));

	ToolKit::paradir(flds_[ 0]->get());
	ToolKit::indir(flds_[ 1]->get());
	ToolKit::workdir(flds_[ 2]->get());
	ToolKit::outdir(flds_[ 3]->get());

	set(TS_Parameter::nameptr(), flds_[ 4]->get());
	set(RoadNetwork::nameptr(), flds_[ 5]->get());

	set(RN_DynamicRoute::fileptr(0), flds_[ 6]->get());
	  
	set(RN_PathTable::nameptr(), flds_[ 7]->get());
	set(OD_Table::nameptr(), flds_[ 8]->get());
	set(VehicleTable::nameptr(), flds_[ 9]->get());
      	set(TS_Incident::filenameptr(), flds_[10]->get());
	theFileManager->moeSpecFile(flds_[11]->get());

	theSimulationClock->startTime(flds_[12]->get());
	theSimulationClock->stopTime(flds_[13]->get());
	theSimulationClock->stepSize(flds_[14]->getTime());
	theEngine->pathStepSize_ = flds_[15]->getTime();

	theEngine->segmentStatisticsSamplingStepSize_ = flds_[19]->getTime();
	theEngine->segmentStatisticsReportStepSize_ = flds_[20]->getTime();
	theEngine->stateStepSize_ = flds_[21]->getTime();
	theEngine->survPointStepSize_ = flds_[22]->getTime();
	theEngine->survAreaStepSize_ = flds_[23]->getTime();
	theEngine->depRecordStepSize_ = flds_[24]->getTime();
	theEngine->trajectoryStepSize_ = flds_[25]->getTime();
	if (theMoeCollector) {
	  theMoeCollector->stepSize(int(flds_[26]->getTime()));
	}
        //Dan
	theEngine->transitTrajectoryStepSize_ = flds_[27]->getTime();
	set(RN_BusRouteTable::nameptr(), flds_[28]->get());
	set(RN_BusScheduleTable::nameptr(), flds_[29]->get());
	set(RN_BusRunTable::nameptr(), flds_[30]->get());
	set(BusAssignmentTable::nameptr(), flds_[31]->get());
  }
   
  theEngine->microStepSize_ = flds_[16]->getTime();
  theEngine->macroStepSize_ = flds_[17]->getTime();
  theEngine->batchStepSize_ = flds_[18]->getTime();

  unmanage();

  theMenu->needSave();
}


void TS_SetupDialog::help(Widget, XtPointer, XtPointer)
{
  theInterface->openUrl("setup", "mitsim.html");
}


void TS_SetupDialog::post()
{
  XmtInputFieldSetString(title_, theFileManager->title());

  flds_[ 0]->set(ToolKit::paradir());
  flds_[ 1]->set(ToolKit::indir());
  flds_[ 2]->set(ToolKit::workdir());
  flds_[ 3]->set(ToolKit::outdir());
  
  flds_[ 4]->set(TS_Parameter::name());
  flds_[ 5]->set(RoadNetwork::name());
  
  flds_[ 6]->set(RN_DynamicRoute::file(0));
  
  flds_[ 7]->set(RN_PathTable::name());
  flds_[ 8]->set(OD_Table::name());
  flds_[ 9]->set(VehicleTable::name());
  flds_[10]->set(TS_Incident::filename());
  flds_[11]->set(theFileManager->moeSpecFile());
  
  flds_[12]->set(theSimulationClock->startStringTime());
  flds_[13]->set(theSimulationClock->stopStringTime());
  flds_[14]->set(theSimulationClock->stepSize());
  flds_[15]->set(theEngine->pathStepSize_);
  
  flds_[16]->set(theEngine->microStepSize_);
  flds_[17]->set(theEngine->macroStepSize_);
  flds_[18]->set(theEngine->batchStepSize_);
  
  flds_[19]->set(theEngine->segmentStatisticsSamplingStepSize_);
  flds_[20]->set(theEngine->segmentStatisticsReportStepSize_);
  flds_[21]->set(theEngine->stateStepSize_);
  flds_[22]->set(theEngine->survPointStepSize_);
  flds_[23]->set(theEngine->survAreaStepSize_);
  
  flds_[24]->set(theEngine->depRecordStepSize_);
  flds_[25]->set(theEngine->trajectoryStepSize_);
  if (theMoeCollector) {
	flds_[26]->set(theMoeCollector->stepSize());
  }
  flds_[27]->set(theEngine->transitTrajectoryStepSize_);
  flds_[28]->set(RN_BusRouteTable::name());
  flds_[29]->set(RN_BusScheduleTable::name());
  flds_[30]->set(RN_BusRunTable::name());
  flds_[31]->set(BusAssignmentTable::name());

  // Do not allow changes after simulation is started

  if (theSimulationClock->isStarted()) {
	deactivate();
  } else {
	activate();
  }

  XmwDialogManager::post();
}

void TS_SetupDialog::activate()
{
  if (XtIsSensitive(title_)) return;

  XmwDialogManager::activate(title_);
  for (int i = 0; i < flds_.size(); i ++) {
	XmwDialogManager::activate(flds_[i]->widget());
  }
}

void TS_SetupDialog::deactivate()
{
  if (!XtIsSensitive(title_)) return;

  XmwDialogManager::deactivate (title_);
  int i;
  for (i = 0; i <= 15; i ++) {
	XmwDialogManager::deactivate(flds_[i]->widget());
  }
  for (i = 19; i <= 26; i ++) {
	XmwDialogManager::deactivate(flds_[i]->widget());
  }
}

#endif // INTERNAL_GUI
