//-*-c++-*------------------------------------------------------------
// NAME: Traffic simulation
// AUTH: Qi Yang
// FILE: TS_Incident.h
// DATE: Sat Aug 31 16:01:59 1996
//--------------------------------------------------------------------

#ifndef TS_INCIDENT_HEADER
#define TS_INCIDENT_HEADER

#include "TS_Signal.h"

#include <iostream>
using namespace std;

class TS_Vehicle;
class Reader;

class TS_Incident : public TS_Signal
{
	  friend class TS_Network;
#ifdef INTERNAL_GUI
	  friend class TS_IncidentDialog;
#endif

   protected:

	  float severity_;			// deduction of capaciy
      double startTime_;		// in seconds
      double duration_;			// in seconds
      float speedLimit_;		// speed upper bound

      // Incidents are treated as signals and stored at the end of the
      // signal list

      static int startIndexAsSignals_;
      static char* filename_;

   public:

      static int readIncidents(char * filename);

      TS_Incident() : TS_Signal() { }
      ~TS_Incident() { }

      // Call check() for every incident
      
      static void checkIncidents();
      static char* filename() { return filename_; }
      static char** filenameptr() { return &filename_; }

      void print(ostream& os = cout);
      void check();

      inline float speedLimit() { return speedLimit_; }
      inline double startTime() { return startTime_; }
      inline double duration() { return duration_; }
      float acceleration(TS_Vehicle *, float);
      void remove();

	  void set(RN_CtrlStation* s, RN_Lane *lane);

#ifdef INTERNAL_GUI

      // Overload the virtual function in DRN_Signal

      Pixel * calcColor(unsigned int);
#endif

      void send(unsigned int msg_type);

      virtual void send(IOService &); // virtual
      void receive(IOService &); // virtual
};

#endif
