//-*-c++-*------------------------------------------------------------
// TS_IncidentDialog.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef DRN_INCIDENT_DIALOG_HEADER
#define DRN_INCIDENT_DIALOG_HEADER

#include <Xmw/XmwDialogManager.h>
#include <Xmw/XmwCallback.h>

class TS_Incident;
class RN_CtrlStation;

class TS_IncidentDialog : public XmwDialogManager
{
	  CallbackDeclare(TS_IncidentDialog);

   public:

	  TS_IncidentDialog(Widget parent, TS_Incident *p);
	  ~TS_IncidentDialog();

	  static void createAndPost(TS_Incident *, RN_CtrlStation *s = NULL);

   private:

	  void postAndWait(RN_CtrlStation *s);

	  // existing callbacks

	  void okay(Widget, XtPointer, XtPointer);
	  void cancel(Widget, XtPointer, XtPointer);
	  void help(Widget, XtPointer, XtPointer);

	  // new callbacks

	  void enter ( Widget, XtPointer, XtPointer );
// 	  void leave ( Widget, XtPointer, XtPointer );

   private:

	  RN_CtrlStation *station_;	// if new station
	  Widget position_;			// XmLabel
	  Widget visibility_;		// XmtInputField
	  Widget lanes_;			// XbaeMatrix

	  TS_Incident *incidents_;
};

#endif
