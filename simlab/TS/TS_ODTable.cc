/*-*-c++-*-******************************************************/
/*    NAME: Traffic Simulation                                  */
/*    AUTH: Qi Yang                                             */
/*    FILE: TS_ODTable.C                                        */
/*    DATE: Mon Aug 28 16:22:58 1995                            */
/****************************************************************/

#include <GRN/RN_Link.h>
#include "TS_ODTable.h"
#include "TS_ODCell.h"

OD_Cell* 
TS_ODTable::newOD_Cell()
{
   return new TS_ODCell;
}
