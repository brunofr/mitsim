//-*-c++-*------------------------------------------------------------
// TS_VehicleDialog.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI
#ifndef TS_VEHICLEDIALOG_HEADER
#define TS_VEHICLEDIALOG_HEADER

#include <Xmw/XmwDialogManager.h>
#include <Xmw/XmwCallback.h>

class TS_VehicleDialog : public XmwDialogManager
{
  	  CallbackDeclare(TS_VehicleDialog);

   public:

	  TS_VehicleDialog(Widget parent);
	  ~TS_VehicleDialog() { }
	  void post();				// virtual

   private:

	  // overload the virtual functions in base class

	  void okay(Widget, XtPointer, XtPointer);
	  void cancel(Widget, XtPointer, XtPointer);
	  void help(Widget, XtPointer, XtPointer);

	  // new callbacks

	  void clickBorderCodeCB(Widget, XtPointer, XtPointer);
	  void clickCB(Widget, XtPointer, XtPointer);
	  void clickLabelCB(Widget, XtPointer, XtPointer);

   private:
	  
	  Widget borderCodeFld_;	// radio box
	  int borderCode_;
	  Widget shadeCodeFld_;		// radio box
	  int shadeCode_;
	  Widget labelFld_;			// toggle button
	  int label_;
};

#endif
#endif // INTERNAL_GUI
