//-*-c++-*------------------------------------------------------------
// FILE: RN_Segment.h
// AUTH: Qi Yang
// DATE: Wed Oct 18 17:38:54 1995
//--------------------------------------------------------------------

#ifndef TS_SEGMENT_HEADER
#define TS_SEGMENT_HEADER

class TS_Link;
class TS_Lane;
class TS_Vehicle;
class WcsPoint;

#include <fstream>
#include "TS_Parameter.h"


// const double D_AREA_SAMPLING_STEP_SIZE = 20.0;

#ifdef INTERNAL_GUI
#include <DRN/DRN_Segment.h>
#else  // batch mode
#include <GRN/RN_Segment.h>
#define DRN_Segment RN_Segment
#endif // !INTERNAL_GUI

class TS_Segment :  public DRN_Segment
{
  friend class TS_Network;
  friend class TS_Vehicle;
    
public:

  TS_Segment();
  ~TS_Segment();

  inline int nVehicles() { return nVehicles_; }
  inline int nVehicles(int n) {
	nVehicles_ += n;
	return nVehicles_;
  }

  inline TS_Vehicle* firstVehicle() { return firstVehicle_; }
  inline TS_Vehicle* lastVehicle() { return lastVehicle_; }
  inline void firstVehicle(TS_Vehicle *pv) { firstVehicle_ = pv; }
  inline void lastVehicle(TS_Vehicle *pv) { lastVehicle_ = pv; }

  inline float density() { return density_/theParameter->densityFactor(); }
  inline float speed() { return speed_/theParameter->speedFactor(); }
  int flow();
  float calcDensity(void);
  float calcSpeed(void);
  int calcFlow(void);

  // overload thebase class function

  void calcStaticInfo();

  // Segment statistics

  void initializeStatistics();

  void resetEnteringStatistics();
  void resetLeavingStatistics();

  // called when a vehicle enters the segment

  void countVehiclesEntered();

  // called when a vehicle leaves the segment

  void countVehiclesLeft();
  void recordTravelTime(TS_Vehicle*);

  // called periodically at a given sampling rate

  void samplingStatistics();


  int nSamples() { return nSamples_; }

  // These can be reported at the end of each time interval

  int nVehiclesEntered() { return nVehiclesEntered_; }
  int nVehiclesLeft() { return nVehiclesLeft_; }
  float averageDensity();
  float averageSpeed();
  float averageTravelTimeAtLeaving();

  // These are valid only after all the vehicles entered the
  // segment in interval i have left the segment.  */

  float averageTravelTimeAtEntering(int i);
  int numberOfVehiclesEntering(int i);

  void printStatistics(std::ofstream&);
  void printTravelTimes(std::ofstream&);

#ifdef INTERNAL_GUI
  TS_Vehicle* pickVehicle(WcsPoint& pressed);
  void queryVehicle(WcsPoint& pressed);
  void traceVehicle(WcsPoint& pressed);
#endif

protected:

  int nVehicles_;		 // number of vehicles
  TS_Vehicle *firstVehicle_; // macro vehicle list
  TS_Vehicle *lastVehicle_;	 // macro vehicle list

  float density_;		// current density
  float speed_;		// current speed

  // Segment statistics

  int    nVehiclesEntered_;
  int    nVehiclesLeft_;

  int    nSamples_;
  double sumOfDensity_;
  double sumOfSpeed_;

  int    nSamplesTravelTimeAtLeaving_;
  double sumOfTravelTimeAtLeaving_;

  int    *nSamplesTravelTimeAtEntering_;
  double *sumOfTravelTimeAtEntering_;
};

#endif
