//-*-c++-*------------------------------------------------------------
// NAME: Microcscopic Traffic Simulator
// AUTH: Qi Yang
// FILE: TS_.VehicleList.C
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#include <Tools/ToolKit.h>
#include "TS_VehicleList.h"

#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>

#ifndef INTERNAL_GUI
#include "TS_Vehicle.h"
#else
#include "TS_DrawableVehicle.h"
#endif

TS_VehicleList * theVehicleList = new TS_VehicleList;

TS_VehicleList::TS_VehicleList()
   : head_(NULL),
	 tail_(NULL),
	 nVehicles_(0),
	 nPeakVehicles_(0)
{
}

TS_VehicleList::~TS_VehicleList()
{
   while (head_) {
      tail_ = head_;
      head_ = head_->trailing_;
      delete tail_;
   }
   tail_ = NULL;
   nVehicles_ = 0;
}


/*
 * add a vehicle to the end of the vehicle list.
 */

void
TS_VehicleList::recycle(TS_Vehicle *pv)
{
   pv->donePathIndex();
   pv->lane_ = NULL;

   pv->trailing_ = NULL;
   pv->leading_ = tail_;

   if (tail_) {		// at least one in the list
      tail_->trailing_ = pv;
   } else {			// no vehicles in the list
      head_ = pv;
   }
   tail_ = pv;
   nVehicles_ ++;
}


/*
 * Remove a vehicle from the head of the vehicle list.
 */

TS_Vehicle*
TS_VehicleList::recycle()
{
   TS_Vehicle* pv;

   if (head_) {					// get head from the list
      pv = head_;
      if (tail_ == head_) {		// the onle one vehicle in list
		 head_ = tail_ = NULL;
      } else {					// at least two vehicles in list
		 head_ = head_->trailing_;
		 head_->leading_ = NULL;
      }
      nVehicles_ --;
   } else {						// list is empty
#ifdef INTERNAL_GUI
	  pv = new TS_DrawableVehicle;
#else
	  pv = new TS_Vehicle;
#endif
      nPeakVehicles_ ++;
   }
   return (pv);
}
