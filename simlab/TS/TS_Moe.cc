//-*-c++-*------------------------------------------------------------
// NAME: This is generates the moe needed in my thesis
// AUTH: Qi Yang
// FILE: TS_Moe.C
// DATE: Fri Jun 14 12:24:10 1996
//--------------------------------------------------------------------

#include <Tools/SimulationClock.h>

#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>

#include "TS_Moe.h"
#include "TS_Vehicle.h"
#include "TS_Segment.h"
#include "TS_Parameter.h"

TS_Moe::TS_Moe()
{
   reset();
}

void TS_Moe::reset()
{
   count_ = 0;
   travelTimeSum_ = 0.0;
   speedSum_ = 0.0;
   vkmtSum_ = 0.0;
}


double TS_Moe::travelTime()
{
   if (count_) {
      return travelTimeSum_ / count_;
   } else {
      return 0.0;
   }
}


double TS_Moe::speed()
{
   if (count_) {
      return speedSum_ / count_ / theParameter->speedFactor();
   } else {
      return 0.0;
   }
}


double TS_Moe::vkmt()
{
   if (count_) {
      return vkmtSum_ / count_ * 0.001;
   } else {
      return 0.0;
   }
}


void TS_Moe::record(TS_Vehicle *pv)
{
   double t = theSimulationClock->currentTime() - pv->departTime();

   if (t <= 0.1) return;

   double m = pv->mileage();

   if (pv->distance() > 0.0) {
      // it has not finish its current segment
      m += pv->segment()->length() - pv->distance();
   } else {
      // extra length traveled
      m -= pv->distance();
   }
   count_ ++;
   travelTimeSum_ += t;
   vkmtSum_ += m;
   speedSum_ += m / t;
}

