//-*-c++-*------------------------------------------------------------
// TS_Sensor.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef TS_SENSOR_HEADER
#define TS_SENSOR_HEADER

#include <iostream>
#include <TC/TC_Sensor.h>
#include <vector> //IEM(Jun15)
using namespace std;

class TS_Segment;
class TS_Lane;
class TS_Vehicle;

typedef struct _TimeSpeed {
  float time;
  float speed;
} TimeSpeed;

class TS_Sensor : public TC_Sensor
{
  friend class TS_Network;

public:

  TS_Sensor();
  virtual ~TS_Sensor() { }

  float distance2Time(float dis, float vel, float acc);
  float calcSpeedAndTime(TS_Vehicle* pv, float prevpos,
						 float prevvel, float acc,
						 TimeSpeed* p = NULL);

  static inline void harmonic(int h) { harmonic_ = h; }
  static inline int harmonic() { return harmonic_; }

  void calcActivatingData(TS_Vehicle* pv, float prevpos,
						  float prevvel, float acc);

  void calcPassingData(TS_Vehicle* pv, float prevpos,
					   float prevvel, float acc);

  void calcSnapShotData();

  void resetSensorReadings();

  // Overload the virtual functions

  int flow();				// virtual
  float occupancy();		// virtual
  float speed();			// virtual

protected:

  static double resetTime_;
  static int harmonic_;
  static vector<double> resetTimes_; //IEM(Jun15) Store all resetTimes statically

  double invspeed_;
  double prev_time_off_;
};

#endif



