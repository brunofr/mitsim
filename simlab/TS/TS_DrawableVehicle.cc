//-*-c++-*------------------------------------------------------------
// FILE: TS_DrawableVehicle.C
// AUTH: Yang Qi
// DATE: Thu Oct 19 14:02:43 1995
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <cstdio>
#include <sstream>
#include <cstring>

#include <Xmt/Xmt.h>
#include <Xmt/Dialogs.h>

#include <Tools/Math.h>
#include <Tools/SimulationClock.h>
#include <Tools/ToolKit.h>

#include <GRN/WcsPoint.h>
#include <GRN/Constants.h>
#include <GRN/RN_Path.h>

#include "TS_Symbols.h"
#include "TS_Network.h"
#include "TS_DrawingArea.h"
#include "TS_Interface.h"
#include "TS_Menu.h"
#include "TS_DrawableVehicle.h"
#include "TS_Lane.h"
#include "TS_Segment.h"
#include "TS_Link.h"
#include "TS_Node.h"
#include "TS_Parameter.h"
#include "TS_TraceDialog.h"

TS_DrawableVehicle::TS_DrawableVehicle()
  : TS_Vehicle(), 
	statusShown_(0),
	onScreen_(0),
	prevPos_(0.0),
	prevLane_(0),
	shade_color_(0)
{
}


void TS_DrawableVehicle::calcColors()
{
  central_color_ = centralColor();
  side_color_ = sideColor();
  front_color_ = frontColor();
  back_color_ = backColor();
}


// These virtual functions should be overrided in derived class

/*
 * Returns a color that indicates vehicle type
 */

Pixel TS_DrawableVehicle::sideColor()
{
  Pixel clr;

  switch (theSymbols()->vehicleBorderCode().value()) {
  case TS_Symbols::VEHICLE_BORDER_TYPE:
	{
	  clr = theParameter->vehicleColors(type());
	  break;
	}
  case TS_Symbols::VEHICLE_BORDER_INFO:
	{
	  if (isGuided()) {
		if (path()) {
		  clr = theColorTable->cyan();
		} else {
		  clr = theColorTable->lightGreen();
		}
	  } else {
		if (path()) {
		  clr = theColorTable->red();
		} else {
		  clr = theColorTable->yellow();
		}
	  }
	  break;
	}
  case TS_Symbols::VEHICLE_BORDER_LANEUSE:
	{
	  if ((type_ & VEHICLE_HOV) && (type_ & VEHICLE_ETC)) {
		clr = theColorTable->lightGreen();
	  } else if (type_ & VEHICLE_HOV) {
		clr = theColorTable->yellow();
	  } else if (type_ & VEHICLE_ETC) {
		clr = theColorTable->cyan();
	  } else {
		clr = theColorTable->red();
	  }
	  break;
	}
  case TS_Symbols::VEHICLE_BORDER_MOVEMENT:
	{
	  clr = colorByTurnMovement();
	  break;
	}
  case TS_Symbols::VEHICLE_BORDER_GROUP:
	{
	  if (theParameter->nDriverGroups() > 1) {
		float r = (float)driverGroup.spdLmtAddOn /
		  (float)(theParameter->nDriverGroups() - 1);
		clr = theColorTable->color(r);
	  } else {
		clr = theColorTable->color(0.0);
	  }
	  break;
	}
  default:
	{
	  clr = theColorTable->white();
	  break;
	}
  }
  return clr;
}


/*
 * Shows the car-following regime
 */

Pixel TS_DrawableVehicle::frontColor()
{
  Pixel clr;
  int regime = status() & STATUS_REGIME;

  switch (regime) {
  case STATUS_REGIME_FREEFLOWING:
	{
	  clr = theColorTable->lightGreen();
	  break;
	}
  case STATUS_REGIME_CARFOLLOWING:
	{
	  clr = theColorTable->yellow();
	  break;
	}
  case STATUS_REGIME_EMERGENCY:
	{
	  clr = theColorTable->lightRed();
	  break;
	}
  default:
	{
	  clr = theColorTable->white();
	  break;
	}
  }
  return clr;
}


/*
 * Show accelerating/deceleration status
 */

Pixel TS_DrawableVehicle::backColor()
{
  const float small_acc = 0.1; // meters/sec2
  const float big_acc   = 0.5;
  float a = accRate();
  float v = currentSpeed();
  Pixel clr;

  if (a > big_acc) {
	clr = theColorTable->lightGreen();
  } else if (a > small_acc) {
	clr = theColorTable->green();
  } else if (a < - big_acc) {
	clr = theColorTable->lightRed();
  } else if (a < - small_acc) {
	clr = theColorTable->red();
  } else if (v < SPEED_EPSILON) {
	clr = theColorTable->magenta();
  } else {
	clr = theColorTable->yellow();
  }
  return clr;
}


/*
 * Show lane change state
 */

Pixel TS_DrawableVehicle::centralColor()
{
  Pixel clr;
  unsigned int state = status();

  if (state & STATUS_CHANGING)	{
	if (flag(FLAG_ESCAPE)) {
	  clr = theColorTable->magenta();
	} else if (state & STATUS_CURRENT_OK) { // current lane is ok
	  if (state & STATUS_MANDATORY) {
		clr = theColorTable->yellow();
	  } else {
		clr = theColorTable->green();
	  }
	} else {			// curent lane is wrong
	  if (state & STATUS_MANDATORY) {
		clr = theColorTable->lightRed();
	  } else {
		clr = theColorTable->orange();
	  }
	}
  } else {
	clr = 0;
  }

  return clr;
}

Pixel TS_DrawableVehicle::shadeColor()
{
  Pixel clr;

  // Find out the shade

  unsigned int mode = theSymbols()->vehicleShadeCodes().value();

  if (!mode) {
	clr = 0;
  } else if ((mode & TS_Symbols::VEHICLE_SHADE_NOSING) &&
	flag(FLAG_NOSING)) {
	clr = theColorTable->red();
  } else if ((mode & TS_Symbols::VEHICLE_SHADE_YIELDING) &&
	flag(FLAG_YIELDING)) {
	clr = theColorTable->green();
  } else if ((mode & TS_Symbols::VEHICLE_SHADE_MERGING) &&
	flag(FLAG_MERGING)) {
	clr = theColorTable->yellow();
  } else if ((mode & TS_Symbols::VEHICLE_SHADE_WAITING) &&
	(timeInLink() > theVehicleShadeParam[1])) {
	clr = theColorTable->darkGray();
  } else {
	clr = 0;
  }
  return clr;
}


Pixel
TS_DrawableVehicle::colorByTurnMovement()
{
  if (!nextLink_) {
	return theColorTable->white();
  }
  float x = (float)nextLink_->dnIndex() / (1.0 + link()->nDnLinks());
  return theColorTable->color(x);
}


// --------------------------------------------------------------------
// Function Name: draw(DRN_DrawingArea *)
//
// TS_DrawableVehicle::draw() is called by network.  We give the vehicle a
// DRN_DrawingArea to which to draw, and each vehicle to draw itself.
// The vehicle need to know its lane so it is able to query them for
// world coordinates.
//
// HIGH LEVEL VEHICLE DRAWING ALGORITHM:
//
//  o if the vehicle is onScreen, has not moved, and has the same
//    status, skip.
//     
//  o if the vehicle is onSrean, calls draw with the previous position
//    to erase (XOR mode in black background).
//
//  o if the vehicle has moved or it is not on screen then calculate
//    its position in the view area.
//    
//  o if onScreen_ sets status and color and draws the vehicle.
//
// --------------------------------------------------------------------

int TS_DrawableVehicle::draw(DRN_DrawingArea * area)
{
  Pixel shade = shadeColor();
  int moved = lane_ != prevLane_ || !AproxEqual(distance_, prevPos_);
  int changed = statusShown_ != status() || shade != shade_color_;

  onScreen_ = onScreen_ && area->state(DRN_DrawingArea::VEHICLE_ON);

  if (onScreen_ && !moved && !changed) {
	return onScreen_;		// nothing changed
  }

  // erase previous image

  if (onScreen_) drawVehicle(area);

  // calculate new position
      
  if (moved || !onScreen_) {
	onScreen_ = calcPoints(area);
	prevPos_ = distance_;
	prevLane_ = lane_;
  }

  if (onScreen_) {
	statusShown_ = status();
	shade_color_ = shade;
	calcColors();		// translate status to colors
	drawVehicle(area);	// draw at new position
  }

  return onScreen_;
}



// -------------------------------------------------------------------
// Erase a vehicle from the screen
// -------------------------------------------------------------------

void
TS_DrawableVehicle::erase(DRN_DrawingArea * area)
{
  if (area->state(DRN_DrawingArea::VEHICLE_ON) && onScreen_)
	{
      drawVehicle(area);
	}
  onScreen_ = 0;
}


int
TS_DrawableVehicle::calcPoints(DRN_DrawingArea * area)
{
   
  float half_veh_width = 0.5 * width() * PERCENT_OF_VEHICLE_WIDTH;

  /* finds postion of back bumber */

  float back_ratio = position() + length() / lane()->length();

  /* finds postion of front bumber */

  float front_ratio = position();

  WcsPoint front(lane_->centerPoint(front_ratio));
  WcsPoint back(lane_->centerPoint(back_ratio));

  float angle = back.angle(front);

  // back/left corner

  WcsPoint p1 (back.bearing(half_veh_width, angle + HALF_PI));

  // front/left corner 

  WcsPoint p2 (front.bearing(half_veh_width, angle + HALF_PI));

  // front/right corner

  WcsPoint p3 (front.bearing(half_veh_width, angle - HALF_PI));

  // back/right corner

  WcsPoint p4 (back.bearing(half_veh_width, angle - HALF_PI));

  int on = 0;

  on |= area->world2Window(p1,points_[0]);
  on |= area->world2Window(p2,points_[1]);
  on |= area->world2Window(p3,points_[2]);
  on |= area->world2Window(p4,points_[3]);

  return on;
}


void
TS_DrawableVehicle::drawVehicle(DRN_DrawingArea * area)
{
  if (area->isVehicleStateDrawable()) {
	drawLargeVehicle(area);
  } else {
	drawSmallVehicle(area);
  }
}


/*
 *-----------------------------------------------------------------
 * Draw a small vehicle without showing lane change status
 *-----------------------------------------------------------------
 */

void
TS_DrawableVehicle::drawSmallVehicle(DRN_DrawingArea * area)
{
  area->setXorMode();

  if (shade_color_) {
	XPoint p[5];
	memcpy(p, points_, sizeof(XPoint) * 4); // 4 vertices
	memcpy(p + 4, points_, sizeof(XPoint)); // repeat the 1st
	area->foreground(shade_color_);
	area->XmwDrawingArea::drawFilledPolygon(p, 5, Convex);
  }

  /* draw sides */

  area->foreground(side_color_);
  area->XmwDrawingArea::drawLine(points_[0], points_[1]);
  area->XmwDrawingArea::drawLine(points_[2], points_[3]);

  /* draw front */

  area->foreground(front_color_);
  area->XmwDrawingArea::drawLine(points_[1], points_[2]);

  /* draw back */
     
  area->foreground(back_color_);
  area->XmwDrawingArea::drawLine(points_[0], points_[3]);

  if (flag(FLAG_SHINE)) {		// flash was/will be on
	highlight(area);
  }
}


/*
 *-----------------------------------------------------------------
 * Draw a complete colored vehicle
 *-----------------------------------------------------------------
 */

void
TS_DrawableVehicle::drawLargeVehicle(DRN_DrawingArea * area)
{
  char label[12];

  drawSmallVehicle(area);

  /* draw diagonal lines indicating lane changing */

  if (statusShown_ & STATUS_RIGHT) {
	area->foreground(central_color_);
	area->XmwDrawingArea::drawLine(points_[0], points_[2]);
  } else if (statusShown_ & STATUS_LEFT) {
	area->foreground(central_color_);
	area->XmwDrawingArea::drawLine(points_[1], points_[3]);
  } else if (statusShown_ & STATUS_MANDATORY) {
	area->foreground(theColorTable->darkGray());
	area->XmwDrawingArea::drawLine(points_[0], points_[2]);
	area->XmwDrawingArea::drawLine(points_[1], points_[3]);
  }

  /* draw vehicle label */

  if (theSymbols()->isVehicleLabelOn().value() && 
	area->resolution() >= DRN_DrawingArea::HIGHEST_RESOLUTION) {
	XmwPoint p((points_[0].x + points_[2].x) / 2,
	  (points_[0].y + points_[2].y) / 2);
	sprintf(label, "%d", code());
	area->foreground(theColorTable->yellow());
	area->XmwDrawingArea::drawCString(label, p);
  } 
}

void
TS_DrawableVehicle::query()
{
  SimulationClock *clk = theSimulationClock;
  TS_Parameter *p = theParameter;

  ostringstream data;

  float cur_spd = currentSpeed_ / p->speedFactor();
  float des_spd = desiredSpeed_ / p->speedFactor();
  float acc = accRate_ / p->accConverting();

  data << "Plate No = " << code_ << endl
	   << "Type = " << type_ << endl
	   << "Group = 0x" << hex << getDriverGroup() << dec << endl
	   << "Departure Time = " << clk->convert(departTime_) << endl
	   << "Origin = " << oriNode()->code() << endl
	   << "Destination = " << desNode()->code() << endl
	   << "Position = "
	   << link()->code() << "/"
	   << segment()->code() << "/"
	   << lane()->code() << endl
	   << "Desired " << p->speedLabel() << " = "
	   << Fix(des_spd, (float)1.0) << endl
	   << p->speedLabel() << " = "
	   << Fix(cur_spd, (float)1.0) << endl
	   << "Acceleration = " << Fix(acc, (float)0.1);

  if (path_) {
	data << endl
		 << "Path = " << path_->code();
  }

  if (nextLink_) {
	data << endl
		 << "Next Link = " << nextLink_->code();
  }

  data << endl;
  data << endl << "A = "; data.fill('0');
  data.width(8); data << hex << attrs_;

  data << endl << "S = "; data.fill('0');
  data.width(8); data << hex << status_;

  data << endl << "F = "; data.fill('0');
  data.width(8); data << hex << flags_;

  data << null;

  XmtDisplayInformation(theInterface->widget(),
	NULL, // msg_name,
	data.str().c_str(), // msg_default
	clk->currentStringTime());

}

void TS_DrawableVehicle::trace()
{
  TS_TraceDialog::the()->post_info(this);
}

void
TS_DrawableVehicle::highlight(DRN_DrawingArea * area)
{
  const double alpha = M_PI * 0.125;
  const double lw = 9.0;		// in pixels
  const double sw = 6.0;

  // Center of the vehicle

  WcsPoint cp((points_[0].x + points_[2].x) / 2,
	          area->height() - (points_[0].y + points_[2].y) / 2);

  WcsPoint hp((points_[1].x + points_[2].x) / 2,
	          area->height() - (points_[1].y + points_[2].y) / 2);

  double angle = cp.angle(hp);
  double a = length() * area->meter2Pixels();
  double b = width()  * area->meter2Pixels();
  XPoint p1, p2;

  double t = 0.0;
  double cosa = cos(angle);
  double sina = sin(angle);
  double x, y, w;
  area->setXorMode();
  area->foreground(theColorTable->yellow());
  bool longer = true;
  for (t = 0.0; t < M_PI * 2.0; t += alpha) {
	x = a * cos(t);
	y = b * sin(t);
	p1.x = int(cp.x() + (x * cosa - y * sina));
	p1.y = area->height() - int(cp.y() + (y * cosa + x * sina));

	w = longer ? lw : sw;
	longer = !longer;

	x = (a + w) * cos(t);
	y = (b + w) * sin(t);
	p2.x = int(cp.x() + (x * cosa - y * sina));
	p2.y = area->height() - int(cp.y() + (y * cosa + x * sina));

	area->XmwDrawingArea::drawLine(p1, p2);
  }  
}

#endif
