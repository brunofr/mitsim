//-*-c++-*------------------------------------------------------------
// NAME: Microscopic Traffic Simulator
// AUTH: Qi Yang
// FILE: TS_Vehicle.h
// DATE: Mon Feb 19 20:59:54 1996
//--------------------------------------------------------------------

#ifndef TS_VEHICLE_HEADER
#define TS_VEHICLE_HEADER

#include <Templates/Listp.h>
#include <GRN/RN_Vehicle.h>
#include <GRN/RoadNetwork.h>
#include <GRN/BusAssignmentTable.h>
#include "TS_Parameter.h"
#include "TS_Network.h"

class Reader;
class TS_Network;
class TS_Lane;
class TS_Segment;
class TS_Link;
class TS_Node;
class TS_Signal;
class TS_Incident;
class TS_TollBooth;

class RN_CtrlStation;
class RN_SurvStation;
class RN_Path;
class RN_IS;

const float MAX_ACCELERATION		= +10.0; // meter/sec2
const float MAX_DECELERATION		= -10.0; // meter/sec2
const float CF_CRITICAL_TIMER_RATIO	= 0.5;

const unsigned int FLAG_ESCAPE		  = 0x00000003;	// sum
const unsigned int FLAG_ESCAPE_RIGHT      = 0x00000001;
const unsigned int FLAG_ESCAPE_LEFT	  = 0x00000002;

const unsigned int FLAG_AVOID		  = 0x0000000C;	// sum
const unsigned int FLAG_AVOID_RIGHT       = 0x00000004;
const unsigned int FLAG_AVOID_LEFT	  = 0x00000008;


const unsigned int FLAG_BLOCK	          = 0x00000030;	// sum
const unsigned int FLAG_BLOCK_RIGHT       = 0x00000010;
const unsigned int FLAG_BLOCK_LEFT	  = 0x00000020;

const unsigned int FLAG_COURTESY          = 0x00000040;//cfc 06
//const unsigned int FLAG_COURTESY_FEASIBLE   = 0x00000020;// cfc 06


const unsigned int FLAG_PREV_LC           = 0x00000300;	// sum
const unsigned int FLAG_PREV_LC_RIGHT     = 0x00000100;
const unsigned int FLAG_PREV_LC_LEFT      = 0x00000200;

const unsigned int FLAG_NOSING_FEASIBLE   = 0x00000400;

const unsigned int FLAG_YIELDING          = 0x00003000;	// sum
const unsigned int FLAG_YIELDING_RIGHT    = 0x00001000;
const unsigned int FLAG_YIELDING_LEFT     = 0x00002000;

const unsigned int FLAG_NOSING            = 0x0000C000;	// sum
const unsigned int FLAG_NOSING_RIGHT      = 0x00004000;
const unsigned int FLAG_NOSING_LEFT       = 0x00008000;

const unsigned int FLAG_MERGING           = 0x00010000;
const unsigned int FLAG_STUCK_AT_END      = 0x00020000;

const unsigned int FLAG_LC_FAILED         = 0x000C0000;	// sum
const unsigned int FLAG_LC_FAILED_LEAD    = 0x00040000;
const unsigned int FLAG_LC_FAILED_LAG     = 0x00080000;

const unsigned int FLAG_VMS_LANE_USE_BITS = 0x06F00000;	// sum
const unsigned int FLAG_VMS_LANE_USE_DIR  = 0x06000000;	// sum
const unsigned int FLAG_VMS_LANE_USE_RIGHT= 0x02000000;
const unsigned int FLAG_VMS_LANE_USE_LEFT = 0x04000000;
const unsigned int FLAG_VMS_LANE_USE_PIVOT= 0x00F00000;	// pivot index

// These macros are used to get and set lane use pivot index

#define VmsLaneUsePivotToIndex(p) (((p) & 0x000F0000) >> 16)
#define VmsLaneUseIndexToPivot(n) ((n) << 16)

const unsigned int FLAG_PROCESSED         = 0x10000000;
const unsigned int FLAG_IN_HOME           = 0x20000000;
const unsigned int FLAG_SHINE             = 0x40000000;	// for trace
const unsigned int FLAG_DESIRED_SPEED     = 0x80000000; // assigned in trace

// Tracking number of vehicles to yield

const unsigned YIELD_TYPE_CONNECTION	= 0x10000000;
const unsigned YIELD_TYPE_ESCAPE	= 0x20000000;
const unsigned YIELD_COUNTER	        = 0x0000000F;
const unsigned YIELD_REASON             = 0xF0000000;
#define YieldSignatureToLaneIndex(c)	(((c) & 0x0FFFFFF0) >> 4)
#define LaneIndexToYieldSignature(c)	(((c) & 0x0FFFFFF0) << 4)

class TS_Vehicle : public RN_Vehicle
{
  friend class TS_VehicleList;
  friend class TS_TraceDialog;

public:

  typedef Listp<RN_SurvStation*>::link_type SurvList;
  typedef Listp<RN_CtrlStation*>::link_type CtrlList;

  typedef unsigned long MID;
 
private:
      
  // flag of 1/0 switched in each cycle. Used to flag whether a
  // vehicle has been proceed in a cycle (its
  // flags_&FLAG_PROCESSED has same value as cycle_flag)

  static int cycleFlags_;	// prevent vehicle be move twice in a cycle
  static int regime_;	// used after carFollowingRate()



  // tomer - returns a pointer if the vehicle is approaching an intersection 
  // and 0 if not

  RN_IS* isApproachingIS_ ;
  float randomGapFactor_ ;
  float approachSpeedFactor_ ;
  float impatienceTimerStart_;
  float impatienceTimer_;
  // ***



  // CAUTION: It may not correspond to the current distance.  This
  // is a working variable that indicates the current distance
  // from a point where a lane change has to be made (otherwise
  // the vehicle need to stop).  It is set to the distance from
  // the downstream node each time when the vehicle moves into a
  // new link and recalculated each time lane change model is
  // called (where this variable is needed).  The variable
  // dis2stop is updated ONLY when it is needed.  It was the
  // distance to stop when the lane decision was made.

  static float dis2stop_;
  static float vis_;

  // tomer - basic look ahead distance for that driver

  float lookAheadDistance_;
  float lcProdMlcTagRejProb_; // used in tagging mandatory state
  float lcProdNoseRejProb_;	// used in sampling nosing prob
  bool no_conflict; // no coflict when signal is arrow (deepak)
 
  
 // gunwoo - the aggresiveness

  float aggresiveness_;


  float lcNormalRand_;	// random term in kazi's LC gap model

  unsigned int yield_;
  bool willYield(unsigned int reason);

  float probCmfToPmf(float cmf, float &prod_rej);

protected:
	
  unsigned int status_;	// current status indicator
  unsigned int flags_;	// additional indicator for internal use

  TS_Lane *lane_;		// lane it currently resides in
  TS_Lane *nextLane_;	// downstream target lane

  TS_Vehicle *leading_;	// leading vehicle
  TS_Vehicle *trailing_;	// trailing vehicle
 
  CtrlList nextCtrl_;	// next ctrl station or event
  SurvList nextSurv_;	// next station of surveillance

  float cfTimer_;		// timer in second

  float impatient_;		// lane changing parameter

  float lcTimeTag_;		// time changed lane

  // Desired speed is updated when a vehicle is moved into a new
  // segment or it has passed a speed limit sign.

  float desiredSpeed_;	// in meter/sec
  float maxAcceleration_;	// in meter/sec2
  float normalDeceleration_; // in meter/sec2
  float maxDeceleration_;	// in meter/sec2
  float distanceToNormalStop_; // assuming normal dec is applied

  float accRate_;		// acceleration rate

  float timeEntersSegment_; // time enters current segment

  unsigned int signedSpeed_; // speed on previous speed sign
	
  TS_Vehicle *macroLeading_; // macro vehicle list
  TS_Vehicle *macroTrailing_;

  int positionInQueue_;	// position in queue

  double yieldTime_;	// time to start yielding
  TS_Vehicle *yieldVehicle_;
  float nextStepSize_;	// for car-following/lane-changing

  TS_Vehicle *prevLead_; //Anita - for force merging & courtesy cfc 06
  TS_Vehicle *prevLag_;

public:

  TS_Vehicle();
  ~TS_Vehicle() { }

  static void toggleCycleFlag(unsigned int flag) { 
	cycleFlags_ ^= flag;
  }

  struct DriverGroup {
	unsigned maxAccScale   : 4 ;
	unsigned maxDecScale   : 4 ;
	unsigned normalDecScale: 4 ;
	unsigned cfAccAddOn    : 4 ;
	unsigned cfDecAddOn    : 4 ;
	unsigned ffAccAddOn    : 4 ;
	unsigned spdLmtAddOn   : 4 ;
	unsigned hUpper        : 4 ;
      unsigned aUpper        : 4 ;//cfc 06

  } driverGroup;

  void setDriverGroup(unsigned int) ;
  unsigned int getDriverGroup() ;

  // Flags

  bool get_no_conflict(){ //deepak
   return no_conflict;
  }
  void set_no_conflict(bool i){ //deepak
   no_conflict = i;
  }
 
  void print_no_conflict(){
   cout << " no conf = "<<no_conflict<<endl;
  }

  void toggleFlag(unsigned int flag) {
	flags_ ^= flag;
  }
  unsigned int flag(unsigned int mask = 0xFFFFFFFF) {
	return (flags_ & mask);
  }
  void setFlag(unsigned int s) {
	flags_ |= s;
  }
  void unsetFlag(unsigned int s) {
	flags_ &= ~s;
  }

  void setStatus();
  unsigned int status() { return status_; }
  unsigned int status(unsigned int mask) {
	return (status_ & mask);
  }
  void setStatus(unsigned int s) {
	status_ |= s;
  }
  void unsetStatus(unsigned int s) {
	status_ &= ~s;
  }

  void setMandatoryStatusTag();
  int isInMergingArea();
  int isInIncidentArea(TS_Lane *plane);

  int isProcessed(int flag) {
	return !(cycleFlags_ ^ (flags_ & flag)); 
  }

  TS_Node* oriNode() { return (TS_Node *)RN_Vehicle::oriNode(); }
  TS_Node* desNode() { return (TS_Node *)RN_Vehicle::desNode(); }

  int enRoute() {		// virtual
	return attr(ATTR_ACCESSED_INFO);
  }

  // tomer - function that returns a pointer to the intersection the
  // vehicle is approaching 


  RN_IS* getApproachingIS();
    
  void setApproachingIS();
  

  // tomer - function that gets the vehicles movement in the intersection

  int getMovement(RN_IS*);
    
  // tomer - this function finds the conflicting lanes to a movement
  // stores them in a vector and gets reed of duplications
  
  vector <int> conflictLanes(int i);

  // tomer - this function gets a pointer to the first vehicle in a 
  // conflicting  lane

  TS_Vehicle* getFirst(int i);

  // tomer - this function checks the destination lane of the vehicle 
  //returned by getFirst

  int destLane(TS_Vehicle* v);

  // tomer - this function returns the movement of the conflicting vehicle

    MID firstMove(int i);

  //tomer - this function checks if the vehicle in the conflicting
  //lane does conflict. i and j are my lane and the conflicting lane

  int isConflict(MID j);

  // tomer - finding the time it will take a vehicle
  // to arrive at the intersection under current speed and accelaration
   
  float timeToIntersection();

  // tomer - second  overloaded function for the current vehicle with 
  //specific acceleration 
  
  float timeToIntersection(float acc);

  // tomer - this funcction estimates the time until the first conflicting 
  // vehicle reaches the intersection

  float timeToConflict();

  // tomer - this function determines weather a gap ids acceptable or not

  int acceptableGap (float a , int myMove);

  // tomer - this function returns 1 if my vehicle is first in the lane and 0 otherwise.

  int ImFirst();

  // tomer - equivalent to breakToStop without the more complex term that causes problems.


  float brakeToReject (float s);

  float crowlingAcc();

  // tomer   

  float LookAheadDistance() {return lookAheadDistance_ ;}



  // gunwoo - aggresiveness function

  float aggresiveness() {return aggresiveness_ ;}
  void setAggresiveness();

  // tomer - impatience function

  float impatienceTimer() { return impatienceTimer_ ; }
  float impatienceTimerStart() { return impatienceTimerStart_ ;}

  // calls route choice/switch model and update related variables
  // if route changes.

  void changeRoute();

  TS_Link * nextTsLink() {
	return (TS_Link *) nextLink_;
  }

  TS_Link * tsLink() {
	return (TS_Link *) link();
  }

  TS_Segment * tsSegment() {
	return (TS_Segment *) segment();
  }

  TS_Lane* tsLane()  { return lane_; }
  RN_Lane* lane()  { return (RN_Lane *)lane_; }

  float width();
  int direction();
  float position();

  void leading(TS_Vehicle *pv) { leading_ = pv; }
  void trailing(TS_Vehicle *pv) { trailing_ = pv; }
  TS_Vehicle* leading() { return leading_; }
  TS_Vehicle* trailing() { return trailing_; }

  void advanceInMacroList();
  void removeFromMacroList();
  void appendToMacroList(TS_Segment *);
  TS_Vehicle* macroLeading() { return macroLeading_; }
  TS_Vehicle* macroTrailing() { return macroTrailing_; }

  unsigned int signedSpeed() { return signedSpeed_; }
  float accRate() { return accRate_; }

  float distanceFromDevice(CtrlList);
  float distanceFromDevice(SurvList);

  float cfTimer() { return cfTimer_; }
  void cfTimer(float t) { cfTimer_ = t; }
  float timeSinceTagged();

  void nextCtrl(CtrlList n) { nextCtrl_ = n; }
  CtrlList nextCtrl() { return nextCtrl_; }
  SurvList nextSurv() { return nextSurv_; }
  void updateNextCtrl();
  void updateNextSurv(float, float);

  // Dan - these are for bus operations:

  bool isBus();
  bool isPriorityGiven(int c);
  bool isBusWithAssignment();
  bool busStopIsOnRoute(int id);
  void writePriorityRecord(int c, int p);	
  void savePriorityRecord(ofstream &os, int c, int p);

  RN_Segment* segment();
  RN_Link* link();

  int init(int id, int ori, int des,
		   int type_id = 0, int path_id = -1); // virtual

  int initBus(int bid, int ori, int des,
		   int path_id); // virtual

  void initialize();		// virtual, called in init()

  int load(Reader &is, int in_queue);
  void enterPretripQueue();
  int enterNetwork();
  void removeFromNetwork();
  void assignNextLane();
  void assignNextIntLane();//cfc 07
  void move();
  void transposeToNextLane(float oldd, float oldv);
  void append(TS_Lane *);
  void remove();
  
  // tomer - sets the basic look ahead distance

  void setLookAheadDistance ();

  void makeAcceleratingDecision();

  int makeLaneChangingDecision();
  void executeLaneChanging();

#ifndef USE_KAZI_MODELS

  void chooseTargetGap();
#endif

  double dlcExpOfUtility(TS_Lane*) ;

#ifndef USE_KAZI_MODELS//cfc june 4
  double gapExpOfUtility(int, float, float, float, float);
  float getOutDistance(RN_Lane *);

#ifdef USE_TOMER_MODELS 

// tomer deifing functions for the lc model

  double LCUtilityLeft(TS_Lane*) ;
  double LCUtilityRight(TS_Lane*) ;
  double LCUtilityCurrent(TS_Lane*) ;

  double LCUtilityLookAheadLeft(TS_Lane*, int, float) ;
  double LCUtilityLookAheadRight(TS_Lane*, int, float) ;
  double LCUtilityLookAheadCurrent(TS_Lane*, int, float) ;
  
 #endif

 #ifdef USE_CFC_MODELS

// cfc defining functions for the target lane model

  vector <double> TLProbability(TS_Lane*);
  vector <double> TLProbabilityLookAhead(TS_Lane*,vector <int> &,vector <double>& );

  //cfc dec 06

  vector <double> interLaneChoice(TS_Lane*,vector <int> &,int );


  /*double TLCUtilityLeft(TS_Lane*) ;
  double TLCUtilityRight(TS_Lane*) ;
  double TLCUtilityCurrent(TS_Lane*) ;

  double TLCUtilityLookAheadLeft(TS_Lane*,  vector <int> &,vector <float> & ) ;
  double TLCUtilityLookAheadRight(TS_Lane*,  vector <int> &,vector <float> & ) ;
  double TLCUtilityLookAheadCurrent(TS_Lane*,  vector <int> &,vector <float> & ) ;*/
  
#endif

#endif//end of ifndef kazi




  TS_Vehicle* vehicleAhead();
  TS_Vehicle* vehicleAheadInTypedLanes(int);
  TS_Vehicle* vehicleBehind();
  TS_Vehicle* vehicleBehindInTypedLanes(int);
  int canNoseIn(TS_Vehicle *av, TS_Vehicle *bv);
  float gapDistance(TS_Vehicle *);
  float gapDistance();

  // Returns the leading vehicle in plane.  Position is based on
  // the front bumper.

  TS_Vehicle* findNewLeader(TS_Lane *p) {
	// This is not good, but to be compatible to the previous
	// version.  I will not use this in my code. -- Qi
	return findFrontBumperLeaderInSameSegment(p);
  }
  TS_Vehicle* findFrontBumperLeaderInSameSegment(TS_Lane *);
  TS_Vehicle* findFrontBumperLeader(TS_Lane *);

  // Returns the trailing vehicle in plane.  Position is based on
  // the front bumper.

  TS_Vehicle* findNewFollower(TS_Lane *p) {
	// This is not good, but to be compatible to the previous
	// version.  I will not use this in my code. -- Qi
	return findFrontBumperFollowerInSameSegment(p);
  }
  TS_Vehicle* findFrontBumperFollowerInSameSegment(TS_Lane *);
  TS_Vehicle* findFrontBumperFollower(TS_Lane *);

  // Return the leading vehicle in plane or the connected
  // downstream lanes.  The position is based on the back bumper.

  TS_Vehicle* findBackBumperLeader(TS_Lane *);

  // Returns the lag vehicle in plane or the connected upstream
  // lanes.  The position is based on the back bumper.

  TS_Vehicle* findBackBumperFollower(TS_Lane *);

  void calcDesiredSpeed(unsigned int speed_on_sign = 0);
  float normalDeceleration() {
	return normalDeceleration_;
  }

  float maxLaneSpeed();
  float maxSegmentSpeed();
  float headwayBuffer();
// cfc 06 : do we need antiicpationTimeBuffer?
  float calcStartupDelay();
  float tollBoothDelayFactor();

  void countPositionInQueue();

  int isThereBadEventInList(TS_Lane *reflane,
							CtrlList next,
							float vis, float x,
							float* pdis2stop = NULL,
							float* pvis = NULL);

  // Called each time vehicle enter a new segment or change its
  // speed.

  void calcStateBasedVariables();

  float brakeToStop(float);
  float brakeToTargetSpeed(float, float);
  float distanceToNormalStop() {
	return distanceToNormalStop_;
  }

  float stoppingDistance(float s);

  float maxAcceleration() { return maxAcceleration_; }
  float maxDeceleration() { return maxDeceleration_; }

  float speedAhead(TS_Lane *);

  int isGapAcceptable(TS_Vehicle *);
      
  int doesComply(unsigned int signal_type,
				 unsigned int signal_state);

  void responseSignalsAndSigns();
  void responseVms(unsigned int state);
  void responseVsls(unsigned int speed_limit);
  void responseLus(unsigned int state);
  void reportIncident(CtrlList);

  void responseToLaneUseRuleMsgSign(unsigned int);
  void responseToLaneUsePathMsgSign(unsigned int);
  void responseToLaneUseMsgSign(unsigned int); // called by above two

  void responseToPathMsgSign(unsigned int);

  void responseToEnrouteMsg(unsigned int state)      ;

  void reportTripSummary();
  void saveTrajectoryRecord(ofstream &os);
  void saveTransitTrajectoryRecord(ofstream &os);
  void writeAssignmentMatrix();  // Angus
  void writeLaneChanging(TS_Lane *);  // tomer for LC

  float timeEntersSegment() { return timeEntersSegment_; }
  float timeInSegment();
  float speedInSegment();

  int logInfo();
  void watch(ostream &os = cout);

  float lcNormalRand() { return lcNormalRand_; }

private:

  int isReadyForNextDLC(int mode);
  int checkIfMandatory();

#ifndef USE_KAZI_MODELS

  // tomer- for lc model
  //cfc- branched in checkForLookAheadLC
  
  int checkForLC();
  int checkForLookAheadLC(vector <int> &);
  int checkMandatoryLookAhead_discrete(RN_Lane *, vector <int> &);//this is used for the intersection model cfc dec 06
#endif

  int checkMandatoryLaneChanging();
  int checkDiscretionaryLaneChanging();

  int checkNosingFeasibility(TS_Vehicle *av, TS_Vehicle *bv, float dis2stop);

  // tomer
  int checkIfLookAheadEvents();
  int checkMandatoryEventLC();
  int checkMandatoryLookAheadLC( vector <int> &);
  int checkDiscretionaryLookAheadLC( vector <int> &);

  float calcCarFollowingRate(TS_Vehicle *);	// mutable, once per step
  float carFollowingRate(TS_Vehicle *);         // immutable, subrouting
  float calcMergingRate();
  float calcSignalRate();
  float calcYieldingRate();
  float waitExitLaneRate();
  float waitAllowedLaneRate();
  float desiredSpeedRate(TS_Vehicle *front);
  float calcLaneDropRate();
  float calcCreateGapRate(TS_Vehicle *front, float gap);

#ifndef USE_KAZI_MODELS

  float calcForwardRate();
  float calcBackwardRate();
  float calcAdjacentRate();

#endif

  // tomer -function to calculate acc/dec for intersection. function itself is 
  // in TS_CFModells.cc


  float calcIntersectionRate( RN_IS* IS , int myMove );

  void calcUpdateStepSizes(); // called only once
  void calcNextStepSize();

  void load_error(const char *msg, Reader &is);

  float nextStepSize() { return nextStepSize_; }
  float updateStepSize(int i) {
	return 0.1 * updateStepSize_[i];
  }
  unsigned char updateStepSize_[4];	// 0=dec, 1=acc, 2=unform, 3=stopped
};

#endif
