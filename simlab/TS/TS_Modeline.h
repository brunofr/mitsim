//-*-c++-*------------------------------------------------------------
// TS_Modeline.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef TS_MODELINE_HEADER
#define TS_MODELINE_HEADER

#include <DRN/DRN_Modeline.h>

class TS_Modeline : public DRN_Modeline
{
   public:

	  TS_Modeline(Widget parent);
	  ~TS_Modeline() { }

	  void updateCounter(int i, int n);

   protected:
	  
	  Widget counters_;
};

#endif
