//-*-c++-*------------------------------------------------------------
// NAME: Traffic Simulation
// AUTH: Qi Yang
// FILE: TS_Signal.C
// DATE: Sat Aug 31 15:58:05 1996
//--------------------------------------------------------------------

#include <Tools/ToolKit.h>
#include <Tools/Random.h>

#include <Tools/Math.h>

#include <GRN/Constants.h>

#include "TS_Lane.h"
#include "TS_Vehicle.h"
#include "TS_VehicleLib.h"
#include "TS_Signal.h"
#include "TS_Parameter.h"

#include "TS_Link.h"

TS_Signal::TS_Signal() 
  : TC_Signal() 
{
}

TS_Signal::~TS_Signal() 
{ 
}


/*
 *--------------------------------------------------------------------
 *  Acceleration rate in response to a signal is in the view 
 *--------------------------------------------------------------------
 */

float
TS_Signal::acceleration(TS_Vehicle* pv, float dist)
{
  //  if (!pv->nextLink())
  //  {
  //    pv->removeFromNetwork();
  //  }

   if (dist > pv->distanceToNormalStop()) {
      return MAX_ACCELERATION;
   }
   int s;
   if (type() == CTRL_TS) {
	 s = stateForExit(link()->signalIndex(pv->nextLink()));
   } else {
	 s = state_;
   }
   int comply = pv->doesComply(type(), s);

   if (!comply) {
      return  MAX_ACCELERATION;
   }

   switch (s & SIGNAL_COLOR) {

	 case SIGNAL_BLANK:
	 {
	   return MAX_ACCELERATION;
	 }

	 case SIGNAL_RED:
	 {
	   if (comply &&
		   (!(s & SIGNAL_FLASHING) ||
			pv->currentSpeed() > SPEED_EPSILON)) {
		 return pv->brakeToStop(dist);
	   }
	   break;
	 }
	  
	 case SIGNAL_YELLOW:
	 {
	   if (comply &&
		   type() != CTRL_LUS &&
		   dist / Max(pv->currentSpeed(), theParameter->minSpeed()) >
		   theParameter->yellowStopHeadway()) {
		 return pv->brakeToStop(dist);
	   }
	   break;
	 }
	 case SIGNAL_GREEN:
	 {
	   return MAX_ACCELERATION;
	 }
   }
   return MAX_ACCELERATION;
}

