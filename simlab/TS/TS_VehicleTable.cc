//-*-c++-*------------------------------------------------------------
// NAME: Read vehicles from a file
// AUTH: Qi Yang
// FILE: TS_VehicleTable.C
// DATE: Tue Mar  5 23:04:25 1996
//--------------------------------------------------------------------

#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>

#include "TS_VehicleTable.h"
#include "TS_Vehicle.h"
#include "TS_VehicleList.h"

RN_Vehicle *
TS_VehicleTable::newVehicle()
{
   return theVehicleList->recycle();
}

