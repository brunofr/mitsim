//-*-c++-*------------------------------------------------------------
// NAME: Microcscopic Traffic Simulator
// AUTH: Qi Yang
// FILE: TS_Network.h
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#ifndef TS_NETWORK_HEADER
#define TS_NETWORK_HEADER

#include <list>

#ifdef INTERNAL_GUI
class DRN_DrawingArea;
#include <DRN/DrawableRoadNetwork.h>
#else  // match mode
#include <GRN/RoadNetwork.h>
#define DrawableRoadNetwork RoadNetwork
#endif // !INTERNAL_GUI

class Reader;
class IOService;
class TS_Node;
class TS_Link;
class TS_Segment;
class TS_Lane;
class TS_Incident;


class TS_Network : public DrawableRoadNetwork
{
   public:

      TS_Network();
      ~TS_Network();

      // Overload the virtual function defined in the base class

      RN_Node* newNode();
      RN_Link* newLink();
      RN_Segment* newSegment();
      RN_Lane* newLane(); 
      RN_Sensor* newSensor();
      RN_Signal* newSignal();
      RN_TollBooth* newTollBooth();
      RN_BusStop* newBusStopChars(); //margaret

      //IEM(May2) Implementation of virtual function in Roadnetwork
      //          Fill sensorsOfType_ with sensors sorted by intervalType
      virtual void sortSensors(); 

      void calcStaticInfo(void);
      void organize();
      
      TS_Node * tsNode(int i) { return (TS_Node *) node(i); }
      TS_Link * tsLink(int i) { return (TS_Link *) link(i); }
      TS_Segment * tsSegment(int i) { return (TS_Segment *) segment(i); }
      TS_Lane * tsLane(int i) { return (TS_Lane *) lane(i); }

      double loadInitialState(const char *name);

      void watchVehicles(ostream &os = cout);
      void watchVehiclesInPretripQueue(ostream &os = cout);
      void watchVehiclesInNetwork(ostream &os = cout);
      int watchQueueLength(ostream &os = cout);

      void calcSegmentData();

  //      void resetSensorReadings(); //IEM(Jun20) Commented out older version
      void resetSensorReadings(int intervalType); //IEM(May2) overloaded for vector
  //      void sendSensorReadings(IOService&, unsigned int code); //IEM(Jun20) Commented out older version
      void sendSensorReadings(IOService&, unsigned int code, int intervalType); //IEM(May2)
  //      void writeSensorReadings(); //IEM(Jun20) Commented out older version
      void writeSensorReadings(int intervalType); //IEM(May2)
  //      void dumpSensorReadings(double fromTime, double toTime); //IEM(Jun20) Commented out older version
      void dumpSensorReadings(double fromTime, double toTime, int intervalType); //IEM(May2)
      void enterVehiclesIntoNetwork();
      void calcVehicleStates();
      void moveVehicles();

      void calcLaneSpeeds();

      void initializeSegmentStatistics();
      void resetSegmentStatistics();
      void updateSegmentStatistics();

	  void guidedVehiclesUpdatePaths();

      // This is called periodically during the simulation

      void outputSegmentStatistics();

      // These are called at the end when simulation is done

      void outputSegmentTravelTimes();

      // This is called when a request is received from TMS or menu
      // button is presseed

      int dumpNetworkState(const char *);
	  void dumpTrajectoryRecords();
	  void dumpTransitTrajectoryRecords();	

      void collectMoeForUnfinishedTrips();
	  void recordLinkTravelTimeOfActiveVehicle();
      
#ifdef INTERNAL_GUI
      void unsetDrawingStates(DRN_DrawingArea *); // virtual
      void drawVehicles(DRN_DrawingArea *);
      void drawNetwork(DRN_DrawingArea *);	// virtual
	  void queryVehicle(const XmwPoint&);
	  void traceVehicle(const XmwPoint&);
	  void traceVehicle();		// popup existing dialog box
	  void setIncident(const XmwPoint&);
	  void editIncident(TS_Incident *inc);
	  void addIncident(TS_Segment *ps, float pos);
	  void showIncident(const XmwPoint &p);
#endif

	  list<TS_Incident*>& incidents() { return incidents_; }

   private:
	  list<TS_Incident*> incidents_;
};

inline TS_Network * tsNetwork() {
   return (TS_Network *)theNetwork;
}

#endif
