/*-*-c++-*-******************************************************/
/*    NAME: Traffic Simulation                                  */
/*    AUTH: Qi Yang                                             */
/*    FILE: TS_ODCell.h                                         */
/*    DATE: Fri Jun 23 22:07:37 1995                            */
/****************************************************************/


#ifndef TS_ODCELL_H
#define TS_ODCELL_H

#ifdef INTERNAL_GUI
#include <DRN/DRN_ODCell.h>
class TS_ODCell : public DRN_ODCell
#else
#include <GRN/OD_Cell.h>
class TS_ODCell : public OD_Cell
#endif

{
   public:

#ifdef INTERNAL_GUI
      TS_ODCell() : DRN_ODCell() { }
#else
      TS_ODCell() : OD_Cell() { }
#endif

      ~TS_ODCell() { }

      void emitVehicles();
      RN_Vehicle* newVehicle();
};

#endif
