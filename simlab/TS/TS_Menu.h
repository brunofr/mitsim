//-*-c++-*------------------------------------------------------------
// TS_Menu.h
//
// qiyang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef TS_MENU_HEADER
#define TS_MENU_HEADER

#include <DRN/DRN_Menu.h>

class TS_Menu : public DRN_Menu
{
	  friend class TS_Interface;
	  friend class TS_KeyInteractor;
	  
	  CallbackDeclare(TS_Menu);

   public:

	  TS_Menu(Widget parent);
	  ~TS_Menu() { }

   protected:

	  // New callbacks
	  
	  void randomizer ( Widget, XtPointer, XtPointer );
	  void setup ( Widget, XtPointer, XtPointer );
	  void advancedSetup ( Widget, XtPointer, XtPointer );
	  void output ( Widget, XtPointer, XtPointer );
	  void vehicle ( Widget, XtPointer, XtPointer );
	  void dumpState ( Widget, XtPointer, XtPointer );

   private:

	  static void browsePath ( Widget, XtPointer, XtPointer );
};

#endif
