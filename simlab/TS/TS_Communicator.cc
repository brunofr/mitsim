//-*-c++-*------------------------------------------------------------
// NAME: Functions for communication to TMS and MesoTS
// AUTH: Qi Yang
// FILE: TS_Communicator.C
// DATE: Sun Nov 19 12:40:25 1995
//--------------------------------------------------------------------


#include <iostream>
using namespace std;

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>
#include <Tools/Math.h>
#include <GRN/RN_Route.h>
#include <GRN/RN_DynamicRoute.h>

#include "TS_Communicator.h"
#include "TS_Engine.h"
#include "TS_Network.h"
#include "TS_Signal.h"
#include "TS_Parameter.h"
#include "TS_Exception.h"

#ifdef INTERNAL_GUI
#include <Xmt/Xmt.h>
#include <Xmt/Dialogs.h>
#include "TS_Interface.h"
#endif

TS_Communicator *theCommunicator = NULL;

TS_Communicator::TS_Communicator(int sndtag)
  : Communicator(sndtag)
{
}

double
TS_Communicator::batchStepSize()
{
  return theEngine->batchStepSize_;
}


// Make connection to its parent process (Simlab) and processes for
// other sibling modules.

void TS_Communicator::makeFriends()
{
  if (!simlab_.isConnected()) return;

  // Send simlab the timing information

  simlab_ << MSG_TIMING_DATA
		  << theSimulationClock->startTime()
		  << theSimulationClock->stopTime()
		  << theSimulationClock->stepSize()
		  << batchStepSize()
		  << ToolKit::workdir();
  simlab_.flush(SEND_IMMEDIATELY);

  tms_.init("TMS", simlab_.sndtag(), MSG_TAG_TMS);

  if (ToolKit::verbose()) {
	cout << theExec
		 << " is waiting process ids from SIMLAB ..." << endl;
  }
}


// Response to the messages received from other processes

int
TS_Communicator::receiveMessages(double sec)
{
  int msg, flag = 0;

  msg = receiveSimlabMessages(sec);
  if (msg < 0) {
	theEngine->quit(STATE_ERROR_QUIT);
	return -1;
  } else if (msg > 0) {
	flag |= 1;
  }
   
  msg = receiveTmsMessages(sec);
  if (msg < 0) {
	theEngine->quit(STATE_ERROR_QUIT);
	return -1;
  } else if (msg > 0) {
	flag |= 2;
  }

  return flag;
}


// Response to the messages received from other processes

int
TS_Communicator::receiveSimlabMessages(double sec)
{
  if (!simlab_.isConnected()) return 0;

  // Limited blocking receive (wait at most sec seconds)

  int flag = simlab_.receive(sec);
  if (flag <= 0) return flag;

  unsigned int type;
  simlab_ >> type;

  switch (type) {
	  
  case MSG_PROCESS_IDS:
	int id;
	simlab_ >> id;
	if (id > 0) {
	  tms_.connect(id);
	  tms_ << MSG_TIMING_DATA
		   << theGuidedRoute->infoPeriodLength();
	  tms_.flush(SEND_IMMEDIATELY);
	}
	if (ToolKit::verbose()) {
	  cout << theExec << " received process ids from SIMLAB." << endl;
	}
	break;
			
  case MSG_CURRENT_TIME:
	double now;
	simlab_ >> now;
	theSimulationClock->masterTime(now);
	break;

#ifdef INTERNAL_GUI
  case MSG_PAUSE:
	theInterface->pause(0);
	break;

  case MSG_RESUME:
	theInterface->run(0);
	break;

  case MSG_WINDOW_SHOW:
	theInterface->manage();
	break;

  case MSG_WINDOW_HIDE:
	theInterface->unmanage();
	break;

#else
  case MSG_PAUSE:
  case MSG_RESUME:
  case MSG_WINDOW_SHOW:
  case MSG_WINDOW_HIDE:
	{
	  break;			// not used in batch mode
	}
#endif

  case MSG_QUIT:
	theEngine->state(STATE_QUIT);
	break;

  case MSG_DONE:
	theEngine->state(STATE_DONE);
	break;

  default:
	skipUnknownMsg(simlab_.name(), type);
	break;
  }
  return flag;
}


int
TS_Communicator::receiveTmsMessages(double sec)
{
  if (!tms_.isConnected()) return 0;

  // Limited blocking receive (wait at most sec seconds)

  int flag = tms_.receive(sec);
  if (flag <= 0) return flag;

  unsigned int id;
  tms_ >> id;

  unsigned int type = id & MSG_TYPE ;

  if (type) {
	  switch (type) {
	  case MSG_TYPE_INCI:
	  {
		  id &= MSG_DATA_CODE ;
		  if (id < theNetwork->nSignals()) {
			  theNetwork->signal(id)->receive(tms_);
		  } else {
			  skipUnknownMsg(tms_.name(), id,
							 "Signal index out of range");
		  }
		  break ;
	  }
	  default:
		  do {			// signal state
			  id &= MSG_DATA_CODE;
			  if (id < theNetwork->nSignals()) {
				  theNetwork->signal(id)->receive(tms_);
			  } else {
				  skipUnknownMsg(tms_.name(), id,
								 "Signal index out of range");
			  }
			  tms_ >> id;
		  } while (id & MSG_TYPE);
		  break ;
	  }
	  return flag;
  }

  switch (id) {

  case MSG_DUMP_SURVEILLANCE:
        tms_ >> theEngine->sensorDumpNeeded_;
        break;

  case MSG_STATE_START:
	tms_ >> theEngine->dumpTime_;
	break;

  case MSG_MESO_DONE:
	{
	  // Switch to the new routing table calculated by MesoTS
	  double t_begin, t_end;
	  tms_ >> t_begin >> t_end; 

	  char *filename = theEngine->createFilename("ltt");
	  theGuidedRoute->updatePathTable(filename, 1.0, t_begin, t_end);
	  delete [] filename;
			
	  if (isSpFlag(INFO_FLAG_UPDATE_PATHS)) {
		tsNetwork()->guidedVehiclesUpdatePaths();
	  }

#ifdef INTERNAL_GUI
	  static const char *fmt = "Guidance for %s implemented at %s.";
	  char *t1 = Copy(theSimulationClock->convertTime(t_begin)) ;
	  char *t2 = Copy(theSimulationClock->convertTime(t_end)) ;
	  char *msg = StrCopy("%s-%s", t1, t2);
	  StrFree(t1);
	  StrFree(t2);
	  if (theEngine->mode(MODE_DEMO_PAUSE)) {
		XmtDisplayWarningMsgAndWait(theInterface->widget(), "dtaUsed",
									fmt,
									"MITSIM",		// default title
									NULL,			// default help
									msg,
									theSimulationClock->currentStringTime());		// args
	  } else if (theEngine->mode(MODE_DEMO)) {
		XmtDisplayWarningMsg(theInterface->widget(), "dtaUsed",
							 fmt,
							 "MITSIM",		// default title
							 NULL,			// default help
							 msg,
							 theSimulationClock->currentStringTime());		// args
	  }
	  theInterface->msgClear();
	  XmtMsgLinePrintf(theInterface->msgline(), fmt,
					   msg,
					   theSimulationClock->currentStringTime());
	  StrFree(msg);
#endif
	  break;
	}
  default:
	skipUnknownMsg(tms_.name(), id);
	break;
  }

  return flag;
}
