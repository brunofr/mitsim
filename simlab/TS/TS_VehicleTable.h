//-*-c++-*------------------------------------------------------------
// NAME: Read vehicles from a file
// AUTH: Qi Yang
// FILE: TS_VehicleTable.h
// DATE: Tue Mar  5 23:00:07 1996
//--------------------------------------------------------------------

#ifndef TS_VEHICLETABLE_HEADER
#define TS_VEHICLETABLE_HEADER

#include <GRN/VehicleTable.h>

class TS_VehicleTable : public VehicleTable
{
   public:

      TS_VehicleTable() : VehicleTable() { }
      ~TS_VehicleTable() { }

      RN_Vehicle * newVehicle();
};

#endif
