//-*-c++-*------------------------------------------------------------
// NAME: Microscopic traffic simulator
// AUTH: Qi Yang
// FILE: TS_TollBooth.h
// DATE: Fri Aug 30 22:12:28 1996
//--------------------------------------------------------------------

#ifndef TS_TOLLBOOTH_HEADER
#define TS_TOLLBOOTH_HEADER

class TS_Vehicle;

#include <GRN/RN_TollBooth.h>
#include "TS_Signal.h"

class TS_TollBooth : public RN_TollBooth, public TS_Signal
{
   public:

      TS_TollBooth();
      ~TS_TollBooth() { }

      float delay(TS_Vehicle *);
      float acceleration(TS_Vehicle *, float);
};

#endif
