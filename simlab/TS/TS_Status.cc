//-*-c++-*------------------------------------------------------------
// NAME: Microscopic Traffic Simulator
// AUTH: Qi Yang
// FILE: TS_Status.C
// DATE: Fri Nov 17 10:12:59 1995
//--------------------------------------------------------------------


#include <cstdio>
#include <iomanip>
#include <cstring>
#include <ctype.h>
using namespace std;

#include <sys/types.h>
#include <unistd.h>

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>

#ifdef INTERNAL_GUI
#include "TS_Interface.h"
#include "TS_Modeline.h"
#endif

#include "TS_Engine.h"
#include "TS_Status.h"
#include "TS_Network.h"

TS_Status *theStatus = new TS_Status;

TS_Status::TS_Status()
  :	nActive_(0),
	nArrived_(0),
	nNoPath_(0),
	nInQueue_(0),
	nErrors_(0),
	logFile_(NULL)
{
}


TS_Status::~TS_Status()
{
  clean();
}

void
TS_Status::clean()
{
  if (logFile_) {
	delete [] logFile_;
	logFile_ = NULL;
  }
  nActive_ = 0;
  nArrived_ = 0;
  nNoPath_ = 0;
  nInQueue_ = 0;
  nErrors_ = 0;
}

void
TS_Status::openLogFile()
{
  logFile_ = Copy(ToolKit::outfile("mitsim.out"));
  osLogFile_.open(logFile_);
}


void
TS_Status::closeLogFile()
{
  osLogFile_.close();
  if (nErrors_) {
	cout << endl << nErrors_ << " error(s) detected. "
		 << "See <" << logFile_ << "> for details." << endl;
  } else {
	::remove(logFile_);
  }
}


void TS_Status::report(ostream &os)
{
  os << endl << "Number of Vehicles:" << endl
	 << indent << "Active      = " << nActive_ << endl
	 << indent << "Arrived     = " << nArrived_ << endl
	 << indent << "Incompleted = " << nNoPath_ << endl
	 << indent << "InQueue     = " << nInQueue_ << endl;

  os << endl << "Running Time Summary:" << endl
	 << indent << "Simulated = "
	 << theSimulationClock->timeSimulated() << endl
	 << indent << "Real Time = "
	 << theSimulationClock->timeSinceStart() << endl
	 << indent << "CPU Time  = "
	 << theSimulationClock->cpuTimeSinceStart() << endl;

  os << endl;
}


void
TS_Status::print(ostream &os)
{
#ifndef INTERNAL_GUI
  static int first = 1;
  if (first && ToolKit::verbose()) {
	os << endl << indent;
	os.fill(' ');
	os << setw(10) << "Time"
	   << setw(8) << "Active"
	   << setw(8) << "Arrived"
	   << setw(8) << "Incomp."
	   << setw(8) << "InQueue" << endl;
	first = 0;
  }
  if (ToolKit::verbose()) {
	os << indent
	   << setw(10) << theSimulationClock->currentStringTime()
	   << setw(8) << nActive_
	   << setw(8) << nArrived_
	   << setw(8) << nNoPath_
	   << setw(8) << nInQueue_;
  }

  int length = tsNetwork()->watchQueueLength(os);

  if (ToolKit::verbose()) {
	os << endl;
  } else if (length > 0) {
	os << " queued at "
	   << theSimulationClock->currentStringTime()
	   << endl;
  }
#else
  if (tsNetwork()->watchQueueLength()) {
	os << " queued at "
	   << theSimulationClock->currentStringTime()
	   << endl;
  }
#endif
}


#ifdef INTERNAL_GUI

void TS_Status::showStatus()
{
  TS_Modeline * modeline = (TS_Modeline*) theInterface->modeline();
  modeline->updateCounter(0, nActive_);
  modeline->updateCounter(1, nInQueue_);
  modeline->updateCounter(2, nNoPath_);
  modeline->updateCounter(3, nArrived_);
}

#endif
