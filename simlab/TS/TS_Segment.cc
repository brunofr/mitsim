//-*-c++-*------------------------------------------------------------
// FILE: TS_Segment.C
// AUTH: Qi Yang
// DATE: Wed Oct 18 14:29:29 1995
//--------------------------------------------------------------------

#include <Tools/Math.h>

#include <Tools/ToolKit.h>

#include "TS_Network.h"
#include "TS_Node.h"
#include "TS_Link.h"
#include "TS_Segment.h"
#include "TS_Lane.h"
#include "TS_Parameter.h"
#include "TS_Engine.h"
#include "TS_Vehicle.h"

#ifdef INTERNAL_GUI
#include <DRN/DRN_DrawingArea.h>
#include "TS_Interface.h"
#include "TS_Symbols.h"
#include "TS_DrawableVehicle.h"
#endif


TS_Segment::TS_Segment() : DRN_Segment(),
  firstVehicle_(NULL),
  lastVehicle_(NULL),
  nVehicles_(0)
{
}


TS_Segment::~TS_Segment()
{
  delete [] nSamplesTravelTimeAtEntering_;
  delete [] sumOfTravelTimeAtEntering_;
}


// Overload the base class function

void
TS_Segment::calcStaticInfo()
{
#ifdef INTERNAL_GUI
  DRN_Segment::calcStaticInfo();
#else
  RN_Segment::calcStaticInfo();
#endif
  density_ = 0.0;
  speed_ = freeSpeed_;
}


/*
 *------------------------------------------------------------------
 * Calculate the density of a segment, in vehicle/kilometer
 *------------------------------------------------------------------
 */

float
TS_Segment::calcDensity(void)
{
  density_ = 1000.0 * nVehicles_ / (length_ * nLanes());
  return (density_);
}


/*
 *-------------------------------------------------------------------
 * Calculate the harmonic mean speed of the vehicles in a segment, in
 * meter/sec.
 *-------------------------------------------------------------------
 */

float
TS_Segment::calcSpeed(void)
{
  if (nVehicles_ <= 0) return speed_ = freeSpeed_;
  double sum = 0.0;
  TS_Vehicle *pv = firstVehicle();
  while (pv) {
	if (pv->currentSpeed() > SPEED_EPSILON) {
	  sum += 1.0 / pv->currentSpeed();
	} else {
	  sum += 1.0 / SPEED_EPSILON;
	}
	pv = pv->macroTrailing();
  }
  return speed_ = nVehicles_ / sum;
}


int
TS_Segment::calcFlow()
{
  float x = 3.6 * calcSpeed() * calcDensity();
  return (int) (x + 0.5);
}

int
TS_Segment::flow()
{
  float x = 3.6 * speed_ * density_;
  return (int) (x + 0.5);
}


#ifdef INTERNAL_GUI

TS_Vehicle* TS_Segment::pickVehicle(WcsPoint& pressed)
{
  if (!theSymbols()->viewType().is(DRN_Symbols::VIEW_MICRO) ||
	theDrawingArea->resolution() < DRN_DrawingArea::LOW_RESOLUTION ||
	!theSymbols()->vehicleBorderCode().value()) {
	return NULL;
  }

  float offset2 = DBL_INF, dis2, pos;
  TS_Vehicle *pv = firstVehicle_;
  TS_Vehicle *closest = NULL;
  while (pv) {
	if (((TS_DrawableVehicle *)pv)->isVisible()) {
	  pos = pv->position() + 0.5 * pv->length() / pv->lane()->length();
	  dis2 = pressed.distance_squared(pv->tsLane()->centerPoint(pos));
	  if (dis2 < offset2) {
		closest = pv;
		offset2 = dis2;
	  }
	}
	pv = pv->macroTrailing();
  }

  if (closest == NULL || sqrt(offset2) > closest->length()) {
	theInterface->msgSet("No vehicle found");
	return NULL;
  }

  return closest;
}

void TS_Segment::queryVehicle(WcsPoint& pressed)
{
  TS_DrawableVehicle *pv = (TS_DrawableVehicle *)pickVehicle(pressed);
  if (pv) {
	pv->query();
  }
}

void TS_Segment::traceVehicle(WcsPoint& pressed)
{
  TS_DrawableVehicle *pv = (TS_DrawableVehicle *)pickVehicle(pressed);
  if (pv) {
	pv->trace();
  }
}

#endif
