//-*-c++-*------------------------------------------------------------
// NAME: Microcscopic Traffic Simulator
// AUTH: Qi Yang & Daniel Morgan
// FILE: TS_BusLib.h
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#ifndef TS_BUS_LIB_HEADER
#define TS_BUS_LIB_HEADER
using namespace std;

class TS_Parameter;
class TS_Vehicle;
class Scaler;
class ArrayElement;

class TS_BusLib
{
   public:

      TS_BusLib();
      ~TS_BusLib();

      inline char *name() { return name_; }
      inline float length() { return length_; }
      inline int seatingCapacity() { return seatingCapacity_; }
      inline int totalCapacity() { return totalCapacity_; }
      inline float boardingRate() { return boardingRate_; }
      inline float alightingRate() { return alightingRate_; }
      inline float deadTimeUpperBound() { return deadTimeUpperBound_; }
      inline float deadTimeLowerBound() { return deadTimeLowerBound_; }
      inline float crowdingFactor() { return crowdingFactor_; }

      void loadAttributes(ArrayElement *);

      void print(std::ostream &os = cout);

   protected:

      char *name_;		// name of the bus type

      float length_;   // bus length - some buses (e.g. articulated) are longer than others
      int seatingCapacity_;
      int totalCapacity_;
      float crowdingFactor_;
      float boardingRate_;
      float alightingRate_;
      float deadTimeUpperBound_;  
      float deadTimeLowerBound_;

};

#endif
