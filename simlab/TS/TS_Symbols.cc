//-*-c++-*------------------------------------------------------------
// TS_Symbols.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include "TS_Symbols.h"

void TS_Symbols::registerAll()
{
   DRN_Symbols::registerAll();
   vehicleBorderCode_.create("vehicleBorderCode",
							 XtRInt, VEHICLE_BORDER_TYPE);
   vehicleShadeCodes_.create("vehicleShadeCode",
							XtRInt, 0);
   isVehicleLabelOn_.create("isVehicleLabelOn",
							XtRBoolean, False);
}

#endif // INTERNAL_GUI
