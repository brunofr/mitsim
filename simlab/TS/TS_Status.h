//-*-c++-*------------------------------------------------------------
// NAME: Microscopic Traffic Simulator
// AUTH: Qi Yang
// FILE: TS_Status.C
// DATE: Fri Nov 17 10:12:59 1995
//--------------------------------------------------------------------

#ifndef TS_STATUS_HEADER
#define TS_STATUS_HEADER

#include <fstream>

class TS_Status
{
   public:

      TS_Status();
      ~TS_Status();
	  void clean();

      ofstream& osLogFile() { return osLogFile_; }

      void openLogFile();
      void closeLogFile();

      int nActive(int n) { return nActive_ += n; }
      int nArrived(int n) { return nArrived_ += n; }
      int nNoPath(int n) { return nNoPath_ += n; }
      int nInQueue(int n) { return nInQueue_ += n; }

      int nActive() { return nActive_; }
      int nInQueue() { return nInQueue_; }

      inline void nErrors(int n) {nErrors_ += n;}
      inline int nErrors() {return nErrors_;}

      void report(ostream &os = cout);
      void print(ostream &os = cout);

#ifdef INTERNAL_GUI
      void showStatus();
#endif

   protected:

      char* logFile_;
      int nErrors_;		// number of errors
      ofstream osLogFile_;	// error log

      int nActive_;		// in network
      int nArrived_;		// arrived
      int nNoPath_;		// no path
      int nInQueue_;		// queuing outside
      
      
};

extern TS_Status *theStatus;	// current simulation status

#endif
