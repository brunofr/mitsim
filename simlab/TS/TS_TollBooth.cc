//-*-c++-*------------------------------------------------------------
// NAME: Traffic Simulation 
// AUTH: Qi Yang
// FILE: TS_TollBooth.C
// DATE: Fri Aug 30 21:59:01 1996
//--------------------------------------------------------------------

#include <Tools/ToolKit.h>
#include <Tools/Random.h>

#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>

#include "TS_Vehicle.h"
#include "TS_TollBooth.h"
#include "TS_Parameter.h"

#ifdef INTERNAL_GUI
#include <DRN/DRN_DrawingArea.h>
#endif

TS_TollBooth::TS_TollBooth()
   : TS_Signal(),
     RN_TollBooth()
{
}


/*
 *------------------------------------------------------------------
 * Returns delay a vehicle may experienced for paying toll.
 * The time is in seconds.
 *------------------------------------------------------------------
 */

float TS_TollBooth::delay(TS_Vehicle* pv)
{
   // Some vehicles (eg trucks, buses) may expect higher
   // delay than others.

   float u = serviceTime_ * pv->tollBoothDelayFactor(); 

   // Assume the delay is negative exponential distributed.

   float delay = theRandomizer->rrandom(u);
   u = theParameter->maxTollBoothDelay();

   // If it is outside the range, draw a uniform random number

   if (delay > u) {
	  return theRandomizer->urandom(0.0, u); // uniform
   } else {
	  return delay;
   }
}


/*
 *--------------------------------------------------------------------
 * Returns the acceleration at toll booth.
 *--------------------------------------------------------------------
 */

float TS_TollBooth::acceleration(TS_Vehicle* pv, float dist)
{
   float limit = speed_ * theParameter->speedFactor();

   if ((state() & SIGNAL_COLOR) <= SIGNAL_RED) {	// lane is closed
	  return pv->brakeToStop(dist);
   } else if (pv->status(STATUS_STOPPED)) {
      // toll paid
      pv->unsetStatus(STATUS_STOPPED);
      pv->setStatus(STATUS_ALREADY_PAID);
      return MAX_ACCELERATION;
   } else if (pv->status(STATUS_ALREADY_PAID)) {
      // alreadu paid his toll
      return MAX_ACCELERATION;
   } else if (dist > pv->distanceToNormalStop())  {
      // constraints is not applied in this case
      return MAX_ACCELERATION;
   } else if (dist > DIS_EPSILON) {
      // prepared to stop or reduce speed
        if (pv->currentSpeed()>SPEED_EPSILON) {
          return pv->brakeToTargetSpeed(dist, limit);}
	else {return MAX_ACCELERATION; }
   } else if (limit > SPEED_EPSILON) {
      // in ETC lane
      return MAX_ACCELERATION;
   } else {
      // stop to pay toll
      pv->setStatus(STATUS_STOPPED);
      pv->cfTimer(delay(pv));
      return -FLT_INF;
   }
}
