//-*-c++-*------------------------------------------------------------
// NAME: Microscopic Traffic Simulator
// AUTH: Qi Yang
// FILE: TS_Engine.h
// DATE: Tue Nov 21 21:55:21 1995
//--------------------------------------------------------------------

#ifndef TS_ENGINE_HEADER
#include <Templates/Object.h>
#define TS_ENGINE_HEADER
using namespace std;

#include <Tools/SimulationEngine.h>
#include <vector>  //IEM(Apr19)
#include <Tools/GenericVariable.h> //IEM(Apr24)

class TS_Engine : public SimulationEngine
{
  friend class TS_FileManager;
  friend class TS_Communicator;
  friend class TS_CmdArgsParser;

#ifdef INTERNAL_GUI
  friend class TS_SetupDialog;
#endif

protected:
      
  double survPointStepSize_; /* sending point surv */
  double survAreaStepSize_;	/* sending area surv */
  std::vector <double> survPointStepSizes_; //IEM(Apr24) 0 is the default one (==survPointStepSize_),
                                       //           >=1 are additional ones

  vector <bool> sensorOutputFlags_; //IEM(Jun19) For each additional sensor, a 0 for no report, a 1 for yes

  //IEM(Apr24) Given a vector of numbers (by master.mitsim parser), stick them in survPointStepSizes_:
  int survPointStepSizes(GenericVariable &gv); 

  //IEM(Jun19) Given a vector of 0s and 1s, sticks them in sensorOutputFlags_
  int TS_Engine::sensorOutputFlags(GenericVariable &gv);

  double segmentStatisticsSamplingStepSize_;
  double segmentStatisticsReportStepSize_;

  double batchStepSize_;	/* update console and queue info */
  double microStepSize_;	/* update animation */
  double macroStepSize_;	/* update segment color */
  double pathStepSize_;	/* calc shortest path */

  double stateStepSize_;
  double depRecordStepSize_;
  double trajectoryStepSize_;
  double transitTrajectoryStepSize_;

  double dumpTime_;			// time to dump network state

  int sensorDumpNeeded_ ;  // dump sensor readings into file

public:
      
  TS_Engine();
  ~TS_Engine() { }

  const char *ext();		// virtual

  void setTimes();

  // This function starts the simulation loops. It will NOT return
  // until simulation is done (or cancelled in graphical mode).
  // This function calls simulationLoop() iteratively or as a
  // recusive timeout callback.

  void run();

  void quit(int s);			// virtual

  int loadMasterFile();
  int loadSimulationFiles(); // virtual
  int simulationLoop();
      
  int receiveMessages(double sec = 0.0);

  //IEM(Apr26)
  int nSurvPointStepSizes() {return survPointStepSizes_.size();}

  void save(std::ostream &os = cout); // virtual
};

extern TS_Engine * theEngine;

#endif

