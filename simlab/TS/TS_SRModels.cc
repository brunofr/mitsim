//-*-c++-*------------------------------------------------------------
// NAME: Microscopic Traffic Simulation
// NOTE: Response to traffic signals
// AUTH: Qi Yang
// FILE: TS_SRModels.C
// DATE: Mon Oct 14 17:50:59 1996
//--------------------------------------------------------------------

#include <Tools/Math.h>

#include <Tools/ToolKit.h>
#include <Tools/Random.h>
#include <Tools/SimulationClock.h>

#include <GRN/Constants.h>
#include <GRN/RN_Path.h>
#include <GRN/RN_CtrlStation.h>

#include "TS_Node.h"
#include "TS_Link.h"
#include "TS_Segment.h"
#include "TS_Lane.h"
#include "TS_VehicleList.h"
#include "TS_Vehicle.h"
#include "TS_VehicleLib.h"
#include "TS_Signal.h"
#include "TS_TollBooth.h"
#include "TS_BusStop.h" //margaret
#include "TS_Incident.h"
#include "TS_Parameter.h"
#include "TS_Engine.h"
#include "TS_Status.h"
#include "TS_Network.h"

/*
 *---------------------------------------------------------------------
 * Update behavior parameters, make enroute decision, etc.  Partially
 * implemented.
 *
 * Q: Do we want to response to a same signs multiple times?  (as 
 * vehicle's position changes).
 *---------------------------------------------------------------------
 */

void
TS_Vehicle::responseSignalsAndSigns()
{
  CtrlList ctrl = nextCtrl_;
  float visfac = attr(ATTR_FAMILIARITY) ? 2.0 : 1.0;
  float vis = visfac * theParameter->visibility();
  float dis;
  int nlanes = segment()->nLanes();
   
  while (ctrl &&
	(dis = distanceFromDevice(ctrl)) < vis ) {

	float view = (*ctrl)->visibility() * visfac; 

	if (dis < view) {

	  switch ((*ctrl)->type()) {
	  case CTRL_VMS:
		{
		  unsigned int s = (*ctrl)->signal(0)->state();
		  unsigned int x = s & SIGN_TYPE;

		  if (x != SIGN_LANE_USE_RULE &&
			x != SIGN_LANE_USE_PATH) {

			// not concerned about the number of lanes

			responseVms(s);

		  } else if ((*ctrl)->segment()->nLanes() == nlanes) {

			// for a lane use sign related the rule or path,
			// make sure the number of lanes are the same

			responseVms(s);
		  }
		  break;
		}
	  case CTRL_VSLS:
		{
		  unsigned int s = (*ctrl)->signal(0)->state(); 
		  responseVsls(s);
		  break;
		}
	  case CTRL_LUS:
		{
		  int k = lane_->localIndex();
		  unsigned int signature = 1 << k;
		  int n = (*ctrl)->nLanes();
		  for (int i = 0; i < n; i ++) {
			// Look the most likely connected lane first
			RN_Signal *signal = (*ctrl)->signal((i + k) % n);
			if (signal && signal->lane()->isConnected(signature)) {
			  responseLus(signal->state());
			  break;
			}
		  }
		  break;
		}
	  case CTRL_INCIDENT:
		{
		  reportIncident(ctrl);
		  break;
		}
	  default:
		{
		  break;
		}
	  }
	}
	ctrl = ctrl->next();
  }
}


/*
 *-------------------------------------------------------------------
 * response to speed limits signs
 *-------------------------------------------------------------------
 */

void
TS_Vehicle::responseVsls(unsigned int speed_limit)
{
  if (speed_limit == signedSpeed_) {
	return;
  }
  calcDesiredSpeed(speed_limit);
}


/*
 *-------------------------------------------------------------------
 * response to lane use signs
 *-------------------------------------------------------------------
 */

void
TS_Vehicle::responseLus(unsigned int s)
{
  if (flag(FLAG_VMS_LANE_USE_BITS)) {
	// VMS has the higher priority
	return;
  }

  if (s & SIGNAL_RIGHT) {
	setStatus(STATUS_RIGHT);
  } else if (s & SIGNAL_LEFT) {
	setStatus(STATUS_LEFT);
  }
}

/*
 *-------------------------------------------------------------------
 * Response to message signs
 *-------------------------------------------------------------------
 */

void
TS_Vehicle::responseVms(unsigned int state)
{
  switch (SIGN_TYPE & state) {
  case SIGN_LANE_USE_RULE:
	{
	  // The sign is related to lane use.
	  if (state & SIGN_MANDATORY ||
		attr(ATTR_MSG_TYPE_LU_COMPLY)) {
		responseToLaneUseRuleMsgSign(state);
	  }
	  break;
	}
  case SIGN_LANE_USE_PATH:
	{
	  if (state & SIGN_MANDATORY ||
		attr(ATTR_MSG_PATH_LU_COMPLY)) {
		responseToLaneUsePathMsgSign(state);
	  }
	  break;
	}
  case SIGN_PATH:
	{
	  if (state & SIGN_MANDATORY ||
		attr(ATTR_MSG_GUIDE_COMPLY)) {
		responseToPathMsgSign(state);
	  }
	  break;
	}
  case SIGN_ENROUTE:
	{
	  responseToEnrouteMsg(state);
	  break;
	}
  default:
	{
	  break;
	}
  }
}


void
TS_Vehicle::responseToEnrouteMsg(unsigned int state)
{
  // Check first if the message applies to the class of the driver

  if (SignEnrouteClass(state) != infoType()) return;

  // Check if the message has already been read by the driver using
  // the Time tag

  unsigned int tag = SignEnrouteTimeTag(state);
  if (theSimulationClock->currentTime() <= tag) return;

  // The message has been read (whatever the compliance will
  // be). Therefore, we can save the Time tag.

 // read the message just once. otherwise the compliance will always be 100% - tomer

 if (theSimulationClock->currentTime() > info_) return;

  info_ = theSimulationClock->currentTime(); 
  
  // Check the compliance
  // 1. If the rate is greater or equal to 10 (i.e.equal to 100%)
  // 2. If the rate is 0, it means that everyone complies
  // 3. A random number, based on the compliance rate, is drawn to
  // decide if the driver complies or not

 

  int r = SignEnrouteComply(state);
  
  if (!(r >= 10 || r == 0 || theRandomizer->brandom(0.1 * r))) {
	return;
  }

  setAttr(ATTR_ACCESSED_INFO);

  // Now, we are ready to response to the message

  changeRoute();
}


void
TS_Vehicle::responseToLaneUseRuleMsgSign(unsigned int state)
{
  int sign;

  // CHECK IF THE MESSAGE IS RELEVANT REGARDING TO:

  if (SIGN_NEGATIVE & state) {

	// vehicle class

	if ((sign = (SIGN_LANE_USE_CLASS & state)) && // class specific
	  sign == type()) {	// does not apply to this class
	  return;
	}

	// vehicle group

	if ((sign = (SIGN_LANE_USE_GROUP & state)) && // group specific
	  (sign & group())) {	// does not apply to this group
	  return;
	}

  } else {

	// vehicle class

	if ((sign = (SIGN_LANE_USE_CLASS & state)) && // class specific
	  sign != type()) {	// does not apply to this class
	  return;
	}

	// vehicle group

	if ((sign = (SIGN_LANE_USE_GROUP & state)) && // group specific
	  !(sign & group())) {	// does not apply to this group
	  return;
	}
  }

  // CHECK THE LANE POSITION AND CANCEL OR SET LANE CHANGES

  responseToLaneUseMsgSign(state);
}


void
TS_Vehicle::responseToLaneUsePathMsgSign(unsigned int state)
{
  int sign = SignSuffix(state); // link index

  // CHECK IF THE LINK IS IN MY PATH

  int rel = isLinkInPath(
	theNetwork->link(sign),
	SignLaneUseNumLinks(state));

  // CHECK THE LANE POSITION AND CANCEL OR SET LANE CHANGES

  if (SIGN_NEGATIVE & state) {
	if (rel == 0) {
	  responseToLaneUseMsgSign(state);
	}
  } else {
	if (rel == 1) {
	  responseToLaneUseMsgSign(state);
	}
  }
}


// This function makes the vehicles use the lane suggested by VMS

void
TS_Vehicle::responseToLaneUseMsgSign(unsigned int state)
{
  int sign;

  int n = SignLaneUseNumLanes(state);
  int i = lane_->localIndex();
  int m = segment()->nLanes();
  int p;
  
  if (!(sign = (SIGN_LANE_USE_DIR & state)) || // stay in lane
	sign == SIGN_LANE_USE_DIR) { // also stay in lane

	unsetStatus(STATUS_CHANGING);
	unsetFlag(FLAG_VMS_LANE_USE_PIVOT);
	setFlag(FLAG_VMS_LANE_USE_DIR);

  } else {

	this->unsetFlag(FLAG_VMS_LANE_USE_BITS);

	if (sign == SIGN_LANE_USE_DIR_R && // VMS tells me go right
	  !flag(FLAG_ESCAPE_LEFT | FLAG_BLOCK_RIGHT)) {
	  // I am not escaping to left and Right lane(s) is not blocked

	  // This vehicle should use the n right lanes
	 
	  p = m - n;		// pivot index
	  if (i < p) {
		setStatus(STATUS_RIGHT);
	  } else if (i == p && status(STATUS_LEFT)) {
		unsetStatus(STATUS_LEFT);
	  }
	  setFlag(FLAG_VMS_LANE_USE_RIGHT | VmsLaneUseIndexToPivot(p));

	} else if (sign == SIGN_LANE_USE_DIR_L && // VMS tells me go left
	  !flag(FLAG_ESCAPE_RIGHT | FLAG_BLOCK_LEFT)) {
	  // I am not escaping to right and Left lane(s) is not blocked

	  // This vehicle should use the n left lanes

	  p = n - 1;		// pivot index
	  if (i >= n) {
		setStatus(STATUS_LEFT);
	  } else if (i == p && status(STATUS_RIGHT)) {
		unsetStatus(STATUS_RIGHT);
	  }
	  setFlag(FLAG_VMS_LANE_USE_LEFT | VmsLaneUseIndexToPivot(p));
	}
  }

  if (state & SIGN_MANDATORY) {
	if (flag(FLAG_VMS_LANE_USE_RIGHT) && i<(m-n) ) {
	  setFlag(FLAG_ESCAPE_RIGHT);
	} else if (flag(FLAG_VMS_LANE_USE_LEFT) && i>(n-1) ) {
	  setFlag(FLAG_ESCAPE_LEFT);
	}
  }
}


void
TS_Vehicle::responseToPathMsgSign(unsigned int)
{
  // will be implemented later.
}


/*
 *-------------------------------------------------------------------
 * response to an event such as incident or signal failure
 *-------------------------------------------------------------------
 */

void
TS_Vehicle::reportIncident(CtrlList)
{
  /* May report an event if it is an service patrol vehicle
   * or a vehicle equipped will cellular phone
   */
}


/*
 *-------------------------------------------------------------------
 * Returns true if it comply to the signal when it is red or 0
 * otherwise
 *-------------------------------------------------------------------
 */

int
TS_Vehicle::doesComply(unsigned int signal_type,
  unsigned int signal_state)
{
  int comply = 0;

  switch (signal_type) {

  case CTRL_TS:
	{
	  comply = attr(ATTR_TS_COMPLY);
	  break;
	}

  case CTRL_PS:
	{
	  comply = attr(ATTR_PS_COMPLY);;
	  break;
	}

  case CTRL_RAMPMETER:
	{
	  comply = attr(ATTR_RAMPMETER_COMPLY);
	  break;
	}

  case CTRL_LUS:
	{
	  if (!flag(FLAG_NOSING) || !flag(FLAG_NOSING_FEASIBLE)) {
		unsigned int signal_color = signal_state & SIGNAL_COLOR;
		if (signal_color == SIGNAL_YELLOW) {
		  comply = attr(ATTR_LUS_COMPLY_YELLOW);
		} else if (signal_color == SIGNAL_RED) {
		  comply = attr(ATTR_LUS_COMPLY_RED);
		}
	  }
	  break;
	}
  }
  return comply;
}
