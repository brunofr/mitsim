//-*-c++-*------------------------------------------------------------
// TS_CmdArgsParser.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class definition/implementation for the class
//--------------------------------------------------------------------

#ifndef TS_CMDARGSPARSER_HEADER
#define TS_CMDARGSPARSER_HEADER

#include <Tools/CmdArgsParser.h>

class TS_CmdArgsParser : public CmdArgsParser
{
   public:
      
      TS_CmdArgsParser();
      ~TS_CmdArgsParser() { }
};

#endif
