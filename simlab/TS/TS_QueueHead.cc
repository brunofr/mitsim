//-*-c++-*------------------------------------------------------------
// NAME: Traffic Simulation
// NOTE: Holds information for queues
// AUTH: Qi Yang
// FILE: TS_QueueHead.C
// DATE: Wed Oct 23 20:31:21 1996
//--------------------------------------------------------------------

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <Tools/Math.h>

#include <Tools/Reader.h>
#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>

#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>

#include "TS_QueueHead.h"
#include "TS_Parameter.h"
#include "TS_Network.h"
#include "TS_Lane.h"
#include "TS_Exception.h"
#include "TS_Engine.h"
#include "TS_FileManager.h"

#ifdef EXTERNAL_MOE
#include <MOE/MOE_TSSampler.h>
#endif

FILE* TS_QueueHead::fpo_ = NULL;
int TS_QueueHead::nElements_ = 0;
TS_QueueHead* TS_QueueHead::elements_ = NULL;
double TS_QueueHead::stepSize_ = 300.0;
double TS_QueueHead::nextTime_ = 0.0;

void
TS_QueueHead::load(Reader& is)
{
   is >> stepSize_ >> nElements_;

   // We always parse the input file so other MOE data can be read
   // later

   elements_ = new TS_QueueHead[nElements_];
   if (!is.findToken(OPEN_TOKEN)) theException->exit(1);

   for (int i = 0; i < nElements_; i ++) {
      elements_[i].read(is);
   }

   if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);

   // We open queue file only when this output is chosen

   if (theEngine->chosenOutput(OUTPUT_QUEUE_STATISTICS)) {

      const char *filename = ToolKit::outfile(theFileManager->queueFile());

      // open queue output file

      if ((fpo_ = fopen(filename, "w")) == NULL) {
		 cerr << "Error:: Cannot open queue output file <"
			  << filename << ">" << endl;
		 theException->exit();
      }
   }
}


void TS_QueueHead::close()
{
   if (fpo_) {
	  fprintf(fpo_, "\n# The End\n");
	  fclose(fpo_);
   }
}

void
TS_QueueHead::read(Reader& is)
{
   int code;
   is >> code >> position_
      >> firstMaxSpeed_ >> lastMaxSpeed_ >> maxGap_;
   if (lane_ = (TS_Lane *)theNetwork->findLane(code)) {
      firstMaxSpeed_ *= theParameter->speedFactor();
      lastMaxSpeed_ *= theParameter->speedFactor();
      maxGap_ *= theParameter->lengthFactor();
      position_ = 1.0 - position_;
      distance_ = position_ * lane_->length();
   } else {
      cerr << "Error:: Unknown lane <" << code << ">. ";
      is.reference();
      theException->exit(1);
   }
}


void
TS_QueueHead::print(ostream &os)
{
   os << indent
      << lane_->code() << endc
      << (1.0 - position_) << endc
      << firstMaxSpeed_/theParameter->speedFactor() << endl;
}


int
TS_QueueHead::report()
{  
   int num = 0;

   // Save the pointer

   long int start_pos = ftell(fpo_);
   fprintf(fpo_, "%d {\n", lane_->code());

   TS_Vehicle* fv = lane_->findUpVehicle(distance_);
   TS_Lane::unMarkAllLanes();
   num = lane_->searchQueue(this, NULL, fv);

   fprintf(fpo_, "} %d\n",  num);
   
   // Back to the start position if no queues

   if (!num) fseek(fpo_, start_pos, SEEK_SET);

   return num;
}


// This is static function

void
TS_QueueHead::reportQueues()
{
   static int headline = 1;
   if (headline) {
      fprintf(fpo_, "# ReportTime\n");
      fprintf(fpo_, "# StartLaneID {\n");
      fprintf(fpo_, "#   FromLaneID ToLaneID QueueLength [# Spillback]\n");
      fprintf(fpo_, "# } TotalVehiclsInQueues\n");
      headline = 0;
   }

   // Save the pointer

   long int start_pos = ftell(fpo_);

   int num = 0;
   fprintf(fpo_, "\n%s\n", theSimulationClock->currentStringTime());
   for (int i = 0; i < nElements_; i ++) {
      num += element(i)->report();
   }

   // Back to the start position if no queues

   if (!num) fseek(fpo_, start_pos, SEEK_SET);
}







