//-*-c++-*------------------------------------------------------------
// NAME: Traffic Simulation
// NOTE: Holds queue information from a selected point
// AUTH: Qi Yang
// FILE: TS_Queue.h
// DATE: Wed Oct 23 20:27:27 1996
//--------------------------------------------------------------------

#ifndef TS_QUEUE_HEADER
#define TS_QUEUE_HEADER

class TS_Lane;
class TS_Vehicle;
class TS_QueueHead;

// Queue statistics in a lane

class TS_Queue
{
   public:

      TS_Queue(TS_QueueHead*, TS_Queue*, TS_Lane*);
      ~TS_Queue() { }
  
      void start(unsigned int s) { start_ = s; }
      void update(TS_Vehicle*);
      int isFull();
      int isEmpty();
      inline int nVehicles() { return nVehicles_; }
      inline int nVehiclesAhead() { return nVehiclesAhead_; }

      void report();
  
      float firstMaxSpeed();	// threshold for head
      float lastMaxSpeed();	// threshold for tail
      float maxGap();

   protected:

      TS_QueueHead* head_;	// queue handle
      TS_Lane* lane_;		// container
      float endPos_;		// end position of the queue
      TS_Queue* front_;		// front queue
      int nVehiclesAhead_;	// total num of veh ahead
      int nVehicles_;		// number of vehicles
      unsigned int start_:1;	// 1=queue head
};

#endif
