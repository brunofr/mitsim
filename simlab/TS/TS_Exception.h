//-*-c++-*------------------------------------------------------------
// NAME: Exception handler
// AUTH: Qi Yang
// FILE: TS_Exception.h
// DATE: Sun Mar 17 14:36:47 1996
//--------------------------------------------------------------------

#ifndef TS_EXCEPTION_HEADER
#define TS_EXCEPTION_HEADER

#include <IO/Exception.h>

class TS_Exception : public Exception
{
   public:

      TS_Exception(const char *name) : Exception(name) { }
      ~TS_Exception() { }

      void exit(int code = 0);
      void done(int code = 0);

   private:

      void cleanup();
};

#endif
