//-*-c++-*------------------------------------------------------------
// NAME: Bus sensing parameters table
// AUTH: Qi Yang & Daniel Morgan
// FILE: TS_BusCommPrmTable.C
// DATE: Fri Nov 30 16:30:20 2001
//--------------------------------------------------------------------

#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>

#include "TS_BusCommPrmTable.h"
