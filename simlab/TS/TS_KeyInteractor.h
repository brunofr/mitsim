//-*-c++-*------------------------------------------------------------
// TS_KeyInteractor.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI
#ifndef TS_KEYINTERACTOR_HEADER
#define TS_KEYINTERACTOR_HEADER

#include <DRN/DRN_KeyInteractor.h>

class TS_KeyInteractor : public DRN_KeyInteractor
{
   public:

	  TS_KeyInteractor(XmwDrawingArea* drawing_area) :
		 DRN_KeyInteractor(drawing_area) {
	  }

	  ~TS_KeyInteractor() { }

	  Boolean keyPress(KeySym keysym);
};

#endif
#endif // INTERNAL_GUI
