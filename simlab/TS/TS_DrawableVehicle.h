//-*-c++-*------------------------------------------------------------
// TS_DrawableVehicle.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#ifndef TS_DRAWABLEVEHICLE_HEADER
#define TS_DRAWABLEVEHICLE_HEADER

#include <X11/Intrinsic.h>

#include "TS_Vehicle.h"

const float PERCENT_OF_VEHICLE_WIDTH  =  0.9;

class DRN_DrawingArea;
class TS_Lane;

class TS_DrawableVehicle : public TS_Vehicle
{
protected:

  Pixel central_color_;		/* central color */
  Pixel side_color_;		/* side color */
  Pixel front_color_;		/* front color */
  Pixel back_color_;		/* back color */
  Pixel shade_color_;		/* shade color */

  XPoint points_[4];		/* 4 vertices:
							 * 0 = back_left,  1 = front_left, 
							 * 3 = back_right, 2 = front_right
							 */

  /* status is defined in RN_Constants.h. For drawing purposes we
   * are interested in the status to determine color codes and
   * turn signals.  */

  unsigned int statusShown_; /* when entering draw(), this
							  * is the status currently on
							  * screen */

  unsigned int onScreen_:1;	/* 1 = visible in the window
							 * 0 = not visible */

  float prevPos_;		// previous position
  TS_Lane *prevLane_;	// previous lane

private:

  int calcPoints(DRN_DrawingArea *area);
  Pixel colorByTurnMovement();

  void calcColors(); // will call next four fns
  Pixel sideColor();
  Pixel frontColor();
  Pixel backColor();
  Pixel centralColor();

  Pixel shadeColor();
	
public:

  TS_DrawableVehicle();
  ~TS_DrawableVehicle() { }

  inline int isVisible() { return onScreen_; }

  int draw(DRN_DrawingArea *);
  void erase(DRN_DrawingArea *);
  void drawLargeVehicle(DRN_DrawingArea *);
  void drawSmallVehicle(DRN_DrawingArea *);
  void drawVehicle(DRN_DrawingArea *);

  void resetDrawing() { onScreen_ = 0; }

  void query();
  void trace();
  void highlight(DRN_DrawingArea *);
};

extern int theVehicleShadeParam[];

#endif
#endif // INTERNAL_GUI
