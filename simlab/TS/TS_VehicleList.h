//-*-c++-*------------------------------------------------------------
// NAME: Microcscopic Traffic Simulator
// AUTH: Qi Yang
// FILE: TS_VehicleList.h
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#ifndef TS_VEHICLE_LIST_H
#define TS_VEHICLE_LIST_H

class TS_Vehicle;

class TS_VehicleList
{
  public:

	TS_VehicleList();
	~TS_VehicleList();

	void recycle(TS_Vehicle *); /* put a vehicle into the list */
	TS_Vehicle* recycle();	/* get a vehicle from the list */

	inline TS_Vehicle* head() { return head_; }
	inline TS_Vehicle* tail() { return tail_; }
	inline int nVehicles() { return nVehicles_; }
	inline int nPeakVehicles() { return nPeakVehicles_; }

  private:

	TS_Vehicle *head_;
	TS_Vehicle *tail_;
	int nVehicles_;		/* number of vehicles */
	int nPeakVehicles_;	/* max number of vehicles */
};

extern TS_VehicleList *theVehicleList; /* recyclable vehicles */

#endif // THE END OF THIS FILE
