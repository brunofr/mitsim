//-*-c++-*------------------------------------------------------------
// TS_FileManager.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <cstring>
#include <cstdio>
#include <sstream> //IEM(Jun19)
#include <fstream> //IEM(Jun19)
using namespace std;

#include "TS_FileManager.h"

#include <Tools/ToolKit.h>
#include <Tools/Reader.h>
#include <Tools/SimulationClock.h>
#include <Tools/VariableParser.h>
#include <IO/Exception.h>
#include <IO/MessageTags.h>

#include <GRN/VehicleTable.h>

//Dan - including bus assignment header file
#include <GRN/BusAssignmentTable.h>

#include <GRN/OD_Table.h>
#include <GRN/OD_Cell.h>
#include <GRN/RN_PathTable.h>

// Dan:
#include <GRN/RN_BusRouteTable.h>    
#include <GRN/RN_BusScheduleTable.h> 
#include <GRN/RN_BusRunTable.h>   
#include <GRN/RN_BusStopPrmTable.h>
#include <GRN/RN_BusCommPrmTable.h>

#include <GRN/RN_DynamicRoute.h>
#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>

#ifdef INTERNAL_GUI
#include <Xmt/Xmt.h>
#include <DRN/DRN_MapFeature.h>
#include <DRN/DRN_Segment.h>
#include <DRN/DRN_ViewMarker.h>
#include "TS_Menu.h"
#include "TS_Interface.h"
#include "TS_Symbols.h"
#endif

#include "TS_Engine.h"
#include "TS_Network.h"
#include "TS_Incident.h"
#include "TS_Parameter.h"
#include "TS_MoeCollector.h"
#include "TS_QueueHead.h"
#include "TS_Sensor.h"
#include "TS_Vehicle.h"

TS_FileManager* theFileManager = new TS_FileManager;

extern int loadVehicleShadeParams(GenericVariable &);
extern void logSuspiciousVehicles();

TS_FileManager::TS_FileManager()
  : GenericSwitcher(),
	title_(NULL),
	moeSpecFile_(NULL),
	moeOutputFile_(strdup("moe.out")),
	vehicleFile_(strdup("vehicle_i.out")),
	sensorFile_(strdup("sensor.out")),
	sensorVRCFile_(strdup("vrc.out")),
        assignmentMatrixFile_(strdup("assignment_matrix.out")),  // Angus
        laneChangingFile_(strdup("lane_changing.out")), //tomer for lane changing
        laneUtilitiesFile_(strdup("lane_utilities.out")), // gunwoo - default name of the output file
	segmentTravelTimesFile_(strdup("segtime.out")),
	segmentStatisticsFile_(strdup("segstat.out")),
	linkFlowTravelTimesFile_(strdup("lft_i.out")),
	linkTravelTimesFile_(strdup("ltt_i.out")),
	state3d_(strdup("i3d")),
	depRecordFile_(strdup("dep.out")),
	pathRecordFile_(strdup("path_i.out")),
	trajectoryFile_(strdup("trajectory.out")),
	transitTrajectoryFile_(strdup("transtraj.out")),
	busStopArrivalFile_(strdup("busstop.out")),
	signalPriorityFile_(strdup("priority.out")),
	queueFile_(strdup("queue.out")),
	stateDumpFile_(NULL)
{
}

void TS_FileManager::set(char **ptr, const char *s)
{
  ::Copy(ptr, s);
}


// Read the control variables from master file.

void
TS_FileManager::readMasterFile()
{
  VariableParser vp(this);
  vp.parse(theEngine->master());
}


// Check if two token are equal.

int TS_FileManager::isEqual(const char *s1, const char *s2)
{
  return IsEqual(s1, s2);
}


// This function returns 0 if the variable is parsed or 1 if it is
// skipped (in case command line provides the value), or -1 if error
// occurs.

int TS_FileManager::parseVariable(char ** varptr, GenericVariable &gv)
{
  *varptr = Copy(gv.string());
  return 0;
}

//
// parseVariable() parses master.mitsim
// All the variables provided in the master file should be parsed in
// this function.
//

int
TS_FileManager::parseVariable(GenericVariable &gv) 
{
  char * token = compact(gv.name());

  // [Title]

  if (isEqual(token, "Title")) {
	title_ = Copy(gv.string());

  } else if (isEqual(token, "DefaultParameterDirectory")) {
	ToolKit::paradir(gv.string());
  } else if (isEqual(token, "InputDirectory")) {
	ToolKit::indir(gv.string());
  } else if (isEqual(token, "OutputDirectory")) {
	ToolKit::outdir(gv.string());
  } else if (isEqual(token, "WorkingDirectory")) {
	ToolKit::workdir(gv.string());

  } else if (isEqual(token, "NetworkDatabaseFile")) {
	return parseVariable(RoadNetwork::nameptr(), gv);

  } else if (isEqual(token, "GDSFiles")) {
#ifdef INTERNAL_GUI
	theMapFeatures = new DRN_MapFeature;
	theMapFeatures->load(gv);
#endif
	return 0;

  } else if (isEqual(token, "LinkTravelTimesInputFile")) {
	return RN_DynamicRoute::setFileNamesOfTravelTimeTables(gv);

  } else if (isEqual(token, "IncidentFile")) {
	return parseVariable(TS_Incident::filenameptr(), gv);
  } else if (isEqual(token, "PathTableFile")) {
	return parseVariable(RN_PathTable::nameptr(), gv);


        //Dan - read transit network file

  } else if (isEqual(token, "TransitNetworkFile")) {
	return parseVariable(RN_BusRouteTable::nameptr(), gv);


  } else if (isEqual(token, "VehicleTableFile")) {
	return parseVariable(VehicleTable::nameptr(), gv);


	//Dan - read bus schedule file

  } else if (isEqual(token, "BusScheduleFile")) {
        return parseVariable(RN_BusScheduleTable::nameptr(), gv);

	//Dan - read bus run file

  } else if (isEqual(token, "BusRunFile")) {
        return parseVariable(RN_BusRunTable::nameptr(), gv);

	//Dan - read bus assignment file

  } else if (isEqual(token, "BusAssignmentFile")) {
        return parseVariable(BusAssignmentTable::nameptr(), gv);

  } else if (isEqual(token, "TransitDemandFile")) {
        return parseVariable(RN_BusStopPrmTable::nameptr(), gv);

  } else if (isEqual(token, "BusSurveillanceFile")) {
        return parseVariable(RN_BusCommPrmTable::nameptr(), gv);

  } else if (isEqual(token, "TripTableFile")) {
	return parseVariable(OD_Table::nameptr(), gv);

  } else if (isEqual(token, "StateDumpFile")) {
	return parseVariable(&stateDumpFile_, gv);

  } else if (isEqual(token, "ParameterFile")) {
	return parseVariable(TS_Parameter::nameptr(), gv);

  } else if (isEqual(token, "MOEOutputFile")){
	theMoeCollector = new TS_MoeCollector();
	return parseVariable(&moeOutputFile_, gv);

  } else if (isEqual(token, "VehicleFile")) {
	return parseVariable(&vehicleFile_, gv);

  } else if (isEqual(token, "PointSensorFile")) {
	return parseVariable(&sensorFile_, gv);

  } else if (isEqual(token, "HarmonicSpeed")) {
	TS_Sensor::harmonic(gv.element());

  } else if (isEqual(token, "VRCSensorFile")) {
	return parseVariable(&sensorVRCFile_, gv);

  } else if (isEqual(token, "AssignmentMatrixFile")) {   // Angus
	return parseVariable(&assignmentMatrixFile_, gv);

  } else if (isEqual(token, "LaneChangingFile")) {   // tomer for LC
	return parseVariable(&laneChangingFile_, gv);

 } else if (isEqual(token, "LaneUtilitiesFile")) {   // gunwoo - parsing the master file
	return parseVariable(&laneUtilitiesFile_, gv);

  } else if (isEqual(token, "NetworkStateTag")) {
	return parseVariable(&state3d_, gv);
 
 } else if (isEqual(token, "SegmentStatisticsFile")) {
	return parseVariable(&segmentStatisticsFile_, gv);

  } else if (isEqual(token, "SegmentTravelTimesFile")) {
	return parseVariable(&segmentTravelTimesFile_, gv);

  } else if (isEqual(token, "LinkFlowTravelTimesOutputFile")) {
	return parseVariable(&linkFlowTravelTimesFile_, gv);

  } else if (isEqual(token, "LinkTravelTimesOutputFile")) {
	return parseVariable(&linkTravelTimesFile_, gv);

  } else if (isEqual(token, "VehicleTrajectoryFile")) {
	return parseVariable(&trajectoryFile_, gv);

  } else if (isEqual(token, "TransitTrajectoryFile")) {
	return parseVariable(&transitTrajectoryFile_, gv);

  } else if (isEqual(token, "BusStopArrivalFile")) {
	return parseVariable(&busStopArrivalFile_, gv);

  } else if (isEqual(token, "SignalPriorityFile")) {
	return parseVariable(&signalPriorityFile_, gv);

  } else if (isEqual(token, "VehiclePathRecordFile")) {
	return parseVariable(&pathRecordFile_, gv);

  } else if (isEqual(token, "DepartureRecordFile")) {
	return parseVariable(&depRecordFile_, gv);

  } else if (isEqual(token, "MOESpecificationFile")) {
	return parseVariable(&moeSpecFile_, gv);

  } else if (isEqual(token, "QueueFile")) {
	return parseVariable(&queueFile_, gv);

  } else if (isEqual(token, "SegmentDataSamplingStepSize")) {
	theEngine->segmentStatisticsSamplingStepSize_ = gv.element();

  } else if (isEqual(token, "SegmentDataReportStepSize")) {
	theEngine->segmentStatisticsReportStepSize_ = gv.element();
  }

  else if (isEqual(token, "StartTime")) {
	theSimulationClock->startTime(gv.getTime());
  } else if (isEqual(token, "StopTime")) {
	theSimulationClock->stopTime(gv.getTime());
  } else if (isEqual(token, "StepSize")) {
	theSimulationClock->stepSize(gv.getTime());

  } else if (isEqual(token, "BreakPoints")) {
	return theEngine->parseBreakPoints(gv);

  } else if (isEqual(token, "PointSensorStepSize")) {
	theEngine->survPointStepSize_ = gv.getTime();
	//IEM(Jun15) Added so that vector always has at least the default element
	theEngine->survPointStepSizes_.push_back(gv.getTime());

  //IEM(Apr24) Should be a space-delimited list of numbers, enclosed in brackets
  } else if (isEqual(token, "PointSensorStepSizes")) {
	theEngine->survPointStepSizes(gv);

  //IEM(Jun19) Should be a space-delimited list of 0s and 1s, equal in size to PointSensorStepSizes
  //           Tells whether the user wants an output file for each of the additional sensor types
  } else if (isEqual(token, "SensorOutputFlags")) {
	theEngine->sensorOutputFlags(gv);

  } else if (isEqual(token, "AreaSensorStepSize")) {
	theEngine->survAreaStepSize_ = gv.getTime();

  } else if (isEqual(token, "PathStepSize")) {
	theEngine->pathStepSize_ = gv.getTime();

  } else if (isEqual(token, "SPFlags")) {
	theSpFlag = gv.element();
    
  } else if (isEqual(token, "AnimatiionStepSize")) {
	theEngine->microStepSize_ = gv.getTime();
  } else if (isEqual(token, "SegmentColorStepSize")) {
	theEngine->macroStepSize_ = gv.getTime();
  } else if (isEqual(token, "NetworkStateStepSize")) {
	theEngine->stateStepSize_ = gv.getTime();
  } else if (isEqual(token, "ConsoleMessageStepSize")) {
	theEngine->batchStepSize_ = gv.getTime();

  } else if (isEqual(token, "DepartureRecordStepSize")) {
	theEngine->depRecordStepSize_ = gv.getTime();

  } else if (isEqual(token, "TrajectoryRecordStepSize")) {
	theEngine->trajectoryStepSize_ = gv.getTime();

  } else if (isEqual(token, "TransitTrajectoryRecordStepSize")) {
	theEngine->transitTrajectoryStepSize_ = gv.getTime();

  } else if (isEqual(token, "MOEStepSize")) {
     
	if (theMoeCollector) {
	  theMoeCollector->create((int) gv.getTime());
	}

  } else if (isEqual(token, "MOEODPairs")) {

	if (theMoeCollector) {
	  return theMoeCollector->choosePairs(gv);
	}

  } else if (isEqual(token, "VehicleShadeParams")) {
	return loadVehicleShadeParams(gv);

  } else if (isEqual(token, "Output")) {
	theEngine->chooseOutput(gv.element());

  } else if (isEqual(token, "Signals")) {
#ifdef INTERNAL_GUI
	theDrnSymbols()->signalTypes().set(gv.element());
#endif
	return 0;

  } else if (isEqual(token, "SensorTypes")) {
#ifdef INTERNAL_GUI
	theSymbols()->sensorTypes().set(gv.element());
#endif
	return 0;

  } else if (isEqual(token, "SensorColorCode")) {
#ifdef INTERNAL_GUI
	theSymbols()->sensorColorCode().set(gv.element());
#endif
	return 0;

  } else if (isEqual(token, "Vehicles")) {
#ifdef INTERNAL_GUI
	theSymbols()->vehicleBorderCode().set(gv.element());
#endif
	return 0;

  } else if (isEqual(token, "Segments")) {
#ifdef INTERNAL_GUI
	theSymbols()->segmentColorCode().set(gv.element());
#endif
	return 0;

  } else if (isEqual(token, "ViewMarkers")) {
#ifdef INTERNAL_GUI
	DRN_ViewMarker::LoadViewMarkers(gv) ;
#endif
	return 0;

  } else if (isEqual(token, "Timeout")) {

	// This will overwrite the value parsed from command line

	NO_MSG_WAITING_TIME = gv.element();

  } else if (ToolKit::verbose()) {
	return 1;
  }
  return 0;
}


void
TS_FileManager::openOutputFiles()
{
  const char *fn;
   
  // Check the file name is valid if the output is selected

  if (theEngine->chosenOutput(OUTPUT_LINK_TRAVEL_TIMES) &&
	  linkTravelTimesFile_ == NULL) {
	cerr << "Error:: No output file name for link travel times "
		 << "in master file <"
		 << theEngine->master() << ">." << endl;
	theException->exit();
  }

  if (theEngine->chosenOutput(OUTPUT_SEGMENT_TRAVEL_TIMES) &&
	  segmentTravelTimesFile_ == NULL) {
	cerr << "Error:: No output file name for seggment travel times "
		 << "in master file <"
		 << theEngine->master() << ">." << endl;
	theException->exit();
  }

  /* Open vehicle log file */

  if (theEngine->chosenOutput(OUTPUT_VEHICLE_LOG)) {
	if (vehicleFile_ == NULL) {
	  fn = vehicleFile_ = tmpnam(NULL);
	} else {
	  fn = ToolKit::outfile(vehicleFile_);
	}
	osVehicle_.open(fn);
	if (osVehicle_.good()) {
	  cout << "Vehicle output goes to "
		   << fn << "." << endl;
	} else goto error;
  }

  /* Open surveillance log file */

  //IEM(Jun19) make the vector of additional sensor outfiles the right size:
  //osSensors_.resize(theEngine->nSurvPointStepSizes() - 1);
  for (int oCounter = 0; oCounter < theEngine->nSurvPointStepSizes() - 1; oCounter++) {
    ofstream* newStream = new ofstream;
    osSensorPtrs_.push_back(newStream);
  }

  //IEM(Jun19) If no output flags were given, default to no additional outputs:
  if (theEngine->sensorOutputFlags_.empty())
    theEngine->sensorOutputFlags_.resize(osSensorPtrs_.size(), 0);

  if (theEngine->chosenOutput(OUTPUT_SENSOR_READINGS)) {
	if (sensorFile_ == NULL) {
	  fn = sensorFile_ = tmpnam(NULL);
	} else {
	  fn = ToolKit::outfile(sensorFile_);
	}
	osSensor_.open(fn);
	if (osSensor_.good()) {
	  cout << "Aggregate sensor output goes to "
		   << fn << "." << endl;
	} else goto error;

        //IEM(Jun19) Take care of the additional sensor types
	for (int fCounter = 0; fCounter < osSensorPtrs_.size(); fCounter++) {

	  // Generate a file name for each sensor type by appending the step size:
          std::ostringstream newname;
          newname << sensorFile_;
          newname << theEngine->survPointStepSizes_[fCounter + 1];
	  newname << ends;
	  sensorFiles_.push_back(newname.str());
	   
	  if (theEngine->sensorOutputFlags_[fCounter]) {

   	    if (sensorFile_ == NULL) {
//     fn = sensorFiles_[fCounter] = tmpnam(NULL);
              fn = tmpnam(NULL);
              sensorFiles_[fCounter] = string(fn);
	    } else {
	      fn = ToolKit::outfile(sensorFiles_[fCounter].c_str());
	    }
	    
	    osSensorPtrs_[fCounter]->open(fn);
	    if (osSensorPtrs_[fCounter]->good()) {
	      cout << "Additional aggregate sensor output goes to "
		   << fn << "." << endl;
	    } else goto error;
          }
  	}
  }

  if (theEngine->chosenOutput(OUTPUT_VRC_READINGS)) {
	if (sensorVRCFile_ == NULL) {
	  fn = sensorVRCFile_ = tmpnam(NULL);
	} else {
	  fn = ToolKit::outfile(sensorVRCFile_);
	}
	osVRCSensor_.open(fn);
	if (osVRCSensor_.good()) {
	  cout << "Probe vehicle sensor output goes to "
		   << fn << "." << endl;
	  if (!theEngine->skipComment()) {
		osVRCSensor_
		  << "# Time Sensor Keys"
		  << " ID(" << hex << SENSOR_ICODE(SENSOR_IID) << dec << ")"
		  << " Type(" << hex << SENSOR_ICODE(SENSOR_ITYPE) << dec << ")"
		  << " DepTime(" << hex << SENSOR_ICODE(SENSOR_IDEP) << dec << ")"
		  << " Ori(" << hex << SENSOR_ICODE(SENSOR_IORI) << dec << ")"
		  << " Des(" << hex << SENSOR_ICODE(SENSOR_IDES) << dec << ")"
		  << endl << "#                 "
		  << " Path(" << hex << SENSOR_ICODE(SENSOR_IPATH) << dec << ")"
		  << " Height(" << hex << SENSOR_ICODE(SENSOR_IHEIGHT) << dec << ")"
		  << " Speed(" << hex << SENSOR_ICODE(SENSOR_ISPEED ) << dec << ")"
		  << endl;
	  }
	} else goto error;
  }

  if (theEngine->chosenOutput(OUTPUT_ASSIGNMENT_MATRIX)) {  // Angus
	if (assignmentMatrixFile_ == NULL) {
	  fn = assignmentMatrixFile_ = tmpnam(NULL);
	} else {
	  fn = ToolKit::outfile(assignmentMatrixFile_);
	}
	osAssignmentMatrix_.open(fn);
	if (osAssignmentMatrix_.good()) {
	  cout << "Assignment matrix output goes to "
		   << fn << "." << endl;
	  if (!theEngine->skipComment()) {
		osAssignmentMatrix_
		  << "# Sensor_ID / Time(sec) / Vehicle_ID / "
		  << "Origin / Destination / Departure_Time(sec)"
		  << endl;
	  }
	} else goto error;
  }

  if (theEngine->chosenOutput(OUTPUT_LANE_CHANGING)) {  // tomer for LC
	if (laneChangingFile_ == NULL) {
	  fn = laneChangingFile_ = tmpnam(NULL);
	} else {
	  fn = ToolKit::outfile(laneChangingFile_);
	}
	osLaneChanging_.open(fn);
	if (osLaneChanging_.good()) {
	  cout << "Lane changing output goes to "
		   << fn << "." << endl;
	  if (!theEngine->skipComment()) {
		osLaneChanging_
		  << "# Vehicle_ID / origin/ desitination/ Time(sec) /"
		  << "from lane /to lane / Distance(m)"
		  << endl;
	  }
	} else goto error;
  }

 if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES)) {  // gunwoo - opening the output file and writing headers
	if (laneUtilitiesFile_ == NULL) {
	  fn = laneUtilitiesFile_ = tmpnam(NULL);
	} else {
	  fn = ToolKit::outfile(laneUtilitiesFile_);
	}
	osLaneUtility_.open(fn);
	if (osLaneUtility_.good()) {
	  cout << "Lane utilities output goes to "
		   << fn << "." << endl;
	  if (!theEngine->skipComment()) {
		osLaneUtility_
		  << "lane (1=left, 2=current, 3=left) / Vehicle_ID / Time(sec) / explanatory variables and utility"
		  <<" or: 0 / Vehicle_ID / Time(sec) / choice probabilities (left, current, right)" 
		  << endl;
	  }
	} else goto error;
  }

  
  /* Open segment statistics file */

#ifdef INTERNAL_MOE
  if (theEngine->chosenOutput(OUTPUT_SEGMENT_STATISTICS)) {
	if (segmentStatisticsFile_ == NULL) {
	  fn = segmentStatisticsFile_ = tmpnam(NULL);
	} else {
	  fn = ToolKit::outfile(segmentStatisticsFile_);
	}
	osSegmentStatistics_.open(fn);
	if (osSegmentStatistics_.good()) {
	  cout << "Segment output goes to "
		   << fn << "." << endl;
	} else goto error;
  }
#endif

  if (theEngine->chosenOutput(OUTPUT_VEHICLE_PATH_RECORDS)) {
	if (pathRecordFile_ == NULL) {
	  fn = pathRecordFile_ = tmpnam(NULL);
	} else {
	  fn = ToolKit::outfile(pathRecordFile_);
	}
	osPathRecord_.open(fn);
	if (osPathRecord_.good()) {
	  cout << "Vehicle path record output goes to "
		   << fn << "." << endl;
	} else goto error;
  }

  if (theEngine->chosenOutput(OUTPUT_VEHICLE_DEP_RECORDS)) {
	TS_Vehicle::openDepartureRecords(depRecordFile_);
  }

  if (theMoeCollector && !theMoeCollector->isInitialized()) {
	// Moe step size is not explicitly specified
	theMoeCollector->create(); // use the default step size
  }

  return;

 error:

  cerr << "Error:: Failed opening output file <"
	   << fn << ">." << endl;
  theException->exit();
}

void
TS_FileManager::closeOutputFiles()
{
  if (ToolKit::outdir()) {
	cout << "Output files are in directory <"
		 << ToolKit::outDir() << ">." << endl;
  }

  if (theEngine->chosenOutput(OUTPUT_VEHICLE_LOG)) {
	cout << "Vehicle trip information saved in <"
		 << vehicleFile_ << ">." << endl;
	osVehicle_.close();
  }

  if (theEngine->chosenOutput(OUTPUT_SENSOR_READINGS)) {
	cout << "Aggregate sensor output saved in <"
		 << sensorFile_ << ">." << endl;
	osSensor_.close();

        //IEM(Jun19) Report filenames of additional sensor outputs.
	for(int fCounter = 0; fCounter < osSensorPtrs_.size(); fCounter++) {
   	  cout << "Additional aggregate sensor output saved in <"
		   << sensorFiles_[fCounter] << ">." << endl;
	  osSensorPtrs_[fCounter]->close();
	  delete osSensorPtrs_[fCounter];
        }
  }

  if (theEngine->chosenOutput(OUTPUT_VRC_READINGS)) {
	cout << "Probe vehicle sensor output saved in <"
		 << sensorVRCFile_ << ">." << endl;
	osVRCSensor_.close();
  }

  if (theEngine->chosenOutput(OUTPUT_ASSIGNMENT_MATRIX)) {  // Angus
	cout << "Assignment matrix output saved in <"
		 << assignmentMatrixFile_ << ">." << endl;
	osAssignmentMatrix_.close();
  }

  if (theEngine->chosenOutput(OUTPUT_LANE_CHANGING)) {  // tomer temp for LC
	cout << "Lane changing output saved in <"
		 << laneChangingFile_ << ">." << endl;
	osLaneChanging_.close();
  }

 if (theEngine->chosenOutput(OUTPUT_LANE_UTILITIES)) {  // gunwoo - closing the output file
	cout << "Lane utilities output saved in <"
		 << laneUtilitiesFile_ << ">." << endl;
	osLaneUtility_.close();
  }


  if (theEngine->chosenOutput(OUTPUT_SEGMENT_STATISTICS)) {
	cout << "Segment output saved in <"
		 << segmentStatisticsFile_ << ">." << endl;
	osSegmentStatistics_.close();
  }

  if (theEngine->chosenOutput(OUTPUT_QUEUE_STATISTICS)) {
	cout << "Queue output saved in <"
		 << queueFile_ << ">." << endl;
	TS_QueueHead::close();
  }

  if (theEngine->chosenOutput(OUTPUT_VEHICLE_PATH_RECORDS)) {
	cout << "Vehicle path output saved in <"
		 << pathRecordFile_ << ">." << endl;
	osPathRecord_.close();
  }

  if (!theNetwork) return;

  if (theEngine->chosenOutput(OUTPUT_SEGMENT_TRAVEL_TIMES)) {
	cout << "Writing segment travel times in <"
		 << segmentTravelTimesFile_ << "> ... ";
	cout.flush();
	tsNetwork()->outputSegmentTravelTimes();
	cout << "done." << endl;
  }      

  if (theEngine->chosenOutput(
							  OUTPUT_LINK_TRAVEL_TIMES |
							  OUTPUT_TRAVEL_TIMES_TABLE)) {
	tsNetwork()->recordLinkTravelTimeOfActiveVehicle();
  }

  if (theEngine->chosenOutput(OUTPUT_LINK_TRAVEL_TIMES)) {
	const char *filename = ToolKit::outfile(linkFlowTravelTimesFile_);
	cout << "Writing link travel times in <"
		 << filename << ">." << endl;
	tsNetwork()->outputLinkTravelTimes(filename);
  }
      
  if (theEngine->chosenOutput(OUTPUT_TRAVEL_TIMES_TABLE)) {
	const char *filename = ToolKit::outfile(linkTravelTimesFile_);
	cout << "Writing link travel times in <"
		 << filename << ">." << endl;
	tsNetwork()->outputReloadableLinkTravelTimes(filename);
  }

  if (theMoeCollector) {
	const char *filename = ToolKit::outfile(moeOutputFile_);
	cout << "Writing moe data in <"
		 << filename << ">." << endl;
	tsNetwork()->collectMoeForUnfinishedTrips();
	theMoeCollector->output(filename);
  }

  if (theEngine->chosenOutput(OUTPUT_VEHICLE_DEP_RECORDS)) {
	TS_Vehicle::closeDepartureRecords();
  }

  // Write vehicles waiting too long into the log file

  if (ToolKit::verbose()) {
	logSuspiciousVehicles();
  }
}
