//-*-c++-*------------------------------------------------------------
// NAME: Traffic Simulation
// NOTE: 
// AUTH: Qi Yang
// FILE: TS_Incident.C
// DATE: Mon Dec 16 11:44:19 1996
//--------------------------------------------------------------------

#include <Tools/Reader.h>
#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>

#include <GRN/RN_CtrlStation.h>

#include "TS_Incident.h"

#include "TS_Network.h"
#include "TS_Node.h"
#include "TS_Link.h"
#include "TS_Segment.h"
#include "TS_Lane.h"
#include "TS_Vehicle.h"
#include "TS_Communicator.h"
#include "TS_Exception.h"

#include "TS_Parameter.h"
#include "TS_Engine.h"

#ifdef INTERNAL_GUI
#include <DRN/DRN_DrawingArea.h>
#endif

int TS_Incident::startIndexAsSignals_ = -1;
char * TS_Incident::filename_ = NULL;

int TS_Incident::readIncidents(char * filename)
{
  // Incidents are treated as signals and stored at the end of the
  // signal list.
   
  startIndexAsSignals_ = theNetwork->nSignals();
   
  if (!ToolKit::isValidInputFilename(filename)) {
	cout << "No incident." << endl;
	return 0;
  }

  Reader is(ToolKit::infile(filename));

  cout << endl << "Loading incidents <"
	   << is.name() << "> ..." << endl;

  int n = 0, num = 0, seg = 0;
  float vis, pos;

  for (is.eatwhite(); !is.eof() && is.good(); is.eatwhite()) {

	if (!is.findToken(OPEN_TOKEN)) theException->exit(1);

	// Read station wide info

	is >> vis >> seg >> pos;

	RN_CtrlStation *ctrl = theNetwork->newCtrlStation();
	ctrl->init(CTRL_INCIDENT, vis, seg, pos);

	int i, nls = ctrl->nLanes();
	TS_Incident *inc = new TS_Incident[nls];
	for (i = 0; i < nls; i ++) {
	  inc[i].set(ctrl, ctrl->segment()->lane(i));
	  ctrl->signal(i, inc + i);
	}

	// Read incidents

	for (is.eatwhite();
		 !is.eof() && is.good() && is.peek() != CLOSE_TOKEN;
		 is.eatwhite()) {

	  if (!is.findToken(OPEN_TOKEN)) theException->exit(1);

	  int flag, laneno, id;
	  float sev;
	  double st, du, spd;
		
	  is >> flag >> sev;
	  st = is.gettime();
	  du = is.gettime();
	  is >> spd >> laneno >> id;

	  RN_Lane *pl = theNetwork->findLane(laneno);
	  if (pl && pl->segment() == ctrl->segment()) {
		i = pl->localIndex();
		inc[i].lane_ = pl;
	  } else {
		cerr << "Error:: Bad lane ID <" << laneno << ">. ";
		is.reference();
		theException->exit(1);
	  }

	  inc[i].code_ = id;
	  inc[i].state_ = flag;
	  inc[i].severity_   = sev;
	  inc[i].startTime_  = st;
	  inc[i].duration_   = du;
	  inc[i].speedLimit_ = spd * theParameter->speedFactor();

	  if (inc[i].speedLimit_ < SPEED_EPSILON) {
		inc[i].state_ |= INCI_STOP;
	  }

#ifdef INTERNAL_GUI
	  inc[i].calcGeometricData();
#endif

	  inc[i].addIntoNetwork();
	  tsNetwork()->incidents().push_back(inc+i);

	  n ++;

	  if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
	}

	num ++;

	if (!is.findToken(CLOSE_TOKEN)) theException->exit(1);
  }

  cout << indent << "Parsed " << num << " incidents. "
	   << n << " lanes affected." << endl;
   
  return 0;
}

void
TS_Incident::print(ostream &os)
{
  os << indent << indent
	 << OPEN_TOKEN << endc
	 << (state_ & 0xF) << endc
	 << severity_ << endc
	 << startTime_ << endc
	 << duration_ << endc
	 << (speedLimit_ / theParameter->speedFactor()) << endc
	 << lane_->code() << endc
	 << code_ << endc
	 << CLOSE_TOKEN << endl;
}


/*
 * Check incident time settings and update its state.
 */

void
TS_Incident::check()
{
  // The incident clean up time can be changed here by modifying
  // variable 'duration_'.

  if (state_ & INCI_ACTIVE) {

	// This incident is currently active. Check if it is the time to
	// clean it up.

	if (theSimulationClock->currentTime() > startTime_ + duration_) {
	  remove();
	  send(MSG_TYPE_INCI);	// send a clearance msg
	}

  } else {

	// This incident is currently inactive.  Check if it is the
	// time to make it active.

	if (theSimulationClock->currentTime() >= startTime_) {

	  // make this incident active.

	  state_ |= INCI_ACTIVE;

#ifdef INTERNAL_GUI
	  draw(theDrawingArea);
#endif // !INTERNAL_GUI

	  send(MSG_TYPE_INCI);	// send occurance msg
	}
  }
}


/*
 * Clear the incident
 */

void TS_Incident::remove()
{
  // mark this incident as inactive

  state_ &= (~INCI_ACTIVE);
  state_ |= INCI_REMOVED;

#ifdef INTERNAL_GUI
  draw(theDrawingArea);	// erase
#endif // !INTERNAL_GUI

  startTime_ = ONE_DAY;
}


/*
 * Acceleration rate in response of an incident.
 */

float
TS_Incident::acceleration(TS_Vehicle* pv, float dist)
{
  float acc;
  if (state_ & INCI_ACTIVE &&
	  dist <= pv->distanceToNormalStop()) {

	if (dist < pv->length() &&
		pv->flag(FLAG_NOSING) &&
		pv->flag(FLAG_NOSING_FEASIBLE)) {

	  // A quick fix to the gridlock caused by conflict between
	  // incident and yeilding.

	  acc = MAX_ACCELERATION;
	} else {
	  acc = pv->brakeToTargetSpeed(dist, speedLimit_);
	}

  } else {
	acc = MAX_ACCELERATION;
  }
  return (acc);
}


/*
 * Send incident occurrance or clearance message
 */


void
TS_Incident::send(unsigned int msg_type)
{
  TS_Communicator *communicator = (TS_Communicator*) theCommunicator;

  if (!communicator->isTmsConnected()) return;

  communicator->tms() << (msg_type | index_) ;
  send(communicator->tms());
}


void
TS_Incident::send(IOService &tms)
{
  tms << state_
	  << severity_
	  << startTime_ 
	  << duration_
	  << lane_->code()
	  << distance();
  tms.flush(SEND_IMMEDIATELY);
}


void
TS_Incident::receive(IOService &tms)
{
  tms >> state_;
  if (state_ & INCI_REMOVED) {
	remove();
  }
}


/*
 * Check if any incident in the network becomes active or cleaned.
 */

void
TS_Incident::checkIncidents()
{
  list<TS_Incident*> &lst = tsNetwork()->incidents();
  list<TS_Incident*>::iterator c;
  for (c = lst.begin(); c != lst.end(); c ++) {
	(*c)->check();
  }
}


void TS_Incident::set(RN_CtrlStation* s, RN_Lane *pl)
{
  static int id = 0;
  id ++;
  code_ = -id;
  state_ = 0;
  station_ = s;
  severity_   = 0.0;
  startTime_  = theSimulationClock->currentTime();
  duration_   = 300;
  speedLimit_ = 0 ;

  lane_ = pl;
}

#ifdef INTERNAL_GUI

// This function overload the function defined in DRN_Signal

Pixel *
TS_Incident::calcColor(unsigned int s)
{
  static Pixel clr[2];

  if (s & INCI_REMOVED) {		// removed
	clr[0] = clr[1] = theColorTable->inciAfter();
  } else if (s & INCI_ACTIVE) { // active
	if (speedLimit_ < SPEED_EPSILON) {
	  clr[0] = theColorTable->lightRed();
	  clr[1] = theColorTable->red();
	} else {
	  clr[0] = theColorTable->darkGray();
	  clr[1] = theColorTable->black();
	}
  } else {						// will active
	clr[0] = clr[1] = theColorTable->inciBefore();
  }
  return clr;
}

#endif
