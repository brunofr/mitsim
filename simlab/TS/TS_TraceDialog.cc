//-*-c++-*------------------------------------------------------------
// TS_TraceDialog.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <iostream>
using namespace std;

#include <Xmt/Xmt.h>
#include <Xmt/Dialogs.h>
#include <Xbae/Matrix.h>

#include <Tools/SimulationClock.h>
#include <UTL/Misc.h>

#include "TS_TraceDialog.h"
#include "TS_Interface.h"
#include "TS_Node.h"
#include "TS_Link.h"
#include "TS_Segment.h"
#include "TS_Lane.h"
#include "TS_DrawingArea.h"
#include "TS_Network.h"
#include "TS_Parameter.h"
#include "TS_DrawableVehicle.h"


TS_TraceDialog *TS_TraceDialog::instance_ = 0;

TS_TraceDialog *TS_TraceDialog::the()
{
  if (instance_) return instance_;
  return (instance_ = new TS_TraceDialog(theInterface->widget()));
}

TS_TraceDialog::TS_TraceDialog(Widget parent)
  : XmwDialogManager(parent, "traceDialog", NULL, 0, "apply", "cancel"),
	vehicle_(0)
{
  info_ = XmtNameToWidget(widget_, "info");
  assert(info_);

  addCallback(info_, XmNenterCellCallback, &TS_TraceDialog::enter, NULL);
//   addCallback(info_, XmNleaveCellCallback, &TS_TraceDialog::leave, NULL);

  theInterface->showDefaultCursor();
}

const char * TS_TraceDialog::getValue(int i)
{
  return XbaeMatrixGetCell(info_, i, 0);
}

void TS_TraceDialog::setValue(int i, const char *value)
{
  const char *current = getValue(i);
  if (strcmp(current, value)) {
	XbaeMatrixSetCell(info_, i, 0, String(value));
  }
}

TS_TraceDialog::~TS_TraceDialog()
{
}


void TS_TraceDialog::enter( Widget, XtPointer, XtPointer data )
{
  if (!vehicle_) return;

  XbaeMatrixEnterCellCallbackStruct *p;
  p = (XbaeMatrixEnterCellCallbackStruct *) data;
  switch (p->row) {
  case 2:						// drive group
  case 8:						// desired speed
	p->select_text = True;
	break;
  default:
	p->select_text = false;
	break;
  }
}

// void TS_TraceDialog::leave( Widget, XtPointer, XtPointer data )
// {
//   if (!vehicle_) return;
//   XbaeMatrixEnterCellCallbackStruct *p;
//   p = (XbaeMatrixEnterCellCallbackStruct *) data;
// }

// These overload the functions in base class

void TS_TraceDialog::cancel(Widget, XtPointer, XtPointer)
{
  XbaeMatrixCommitEdit(info_, False);
  unmanage();
  if (vehicle_) {
	vehicle_->highlight(theDrawingArea); // erase
	vehicle_->unsetFlag(FLAG_SHINE);
    vehicle_->unsetFlag(FLAG_DESIRED_SPEED) ;
	vehicle_->draw(theDrawingArea);
	vehicle_ = 0;
  }
  theInterface->handlePendingEvents();
}

void TS_TraceDialog::okay(Widget, XtPointer, XtPointer)
{
  XbaeMatrixCommitEdit(info_, False);
  if (vehicle_) {
	const char* s = getValue(2) ;
	unsigned int v = strtoul(s, 0, 16) ;
    float spd = atof(getValue(8)) * theParameter->speedFactor();
	vehicle_->setDriverGroup(v) ;
    if (!AproxEqual(vehicle_->desiredSpeed_, spd)) {
        vehicle_->desiredSpeed_ = spd ;
        vehicle_->setFlag(FLAG_DESIRED_SPEED) ;
    }
    vehicle_->cfTimer(0.0) ;
	vehicle_->draw(theDrawingArea);
  }
  theInterface->handlePendingEvents();
}

void TS_TraceDialog::help(Widget, XtPointer, XtPointer)
{
  theInterface->openUrl("trace", "mitsim.html");
}


void TS_TraceDialog::post_info(TS_DrawableVehicle *pv)
{
  if (vehicle_ != pv) {
	if (vehicle_) { // erase
	  vehicle_->unsetFlag(FLAG_SHINE);
	  vehicle_->highlight(theDrawingArea);
	}
	pv->setFlag(FLAG_SHINE);
	pv->highlight(theDrawingArea);
  }
  update(pv);
  manage(widget_);
}

void TS_TraceDialog::update(TS_DrawableVehicle *pv)
{
  if (pv) {						// update
	SimulationClock *clk = theSimulationClock;
	TS_Parameter *p = theParameter;
	float cur_spd = pv->currentSpeed_ / p->speedFactor();
	float des_spd = pv->desiredSpeed_ / p->speedFactor();
	float dis = pv->distance_/p->lengthFactor();
	if (pv != vehicle_) {		// new vehicle, static info
	  setValue(0, Str("%d", pv->code_));
	  setValue(1, Str("%d", pv->type_));
	  setValue(2, Str("%8.8x", pv->getDriverGroup()));
	  setValue(3, clk->convertTime(pv->departTime_));
	  setValue(4, Str("%d", pv->oriNode()->code()));
	  setValue(5, Str("%d", pv->desNode()->code()));
	}
	setValue(6, Str("%d / %.1f", pv->lane()->code(), dis));
	setValue(7, Str("%.0f", cur_spd));
	setValue(8, Str("%.0f", des_spd));
	setValue(9, clk->convertTime(clk->currentTime()-pv->departTime_));
  } else {						// clean
	for (int i = 0; i < 10; i ++) {
	  setValue(i, "");
	} 
  }
  vehicle_ = pv;
}

#endif
