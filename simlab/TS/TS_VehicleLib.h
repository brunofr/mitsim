//-*-c++-*------------------------------------------------------------
// NAME: Microcscopic Traffic Simulator
// AUTH: Qi Yang
// FILE: TS_VehicleLib.h
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#ifndef TS_VEHICLE_LIB_HEADER
#define TS_VEHICLE_LIB_HEADER
#include <iostream>
using namespace std;

class TS_Parameter;
class TS_Vehicle;
class Scaler;
class ArrayElement;

class TS_VehicleLib
{
   public:

      TS_VehicleLib();
      ~TS_VehicleLib();

      inline char *name() { return name_; }
      inline float length() { return length_; }
      inline float width() { return width_; }
      inline float tollBoothDelayFactor() {
	 return tollBoothDelayFactor_;
      }
      inline float etcRate() { return etcRate_; }
      inline float overHeightRate() { return overHeightRate_; }
      inline float hovRate() { return hovRate_; }

      float maxAcceleration(TS_Vehicle*);
      float maxDeceleration(TS_Vehicle*);
      float normalDeceleration(TS_Vehicle*);
      float maxLaneSpeed(TS_Vehicle*);
      float maxSegmentSpeed(TS_Vehicle*);
      float impatient();

      int nGradeGroups();
      Scaler& gradeScaler();

      int nSpeedGroups();
      Scaler& speedScaler();

      void createLib(TS_Parameter&);
      void loadAttributes(ArrayElement *);
      void loadMaximumAcceleration(ArrayElement *);
      void loadMaximumDeceleration(ArrayElement *);
      void loadNormalDeceleration(ArrayElement *);
      void loadMaxSpeed(ArrayElement *);

      void print(std::ostream &os = cout);

   protected:

      char *name_;		// name of the vehicle type
      float length_;		// vehicle length
      float width_;		// vehicle width
      float percent_;

      // An array: [FacilityType][Speed]

      float *acc_;		// maximum acceleration rates 

      // An array: [Speed]

      float *dec_;		// normal deceleration rates

      float *maxDec_;		// maximum deceleration rates

      float *maxSpeed_;	// max speed by grade (%)

      // delay factor at toll booth

      float tollBoothDelayFactor_;

      // Probabilities for vehicle type.

      float etcRate_;		// probability with ETC device
      float overHeightRate_;	// probability with over height
      float hovRate_;		// probability to be HOV

};

#endif
