//-*-c++-*------------------------------------------------------------
// NAME: Microscopic Traffic Simulator
// NOTE: 
// AUTH: Qi Yang
// FILE: TS_Link.C
// DATE: Fri May 10 10:37:21 1996
//--------------------------------------------------------------------

#include <cstdio>
#include <iostream>
#include <new>
using namespace std;

#include <Tools/ToolKit.h>
#include <Tools/SimulationClock.h>

#include <Tools/Math.h>

#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>

#include "TS_Network.h"
#include "TS_Node.h"
#include "TS_Link.h"
#include "TS_Segment.h"
#include "TS_Lane.h"
#include "TS_Vehicle.h"
#include "TS_Status.h"
#include "TS_Parameter.h"
#include "TS_Engine.h"


TS_Link::TS_Link()
   : RN_Link(),	
     queueHead_(NULL),
     queueTail_(NULL),
     queueLength_(0)
{
#ifdef COUNT_DLC
      num_dlc = 0 ;
#endif
}


/*
 *-------------------------------------------------------------------
 * Mark a downstream link as connected if there is at least one
 * connection between the lanes in this link to the lanes in the
 * downstream link.
 *-------------------------------------------------------------------
 */

void
TS_Link::checkConnectivity()
{
   TS_Lane *plane;
   RN_Segment *psegment = endSegment();
	
   /* Set lane changing information */

   while (psegment)
   {
      plane = (TS_Lane*)psegment->rightLane();
      while (plane)
      {
		 plane->createLaneUseRules();
		 plane->createNumberOfChanges2OutArcs();
		 plane = plane->left();
      }
      psegment = psegment->upstream();
   }
}


/*
 * add a vehicle to the end of the queue.
 */

void
TS_Link::queue(TS_Vehicle* vehicle)
{
   if (queueTail_ != NULL) {	/* current queue is not empty */
      queueTail_->trailing(vehicle);
      vehicle->leading(queueTail_);
      queueTail_ = vehicle;
   } else {			/* current queue is empty */
      vehicle->leading(NULL);
      queueHead_ = queueTail_ = vehicle;
   }
   vehicle->trailing(NULL);
   queueLength_ ++;
   ::theStatus->nInQueue(1);
}


/*
 * remove vehicle "pv" from the queue.
 */

void
TS_Link::dequeue(TS_Vehicle* pv)
{
   if (pv->leading()) {
      pv->leading()->trailing(pv->trailing());
   } else {			/* first vehicle in the queue */
      queueHead_ = pv->trailing();
   }
   if (pv->trailing()) {
      pv->trailing()->leading(pv->leading());
   } else {			/* last vehicle in the queue */
      queueTail_ = pv->leading();
   }
   queueLength_ --;
   ::theStatus->nInQueue(-1);
}


/* 
 * Print the number of vehicles queue for enter a link
 */

int
TS_Link::reportQueueLength(ostream &os)
{
   const int MinQueueLength = 20;
   if (queueLength_ > MinQueueLength) {
      os << " (" << code_ << ":"
		 << queueLength_ << ")";
      return 1;
   }
   return 0;
}


// Expected travel time for the vehicles that are currently in the
// link.

float
TS_Link::calcTravelTime()
{
   RN_Segment *ps = startSegment();
   TS_Vehicle *pv;
   double sum = 0.0;
   int cnt = 0;
   float tt = travelTime();

   // Vehicles already on the link

   while (ps) {
      pv = ((TS_Segment *)ps)->firstVehicle();
      while (pv) {
		 float pos = pv->distanceFromDownNode();
		 sum += pv->timeInLink() + pos / length() * tt;
		 cnt ++;
		 pv = pv->macroTrailing();
      }
      ps = ps->downstream();
   }

   // Vehicles in pretrip queue

   pv = queueHead_;
   while (pv) {
      sum += pv->timeInLink() + tt * 1.25;
      cnt ++;
      pv = pv->trailing();
   }

   if (cnt) {
	  return sum / cnt;
   } else {
	  return tt;
   }
}


void TS_Link::vehicleUpdateNextCtrl(RN_CtrlStation *ctrl)
{
   RN_Segment *ps = startSegment();
   TS_Vehicle *pv;
   float dis = ctrl->distance();
   while (ps) {
      pv = ((TS_Segment *)ps)->firstVehicle();
      while (pv) {
		 if (pv->distanceFromDownNode() > dis) { // upstream vehicles
			pv->nextCtrl(ctrlList());
			pv->updateNextCtrl();
		 } else {
			return;				// downstream of the device
		 }
		 pv = pv->macroTrailing();
      }
      ps = ps->downstream();
   }
}
