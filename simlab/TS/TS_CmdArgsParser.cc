//-*-c++-*------------------------------------------------------------
// TS_CmdArgsParser.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <iomanip>
using namespace std;

#include <GRN/RN_Route.h>
#include <Tools/ToolKit.h>

#include "TS_CmdArgsParser.h"
#include "TS_Engine.h"

TS_CmdArgsParser::TS_CmdArgsParser()
  : CmdArgsParser()
{
  add(new Clo("-output", &theEngine->chosenOutput_, 0,
			  "Output mask"));

  char *doc;
  doc = StrCopy("Routing options for guided drivers:\n"
				"%6X = Time variant\n"
				"%6X = Update shortest path tree\n"
				"%6X = Update path table info\n"
				"%6X = Use existing tables if available\n"
				"%6X = Updated travel time available for pretrip planning",
				INFO_FLAG_DYNAMIC,
				INFO_FLAG_UPDATE_TREES,
				INFO_FLAG_UPDATE_PATHS,
				INFO_FLAG_USE_EXISTING_TABLES,
				INFO_FLAG_AVAILABILITY);
  
  add(new Clo("-sp", &theSpFlag, 0, doc));
}
