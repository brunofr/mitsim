//-*-c++-*------------------------------------------------------------
// NAME: Traffic Simulation
// NOTE: 
// AUTH: Qi Yang
// FILE: TS_OtherModels.C
// DATE: Wed Oct 16 18:08:21 1996
//--------------------------------------------------------------------

#include <Tools/Math.h>

#include <Tools/ToolKit.h>
#include <Tools/Random.h>

#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>

#include "TS_Parameter.h"
#include "TS_Segment.h"

#if defined(INTERNAL_GUI)
#include "TS_TraceDialog.h"
#include "TS_DrawableVehicle.h"
#else
#include "TS_Vehicle.h"
#endif

/*
 *-------------------------------------------------------------------
 * Find a headway buffer for a vehicle.  This is a behavior parameter
 * that describe how aggressive for a driver to accept a headway
 * gap in lane changing, merging, and car-following.  The value
 * return by this function will be added to the minimum headway gaps
 * for the population, which are constants provided in parameter
 * file.
 *
 * The returned value is in seconds.
 *-------------------------------------------------------------------
 */



float
TS_Vehicle::headwayBuffer()
{
   TS_Parameter& p = *theParameter;
   return theRandomizer->urandom(p.hBufferLower(), p.hBufferUpper());
}

/*
 *-------------------------------------------------------------------
 * Desired speed is a function of free flow speed, speed
 * limit sign, and driver's behavior.
 *
 * This function assumes that the data member TS_Vehicle::signedSpeed_ 
 * has been set.
 *-------------------------------------------------------------------
 */

void
TS_Vehicle::calcDesiredSpeed(unsigned int speed_on_sign)
{
  if (speed_on_sign) {
    signedSpeed_ = speed_on_sign;
  } else {
    signedSpeed_ = segment()->speedLimit();
  }

#if defined(INTERNAL_GUI)
  // We do not auto update the speed if the vehicle is traced and
  // assigned a speed manually 
  TS_Vehicle* pv = TS_TraceDialog::the()->vehicle() ;
  if (pv == this && flag(FLAG_DESIRED_SPEED)) return ;
#endif  

  float desired = theParameter->speedFactor() * signedSpeed_ ;
  desired = desired * (1+theParameter->spdLmtAddOn(driverGroup.spdLmtAddOn));

  desiredSpeed_ = Min(desired, maxSegmentSpeed());
}


/*
 *-------------------------------------------------------------------
 * Returns a startup delay. This function is used to impose a delay
 * for stopped vehicle after the leading vehicle has moved or
 * traffic light has changed to green.
 *-------------------------------------------------------------------
 */

float
TS_Vehicle::calcStartupDelay()
{
   return theParameter->startupDelay(positionInQueue_);
}



/*
 *----------------------------------------------------------------------
 * Antiicpation time of the driver : for courtesy merging 
 * cfc -06 
 *-----------------------------------------------------------------------
*/

//float
//TS_Vehicle::anticipationTime()
//{
//   TS_Parameter& p = *theParameter;
//   return theRandomizer->urandom(p.anticipationTimeLower(), p.anticipationTimeUpper());
//}

