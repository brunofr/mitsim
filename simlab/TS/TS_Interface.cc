//-*-c++-*------------------------------------------------------------
// TS_Interface.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class implementation for the class
// TS_Interface
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#include <iostream>
#include <cstdlib>

#include <Xmt/Xmt.h>
#include <Xmt/Create.h>

#include <Xmw/XmwImage.h>
#include <Xmw/XmwColor.h>

#include "TS_Symbols.h"
#include "TS_Interface.h"
#include "TS_DrawingArea.h"
#include "TS_Menu.h"
#include "TS_Modeline.h"
#include "TS_Icon.cc"

TS_Interface* theInterface = NULL;

TS_Interface::TS_Interface(int *argc, char **argv)
   : DRN_Interface(argc, argv,
				   new XmwImage("mitsim", icon_bits, NULL,
								icon_width, icon_height))
{
   extern void CreateHelpMsg() ;
   CreateHelpMsg() ;
   theInterface = this;
   create();
}

TS_Interface::~TS_Interface()
{
}

void TS_Interface::create()		// virtual
{
   DRN_Interface::create();
   manage ();
}

XmwSymbols* TS_Interface::createSymbols()
{
   return new TS_Symbols;
}

XmwMenu* TS_Interface::menu(Widget parent)
{
   return new TS_Menu(parent);
}

XmwModeline* TS_Interface::modeline(Widget parent)
{
   return new TS_Modeline(parent);
}

DRN_DrawingArea* TS_Interface::drawingArea(Widget w)
{
   return new TS_DrawingArea(w);
}

#endif // INTERNAL_GUI
