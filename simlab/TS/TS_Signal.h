//-*-c++-*------------------------------------------------------------
// NAME: Traffic Simulation
// AUTH: Qi Yang
// FILE: TS_Signal.h
// DATE: Sat Aug 31 16:00:27 1996
//--------------------------------------------------------------------

#ifndef TS_SIGNAL_HEADER
#define TS_SIGNAL_HEADER

#include <TC/TC_Signal.h>

class TS_Vehicle;

class TS_Signal : public TC_Signal
{
   public:

      TS_Signal();
      virtual ~TS_Signal();

      virtual float acceleration(TS_Vehicle *, float);
};

#endif
