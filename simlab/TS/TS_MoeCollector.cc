//-*-c++-*------------------------------------------------------------
// NAME: This generates the moe needed in my thesis
// AUTH: Qi Yang
// FILE: TS_MoeCollector.C
// DATE: Fri Jun 14 13:27:17 1996
//--------------------------------------------------------------------

#include <Tools/SimulationClock.h>
#include <Tools/SimulationEngine.h>
#include <Tools/ToolKit.h>
#include <Tools/GenericVariable.h>

#include "TS_Network.h"
#include "TS_MoeCollector.h"
#include "TS_Moe.h"
#include "TS_Vehicle.h"
#include "TS_Node.h"

TS_MoeCollector * theMoeCollector = NULL;

TS_MoeCollector::TS_MoeCollector()
  : nIntervals_(0),
	stepSize_(86400),
	nComplete_(0),
	nInComplete_(0)
{
   for (register int i = 0; i < 2; i ++) {
      for (int j = 0; j < 2; j ++) {
		 arrivals_[i][j] = NULL;
		 finished_[i][j] = NULL;
		 chosenPair_[i][j] = NULL;
      }
   }
}

TS_MoeCollector::~TS_MoeCollector()
{
   remove();
}


void TS_MoeCollector::remove()
{
   for (register int i = 0; i < 2; i ++) {
      for (int j = 0; j < 2; j ++) {
		 delete [] arrivals_[i][j];
		 delete [] finished_[i][j];
		 delete [] chosenPair_[i][j];
		 arrivals_[i][j] = NULL;
		 finished_[i][j] = NULL;
		 chosenPair_[i][j] = NULL;
      }
   }
}


void TS_MoeCollector::create(int step)
{
   stepSize_ = step;

   SimulationClock *clk = theSimulationClock;
   long int len = (long int) (clk->stopTime() - clk->startTime());

   nIntervals_ = len / stepSize_ + 1;

   for (register int i = 0; i < 2; i ++) {
      for (register int j = 0; j < 2; j ++) {
		 arrivals_[i][j] = new int[nIntervals_];
		 finished_[i][j] = new TS_Moe[nIntervals_];
		 chosenPair_[i][j] = new TS_Moe[nIntervals_];
		 for (register int k = 0; k < nIntervals_; k ++) {
			arrivals_[i][j][k] = 0;
		 }
      }
   }

   if (ToolKit::verbose()) {
	  cout << "Collecting internal MOE for "
		   << nIntervals_ << " intervals, "
		   << stepSize_ << " seconds in each interval"
		   << endl;
   }
}


int TS_MoeCollector::choosePairs(GenericVariable &gv)
{
   register int i, n = gv.nElements();
   for (i = 0; i < n; i ++) {
      if (gv.element(i).type() != ArrayElement::PAIR) {
		 return -1;
      }
      PairElement *p = gv.pair(i);
      tmp.push_back(p->o());
      tmp.push_back(p->d());
   }
   return 0;
}


void TS_MoeCollector::findSelectedPairs()
{
   register int i, k = 0, n = tmp.size() / 2;
   RN_Node *o, *d;
   int ori, des;
   for (i = 0; i < n; i ++) {
      ori = tmp[k++];
      des = tmp[k++];
      o = theNetwork->findNode(ori);
      d = theNetwork->findNode(des);
      if (!o || !d) {
		 cerr << "Warning:: Moe OD pair ("
			  << ori << "->"
			  << des << ") skipped." << endl;
      } else {
		 selectedPairs_.insert(PtrOD_Pair(new OD_Pair(o, d)));
      }
   }
   tmp.erase(tmp.begin(), tmp.end());
}


bool TS_MoeCollector::isPairSelected(OD_Pair *p)
{
   if (selectedPairs_.find(PtrOD_Pair(p)) != selectedPairs_.end()) {
      return true;
   } else {
      return false;
   }
}


int TS_MoeCollector::printSelectedPairs(FILE *fpo)
{
   OdPairSetType::iterator i;
   for (i = selectedPairs_.begin(); i != selectedPairs_.end(); i ++) {
      fprintf(fpo, " %d-%d",
			  (*i)->oriNode()->code(),
			  (*i)->desNode()->code());
   }
   return selectedPairs_.size();
}

void TS_MoeCollector::printSelectedPairs(ostream &os)
{
   OdPairSetType::iterator i;
   for (i = selectedPairs_.begin(); i != selectedPairs_.end(); i ++) {
	  os << " " << (*i)->oriNode()->code()
		 << "-" << (*i)->desNode()->code();
   }
}


void TS_MoeCollector::recordFinished(TS_Vehicle *pv)
{
   SimulationClock *clk = theSimulationClock;
   long int clen = (long int) (clk->currentTime() - clk->startTime());
   long int dlen = (long int) (pv->departTime() - clk->startTime());
   int ci = clen / stepSize_;	// current time interval
   int di = dlen / stepSize_;	// depart time interval
   int i = pv->infoType();	// unguided=0 guided=1

   arrivals_[0][i][ci] ++;		// number arrived in interval ci

   finished_[0][i][di].record(pv);	// departed in interval di

   if (isPairSelected(pv->od())) {
      chosenPair_[0][i][di].record(pv);
   }
   nComplete_ ++;
}


//
// for now, this function is the same as recordFinish 
// (except it stores info in j=0)  In the future the tt
// stored for unFinished vehs may change to an expected tt.
//

void TS_MoeCollector::recordUnFinished(TS_Vehicle *pv)
{
   SimulationClock *clk = theSimulationClock;
   long int clen = (long int) (clk->currentTime() - clk->startTime());
   long int dlen = (long int) (pv->departTime() - clk->startTime());
   int ci = clen / stepSize_;	// current time interval
   int di = dlen / stepSize_;	// depart time interval
   int i = pv->infoType();	// unguided=0 guided=1

   arrivals_[1][i][ci] ++;		// number arrived in interval ci

   finished_[1][i][di].record(pv);	// departed in interval di

   if (isPairSelected(pv->od())) {
      chosenPair_[1][i][di].record(pv);
   }

   nInComplete_ ++;
}


void TS_MoeCollector::output(const char *filename)
{
   FILE *fpo = fopen(filename, "w");

   if (fpo == NULL) {
      cerr << "Error:: Cannot open output file <"
		   << filename << ">." << endl;
      return;
   }
   
   SimulationClock *clk = theSimulationClock;

   if (!theSimulationEngine->isRectangleFormat()) {
      fprintf(fpo, "%% %d MINUTES AGGREGATED MOE DATA (%s-%s)\n",
			  stepSize_/60,
			  clk->startStringTime(), clk->stopStringTime());
      fprintf(fpo, "%% x.ij in the 1st column means\n");
      fprintf(fpo, "%%   i: 0=arrived  1=not arrived\n");
      fprintf(fpo, "%%   j: 0=unguided 1=guided\n");
      fprintf(fpo, "%% The first and last column may not be useful.\n");
   }
   outputCounts(fpo);
   outputTravelTimes(fpo);
   outputSpeeds(fpo);
   outputVKMT(fpo);

   fclose(fpo);

   cout << nComplete_ << " complete and "
		<< nInComplete_ << " incomplete trips recorded in <"
		<< filename << ">." << endl;
}


void TS_MoeCollector::outputCounts(FILE *fpo)
{
   register int i, j, k;

   if (!theSimulationEngine->isRectangleFormat()) {
      fprintf(fpo, "\n%% Number of vehicles arrived\n\n");
   }
   for (i = 0; i < 2; i ++) {
      for (j = 0; j < 2; j ++) {
		 fprintf(fpo, "0.%d%d", i, j);
		 for (k = 0; k < nIntervals_; k ++) {
			fprintf(fpo, "%7d", arrivals_[i][j][k]);
		 }
		 fprintf(fpo, "\n");
      }
   }

   if (!theSimulationEngine->isRectangleFormat()) {
      fprintf(fpo, "\n%% Number of vehicles departed\n\n");
   }
   for (i = 0; i < 2; i ++) {
      for (j = 0; j < 2; j ++) {
		 fprintf(fpo, "1.%d%d", i,j);
		 for (k = 0; k < nIntervals_; k ++) {
			fprintf(fpo, "%7d", finished_[i][j][k].count());
		 }
		 fprintf(fpo, "\n");
      }
   }

   if (selectedPairs_.size() <= 0) return;

   if (!theSimulationEngine->isRectangleFormat()) {
      fprintf(fpo, "\n%% Number of vehicles departed for OD pairs");
      printSelectedPairs(fpo);
      fprintf(fpo, "\n\n");
   }
   for (i = 0; i < 2; i ++) {
      for (j = 0; j < 2; j ++) {
		 fprintf(fpo, "2.%d%d", i,j);
		 for (k = 0; k < nIntervals_; k ++) {
			fprintf(fpo, "%7d", chosenPair_[i][j][k].count());
		 }
		 fprintf(fpo, "\n");
      }
   }
}


void TS_MoeCollector::outputTravelTimes(FILE *fpo)
{
   register int i, j, k;

   if (!theSimulationEngine->isRectangleFormat()) {
      fprintf(fpo, "\n%% Travel time (seconds, by departure time)\n\n");
   }
   for (i = 0; i < 2; i ++) {
      for (j = 0; j < 2; j ++) {
		 fprintf(fpo, "3.%d%d", i,j);
		 for (k = 0; k < nIntervals_; k ++) {
			fprintf(fpo, "%7.0lf", finished_[i][j][k].travelTime());
		 }
		 fprintf(fpo, "\n");
      }
   }

   if (selectedPairs_.size() <= 0) return;

   if (!theSimulationEngine->isRectangleFormat()) {
      fprintf(fpo, "\n%% Travel time (seconds) for OD pairs");
      printSelectedPairs(fpo);
      fprintf(fpo, "\n\n");
   }
   for (i = 0; i < 2; i ++) {
      for (j = 0; j < 2; j ++) {
		 fprintf(fpo, "4.%d%d", i, j);
		 for (k = 0; k < nIntervals_; k ++) {
			fprintf(fpo, "%7.0lf", chosenPair_[i][j][k].travelTime());
		 }
		 fprintf(fpo, "\n");
      }
   }
}


void TS_MoeCollector::outputSpeeds(FILE *fpo)
{
   register int i, j, k;

   if (!theSimulationEngine->isRectangleFormat()) {
      fprintf(fpo, "\n%% Speed (mph, by departure time)\n\n");
   }
   for (i = 0; i < 2; i ++) {
      for (j = 0; j < 2; j ++) {
		 fprintf(fpo, "5.%d%d", i,j);
		 for (k = 0; k < nIntervals_; k ++) {
			fprintf(fpo, "%7.1lf", finished_[i][j][k].speed());
		 }
		 fprintf(fpo, "\n");
      }
   }

   if (selectedPairs_.size() <= 0) return;

   if (!theSimulationEngine->isRectangleFormat()) {
      fprintf(fpo, "\n%% Speed (mph) for OD pairs");
      printSelectedPairs(fpo);
      fprintf(fpo, "\n\n");
   }
   for (i = 0; i < 2; i ++) {
      for (j = 0; j < 2; j ++) {
		 fprintf(fpo, "6.%d%d", i,j);
		 for (k = 0; k < nIntervals_; k ++) {
			fprintf(fpo, "%7.1lf", chosenPair_[i][j][k].speed());
		 }
		 fprintf(fpo, "\n");
      }     
   }
}


void TS_MoeCollector::outputVKMT(FILE *fpo)
{
   register int i, j, k;

   if (!theSimulationEngine->isRectangleFormat()) {
      fprintf(fpo, "\n%% VKMT (by departure time)\n\n");
   }
   for (i = 0; i < 2; i ++) {
      for (j = 0; j < 2; j ++) {
		 fprintf(fpo, "7.%d%d", i,j);  
		 for (k = 0; k < nIntervals_; k ++) {
			fprintf(fpo, "%7.3lf", finished_[i][j][k].vkmt());
		 }
		 fprintf(fpo, "\n");
      }
   }

   if (selectedPairs_.size() <= 0) return;

   if (!theSimulationEngine->isRectangleFormat()) {
      fprintf(fpo, "\n%% VKMT for OD pairs");
      printSelectedPairs(fpo);
      fprintf(fpo, "\n\n");
   }
   for (i = 0; i < 2; i ++) {
      for (j = 0; j < 2; j ++) {
		 fprintf(fpo, "8.%d%d", i,j);
		 for (k = 0; k < nIntervals_; k ++) {
			fprintf(fpo, "%7.3lf", chosenPair_[i][j][k].vkmt());
		 }
		 fprintf(fpo, "\n");
      }
   }
}
