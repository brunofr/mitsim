//-*-c++-*------------------------------------------------------------
// NAME: Read buses from a file
// AUTH: Qi Yang & Daniel Morgan
// FILE: TS_BusAssignmentTable.C
// DATE: Fri Nov 30 16:30:20 2001
//--------------------------------------------------------------------

#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>

#include "TS_BusAssignmentTable.h"
#include "TS_Vehicle.h"
#include "TS_VehicleList.h"

RN_Vehicle *
TS_BusAssignmentTable::newVehicle()
{
   return theVehicleList->recycle();
}

