//-*-c++-*------------------------------------------------------------
// NAME: Microscopic Traffic Simulator
// AUTH: Qi Yang
// FILE: TS_SegmentStatistics.C
// DATE: Mon Feb 19 19:48:19 1996
//--------------------------------------------------------------------

#include <Tools/SimulationClock.h>
#include <Tools/ToolKit.h>

#include <Tools/Math.h>

#include <GRN/RN_DynamicRoute.h>
#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>

#include "TS_Network.h"
#include "TS_Segment.h"
#include "TS_Lane.h"
#include "TS_Vehicle.h"
#include "TS_Parameter.h"
#include "TS_Engine.h"

#ifdef EXTERNAL_MOE
#include <MOE/MOE_TSSampler.h>
#endif

void
TS_Segment::initializeStatistics()
{
#ifdef EXTERNAL_MOE
   // Peter: I am not sure if this is right. Let us discuss (Qi)
   if (samplingStepSize_ > 0) {
      nSecondsPerPeriod_ = samplingStepSize_;
   } else {
      samplingStepSize_ = D_AREA_SAMPLING_STEP_SIZE;
   }
#endif

   int n = theGuidedRoute->infoPeriods();

   nSamplesTravelTimeAtEntering_ = new int[n];
   sumOfTravelTimeAtEntering_ = new double[n];

   resetEnteringStatistics();
   resetLeavingStatistics();
}

void
TS_Segment::resetLeavingStatistics()
{
   nVehiclesEntered_ = 0;
   nVehiclesLeft_ = 0;

   nSamples_ = 0;
   sumOfDensity_ = 0.0;
   sumOfSpeed_ = 0.0;

   nSamplesTravelTimeAtLeaving_ = 0;
   sumOfTravelTimeAtLeaving_ = 0.0;

   for (int i = 0; i < nLanes_; i ++) {
      ((TS_Lane *)lane(i))->resetStatistics();
   }
}

void
TS_Segment::resetEnteringStatistics()
{
   register int i, n = theGuidedRoute->infoPeriods();
   for (i = 0; i < n; i ++) {
      nSamplesTravelTimeAtEntering_[i] = 0;
      sumOfTravelTimeAtEntering_[i] = 0.0;
   }
}

void
TS_Segment::countVehiclesEntered()
{
   nVehiclesEntered_ ++;
}

void
TS_Segment::countVehiclesLeft()
{
   nVehiclesLeft_ ++;
}

void
TS_Segment::samplingStatistics()
{
   // Number of samples

   nSamples_ ++;

   // Density

   sumOfDensity_ += calcDensity();

   // Speed

   double sum = 0.0;
   int num = 0;
   TS_Vehicle *pv = firstVehicle();
   while (pv) {
      sum += pv->currentSpeed();
      pv = pv->macroTrailing();
      num ++;
   }

   sumOfSpeed_ += (num ? (sum / num) : freeSpeed());

   // Speed and density in each lane

   for (register int i = 0; i < nLanes_; i ++) {
      ((TS_Lane *)lane(i))->samplingStatistics();
   }
}


float
TS_Segment::averageSpeed()
{
   if (nSamples_ > 0) {
      return (sumOfSpeed_ / (float)nSamples_); 
   } else {
      return freeSpeed();
   }
}


float
TS_Segment::averageDensity()
{
   if (nSamples_ > 0) {
      return (sumOfDensity_ / nSamples_);
   } else {
      return (0.0);
   }
}


void
TS_Segment::recordTravelTime(TS_Vehicle * pv)
{
   /* Tavel time spent in current segment */

   float tt = pv->timeInSegment();

   /* These are for calculating average travel time for the vehicle
   * who LEFT this segment during the reporting time interval. */

   nSamplesTravelTimeAtLeaving_ ++;
   sumOfTravelTimeAtLeaving_ += tt;
  
   // These are for calculating average travel time for the vehicle
   // who ENTER this segment during the reporting time interval.
	
   // Calculate the ID of the entry time period.
   
   int i = theGuidedRoute->whichPeriod(pv->timeEntersSegment());

   // Time spent in this segment

   sumOfTravelTimeAtEntering_[i] += tt;
   nSamplesTravelTimeAtEntering_[i] ++;
}

float
TS_Segment::averageTravelTimeAtLeaving()
{
   if (nSamplesTravelTimeAtLeaving_ > 0) { 
      return (sumOfTravelTimeAtLeaving_ / nSamplesTravelTimeAtLeaving_);
   } else {
      return (length() / freeSpeed());
   }
}

float
TS_Segment::averageTravelTimeAtEntering(int i)
{
   float tt;

   if (nSamplesTravelTimeAtEntering_[i] > 0) {
      tt = sumOfTravelTimeAtEntering_[i] / 
			nSamplesTravelTimeAtEntering_[i];
   } else {
      tt = length() / freeSpeed();
   }

   return (tt);
}

int
TS_Segment::numberOfVehiclesEntering(int i)
{
   return (nSamplesTravelTimeAtEntering_[i]);
}

void
TS_Segment::printStatistics(ofstream& os)
{
  double d = averageDensity() / theParameter->densityFactor();
  double v = averageSpeed() / theParameter->speedFactor();
  // double d = averageDensity() ;
  // double v = averageSpeed();
   double t = averageTravelTimeAtLeaving();

   os << indent << code_ << endc
      << nVehiclesEntered()/nLanes_ << endc
      << nVehiclesLeft()/nLanes_ << endc
      << Fix(d, 0.1) << endc
      << Fix(v, 0.1) << endc
      << Fix(t, 0.1) << endc
      << OPEN_TOKEN << endl;

   for (register int i = 0; i < nLanes_; i ++) {
      ((TS_Lane*)lane(i))->printStatistics(os);
   }
   os << indent << CLOSE_TOKEN << endl;

#ifdef EXTERNAL_MOE
   const int MAX_LANES = 10;
   double lanes_statistics[MAX_LANES];

   for  (i = 0; i< nLanes_; i++){
      lanes_statistics[2*i] = ((TS_Lane*)lane(i))->averageDensity();
      lanes_statistics[2*i+1] = ((TS_Lane*)lane(i))->averageSpeed();
   }

   theMOESampler->writeAreaMsg(
      code_,
      nVehiclesEntered()/nLanes_,
      nVehiclesLeft()/nLanes_,
      averageDensity()/theParameter->densityFactor(),
      averageSpeed()/theParameter->speedFactor() ,
      averageTravelTimeAtLeaving(),
      nLanes_,
      lanes_statistics);
			       
   delete [] lanes_statistics;
#endif	
}


void
TS_Segment::printTravelTimes(ofstream& os)
{
   os << code_
      << endc << type()
      << endc << length() / theParameter->lengthFactor();

   if (!theEngine->isRectangleFormat()) {
      os << endc << OPEN_TOKEN << endl;
   }

   int n = theGuidedRoute->infoPeriods();

   for (int i = 0; i < n; i ++) {
      int n = nSamplesTravelTimeAtEntering_[i]; 
      double t = averageTravelTimeAtEntering(i);

      if (!theEngine->isRectangleFormat()) {
			os << endc << OPEN_TOKEN;
      }
      os << endc << n << endc << Fix(t, 0.1);
      if (!theEngine->isRectangleFormat()) {
			os << endc << CLOSE_TOKEN;
      }
   }
	os << endl;
   if (!theEngine->isRectangleFormat()) {
      os << CLOSE_TOKEN << endl;
   }
}
