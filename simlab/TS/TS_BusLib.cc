//-*-c++-*------------------------------------------------------------
// NAME: Microcscopic Traffic Simulator
// AUTH: Qi Yang & Daniel Morgan
// FILE: TS_BusLib.C
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#include <new>
using namespace std;

#include <Tools/Math.h>

#include <Tools/ToolKit.h>
#include <Tools/Random.h>
#include <Tools/Scaler.h>
#include <Tools/GenericVariable.h>

#include <GRN/RN_CtrlStation.h>
#include <GRN/RN_SurvStation.h>

#include "TS_BusLib.h"
#include "TS_Parameter.h"
#include "TS_Network.h"
#include "TS_Segment.h"
#include "TS_Lane.h"
#include "TS_Vehicle.h"

TS_BusLib::TS_BusLib()
{
}

TS_BusLib::~TS_BusLib()
{
}

/*
 * Read bus information from the given file.
 */

void
TS_BusLib::loadAttributes(ArrayElement *data)
{
   static int type = 0;

   name_ = Copy((const char*)data[0]);
   length_ = data[1];
   seatingCapacity_ = data[2];
   totalCapacity_ = data[3];
   boardingRate_ = data[4];
   alightingRate_ = data[5];
   deadTimeLowerBound_ = data[6];
   deadTimeUpperBound_ = data[7];
   crowdingFactor_ = data[8];
   type ++;

   length_ *= theParameter->lengthFactor();
}

void
TS_BusLib::print(ostream &os)
{
   os << name_ << endc 
      << length_ << endc
      << seatingCapacity_ << endc
      << totalCapacity_ << endc
      << boardingRate_ << endc
      << alightingRate_ << endc
      << deadTimeLowerBound_ << endc
      << deadTimeUpperBound_ << endc 
      << crowdingFactor_ << endc << endl;
}
