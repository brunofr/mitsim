//-*-c++-*------------------------------------------------------------
// NAME: Drawable Road Network
// AUTH: Qi Yang
// FILE: TS_Legend.h
// DATE: Sun Feb 11 16:32:40 1996
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#ifndef TS_LEGEND_HEADER
#define TS_LEGEND_HEADER

#include <DRN/DRN_Legend.h>

class TS_Legend : public DRN_Legend
{
   public:

      TS_Legend(DRN_DrawingArea *d) : DRN_Legend(d) { }
      ~TS_Legend() { }

      void draw(int mode);	// virtual
      void draw_more();		// virtual, called by draw
      void drawVehicleLegend();
};

#endif

#endif // INTERNAL_GUI
