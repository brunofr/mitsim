//-*-c++-*------------------------------------------------------------
// TS_SetupDialog.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI
#ifndef TS_SETUPDIALOG_HEADER
#define TS_SETUPDIALOG_HEADER


#include <vector>
#include <Xmw/XmwDialogManager.h>
#include <Xmw/XmwInputField.h>
using namespace std;

class TS_SetupDialog : public XmwDialogManager
{
public:

  TS_SetupDialog(Widget parent);
  ~TS_SetupDialog();
  void post();				// virtual

private:

  void set(char **ptr, const char *s);

  void activate();
  void deactivate();

  // overload the virtual functions in base class

  void okay(Widget, XtPointer, XtPointer);
  void cancel(Widget, XtPointer, XtPointer);
  void help(Widget, XtPointer, XtPointer);

private:

  Widget title_;
  std::vector<XmwInputField *> flds_;
};

#endif
#endif // INTERNAL_GUI
