//-*-c++-*------------------------------------------------------------
// NAME: Microcscopic Traffic Simulator
// AUTH: Qi Yang
// FILE: TS_Version.C
// DATE: Fri Jun 23 21:07:18 1995
//--------------------------------------------------------------------

#include <iostream>
#include <new>
using namespace std;

#include <Tools/ToolKit.h>

#include "TS_Exception.h"
#include "TS_Version.h"

char*  g_majorVersionNumber = "1";
char*  g_minorVersionNumber = "0 Beta";
char*  g_codeDate           = "1996";

void
Welcome(char *exec)
{
   theExec = Copy(ToolKit::RealPath(exec));

   cout << "MITSIM Release "
	<< g_majorVersionNumber << "." << g_minorVersionNumber << endl
	<< "Copyright (c) " << g_codeDate << endl
	<< "Massachusetts Institute of Technology" << endl
	<< "All Right Reserved" << endl << endl;

#ifndef FINAL_VERSION
   PrintVersionInfo();
#endif

   cout.flush();

   set_new_handler(::FreeRamException);
}
