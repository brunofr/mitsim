//-*-c++-*------------------------------------------------------------
// NAME: Traffic Simulation
// NOTE: Hold information for a queue
// AUTH: Qi Yang
// FILE: TS_Queue.C
// DATE: Wed Oct 23 20:31:51 1996
//--------------------------------------------------------------------

#include <fstream>
using namespace std;

#include <Tools/ToolKit.h>

#include "TS_Queue.h"
#include "TS_QueueHead.h"
#include "TS_Lane.h"
#include "TS_Segment.h"
#include "TS_Link.h"
#include "TS_Vehicle.h"

#ifdef EXTERNAL_MOE
#include <MOE/MOE_TSSampler.h>
#endif


TS_Queue::TS_Queue(TS_QueueHead* h, TS_Queue* q, TS_Lane* pl)
   : nVehicles_(0),
     endPos_(0.0),
     start_(0)
{
   head_ = h;
   front_ = q;
   lane_ = pl;
   if (q) {
      nVehiclesAhead_ = q->nVehiclesAhead_ + q->nVehicles_;
   } else {
      nVehiclesAhead_ = 0;
   }
}


float
TS_Queue::firstMaxSpeed()
{
   return head_->firstMaxSpeed();
}

float
TS_Queue::lastMaxSpeed()
{
   return head_->lastMaxSpeed();
}

float
TS_Queue::maxGap()
{
   return head_->maxGap();
}


int
TS_Queue::isFull()
{
   if (lane_->length() - endPos_ > TS_Queue::maxGap()) {
      return (0);
   } else {
      return (1);
   }
}

int
TS_Queue::isEmpty()
{
   if (nVehicles_) {
      return (0);
   } else {
      return (1);
   }
}



// Update statistics of the queue,. Currently it has only the queue
// length, but we could add more statistics later.

void
TS_Queue::update(TS_Vehicle* pv)
{
   nVehicles_ ++;
   endPos_ = pv->distance() + pv->length();
}


// Report queue infomation

void
TS_Queue::report()
{
   int num = nVehiclesAhead_ + nVehicles_;

   // Find where queue starts

   TS_Queue* q = this;
   while (!(q->start_)) {
      q = q->front_;
   }

   // Report lane IDs and queue length

   fprintf(TS_QueueHead::fpo(), "%s%d %d %d",
	   indent, q->lane_->code(), lane_->code(), num);

   // Report the spillback queue

   if (isFull() && lane_->segment()->isTheBeginning()) {
	 int n = ((TS_Link*)lane_->link())->queueLength();
	 if (n) {
	   fprintf(TS_QueueHead::fpo(), " # %d",
			   n / lane_->segment()->nLanes());
	 }
   }

   fprintf(TS_QueueHead::fpo(), "\n");

#ifdef EXTERNAL_MOE 
   theMOESampler->writeQueue(q->lane_->code(),
			     lane_->code(),
			     num);
#endif
}
