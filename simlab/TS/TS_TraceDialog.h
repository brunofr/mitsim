//-*-c++-*------------------------------------------------------------
// TS_TraceDialog.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef TS_TRACE_DIALOG_HEADER
#define TS_TRACE_DIALOG_HEADER

#include <Xmw/XmwDialogManager.h>
#include <Xmw/XmwCallback.h>

class TS_DrawableVehicle;

class TS_TraceDialog : public XmwDialogManager
{
  CallbackDeclare(TS_TraceDialog);

public:

  ~TS_TraceDialog();

  static TS_TraceDialog *the();

  void post_info(TS_DrawableVehicle *p);

  TS_DrawableVehicle* vehicle() { return vehicle_; }
  void update(TS_DrawableVehicle *pv);

private:

  TS_TraceDialog(Widget parent);

  void setValue(int i, const char *value);
  const char* getValue(int i);

  // existing callbacks

  void okay(Widget, XtPointer, XtPointer);
  void cancel(Widget, XtPointer, XtPointer);
  void help(Widget, XtPointer, XtPointer);

  // new callbacks

  void enter ( Widget, XtPointer, XtPointer );
//   void leave ( Widget, XtPointer, XtPointer );

private:

  static TS_TraceDialog *instance_;

  TS_DrawableVehicle *vehicle_;	// current vehicle
  Widget info_;					// XbaeMatrix
};

#endif
