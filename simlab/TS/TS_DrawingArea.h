//-*-c++-*------------------------------------------------------------
// TS_DrawingArea.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#ifndef TS_DRAWINGAREA_HEADER
#define TS_DRAWINGAREA_HEADER

#include <DRN/DRN_DrawingArea.h>

class TS_DrawingArea : public DRN_DrawingArea
{
   public:

      TS_DrawingArea(Widget parent);

      ~TS_DrawingArea() { }
      
	  // Overload the virtual functions

      DRN_Legend * newLegend();
      DRN_KeyInteractor * newKeyInteractor();

	  void updateButtons();	// virtual

   private:
	  
};

#endif // TS_DRAWINGAREA_HEADER
#endif // INTERNAL_GUI
