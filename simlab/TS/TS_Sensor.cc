//-*-c++-*------------------------------------------------------------
// TS_Sensor.cc
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#include <Tools/Math.h>

#include <Tools/ToolKit.h>
#include <Tools/Random.h>
#include <Tools/SimulationClock.h>
#include <vector> //IEM(Jun18)

#include <GRN/Constants.h>

#include "TS_Communicator.h"
#include "TS_Network.h"
#include "TS_Node.h"
#include "TS_Link.h"
#include "TS_Segment.h"
#include "TS_Lane.h"
#include "TS_Vehicle.h"
#include "TS_VehicleLib.h"
#include "TS_Sensor.h"
#include "TS_Engine.h"
#include "TS_Parameter.h"
#include "TS_FileManager.h"

double TS_Sensor::resetTime_ = 0.0;
int TS_Sensor::harmonic_ = 0;
vector<double> TS_Sensor::resetTimes_; //IEM(Jun18) required initialization of static data member

TS_Sensor::TS_Sensor()
  : TC_Sensor(), invspeed_(0.0), prev_time_off_(0.0)
{
}


// Calculate the time required to travel a distance of d, given the
// vehicle's initial speed v and a constant acc/deceleration rate of
// acc.

float
TS_Sensor::distance2Time(float dis, float vel, float acc)
{
  const float acc_epsilon = 1.0E-2;
  const float spd_epsilon = 1.0E-2;
  const float dis_epsilon = 1.0E-3;
  const float inf_time = 86400.0;

  if (dis < dis_epsilon) {		// already there

	return 0.0;

  } else if (fabs(acc) > acc_epsilon) { // acc is NOT zero

	float delta = vel * vel + 2.0 * acc * dis;

	if (delta > 0.0) {

	  return (sqrt(delta) - vel) / acc;

	} else {			// stop before move to the given distance

	  return inf_time;
	}
  } else {						// acc is zero

	if (vel > spd_epsilon) { // NOT stopped
	  return dis / vel;
	} else {					// stopped
	  return inf_time;
	}
  }
}


// Calculate the speed and the time at the moment that the vehicle's
// back bumper leaves the detector.  It returns the speed the vehicle
// passes the detector.

float
TS_Sensor::calcSpeedAndTime(TS_Vehicle* pv, float prevpos,
							float prevvel, float acc, TimeSpeed *p)
{
  // Distance traveled from previous position to the position it just
  // leave the sensor.

  float dis = prevpos - distance() + pv->length();

  // Time required to travel such a distance

  float delta = distance2Time(dis, prevvel, acc);

  // Speed

  float vel = prevvel + delta * acc;

  if (p) {
	p->time = theSimulationClock->currentTime() + delta;
	p->speed = vel;
  }

  return vel;
}


/*
 *------------------------------------------------------------------
 * Convert the sensor readings from accumulated to average flow per
 * lane per hour
 *------------------------------------------------------------------
 */

int TS_Sensor::flow()
{
  //IEM(Jun15) Fixed to use appropriate element of resetTimes vector
  float step = theSimulationClock->currentTime() - resetTimes_[intervalType_];
  if (step < FLT_EPSILON) return 0;
  float x = count_ / step * 3600.0;
  if (isLinkWide()) {
	return (int) (x / (float)segment()->nLanes() + 0.5);
  } else {
	return (int) (x + 0.5);
  }
}


// Occupancy makes sense only for lane specific sensors.

float TS_Sensor::occupancy()
{
  //IEM(Jun15) Fixed to use appropriate element of resetTimes vector
  float step = theSimulationClock->currentTime() - resetTimes_[intervalType_];
  if (step < 0.1) return 0.0;

  float occ = occupancy_;
  if (isLinkWide()) {
	occ /= step * (float)segment()->nLanes();
  } else {
	occ /= step;
  }
  return occ * 100.0;
}


// Average speed of the sensor

float TS_Sensor::speed()
{
  if (count_ > 0) {
	if (harmonic_) {
	  return count_ / invspeed_;
	} else {
	  return speed_ / count_;
	}
  } else {
	return segment()->freeSpeed();
  }
}


// Reset the sensor readings to zeros

void
TS_Sensor::resetSensorReadings()
{
  RN_Sensor::resetSensorReadings();
  invspeed_	= 0.0;
}


// Occupance is calculated by accumulating the occupied time by
// individual vehicles.

void
TS_Sensor::calcActivatingData(TS_Vehicle *pv,
							  float prevpos, float prevvel, float acc)
{
  /*
   * LOOP POSITION and DETECTION ZONE
   * 
   * Detector location is represented by the distance from the
   * downstream edge of the detection zone to the end of the
   * link.  A detector is occupied if
   *
   *	 sensor_pos + zone_length > vehicle_pos >  sensor_pos.
   *
   * ACTIVATE and DEACTIVATE
   * 
   * A vehicle will activate the detector when its front bomper
   * touch the upstream edge of the detection zone, i.e.:
   *
   *    vehicle_pos <= sensor_pos + zone
   *
   * and deactivate the detector when its back bumper leave the
   * downstream edge of the detector, i.e.:
   *
   *    vehicle_pos + vehicle_length < sensor_pos
   */

   // Distance to the detector at the beginning of current interval
   //        |------dis------|
   //     PPPP             DDDCCCC
   //     PPPP             DDDCCCC

  static SimulationClock *clk = theSimulationClock;

  float dis = prevpos - distance();
	
  // Distance traveled before activate the detector
  //        |----dis_on---|
  //     PPPP             DDDCCCC
  //     PPPP             DDDCCCC

  float dis_on = dis - zoneLength();

   // Distance traveled before deactivate the detector
   //        |------dis_off------|
   //     PPPP             DDDCCCC
   //     PPPP             DDDCCCC

  float dis_off = dis + pv->length();
		
  // Time to deactivate the detector

  float delta_off = distance2Time(dis_off, prevvel, acc);

  if (delta_off > clk->stepSize()) {
	delta_off = clk->stepSize();
  }

  // Time to activate the detector

  float delta_on = distance2Time(dis_on, prevvel, acc);

  if (delta_on > clk->stepSize()) {
	delta_on = clk->stepSize();
  }

  // Time previous vehicle left the detector wrt the begining of the
  // interval

  float prev_delta_off = prev_time_off_ - clk->currentTime();

  // Calculate overlap time with repect to previous vehicle

  float delta_overlap;
  if (delta_on < prev_delta_off) { // activate same detector simultaneously
	delta_overlap = prev_delta_off - delta_on;
  } else {						// no overlapping
	delta_overlap = 0.0;
  }

  // Record the time this vehicle "leaves" the detector

  prev_time_off_ = clk->currentTime() + delta_off;

  double delta = delta_off - delta_on - delta_overlap;
  if (delta > 0.0) {
	occupancy_ += delta;
  }
}


// Called when the back bumper of the vehicle crosses the down-edge of
// the of detection zone.

void
TS_Sensor::calcPassingData(TS_Vehicle* pv, float prevpos,
						   float prevvel, float acc)
{
  float vel = calcSpeedAndTime(pv, prevpos, prevvel, acc);
  if (vel > theParameter->detectedMinSpeed()) {
	invspeed_ += 1.0 / vel;
  } else {
	invspeed_ += 1.0 / theParameter->detectedMinSpeed();
  }
  speed_ += vel;
  count_ ++;

  if (!pv->isType(VEHICLE_PROBE) || !itask()) return;

  // This probe vehicle and the sensor collects data on individual
  // vehicles. Therefore, we send data TMS or write a record into the
  // avi data file.
   
  TS_Communicator *cmu = (TS_Communicator *)theCommunicator;
  if (cmu->isTmsConnected()) {
	send_i(cmu->tms(), pv);
  }

  if (theEngine->chosenOutput(OUTPUT_VRC_READINGS)) {
	write_i(theFileManager->osVRCSensor(), pv);
  }
}

void
TS_Sensor::calcSnapShotData()
{
  // To be implemented later
}
