//-*-c++-*------------------------------------------------------------
// FILE: TS_Lane.h
// AUTH: Qi Yang
// DATE: Wed Oct 18 14:29:29 1995
//--------------------------------------------------------------------

#ifndef TS_LANE_H
#define TS_LANE_H

#include <iostream>
using namespace std;

#ifdef INTERNAL_GUI
#include <DRN/DRN_Lane.h>
#else  // batch mode
#include <GRN/RN_Lane.h>
#endif // !INTERNAL_GUI

class TS_Link;
class TS_Segment;
class TS_Vehicle;
class TS_Queue;
class TS_QueueHead;
class RN_CtrlStation;
class RN_SurvStation;

#ifdef INTERNAL_GUI
class TS_Lane : public DRN_Lane
#else
class TS_Lane : public RN_Lane
#endif // !INTERNAL_GUI

{				// TS_Lane
      friend class TS_Vehicle;

   public:

      TS_Lane();
      ~TS_Lane() { }

      void calcStaticInfo();	// virtual

      TS_Vehicle * lastInDnLanes();
      TS_Vehicle * firstFromUpLanes();
      TS_Vehicle * firstFromOtherUpLanes(TS_Lane *, int);

      double freeSpeed() { return freeSpeed_; }
      void calcFreeSpeed();
      

      int isDownLanes2Link(int);
      int isDownLanesInLink(TS_Link *);
      int isDownLanesInNextLink(TS_Link *);

      void createNumberOfChanges2OutArcs();
      void createLaneUseRules();

      short int isWrongLane(TS_Vehicle *);
      int doesNotAllow(TS_Vehicle *);
      int isThereBadEventAhead(TS_Vehicle *,
							   float *pdis2stop = NULL,
							   float *pvis = NULL);

      int searchQueue(TS_QueueHead *, TS_Queue *, TS_Vehicle*);
      TS_Queue * calcQueueInfo(TS_QueueHead *, TS_Queue *, TS_Vehicle *);
      TS_Vehicle * findUpVehicle(double);

      double density();
      double dischargeRate(TS_Lane *);//cfc JAn 07
      float speed() { return speed_; }
      float calcSpeed();
      int queueahead(float pos); // VR 01/06/07 -- calculates length of queue ahead of the lvehicle in the lane

      void resetStatistics();
      void printStatistics(std::ostream& os);

      inline unsigned int cRules() { return rules_; }
      inline unsigned int lRules() { return lRules_; }
      inline unsigned int rRules() { return rRules_; }

      inline TS_Lane * left()  { return (TS_Lane*) RN_Lane::left(); }
      inline TS_Lane * right() { return (TS_Lane*) RN_Lane::right(); }
      inline TS_Lane * upLane(int i) {
		 return (TS_Lane *) upLanes_[i];
      }
      inline TS_Lane * dnLane(int i) {
		 return (TS_Lane *) dnLanes_[i];
      }
          
      inline TS_Vehicle* firstVehicle() {	
		 return firstVehicle_;
      }
  
      inline TS_Vehicle* lastVehicle() {
		 return lastVehicle_;
      }
  
      inline void firstVehicle(TS_Vehicle * first) {
		 firstVehicle_ = first;
      }
  
      inline void lastVehicle(TS_Vehicle * last) {
		 lastVehicle_ = last;
      }
  
      inline int nVehicles() {
		 return nVehicles_;
      }

      inline int nVehicles(int n) {
		 nVehicles_ += n;
		 return nVehicles_;
      }
  
      inline short int nChanges(int i) {
		 return nChanges_[i];
      }

      // Accumulate statistics, called by functions with the same name
      // in TS_Segment.

      void samplingStatistics();

      double averageDensity();
      double averageSpeed();

	  inline float maxSpeed() { return maxSpeed_; }
	  float calcMaxSpeed();

   protected:

      unsigned int lRules_;
      unsigned int rRules_;
	
      int nVehicles_;		/* number of vehicle in the lane */
      double dischargeRate_;/*discharge rate of the lane */			
      /*
       * To get on the next link of the path, a vehicle may need to
       * make necessary lane changes. The variable nChanges_ indicates
       * the number of lane changes required before getting on the
       * downstream link. nChanges_ is an array, and each element
       * corresponse to one out going arc at the downstream node. Its
       * value is defined as followings:
       *
       *   positive = change to left
       *   zero = no need to change
       *   negative = change to right
       *
       */

      short int * nChanges_;	/* lane change indicator */

      TS_Vehicle * firstVehicle_; /* pointer to the leading vehicle */
      TS_Vehicle * lastVehicle_;  /* Pointer to the trailing vehicle */

      double accumulatedDensity_;
      double accumulatedSpeed_;

      float speed_;
      float freeSpeed_;
      float maxSpeed_;
};

#endif
