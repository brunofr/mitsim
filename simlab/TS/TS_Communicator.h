//-*-c++-*------------------------------------------------------------
// NAME: Class for communication to TMS and MesoTS.
// AUTH: Qi Yang
// FILE: TS_Communicator.h
// DATE: Sun Nov 19 12:33:35 1995
//--------------------------------------------------------------------

#ifndef TS_COMMUNICATOR_HEADER
#define TS_COMMUNICATOR_HEADER

#include <IO/Communicator.h>

class TS_Communicator : public Communicator
{
   protected:

      IOService tms_;

   public:

      TS_Communicator(int sndtag);

      ~TS_Communicator() { }
      
      inline int isTmsConnected() {
		 return tms_.isConnected();
      }

      IOService& tms() { return tms_; }

      void makeFriends();		// virtual
      int receiveMessages(double sec = 0.0);
      int receiveSimlabMessages(double sec = 0.0);
      int receiveTmsMessages(double sec = 0.0);

      double batchStepSize();	// virtual
};

extern TS_Communicator *theCommunicator;

#endif
