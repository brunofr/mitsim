//-*-c++-*------------------------------------------------------------
// TS_Symbols.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// This file contains the class definition/implementation for the class
//--------------------------------------------------------------------

#ifdef INTERNAL_GUI

#ifndef TS_SYMBOLS_HEADER
#define TS_SYMBOLS_HEADER

#include <DRN/DRN_Symbols.h>

class TS_Symbols : public DRN_Symbols
{
   public:

	  enum {
		 VEHICLE_BORDER_NONE     = 0,
		 VEHICLE_BORDER_TYPE     = 1,
		 VEHICLE_BORDER_INFO     = 2,
		 VEHICLE_BORDER_MOVEMENT = 3,
		 VEHICLE_BORDER_GROUP    = 4,
		 VEHICLE_BORDER_LANEUSE  = 5
      };

	  enum {
		 VEHICLE_SHADE_WAITING   = 0x1,
		 VEHICLE_SHADE_NOSING    = 0x2,
		 VEHICLE_SHADE_YIELDING  = 0x4,
		 VEHICLE_SHADE_MERGING   = 0x8
	  };

   public:

	  TS_Symbols() : DRN_Symbols() { };
	  ~TS_Symbols() { }

	  void registerAll();		// virtual

	  // Access functions

	  XmwSymbol<int>& vehicleBorderCode() {
		 return vehicleBorderCode_;
	  }
	  XmwSymbol<int>& vehicleShadeCodes() {
		 return vehicleShadeCodes_;
	  }
	  XmwSymbol<Boolean>& isVehicleLabelOn() {
		 return isVehicleLabelOn_;
	  }

   protected:

	  // New symbols

	  XmwSymbol<int> vehicleBorderCode_;
	  XmwSymbol<int> vehicleShadeCodes_;
	  XmwSymbol<Boolean> isVehicleLabelOn_;
};

#endif // TS_SYMBOLS_HEADER
#endif // INTERNAL_GUI
