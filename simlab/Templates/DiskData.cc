//-*-C++-*------------------------------------------------------------
// NAME: Template Classes
// NOTE: Binary data file. Store large array or multi-dimensional
//       matrix.
// AUTH: Yang, Qi
// FILE: DiskData.cc
// DATE: Thu Sep 28 18:02:13 1995
//--------------------------------------------------------------------

#ifndef DISKDATA_IMPLEMENTATION
#define DISKDATA_IMPLEMENTATION

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <iostream.h>

#include <IO/Exception.h>
#include <Tools/ToolKit.h>
#include <UTL/Misc.h>

#include "DiskData.h"

template <class T>
DiskData<T>::DiskData()
   : name_(NULL),
	 timeTags_(NULL),
     handle_(-1),
     nItems_(0),
     start_(0),
     bytes_(sizeof(T)),
     size_(0),
     ncols_(8),
     delete_when_done_(1)
{
}

template <class T>
DiskData<T>::DiskData(const DiskData& other)
{
   *this = other;
}


template <class T>
DiskData<T>::~DiskData()
{
   if (delete_when_done_) remove();
   if (name_) free(name_);
}


template <class T>
void DiskData<T>::remove()
{
   if (handle_ >= 0) {
      ::close(handle_);
      ::unlink(name_);
      handle_ = -1;
   }
   if (name_) {
	  free(name_);
   }
   name_ = NULL;
}


// Create a new file.

template <class T>
void DiskData<T>::create(const char *name, T data, size_t nitems,
						 int ntags, const time_t *tags, int ncols)
{
   const size_t BUFFER_SIZE = 16384;	// 16 KB
   T *buffer = NULL;
   unsigned int sz = 0;

   if (delete_when_done_) remove();

   ncols_ = ncols;

   if ((handle_ = ::open(name, O_RDWR|O_CREAT|O_TRUNC,
						 S_IRUSR|S_IWUSR|
						 S_IRGRP|S_IWGRP|
						 S_IROTH)) < 0) {
      ErrorMsg(name_);
      goto error;
   }

   name_ = strdup(name);

   if (::write(handle_, &nitems, sizeof(size_t)) != sizeof(size_t) ||
       ::write(handle_, &ntags, sizeof(int)) != sizeof(int)) {
      ErrorMsg(name_);
      goto error;
   }

   nTags_ = ntags;
   sz = ntags * sizeof(time_t);
   if (tags) {
	  timeTags_ = new time_t [ntags];
	  memcpy(timeTags_, tags, sz);
      if (::write(handle_, tags, sz) != sz) {
		 ErrorMsg(name_);
		 goto error;
      }
      delete_when_done_ = 0;
   } else {
	  timeTags_ = NULL;
      delete_when_done_ = 1;
   }

   start_ = sizeof(size_t) + sizeof(int) + sz;
   nItems_ = nitems;
   size_ = start_ + nItems_ * bytes_;
   
   buffer = new T [BUFFER_SIZE];

   size_t i;
   for (i = 0; i < BUFFER_SIZE; i ++) {
      buffer[i] = data;
   }

   for (i = 0; i < nitems; i += BUFFER_SIZE) {
      size_t left = nitems - i;
      if (left > BUFFER_SIZE) left = BUFFER_SIZE;
      size_t size = left * bytes_;
      if (::write(handle_, buffer, size) != size) {
		 ErrorMsg(name_);
		 goto error;
      }
   }

   delete [] buffer;

   return;

  error:

   cerr << "Error:: Failed creating temporary data file <"
		<< name << ">." << endl;
   theException->exit (1);
}


// Open an existing file. Return 0 if the file has same time tag,
// otherwise it returns -1 if error and 1 if time tags are not the
// same.

template <class T>
int DiskData<T>::open(
   const char *name,
   size_t n, int ntags, const time_t *tags,
   int ncols)
{
   // Close and may erase the old file 

   if (name != name_) {
	  if (delete_when_done_) {
		 remove();
	  }
	  if (name_) {
		 free(name_);
	  }
	  name_ = strdup(name);
	  delete_when_done_ = 1;
   } else {
	  delete_when_done_ = 0;
   }

   if ((handle_ = ::open(name, O_RDWR)) < 0) {
      return -1;
   }

   ncols_ = ncols;

   if (::read(handle_, &nItems_, sizeof(size_t)) != sizeof(size_t) ||
       ::read(handle_, &nTags_, sizeof(int)) != sizeof(int)) {
      return -1;
   } else if (n > 0 && n != nItems_) {
      return 1;
   } else if (ntags > 0 && nTags_ != ntags) {
      return 1;
   } else if (nTags_ > 0) {
	  timeTags_ = new time_t [nTags_];
	  size_ = nTags_ * sizeof(size_t);
	  if (::read(handle_, timeTags_, size_) != size_) {
		 return -1;
	  }
	  if (ntags > 0) {
		 for (int i = 0; i < nTags_; i ++) {
			if (tags[i] != timeTags_[i]) return 1;
		 }
	  }
      delete_when_done_ = 0;
   } else {
	  timeTags_ = NULL;
   }

   start_ = sizeof(size_t) + sizeof(int) + nTags_ * sizeof(time_t);
   bytes_ = sizeof(T);
   size_ = start_ + nItems_ * bytes_;

   return 0;
}


template <class T>
DiskData<T>& DiskData<T>::operator=(const DiskData<T> &other)
{
   static int id = 0;
   
   if (delete_when_done_) {
	  remove();
   }

   name_ = StrCopy("%s.%d", other.name_, (++id));

   // Copy the file

   if (ToolKit::copyFile(other.name_, name_) != 0) {
	  goto error;
   }
   if (open(name_) != 0) {
	  goto error;
   }

   return *this;
   
  error:
   
   cerr << "Error:: Failed copying <"
		<< other.name_ << "> to <" << name_ << ">." << endl;
   theException->exit(1);
   return *this;
}


// Return the ith data item

template <class T>
T DiskData<T>::read(size_t i)
{
   T data;
   if (::lseek(handle_, start_ + bytes_ * i, SEEK_SET) < 0 ||
       ::read(handle_, &data, bytes_) != bytes_) {
      cerr << "Error:: Failed reading data item <" << i
		   << "> from <" << name_ << ">." << endl;
      Error_Bail_Out();
   }
   return data;
}


// Write the data at the given pos

template <class T>
void DiskData<T>::write(size_t i, T data)
{
   if (::lseek(handle_, start_ + bytes_ * i, SEEK_SET) < 0 ||
       ::write(handle_, &data, bytes_) != bytes_) {
      cerr << "Error:: Failed writting data item <" << i
		   << "> from <" << name_ << ">." << endl;
      Error_Bail_Out();
   }
}

// Print all data

template <class T>
void DiskData<T>::print(ostream &os)
{
   os << "% File name  = " << name_ << endl;
   os << "% Data items = " << nItems_ << endl;
   os << "% Total size = " << size_ << endl;
   os << "{" << endl;
   T data;
   for (size_t i = 0; i < nItems_; i ++) {
      data = read(i);
      os << '\t' << data;
      if (i % ncols_ == 0) {
		 os << endl;
      }
   }
   os << "}" << endl;
}

#endif // DISKDATA_IMPLEMENTATION
