//-*-c++-*------------------------------------------------------------
// NAME: Template Classes
// NOTE: A generic list class
// AUTH: Qi Yang
// FILE: Queue.cc
// DATE: Thu Sep 21 21:42:06 1995
//--------------------------------------------------------------------

#ifndef QUEUE_IMPLEMENTATION
#define QUEUE_IMPLEMENTATION

#include "Queue.h"

#include <cstdlib>
#include <iostream>
//using namespace std;


//--------------------------------------------------------------------
// Requires: No dangling pointers to items in the queue
// Modifies: all handles of the queue
// Effects : Release all items in the queue
//--------------------------------------------------------------------

template <class T>
Queue<T>::~Queue()
{
  while (head_) {
    tail_ = head_;
    head_ = head_->next_;
    delete tail_;
  }
  tail_ = NULL;					// is this necessary?
  nElements_ = 0;
}


template <class T>
QueueItem<T> * Queue<T>::find(T *i)
{
  QueueItem<T> *c = tail_;
  while (c && c->item_ != i) {
	c = c->prev_;
  }
  return (c);
}

template <class T>
QueueItem<T> * Queue<T>::find(int c) // c is item code we are looking for
{
   QueueItem<T> *a = head_;
   while (a && a->item_->code() != c) {
      a = a->next_;
   }
   return a;
}



//--------------------------------------------------------------------
// Requires: Items in the queue are sorted by key.
// Modifies: Item's pointers and head_, tail_ and elements counter
// Effects : Returns a pointer to the item with a key after (or 
//           equal to) k, or NULL if no such item is found.
//--------------------------------------------------------------------

template <class T>
void Queue<T>::append(QueueItem<T> *i)
{
  i->prev_ = tail_;
  i->next_ = NULL;
  
  if (tail_) {					// append at end
	tail_->next_ = i;
  } else {						// queue is empty
	head_ = i;
  }
  tail_ = i;
  nElements_ ++;
}

template <class T>
QueueItem<T> * Queue<T>::append(T *i)
{
  QueueItem<T> *item = new QueueItem<T> (i);
  append(item);
  return (item);
}


//--------------------------------------------------------------------
// Requires: Items in the queue is sorted and item must be an element 
//           in the queue.  
// Modifies: Item's pointers and head_, tail_ and elements counter
// Effects : Removes item from the queue.
//--------------------------------------------------------------------

template <class T>
void Queue<T>::remove(QueueItem<T> *item)
{
  if (item->prev_) {
    item->prev_->next_ = item->next_;
  } else {						// head
    head_ = item->next_;
  }
  if (item->next_) {
    item->next_->prev_ = item->prev_;
  } else {						// tail
    tail_ = item->prev_;
  }
  nElements_ --;
}

template <class T>
QueueItem<T> * Queue<T>::remove(T *i)
{
  QueueItem<T> *item = find(i);
  if (item) remove(item);
  return item;
}

template <class T>
QueueItem<T> * Queue<T>::remove()
{
  if (head_) remove(head_);
  return head_;
}


#endif // QUEUE_IMPLEMENTATION
