//-*-c++-*------------------------------------------------------------
// Object.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
//--------------------------------------------------------------------

#ifndef OBJECT_HEADER
#define OBJECT_HEADER

#include <cstring>
#include <cstdlib>
#include <iostream>
#include <functional>

class CodedObject
{
protected:

  int code_;					// ID of the thing

public:

  CodedObject() : code_(0) { }
  CodedObject(int c) { code_ = c; }

  virtual ~CodedObject() { }

  virtual void code(int c) { code_ = c; }
  virtual int code() const { return code_; }
  virtual char *name() { return 0; }

  bool operator == (CodedObject &i) {
	return (code_ == i.code());
  }
  bool operator != (CodedObject &i) {
	return (code_ != i.code());
  }

  virtual bool need_deep_copy() { return false; }
  virtual void deep_copy(CodedObject &i) { code_ = i.code(); }

  // By default these comp functions has the same functionality as
  // op ==

  virtual int cmp(CodedObject *i) const { // for sorting
	if (code_ < i->code()) return -1;
	else if (code_ > i->code()) return 1;
	else return 0;
  }
  virtual bool eq(CodedObject *i) const {	// for identification
	return (code_ == i->code());
  }
  virtual int cmp(int c) const {		// for search and identification
	if (code_ < c) return -1;
	else if (code_ > c) return 1;
	else return 0;
  }

  virtual void print(std::ostream &os = std::cout) {
	os << "<" << code_ << ">";
  }
};


class NamedObject : public CodedObject
{
protected:

  char *name_;					// name of the thing

public:

  NamedObject() : CodedObject(), name_(0) { }
  NamedObject(char *n, int c) : name_(0), CodedObject(c) {
	name(n);
  }
  virtual ~NamedObject() {
	if (name_) free(name_);
  }
  void name(const char *n) {
	if (name_ == n) return;
	if (name_) free(name_);
	if (n && *n != '\0') name_ = strdup(n);
	else name_ = 0;
  }
  char * name() { return name_; }

  bool need_deep_copy() { return true; }
  virtual void deep_copy(CodedObject &i) {
	name(i.name());
	code(i.code());
  }
  virtual void print(std::ostream &os = std::cout) {
	os << "<" << (name_ ? name_ : "")
	   << ":" << code_ << ">";
  }
};

class CodeEqual : public std::unary_function<CodedObject, bool>
{
private:

  int code_;

public:

  explicit CodeEqual(const int c) : code_(c) { }
  bool operator()(const CodedObject &c) const {
	return (c.code() == code_);
  }
  bool operator()(const CodedObject *c) const {
	return (c->code() == code_);
  }
};

#endif
