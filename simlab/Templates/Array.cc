//-*-c++-*------------------------------------------------------
// NAME: Template Classes
// NOTE: An array class
// AUTH: Qi Yang
// FILE: Array.h
// DATE: Thu Sep 21 21:35:59 1995
//--------------------------------------------------------------

#ifndef ARRAY_IMPLEMENTATION
#define ARRAY_IMPLEMENTATION

#include "Array.h"

#include <new>
#include <cstring>
using namespace std;

//--------------------------------------------------------------------
// Requires: sz >= 0
// Modifies: elements_ and nElements_
// Effects : creates sz new elements of class T.
//--------------------------------------------------------------------

template <class T>
Array<T>::Array(int sz)
   : PtrArray<T>(sz),
     objects_(sz > 0 ? new T[sz] : 0)
{
   for (int i = 0; i < sz; i ++) {
      setElement(i, objects_ + i);
   }
}


//--------------------------------------------------------------------
// Requires: s is an array of sz elements of class T
// Modifies: objects, elements_ and nElements_
// Effects : objects_ points to the array s
//--------------------------------------------------------------------

template <class T>
Array<T>::Array(T* s, int sz)
   : PtrArray<T>(sz), objects_(s)
{
   this->nElements_ = sz;
   for (int i = 0; i < sz; i ++) {
      setElement(i, objects_ + i);
   }
}


template <class T>
T* Array<T>::removeElementByIndex(int index)
{
   if (index < 0 || index >= this->nElements_) return NULL;
   register int i;
   T *elem = this->elements_[index];
   for (i = index + 1; i < this->nElements_; i ++) {
      this->elements_[i - 1] = this->elements_[i];
      if (this->elements_[i]->need_deep_copy()) {
		 this->elements_[i - 1]->deep_copy(*this->elements_[i]);
      }
   }
   this->nElements_ --;
   return elem;
}

//--------------------------------------------------------------------
// Requires: sz > 0 and class T has assignment operator "=" defined
// Modifies: elements_ and nElements_
// Effects : Creates a new array whose length increates by n.  The
//           first min(n, nElements) items in existing array is
//           copied.  This fn returns the number of elements in the
//           array.  Since the old array is freed, all references to
//           previous array element will be invalid.
//--------------------------------------------------------------------
 
template <class T>
int Array<T>::resize(int sz)
{
   register int i;
   this->nElements_ = Min(sz, this->nElements_);
   if (sz == this->nMaxElements_) {	// nothing needs to be done
      return (sz);
   } else if (!sz) {		// new size is 0
      delete [] objects_;
      delete [] this->elements_;
      this->elements_ = NULL;
      objects_ = NULL;
      return 0;
   } else if (objects_) {
      T* foo = new T[sz];
      memcpy(foo, objects_, this->nElements_ * sizeof(T));
      if (objects_->need_deep_copy()) {
		 for (i = 0; i < this->nElements_; i ++) {
			foo[i].deep_copy(objects_[i]);
		 }
      }
      delete [] objects_;
	  objects_ = foo;
   } else {			// array is currently empty
      objects_ = new T[sz];
   }
   if (this->elements_) {
	  delete [] this->elements_;
   }
   this->elements_ = new T* [sz];
   this->nMaxElements_ = sz;

   // Copy pointers of the original elements

   for (i = 0; i < sz; i ++) {
      setElement(i, objects_ + i);
   }

   return (sz);
}


//--------------------------------------------------------------------
// Requires: No dangling reference to current array. gz > -nElements.
// Modifies: objects_, elements_ and nElements_
// Effects : Creates a new array whose length increates by gz.  The
//           content in existing array is copied.  If gz is 0, the
//           default ARRAY_GROW_SIZE is used instead.  This fn returns
//           the number of elements in the array.
//--------------------------------------------------------------------

template <class T>
int Array<T>::grow(int gz)
{
   if (gz == 0) gz = ARRAY_GROW_SIZE;
   return (resize(this->nMaxElements_ + gz));
}

#endif // ARRAY_IMPLEMENTATION
