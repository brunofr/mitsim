//-*-c++-*------------------------------------------------------------
// NAME: Template Classes
// NOTE: A queue class.
// AUTH: Qi Yang
// FILE: Queue.h
// DATE: Thu Sep 21 21:42:52 1995
//--------------------------------------------------------------------

#ifndef QUEUE_HEADER
#define QUEUE_HEADER

//#include <iostream>
#include "Object.h"

template <class T> class Queue;

//--------------------------------------------------------------------
// CLASS NAME: QueueItem
//--------------------------------------------------------------------

template <class T> class QueueItem {

protected:
  T * item_;					// item data
  QueueItem<T> * prev_;			// prev item in queue
  QueueItem<T> * next_;			// next item in queue

public:

  QueueItem(T *i) : prev_(NULL), next_(NULL) { item_ = i; }
  virtual ~QueueItem() { }

  inline QueueItem<T> * prev() { return prev_; }
  inline QueueItem<T> * next() { return next_; }

  T * item() { return item_; }

  friend class Queue<T>;
};


//--------------------------------------------------------------------
// CLASS NAME: Queue
// A queue class.  Items are always appended to the end of the queue.
// Queue items can be either removed from beginning of the queue
// (FIFO) or anywhere in the queue by specifying pointer to the item.
//--------------------------------------------------------------------

template <class T> class Queue {

private:

  QueueItem<T> * head_;		// first item in queue
  QueueItem<T> * tail_;		// last item in queue
  int nElements_;				// number of elements in queue

public:

  Queue() : head_(NULL), tail_(NULL), nElements_(0) { }
  virtual ~Queue();

  inline QueueItem<T> * head() { return head_; }
  inline QueueItem<T> * tail() { return tail_; }
  inline int nElements() { return nElements_; }

  QueueItem<T> * find(T *);
  QueueItem<T> * find(int t);
  void append(QueueItem<T> *);
  void remove(QueueItem<T> *);
  QueueItem<T> * append(T *);
  QueueItem<T> * remove(T *);
  QueueItem<T> * remove();

};

#include "Queue.cc"

#endif
