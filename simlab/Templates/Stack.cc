//-*-c++-*------------------------------------------------------------
// NAME: Template Classes
// NOTE: A generic list class
// AUTH: Qi Yang
// FILE: Stack.cc
// DATE: Thu Sep 21 21:42:06 1995
//--------------------------------------------------------------------

#ifndef STACK_IMPLEMENTATION
#define STACK_IMPLEMENTATION

#include "Stack.h"

#include <stdlib.h>
#include <iostream.h>


//--------------------------------------------------------------------
// Requires: No dangling pointers to items in the stack
// Modifies: all handles of the stack
// Effects : Release all items in the stack
//--------------------------------------------------------------------

template <class T>
Stack<T>::~Stack()
{
  while (head_) {
    tail_ = head_;
    head_ = head_->next_;
    delete tail_;
  }
  tail_ = NULL;					// is this necessary?
  nElements_ = 0;
}


template <class T>
StackItem<T> * Stack<T>::find(T *i)
{
  StackItem<T> *c = head_;
  while (c && c->item_ != i) {
	c = c->next_;
  }
  return (c);
}

//--------------------------------------------------------------------
// Requires: Items in the stack are sorted by key.
// Modifies: Item's pointers and head_, tail_ and elements counter
// Effects : Append item to the top of the stach
//--------------------------------------------------------------------

template <class T>
void Stack<T>::push(StackItem<T> *i)
{
  i->prev_ = NULL;
  i->next_ = head_;
  
  if (head_) {					// append on top
	head_->prev_ = i;
  } else {						// stack is empty
	tail_ = i;
  }
  head_ = i;
  nElements_ ++;
}

template <class T>
StackItem<T> * Stack<T>::push(T *i)
{
  StackItem<T> *item = new StackItem<T> (i);
  push(item);
  return (item);
}


//--------------------------------------------------------------------
// Requires: Items in the stack is sorted and item must be an element 
//           in the stack.  
// Modifies: Item's pointers and head_, tail_ and elements counter
// Effects : Removes item from the stack.
//--------------------------------------------------------------------

template <class T>
void Stack<T>::remove(StackItem<T> *item)
{
  if (item->prev_) {
    item->prev_->next_ = item->next_;
  } else {						// head
    head_ = item->next_;
  }
  if (item->next_) {
    item->next_->prev_ = item->prev_;
  } else {						// tail
    tail_ = item->prev_;
  }
  nElements_ --;
}

template <class T>
StackItem<T> * Stack<T>::remove(T *i)
{
  StackItem<T> *item = find(i);
  if (item) remove(item);
  return item;
}

template <class T>
StackItem<T> * Stack<T>::pop()
{
  if (head_) remove(head_);
  return head_;
}

#endif // STACK_IMPLEMENTATION
