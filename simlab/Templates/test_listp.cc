// Testing a sorted list. Arguments passed as pointers.

#include "Listp.h"

#include <iostream.h>
#include <string.h>
#include <string>

class Foo {
public:
  Foo(const char *name_, int key_) : _name(name_), _key(key_) { }
  ~Foo() { }

  void print(ostream &os = cout) {
	os << "  " << _key << " " << _name << endl;
  }

  int cmp(const Foo* a) const {
	return strcmp(_name.c_str(), a->_name.c_str());
  }

  int cmp(int c) const {
	if (_key < c) return -1;
	else if (_key > c) return 1;
	else return 0;
  }

  bool eq(const Foo* a) const {
	return (this == a);
  }

private:

  string _name;
  int _key;
};

void print(const char* msg, Listp<Foo*> &a, ostream &os = cout = cout)
{
  os << msg << ": " << a.nNodes() << endl;
  Listp<Foo*>::iterator i = a.begin();
  while (i != a.end()) {
	(*i)->print(os);
	i ++;
  }
}

void main()
{
  Foo x1("Dave",	3);
  Foo x2("Masroor", 2);
  Foo x3("Yang Qi",	2);
  Foo x4("Didier",	5);
  Foo x5("Michel",	4);
  Foo x6("Owen",	0);

  Listp<Foo*> a;
  a.insert(&x1);
  a.insert(&x2);
  a.insert(&x3);
  a.insert(&x4);
  a.insert(&x5);
  print("Original", a);

  a.insert(&x6);
  print("Owen added", a);

  a.remove(&x3);
  print("Qi removed", a);
}
