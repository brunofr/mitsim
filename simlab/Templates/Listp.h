//-*-c++-*------------------------------------------------------------
// Listp.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// A sorted list of type T.  T is pointer type to a class which should
// have following methods defined:
//
// A Sorting Criteria: int cmp(T b) const and int cmp(int b) const
// compares self with b and returns:
//
//   -1(<b), 0(==b), 1(>b) for ascending order or
//   1(<b), 0(==b), -1(>b) for descending order
//
// and
//
// An Identification Criteria: bool eq(T b) compares self with b and
// returns true if self==b or false otherwise.
//
//--------------------------------------------------------------------

#ifndef LISTP_HEADER
#define LISTP_HEADER

#include <UTL/bool.h>

template <class T> class Listp;

//--------------------------------------------------------------------
// CLASS NAME: ListpNode
//--------------------------------------------------------------------

template <class T> class ListpNode
{
  friend class Listp<T>;

public:

  typedef ListpNode<T> node_type;
  typedef node_type* link_type;

private:

  T _data;						// data of the node
  ListpNode<T>* _prev;				// previous node in list
  ListpNode<T>* _next;				// next node in list

public:

  ListpNode(T d_) : _next(0), _prev(0), _data(d_) { }

  ~ListpNode() { }

  T data() { return _data; }
  T operator *() { return _data; }
  T operator ->() { return _data; }
  
  ListpNode<T>* next() { return _next; }
  ListpNode<T>* prev() { return _prev; }
};


//--------------------------------------------------------------------
// CLASS NAME: ListpIterator
//--------------------------------------------------------------------

template <class T> class ListpIterator
{
public:

  typedef ListpNode<T> node_type;
  typedef node_type* link_type;
  typedef ListpIterator<T> iterator;
  typedef Listp<T> list_type;

private:

  ListpNode<T>* _node;
    
public:

  ListpIterator() : _node(0) { }
  ListpIterator(ListpNode<T>* x) : _node(x) { }
  ListpIterator(const iterator &i) : _node(i._node) { }
  ListpIterator(const list_type &l) : _node(l.head()) { }
  ListpIterator(list_type *p) : _node(p->head()) { }
  ~ListpIterator() { }
    
  bool operator ==(const iterator& i) const {
	return _node == i._node;
  }
  bool operator !=(const iterator& i) const {
	return _node != i._node;
  }

  T operator*() const { return _node->data(); }

  ListpNode<T>* node() const { return _node; }
  ListpNode<T>* prev() const { return (_node ? _node->prev() : 0); }
  ListpNode<T>* next() const { return (_node ? _node->next() : 0); }

  // Operators for moving current item pointer

  iterator& operator ++() {		// next node
	_node = _node->next();
    return *this;
  }
  iterator operator ++(int) {	// postfix
	iterator i = *this;
    _node = _node->next();
    return i;
  }
  iterator& operator --() {		// previous node
	_node = _node->prev();
    return *this;
  }
  iterator operator --(int) {	// postfix
	iterator i = *this;
    _node = _node->prev();
    return i;
  }
};


//--------------------------------------------------------------------
// CLASS NAME: Listp
// A sorted list class. Nodes are sorted according to a static
// function cmp() passed as an argument to the constructor
//--------------------------------------------------------------------

template <class T> class Listp
{
public:

  typedef ListpNode<T> node_type;
  typedef node_type* link_type;
  typedef ListpIterator<T> iterator;

public:

  Listp() : _head(0), _tail(0), _nNodes(0) { }
  ~Listp() { remove(); }

private:

  ListpNode<T>* _head;				// first node in list
  ListpNode<T>* _tail;				// last node in list
  int _nNodes;					// number of nodes in list

public:

  ListpNode<T>* head() const { return _head; }
  ListpNode<T>* tail() const { return _tail; }
  ListpNode<T>* begin() const { return _head; }
  ListpNode<T>* end() const { return 0; }

  int nNodes() const { return _nNodes; }

  void remove();
  ListpNode<T>* cmp_find_before(T b) const;
  ListpNode<T>* cmp_find_after(T b) const;
  ListpNode<T>* cmp_find(T b) const;
  ListpNode<T>* eq_find(T b) const;
  ListpNode<T>* find(int c) const;
  ListpNode<T>* insert(T d);
  ListpNode<T>* remove(T d);
  ListpNode<T>* remove(int c);
  ListpNode<T>* append(T d);
  void insert(ListpNode<T>* i);
  void append(ListpNode<T>* i);
  void remove(ListpNode<T>* i);
  void resort(ListpNode<T>* i);
};

// ================ I M P L E M E N T A T I O N ======================
  
//--------------------------------------------------------------------
// Requires: None
// Modifies: Data member _head, _tail, and _nNodes
// Effects : Remove all list nodes
//--------------------------------------------------------------------

template <class T>
void Listp<T>::remove() {
  while (_head) {
	_tail = _head;
	_head = _head->_next;
	delete _tail;
  }
  _tail = 0;
  _nNodes = 0;
}

//--------------------------------------------------------------------
// Requires: Items in the list are sorted
// Modifies: None
// Effects : Returns a pointer to the item whose position is before
//           (or same as) b, or 0 if no such item is found.
//--------------------------------------------------------------------
 
// For sorting

template <class T>
ListpNode<T>* Listp<T>::cmp_find_before(T b) const {
  if (!_tail) return 0;	// list is empty
  ListpNode<T>* a = _tail;
  while (a && a->data()->cmp(b) > 0) {
	a = a->_prev;
  }
  return a;
}

//--------------------------------------------------------------------
// Requires: Items in the list are sorted
// Modifies: None
// Effects : Returns a pointer to the item whose position is after (or 
//           same as) b, or 0 if no such item is found.
//--------------------------------------------------------------------

// For sorting

template <class T>
ListpNode<T>* Listp<T>::cmp_find_after(T b) const {
  if (!_head) return 0;	// list is empty
  ListpNode<T>* a = _head;
  while (a && a->data()->cmp(b) < 0) {
	a = a->_next;
  }
  return a;
}

//--------------------------------------------------------------------
// Requires: None
// Modifies: None
// Effects : Find an item and returns pointer to the ListpNode.  It
//           returns 0 if the item is not found in the list.
//--------------------------------------------------------------------

// For sorting

template <class T>
ListpNode<T>* Listp<T>::cmp_find(T b) const {
  ListpNode<T>* a = _head;
  while (a && a->data()->cmp(b) != 0) {
	a = a->_next;
  }
  return a;
}

// For identification

template <class T>
ListpNode<T>* Listp<T>::eq_find(T b) const {
  ListpNode<T>* a = _head;
  while (a && !a->data()->eq(b)) {
	a = a->_next;
  }
  return a;
}

// For search and identification

template <class T>
ListpNode<T>* Listp<T>::find(int c) const {
  ListpNode<T>* a = _head;
  while (a && a->data()->cmp(c)) {
	a = a->_next;
  }
  return a;
}

//--------------------------------------------------------------------

template <class T>
ListpNode<T>* Listp<T>::insert(T d) {
  ListpNode<T>* i = new node_type(d);
  insert(i);
  return i;
}

//--------------------------------------------------------------------

template <class T>
ListpNode<T>* Listp<T>::remove(T d) {
  ListpNode<T>* i = eq_find(d);
  if (i) remove(i);
  return i;
}

template <class T>
ListpNode<T>* Listp<T>::remove(int c) {
  ListpNode<T>* i = find(c);
  if (i) remove(i);
  return i;
}

//--------------------------------------------------------------------

template <class T>
ListpNode<T>* Listp<T>::append(T d) {
  ListpNode<T>* i = new node_type(d);
  i->_prev = _tail;
  i->_next = 0;
  
  if (_tail) {					// append at end
	_tail->_next = i;
  } else {						// queue is empty
	_head = i;
  }
  _tail = i;
  _nNodes ++;
  return i;
}

//--------------------------------------------------------------------
// Requires: Items in the list are sorted
// Modifies: Item's pointers and head_, tail_ and elements counter
// Effects : insert an item into the list
//--------------------------------------------------------------------

template <class T>
void Listp<T>::insert(ListpNode<T>* i) {
  i->_prev = cmp_find_before(i->_data);
  if (i->_prev) {		// insert after
	i->_next = i->_prev->_next;
	i->_prev->_next = i;
  } else {			// insert at beginning
	i->_next = _head;
	_head = i;
  }
  if (i->_next) {		// not at the end
	i->_next->_prev = i;
  } else {			// inserted at the end
	_tail = i;
  }
  _nNodes ++;
}


//--------------------------------------------------------------------
// Requires: None
// Modifies: Item's pointers and head_, tail_ and elements counter
// Effects : Add a node at the end
//--------------------------------------------------------------------

template <class T>
void Listp<T>::append(ListpNode<T>* i) {
  i->_prev = _tail;
  i->_next = 0;
  if (_tail) {
	_tail->_next = i;
  } else {        // empty List
	_head = i;
  }
  _tail = i;
  _nNodes ++;
}

//--------------------------------------------------------------------

template <class T>
void Listp<T>::resort(ListpNode<T>* i) {
  remove(i);
  insert(i);
}

//--------------------------------------------------------------------
// Requires: Items in the list is sorted.
// Modifies: Item's pointers and head_, tail_ and elements counter
// Effects : Removes a item from the list.
//--------------------------------------------------------------------

template <class T>
void Listp<T>::remove(ListpNode<T>* i) {
  if (i->_prev) {
	i->_prev->_next = i->_next;
  } else {			// head
	_head = i->_next;
  }
  if (i->_next) {
	i->_next->_prev = i->_prev;
  } else {			// tail
	_tail = i->_prev;
  }
  _nNodes --;
}

#endif // LIST_HEADER
