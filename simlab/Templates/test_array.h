//-*-c++-*------------------------------------------------------------
// NAME: Template classes
// NOTE: 
// AUTH: Yang, Qi
// FILE: test_array.h
// DATE: Tue Sep 26 21:25:04 1995
//--------------------------------------------------------------------

#ifndef ARRAY_TEST_HEADER
#define ARRAY_TEST_HEADER

#include <iostream.h>
#include <string.h>
#include "Object.h"

class Foo : public NamedObject
{
public:
  Foo() : NamedObject() { }
  ~Foo() { }

  int comp(Foo *i) { return strcmp(name_,i->name_); }
  int comp(int c) { return NamedObject::comp(c); }

  void print(ostream &os = cout) {
	os << name_ << ":" << code_ << endl;
  }

};


#endif
