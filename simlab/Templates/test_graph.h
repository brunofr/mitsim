//-*-c++-*------------------------------------------------------------
// NAME: Template classes
// NOTE: 
// AUTH: Yang, Qi
// FILE: test_graph.h
// DATE: Tue Sep 26 21:25:04 1995
//--------------------------------------------------------------------

#ifndef GRAPH_TEST_HEADER
#define GRAPH_TEST_HEADER

#include <iostream.h>
#include <string.h>
#include "Object.h"

class FooNode : public Object
{
public:
  FooNode() : Object() { }
  ~FooNode() { }
};

class FooLink : public Object
{
public:
  FooLink() : Object() { }
  ~FooLink() { }
};


#endif
