//-*-c++-*------------------------------------------------------------
// Listv.h
//
// Qi Yang
// Copyright (C) 1997
// Massachusetts Institue of Technology
// All Rights Reserved
//
// A sorted list of type T.  T is a class which should have the
// following methods defined:
//
// A Sorting Criteria: int cmp(T& b) const and int cmp(int b) const
// compares self with b and returns:
//
//   -1(<b), 0(==b), 1(>b) for ascending order or
//   1(<b), 0(==b), -1(>b) for descending order
//
// and
//
// An Identification Criteria: bool eq(T& b) const compares self with
// b and returns true if self==b or false otherwise.
//
//--------------------------------------------------------------------

#ifndef LISTV_HEADER
#define LISTV_HEADER

template <class T> class Listv;

//--------------------------------------------------------------------
// CLASS NAME: ListvNode
//--------------------------------------------------------------------

template <class T> class ListvNode
{
  friend class Listv<T>;

public:

  typedef ListvNode<T> node_type;
  typedef node_type* link_type;

private:

  T _data;						// data of the node
  link_type _prev;				// previous node in list
  link_type _next;				// next node in list

public:

  ListvNode(T& d_) : _next(0), _prev(0), _data(d_) { }

  ~ListvNode() { }

  T& data() { return _data; }
  T& operator *() { return _data; }
  
  link_type next() { return _next; }
  link_type prev() { return _prev; }
};


//--------------------------------------------------------------------
// CLASS NAME: ListvIterator
//--------------------------------------------------------------------

template <class T> class ListvIterator
{
public:

  typedef ListvNode<T> node_type;
  typedef node_type* link_type;
  typedef ListvIterator<T> iterator;
  typedef Listv<T> list_type;

private:

  link_type _node;
    
public:

  ListvIterator() : _node(0) { }
  ListvIterator(link_type x) : _node(x) { }
  ListvIterator(const iterator &i) : _node(i._node) { }
  ListvIterator(const list_type &l) : _node(l.head()) { }
  ListvIterator(list_type *p) : _node(p->head()) { }
  ~ListvIterator() { }
    
  bool operator ==(const iterator& i) const {
	return _node == i._node;
  }
  bool operator !=(const iterator& i) const {
	return _node != i._node;
  }

  T& operator*() const { return _node->data(); }

  link_type node() const { return _node; }
  link_type prev() const { return (_node ? _node->prev() : 0); }
  link_type next() const { return (_node ? _node->next() : 0); }

  // Operators for moving current item pointer

  iterator& operator ++() {		// next node
	_node = _node->next();
    return *this;
  }
  iterator operator ++(int) {	// postfix
	iterator i = *this;
    _node = _node->next();
    return i;
  }
  iterator& operator --() {		// previous node
	_node = _node->prev();
    return *this;
  }
  iterator operator --(int) {	// postfix
	iterator i = *this;
    _node = _node->prev();
    return i;
  }
};


//--------------------------------------------------------------------
// CLASS NAME: Listv
// A sorted list class. Nodes are sorted according to a static
// function cmp() passed as an argument to the constructor
//--------------------------------------------------------------------

template <class T> class Listv
{
public:

  typedef ListvNode<T> node_type;
  typedef node_type* link_type;
  typedef ListvIterator<T> iterator;

public:

  Listv() : _head(0), _tail(0), _nNodes(0) { }
  ~Listv() { remove(); }

private:

  ListvNode<T>* _head;				// first node in list
  ListvNode<T>* _tail;				// last node in list
  int _nNodes;					// number of nodes in list

public:

  ListvNode<T>* head() const { return _head; }
  ListvNode<T>* tail() const { return _tail; }
  ListvNode<T>* begin() const { return _head; }
  ListvNode<T>* end() const { return 0; }

  int nNodes() { return _nNodes; }

  ListvNode<T>* cmp_find_before(T& b) const;
  ListvNode<T>* cmp_find_after(T& b) const;
  ListvNode<T>* cmp_find(T& b) const;
  ListvNode<T>* eq_find(T& b) const;
  ListvNode<T>* find(int c) const;
  ListvNode<T>* insert(T& d);
  ListvNode<T>* remove(T& d);
  ListvNode<T>* remove(int c);
  ListvNode<T>* append(T& d);
  void insert(ListvNode<T>* i);
  void append(ListvNode<T>* i);
  void remove(ListvNode<T>* i);
  void resort(ListvNode<T>* i);

  void remove();
};

// ================ I M P L E M E N T A T I O N ======================

//--------------------------------------------------------------------
// Requires: Items in the list are sorted
// Modifies: None
// Effects : Returns a pointer to the item whose position is before
//           (or same as) b, or 0 if no such item is found.
//--------------------------------------------------------------------
  
template <class T>
ListvNode<T>* Listv<T>::cmp_find_before(T& b) const {
  if (!_tail) return 0;	// list is empty
  ListvNode<T>* a = _tail;
  while (a && a->data().cmp(b) > 0) {
	a = a->_prev;
  }
  return a;
}

//--------------------------------------------------------------------
// Requires: Items in the list are sorted
// Modifies: None
// Effects : Returns a pointer to the item whose position is after (or 
//           same as) b, or 0 if no such item is found.
//--------------------------------------------------------------------

template <class T>
ListvNode<T>* Listv<T>::cmp_find_after(T& b) const { // for sorting
  if (!_head) return 0;	// list is empty
  ListvNode<T>* a = _head;
  while (a && a->data().cmp(b) < 0) {
	a = a->_next;
  }
  return a;
}

//--------------------------------------------------------------------
// Requires: None
// Modifies: None
// Effects : Find an item and returns pointer to the ListvItem.  It
//           returns 0 if the item is not found in the list.
//--------------------------------------------------------------------

// For sorting

template <class T>
ListvNode<T>* Listv<T>::cmp_find(T& b) const {
  ListvNode<T>* a = _head;
  while (a && a->data()->cmp(b) != 0) {
	a = a->_next;
  }
  return a;
}

// For identification

template <class T>
ListvNode<T>* Listv<T>::eq_find(T& b) const {
  ListvNode<T>* a = _head;
  while (a && !a->data().eq(b)) {
	a = a->_next;
  }
  return a;
}

// For search and identification

template <class T>
ListvNode<T>* Listv<T>::find(int c) const {
  ListvNode<T>* a = _head;
  while (a && a->data().cmp(c)) {
	a = a->_next;
  }
  return a;
}

//--------------------------------------------------------------------

template <class T>
ListvNode<T>* Listv<T>::insert(T& d) {
  ListvNode<T>* i = new node_type(d);
  insert(i);
  return i;
}

//--------------------------------------------------------------------

template <class T>
ListvNode<T>* Listv<T>::remove(T& d) {
  ListvNode<T>* i = eq_find(d);
  if (i) remove(i);
  return i;
}

template <class T>
ListvNode<T>* Listv<T>::remove(int c) {
  ListvNode<T>* i = find(c);
  if (i) remove(i);
  return i;
}

//--------------------------------------------------------------------

template <class T>
ListvNode<T>* Listv<T>::append(T& d) {
  ListvNode<T>* i = new node_type(d);
  i->_prev = _tail;
  i->_next = 0;
  
  if (_tail) {					// append at end
	_tail->_next = i;
  } else {						// queue is empty
	_head = i;
  }
  _tail = i;
  _nNodes ++;
  return i;
}

//--------------------------------------------------------------------
// Requires: Items in the list are sorted
// Modifies: Item's pointers and head_, tail_ and elements counter
// Effects : insert an item into the list
//--------------------------------------------------------------------

template <class T>
void Listv<T>::insert(ListvNode<T>* i) {
  i->_prev = cmp_find_before(i->_data);
  if (i->_prev) {		// insert after
	i->_next = i->_prev->_next;
	i->_prev->_next = i;
  } else {			// insert at beginning
	i->_next = _head;
	_head = i;
  }
  if (i->_next) {		// not at the end
	i->_next->_prev = i;
  } else {			// inserted at the end
	_tail = i;
  }
  _nNodes ++;
}


//--------------------------------------------------------------------
// Requires: None
// Modifies: Item's pointers and head_, tail_ and elements counter
// Effects : Add a node at the end
//--------------------------------------------------------------------

template <class T>
void Listv<T>::append(ListvNode<T>* i) {
  i->_prev = _tail;
  i->_next = 0;
  if (_tail) {
	_tail->_next = i;
  } else {        // empty List
	_head = i;
  }
  _tail = i;
  _nNode ++;
}


//--------------------------------------------------------------------

template <class T>
void Listv<T>::resort(ListvNode<T>* i) {
  remove(i);
  insert(i);
}

//--------------------------------------------------------------------
// Requires: Items in the list is sorted.
// Modifies: Item's pointers and head_, tail_ and elements counter
// Effects : Removes a item from the list.
//--------------------------------------------------------------------

template <class T>
void Listv<T>::remove(ListvNode<T>* i) {
  if (i->_prev) {
	i->_prev->_next = i->_next;
  } else {			// head
	_head = i->_next;
  }
  if (i->_next) {
	i->_next->_prev = i->_prev;
  } else {			// tail
	_tail = i->_prev;
  }
  _nNodes --;
}

//--------------------------------------------------------------------
// Requires: None
// Modifies: Data member _head, _tail, and _nNodes
// Effects : Remove all list nodes
//--------------------------------------------------------------------

template <class T>
void Listv<T>::remove() {
  while (_head) {
	_tail = _head;
	_head = _head->_next;
	delete _tail;
  }
  _tail = 0;
  _nNodes = 0;
}

#endif // LIST_HEADER
