//-*-c++-*------------------------------------------------------
// NAME: Template Classes
// NOTE: Testing template Queue and QueueItem.
// AUTH: Qi Yang
// FILE: test_queue.cc
// DATE: Thu Sep 21 22:00:04 1995
//--------------------------------------------------------------

#include "Queue.h"

#include <iostream.h>
#include <string.h>
#include <stdlib.h>

void print(Queue<NamedObject> &queue)
{
  QueueItem<NamedObject> *q = queue.head();
  cout << queue.nElements() << " itesm:" << endl;
  while (q) {
	q->item()->print();
	q = q->next();
  }
  cout << endl;
}

void main()
{
  Queue<NamedObject> queue;

  NamedObject *x1 = new NamedObject("1st", 3);
  NamedObject *x2 = new NamedObject("2nd", 2);
  NamedObject *x3 = new NamedObject("3rd", 2);
  NamedObject *x4 = new NamedObject("4th", 5);
  NamedObject *x5 = new NamedObject("5th", 4);
  NamedObject *x6 = new NamedObject("6th", 0);

  queue.append(x1);
  queue.append(x2);
  queue.append(x3);
  queue.append(x4);
  queue.append(x5);
  print(queue);

  queue.remove(x3);
  print(queue);

  queue.append(x6);
  print(queue);
}

