//-*-c++-*------------------------------------------------------
// NAME: Template Classes
// NOTE: Array of pointers
// AUTH: Qi Yang
// FILE: PtrArray.cc
// DATE: Thu Sep 21 21:35:59 1995
//--------------------------------------------------------------

#ifndef PTRARRAY_IMPLEMENTATION
#define PTRARRAY_IMPLEMENTATION

#include <new>
#include <cstring>
#include <cstdlib>

#include <IO/Exception.h>

#include "PtrArray.h"

//--------------------------------------------------------------------
// Requires: sz >= 0
// Modifies: elements_ and nElements_
// Effects : creates sz new elements of class T.
//--------------------------------------------------------------------

template <class T>
PtrArray<T>::PtrArray(int sz)
   : nMaxElements_(sz),
     nElements_(0),
     elements_(sz > 0 ? new T* [sz] : 0),
     order_(0)
{
   if (sz) memset(elements_, sizeof(T*) * sz, 0);
}


//--------------------------------------------------------------------
// Requires: 
// Modifies: value of last element. Array size may grow
// Effects : Add an element and returns the total number of elements
//           currently in the array.
//--------------------------------------------------------------------

template <class T>
int PtrArray<T>::addElement(T *e)
{
   if (nElements_ >= nMaxElements_) {
      grow();
   }
   elements_[nElements_] = e;
   return (++nElements_);
}


//--------------------------------------------------------------------
// Requires: Elements in the array are coded and has code() defined.
// Modifies: nElements_ and elements_[0, nElements-1]
// Effects : Remove an element from the array. Returns 0 if success
//           or -1 if fail (elements not found in teh array)
//--------------------------------------------------------------------

template <class T>
T* PtrArray<T>::removeElement(T *elem)
{
   int i = findIndex(elem);
   return removeElementByIndex(i);
}

template <class T>
T* PtrArray<T>::removeElementByCode(int c)
{
   int i = findIndex(c);
   return removeElementByIndex(i);
}

template <class T>
T* PtrArray<T>::removeElementByIndex(int index)
{
   if (index < 0 || index >= nElements_) return NULL;
   register int i;
   T *elem = elements_[index];
   for (i = index + 1; i < nElements_; i ++) {
      elements_[i - 1] = elements_[i];
   }
   nElements_ --;
   return elem;
}


template <class T>
void PtrArray<T>::setElement(int i, T* pe)
{
   if (i >= 0 && i < nMaxElements_) { 
	  if (i >= nElements_) nElements_ ++;
      elements_[i] = pe;
   } else {
      std::cerr << "Error:: array index <" << i << "> not in range [0,";
      std::cerr << nMaxElements_ << ").\n"; 
      theException->exit(1);
   }
}


//--------------------------------------------------------------------
// Requires: sz >= 0
// Modifies: elements_ and nElements_
// Effects : Creates a new array of pointers whose length increates
//           to sz.  The first min(n, nElements) pointers in existing
//           array is copied.
//--------------------------------------------------------------------

template <class T>
int PtrArray<T>::resize(int sz)
{
   if (sz == 0) {
	  nElements_ = 0;
	  if (elements_) delete [] elements_;
	  elements_ = NULL;
   } else {
	  T** foo = elements_;
	  elements_ = new T* [sz];
	  if (sz > nMaxElements_) {
		 memset(elements_+nMaxElements_, 0, (sz-nMaxElements_)*sizeof(T*));
	  } else if (sz < nMaxElements_) {
		 nElements_ = Min(nElements_, sz);
	  }
	  if (foo) {
		 int n = Min(nMaxElements_, sz);
		 memcpy(elements_, foo, n * sizeof(T*));
		 delete [] foo;
	  }
   }

   nMaxElements_ = sz;
   return (sz);
}


//--------------------------------------------------------------------
// Requires: gz <= nElements_
// Modifies: elements_ and nElements_
// Effects : Creates a new array whose length increates by n.  The
//           content in existing array is copied.  If gz is 0, the
//           default grow_size is used instead.  This fn returns
//           the maximum number of elements in the array.
//--------------------------------------------------------------------

template <class T>
int PtrArray<T>::grow(int gz)
{
   if (gz == 0) gz = ARRAY_GROW_SIZE;
   return (resize(nMaxElements_ + gz));
}


//--------------------------------------------------------------------
// Requires: 
// Modifies: None
// Effects : returns pointer to the element whose code is c or NULL if
//           no element with code c is found in the array
//--------------------------------------------------------------------

template <class T>
T* PtrArray<T>::find(int c)
{
   int i = findIndex(c);
   if (i >= 0 && i < nElements_) return elements_[i];
   else return NULL;
}

//--------------------------------------------------------------------
// Requires: 
// Modifies: None
// Effects : returns index in array or -1 if the pointer is not found
//           in the array
//--------------------------------------------------------------------

template <class T>
int PtrArray<T>::findIndex(T *elem)
{
   if (order_) return (findIndexSorted(elem));
   else return (findIndexUnSorted(elem));
}


//--------------------------------------------------------------------
// Requires: class T has fn comp(int code) defined
// Modifies: None
// Effects : returns index of the element or -1 if no element with
//           code c is found in the array.
//--------------------------------------------------------------------

template <class T>
int PtrArray<T>::findIndex(int c)
{
   if (order_) return (findIndexSorted(c));
   else return (findIndexUnSorted(c));
}


//--------------------------------------------------------------------
// Requires: (1) class T has fn comp(int code) defined
//           (2) all elements are sorted in ascending order
// Modifies: None
// Effects : returns index of the element or -1 if no element with
//           code c is found in the array
//--------------------------------------------------------------------

template <class T>
int PtrArray<T>::findIndexSorted(int c)
{
   int l = 0;
   int r = nElements_ - 1;
   int i, cmp;

   while (l <= r) {
      i = (l + r) / 2;
      cmp = elements_[i]->comp(c);
      if (cmp < 0) {
		 l = i + 1;
      } else if (cmp > 0) {
		 r = i - 1;
      } else {
		 return i;
      }
   }
   return -1;
}


//--------------------------------------------------------------------
// Requires: (1) class T has fn comp(T *) defined
//           (2) all elements are sorted in ascending order
// Modifies: None
// Effects : returns index of the element or -1 if the pointer is not
//           found in the array
//--------------------------------------------------------------------

template <class T>
int PtrArray<T>::findIndexSorted(T *elem)
{
   int l = 0;
   int r = nElements_ - 1;
   int i, cmp;

   while (l <= r) {
      i = (l + r) / 2;
      cmp = elements_[i]->comp(elem);
      if (cmp < 0) {
		 l = i + 1;
      } else if (cmp > 0) {
		 r = i - 1;
      } else {
		 return i;
      }
   }
   return -1;
}

//--------------------------------------------------------------------
// Requires: class T has fn comp(int code) defined.
// Modifies: None
// Effects : returns index of the element or -1 if no element with
//           code c is found in the array
//--------------------------------------------------------------------

template <class T>
int PtrArray<T>::findIndexUnSorted(int c)
{
   register int i;
   for (i = nElements_ - 1; i >= 0; i --) {
      if (elements_[i]->comp(c) == 0) break;
   }
   return i;
}


//--------------------------------------------------------------------
// Requires: class T has fn comp(T *) defined.
// Modifies: None
// Effects : returns index of the element or -1 if the pointer is not
//           found in the array
//--------------------------------------------------------------------

template <class T>
int PtrArray<T>::findIndexUnSorted(T *elem)
{
   register int i;
   for (i = nElements_ - 1; i >= 0; i --) {
      if (elements_[i]->comp(elem) == 0) break;
   }
   return i;
}


//--------------------------------------------------------------------
// Requires: class T has fn comp(T *) defined
// Modifies: None
// Effects : returns -1 if a < b, 0 if a == b, and 1 if a > b.
//--------------------------------------------------------------------

template <class T>
int PtrArray<T>::comp(T *a, T *b)
{
   return  a->comp(b) * order_;
}


template <class T>
int PtrArray<T>::find_by_op_equal(T *b)
{
   register int i;
   for (i = nElements_ - 1; i >= 0; i --) {
      if (*(elements_[i]) == (*b)) break;
   }
   return i;
}


//--------------------------------------------------------------------
// Requires: r != 0
// Modifies: order of the elements in the array
// Effects : Sorting elements using qsort.
//--------------------------------------------------------------------

template <class T>
void PtrArray<T>::sort(int r)
{
   if (r < 0) order_ = -1;
   else if (r > 0) order_ = 1;
   else {
      std::cerr << "Error:: Order not specified. ";
      std::cerr << "Array sorting ignored.\n";
      return; 
   }
   quick_sort(0, nElements_-1);
}


//--------------------------------------------------------------------
// Requires: p < r
// Modifies: order of the elements[p..r] in the array
// Effects : This is the quick sort algorithm
//--------------------------------------------------------------------

template <class T>
void PtrArray<T>::quick_sort(int p, int r)
{
   if (p >= r) return;
   T *x = elements_[r];
   register int i = p - 1;
   register int j = r;
   while (1) {
      while (i < r && comp(elements_[++i], x) < 0) ;
      while (j > 0 && comp(elements_[--j], x) > 0) ;
      if (i >= j) break;
      swap(i, j);
   }
   swap(i, r);
   quick_sort(p, i-1);
   quick_sort(i+1, r);
}

template <class T>
void PtrArray<T>::swap(int p, int r)
{
   T *tmp = elements_[p];
   elements_[p] = elements_[r];
   elements_[r] = tmp;
}

#endif // PTRARRAY_IMPLEMENTATION
