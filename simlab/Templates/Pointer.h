//-*-c++-*------------------------------------------------------------
//
// File name : Pointer.h
// Author :    Michel Bierlaire
// Date :      Tue Aug  6 09:41:21 1996
//
// Objectives: enables an easy use of pointers with STL containers
//
// Modification history:
//
// Date            Author         Description
// ======          ======         ============
// Aug 12, 1996    Qi Yang        Modified from Michel's dtaPointer
//
//--------------------------------------------------------------------
//
// Sources: Mumit's STL Newbie guide
//   http://www.xraylith.wisc.edu/~khan/software/stl/STL.newbie.html
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#ifndef Pointer_h
#define Pointer_h

template <class T>
class Pointer
{
   public:

      Pointer(T* p = 0) : p_(p) { }
      Pointer(const Pointer& p) : p_(p.p_) { }
      ~Pointer() { }

      Pointer& operator = (const Pointer& p) {
		 p_ = p.p_;
		 return *this;
	  }
      
      // The object Pointer can be dereferenced using the usual * and
      // -> operators:

      T& operator *() { return *p_ ; }
      const T* operator ->() const { return p_ ; }
      T* operator ->() { return p_ ; }
      T* operator()() const { return p_; }
      T* p() const { return p_; }

   private:

      T* p_;
};

template <class T>
int operator == (const Pointer<T>& p1, const Pointer<T>& p2) {
   return (p1() && p2()) ? (*p1() == *p2()) : false;
}

template <class T>
int operator < (const Pointer<T>& p1, const Pointer<T>& p2) {
   return (p1() && p2()) ? (*p1() < *p2()) : false;
}

#endif // Pointer_h
