//-*-c++-*------------------------------------------------------------
// NAME: Template Classes
// NOTE: A stack class.
// AUTH: Qi Yang
// FILE: Stack.h
// DATE: Thu Sep 21 21:42:52 1995
//--------------------------------------------------------------------

#ifndef STACK_HEADER
#define STACK_HEADER

#include <iostream.h>
#include "Object.h"

template <class T> class Stack;

//--------------------------------------------------------------------
// CLASS NAME: StackItem
//--------------------------------------------------------------------

template <class T> class StackItem {

protected:
  T * item_;					// item data
  StackItem<T> * prev_;			// prev item in stack
  StackItem<T> * next_;			// next item in stack

public:

  StackItem(T *i) : prev_(NULL), next_(NULL) { item_ = i; }
  virtual ~StackItem() { }

  inline StackItem<T> * prev() { return prev_; }
  inline StackItem<T> * next() { return next_; }

  T * item() { return item_; }

  friend class Stack<T>;
};


//--------------------------------------------------------------------
// CLASS NAME: Stack
// A stack class.  Items are always appended to the top of the stack.
// Stack items can be either removed from beginning of the stack
// (FIFO) or anywhere in the stack by specifying pointer to the item.
//--------------------------------------------------------------------

template <class T> class Stack {

private:

  StackItem<T> * head_;			// first item in stack
  StackItem<T> * tail_;			// last item in stack
  int nElements_;				// number of elements in stack

public:

  Stack() : head_(NULL), tail_(NULL), nElements_(0) { }
  virtual ~Stack();

  inline StackItem<T> * head() { return head_; }
  inline StackItem<T> * tail() { return tail_; }
  inline int nElements() { return nElements_; }

  StackItem<T> * find(T *);
  void push(StackItem<T> *);
  void remove(StackItem<T> *);
  StackItem<T> * push(T *);
  StackItem<T> * remove(T *);
  StackItem<T> * pop();

};

#include "Stack.cc"

#endif
