//-*-c++-*------------------------------------------------------
// NAME: Template Classes
// NOTE: Testing template Stack and StackItem.
// AUTH: Qi Yang
// FILE: test_stack.cc
// DATE: Thu Sep 21 22:00:04 1995
//--------------------------------------------------------------

#include "Stack.h"

#include <iostream.h>
#include <string.h>
#include <stdlib.h>

void print(Stack<NamedObject> &stack)
{
  StackItem<NamedObject> *q = stack.head();
  cout << stack.nElements() << " itesm:" << endl;
  while (q) {
	q->item()->print();
	q = q->next();
  }
  cout << endl;
}

void main()
{
  Stack<NamedObject> stack;

  NamedObject *x1 = new NamedObject("1st", 3);
  NamedObject *x2 = new NamedObject("2nd", 2);
  NamedObject *x3 = new NamedObject("3rd", 2);
  NamedObject *x4 = new NamedObject("4th", 5);
  NamedObject *x5 = new NamedObject("5th", 4);
  NamedObject *x6 = new NamedObject("6th", 0);

  stack.push(x1);
  stack.push(x2);
  stack.push(x3);
  stack.push(x4);
  stack.push(x5);
  print(stack);

  stack.remove(x3);
  print(stack);

  stack.push(x6);
  print(stack);
}

