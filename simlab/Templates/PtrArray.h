//-*-c++-*------------------------------------------------------------
// NAME: Template Classes
// NOTE: Array of pointers. T must be a derived class from Object,
//       Object, or NamedObject (see Object.h).
// AUTH: Qi Yang
// FILE: PtrArray.h
// DATE: Thu Sep 21 21:35:59 1995
//--------------------------------------------------------------------


#ifndef PTRARRAY_HEADER
#define PTRARRAY_HEADER

#include <new>
#include "Object.h"

const int ARRAY_GROW_SIZE = 4;	// default array grow size

template <class T> class PtrArray
{
   protected:

      int	nMaxElements_;	// maximum number of elements
      int	nElements_;	// number of elements
      T**	elements_;	// array of pointers
      int	order_;		// 1=ascending 0=unsorted -1=decending

   public:

      PtrArray(int sz = 0);
      virtual ~PtrArray() { delete [] elements_; }

      inline T& operator[](int i) { return *element(i); }
      inline T* operator()(int i) { return element(i); }

      T* first() { return element(0); }
      T* last() { return element(nElements_ - 1); }

      // Add an element and returns actual size (may not be the
      // maximum size)

      int addElement(T *pe);
      int addElement(T &e) { return addElement(&e); }

      // Remove an element from the array. Returns the element if
      // success or NULL if fail

      virtual T* removeElementByIndex(int index);
      virtual T* removeElementByCode(int code);
      virtual T* removeElement(T *);

      inline T* element(int i) {
		return (i >= 0 && i < nElements_) ? elements_[i] : (T*)NULL;
	  }
      inline int nElements() { return nElements_; }

      // Update an element. Requires 0 <= i < nElements_.

      void setElement(int i, T *pe);
      inline void setElement(int i, T &e) { setElement(i, &e); }

      virtual int resize(int);
      virtual int grow(int gz = 0);

      // This can be used to finalize the array size
      virtual int resize() { return resize(nElements_); }

      inline int order() { return order_; }
      inline void order(int s) { order_ = s; }
      void sort(int r = 1);

      T* find(int c);

      int findIndex(int c);
      int findIndexSorted(int c);
      int findIndexUnSorted(int c);

      int findIndex(T *);
      int findIndexSorted(T *);
      int findIndexUnSorted(T *);

      int find_by_op_equal(T *b);

   private:

      int comp(T *, T *);
      void quick_sort(int, int);
      void swap(int, int);
};

#include "PtrArray.cc"

#endif
