//-*-C++-*------------------------------------------------------------
// NAME: Template Classes
// NOTE: Binary data file. Store large array or multi-dimensional
//       matrix.
// AUTH: Yang, Qi
// FILE: DiskData.h
// DATE: Thu Sep 28 18:02:13 1995
//--------------------------------------------------------------------

#ifndef DISK_DATA_HEADER
#define DISK_DATA_HEADER

#include <cstdio>
#include <iostream>
#include <sys/types.h>

template <class T> class DiskData
{
   private:

      char * name_;				// file name
	  int nTags_;				// number of time tags
	  time_t *timeTags_;		// time tags
      int handle_;				// file descriptor
      long int start_;			// start position

      size_t nItems_;			// number of data items
      size_t bytes_;			// size of each data item
      size_t size_;				// size of data

      int ncols_;				// num of cols for printing

      int delete_when_done_;	// 1=delete

   public:

      DiskData();
      DiskData(const DiskData &);

      ~DiskData();

	  void keep(int flag = 0) { delete_when_done_ = flag; }

      DiskData<T> & operator = (const DiskData<T> &other);

      // Create a tempory file and fill the file with "data"

      void create(const char *name, T data, size_t size,
		  int ntags = 0, const time_t *tags = NULL,
		  int ncols = 8);

      // open the file and return 0 if the file has same time tag,
      // otherwise it returns -1 if error and 1 if time tags are not
      // the same.
      
      int open(const char *name, size_t nitems = 0,
	       int ntags = 0, const time_t *tags = NULL, 
	       int ncols = 8) ;

      // Remove the temporary file
      
      void remove();

      const char *name() const { return name_; }

	  int nTags() const { return nTags_; }
	  const time_t *timeTags() const { return timeTags_; }

      size_t size() const { return size_; }
	  size_t nItems() const { return nItems_; }

      // Return the ith data item

      inline T operator [] (size_t i) { return read(i); }
      T read(size_t i);

      // Write the data at the ith position

      void write(size_t i, T data);

      // Print all data

      void print(std::ostream &os = std::cout);
};

//#include "DiskData.cc"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <cstdlib>
#include <cstring>
#include <iostream>

#include <IO/Exception.h>
#include <Tools/ToolKit.h>
#include <UTL/Misc.h>
//using namespace std;

//#include "DiskData.h"

template <class T>
DiskData<T>::DiskData()
   : name_(NULL),
	 timeTags_(NULL),
     handle_(-1),
     nItems_(0),
     start_(0),
     bytes_(sizeof(T)),
     size_(0),
     ncols_(8),
     delete_when_done_(1)
{
}

template <class T>
DiskData<T>::DiskData(const DiskData& other)
{
   *this = other;
}


template <class T>
DiskData<T>::~DiskData()
{
   if (delete_when_done_) remove();
   if (name_) free(name_);
}


template <class T>
void DiskData<T>::remove()
{
   if (handle_ >= 0) {
      ::close(handle_);
      ::unlink(name_);
      handle_ = -1;
   }
   if (name_) {
	  free(name_);
   }
   name_ = NULL;
}


// Create a new file.

template <class T>
void DiskData<T>::create(const char *name, T data, size_t nitems,
						 int ntags, const time_t *tags, int ncols)
{
   const size_t BUFFER_SIZE = 16384;	// 16 KB
   T *buffer = NULL;
   unsigned int sz = 0;

   if (delete_when_done_) remove();

   ncols_ = ncols;

   if ((handle_ = ::open(name, O_RDWR|O_CREAT|O_TRUNC,
						 S_IRUSR|S_IWUSR|
						 S_IRGRP|S_IWGRP|
						 S_IROTH)) < 0) {
      ErrorMsg(name_);
      goto error;
   }

   name_ = strdup(name);

   if (::write(handle_, &nitems, sizeof(size_t)) != sizeof(size_t) ||
       ::write(handle_, &ntags, sizeof(int)) != sizeof(int)) {
      ErrorMsg(name_);
      goto error;
   }

   nTags_ = ntags;
   sz = ntags * sizeof(time_t);
   if (tags) {
	  timeTags_ = new time_t [ntags];
	  memcpy(timeTags_, tags, sz);
      if (::write(handle_, tags, sz) != sz) {
		 ErrorMsg(name_);
		 goto error;
      }
      delete_when_done_ = 0;
   } else {
	  timeTags_ = NULL;
      delete_when_done_ = 1;
   }

   start_ = sizeof(size_t) + sizeof(int) + sz;
   nItems_ = nitems;
   size_ = start_ + nItems_ * bytes_;
   
   buffer = new T [BUFFER_SIZE];

   size_t i;
   for (i = 0; i < BUFFER_SIZE; i ++) {
      buffer[i] = data;
   }

   for (i = 0; i < nitems; i += BUFFER_SIZE) {
      size_t left = nitems - i;
      if (left > BUFFER_SIZE) left = BUFFER_SIZE;
      size_t size = left * bytes_;
      if (::write(handle_, buffer, size) != size) {
		 ErrorMsg(name_);
		 goto error;
      }
   }

   delete [] buffer;

   return;

  error:

   std::cerr << "Error:: Failed creating temporary data file <"
		<< name << ">." << std::endl;
   theException->exit (1);
}


// Open an existing file. Return 0 if the file has same time tag,
// otherwise it returns -1 if error and 1 if time tags are not the
// same.

template <class T>
int DiskData<T>::open(
   const char *name,
   size_t n, int ntags, const time_t *tags,
   int ncols)
{
   // Close and may erase the old file 

   if (name != name_) {
	  if (delete_when_done_) {
		 remove();
	  }
	  if (name_) {
		 free(name_);
	  }
	  name_ = strdup(name);
	  delete_when_done_ = 1;
   } else {
	  delete_when_done_ = 0;
   }

   if ((handle_ = ::open(name, O_RDWR)) < 0) {
      return -1;
   }

   ncols_ = ncols;

   if (::read(handle_, &nItems_, sizeof(size_t)) != sizeof(size_t) ||
       ::read(handle_, &nTags_, sizeof(int)) != sizeof(int)) {
      return -1;
   } else if (n > 0 && n != nItems_) {
      return 1;
   } else if (ntags > 0 && nTags_ != ntags) {
      return 1;
   } else if (nTags_ > 0) {
	  timeTags_ = new time_t [nTags_];
	  size_ = nTags_ * sizeof(size_t);
	  if (::read(handle_, timeTags_, size_) != size_) {
		 return -1;
	  }
	  if (ntags > 0) {
		 for (int i = 0; i < nTags_; i ++) {
			if (tags[i] != timeTags_[i]) return 1;
		 }
	  }
      delete_when_done_ = 0;
   } else {
	  timeTags_ = NULL;
   }

   start_ = sizeof(size_t) + sizeof(int) + nTags_ * sizeof(time_t);
   bytes_ = sizeof(T);
   size_ = start_ + nItems_ * bytes_;

   return 0;
}


template <class T>
DiskData<T>& DiskData<T>::operator=(const DiskData<T> &other)
{
   static int id = 0;
   
   if (delete_when_done_) {
	  remove();
   }

   name_ = StrCopy("%s.%d", other.name_, (++id));

   // Copy the file

   if (ToolKit::copyFile(other.name_, name_) != 0) {
	  goto error;
   }
   if (open(name_) != 0) {
	  goto error;
   }

   return *this;
   
  error:
   
   std::cerr << "Error:: Failed copying <"
		<< other.name_ << "> to <" << name_ << ">." << std::endl;
   theException->exit(1);
   return *this;
}


// Return the ith data item

template <class T>
T DiskData<T>::read(size_t i)
{
   T data;
   if (::lseek(handle_, start_ + bytes_ * i, SEEK_SET) < 0 ||
       ::read(handle_, &data, bytes_) != bytes_) {
      std::cerr << "Error:: Failed reading data item <" << i
		   << "> from <" << name_ << ">." << std::endl;
      Error_Bail_Out();
   }
   return data;
}


// Write the data at the given pos

template <class T>
void DiskData<T>::write(size_t i, T data)
{
   if (::lseek(handle_, start_ + bytes_ * i, SEEK_SET) < 0 ||
       ::write(handle_, &data, bytes_) != bytes_) {
      std::cerr << "Error:: Failed writting data item <" << i
		   << "> from <" << name_ << ">." << std::endl;
      Error_Bail_Out();
   }
}

// Print all data

template <class T>
void DiskData<T>::print(std::ostream &os)
{
   os << "% File name  = " << name_ << '\n';
   os << "% Data items = " << nItems_ << '\n';
   os << "% Total size = " << size_ << '\n';
   os << "{" << '\n';
   T data;
   for (size_t i = 0; i < nItems_; i ++) {
      data = read(i);
      os << '\t' << data;
      if (i % ncols_ == 0) {
		 os << '\n';
      }
   }
   os << "}" << std::endl;
}


#endif
