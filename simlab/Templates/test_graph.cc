//-*-c++-*------------------------------------------------------------
// NAME: Template Classes
// NOTE: Testing template class Graph
// AUTH: Qi Yang
// FILE: test_graph.cc
// DATE: Thu Sep 21 22:00:04 1995
//--------------------------------------------------------------------

#include "Graph.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream.h>

void main()
{
  Graph<int, GraphNoInfo<int> > g(6, 11, 999);

  g.addLink( 0, 0, 1,  0);
  g.addLink( 1, 1, 2, 10);
  g.addLink( 2, 1, 3,  5);
  g.addLink( 3, 2, 3,  5);
  g.addLink( 4, 2, 4,  1);
  g.addLink( 5, 3, 2,  3);
  g.addLink( 6, 3, 4,  9);
  g.addLink( 7, 3, 5,  2);
  g.addLink( 8, 4, 5,  4);
  g.addLink( 9, 5, 1,  7);
  g.addLink(10, 5, 4,  6);

  cout << "\nLabel correcting:" << endl;
  g.labelCorrecting(0);
  g.printNodePathTree();

  cout << "\nLabel setting:" << endl;
  g.labelSetting(0, 8);
  g.printNodePathTree();

  cout << "\nLabel correcting (no turn from link 2 to 5):" << endl;
  g.penalty(2, 5, 99);
  g.labelCorrecting(0);
  g.printNodePathTree();
}



