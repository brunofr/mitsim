//-*-c++-*------------------------------------------------------------
// NAME: Template Classes
// NOTE: Array of objects of class T. T must be a derived class from
//       Object, Object, or NamedObject (see Object.h).
// AUTH: Qi Yang
// FILE: Array.h
// DATE: Thu Sep 21 21:35:59 1995
//--------------------------------------------------------------------


#ifndef ARRAY_HEADER
#define ARRAY_HEADER

#include "PtrArray.h"

template <class T> class Array : public PtrArray<T>
{
   protected:
      T*	objects_;	   // array of objects

   public:

      Array(int sz = 0);
      Array(T*, int sz);

      virtual ~Array() { delete [] objects_; }

      virtual int resize(int);
      virtual int resize() { return resize(this->nElements_); }
      virtual int grow(int gz = 0);
      virtual T* removeElementByIndex(int index);
};


#include "Array.cc"

#endif

