//-*-c++-*------------------------------------------------------
// NAME: Template Classes
// NOTE: Testing template List and ListItem.
// AUTH: Qi Yang
// FILE: test_array.cc
// DATE: Thu Sep 21 22:00:04 1995
//--------------------------------------------------------------

#include "test_array.h"
#include "Array.h"

#include <stdio.h>

void print(char *msg, Array<Foo> &a)
{
  cout << msg << ": " << a.nElements() << endl;
  for (int i = 0; i < a.nElements(); i ++) {
	a(i)->print();
  }
  cout << endl;
}

void main(int argc, char *argv[])
{
  const int n = 3;
  Array<Foo> a(n);
  int i;

  a(0)->set("Peter", 1);
  a(1)->set("Tony", 2);
  a(2)->set("Rabi", 3);
  print("Initail", a);

  a.resize(5);
  print("After Resized", a);

  a(3)->set("Hari", 4);
  a(4)->set("Yang Qi", 5);
  print("More Added", a);

  if (argc > 1) sscanf(argv[1], "%d", &i);
  else i = 1;
  a.sort(i);
  print("After Sorted", a);
}

