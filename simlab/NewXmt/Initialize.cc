/* 
 * Motif Tools Library, Version 2.0
 * $Id: Initialize.cc,v 1.3 1998/02/05 18:15:48 qiyang Exp $
 * 
 * Written by David Flanagan.
 * Copyright (c) 1992, 1993, 1994 by Dovetail Systems.
 * All Rights Reserved.  See the file COPYRIGHT for details.
 * This is not free software.  See the file SHAREWARE for details.
 * There is no warranty for this software.  See NO_WARRANTY for details.
 */


/*
 * Some of the code in this file is derived from the Xt sources:
 * Xt/Initialize.c and Xt/Display.c
 */

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <iostream>
//#include <sstream>

#include <Xmt/Xmt.h>
#include <Xmt/AppRes.h>
#include <Xmt/Converters.h>
#include <Xmt/Layout.h>

#include <Xmw/XmwColorMap.h>
#include <UTL/CloParser.h>
#include <UTL/Misc.h>

#if NeedFunctionPrototypes
Widget XmtInitialize(XtAppContext *app_context_return,
					 String application_class,
					 XrmOptionDescList options, Cardinal num_options,
					 int *argc_in_out, String *argv_in_out,
					 String *fallback_resources,
					 ArgList args, Cardinal num_args)
#else
Widget XmtInitialize(app_context_return, application_class,
					 options, num_options, argc_in_out, argv_in_out,
					 fallback_resources, args, num_args)
  XtAppContext *app_context_return;
  String application_class;
  XrmOptionDescList options;
  Cardinal num_options;
  int *argc_in_out;
  String *argv_in_out;
  String *fallback_resources;
  ArgList args;
  Cardinal num_args;
#endif
{
  Widget root;

  /*
   * Initialize Xt and create the root shell widget.
   */
  
  /* check if "-c" has been specified. */

  bool use_private_map = false;
  const char *dsp = NULL;
  CloParser cp;
  cp.add(new Clo("-c", &use_private_map, false, "Use private colormap"));
  cp.add(new Clo("-d", &dsp, NULL, "display"));

  cp.parse(argc_in_out, argv_in_out);

  if (dsp) {
  	if (strchr(dsp, ':')) {
  	  PutEnv("DISPLAY=%s", dsp);
  	} else {
  	  PutEnv("DISPLAY=%s:0", dsp);
  	}
  }
  
  if ( use_private_map == false ) {

    root = XtAppInitialize( app_context_return,
							application_class,
							options, num_options,
							argc_in_out, argv_in_out,
							fallback_resources,
							args, num_args);

  } else {	/* -c is specified in the arguments. */

	XtAppContext &context = *app_context_return;

	/* 
	 * Cannot use XtAppInitialize() because must allocate a private
	 * colormap.
	 */

	XmtPatchVisualInheritance();

	/* intialize Xt toolkit and application context. */

    XtToolkitInitialize();
    context = XtCreateApplicationContext();

	/* set the fallback resources and open Display. */

    XtAppSetFallbackResources( context, 
                               fallback_resources );

    Display* display = XtOpenDisplay( context,
									  0, NULL, "Visual", 0, 0,
									  argc_in_out, argv_in_out );

	if (display == NULL) XtError("Cannot open diplay");

	/* create private colormap. */

	XmwColorMap *cmap = new XmwColorMap ( display );

    Arg xargs[4];
    Cardinal argcnt = 0;

	XInstallColormap( display, cmap->colormap() );

	XtSetArg( xargs[argcnt], XmNvisual, cmap->visual() ); ++argcnt;
	XtSetArg( xargs[argcnt], XmNdepth, cmap->depth() ); ++argcnt;
	XtSetArg( xargs[argcnt], XmNcolormap, cmap->colormap() ); ++argcnt;

	/* finally create the root shell. */

    root = XtAppCreateShell( application_class, application_class,
                             applicationShellWidgetClass, display,
                             xargs, argcnt );
  }

  /* Continue normally the Xmt initialization ... */

  /*
   * parse Xmt-specific command-line options
   */

  XmtParseCommandLine(root, argc_in_out, argv_in_out);

  /*
   * register some type converters.
   * We do this now so we can convert
   * Xmt application resources read below.
   */

  XmtRegisterPixelConverter();
  XmtRegisterBitmapConverter();
  XmtRegisterBitmaskConverter();
  XmtRegisterPixmapConverter();
  XmtRegisterColorTableConverter();
  XmtRegisterWidgetConverter();
  XmtRegisterCallbackConverter();
  XmtRegisterXmStringConverter(); 
  XmtRegisterXmFontListConverter();
  XmtRegisterStringListConverter();
  XmtRegisterMenuItemsConverter();
  XmtRegisterPixmapListConverter();

  /*
   * These aren't strictly type converters, but
   * the idea is the same.
   */

  XmtRegisterLayoutParser();
  XmtRegisterLayoutCreateMethod();

  /*
   * Now register the shell, and look up the app-resources
   * (and do app-resource-related initialization.)
   */

  XmtInitializeApplicationShell(root, args, num_args);

  return(root);
}
